//package in.msewa.nearbymerchant;
//
//import android.content.Context;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//
//import in.msewa.model.MerchantAgent;
//import in.msewa.vpayqwik.R;
//
//public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
//
//  private SampleClusterItem currentItem;
//  private MerchantAgent merchantAgent;
//
//  @Override
//  protected void onCreate(Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    setContentView(R.layout.activity_maps_list);
//    ImageView ivBackBtn = (ImageView) findViewById(R.id.ivBackBtn);
//    ivBackBtn.setVisibility(View.VISIBLE);
//    Button nagivation = (Button) findViewById(R.id.nagivation);
//    nagivation.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        String uri = "http://maps.google.com/maps?saddr=" + merchantAgent.getCurrentlatitude() + "," + merchantAgent.getCurrentlongitude() + "&daddr=" + merchantAgent.getLatitude() + "," + merchantAgent.getLongitude();
//        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//        startActivity(intent);
//      }
//    });
////
//    ivBackBtn.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        onBackPressed();
//      }
//    });
//    merchantAgent = getIntent().getParcelableExtra("location");
//    if (savedInstanceState == null) {
//      setupMapFragment();
//    }
//  }
//
//  @Override
//  public void onMapReady(final GoogleMap googleMap) {
//
//    LatLng latLng = new LatLng(merchantAgent.getLatitude(), merchantAgent.getLongitude());
//    MarkerOptions markerOptions = new MarkerOptions();
//    markerOptions.position(latLng);
//    markerOptions.title(merchantAgent.getMerchantName());
//    markerOptions.snippet(merchantAgent.getAddress());
//    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//    Marker marker = googleMap.addMarker(markerOptions);
////     Showing InfoWindow on the GoogleMap
////    marker.showInfoWindow();
//
//    //move map camera
//    CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
//    CameraUpdate zoom = CameraUpdateFactory.zoomTo(100);
//    googleMap.moveCamera(center);
//    googleMap.animateCamera(zoom);
//    googleMap.setBuildingsEnabled(true);
//    googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//      @Override
//      public View getInfoWindow(Marker marker) {
//        return null;
//      }
//
//      @Override
//      public View getInfoContents(Marker marker) {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View v = inflater.inflate(R.layout.map_custom_infowindow, null);
//        TextView tvLat = (TextView) v.findViewById(R.id.name);
//        TextView tvLng = (TextView) v.findViewById(R.id.details);
//        tvLat.setText(merchantAgent.getMerchantName());
//        tvLng.setText(merchantAgent.getAddress());
//        return v;
//      }
//    });
//
//  }
//
//  private void setupMapFragment() {
//    SupportMapFragment mapFragment = new SupportMapFragment();
//    getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
//    mapFragment.setRetainInstance(true);
//    mapFragment.getMapAsync(this);
//  }
//
//
//}
