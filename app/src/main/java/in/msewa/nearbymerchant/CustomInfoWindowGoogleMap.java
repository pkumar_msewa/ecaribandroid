//package in.msewa.nearbymerchant;
//
//import android.app.Activity;
//import android.content.Context;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.model.Marker;
//
//import in.msewa.vpayqwik.R;
//
//public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {
//
//  private Context context;
//  private SampleClusterItem clusterItem;
//
//  public CustomInfoWindowGoogleMap(Context ctx, SampleClusterItem clusterItem) {
//    context = ctx;
//    clusterItem = clusterItem;
//  }
//
//  @Override
//  public View getInfoWindow(Marker marker) {
//    return null;
//  }
//
//  @Override
//  public View getInfoContents(Marker marker) {
//    View view = ((Activity) context).getLayoutInflater()
//      .inflate(R.layout.map_custom_infowindow, null);
//
//    TextView name_tv = view.findViewById(R.id.name);
//    TextView details_tv = view.findViewById(R.id.details);
//    ImageView img = view.findViewById(R.id.pic);
//
//
//    name_tv.setText(marker.getTitle());
//    details_tv.setText(marker.getSnippet());
//
////    SampleClusterItem infoWindowData = (SampleClusterItem) marker.getTag();
//
////    int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
////      "drawable", context.getPackageName());
////    img.setImageResource(imageId);
//
////    hotel_tv.setText(clusterItem.getSnippet());
////    food_tv.setText(clusterItem.getTitle());
//
//    return view;
//  }
//}
