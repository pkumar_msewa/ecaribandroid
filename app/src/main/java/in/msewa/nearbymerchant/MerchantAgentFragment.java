//package in.msewa.nearbymerchant;
//
//import android.annotation.SuppressLint;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Rect;
//import android.location.Address;
//import android.location.Geocoder;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.support.annotation.Nullable;
//import android.support.annotation.RequiresApi;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v4.view.ViewPager;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Html;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.androidnetworking.AndroidNetworking;
//import com.androidnetworking.common.Priority;
//import com.androidnetworking.error.ANError;
//import com.androidnetworking.interfaces.JSONObjectRequestListener;
//import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
//import com.google.android.gms.common.GooglePlayServicesRepairableException;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
//import com.google.android.gms.location.places.ui.PlacePicker;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.maps.android.SphericalUtil;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//
////import in.msewa.adapter.NearByMerchantAdapter;
//import in.msewa.custom.CustomAlertDialog;
//import in.msewa.custom.CustomToast;
//import in.msewa.custom.LoadingDialog;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.metadata.AppMetadata;
//import in.msewa.model.MerchantAgent;
//import in.msewa.model.UserModel;
//import in.msewa.permission.PermissionHandler;
//import in.msewa.permission.Permissions;
//import in.msewa.vpayqwik.Manifest;
//import in.msewa.vpayqwik.R;
//
//import static android.app.Activity.RESULT_OK;
//
///**
// * Created by kashifimam on 21/02/17.
// */
//
//public class MerchantAgentFragment extends Fragment implements LocationListener {
//
//  private static final String TAG = "values";
//  private static Geocoder geocoder;
//  private ImageView ivBanner;
//  private TabLayout tabPage;
//  //  private ProgressBar pbGiftCardCat;
//  private ViewPager viewPage;
//  private UserModel session = UserModel.getInstance();
//  String cardTerms;
//  private ArrayList<MerchantAgent> merchantAgents;
//
//  //Volley
//  private String tag_json_obj = "json_events";
//  private JsonObjectRequest postReq;
//  private HashMap<String, String> stringStringHashMap;
//  private LoadingDialog loadingDialog;
//  private RecyclerView merchantLocation;
//  private PlaceAutocompleteFragment placeAutocompleteFragment;
//  private LocationManager locationManager;
//  private boolean isGPSEnable;
//  private boolean isNetworkEnable;
//  private Location location;
//  private String url;
//  private TextView place_autocomplete_fragment;
//  private LinearLayout llNoReceipt;
//  private TextView title;
//
//  @SuppressLint("ClickableViewAccessibility")
//  @Nullable
//  @Override
//  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//    View rootView = inflater.inflate(R.layout.fragment_near_by_merchant, container, false);
//    loadingDialog = new LoadingDialog(getActivity());
//    merchantLocation = (RecyclerView) rootView.findViewById(R.id.merchantLocation);
//    llNoReceipt = (LinearLayout) rootView.findViewById(R.id.llNoReceipt);
//    title = (TextView) rootView.findViewById(R.id.tvNoReceipt);
//    place_autocomplete_fragment = (TextView) rootView.findViewById(R.id.place_autocomplete_fragment);
////    ivBanner.setVisibility(View.GONE);
//    place_autocomplete_fragment.setOnTouchListener(new View.OnTouchListener() {
//      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
//      @Override
//      public boolean onTouch(View v, MotionEvent event) {
//        final int DRAWABLE_LEFT = 0;
//        final int DRAWABLE_TOP = 1;
//        final int DRAWABLE_RIGHT = 2;
//        final int DRAWABLE_BOTTOM = 3;
//
//        if (event.getAction() == MotionEvent.ACTION_UP) {
//          if (event.getRawX() >= (place_autocomplete_fragment.getRight() - place_autocomplete_fragment.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//            // your action here
//            if (location != null) {
//              try {
//                place_autocomplete_fragment.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_my_location_black_24dp, 0);
//                place_autocomplete_fragment.setText(getAddressFromLocation(location.getLatitude(), location.getLongitude(), getActivity()));
//              } catch (IOException e) {
//                e.printStackTrace();
//              }
//            }
//
//            return true;
//          }
//        }
//        return false;
//      }
//    });
//
//    place_autocomplete_fragment.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//        try {
//          startActivityForResult(builder.build(getActivity()), 1);
//        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
//          e.printStackTrace();
//        }
//      }
//    });
//    if (Build.VERSION.SDK_INT >= 23) {
//      Permissions.check(getActivity(), new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, 0, null,
//        new PermissionHandler() {
//          @Override
//          public void onGranted() {
//            getLocation();
//          }
//        });
//
//    } else {
//      getLocation();
//    }
//
//    return rootView;
//  }
//
//  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
//  @Override
//  public void onActivityResult(int requestCode, int resultCode, Intent data) {
//    if (requestCode == 1) {
//      if (resultCode == RESULT_OK) {
//        Place place = PlacePicker.getPlace(data, getActivity());
//        StringBuilder stBuilder = new StringBuilder();
//        String placename = String.format("%s", place.getName());
//        String latitude = String.valueOf(place.getLatLng().latitude);
//        String longitude = String.valueOf(place.getLatLng().longitude);
//        String address = String.format("%s", place.getAddress());
//        place_autocomplete_fragment.setText(address);
//        place_autocomplete_fragment.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_location_searching_black_24dp, 0);
//        try {
//          getCardCategory(Double.parseDouble(latitude), Double.parseDouble(longitude));
//        } catch (JSONException e) {
//          e.printStackTrace();
//        }
//      }
//    }
//  }
//
//  public String loadJSONFromAsset() {
//    String json = null;
//    try {
//      InputStream is;
//      if (getArguments().getBoolean("merchant", false)) {
//        is = getActivity().getAssets().open("Merchant.json");
//      } else {
//        is = getActivity().getAssets().open("Agent.json");
//      }
//
//      int size = is.available();
//      byte[] buffer = new byte[size];
//      is.read(buffer);
//      is.close();
//      json = new String(buffer, "UTF-8");
//    } catch (IOException ex) {
//      ex.printStackTrace();
//      return null;
//    }
//    return json;
//  }
//
//  public void getCardCategory(final double latitude, final double longitude) throws JSONException {
//    llNoReceipt.setVisibility(View.GONE);
//    merchantLocation.setVisibility(View.VISIBLE);
//    if (getArguments().getBoolean("merchant", false)) {
//      url = ApiUrl.URL_NEAR_BY_MERCHANT;
//    } else {
//      url = ApiUrl.URL_NEAR_BY_AGENT;
//    }
////    pbGiftCardCat.setVisibility(View.VISIBLE);
////    rvGiftCardCat.setVisibility(View.GONE);
//    JSONObject jsonObject1 = new JSONObject();
//    jsonObject1.put("sessionId", session.getUserSessionId());
//    jsonObject1.put("latitude", latitude);
//    jsonObject1.put("longitude", longitude);
//    jsonObject1.put("radius", "2");
//    Log.i("results", jsonObject1.toString());
//    Log.i("resultsURL", url);
//    loadingDialog.show();
//    AndroidNetworking.post(url)
//      .addJSONObjectBody(jsonObject1)
//      .setTag("test")
//      .setPriority(Priority.HIGH)
//      .build()
//      .getAsJSONObject(new JSONObjectRequestListener() {
//        @Override
//        public void onResponse(JSONObject jsonObj) {
////
//          try {
////      JSONObject jsonObj;
////      if (getArguments().getBoolean("merchant", false)) {
//////        jsonObj = new JSONObject(loadJSONFromAsset());
////      } else {
//////        jsonObj = new JSONObject(loadJSONFromAsset());
////      }
//            Log.i("EventsCatResponse", jsonObj.toString());
//            String code = jsonObj.getString("code");
//            String message = jsonObj.getString("message");
//            String res = jsonObj.getString("response");
//            merchantAgents = new ArrayList<>();
//            JSONObject response = new JSONObject(res);
//            loadingDialog.dismiss();
//            if (code.equalsIgnoreCase("S00")) {
//              JSONArray jsonObject = response.getJSONArray("details");
//              for (int i = 0; i < jsonObject.length(); i++) {
//                if (getArguments().getBoolean("merchant", false)) {
//                  MerchantAgent merchantAgent = new MerchantAgent(jsonObject.getJSONObject(i).getDouble("latitude"), jsonObject.getJSONObject(i).getDouble("longitude"), jsonObject.getJSONObject(i).getString("merchantName"), jsonObject.getJSONObject(i).getString("betweenDistance"), jsonObject.getJSONObject(i).getString("address"), latitude, longitude);
//                  merchantAgents.add(merchantAgent);
//                } else {
//                  MerchantAgent merchantAgent = new MerchantAgent(jsonObject.getJSONObject(i).getDouble("latitude"), jsonObject.getJSONObject(i).getDouble("longitude"), jsonObject.getJSONObject(i).getString("agentName"), jsonObject.getJSONObject(i).getString("betweenDistance"), jsonObject.getJSONObject(i).getString("address"), latitude, longitude);
//                  merchantAgents.add(merchantAgent);
//                }
//              }
//              if (merchantAgents.size() != 0) {
//                NearByMerchantAdapter nearByMerchantAdapter = new NearByMerchantAdapter(getActivity(), merchantAgents);
//                merchantLocation.setLayoutManager(new LinearLayoutManager(getActivity()));
//                merchantLocation.setHasFixedSize(true);
//                merchantLocation.setAdapter(nearByMerchantAdapter);
//              }
//            } else if (code.equalsIgnoreCase("F03")) {
//              showInvalidSessionDialog();
//            } else {
//              llNoReceipt.setVisibility(View.VISIBLE);
//              merchantLocation.setVisibility(View.GONE);
//              title.setText(message);
//            }
//
////
//          } catch (JSONException e) {
//            e.printStackTrace();
//            loadingDialog.dismiss();
//            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//          }
//
//        }
//
//        @Override
//        public void onError(ANError anError) {
//          loadingDialog.dismiss();
//          CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception));
//        }
//      });
//  }
//
//
//  public class ViewPagerAdapter extends FragmentStatePagerAdapter {
//
//    ArrayList<String> Titles; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
//    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
//
//
//    // Build a Constructor and assign the passed Values to appropriate values in the class
//    public ViewPagerAdapter(FragmentManager fm, ArrayList<String> mTitles, int mNumbOfTabsumb) {
//      super(fm);
//      this.Titles = mTitles;
//      this.NumbOfTabs = mNumbOfTabsumb;
//
//    }
//
//    @Override
//    public Fragment getItem(int position) {
//
//
////      GiftCardCatFragment tab1 = new GiftCardCatFragment();
////      Bundle bundle = new Bundle();
////      bundle.putInt("values", Integer.parseInt(stringStringHashMap.get(Titles.get(position))));
////      tab1.setArguments(bundle);
//      return null;
//
//    }
//
//    @Override
//    public CharSequence getPageTitle(int position) {
//      return Titles.get(position);
//    }
//
//
//    @Override
//    public int getCount() {
//      return NumbOfTabs;
//    }
//  }
//
//
//  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
//    private int space;
//
//    public SpacesItemDecoration(int space) {
//      this.space = space;
//    }
//
//    @Override
//    public void getItemOffsets(Rect outRect, View view,
//                               RecyclerView parent, RecyclerView.State state) {
//      outRect.left = space;
//      outRect.right = space;
//      outRect.bottom = space;
//      outRect.top = space;
//    }
//
//  }
//
//  public static Double distanceBetween(LatLng point1, LatLng point2) {
//    if (point1 == null || point2 == null) {
//      return null;
//    }
//
//    return SphericalUtil.computeDistanceBetween(point1, point2);
//  }
//
//  @Override
//  public void onDetach() {
//    super.onDetach();
//  }
//
//  public void showInvalidSessionDialog() {
//    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
//    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//      public void onClick(DialogInterface dialog, int id) {
//        sendLogout();
//      }
//    });
//    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//      public void onClick(DialogInterface dialog, int id) {
//        dialog.dismiss();
//
//      }
//    });
//    builder.show();
//  }
//
//  private void sendLogout() {
//    Intent intent = new Intent("setting-change");
//    intent.putExtra("updates", "4");
//    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
//  }
//
//  private void getLocation() {
//
//    locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
//    isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//    isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//
//    if (!isGPSEnable && !isNetworkEnable) {
//      buildAlertMessageNoGps();
//    } else {
//      if (isNetworkEnable) {
//        location = null;
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 0, this);
//        if (locationManager != null) {
//          location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//          if (location != null) {
//            try {
//              place_autocomplete_fragment.setText(getAddressFromLocation(location.getLatitude(), location.getLongitude(), getActivity()));
//              getCardCategory(location.getLatitude(), location.getLongitude());
//            } catch (JSONException e) {
//              e.printStackTrace();
//            } catch (IOException e) {
//              e.printStackTrace();
//            }
//
//          }
//        }
//      } else if (isGPSEnable) {
//        location = null;
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
//        if (locationManager != null) {
//          location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//          if (location != null) {
//            try {
//              getCardCategory(location.getLatitude(), location.getLongitude());
//            } catch (JSONException e) {
//              e.printStackTrace();
//            }
//
//          }
//        }
//      }
//
//    }
//  }
//
//  private class GeocoderHandler extends Handler {
//    @Override
//    public void handleMessage(Message message) {
//      String locationAddress;
//      switch (message.what) {
//        case 1:
//          Bundle bundle = message.getData();
//          locationAddress = bundle.getString("address");
//          break;
//        default:
//          locationAddress = null;
//      }
//      place_autocomplete_fragment.setText(locationAddress);
//    }
//  }
//
//  private void buildAlertMessageNoGps() {
//    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//    builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
//      .setCancelable(false)
//      .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//        public void onClick(final DialogInterface dialog, final int id) {
//          startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//        }
//      })
//      .setNegativeButton("No", new DialogInterface.OnClickListener() {
//        public void onClick(final DialogInterface dialog, final int id) {
//          dialog.cancel();
//        }
//      });
//    final AlertDialog alert = builder.create();
//    alert.show();
//  }
//
//  @Override
//  public void onLocationChanged(Location location) {
//
//  }
//
//  @Override
//  public void onStatusChanged(String provider, int status, Bundle extras) {
//
//  }
//
//  @Override
//  public void onProviderEnabled(String provider) {
//
//  }
//
//  @Override
//  public void onProviderDisabled(String provider) {
//
//  }
//
//  public String getAddressFromLocation(final double latitude, final double longitude,
//                                       final Context context) throws IOException {
//    List<Address> addresses;
//    geocoder = new Geocoder(context, Locale.getDefault());
//
//    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//
//    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//    String city = addresses.get(0).getLocality();
//    String state = addresses.get(0).getAdminArea();
//    String country = addresses.get(0).getCountryName();
//    String postalCode = addresses.get(0).getPostalCode();
//    String knownName = addresses.get(0).getFeatureName();
//    return address;
//  }
//}
