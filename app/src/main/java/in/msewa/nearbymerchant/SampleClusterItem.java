//package in.msewa.nearbymerchant;
//
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//
//import com.google.android.gms.maps.model.LatLng;
//
//import net.sharewire.googlemapsclustering.ClusterItem;
//
//class SampleClusterItem implements ClusterItem {
//
//    private final LatLng location;
//    private String tittle;
//    private String snippet;
//
//    SampleClusterItem(@NonNull LatLng location, @NonNull String tittle, @NonNull String snippet) {
//        this.location = location;
//        this.tittle = tittle;
//        this.snippet = snippet;
//    }
//
//    @Override
//    public double getLatitude() {
//        return location.latitude;
//    }
//
//    @Override
//    public double getLongitude() {
//        return location.longitude;
//    }
//
//    @Nullable
//    @Override
//    public String getTitle() {
//        return tittle;
//    }
//
//    @Nullable
//    @Override
//    public String getSnippet() {
//        return snippet;
//    }
//}
