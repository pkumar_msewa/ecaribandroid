package in.msewa.util;


import android.util.Base64;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Ksf on 5/29/2016.
 */
public class EncryptDecryptCheckUtility {
    private static final String ALGO = "AES";
    private static final String WORKING_KEY = "VIJAYABANKM2PBNK";



    public static String decrypt_data(String encData)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec skeySpec = new SecretKeySpec(WORKING_KEY.getBytes(), "AES");

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] original = cipher
                .doFinal(Base64.decode(encData.getBytes(),Base64.DEFAULT));

        return new String(original).trim();
    }

    public static String encrypt_data(String data)
            throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(WORKING_KEY.getBytes(), "AES");

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(data.getBytes());
//        byte[] original = Base64.encode(cipher.doFinal(data.getBytes()), Base64.DEFAULT);
        return new String(encrypted);
    }
}
