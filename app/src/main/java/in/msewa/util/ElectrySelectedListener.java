package in.msewa.util;

import in.msewa.model.BankListModel;
import in.msewa.model.BillPaymentOperatorsModel;
import in.msewa.model.BusCityModel;
import in.msewa.model.TravelCityModel;

/**
 * Created by Ksf on 9/30/2016.
 */
public interface ElectrySelectedListener {

  public void Selected(BillPaymentOperatorsModel type, int currentPostion);
}
