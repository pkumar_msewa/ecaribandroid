package in.msewa.util;

import in.msewa.model.BankListModel;
import in.msewa.model.BusCityModel;
import in.msewa.model.TravelCityModel;

/**
 * Created by Ksf on 9/30/2016.
 */
public interface CitySelectedListener {

    public void citySelected(String type, long cityCode, String cityName );
    public void cityFilter(BusCityModel type);
  public void bankFilter(BankListModel type);
  public void travelFilter(TravelCityModel type);
}
