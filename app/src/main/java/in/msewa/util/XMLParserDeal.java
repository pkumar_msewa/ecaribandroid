package in.msewa.util;

import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.ArrayList;

import in.msewa.model.MyCouponsModel;

/**
 * Created by admin on 7/27/2015.
 */
public class XMLParserDeal {


    // TODO XML Deal Child Node
    public static final String KEY_DEALS = "ROWSET";
    public static final String KEY_DEAL = "ROW";
    public static final String KEY_DEAL_ID = "id";
    public static final String KEY_DEAL_TITLE = "title";
    public static final String KEY_DEAL_SHORT_TITLE = "name";
    public static final String KEY_DEAL_Body = "DealBody";
    public static final String KEY_DEAL_RESTRICTIONS = "DealRestrictions";
    public static final String KEY_MERCHANT_NAME = "brand";
    public static final String KEY_MERCHANT_ADDRESS = "categoryid2";
    public static final String KEY_DEAL_CATEGORIES = "categoryid1";
    public static final String KEY_DEAL_URI = "producturl";
    public static final String KEY_DEAL_IMAGE_URL = "smallimage";


    private static XMLParserUtils parser = new XMLParserUtils();
    private static String assetString;

    private MyCouponsModel myCouponsModel;

    public ArrayList<MyCouponsModel> getData(Context context) {
        String xml = null;
        try {
            xml = parser.doGetRequest("http://grouponindia1.go2feeds.org/feed.php?subdomain=grouponindia1&aff_id=4044&offer_id=9&file_id=248&feed=aHR0cHM6Ly9uZWFyYnV5LWltYWdlcy5zMy5hbWF6b25hd3MuY29tL3htbF9kYXRhLnhtbA&objects=ROWSET&object=ROW&url_tag=producturl&url_query=utm_source%3Dnap%26utm_medium%3Dcps%26utm_campaign%3D%7Baffiliate_id%7D%26utm_content%3Dxml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Document doc = parser.getDomElement(xml);
        return getDealOnFromDocument(doc);
    }


    public ArrayList<MyCouponsModel> getDealOnFromDocument(Document doc) {
        ArrayList<MyCouponsModel> datas = new ArrayList<>();
        try {

            NodeList deals = doc.getElementsByTagName(KEY_DEALS);
            for (int i = 0; i < deals.getLength(); i++) {
                NodeList nl = doc.getElementsByTagName(KEY_DEAL);
                for (int j = 0; j < 52; j++) {
                    Element e = (Element) nl.item(j);
                    String dealID = parser.getValueGroupOn(e, KEY_DEAL_ID);
                    String DealTitle = parser.getValueGroupOn(e, KEY_DEAL_TITLE);
                    String merchantName = parser.getValueGroupOn(e, KEY_MERCHANT_NAME);
                    String merchantAddress = parser.getValueGroupOn(e,KEY_MERCHANT_ADDRESS);
                    String dealURL = parser.getValueGroupOn(e, KEY_DEAL_URI);
                    String dealImageURL = parser.getValueGroupOn(e, KEY_DEAL_IMAGE_URL);

                    myCouponsModel = new MyCouponsModel(DealTitle,dealImageURL, merchantAddress,merchantName,"",dealURL,dealID,"");
                    myCouponsModel.save();
                    datas.add(myCouponsModel);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return datas;
    }

}


