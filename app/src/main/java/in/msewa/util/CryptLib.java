package in.msewa.util;
/*
 * MIT License
 *
 * Copyright (c) 2017 Kavin Varnan
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import android.util.Base64;

public class CryptLib {

    /**
     * Encryption mode enumeration
     */
    private enum EncryptMode {
        ENCRYPT, DECRYPT;
    }

    // cipher to be used for encryption and decryption
    Cipher _cx;
    String s="[ \n" +
      "  {name: 'Afghanistan', code: 'AF'}, \n" +
      "  {name: 'Åland Islands', code: 'AX'}, \n" +
      "  {name: 'Albania', code: 'AL'}, \n" +
      "  {name: 'Algeria', code: 'DZ'}, \n" +
      "  {name: 'American Samoa', code: 'AS'}, \n" +
      "  {name: 'AndorrA', code: 'AD'}, \n" +
      "  {name: 'Angola', code: 'AO'}, \n" +
      "  {name: 'Anguilla', code: 'AI'}, \n" +
      "  {name: 'Antarctica', code: 'AQ'}, \n" +
      "  {name: 'Antigua and Barbuda', code: 'AG'}, \n" +
      "  {name: 'Argentina', code: 'AR'}, \n" +
      "  {name: 'Armenia', code: 'AM'}, \n" +
      "  {name: 'Aruba', code: 'AW'}, \n" +
      "  {name: 'Australia', code: 'AU'}, \n" +
      "  {name: 'Austria', code: 'AT'}, \n" +
      "  {name: 'Azerbaijan', code: 'AZ'}, \n" +
      "  {name: 'Bahamas', code: 'BS'}, \n" +
      "  {name: 'Bahrain', code: 'BH'}, \n" +
      "  {name: 'Bangladesh', code: 'BD'}, \n" +
      "  {name: 'Barbados', code: 'BB'}, \n" +
      "  {name: 'Belarus', code: 'BY'}, \n" +
      "  {name: 'Belgium', code: 'BE'}, \n" +
      "  {name: 'Belize', code: 'BZ'}, \n" +
      "  {name: 'Benin', code: 'BJ'}, \n" +
      "  {name: 'Bermuda', code: 'BM'}, \n" +
      "  {name: 'Bhutan', code: 'BT'}, \n" +
      "  {name: 'Bolivia', code: 'BO'}, \n" +
      "  {name: 'Bosnia and Herzegovina', code: 'BA'}, \n" +
      "  {name: 'Botswana', code: 'BW'}, \n" +
      "  {name: 'Bouvet Island', code: 'BV'}, \n" +
      "  {name: 'Brazil', code: 'BR'}, \n" +
      "  {name: 'British Indian Ocean Territory', code: 'IO'}, \n" +
      "  {name: 'Brunei Darussalam', code: 'BN'}, \n" +
      "  {name: 'Bulgaria', code: 'BG'}, \n" +
      "  {name: 'Burkina Faso', code: 'BF'}, \n" +
      "  {name: 'Burundi', code: 'BI'}, \n" +
      "  {name: 'Cambodia', code: 'KH'}, \n" +
      "  {name: 'Cameroon', code: 'CM'}, \n" +
      "  {name: 'Canada', code: 'CA'}, \n" +
      "  {name: 'Cape Verde', code: 'CV'}, \n" +
      "  {name: 'Cayman Islands', code: 'KY'}, \n" +
      "  {name: 'Central African Republic', code: 'CF'}, \n" +
      "  {name: 'Chad', code: 'TD'}, \n" +
      "  {name: 'Chile', code: 'CL'}, \n" +
      "  {name: 'China', code: 'CN'}, \n" +
      "  {name: 'Christmas Island', code: 'CX'}, \n" +
      "  {name: 'Cocos (Keeling) Islands', code: 'CC'}, \n" +
      "  {name: 'Colombia', code: 'CO'}, \n" +
      "  {name: 'Comoros', code: 'KM'}, \n" +
      "  {name: 'Congo', code: 'CG'}, \n" +
      "  {name: 'Congo, The Democratic Republic of the', code: 'CD'}, \n" +
      "  {name: 'Cook Islands', code: 'CK'}, \n" +
      "  {name: 'Costa Rica', code: 'CR'}, \n" +
      "  {name: 'Cote D\\'Ivoire', code: 'CI'}, \n" +
      "  {name: 'Croatia', code: 'HR'}, \n" +
      "  {name: 'Cuba', code: 'CU'}, \n" +
      "  {name: 'Cyprus', code: 'CY'}, \n" +
      "  {name: 'Czech Republic', code: 'CZ'}, \n" +
      "  {name: 'Denmark', code: 'DK'}, \n" +
      "  {name: 'Djibouti', code: 'DJ'}, \n" +
      "  {name: 'Dominica', code: 'DM'}, \n" +
      "  {name: 'Dominican Republic', code: 'DO'}, \n" +
      "  {name: 'Ecuador', code: 'EC'}, \n" +
      "  {name: 'Egypt', code: 'EG'}, \n" +
      "  {name: 'El Salvador', code: 'SV'}, \n" +
      "  {name: 'Equatorial Guinea', code: 'GQ'}, \n" +
      "  {name: 'Eritrea', code: 'ER'}, \n" +
      "  {name: 'Estonia', code: 'EE'}, \n" +
      "  {name: 'Ethiopia', code: 'ET'}, \n" +
      "  {name: 'Falkland Islands (Malvinas)', code: 'FK'}, \n" +
      "  {name: 'Faroe Islands', code: 'FO'}, \n" +
      "  {name: 'Fiji', code: 'FJ'}, \n" +
      "  {name: 'Finland', code: 'FI'}, \n" +
      "  {name: 'France', code: 'FR'}, \n" +
      "  {name: 'French Guiana', code: 'GF'}, \n" +
      "  {name: 'French Polynesia', code: 'PF'}, \n" +
      "  {name: 'French Southern Territories', code: 'TF'}, \n" +
      "  {name: 'Gabon', code: 'GA'}, \n" +
      "  {name: 'Gambia', code: 'GM'}, \n" +
      "  {name: 'Georgia', code: 'GE'}, \n" +
      "  {name: 'Germany', code: 'DE'}, \n" +
      "  {name: 'Ghana', code: 'GH'}, \n" +
      "  {name: 'Gibraltar', code: 'GI'}, \n" +
      "  {name: 'Greece', code: 'GR'}, \n" +
      "  {name: 'Greenland', code: 'GL'}, \n" +
      "  {name: 'Grenada', code: 'GD'}, \n" +
      "  {name: 'Guadeloupe', code: 'GP'}, \n" +
      "  {name: 'Guam', code: 'GU'}, \n" +
      "  {name: 'Guatemala', code: 'GT'}, \n" +
      "  {name: 'Guernsey', code: 'GG'}, \n" +
      "  {name: 'Guinea', code: 'GN'}, \n" +
      "  {name: 'Guinea-Bissau', code: 'GW'}, \n" +
      "  {name: 'Guyana', code: 'GY'}, \n" +
      "  {name: 'Haiti', code: 'HT'}, \n" +
      "  {name: 'Heard Island and Mcdonald Islands', code: 'HM'}, \n" +
      "  {name: 'Holy See (Vatican City State)', code: 'VA'}, \n" +
      "  {name: 'Honduras', code: 'HN'}, \n" +
      "  {name: 'Hong Kong', code: 'HK'}, \n" +
      "  {name: 'Hungary', code: 'HU'}, \n" +
      "  {name: 'Iceland', code: 'IS'}, \n" +
      "  {name: 'India', code: 'IN'}, \n" +
      "  {name: 'Indonesia', code: 'ID'}, \n" +
      "  {name: 'Iran, Islamic Republic Of', code: 'IR'}, \n" +
      "  {name: 'Iraq', code: 'IQ'}, \n" +
      "  {name: 'Ireland', code: 'IE'}, \n" +
      "  {name: 'Isle of Man', code: 'IM'}, \n" +
      "  {name: 'Israel', code: 'IL'}, \n" +
      "  {name: 'Italy', code: 'IT'}, \n" +
      "  {name: 'Jamaica', code: 'JM'}, \n" +
      "  {name: 'Japan', code: 'JP'}, \n" +
      "  {name: 'Jersey', code: 'JE'}, \n" +
      "  {name: 'Jordan', code: 'JO'}, \n" +
      "  {name: 'Kazakhstan', code: 'KZ'}, \n" +
      "  {name: 'Kenya', code: 'KE'}, \n" +
      "  {name: 'Kiribati', code: 'KI'}, \n" +
      "  {name: 'Korea, Democratic People\\'S Republic of', code: 'KP'}, \n" +
      "  {name: 'Korea, Republic of', code: 'KR'}, \n" +
      "  {name: 'Kuwait', code: 'KW'}, \n" +
      "  {name: 'Kyrgyzstan', code: 'KG'}, \n" +
      "  {name: 'Lao People\\'S Democratic Republic', code: 'LA'}, \n" +
      "  {name: 'Latvia', code: 'LV'}, \n" +
      "  {name: 'Lebanon', code: 'LB'}, \n" +
      "  {name: 'Lesotho', code: 'LS'}, \n" +
      "  {name: 'Liberia', code: 'LR'}, \n" +
      "  {name: 'Libyan Arab Jamahiriya', code: 'LY'}, \n" +
      "  {name: 'Liechtenstein', code: 'LI'}, \n" +
      "  {name: 'Lithuania', code: 'LT'}, \n" +
      "  {name: 'Luxembourg', code: 'LU'}, \n" +
      "  {name: 'Macao', code: 'MO'}, \n" +
      "  {name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'}, \n" +
      "  {name: 'Madagascar', code: 'MG'}, \n" +
      "  {name: 'Malawi', code: 'MW'}, \n" +
      "  {name: 'Malaysia', code: 'MY'}, \n" +
      "  {name: 'Maldives', code: 'MV'}, \n" +
      "  {name: 'Mali', code: 'ML'}, \n" +
      "  {name: 'Malta', code: 'MT'}, \n" +
      "  {name: 'Marshall Islands', code: 'MH'}, \n" +
      "  {name: 'Martinique', code: 'MQ'}, \n" +
      "  {name: 'Mauritania', code: 'MR'}, \n" +
      "  {name: 'Mauritius', code: 'MU'}, \n" +
      "  {name: 'Mayotte', code: 'YT'}, \n" +
      "  {name: 'Mexico', code: 'MX'}, \n" +
      "  {name: 'Micronesia, Federated States of', code: 'FM'}, \n" +
      "  {name: 'Moldova, Republic of', code: 'MD'}, \n" +
      "  {name: 'Monaco', code: 'MC'}, \n" +
      "  {name: 'Mongolia', code: 'MN'}, \n" +
      "  {name: 'Montserrat', code: 'MS'}, \n" +
      "  {name: 'Morocco', code: 'MA'}, \n" +
      "  {name: 'Mozambique', code: 'MZ'}, \n" +
      "  {name: 'Myanmar', code: 'MM'}, \n" +
      "  {name: 'Namibia', code: 'NA'}, \n" +
      "  {name: 'Nauru', code: 'NR'}, \n" +
      "  {name: 'Nepal', code: 'NP'}, \n" +
      "  {name: 'Netherlands', code: 'NL'}, \n" +
      "  {name: 'Netherlands Antilles', code: 'AN'}, \n" +
      "  {name: 'New Caledonia', code: 'NC'}, \n" +
      "  {name: 'New Zealand', code: 'NZ'}, \n" +
      "  {name: 'Nicaragua', code: 'NI'}, \n" +
      "  {name: 'Niger', code: 'NE'}, \n" +
      "  {name: 'Nigeria', code: 'NG'}, \n" +
      "  {name: 'Niue', code: 'NU'}, \n" +
      "  {name: 'Norfolk Island', code: 'NF'}, \n" +
      "  {name: 'Northern Mariana Islands', code: 'MP'}, \n" +
      "  {name: 'Norway', code: 'NO'}, \n" +
      "  {name: 'Oman', code: 'OM'}, \n" +
      "  {name: 'Pakistan', code: 'PK'}, \n" +
      "  {name: 'Palau', code: 'PW'}, \n" +
      "  {name: 'Palestinian Territory, Occupied', code: 'PS'}, \n" +
      "  {name: 'Panama', code: 'PA'}, \n" +
      "  {name: 'Papua New Guinea', code: 'PG'}, \n" +
      "  {name: 'Paraguay', code: 'PY'}, \n" +
      "  {name: 'Peru', code: 'PE'}, \n" +
      "  {name: 'Philippines', code: 'PH'}, \n" +
      "  {name: 'Pitcairn', code: 'PN'}, \n" +
      "  {name: 'Poland', code: 'PL'}, \n" +
      "  {name: 'Portugal', code: 'PT'}, \n" +
      "  {name: 'Puerto Rico', code: 'PR'}, \n" +
      "  {name: 'Qatar', code: 'QA'}, \n" +
      "  {name: 'Reunion', code: 'RE'}, \n" +
      "  {name: 'Romania', code: 'RO'}, \n" +
      "  {name: 'Russian Federation', code: 'RU'}, \n" +
      "  {name: 'RWANDA', code: 'RW'}, \n" +
      "  {name: 'Saint Helena', code: 'SH'}, \n" +
      "  {name: 'Saint Kitts and Nevis', code: 'KN'}, \n" +
      "  {name: 'Saint Lucia', code: 'LC'}, \n" +
      "  {name: 'Saint Pierre and Miquelon', code: 'PM'}, \n" +
      "  {name: 'Saint Vincent and the Grenadines', code: 'VC'}, \n" +
      "  {name: 'Samoa', code: 'WS'}, \n" +
      "  {name: 'San Marino', code: 'SM'}, \n" +
      "  {name: 'Sao Tome and Principe', code: 'ST'}, \n" +
      "  {name: 'Saudi Arabia', code: 'SA'}, \n" +
      "  {name: 'Senegal', code: 'SN'}, \n" +
      "  {name: 'Serbia and Montenegro', code: 'CS'}, \n" +
      "  {name: 'Seychelles', code: 'SC'}, \n" +
      "  {name: 'Sierra Leone', code: 'SL'}, \n" +
      "  {name: 'Singapore', code: 'SG'}, \n" +
      "  {name: 'Slovakia', code: 'SK'}, \n" +
      "  {name: 'Slovenia', code: 'SI'}, \n" +
      "  {name: 'Solomon Islands', code: 'SB'}, \n" +
      "  {name: 'Somalia', code: 'SO'}, \n" +
      "  {name: 'South Africa', code: 'ZA'}, \n" +
      "  {name: 'South Georgia and the South Sandwich Islands', code: 'GS'}, \n" +
      "  {name: 'Spain', code: 'ES'}, \n" +
      "  {name: 'Sri Lanka', code: 'LK'}, \n" +
      "  {name: 'Sudan', code: 'SD'}, \n" +
      "  {name: 'Suriname', code: 'SR'}, \n" +
      "  {name: 'Svalbard and Jan Mayen', code: 'SJ'}, \n" +
      "  {name: 'Swaziland', code: 'SZ'}, \n" +
      "  {name: 'Sweden', code: 'SE'}, \n" +
      "  {name: 'Switzerland', code: 'CH'}, \n" +
      "  {name: 'Syrian Arab Republic', code: 'SY'}, \n" +
      "  {name: 'Taiwan, Province of China', code: 'TW'}, \n" +
      "  {name: 'Tajikistan', code: 'TJ'}, \n" +
      "  {name: 'Tanzania, United Republic of', code: 'TZ'}, \n" +
      "  {name: 'Thailand', code: 'TH'}, \n" +
      "  {name: 'Timor-Leste', code: 'TL'}, \n" +
      "  {name: 'Togo', code: 'TG'}, \n" +
      "  {name: 'Tokelau', code: 'TK'}, \n" +
      "  {name: 'Tonga', code: 'TO'}, \n" +
      "  {name: 'Trinidad and Tobago', code: 'TT'}, \n" +
      "  {name: 'Tunisia', code: 'TN'}, \n" +
      "  {name: 'Turkey', code: 'TR'}, \n" +
      "  {name: 'Turkmenistan', code: 'TM'}, \n" +
      "  {name: 'Turks and Caicos Islands', code: 'TC'}, \n" +
      "  {name: 'Tuvalu', code: 'TV'}, \n" +
      "  {name: 'Uganda', code: 'UG'}, \n" +
      "  {name: 'Ukraine', code: 'UA'}, \n" +
      "  {name: 'United Arab Emirates', code: 'AE'}, \n" +
      "  {name: 'United Kingdom', code: 'GB'}, \n" +
      "  {name: 'United States', code: 'US'}, \n" +
      "  {name: 'United States Minor Outlying Islands', code: 'UM'}, \n" +
      "  {name: 'Uruguay', code: 'UY'}, \n" +
      "  {name: 'Uzbekistan', code: 'UZ'}, \n" +
      "  {name: 'Vanuatu', code: 'VU'}, \n" +
      "  {name: 'Venezuela', code: 'VE'}, \n" +
      "  {name: 'Viet Nam', code: 'VN'}, \n" +
      "  {name: 'Virgin Islands, British', code: 'VG'}, \n" +
      "  {name: 'Virgin Islands, U.S.', code: 'VI'}, \n" +
      "  {name: 'Wallis and Futuna', code: 'WF'}, \n" +
      "  {name: 'Western Sahara', code: 'EH'}, \n" +
      "  {name: 'Yemen', code: 'YE'}, \n" +
      "  {name: 'Zambia', code: 'ZM'}, \n" +
      "  {name: 'Zimbabwe', code: 'ZW'} \n" +
      "]";

    // encryption key and initialization vector
    byte[] _key, _iv;

    public CryptLib() throws NoSuchAlgorithmException, NoSuchPaddingException {
        // initialize the cipher with transformation AES/CBC/PKCS5Padding
        _cx = Cipher.getInstance("AES/CBC/PKCS5Padding");
        _key = new byte[32]; //256 bit key space
        _iv = new byte[16]; //128 bit IV
    }

    /**
     * Note: This function is no longer used.
     * This function generates md5 hash of the input string
     * @param inputString
     * @return md5 hash of the input string
     */
    public static final String md5(final String inputString) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(inputString.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     *
     * @param _inputText
     *            Text to be encrypted or decrypted
     * @param _encryptionKey
     *            Encryption key to used for encryption / decryption
     * @param _mode
     *            specify the mode encryption / decryption
     * @param _initVector
     * 	      Initialization vector
     * @return encrypted or decrypted string based on the mode
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private String encryptDecrypt(String _inputText, String _encryptionKey,
                                  EncryptMode _mode, String _initVector) throws UnsupportedEncodingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        String _out = "";

        int len = _encryptionKey.getBytes("UTF-8").length; // length of the key	provided

        if (_encryptionKey.getBytes("UTF-8").length > _key.length)
            len = _key.length;

        int ivlen = _initVector.getBytes("UTF-8").length;

        if(_initVector.getBytes("UTF-8").length > _iv.length)
            ivlen = _iv.length;

        System.arraycopy(_encryptionKey.getBytes("UTF-8"), 0, _key, 0, len);
        System.arraycopy(_initVector.getBytes("UTF-8"), 0, _iv, 0, ivlen);
        //KeyGenerator _keyGen = KeyGenerator.getInstance("AES");
        //_keyGen.init(128);

        SecretKeySpec keySpec = new SecretKeySpec(_key, "AES"); // Create a new SecretKeySpec
        // for the
        // specified key
        // data and
        // algorithm
        // name.

        IvParameterSpec ivSpec = new IvParameterSpec(_iv); // Create a new
        // IvParameterSpec
        // instance with the
        // bytes from the
        // specified buffer
        // iv used as
        // initialization
        // vector.

        // encryption
        if (_mode.equals(EncryptMode.ENCRYPT)) {
            // Potentially insecure random numbers on Android 4.3 and older.
            // Read
            // https://android-developers.blogspot.com/2013/08/some-securerandom-thoughts.html
            // for more info.
            _cx.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);// Initialize this cipher instance
            byte[] results = _cx.doFinal(_inputText.getBytes("UTF-8")); // Finish
            // multi-part
            // transformation
            // (encryption)
            _out = Base64.encodeToString(results, Base64.DEFAULT); // ciphertext
            // output
        }

        // decryption
        if (_mode.equals(EncryptMode.DECRYPT)) {
            _cx.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);// Initialize this ipher instance

            byte[] decodedValue = Base64.decode(_inputText.getBytes(),
                    Base64.DEFAULT);
            byte[] decryptedVal = _cx.doFinal(decodedValue); // Finish
            // multi-part
            // transformation
            // (decryption)
            _out = new String(decryptedVal);
        }
        return _out; // return encrypted/decrypted string
    }

    /***
     * This function computes the SHA256 hash of input string
     * @param text input text whose SHA256 hash has to be computed
     * @param length length of the text to be returned
     * @return returns SHA256 hash of input text
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String SHA256 (String text, int length) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        String resultStr;
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(text.getBytes("UTF-8"));
        byte[] digest = md.digest();

        StringBuffer result = new StringBuffer();
        for (byte b : digest) {
            result.append(String.format("%02x", b)); //convert to hex
        }
        //return result.toString();

        if(length > result.toString().length())
        {
            resultStr = result.toString();
        }
        else
        {
            resultStr = result.toString().substring(0, length);
        }

        return resultStr;

    }

    /***
     * This function encrypts the plain text to cipher text using the key
     * provided. You'll have to use the same key for decryption
     *
     * @param _plainText
     *            Plain text to be encrypted
     * @param _key
     *            Encryption Key. You'll have to use the same key for decryption
     * @param _iv
     * 	    initialization Vector
     * @return returns encrypted (cipher) text
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */

    public String encrypt(String _plainText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        return encryptDecrypt(_plainText, _key, EncryptMode.ENCRYPT, _iv);
    }

    public String encryptSimple(String _plainText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException {
        return encryptDecrypt(_plainText, CryptLib.SHA256(_key, 32), EncryptMode.ENCRYPT, _iv);
    }


    /***
     * This funtion decrypts the encrypted text to plain text using the key
     * provided. You'll have to use the same key which you used during
     * encryprtion
     *
     * @param _encryptedText
     *            Encrypted/Cipher text to be decrypted
     * @param _key
     *            Encryption key which you used during encryption
     * @param _iv
     * 	    initialization Vector
     * @return encrypted value
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public String decrypt(String _encryptedText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        return encryptDecrypt(_encryptedText, _key, EncryptMode.DECRYPT, _iv);
    }

    public String decryptSimple(String _encryptedText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException {
        return encryptDecrypt(_encryptedText, CryptLib.SHA256(_key, 32), EncryptMode.DECRYPT, _iv);
    }

    /**
     * this function generates random string for given length
     * @param length
     * 				Desired length
     * * @return
     */
    public static String generateRandomIV(int length)
    {
        SecureRandom ranGen = new SecureRandom();
        byte[] aesKey = new byte[16];
        ranGen.nextBytes(aesKey);
        StringBuffer result = new StringBuffer();
        for (byte b : aesKey) {
            result.append(String.format("%02x", b)); //convert to hex
        }
        if(length> result.toString().length())
        {
            return result.toString();
        }
        else
        {
            return result.toString().substring(0, length);
        }
    }
}
