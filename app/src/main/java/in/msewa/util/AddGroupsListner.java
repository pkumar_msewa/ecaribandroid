package in.msewa.util;

import java.util.List;

import in.msewa.model.SplitMoneyGroupModel;

/**
 * Created by Ksf on 6/22/2016.
 */
public interface AddGroupsListner {
    public void addGroupCompleted(String groupName, String groupAmount, String groupType, List<SplitMoneyGroupModel> splitMoneyGroupModels);
}
