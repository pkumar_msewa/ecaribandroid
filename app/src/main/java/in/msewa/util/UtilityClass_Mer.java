//
//package in.msewa.util;
//
//import android.annotation.SuppressLint;
//import android.app.ActivityManager;
//import android.app.ActivityManager.MemoryInfo;
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.content.pm.PackageManager.NameNotFoundException;
//import android.content.res.Configuration;
//import android.net.ConnectivityManager;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.os.Build;
//import android.util.Log;
//
//import java.net.Inet4Address;
//import java.net.InetAddress;
//import java.net.NetworkInterface;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Enumeration;
//import java.util.HashMap;
//import java.util.UUID;
//
//import in.msewa.upimerchant.MerchantDetails;
//
////import com.fss.ubimersdk.R;
//
//@SuppressLint("SimpleDateFormat")
//public class UtilityClass_Mer {
//
//    public static boolean Logwrite = true;
//
//    public static boolean showDialog_Enabled = true;
//
//	// VIJAYA UAT
//    public static String PI_ENABLE_URL = "vijayaupitest.fssnet.co.in"; ///UPIService
//    public static String MERCHANT_ENABLE = "/UPIMerchantService"; //UPIMerchantService
//	public static String URL = "https://vijayaupitest.fssnet.co.in/UPIMerchantService"; // UAT
//	public static String ALABHABATH_ORGID = "400053"; // UAT
//
//	public static String BANKPARTICIPANT_NPCI_CODE = "VJB";
//	public static String ALAGHABATH_BANKID = "454545"; // UAT
//	public static String BANKNAME = "VIJAYA BANK";
//
//    public static final String MY_PREFS_NAME = "MyPrefsFile";
//
//    public static final long TEN_MINUTES = (long) (1000 * 60 * 5);
//
//    public static String CHANNEL = "Channel";
//
//    public static String MOBILENO = "MobileNo"; //
//
//    public static String BANKID = "BankId";
//
//    public static String MSGID = "MsgId";
//
//    public static String DeviceID = "DeviceID";
//
//    public static String PayerType = "PERSON"; // for Merchant Purpose
//
//    public static String PayeeType = "ENTITY"; // for Merchant Purpose
//
//    public static String ORDID = "OrgId";
//
//    public static String REMARKS = "Remarks";
//
//    public static String TIMESTAMP = "TimeStamp";
//
//    public static String USERID = "UserID";
//
//    public static String USERPWD = "UserPwd";
//
//    public static String APPVERSION = "AppVersion";
//
//    public static String PayeeCode = "0000";
//
//    public static String PayerCode = "0000";
//
//    public static String Geocode = "";
//
//    public static String SDKVERSION = "1.0.0";
//
//    public static String SDKNAME = BANKNAME;
//
//    public static String SER_LOC = "SIPCOT-Thalambur Road, Semmanjeri, India";
//
//    public static String Mer_DeviceUniqueSerialId = "";
//
//    public static String Mer_Device_Internet_IP = "";
//
//    public static String generateUUID() {
//
//        String uuid = null;
//
//        uuid = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
//
//        return uuid;
//
//    }
//
//    public UtilityClass_Mer(Context context) {
//
//
//    }
//
//    public static boolean isNetworkConnected(Context mContext) {
//
//        ConnectivityManager cm = (ConnectivityManager) mContext
//                .getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        return cm.getActiveNetworkInfo() != null;
//    }
//
//    public static String getPackageName(Context mContext) {
//
//        String packageName = mContext.getApplicationContext().getPackageName();
//
//        return packageName;
//    }
//
//    public static String getAppVersion(Context mContext) {
//
//        String appVersion = null;
//
//        PackageManager manager = mContext.getPackageManager();
//
//        PackageInfo info;
//
//        try {
//
//            info = manager.getPackageInfo(
//
//                    mContext.getPackageName(), 0);
//
//            appVersion = info.versionName;
//
//            return appVersion;
//
//        } catch (NameNotFoundException e) {
//
//            e.printStackTrace();
//        }
//        return appVersion;
//
//    }
//
//    public static String getDeviceUniqueSerialNo(Context mContext) {
//
//        String savedDeviceUniqueSerial = null, serialno = null;
//
//        // get device serial if already saved means....
//
//        SharedPreferences prefs = mContext.getApplicationContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
//
//        savedDeviceUniqueSerial = prefs.getString("DeviceUniqueSerial", null);
//
//        if (savedDeviceUniqueSerial != null && savedDeviceUniqueSerial.trim().length() > 0) {
//
//            return savedDeviceUniqueSerial;
//
//        } else {
//
//            serialno = Build.SERIAL;
//
//            if (serialno != null) {
//
//                return serialno;
//
//            } else {
//
//                return "0";
//            }
//        }
//    }
//
//    public static String getDeviceGeoCode(Context mContext) {
//
//        SharedPreferences prefs = mContext.getApplicationContext()
//
//                .getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
//
//        String geoCode = prefs.getString("GeoCode", null);
//
//        if (geoCode != null) {
//
//            if (!geoCode.equalsIgnoreCase("0.0,0.0")) {
//                return geoCode;
//            } else {
//                geoCode = "12.8355885,80.270718";
//            }
//
//        } else {
//
//            return "12.8355885,80.270718";
//        }
//
//        return "12.8355885,80.270718";
//    }
//
//
//    public static String getDevLocation(Context mContext) {
//
//        SharedPreferences prefs = mContext.getApplicationContext()
//
//                .getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
//
//        String DevLocation = prefs.getString("DevLocation", null);
//
//        if (DevLocation != null) {
//
//            DevLocation = removeJunkSpecialChars(mContext, DevLocation);
//
//            return DevLocation;
//
//        } else {
//
//            return null;
//        }
//
//    }
//
//    public static String getDevIP(Context mContext) {
//
//        String DevDefaultIP = "10.10.10.10";
//
//        WifiManager wifiMgr = (WifiManager) mContext
//                .getSystemService(mContext.WIFI_SERVICE);
//        if (wifiMgr.isWifiEnabled()) {
//            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
//            int ip = wifiInfo.getIpAddress();
//            String wifiIpAddress = String.format("%d.%d.%d.%d", (ip & 0xff),
//                    (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
//
//            if (wifiIpAddress.equalsIgnoreCase("0.0.0.0")) {
//                return DevDefaultIP;
//            } else {
//                return wifiIpAddress;
//            }
//        }
//
//        try {
//
//            for (Enumeration<NetworkInterface> en = NetworkInterface
//                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
//                NetworkInterface intf = en.nextElement();
//                for (Enumeration<InetAddress> enumIpAddr = intf
//                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
//                    InetAddress inetAddress = enumIpAddr.nextElement();
//                    // the condition after && is missing in your snippet,
//                    // checking instance of inetAddress
//                    if (!inetAddress.isLoopbackAddress()
//                            && inetAddress instanceof Inet4Address) {
//
//                        if (inetAddress.getHostAddress().equalsIgnoreCase("0.0.0.0")) {
//                            return DevDefaultIP;
//                        } else {
//                            return inetAddress.getHostAddress();
//                        }
//
//                    }
//
//                }
//            }
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//            return DevDefaultIP;
//        }
//        return DevDefaultIP;
//    }
//
//    public static String getDevCapability(Context mContext) {
//
//        String DevCapability = "Not Yet Decide";
//
//        ActivityManager actManager = (ActivityManager) mContext
//                .getSystemService(Context.ACTIVITY_SERVICE);
//        MemoryInfo memInfo = new MemoryInfo();
//        actManager.getMemoryInfo(memInfo);
//        long totalMemory = 1000l;
//
//        return Long.toString(totalMemory);
//
//    }
//
//    public static String getTodayDate_Format(Context mContext) {
//
//        try {
//
//            int year;
//
//            int month;
//
//            int day;
//
//            String currentDate;
//
//            final Calendar c = Calendar.getInstance();
//
//            year = c.get(Calendar.YEAR);
//
//            month = c.get(Calendar.MONTH) + 1;
//
//            day = c.get(Calendar.DAY_OF_MONTH);
//
//            if (day < 10) {
//
//                currentDate = "0" + day + "-"
//                        + (month < 10 ? ("0" + month) : (month)) + "-" + year;
//            } else {
//
//                currentDate = day + "-"
//                        + (month < 10 ? ("0" + month) : (month)) + "-" + year;
//            }
//
//            return currentDate;
//
//        } catch (Exception e) {
//
//            return null;
//        }
//
//    }
//
//    public static long getCurrentTimestamp(Context mContext) {
//
//        Date date = new Date();
//
//        date.getTime();
//
//        long timeDate = date.getTime();
//
//        return timeDate;
//    }
//
//    public static String getTimeStampFormat(Context mContext) {
//
//        Long tsLong = System.currentTimeMillis() / 1000;
//
//        String ts = tsLong.toString();
//
//        return ts;
//    }
//
//    public static String retriveSavedTimeStamp(Context mContext) {
//
//        SharedPreferences prefs = mContext.getApplicationContext()
//                .getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
//
//        String previousTimeStamp = prefs.getString("CurrentTimestamp", null);
//
//        return previousTimeStamp;
//
//    }
//    // ----------------------------------------------------------------------------------------------------------
//
//    public static HashMap<String, String> storeDefaultValues(Context mContext) {
//
//        HashMap<String, String> storeHeaderDatas = new HashMap<String, String>();
//
//        storeHeaderDatas.put(UtilityClass_Mer.CHANNEL, "07");
//        storeHeaderDatas.put(UtilityClass_Mer.BANKID,
//                UtilityClass_Mer.ALAGHABATH_BANKID);
//
//        storeHeaderDatas.put(UtilityClass_Mer.MOBILENO, "");
//        storeHeaderDatas.put(UtilityClass_Mer.MSGID, UtilityClass_Mer.BANKPARTICIPANT_NPCI_CODE + UtilityClass_Mer.generateUUID());
//        storeHeaderDatas.put(UtilityClass_Mer.ORDID, UtilityClass_Mer.ALABHABATH_ORGID);
//        storeHeaderDatas.put("PayerCode", MerchantDetails.PayerCode); // for Merchant Purpose
//        storeHeaderDatas.put("PayeeCode", MerchantDetails.PayerCode); // for Merchant Purpose
//        storeHeaderDatas.put("PayerType", UtilityClass_Mer.PayerType); // for Merchant Purpose
//        storeHeaderDatas.put("PayeeType", UtilityClass_Mer.PayeeType); // for Merchant Purpose
//        storeHeaderDatas.put(UtilityClass_Mer.REMARKS, "");
//        storeHeaderDatas.put(UtilityClass_Mer.TIMESTAMP, UtilityClass_Mer.getTimeStampFormat(mContext));
//        storeHeaderDatas.put(UtilityClass_Mer.USERID, "UserID");
//        storeHeaderDatas.put(UtilityClass_Mer.USERPWD, "111111");
//
//        // check with WebServices and enable this New Param
//        storeHeaderDatas.put(UtilityClass_Mer.APPVERSION, UtilityClass_Mer.getAppVersion(mContext));
//
//        //Merchant Additional Details
//        storeHeaderDatas.put("MerchantID", MerchantDetails.Mer_MerchantId);
//        storeHeaderDatas.put("TerminalID", MerchantDetails.Mer_TerminalId);
//        storeHeaderDatas.put("SubMerchantID", MerchantDetails.Mer_SubMerchantID);
//        storeHeaderDatas.put("SdkVersion", UtilityClass_Mer.SDKVERSION);
//        storeHeaderDatas.put("SdkName", UtilityClass_Mer.SDKNAME);
//        storeHeaderDatas.put("RefId", MerchantDetails.Mer_Txn_Ref_Id);
//        storeHeaderDatas.put("CurrencyType", MerchantDetails.Mer_Txn_Currency);
//        storeHeaderDatas.put(UtilityClass_Mer.DeviceID, MerchantDetails.Mer_DeviceUniqueSerialId);
//
//        return storeHeaderDatas;
//    }
//
//    public static void LogPrinter(char c, String Title, String msg) {
//        if (Logwrite) {
//            if (c == 'v')
//                Log.v(Title, msg);
//            else if (c == 'd')
//                Log.d(Title, msg);
//            else if (c == 'i' || c == 'I')
//                Log.i(Title, msg);
//            else if (c == 'w')
//                Log.w(Title, msg);
//            else if (c == 'e')
//                Log.e(Title, msg);
//        }
//    }
//
//    public static String removeJunkSpecialChars(Context mContext, String message) {
//
//        String formattedString = "";
//
//        String str = message;
//
//        for (Character c : str.toCharArray()) {
//
//            // if (c > 127) //character is invalid
//
//            if (!c.toString().matches("\\A\\p{ASCII}*\\z")) {
//
//                Log.v("upi", "Single Character " + c + " is invalid");
//
//            } else {
//
//                Log.v("upi", "Single Character " + c + " is valid");
//
//                formattedString += c;
//            }
//        }
//
//        formattedString = formattedString.replaceAll("[^\\w,.-]+", "");
//
//        return formattedString;
//    }
//
//    public static boolean isTablet(Context mContext) {
//        return (mContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
//    }
//}
