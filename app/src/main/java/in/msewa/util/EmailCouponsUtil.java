package in.msewa.util;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;

/**
 * Created by Ksf on 6/10/2016.
 */
public class EmailCouponsUtil {

  private static UserModel session = UserModel.getInstance();


  public static void emailForCoupons(String tag_json_obj) {
    StringRequest jsonObjReq = new StringRequest(Request.Method.GET, ApiUrl.URL_EMAIL_COUPONS + session.getUserEmail(),
      new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
          Log.i("Email Successful", "Yeh!! Email Successful");
        }
      }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.i("Error Response", "Email not Successful");
      }
    }) {


    };
    PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

  }


}
