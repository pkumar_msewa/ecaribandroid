package in.msewa.util;


import java.util.ArrayList;

import in.msewa.model.MobilePlansModel;


public class MobilePlansCheck {
	
	private volatile static MobilePlansCheck uniqueInstance;
	
	private ArrayList<MobilePlansModel> talkTimePlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> topUpPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> threeGPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> twoGPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> specialPlans = new ArrayList<>();
	
	/*
	 * Singleton
	 */
	private MobilePlansCheck() {}
	
	/*
	 * Singleton getInstance()
	 */
	public static MobilePlansCheck getInstance() {
		if (uniqueInstance == null) {
			synchronized (MobilePlansCheck.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new MobilePlansCheck();
				}
			}
		}
		return uniqueInstance;
	}

	public ArrayList<MobilePlansModel> getTalkTimePlans() {
		return talkTimePlans;
	}

	public void setTalkTimePlans(ArrayList<MobilePlansModel> talkTimePlans) {
		this.talkTimePlans = talkTimePlans;
	}

	public ArrayList<MobilePlansModel> getTopUpPlans() {
		return topUpPlans;
	}

	public void setTopUpPlans(ArrayList<MobilePlansModel> topUpPlans) {
		this.topUpPlans = topUpPlans;
	}

	public ArrayList<MobilePlansModel> getThreeGPlans() {
		return threeGPlans;
	}

	public void setThreeGPlans(ArrayList<MobilePlansModel> threeGPlans) {
		this.threeGPlans = threeGPlans;
	}

	public ArrayList<MobilePlansModel> getTwoGPlans() {
		return twoGPlans;
	}

	public void setTwoGPlans(ArrayList<MobilePlansModel> twoGPlans) {
		this.twoGPlans = twoGPlans;
	}

	public ArrayList<MobilePlansModel> getSpecialPlans() {
		return specialPlans;
	}

	public void setSpecialPlans(ArrayList<MobilePlansModel> specialPlans) {
		this.specialPlans = specialPlans;
	}
	
}
