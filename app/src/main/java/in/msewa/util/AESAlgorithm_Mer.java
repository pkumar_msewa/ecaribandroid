//package in.msewa.util;
//
///**
// * Created by harikrishnanv on 12-01-2017.
// */
//
//
//
//import javax.crypto.Cipher;
//import javax.crypto.spec.IvParameterSpec;
//import javax.crypto.spec.SecretKeySpec;
//
//import Decoder.BASE64Decoder;
//import Decoder.BASE64Encoder;
//
//public class AESAlgorithm_Mer {
//
//    public static String encryptTextusingAES(String text, String kek) throws Exception {
//        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        byte[] keyBytes = new byte[16];
//        byte[] b = kek.getBytes("UTF-8");
//        int len = b.length;
//        if (len > keyBytes.length) len = keyBytes.length;
//        System.arraycopy(b, 0, keyBytes, 0, len);
//        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
//        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
//        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
//        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
//        BASE64Encoder encoder = new BASE64Encoder();
//        return encoder.encode(results);
//    }
//
//
//    public static String decryptTextusingAES(String text, String kek) throws Exception {
//
//        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        byte[] keyBytes = new byte[16];
//        byte[] b = kek.getBytes("UTF-8");
//        int len = b.length;
//        if (len > keyBytes.length) len = keyBytes.length;
//        System.arraycopy(b, 0, keyBytes, 0, len);
//        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
//        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
//        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
//
//        BASE64Decoder decoder = new BASE64Decoder();
//        UtilityClass_Mer.LogPrinter('I', "UPI", "text::::" + text);
//        byte[] results = cipher.doFinal(decoder.decodeBuffer(text));
//        return new String(results, "UTF-8");
//    }
//}
