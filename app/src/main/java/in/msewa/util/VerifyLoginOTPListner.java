package in.msewa.util;

/**
 * Created by Kashif-PC on 1/21/2017.
 */
public interface VerifyLoginOTPListner {

    void onVerifySuccess();
    void onVerifyError();
}
