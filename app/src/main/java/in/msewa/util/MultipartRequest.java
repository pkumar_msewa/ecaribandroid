package in.msewa.util;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import in.msewa.model.UserModel;

/**
 * Created by Ksf on 4/13/2016.
 */
public class MultipartRequest extends Request<String> {

    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpentity;
    private String FILE_PART_NAME = "profilePicture";

    private UserModel userModel = UserModel.getInstance();

    private final Response.Listener<String> mListener;
    private final File mFilePart;
    private Map<String, String> headerParams;

    public MultipartRequest(String url, Response.ErrorListener errorListener,
                            Response.Listener<String> listener, File file,
                            final Map<String, String> headerParams, String partName) {

        super(Method.POST, url, errorListener);
        this.mListener = listener;
        this.mFilePart = file;
        this.headerParams = headerParams;
        this.FILE_PART_NAME = partName;

        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        buildMultipartEntity();
        httpentity = entity.build();
    }


    private void buildMultipartEntity() {
        if (mFilePart.exists()) {

        }

        ContentType contentType = ContentType.create("image/jpeg");
        entity.addPart(FILE_PART_NAME, new FileBody(mFilePart, contentType, mFilePart.getName()));
        entity.addTextBody("sessionId", userModel.getUserSessionId());
    }

//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//        return (headerParams != null) ? headerParams : super.getHeaders();
//    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
//        String s = createPostBody(headerParams);
//        return s.getBytes();
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        try {
            return Response.success(new String(response.data, "UTF-8"),
                    getCacheEntry());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.success(new String(response.data), getCacheEntry());
        }
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }

    private String createPostBody(Map<String, String> params) {
        StringBuilder sbPost = new StringBuilder();
        for (String key : params.keySet()) {
            if (params.get(key) != null) {
                sbPost.append("\r\n" + "--" + "BOUNDARY" + "\r\n");
                sbPost.append("Content-Disposition: form-data; name=\"" + key + "\"" + "\r\n\r\n");
                sbPost.append(params.get(key));
            }
        }

        return sbPost.toString();
    }

}
