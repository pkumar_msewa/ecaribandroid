package in.msewa.util;


import org.json.JSONArray;

import java.util.ArrayList;

import in.msewa.model.TravelKhanaMenuModel;

public interface AddRemoveMenuListner {
	void taskCompleted(ArrayList<TravelKhanaMenuModel> menuArray);
	void AddRemoveComplete(double totapPrice, JSONArray items);
}
