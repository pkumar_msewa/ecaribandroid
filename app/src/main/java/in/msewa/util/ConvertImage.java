package in.msewa.util;

/**
 * Created by user on 7/29/2015.
 */

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.View;

public class ConvertImage {
    // Convert Text to image Method
    // pass the paramter : text , size , stroke ,color,typeface
    public Bitmap textAsBitmap(String text, float textSize, float stroke, int color, Typeface typeface) {

        TextPaint paint = new TextPaint();
        paint.setColor(color);
        paint.setTextSize(textSize);
        paint.setStrokeWidth(stroke);
        paint.setTypeface(typeface);

        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.LEFT);

        float baseline = (int) (-paint.ascent() + 3f); // ascent() is negative

        StaticLayout staticLayout = new StaticLayout(text, 0, text.length(),
                paint, 435, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0f,
                1.0f, false);

        int linecount = staticLayout.getLineCount();

        int height = (int) (baseline + paint.descent() + 3) * linecount + 10;

        Bitmap image = Bitmap.createBitmap(435, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawARGB(0xFF, 0xFF, 0xFF, 0xFF);

        staticLayout.draw(canvas);

        return image;

    }



    public Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }
}
