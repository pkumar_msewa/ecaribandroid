package in.msewa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;

import java.util.List;

import in.msewa.custom.LoadingDialog;
import in.msewa.model.GiftCardItemModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.giftcardactivity.GciOrderActivity;

/**
 * Created by Kashif-PC on 2/21/2017.
 */
public class GiftCardItemAdapter extends RecyclerView.Adapter<GiftCardItemAdapter.RecyclerViewHolders> {

    private List<GiftCardItemModel> itemList;
    private Context context;
    private String imagePath, itemHash, brandName;

    private LoadingDialog cartLoadindDialog;
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_events";
    private UserModel session = UserModel.getInstance();
    public Button btnGiftCardBuy;
    int cardQty = 1;
    double cardPrice = 0;

    public GiftCardItemAdapter(List<GiftCardItemModel> itemList, Context context, String imagePath, String itemHash, String brandName) {
        this.itemList = itemList;
        this.context = context;
        this.imagePath = imagePath;
        this.itemHash = itemHash;
        this.brandName = brandName;

    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gift_card_item, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {

        holder.tvGiftCardAmount.setText("\u20B9 " + Integer.toString(itemList.get(position).getName()));

        AQuery aq = new AQuery(context);
        if (imagePath != null && !imagePath.isEmpty()) {
            aq.id(holder.ivGiftCardItem).image(imagePath, true, true);
        } else {
            aq.id(holder.ivGiftCardItem).background(R.drawable.loading_image);
        }

        holder.btnGiftCardBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cardPrice == 0) {
                    cardPrice = Double.parseDouble(Integer.toString(itemList.get(position).getName()));
                }
                Intent gciOrderIntent = new Intent(context, GciOrderActivity.class);
                gciOrderIntent.putExtra("Hash", itemHash);
                gciOrderIntent.putExtra("Price", cardPrice);
                gciOrderIntent.putExtra("Quantity", cardQty);
                context.startActivity(gciOrderIntent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        public ImageView ivGiftCardItem;

        public TextView tvGiftCardAmount;
        public Button btnGiftCardBuy;


        public RecyclerViewHolders(View itemView) {
            super(itemView);
            ivGiftCardItem = (ImageView) itemView.findViewById(R.id.ivGiftCardItem);
            tvGiftCardAmount = (TextView) itemView.findViewById(R.id.tvGiftCardItemAmount);
            btnGiftCardBuy = (Button) itemView.findViewById(R.id.btnGiftCardBuy);
//            tvGiftCartQty = (TextView) itemView.findViewById(R.id.tvGiftCartQtyn);


        }


    }

}
