package in.msewa.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.PQCart;
import in.msewa.model.ShoppingModel;
import in.msewa.model.TravelKhanaMenuModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveMenuListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 4/6/2016.
 */
public class MenuCartAdapter extends BaseAdapter {
  private Context context;
  private PQCart pqCart = PQCart.getInstance();
  private RequestQueue rq;
  private UserModel session = UserModel.getInstance();
  private JsonObjectRequest postReq;
  private String tag_json_obj = "json_events";
  boolean addresult, removeresult = false;
  private ArrayList<TravelKhanaMenuModel> cartMenuArray;
  long ticketQty = 1;
  long totalPrice = 0;

  LoadingDialog loadingDialog;

  private AddRemoveMenuListner addRemoveMenuListner;
  private String outletId;

  public MenuCartAdapter(Context context, ArrayList<TravelKhanaMenuModel> cartMenuArray, AddRemoveMenuListner addRemoveMenuListner, String outletId) {
    this.context = context;
    this.cartMenuArray = cartMenuArray;
    this.addRemoveMenuListner = addRemoveMenuListner;
    this.outletId = outletId;
    loadingDialog = new LoadingDialog(context);
    try {
      rq = Volley.newRequestQueue(context);
    } catch (Exception e) {

    }
  }

  @Override
  public int getCount() {
    return cartMenuArray.size();
  }

  @Override
  public Object getItem(int position) {
    return cartMenuArray.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(final int position, View convertView, final ViewGroup parentView) {
    final int currentPosition = position;
//        final ShoppingModel product = cart.getProductsInCartArray().get(currentPosition);
    final ViewHolder viewHolder;
    if (convertView == null) {
      LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
      convertView = mInflater.inflate(R.layout.adapter_cart_menu, null);
      viewHolder = new ViewHolder();
      viewHolder.ivProductImage = (ImageView) convertView.findViewById(R.id.ivInCartProduct);
      viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvInCartTitle);
      viewHolder.tvPrice = (TextView) convertView.findViewById(R.id.tvInCartRupees);
//      viewHolder.tvPriceOld = (TextView) convertView.findViewById(R.id.tvInCartRupeesOld);
      viewHolder.lLInCartBackGround = (LinearLayout) convertView.findViewById(R.id.lLIncartProduct);
      viewHolder.btnRemoveFromCart = (LinearLayout) convertView.findViewById(R.id.btnInCartRemove);
      viewHolder.tvProductQty = (TextView) convertView.findViewById(R.id.tvProductQty);
      viewHolder.btnAddItem = (Button) convertView.findViewById(R.id.btnAddItem);
      viewHolder.btnRemoveItem = (Button) convertView.findViewById(R.id.btnRemoveItem);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }

    viewHolder.tvTitle.setText(cartMenuArray.get(position).getItemName());
    viewHolder.tvPrice.setText("\u20B9 " + cartMenuArray.get(position).getCustomerPayable());
    viewHolder.tvProductQty.setText(String.valueOf(cartMenuArray.get(position).getQuantity()));

    if (cartMenuArray.get(position).getImage() != null && cartMenuArray.get(position).getImage().length() != 0) {
      AQuery aQuery = new AQuery(context);
      URL sourceUrl = null;
      try {
        sourceUrl = new URL(cartMenuArray.get(position).getImage().replaceAll(" ", "%20"));
        aQuery.id(viewHolder.ivProductImage).image(sourceUrl.toString()).background(R.drawable.loading_image).getContext();
      } catch (MalformedURLException e) {
        e.printStackTrace();
      }
//            Picasso.with(context).load(product.getpImage())
//                    .resize(100, 100).placeholder(R.drawable.loading_image)
//                    .into(viewHolder.ivProductImage);
//
    }
    viewHolder.btnRemoveFromCart.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View arg0) {
//                final ShoppingModel product = cart.getProductsInCartArray().get(currentPosition);
//                removeProduct(product, "removeall");

      }

    });
//        if(pqCart.isProductInCart(product)){
//            long qty = Long.parseLong(viewHolder.tvProductQty.getText().toString());
//            ticketQty = qty;
//            Log.i("NUMBER", "qty : " + qty);
//            qty++;
//            long price = product.getpPrice();
//            price = price * qty;
//            viewHolder.tvProductQty.setText(String.valueOf(price));
//            viewHolder.tvPrice.setText("\u20B9 " +" "+ String.valueOf(price));
//            totalPrice =  price;
//            ticketQty = qty;
//        }

    viewHolder.btnAddItem.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
//                final ShoppingModel product = cart.getProductsInCartArray().get(currentPosition);
//                addProduct(product);
//                Log.i("productid", String.valueOf(product.getpQty()));
//                if(addProduct(product)){
//                Log.i("Postionm", String.valueOf(currentPosition));
//                    long qty = Long.parseLong(viewHolder.tvProductQty.getText().toString());
//                    ticketQty = qty;
//                    Log.i("NUMBER", "qty : " + qty);
//                    qty++;
//                    long price = product.getpPrice();
//                    price = price * qty;
//                    viewHolder.tvProductQty.setText(String.valueOf(qty));
//                    viewHolder.tvPrice.setText("\u20B9 " +" "+ String.valueOf(price));
//                    totalPrice =  price;
//                    ticketQty = qty;
//                }
        cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() + 1);
        addRemoveProduct(currentPosition, viewHolder.tvProductQty, "add");
      }
    });

    viewHolder.btnRemoveItem.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (cartMenuArray.get(currentPosition).getQuantity() != 1) {
          cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() - 1);
          addRemoveProduct(currentPosition, viewHolder.tvProductQty, "remove");
        }
      }
    });
    return convertView;
  }

  public JSONArray getMenuArray() {
    JSONArray menuArray = new JSONArray();
    for (int i = 0; i < cartMenuArray.size(); i++) {
      JSONObject object = new JSONObject();
      try {
        object.put("quantity", (long) cartMenuArray.get(i).getQuantity());
        object.put("itemId", String.valueOf(cartMenuArray.get(i).getItemId()));
        menuArray.put(object);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
    return menuArray;
  }

  public boolean addRemoveProduct(final int position, final TextView currentView, final String action) {
    loadingDialog.show();
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("order_outlet_id", outletId);
      jsonRequest.put("orderMenu", getMenuArray());
    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("PRICECALREQ", jsonRequest.toString());
      Log.i("PRICECALURL", ApiUrl.URL_TRAVELKHANA_MENU_PRICE_CAL);
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_MENU_PRICE_CAL, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("PRICECALRES", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              JSONObject menuPriceCalDTO = response.getJSONObject("menuPriceCalDTO");
              JSONObject userOrderInfo = menuPriceCalDTO.getJSONObject("userOrderInfo");
              double totalCustomerPayable = userOrderInfo.getDouble("totalCustomerPayable");
              addRemoveMenuListner.AddRemoveComplete(totalCustomerPayable, getMenuArray());
              currentView.setText(String.valueOf(cartMenuArray.get(position).getQuantity()));
              loadingDialog.dismiss();
              notifyDataSetChanged();
              addresult = true;
            } else {
              if (action.equals("add")) {
                cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() - 1);
                Log.i("POSLOG", String.valueOf(cartMenuArray.get(position).getQuantity()));
              } else {
                cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() + 1);
                Log.i("NEGLOG", String.valueOf(cartMenuArray.get(position).getQuantity()));
              }
              notifyDataSetChanged();
              loadingDialog.dismiss();
              CustomToast.showMessage(context, "oops, something went wrong. Please try again after sometime");
            }

          } catch (JSONException e) {
            if (action.equals("add")) {
              cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() - 1);
              Log.i("POSLOG", String.valueOf(cartMenuArray.get(position).getQuantity()));
            } else {
              cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() + 1);
              Log.i("NEGLOG", String.valueOf(cartMenuArray.get(position).getQuantity()));
            }
            notifyDataSetChanged();
            loadingDialog.dismiss();
            e.printStackTrace();
            Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          if (action.equals("add")) {
            cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() - 1);
            Log.i("POSLOG", String.valueOf(cartMenuArray.get(position).getQuantity()));
          } else {
            cartMenuArray.get(position).setQuantity(cartMenuArray.get(position).getQuantity() + 1);
            Log.i("NEGLOG", String.valueOf(cartMenuArray.get(position).getQuantity()));
          }
          notifyDataSetChanged();
          loadingDialog.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;

      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
      return addresult;
    }
    return addresult;
  }

  public boolean removeProduct(final ShoppingModel shopingModel, final String urlShoppongRemoveItemCart) {
    loadingDialog.show();
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobile", session.getUserMobileNo());
      jsonRequest.put("productId", shopingModel.getPid());
      if (urlShoppongRemoveItemCart.equals("removeall")) {
        jsonRequest.put("quentity", "all");
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    if (jsonRequest != null) {
      Log.i("CARTREMOVEONEURL", ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART);
      Log.i("CARTREMOVEONEREQ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            loadingDialog.dismiss();
            Log.i("CARTREMOVEONERES", jsonObj.toString());
            String code = jsonObj.getString("code");
            if (code != null && code.equals("S00")) {
              if (urlShoppongRemoveItemCart.equals("removeall")) {
                pqCart.addRemoveProductsInCart(shopingModel, shopingModel.getpQty());
//                                inCartListner.taskCompleted();
                loadingDialog.dismiss();
                notifyDataSetChanged();
              } else {
                shopingModel.setpQty(shopingModel.getpQty() - 1);
                shopingModel.save();

//                            pqCart.addRemoveProductsInCart(shopingModel, shopingModel.getpQty());
                loadingDialog.dismiss();
//                                inCartListner.taskCompleted();
                notifyDataSetChanged();
                removeresult = true;
              }

              if (pqCart.getProductsInCartArray().isEmpty()) {
//                                inCartListner.closeCart();
              }
            }
          } catch (JSONException e) {
            loadingDialog.dismiss();
            e.printStackTrace();
            Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();

          CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map getHeaders() throws AuthFailureError {
          HashMap map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }
      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
    return removeresult;
  }

  static class ViewHolder {
    ImageView ivProductImage;
    LinearLayout btnRemoveFromCart, lLInCartBackGround;
    TextView tvTitle;
    TextView tvPrice, tvPriceOld, tvProductQty;
    Button btnRemoveItem, btnAddItem;
  }

}
