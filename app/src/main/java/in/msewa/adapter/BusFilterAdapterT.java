package in.msewa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.msewa.model.BusCityModel;
import in.msewa.model.TravelCityModel;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 10/6/2016.
 */
public class BusFilterAdapterT extends BaseAdapter implements Filterable {

  private Context context;
  private List<TravelCityModel> flightCityArray;
  private List<TravelCityModel> filteredData;
  private ViewHolder viewHolder;

  public BusFilterAdapterT(Context context, List<TravelCityModel> flightCityArray) {
    this.context = context;
    this.flightCityArray = flightCityArray;
    this.filteredData = flightCityArray;
  }

  @Override
  public int getCount() {
    if (flightCityArray == null) {
      return 0;
    }
    return flightCityArray.size();
  }

  @Override
  public Object getItem(int index) {
    return flightCityArray.get(index);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      convertView = LayoutInflater.from(context).inflate(R.layout.adapter_train_city_list, parent, false);
      viewHolder = new ViewHolder();
      viewHolder.tvFlightCity = (TextView) convertView.findViewById(R.id.tvFlightCity);
      viewHolder.tvFlightCode = (TextView) convertView.findViewById(R.id.tvFlightCode);
      viewHolder.tvFlightCode.setVisibility(View.GONE);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }
    if (flightCityArray.get(position).getCityId().length()== 0) {
      viewHolder.tvFlightCity.setText(flightCityArray.get(position).getCityname());
    } else {
      viewHolder.tvFlightCity.setText(flightCityArray.get(position).getCityname() + "      -" + flightCityArray.get(position).getCityId());
    }

    return convertView;
  }

  @Override
  public Filter getFilter() {
    return new ItemFilter();
  }


  static class ViewHolder {
    TextView tvFlightCity;
    TextView tvFlightCode;
  }


  private class ItemFilter extends Filter {
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

      String filterString = constraint.toString().toLowerCase();

      FilterResults results = new FilterResults();

      final List<TravelCityModel> list = filteredData;

      int count = list.size();
      final ArrayList<Object> nlist = new ArrayList<Object>(count);

      String filterableString, filterableStringcode, filterableStringAreo;

      for (int i = 0; i < count; i++) {
        filterableString = list.get(i).getCityname();
        filterableStringcode = String.valueOf(list.get(i).getCityId());

        if (filterableString.toLowerCase().contains(filterString) || filterableStringcode.contains(filterString)) {
          nlist.add(list.get(i));
        }
      }
      results.values = nlist;
      results.count = nlist.size();
      return results;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
      flightCityArray = (ArrayList<TravelCityModel>) results.values;
      notifyDataSetChanged();
    }

  }
}

