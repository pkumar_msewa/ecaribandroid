package in.msewa.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import in.msewa.custom.TextDrawable;
import in.msewa.model.ImagicaPackageModel;
import in.msewa.ecarib.R;

/**
 * Created by acer on 06-07-2017.
 */


public class ImagicaAdapter extends RecyclerView.Adapter<ImagicaAdapter.ViewHolder> {

    ArrayList<ImagicaPackageModel> SubjectValues;
    static Context context;
    View view1;
    ViewHolder viewHolder1;
    ImageButton btndowns;
    JSONObject jsonObject;
    private HashMap<String, String> totalAmount;
    TextView textView;
    HashMap<String, String> stringStringHashMap;

    public ImagicaAdapter(Context context1, ArrayList<ImagicaPackageModel> SubjectValues1) {
        SubjectValues = SubjectValues1;
        context = context1;
        CardView cardview;
        this.jsonObject = new JSONObject();
        this.stringStringHashMap = new HashMap<>();
        this.totalAmount = new HashMap<>();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView pDescription, pTitle, PcostPer, textView3, tvtotalcost, tvquantityn;
        CardView cardView1, cardView2;
        private ImageView ivimage;
        private ImageButton ibbtndowns, btnup, btndown, btnups;
        int i = 0;


        public ViewHolder(View v) {
            super(v);

            pTitle = (TextView) v.findViewById(R.id.tvITitle);
            pDescription = (TextView) v.findViewById(R.id.tvDescription);
            ivimage = (ImageView) v.findViewById(R.id.ivimage);
            PcostPer = (TextView) v.findViewById(R.id.tvpercost);
            cardView1 = (CardView) v.findViewById(R.id.cvamt);
            cardView2 = (CardView) v.findViewById(R.id.cvpacktick);
            ibbtndowns = (ImageButton) v.findViewById(R.id.btndowns);
            btnup = (ImageButton) v.findViewById(R.id.btnup);
            btndown = (ImageButton) v.findViewById(R.id.btndown);
            btnups = (ImageButton) v.findViewById(R.id.btnups);
            tvtotalcost = (TextView) v.findViewById(R.id.tvtotalcost);
            tvquantityn = (TextView) v.findViewById(R.id.tvquantityn);

            TextDrawable plus = new TextDrawable(context, "+", ColorStateList.valueOf(context.getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
            TextDrawable mins = new TextDrawable(context, "-", ColorStateList.valueOf(context.getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
            btndown.setImageDrawable(plus);
            btnups.setImageDrawable(mins);
            cardView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardView1.getVisibility() == View.GONE) {
                        cardView1.setVisibility(View.VISIBLE);
                        btnup.setVisibility(View.GONE);
                        ibbtndowns.setVisibility(View.VISIBLE);
                    } else {
                        cardView1.setVisibility(View.GONE);
                        btnup.setVisibility(View.VISIBLE);
                        ibbtndowns.setVisibility(View.GONE);
                    }
                }
            });


        }
    }

    @Override
    public ImagicaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.imagica_adapter, parent, false);
        viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.pTitle.setText(SubjectValues.get(position).getPtitle());
        holder.pDescription.setText(Html.fromHtml(SubjectValues.get(position).getPdescription()));
        Picasso.with(context).load(SubjectValues.get(position).getPimageURL()).into(holder.ivimage);
        stringStringHashMap.put(String.valueOf(SubjectValues.get(position).getPproductID()), String.valueOf(SubjectValues.get(position).getPproductID()));
        holder.PcostPer.setText(String.valueOf(SubjectValues.get(position).getPcostPer()));
        if (totalAmount.containsKey(String.valueOf(position))) {
            totalAmount.remove(String.valueOf(position));
            totalAmount.put(String.valueOf(position), holder.PcostPer.getText().toString() + "," + holder.tvquantityn.getText().toString());
        } else {
            totalAmount.put(String.valueOf(position), holder.PcostPer.getText().toString() + "," + holder.tvquantityn.getText().toString());
        }
        // holder.textView2.setText((int) SubjectValues.get(position).getPcostPer());
        holder.btndown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int i = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
                String val = String.valueOf(i);
                holder.tvquantityn.setText(val);
                double price = Double.parseDouble(holder.PcostPer.getText().toString());
                holder.tvtotalcost.setText(String.valueOf(price * Double.parseDouble(holder.tvquantityn.getText().toString())));

                try {
                    if (jsonObject.has(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())))) {
                        jsonObject.remove(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())));
                        jsonObject.put(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())), holder.tvquantityn.getText().toString());
                    } else {
                        jsonObject.put(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())), holder.tvquantityn.getText().toString());
                    }

                    if (totalAmount.containsKey(String.valueOf(position))) {
                        totalAmount.remove(String.valueOf(position));
                        totalAmount.put(String.valueOf(position), holder.PcostPer.getText().toString() + "," + holder.tvquantityn.getText().toString());
                    }
                    sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        holder.btnups.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!(holder.tvquantityn == null) && !(holder.tvquantityn.getText().equals("0"))) {
                    int i = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
                    String val = String.valueOf(i);
                    holder.tvquantityn.setText(val);
                    double price = Double.parseDouble(holder.PcostPer.getText().toString());
                    holder.tvtotalcost.setText(String.valueOf(price * Double.parseDouble(holder.tvquantityn.getText().toString())));
                    try {
                        if (jsonObject.has(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())))) {
                            jsonObject.remove(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())));
                            jsonObject.put(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())), holder.tvquantityn.getText().toString());
                        } else {
                            jsonObject.put(stringStringHashMap.get(String.valueOf(SubjectValues.get(position).getPproductID())), holder.tvquantityn.getText().toString());
                        }
                        if (totalAmount.containsKey(String.valueOf(position))) {
                            totalAmount.remove(String.valueOf(position));
                            totalAmount.put(String.valueOf(position), holder.PcostPer.getText().toString() + "," + holder.tvquantityn.getText().toString());
                        }
                        if (holder.tvquantityn.getText().equals("0")) {
                            jsonObject = new JSONObject();
                            sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                        } else {
                            sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        Log.i("Size", String.valueOf(SubjectValues.size()));
        return SubjectValues.size();
    }

    private static void sendRefresh(String packa, JSONObject jsonObject, HashMap<String, String> totalAmount) {
        ArrayList<String> strings = new ArrayList<>();
        for (Object o : totalAmount.keySet()) {
            String[] value = totalAmount.get(o.toString()).split(",");
            double value2 = Double.parseDouble(value[0]) * Double.parseDouble(value[1]);
            strings.add(String.valueOf(value2));
        }
        JSONObject products = new JSONObject();
        Iterator<String> iter = jsonObject.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                Object value = jsonObject.get(key);
                if (!value.equals("0")) {
                    products.put(key, value);
                }
            } catch (JSONException e) {

            }
        }
        if (strings.size() != 0) {
            Intent intent = new Intent("Packages");
            intent.putExtra("updates", "1");
            intent.putExtra("type", "package");
            intent.putExtra("amount", String.valueOf(totalValue(strings)));
            intent.putExtra("packageProduct", products.toString());
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }


    }

    public static double totalValue(ArrayList<String> strings) {
        double sum = 0;
        for (int i = 0; i < strings.size(); i++) {
            sum += Double.parseDouble(strings.get(i));
        }
        return sum;
    }
}
