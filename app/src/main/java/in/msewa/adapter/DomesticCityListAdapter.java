package in.msewa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.msewa.model.DomesticFlightModel;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 10/6/2016.
 */
public class DomesticCityListAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<DomesticFlightModel> flightCityArray;
    private List<DomesticFlightModel> filteredData;
    private ViewHolder viewHolder;

    public DomesticCityListAdapter(Context context, List<DomesticFlightModel> flightCityArray) {
        this.context = context;
        this.flightCityArray = flightCityArray;
        this.filteredData = flightCityArray;
    }

    @Override
    public int getCount() {
        return flightCityArray.size();
    }

    @Override
    public Object getItem(int index) {
        return flightCityArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_flight_city_list, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvFlightCity = (TextView) convertView.findViewById(R.id.tvFlightCity);
            viewHolder.tvFlightCode = (TextView) convertView.findViewById(R.id.tvFlightCode);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvFlightCode.setVisibility(View.VISIBLE);
        viewHolder.tvFlightCity.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
        viewHolder.tvFlightCity.setText(flightCityArray.get(position).getCityName());
        viewHolder.tvFlightCode.setText(flightCityArray.get(position).getAirportCode() + "-" + flightCityArray.get(position).getAirportDesc());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new ItemFilter();
    }


    static class ViewHolder {
        TextView tvFlightCity;
        TextView tvFlightCode;
    }


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<DomesticFlightModel> list = filteredData;

            int count = list.size();
            final ArrayList<DomesticFlightModel> nlist = new ArrayList<DomesticFlightModel>(count);

            String filterableString, filterableStringcode, filterableStringAreo;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getCityName();
                filterableStringcode = list.get(i).getAirportCode();
                filterableStringAreo = list.get(i).getAirportDesc();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                } else if (filterableStringcode.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                } else if (filterableStringAreo.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            flightCityArray = (ArrayList<DomesticFlightModel>) results.values;
            notifyDataSetChanged();
        }

    }
}

