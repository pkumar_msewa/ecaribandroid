package in.msewa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.msewa.metadata.AppMetadata;
import in.msewa.model.FlightListModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.flightinneracitivty.FlightPriceOneWayActivity;

/**
 * Created by Ksf on 10/13/2016.
 */
public class FlightListAdapter extends RecyclerView.Adapter<FlightListAdapter.RecyclerViewHolders> {


  private final boolean intentional;
  private Context context;
  private List<FlightListModel> flightArray;
  private String destinationCode, sourceCode, dateOfJourney;
  private int adultNo, childNo, infantNo;
  private String flightClass;

  private String jsonToSend = "";
  private String jsonFareToSend = "";
  private int pasanger;
  private int citylist;
  private ArrayList<String> strings = new ArrayList<>();

  public FlightListAdapter(Context context, List<FlightListModel> flightArray, String sourceCode, String destinationCode, String dateOfJourney, int adultNo, int childNo, int infantNo, String flightClass, String jsonToSend, String jsonFareToSend, int citylist, boolean intentional) {
    this.context = context;
    this.flightArray = flightArray;
    this.sourceCode = sourceCode;
    this.destinationCode = destinationCode;
    this.dateOfJourney = dateOfJourney;
    this.flightClass = flightClass;
    this.adultNo = adultNo;
    this.childNo = childNo;
    this.citylist = citylist;
    this.intentional = intentional;
    this.infantNo = infantNo;
    this.jsonFareToSend = jsonFareToSend;
    this.jsonToSend = jsonToSend;
    pasanger = adultNo + childNo + infantNo;

  }

  @Override
  public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
    View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_flight_list, null);
    RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
    return rcv;
  }

  @Override
  public void onBindViewHolder(final RecyclerViewHolders viewHolder, int position) {
    String totalDuration;
    final int currentPosition = position;
    viewHolder.tvFlightsource.setText(sourceCode);
    viewHolder.tvFlightdes.setText(destinationCode);
    if (flightArray.get(position).getRefund().equalsIgnoreCase("true")) {
      viewHolder.tvFlightRefund.setText("Refundable");
    } else {
      viewHolder.tvFlightRefund.setText("Non-Refundable");
    }
    if (flightArray.get(position).getFlightListArray().size() == 1) {
//            Picasso.with(context).load(AppMetadata.getFLightImage(flightArray.get(position).getFlightListArray().get(0).getFlightName(), viewHolder.tvFlightListName)).into(viewHolder.ivFightList);
      if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("6E")) {
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        Picasso.with(context).load(R.drawable.flight_indigo).into(viewHolder.ivFightList);
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("SG")) {
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        Picasso.with(context).load(R.drawable.flight_spice_jet).into(viewHolder.ivFightList);
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("UK")) {
        Picasso.with(context).load(R.drawable.flight_vistara).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        viewHolder.tvFightListType.setText("Vistara");
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("9W")) {
        Picasso.with(context).load(R.drawable.flight_jet_airways).into(viewHolder.ivFightList);
        viewHolder.tvFightListType.setText("JetAirWays");
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("AI")) {
        Picasso.with(context).load(R.drawable.flight_air_india).into(viewHolder.ivFightList);
        viewHolder.tvFightListType.setText("AirIndia");
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("I5")) {
        Picasso.with(context).load(R.drawable.flight_air_asia).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("XT")) {
        Picasso.with(context).load(R.drawable.skystar).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("G8")) {
        Picasso.with(context).load(R.drawable.flight_go_air).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      }else {
        Picasso.with(context).load(R.drawable.ic_no_flight).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      }
      viewHolder.tvFlightVia.setText("Non-Stop");
      viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(0).getArrivalTime());

//            totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(0).getArrivalTime());
      viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getDuration());
      viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightRule());
//            viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightListType.setText(flightArray.get(position).getFlightListArray().get(0).getFligtType());
    } else {
//            viewHolder.tvFlightVia.setVisibility(View.VISIBLE);
      viewHolder.tvFlightVia.setText("Non-Stop");
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());
      viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(0).getArrivalTime());
      strings.add(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());

      //Checking connectivity
      if (flightArray.get(position).getFlightListArray().size() == 2) {
        //Stoppage
        viewHolder.tvFlightVia.setText("1Stop ");
        //Time
        totalDuration = flightArray.get(position).getDuration();
        viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getDuration());
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(1).getArrivalTime());
        strings.add(totalDuration);

      } else if (flightArray.get(position).getFlightListArray().size() == 3) {
        viewHolder.tvFlightVia.setText("2Stops");

        //Time
        totalDuration = flightArray.get(position).getDuration();
        viewHolder.tvFlightTotalTime.setText(totalDuration);
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(2).getArrivalTime());
        strings.add(totalDuration);

      } else if (flightArray.get(position).getFlightListArray().size() == 4) {
        viewHolder.tvFlightVia.setText("3Stops");

        //Time
        totalDuration = flightArray.get(position).getDuration();
        viewHolder.tvFlightTotalTime.setText(totalDuration);
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(3).getArrivalTime());
        strings.add(totalDuration);
      } else if (flightArray.get(position).getFlightListArray().size() == 5) {
        viewHolder.tvFlightVia.setText("4Stops");
        //Time
        totalDuration = flightArray.get(position).getDuration();
        viewHolder.tvFlightTotalTime.setText(totalDuration);
        strings.add(totalDuration);
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(4).getArrivalTime());
      } else {
        viewHolder.tvFlightVia.setText("with " + (flightArray.get(position).getFlightListArray().size() - 1) + " Stops");
        //Time
        totalDuration = flightArray.get(position).getDuration();
        viewHolder.tvFlightTotalTime.setText(totalDuration);
        strings.add(totalDuration);
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(flightArray.get(position).getFlightListArray().size() - 1).getArrivalTime());
      }

      viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightRule());
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightListType.setText(flightArray.get(position).getFlightListArray().get(0).getFligtType());
    }

    viewHolder.ivFightList.setImageResource(AppMetadata.getFLightImage(flightArray.get(position).getFlightListArray().get(0).getFlightCode().trim(), viewHolder.tvFlightListName));
    String amount = String.valueOf(pasanger);
    viewHolder.tvFlightFare.setText(context.getResources().getString(R.string.rupease) + " " + String.valueOf(flightArray.get(position).getFlightNetFare()));
    if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("6E")) {
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      Picasso.with(context).load(R.drawable.flight_indigo).into(viewHolder.ivFightList);
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("SG")) {
      Picasso.with(context).load(R.drawable.flight_spice_jet).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("UK")) {
      Picasso.with(context).load(R.drawable.flight_vistara).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightListType.setText("Vistara");
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("9W")) {
      Picasso.with(context).load(R.drawable.flight_jet_airways).into(viewHolder.ivFightList);
      viewHolder.tvFightListType.setText("JetAirWays");
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("AI")) {
      Picasso.with(context).load(R.drawable.flight_air_india).into(viewHolder.ivFightList);
      viewHolder.tvFightListType.setText("AirIndia");
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("I5")) {
      Picasso.with(context).load(R.drawable.flight_air_asia).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("XT")) {
      Picasso.with(context).load(R.drawable.skystar).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("G8")) {
      Picasso.with(context).load(R.drawable.flight_go_air).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    }else {
      Picasso.with(context).load(R.drawable.ic_no_flight).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    }
    viewHolder.llFlightListMain.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent flightDetailIntent = new Intent(context, FlightPriceOneWayActivity.class);
        flightDetailIntent.putExtra("FlightArray", flightArray.get(currentPosition));
        flightDetailIntent.putExtra("sourceCode", sourceCode);
        flightDetailIntent.putExtra("destinationCode", destinationCode);
        flightDetailIntent.putExtra("dateOfJourney", dateOfJourney);
        flightDetailIntent.putExtra("citylist", citylist);
        flightDetailIntent.putExtra("adultNo", adultNo);
        flightDetailIntent.putExtra("childNo", childNo);
        flightDetailIntent.putExtra("infantNo", infantNo);
        flightDetailIntent.putExtra("flightClass", flightClass);
        flightDetailIntent.putExtra("jsonToSend", jsonToSend);
        flightDetailIntent.putExtra("jsonFareToSend", jsonFareToSend);
        flightDetailIntent.putExtra("intentionaloneway", intentional);

        flightDetailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(flightDetailIntent);
      }
    });

  }

  @Override
  public int getItemCount() {
    return this.flightArray.size();
  }


  public class RecyclerViewHolders extends RecyclerView.ViewHolder {
    public TextView tvFlightListName;
    public TextView tvFlightArrivalTime;
    public TextView tvFlightDepTime;
    public TextView tvFlightFare;
    public TextView tvFlightTotalTime;
    public TextView tvFightType;
    public TextView tvFlightRule;
    public TextView tvFlightVia;
    public LinearLayout llFlightListMain;
    public ImageView ivFightList;
    private TextView tvFlightsource, tvFlightdes;
    public TextView tvFightListType, tvFlightRefund;

    public RecyclerViewHolders(View convertView) {
      super(convertView);
      tvFlightListName = (TextView) convertView.findViewById(R.id.tvFightListName);
      tvFlightArrivalTime = (TextView) convertView.findViewById(R.id.tvFlightArrivalTime);
      tvFlightDepTime = (TextView) convertView.findViewById(R.id.tvFlightDepTime);
      tvFlightFare = (TextView) convertView.findViewById(R.id.tvFlightFare);
      tvFlightTotalTime = (TextView) convertView.findViewById(R.id.tvFlightTotalTime);
      llFlightListMain = (LinearLayout) convertView.findViewById(R.id.llFlightListMain);
      tvFightType = (TextView) convertView.findViewById(R.id.tvFightType);
      tvFlightRule = (TextView) convertView.findViewById(R.id.tvFlightRule);
      tvFlightVia = (TextView) convertView.findViewById(R.id.tvFlightVia);
      ivFightList = (ImageView) convertView.findViewById(R.id.ivFightList);
      tvFlightsource = (TextView) convertView.findViewById(R.id.tvFlightsource);
      tvFlightdes = (TextView) convertView.findViewById(R.id.tvFlightdes);
      tvFlightRefund = (TextView) convertView.findViewById(R.id.tvFlightRefund);
      tvFightListType = (TextView) convertView.findViewById(R.id.tvFightListType);
    }
  }

  public String compareDate(String depTime, String arrTime, TextView tvFlightArrivalTime) {
    //HH converts hour in 24 hours format (0-23), day calculation
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");

    Date d1 = null;
    Date d2 = null;


    try {
      d1 = format.parse(depTime);
      d2 = format.parse(arrTime);

      long difference = d2.getTime() - d1.getTime();

      if (difference < 0) {
        Date dateMax = format.parse("24:00");
        Date dateMin = format.parse("00:00");
        difference = (dateMax.getTime() - d1.getTime()) + (d2.getTime() - dateMin.getTime());
      }
      int days = (int) (difference / (1000 * 60 * 60 * 24));
      int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
      int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

      Calendar cal = Calendar.getInstance();
      cal.setTime(d1);
      cal.add(Calendar.HOUR, (int) hours);
      cal.add(Calendar.MINUTE, (int) min);
      String newTime = format.format(cal.getTime());
      tvFlightArrivalTime.setText(newTime);
      if (days >= 1) {
        String date2 = String.valueOf(days + "d" + hours + "h" + min + "m");
        return date2.replace("-", "");
      } else {
        String date2 = String.valueOf(hours + " h" + min + " m");
        return date2.replace("-", "");
      }
//            }
      //in milliseconds


    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;

  }

  public List<FlightListModel> getFlightArray() {
    return flightArray;
  }

  public ArrayList<String> getDepature() {
    return strings;
  }
}

