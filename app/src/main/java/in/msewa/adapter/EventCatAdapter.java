package in.msewa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.List;

import in.msewa.custom.CustomToast;
import in.msewa.model.EventCategoryModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.eventinneractivity.EventListActivity;

/**
 * Created by Kashif-PC on 1/3/2017.
 */
public class EventCatAdapter extends RecyclerView.Adapter<EventCatAdapter.RecyclerViewHolders> {

    private List<EventCategoryModel> itemList;
    private Context context;

    public EventCatAdapter(Context context, List<EventCategoryModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_event_cat, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        holder.tvEventCatName.setText(itemList.get(position).getCatName());
        if (!itemList.get(position).getCatImage().isEmpty()) {
            AQuery aq = new AQuery(context);
            aq.id(holder.ivEventCat).background(R.drawable.loading_image).image(itemList.get(position).getCatImage(), true, true);
            holder.ivEventCat.setVisibility(View.VISIBLE);
        } else {
            holder.ivEventCat.setVisibility(View.GONE);
        }

        holder.llEventMainAdp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context,EventListActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvEventCatName;
        public ImageView ivEventCat;
        public LinearLayout llEventMainAdp;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvEventCatName = (TextView) itemView.findViewById(R.id.tvEventCatName);
            ivEventCat = (ImageView) itemView.findViewById(R.id.ivEventCat);
            llEventMainAdp = (LinearLayout) itemView.findViewById(R.id.llEventMainAdp);
        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();
            CustomToast.showMessage(context, "Pos:" + i);
        }
    }
}

