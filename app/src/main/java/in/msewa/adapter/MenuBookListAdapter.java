package in.msewa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import in.msewa.custom.CustomToast;
import in.msewa.model.MenuOrderListModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.travelKhanaAvtivity.MyOrdersActivity;


/**
 * Created by Kashif-PC on 2/21/2017.
 */
public class MenuBookListAdapter extends RecyclerView.Adapter<MenuBookListAdapter.RecyclerViewHolders> {

    Date arrTime, depTime = null;
    private Context context;
    private ArrayList<MenuOrderListModel> menuArray;

    public MenuBookListAdapter(Context context, ArrayList<MenuOrderListModel> menuArray) {
        this.context = context;
        this.menuArray = menuArray;

    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_book_menu_list, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders viewHolder, final int position) {

        viewHolder.tvUserName.setText(menuArray.get(position).getName());
        viewHolder.tvTrainNoSeatCoach.setText("Train : "+menuArray.get(position).getTrainNumber() + ",  " + menuArray.get(position).getCoach()+" / "+menuArray.get(position).getSeat());
        viewHolder.tvTicketPnrNo.setText("PNR: " + menuArray.get(position).getPnr() + "");
        viewHolder.tvOrderDateTime.setText(menuArray.get(position).getOrderDate() + ",  ETA : " + menuArray.get(position).getEta());
        viewHolder.tvDate.setText(menuArray.get(position).getTxnDate());
        viewHolder.tvUserMobile.setText("Mob: " + menuArray.get(position).getContactNo() + "");
        viewHolder.tvTotalAmount.setText(context.getResources().getString(R.string.rupease) + " " + menuArray.get(position).getTotalCustomerPayable());
        viewHolder.tvStatus.setText(menuArray.get(position).getOrderStatus());
        if(menuArray.get(position).getOrderStatus().equalsIgnoreCase("Success")){
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.success));
        }else if(menuArray.get(position).getOrderStatus().equalsIgnoreCase("Failed")) {
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.faliure));
        }else {
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.others));
        }


        viewHolder.llBusListMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!menuArray.get(position).getUserOrderId().equals("0")){
                    Intent myOrderIntent = new Intent(context, MyOrdersActivity.class);
                    myOrderIntent.putExtra("OrderID",menuArray.get(position).getUserOrderId());
                    context.startActivity(myOrderIntent);
                }
                else {
                    CustomToast.showMessage(context,"Your order was not successful");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.menuArray.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
       private TextView tvTicketPnrNo, tvUserName, tvTrainNoSeatCoach, tvOrderDateTime, tvDate, tvUserMobile, tvStatus, tvTotalAmount;
        LinearLayout llBusListMain;


        public RecyclerViewHolders(View convertView) {
            super(convertView);
            tvUserName = (TextView) convertView.findViewById(R.id.tvUserName);
            tvTrainNoSeatCoach = (TextView) convertView.findViewById(R.id.tvTrainNoSeatCoach);
            tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            tvOrderDateTime = (TextView) convertView.findViewById(R.id.tvOrderDateTime);
            tvTicketPnrNo = (TextView) convertView.findViewById(R.id.tvTicketPnrNo);
            tvTotalAmount = (TextView) convertView.findViewById(R.id.tvTotalAmount);
            tvUserMobile = (TextView) convertView.findViewById(R.id.tvUserMobile);
            tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
            llBusListMain = (LinearLayout) convertView.findViewById(R.id.llBusListMain);
        }
    }

}
