package in.msewa.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import in.msewa.custom.TextDrawable;
import in.msewa.model.AddOnsModel;
import in.msewa.ecarib.R;

/**
 * Created by acer on 06-07-2017.
 */


public class AddonsAdapter extends RecyclerView.Adapter<AddonsAdapter.ViewHolder> {

    List<Object> SubjectValues;
    //ArrayList<AddOnsModel.CarAdons> SubjectValues1;
    static Context context;
    View view1;
    List<AddOnsModel.BusAdons> busAdons;
    ViewHolder viewHolder1;
    ImageButton btndowns;
    JSONObject bus, carAddon, mainJson, bunchJson, snowJson;
    TextView textView;
    String totalAmount;
    private HashMap<String, String> stringStringHashMap, total_Amount, checkBusLocation;
    private HashMap<String, String> map, snowMap;
    private HashMap<String, String> BusJson;
    private int currentAd, carLength, busLength, bunchLength;
    private boolean checkBus = false, checkSnowPark = false;

    public AddonsAdapter(Context context1, ArrayList<Object> objectArrayList, int carLength, int busLength, int bunchLength) {

        SubjectValues = objectArrayList;
        context = context1;
        CardView cardview;
        this.stringStringHashMap = new HashMap<>();
        this.carLength = carLength;
        this.busLength = busLength;
        this.bunchLength = bunchLength;
        this.bunchJson = new JSONObject();
        this.snowJson = new JSONObject();
        this.bus = new JSONObject();
        this.total_Amount = new HashMap<>();
        this.map = new HashMap<>();
        this.carAddon = new JSONObject();
        this.BusJson = new HashMap<>();
        this.snowMap = new HashMap<>();
        this.checkBusLocation = new HashMap<>();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvtype, cTitle, tvLocation, tvpercost, tvaDec, textView3, tvtotalcost, tvquantityn, tvAdTitle;
        CardView cardView1, cardView2;
        private ImageView ivimage;
        private LinearLayout location;
        private Spinner spLocation;
        private ImageButton btnDown, btnUp;
        private ImageButton ibbtndowns, btnup, btndown, btnups;
        int i = 0;


        public ViewHolder(View v) {
            super(v);

            cTitle = (TextView) v.findViewById(R.id.tvaTitle);
            tvAdTitle = (TextView) v.findViewById(R.id.tvAdTitle);
            tvtype = (TextView) v.findViewById(R.id.tvtype);
            tvLocation = (TextView) v.findViewById(R.id.tvLocation);
            spLocation = (Spinner) v.findViewById(R.id.spLocation);
            tvpercost = (TextView) v.findViewById(R.id.tvpercost);
            location = (LinearLayout) v.findViewById(R.id.location);
            btnDown = (ImageButton) v.findViewById(R.id.btnDown);
            btnUp = (ImageButton) v.findViewById(R.id.btnUp);
            cardView1 = (CardView) v.findViewById(R.id.cvaddons);
            tvaDec = (TextView) v.findViewById(R.id.tvaDec);
            tvtotalcost = (TextView) v.findViewById(R.id.tvtotalcost);
            tvquantityn = (TextView) v.findViewById(R.id.tvquantityn);

            TextDrawable plus = new TextDrawable(context, "+", ColorStateList.valueOf(context.getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
            TextDrawable mins = new TextDrawable(context, "-", ColorStateList.valueOf(context.getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);

            btnUp.setImageDrawable(plus);
            btnDown.setImageDrawable(mins);
        }
    }

    @Override
    public AddonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.addons_adapter, parent, false);
        viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tvAdTitle.setVisibility(View.GONE);
        holder.tvaDec.setVisibility(View.GONE);
        if (SubjectValues.get(position) instanceof AddOnsModel.CarAdons) {
            final AddOnsModel.CarAdons carAdons = (AddOnsModel.CarAdons) SubjectValues.get(position);
            Log.i("quality",carAdons.getQuantity());
            if (position == 0) {
                Log.i("tittle", carAdons.getCarAdTitle());
                holder.tvAdTitle.setVisibility(View.VISIBLE);
                holder.tvAdTitle.setText(carAdons.getCarAdTitle());
                holder.cTitle.setText(carAdons.getcTitle());
                holder.tvtype.setText("AddOns");
                holder.tvpercost.setText(carAdons.getAddOnCost() + " INR");
                holder.tvquantityn.setText(carAdons.getQuantity());
                stringStringHashMap.put(carAdons.getAid(), carAdons.getAid());
                holder.btnUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int val = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
                        holder.tvquantityn.setText(String.valueOf(val));
                        holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));

                        try {
                            if (carAddon.has(stringStringHashMap.get(carAdons.getAid()))) {
                                carAddon.remove(carAdons.getAid());
                                carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            } else {
                                carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            }
                            if (total_Amount.containsKey(position + "CarAddon")) {
                                total_Amount.remove(position + "CarAddon");
                                total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            } else {
                                total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            }

                            Log.i("jsonValue", carAddon.toString());

                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.btnDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) != 0) {
                            int val = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
                            holder.tvquantityn.setText(String.valueOf(val));
                            holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                            try {
                                if (carAddon.has(stringStringHashMap.get(carAdons.getAid()))) {
                                    carAddon.remove(carAdons.getAid());
                                    carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                } else {
                                    carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                }
                                if (total_Amount.containsKey(position + "CarAddon")) {
                                    total_Amount.remove(position + "CarAddon");
                                    total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                } else {
                                    total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                }
                                if (holder.tvquantityn.getText().equals("0")) {
                                    carAddon.remove(carAdons.getAid());
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                } else {
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            carAddon.remove(carAdons.getAid());
                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        }
                    }
                });

            } else {
                holder.tvAdTitle.setVisibility(View.GONE);
                holder.tvpercost.setText(carAdons.getAddOnCost() + " INR");
                holder.cTitle.setText(carAdons.getcTitle());
                holder.tvtype.setText(carAdons.getType());
                holder.tvquantityn.setText(carAdons.getQuantity());
                stringStringHashMap.put(carAdons.getAid(), carAdons.getAid());

                Log.i("Str", stringStringHashMap.toString());
                holder.btnUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int val = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
                        holder.tvquantityn.setText(String.valueOf(val));
                        holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));

                        try {
                            if (carAddon.has(stringStringHashMap.get(carAdons.getAid()))) {
                                carAddon.remove(carAdons.getAid());
                                carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            } else {
                                carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            }
                            if (total_Amount.containsKey(position + "CarAddon")) {
                                total_Amount.remove(position + "CarAddon");
                                total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            } else {
                                total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            }

                            Log.i("jsonValue", carAddon.toString());

                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.btnDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) != 0) {
                            int val = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
                            holder.tvquantityn.setText(String.valueOf(val));
                            holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                            try {
                                if (carAddon.has(stringStringHashMap.get(carAdons.getAid()))) {
                                    carAddon.remove(carAdons.getAid());
                                    carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                } else {
                                    carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                }
                                if (total_Amount.containsKey(position + "CarAddon")) {
                                    total_Amount.remove(position + "CarAddon");
                                    total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                } else {
                                    total_Amount.put(position + "CarAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                }
                                if (holder.tvquantityn.getText().equals("0")) {
                                    carAddon.remove(carAdons.getAid());
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                } else {
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            carAddon = new JSONObject();
                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);

                        }
                    }
                });
            }


        } else if (SubjectValues.get(position) instanceof AddOnsModel.BusAdons) {
            final AddOnsModel.BusAdons carAdons = (AddOnsModel.BusAdons) SubjectValues.get(position);
            Log.i("postion", String.valueOf(SubjectValues.size() - carLength) + " " + position + "  " + carAdons.getTitle());
            holder.location.setVisibility(View.VISIBLE);
            holder.tvLocation.setText("Bus location");
            if (position == carLength) {
                Log.i("tittle", carAdons.getQuantity());
                holder.tvAdTitle.setVisibility(View.VISIBLE);
                holder.tvAdTitle.setText("Bus Addons");
                holder.cTitle.setText(carAdons.getTitle());
                holder.tvtype.setText("AddOns");
                holder.tvpercost.setText(carAdons.getAddOnCost() + " INR");
                holder.tvquantityn.setText(carAdons.getQuantity());
                ArrayList<String> strings = new ArrayList<>();

                for (AddOnsModel.BusAdons.locationList locationlist : carAdons.getLocationList()) {
                    strings.add(locationlist.getLocationName());
                    checkBusLocation.put(locationlist.getLocationName(), locationlist.getLocationId());
                }
                ArrayAdapter arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, strings);
                holder.spLocation.setAdapter(arrayAdapter);
                holder.spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) > 0) {
                            if (BusJson.containsKey(position + "BusAddon")) {
                                BusJson.remove(position + "BusAddon");
                                String msg = adapterView.getItemAtPosition(i).toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            } else {
                                String msg = adapterView.getItemAtPosition(i).toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            }
                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });


                stringStringHashMap.put(carAdons.getAid(), carAdons.getAid());
                holder.btnUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int val = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
                        holder.tvquantityn.setText(String.valueOf(val));
                        holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                        try {
                            if (bus.has(stringStringHashMap.get(carAdons.getAid()))) {
                                bus.remove(carAdons.getAid());
                                bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            } else {
                                bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            }

                            if (total_Amount.containsKey(position + "BusAddon")) {
                                total_Amount.remove(position + "BusAddon");
                                total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            } else {
                                total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            }
                            if (BusJson.containsKey(position + "BusAddon")) {
                                BusJson.remove(position + "BusAddon");
                                String msg = holder.spLocation.getSelectedItem().toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            } else {
                                String msg = holder.spLocation.getSelectedItem().toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            }
                            checkBus = true;
                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.btnDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) != 0) {
                            int val = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
                            holder.tvquantityn.setText(String.valueOf(val));
                            holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                            try {
                                if (bus.has(stringStringHashMap.get(carAdons.getAid()))) {
                                    bus.remove(carAdons.getAid());
                                    bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                } else {
                                    bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                }
                                if (total_Amount.containsKey(position + "BusAddon")) {
                                    total_Amount.remove(position + "BusAddon");
                                    total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                } else {
                                    total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                }
                                if (BusJson.containsKey(position + "BusAddon")) {
                                    BusJson.remove(position + "BusAddon");
                                    String msg = holder.spLocation.getSelectedItem().toString();
                                    String[] value = msg.split(",");
                                    BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                                } else {
                                    String msg = holder.spLocation.getSelectedItem().toString();
                                    String[] value = msg.split(",");
                                    BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                                }
                                Log.i("jsonValue", carAddon.toString());
                                if (holder.tvquantityn.getText().equals("0")) {
                                    BusJson.clear();
                                    bus.remove(carAdons.getAid());
                                    checkBus = false;
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                } else {
                                    checkBus = true;
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            } else {
                holder.tvAdTitle.setVisibility(View.GONE);
                holder.cTitle.setText(carAdons.getTitle());
                holder.tvtype.setText("AddOns");
                Log.i("tittle", carAdons.getQuantity());
                holder.tvpercost.setText(carAdons.getAddOnCost() + " INR");
                holder.tvquantityn.setText(carAdons.getQuantity());
                ArrayList<String> strings = new ArrayList<>();
                for (AddOnsModel.BusAdons.locationList locationlist : carAdons.getLocationList()) {
                    strings.add(locationlist.getLocationName());
                    if (checkBusLocation.containsKey(locationlist.getLocationName())) {
                        checkBusLocation.put(locationlist.getLocationName(), locationlist.getLocationId());
                    } else {
                        checkBusLocation.put(locationlist.getLocationName(), locationlist.getLocationId());
                    }
                }
                stringStringHashMap.put(carAdons.getAid(), carAdons.getAid());
                ArrayAdapter arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, strings);
                holder.spLocation.setAdapter(arrayAdapter);
                holder.spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) > 0) {
                            if (BusJson.containsKey(position + "BusAddon")) {
                                BusJson.remove(position + "BusAddon");
                                String msg = adapterView.getItemAtPosition(i).toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            } else {
                                String msg = adapterView.getItemAtPosition(i).toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            }
                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                holder.btnUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int val = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
                        holder.tvquantityn.setText(String.valueOf(val));
                        holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                        try {
                            if (bus.has(stringStringHashMap.get(carAdons.getAid()))) {
                                bus.remove(carAdons.getAid());
                                bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            } else {
                                bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                            }
                            if (total_Amount.containsKey(position + "BusAddon")) {
                                total_Amount.remove(position + "BusAddon");
                                total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            } else {
                                total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                            }
                            String string = holder.spLocation.getSelectedItem().toString();
                            String[] value1 = string.split(",");
                            if (BusJson.containsKey(position + "BusAddon")) {
                                BusJson.remove(position + "BusAddon");
                                String msg = holder.spLocation.getSelectedItem().toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            } else {
                                String msg = holder.spLocation.getSelectedItem().toString();
                                String[] value = msg.split(",");
                                BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                            }
                            Log.i("jsonValue", carAddon.toString());
                            checkBus = true;
                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.btnDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) != 0) {
                            int val = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
                            holder.tvquantityn.setText(String.valueOf(val));
                            holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                            try {
                                if (bus.has(stringStringHashMap.get(carAdons.getAid()))) {
                                    bus.remove(carAdons.getAid());
                                    bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                } else {
                                    bus.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
                                }

                                if (total_Amount.containsKey(position + "BusAddon")) {
                                    total_Amount.remove(position + "BusAddon");
                                    total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                } else {
                                    total_Amount.put(position + "BusAddon", carAdons.getAddOnCost() + "," + holder.tvquantityn.getText().toString());
                                }
                                if (BusJson.containsKey(position + "BusAddon")) {
                                    BusJson.remove(position + "BusAddon");
                                    String msg = holder.spLocation.getSelectedItem().toString();
                                    String[] value = msg.split(",");
                                    BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                                } else {
                                    String msg = holder.spLocation.getSelectedItem().toString();
                                    String[] value = msg.split(",");
                                    BusJson.put(position + "BusAddon", carAdons.getPlaceId() + "," + checkBusLocation.get(holder.spLocation.getSelectedItem().toString()));
                                }
                                if (holder.tvquantityn.getText().equals("0")) {
                                    BusJson.clear();
                                    bus.remove(carAdons.getAid());
                                    checkBus = false;
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                } else {
                                    checkBus = true;
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }
// else if (SubjectValues.get(position) instanceof AddOnsModel.BrunchAdons) {
//            final AddOnsModel.BrunchAdons carAdons = (AddOnsModel.BrunchAdons) SubjectValues.get(position);
//            Log.i("postion", String.valueOf(SubjectValues.size() - position) + " " + position + "  " + carAdons.getTitle());
//            holder.tvaDec.setVisibility(View.VISIBLE);
//
//            if (position == (carLength + busLength)) {
//                Log.i("tittle", carAdons.getTitle());
//                holder.tvAdTitle.setVisibility(View.VISIBLE);
//                holder.tvAdTitle.setText("Brunch Addons");
//                holder.cTitle.setText(carAdons.getTitle());
//                holder.tvaDec.setVisibility(View.VISIBLE);
//                holder.tvtype.setText("AddOns");
//                holder.tvaDec.setText(Html.fromHtml(carAdons.getDescription()));
//                holder.tvpercost.setText(carAdons.getCostPer() + " INR");
//                holder.tvquantityn.setText(carAdons.getQuantity());
//                stringStringHashMap.put(carAdons.getAid(), carAdons.getAid());
//                holder.btnUp.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        int val = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
//                        holder.tvquantityn.setText(String.valueOf(val));
//                        holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
//                        try {
//                            if (bunchJson.has(stringStringHashMap.get(carAdons.getAid()))) {
//                                bunchJson.remove(carAdons.getAid());
//                                bunchJson.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                            } else {
//                                bunchJson.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                            }
//                            if (total_Amount.containsKey(position + "BurnchAddon")) {
//                                total_Amount.remove(position + "BurnchAddon");
//                                total_Amount.put(position + "BurnchAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                            } else {
//                                total_Amount.put(position + "BusAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                            }
//                            sendRefresh(holder.tvtotalcost.getText().toString(), carAddon, bus, bunchJson, snowJson, map, map, total_Amount);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                holder.btnDown.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) != 0) {
//                            int val = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
//                            holder.tvquantityn.setText(String.valueOf(val));
//                            holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
//                            try {
//                                if (bunchJson.has(stringStringHashMap.get(carAdons.getAid()))) {
//                                    bunchJson.remove(carAdons.getAid());
//                                    bunchJson.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                                } else {
//                                    bunchJson.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                                }
//                                Log.i("jsonValue", carAddon.toString());
//                                if (total_Amount.containsKey(position + "BurnchAddon")) {
//                                    total_Amount.remove(position + "BurnchAddon");
//                                    total_Amount.put(position + "BurnchAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                                } else {
//                                    total_Amount.put(position + "BusAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                                }
//                                sendRefresh(holder.tvtotalcost.getText().toString(), carAddon, bus, carAddon, bus, map, map, total_Amount);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                });
//            } else {
//                holder.tvAdTitle.setVisibility(View.GONE);
//                holder.cTitle.setText(carAdons.getTitle());
//                holder.tvtype.setText("AddOns");
//                holder.tvaDec.setText(Html.fromHtml(carAdons.getDescription()));
//                holder.tvpercost.setText(carAdons.getCostPer() + " INR");
//                holder.tvquantityn.setText(carAdons.getQuantity());
//                stringStringHashMap.put(carAdons.getAid(), carAdons.getAid());
//                holder.btnUp.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        int val = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
//                        holder.tvquantityn.setText(String.valueOf(val));
//                        holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
//                        try {
//                            if (bunchJson.has(stringStringHashMap.get(carAdons.getAid()))) {
//                                bunchJson.remove(carAdons.getAid());
//                                bunchJson.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                            } else {
//                                bunchJson.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                            }
//                            if (total_Amount.containsKey(position + "BurnchAddon")) {
//                                total_Amount.remove(position + "BurnchAddon");
//                                total_Amount.put(position + "BurnchAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                            } else {
//                                total_Amount.put(position + "BusAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                            }
//                            Log.i("jsonValue", bunchJson.toString());
//                            sendRefresh(holder.tvtotalcost.getText().toString(), carAddon, bus, bunchJson, snowJson, map, map, total_Amount);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                holder.btnDown.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) != 0) {
//                            int val = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
//                            holder.tvquantityn.setText(String.valueOf(val));
//                            holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
//                            try {
//                                if (carAddon.has(stringStringHashMap.get(carAdons.getAid()))) {
//                                    carAddon.remove(carAdons.getAid());
//                                    carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                                } else {
//                                    carAddon.put(stringStringHashMap.get(carAdons.getAid()), holder.tvquantityn.getText().toString());
//                                }
//                                if (total_Amount.containsKey(position + "BurnchAddon")) {
//                                    total_Amount.remove(position + "BurnchAddon");
//                                    total_Amount.put(position + "BurnchAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                                } else {
//                                    total_Amount.put(position + "BusAddon", carAdons.getCostPer() + "," + holder.tvquantityn.getText().toString());
//                                }
//                                Log.i("jsonValue", carAddon.toString());
//                                sendRefresh(holder.tvtotalcost.getText().toString(), carAddon, bus, bunchJson, snowJson, map, map, total_Amount);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                });
//            }
//        }
        else if (SubjectValues.get(position) instanceof AddOnsModel.snowMagicaAddOn) {
            final AddOnsModel.snowMagicaAddOn carAdons = (AddOnsModel.snowMagicaAddOn) SubjectValues.get(position);
            Log.i("postion", String.valueOf(SubjectValues.size() - position) + " " + position + "  " + carAdons.getTitle());
            holder.tvaDec.setVisibility(View.VISIBLE);
            holder.location.setVisibility(View.VISIBLE);
            holder.tvLocation.setText("Time Slots");
            if (position == (carLength + busLength)) {
                Log.i("tittle", carAdons.getTitle());
                holder.tvAdTitle.setVisibility(View.VISIBLE);
                holder.tvAdTitle.setText("Add Snow Addons Ticket");
                holder.cTitle.setText(carAdons.getTitle());
                holder.tvaDec.setVisibility(View.VISIBLE);
                holder.tvtype.setText("AddOns");
                stringStringHashMap.put(carAdons.getTicketType().getProductId(), carAdons.getTicketType().getProductId());
                holder.tvaDec.setText(Html.fromHtml(carAdons.getTicketDescription()));
                ArrayList<String> strings = new ArrayList<>();

                for (AddOnsModel.snowMagicaAddOn.sessionList list : carAdons.getSessionlist()) {
                    for (AddOnsModel.snowMagicaAddOn.sessionList.timeSlots locationlist : list.getTimelots()) {
                        strings.add(locationlist.getSlotName());
                        if (map.containsKey(locationlist.getSlotName())) {
                            map.remove(locationlist.getSlotName());
                            map.put(locationlist.getSlotName(), list.getSessionId() + "," + locationlist.getSlotId());
                        } else {
                            map.put(locationlist.getSlotName(), list.getSessionId() + "," + locationlist.getSlotId());
                        }
                    }
                }
                ArrayAdapter arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, strings);
                holder.spLocation.setAdapter(arrayAdapter);
                holder.tvpercost.setText(carAdons.getTicketType().getCostPerQty() + " INR");
                holder.spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) > 0) {
                            Log.i("MAPS", map.get(adapterView.getItemAtPosition(i)));
                            if (snowMap.containsKey(position + "snowMap")) {
                                snowMap.remove(position + "snowMap");
                                snowMap.put(position + "snowMap", map.get(adapterView.getItemAtPosition(i)));
                            } else {
                                snowMap.put(position + "snowMap", map.get(adapterView.getItemAtPosition(i)));
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                holder.tvquantityn.setText(carAdons.getTicketType().getQuantity());
                holder.btnUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int val = Integer.parseInt(holder.tvquantityn.getText().toString()) + 1;
                        holder.tvquantityn.setText(String.valueOf(val));
                        holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                        try {
                            if (bunchJson.has(stringStringHashMap.get(carAdons.getTicketType().getProductId()))) {
                                bunchJson.remove(carAdons.getTicketType().getProductId());
                                bunchJson.put(stringStringHashMap.get(carAdons.getTicketType().getProductId()), holder.tvquantityn.getText().toString());
                            } else {
                                bunchJson.put(stringStringHashMap.get(carAdons.getTicketType().getProductId()), holder.tvquantityn.getText().toString());
                            }
                            if (total_Amount.containsKey(position + "snowMap")) {
                                total_Amount.remove(position + "snowMap");
                                total_Amount.put(position + "snowMap", carAdons.getTicketType().getCostPerQty() + "," + holder.tvquantityn.getText().toString());
                            } else {
                                total_Amount.put(position + "snowMap", carAdons.getTicketType().getCostPerQty() + "," + holder.tvquantityn.getText().toString());
                            }
                            if (snowMap.containsKey(position + "snowMap")) {
                                snowMap.remove(position + "snowMap");
                                snowMap.put(position + "snowMap", map.get(holder.spLocation.getSelectedItem()));
                            } else {
                                snowMap.put(position + "snowMap", map.get(holder.spLocation.getSelectedItem()));
                            }
                            checkSnowPark = true;
                            sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.btnDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Integer.parseInt(holder.tvquantityn.getText().toString()) != 0) {
                            int val = Integer.parseInt(holder.tvquantityn.getText().toString()) - 1;
                            holder.tvquantityn.setText(String.valueOf(val));
                            holder.tvtotalcost.setText(String.valueOf(Double.parseDouble(holder.tvpercost.getText().toString().replace(" INR", "")) * val));
                            try {
                                if (bunchJson.has(stringStringHashMap.get(carAdons.getTicketType().getProductId()))) {
                                    bunchJson.remove(carAdons.getTicketType().getProductId());
                                    bunchJson.put(stringStringHashMap.get(carAdons.getTicketType().getProductId()), holder.tvquantityn.getText().toString());
                                } else {
                                    carAddon.put(stringStringHashMap.get(carAdons.getTicketType().getProductId()), holder.tvquantityn.getText().toString());
                                }
                                if (total_Amount.containsKey(position + "snowMap")) {
                                    total_Amount.remove(position + "snowMap");
                                    total_Amount.put(position + "snowMap", carAdons.getTicketType().getCostPerQty() + "," + holder.tvquantityn.getText().toString());
                                } else {
                                    total_Amount.put(position + "snowMap", carAdons.getTicketType().getCostPerQty() + "," + holder.tvquantityn.getText().toString());
                                }
                                if (snowMap.containsKey(position + "snowMap")) {
                                    snowMap.remove(position + "snowMap");
                                    snowMap.put(position + "snowMap", map.get(holder.spLocation.getSelectedItem()));
                                } else {
                                    snowMap.put(position + "snowMap", map.get(holder.spLocation.getSelectedItem()));
                                }
                                if (holder.tvquantityn.getText().equals("0")) {
                                    bunchJson.remove(carAdons.getTicketType().getProductId());
                                    snowMap.clear();
                                    checkSnowPark = false;
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                } else {
                                    checkSnowPark = true;
                                    sendRefresh(total_Amount, carAddon, bus, bunchJson, BusJson, snowMap, checkBus, checkSnowPark);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        }

    }


    @Override
    public int getItemCount() {
        return SubjectValues.size();
    }

    private static void sendRefresh(HashMap<String, String> totalAmount, JSONObject carAddon, JSONObject bus, JSONObject snowJson, HashMap<String, String> busId, HashMap<String, String> snowDetails, boolean checkBus, boolean checkSnowPark) {
        Log.i("CardAddon", String.valueOf(carAddon));
        Log.i("BusAddon", String.valueOf(bus));
        Log.i("snowJson", String.valueOf(snowJson));
        Log.i("busID", String.valueOf(busId));
        Log.i("SnowDetails", snowDetails.toString());
        JSONObject jsonObject = new JSONObject();
        ArrayList<String> strings = new ArrayList<>();
        for (Object o : totalAmount.keySet()) {
            String[] value = totalAmount.get(o.toString()).split(",");
            double value2 = Double.parseDouble(value[0]) * Double.parseDouble(value[1]);
            strings.add(String.valueOf(value2));
        }
        JSONObject busdetails = new JSONObject();
        if (busId != null && busId.size() != 0) {
            for (Object o : busId.keySet()) {
                try {
                    String[] value = busId.get(o.toString()).split(",");
                    try {
                        busdetails.put("placeId", Integer.parseInt(value[0]));
                        busdetails.put("locationId", Integer.parseInt(value[1]));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (NullPointerException e) {

                }
            }
        }
        JSONObject Snowdetails = new JSONObject();
        if (snowDetails != null && snowDetails.size() != 0) {
            for (Object o : snowDetails.keySet()) {
                try {
                    String[] snow = snowDetails.get(o.toString()).split(",");
                    try {
                        Snowdetails.put("sessionId", Integer.parseInt(snow[0]));
                        Snowdetails.put("timeSlotId", Integer.parseInt(snow[1]));
                        Log.i("value", Snowdetails.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (NullPointerException e) {

                }
            }
        }
        JSONObject busproducts = new JSONObject();
        Iterator<String> iter = bus.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                Object value = bus.get(key);
                if (!value.equals("0")) {
                    busproducts.put(key, value);
                }
            } catch (JSONException e) {

            }
        }
        Intent intent = new Intent("Packages");
        intent.putExtra("updates", "1");
        intent.putExtra("type", "ticket");
        intent.putExtra("ticketProduct", jsonObject.toString());
        intent.putExtra("carAddon", carAddon.toString());
        if (!carAddon.toString().trim().equalsIgnoreCase("{}")) {
            intent.putExtra("carExit", "true");
        } else {
            intent.putExtra("carExit", "false");
        }
        intent.putExtra("busAddon", busproducts.toString());
        intent.putExtra("snowAddon", snowJson.toString());
        intent.putExtra("snowdetails", Snowdetails.toString());
        if (!String.valueOf(bus).equalsIgnoreCase("{}")) {
            intent.putExtra("busDetails", busdetails.toString());
            Log.i("busdeatil",busdetails.toString());
        }else {
            busdetails=new JSONObject();
            intent.putExtra("busDetails", busdetails.toString());
            Log.i("busdeatil",busdetails.toString());
        }
        intent.putExtra("ticketAmount", String.valueOf(totalValue(strings)));
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private static double totalValue(ArrayList<String> strings) {
        double sum = 0;
        for (int i = 0; i < strings.size(); i++) {
            sum += Double.parseDouble(strings.get(i));
        }
        return sum;
    }
}
