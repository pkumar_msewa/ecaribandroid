package in.msewa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import in.msewa.model.FoodModel;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 12/21/2016.
 */
public class FoodCartListAdapter extends RecyclerView.Adapter<FoodCartListAdapter.RecyclerViewHolders> {

    private ArrayList<FoodModel> itemList;
    private Context context;

    public FoodCartListAdapter(Context context, ArrayList<FoodModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_food_cart_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        holder.tvFoodCartTitle.setText(itemList.get(position).getpName());
        holder.tvFoodCartRetailPrice.setText(itemList.get(position).getpRetailPrice());
        holder.tvFoodCartApplicablePrice.setText(itemList.get(position).getpApplicablePrice());
        if (!itemList.get(position).getpThumb().isEmpty()) {
            AQuery aq = new AQuery(context);
            aq.id(holder.ivFoodCart).background(R.drawable.loading_image).image(itemList.get(position).getpThumb(), true, true);
        }

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvFoodCartRetailPrice, tvFoodCartApplicablePrice, tvFoodCartTitle;
        public ImageView ivFoodCart;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvFoodCartTitle = (TextView) itemView.findViewById(R.id.tvFoodCartTitle);
            tvFoodCartRetailPrice = (TextView) itemView.findViewById(R.id.tvFoodCartRetailPrice);
            tvFoodCartApplicablePrice = (TextView) itemView.findViewById(R.id.tvFoodCartApplicablePrice);
            ivFoodCart = (ImageView) itemView.findViewById(R.id.ivFoodCart);
        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();
            if (i == 0) {
            }
        }
    }
}
