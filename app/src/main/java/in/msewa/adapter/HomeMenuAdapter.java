package in.msewa.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.msewa.custom.CustomToast;
import in.msewa.fragment.fragmentgiftcard.GiftCardCatFragment;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.MainMenuModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.ecarib.activity.housejoy.HousejoyActivity;
import in.msewa.ecarib.activity.shopping.ShoppingActivity;

public class HomeMenuAdapter extends RecyclerView.Adapter<HomeMenuAdapter.RecyclerViewHolders> {

  private ArrayList<MainMenuModel> itemList;
  private Context context;

  public HomeMenuAdapter(Context context, ArrayList<MainMenuModel> itemList) {
    this.itemList = itemList;
    this.context = context;
  }

  @Override
  public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

    View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_menu, null);
    RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
    return rcv;
  }

  @Override
  public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
    holder.tvMenuItems.setText(itemList.get(position).getMenuTitle());
    holder.ivMenuItems.setImageResource(itemList.get(position).getMenuImage());
    DisplayMetrics displayMetrics = new DisplayMetrics();
    ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
  }

  @Override
  public int getItemCount() {
    return this.itemList.size();
  }

  public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tvMenuItems;
    public ImageView ivMenuItems;

    public RecyclerViewHolders(View itemView) {
      super(itemView);
      itemView.setOnClickListener(this);
      tvMenuItems = (TextView) itemView.findViewById(R.id.tvMenuItems);
      ivMenuItems = (ImageView) itemView.findViewById(R.id.ivMenuItems);
    }

    @Override
    public void onClick(View view) {
      int i = getAdapterPosition();
      Intent menuIntent = new Intent(context, MainMenuDetailActivity.class);
      if (i == 13) {
        context.startActivity(new Intent(context, GiftCardCatFragment.class));
      } else if (i == 10) {
        context.startActivity(new Intent(context, HousejoyActivity.class));
      } else if (i == 15) {
        Intent shoppingIntent = new Intent(context, ShoppingActivity.class);
        context.startActivity(shoppingIntent);
      } else {
        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, itemList.get(i).getType());
        context.startActivity(menuIntent);
      }

    }
  }
}
