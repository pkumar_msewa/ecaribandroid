//package in.msewa.adapter;
//
//import android.Manifest;
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.net.Uri;
//import android.os.Parcelable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.widget.CardView;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.Filter;
//import android.widget.Filterable;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//
//import in.msewa.model.MerchantAgent;
//import in.msewa.nearbymerchant.MapsActivity;
//import in.msewa.vpayqwik.R;
//
///**
// * Created by kashifimam on 21/02/17.
// */
//
//public class NearByMerchantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
//
//  String categoryDS;
//  private ArrayList<MerchantAgent> itemList;
//  private ArrayList<MerchantAgent> orgiNALitemList;
//  private Context context;
//  public static final int header = 0;
//  public static final int Normal = 1;
//  private int lastPosition = -1;
//
//
//  public NearByMerchantAdapter(Context context, ArrayList<MerchantAgent> itemList) {
//    this.itemList = itemList;
//    this.context = context;
//    this.orgiNALitemList = itemList;
//  }
//
//  @Override
//  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//    View v = LayoutInflater.from(parent.getContext())
//      .inflate(R.layout.adapter_merchant_card, parent, false);
//
//
//    return new RecyclerViewHolders(v);
//  }
//
//  @SuppressLint("DefaultLocale")
//  @Override
//  public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
//    ((RecyclerViewHolders) holder).tvDescription.setText(String.valueOf(itemList.get(position).getAddress()));
//    ((RecyclerViewHolders) holder).shopTittle.setText(String.valueOf(itemList.get(position).getMerchantName()));
//    Double aDouble = Double.parseDouble(itemList.get(position).getKms());
//    DecimalFormat format = new DecimalFormat("#.##");
//    ((RecyclerViewHolders) holder).btnKMS.setText(" " + format.format(aDouble.doubleValue()).concat("KM"));
//
//
//    ((RecyclerViewHolders) holder).btnKMS.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        MerchantAgent merchantAgent = itemList.get(position);
//        context.startActivity(new Intent(context, MapsActivity.class).putExtra("location", (Parcelable) merchantAgent));
////        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?q=loc:" + itemList.get(position).getLatitude() + "," + itemList.get(position).getLongitude()));
////        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // Only if initiating from a Broadcast Receiver
////        String mapsPackageName = "com.google.android.apps.maps";
////        if (Utility.isPackageInstalled(context, mapsPackageName)) {
////          i.setClassName(mapsPackageName, "com.google.android.maps.MapsActivity");
////          i.setPackage(mapsPackageName);
////          context.startActivity(i);
////        }
//      }
//    });
//    ((RecyclerViewHolders) holder).сardView.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        String phone = "+918025011300";
//        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
//        context.startActivity(intent);
//      }
//    });
//
//  }
//
//
//  @Override
//  public int getItemCount() {
//    return this.itemList.size();
//  }
//
//  @Override
//  public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
//    super.onViewDetachedFromWindow(holder);
//    holder.itemView.clearAnimation();
//  }
//
//  public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//    public Button btnKMS;
//    private LinearLayout llGiftCard;
//    public TextView tvDescription, shopTittle;
//    Button сardView;
//
//
//    public RecyclerViewHolders(View itemView) {
//      super(itemView);
//      tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
//      btnKMS = (Button) itemView.findViewById(R.id.btnKMS);
//      shopTittle = (TextView) itemView.findViewById(R.id.shopTittle);
//      llGiftCard = (LinearLayout) itemView.findViewById(R.id.llGiftCard);
//      сardView = (Button) itemView.findViewById(R.id.Call);
//      llGiftCard.setOnClickListener(this);
//
//    }
//
//    @Override
//    public void onClick(View view) {
//      int i = getAdapterPosition();
//      if (view.getId() == R.id.llGiftCard) {
////        Intent giftCardItemIntent = new Intent(context, GiftCardListActivity.class);
////        giftCardItemIntent.putParcelableArrayListExtra("Model", itemList.get(i));
////        context.startActivity(giftCardItemIntent);
//      }
//    }
//  }
//
//
//  @Override
//  public Filter getFilter() {
//    return new Filter() {
//      @Override
//      protected FilterResults performFiltering(CharSequence charSequence) {
//        String charString = charSequence.toString();
//        if (charString.isEmpty()) {
//          itemList = orgiNALitemList;
//        } else {
//          ArrayList<MerchantAgent> filteredList = new ArrayList<>();
//          for (MerchantAgent row : orgiNALitemList) {
//
//            // name match condition. this might differ depending on your requirement
//            // here we are looking for name or phone number match
//            if (row.getMerchantName().toLowerCase().contains(charString.toLowerCase())) {
//              filteredList.add(row);
//            }
//          }
//
//          itemList = filteredList;
//        }
//
//        FilterResults filterResults = new FilterResults();
//        filterResults.values = itemList;
//        return filterResults;
//      }
//
//      @Override
//      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//        itemList = (ArrayList<MerchantAgent>) filterResults.values;
//        // refresh the list with filtered data
//        notifyDataSetChanged();
//      }
//    };
//  }
//
//
//}
//
