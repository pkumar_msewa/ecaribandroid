package in.msewa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import in.msewa.model.BillPaymentOperatorsModel;
import in.msewa.model.OperatorsModel;


public class OperatorSpinnerBillpaymentAdapter extends ArrayAdapter<BillPaymentOperatorsModel>{

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<BillPaymentOperatorsModel> values;

    public OperatorSpinnerBillpaymentAdapter(Context context, int textViewResourceId, List<BillPaymentOperatorsModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.size();
    }


    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setPadding(16,8,8,8);
        label.setTextSize(16);
        label.setText(values.get(position).getName());
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setPadding(16,16,16,16);
        label.setTextSize(16);
        label.setText(values.get(position).getName());
        return label;
    }
}



