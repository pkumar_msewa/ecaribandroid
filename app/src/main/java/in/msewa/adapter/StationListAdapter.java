package in.msewa.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import in.msewa.model.TravelKhanaStationListModel;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class StationListAdapter extends BaseAdapter {

    private Context context;
    private List<TravelKhanaStationListModel> stationArray;
    private ViewHolder viewHolder;
    private long destinationCode, sourceCode;
    private String dateOfDep;
    Date arrTime, depTime = null;



    public StationListAdapter(Context context, List<TravelKhanaStationListModel> stationArray) {
        this.context = context;
        this.stationArray = stationArray;
    }

  @Override
  public int getCount() {
    return stationArray.size();
  }

  @Override
  public Object getItem(int index) {
    return stationArray.get(index);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    final int currentPosition = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_station_list, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
            viewHolder.cvItem = (CardView) convertView.findViewById(R.id.cvItem);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvTime.setText(stationArray.get(position).getArrivalTime());
        viewHolder.tvName.setText(stationArray.get(position).getStationName()+" ("+stationArray.get(position).getStationCode()+")");
        viewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String time = stationArray.get(currentPosition).getArrivalTime();
            String stationCode =  stationArray.get(currentPosition).getStationCode();
            String date =  stationArray.get(currentPosition).getDateAtStation();
            String stationName =  stationArray.get(currentPosition).getStationName();
                sendStationDetails(time,stationCode,date,stationName);
                ((Activity) context).finish();
            }
        });

        return convertView;
    }

    static class ViewHolder {
        private TextView tvName;
        private TextView tvTime;
        private CardView cvItem;
    }

    private void sendStationDetails(String time, String stationCode, String date, String stationName) {
        Intent intent = new Intent("select-station-done");
        intent.putExtra("time", time);
        intent.putExtra("stationCode", stationCode);
        intent.putExtra("stationName", stationName);
        intent.putExtra("date", date);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
