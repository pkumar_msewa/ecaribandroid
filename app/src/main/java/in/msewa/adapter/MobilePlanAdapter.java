package in.msewa.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import in.msewa.model.MobilePlansModel;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 3/16/2016.
 */
public class MobilePlanAdapter  extends BaseAdapter {
    private ViewHolder viewHolder;
    private Context context;
    private int post;
    private ArrayList<MobilePlansModel> mobilePlansList;

    public MobilePlanAdapter(Context context, ArrayList<MobilePlansModel> mobilePlansList, int post) {
        this.context = context;
        this.mobilePlansList = mobilePlansList;
        this.post = post;
    }

    @Override
    public int getCount() {
        return mobilePlansList.size();
    }

    @Override
    public Object getItem(int position) {
        return mobilePlansList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_mobile_plans, null);
            viewHolder = new ViewHolder();
            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.tvMobilePlansDescription);
            viewHolder.tvTalktime = (TextView) convertView.findViewById(R.id.tvMobilePlansTalkTime);
            viewHolder.tvValidity = (TextView) convertView.findViewById(R.id.tvMobilePlansValidity);
            viewHolder.btnRecharge = (Button) convertView.findViewById(R.id.btnMobilePlansSelect);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvTalktime.setText(mobilePlansList.get(position).getTalkTime());
        viewHolder.tvDescription.setText(mobilePlansList.get(position).getDescription());

        if (mobilePlansList.get(position).getValidity().equals("1") || mobilePlansList.get(position).getValidity().equals("0")) {
            viewHolder.tvValidity.setText("Validity " + mobilePlansList.get(position).getValidity() + " day");
        } else {
            viewHolder.tvValidity.setText("Validity " + mobilePlansList.get(position).getValidity() + " days");
        }

        viewHolder.btnRecharge.setText("Rs " + mobilePlansList.get(position).getAmount());
        viewHolder.btnRecharge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String selectedAmount = mobilePlansList.get(currentPosition).getAmount().toString();
                String operatorCode = mobilePlansList.get(currentPosition).getOperatorCode();
                Intent resultIntent = new Intent();
                resultIntent.putExtra("amount", selectedAmount);
                ((Activity) context).setResult(Activity.RESULT_OK, resultIntent);
                ((Activity) context).finish();
            }

        });
        return convertView;
    }

    static class ViewHolder {
        Button btnRecharge;
        TextView tvTalktime, tvDescription, tvValidity/*, tvAmount*/;
    }
}

