package in.msewa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.msewa.model.GiftCardCatModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.giftcardactivity.GiftCardListActivity;

/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardCatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

  String categoryDS;
  private List<GiftCardCatModel> itemList;
  private List<GiftCardCatModel> orgiNALitemList;
  private Context context;
  public static final int header = 0;
  public static final int Normal = 1;
  private int lastPosition = -1;


  public GiftCardCatAdapter(Context context, List<GiftCardCatModel> itemList) {
    super();
    this.itemList = itemList;
    this.context = context;
    this.orgiNALitemList = itemList;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
      .inflate(R.layout.adapter_gift_card_cat, parent, false);


    return new RecyclerViewHolders(v);
  }

  @Override
  public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
    if (holder instanceof RecyclerViewHolders) {
      if (itemList.get(position).getBaseImage() != null && !itemList.get(position).getBaseImage().isEmpty()) {
        Picasso.with(context)
          .load(itemList.get(position).getBaseImage())
          .error(R.drawable.no_image_available)
          .placeholder(R.drawable.ic_loading_image)
          .into(((RecyclerViewHolders) holder).ivGiftCardCat);
      } else {
        Picasso.with(context)
          .load("fljsak")
          .error(R.drawable.no_image_available)
          .placeholder(R.drawable.ic_loading_image)
          .into(((RecyclerViewHolders) holder).ivGiftCardCat);
      }

      ((RecyclerViewHolders) holder).tvDescription.setText(String.valueOf(itemList.get(position).getBrand_name()));

      Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
      holder.itemView.startAnimation(animation);
      lastPosition = position;
    }


  }


  @Override
  public int getItemCount() {
    return this.itemList.size();
  }

  @Override
  public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
    super.onViewDetachedFromWindow(holder);
    holder.itemView.clearAnimation();
  }

  public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView ivGiftCardCat;
    public Button btnTerms;
    private LinearLayout llGiftCard;
    public TextView tvDescription, tvTerms;


    public RecyclerViewHolders(View itemView) {
      super(itemView);
      ivGiftCardCat = (ImageView) itemView.findViewById(R.id.ivGiftCardCat);
      tvDescription = (TextView) itemView.findViewById(R.id.tvTittle);
      llGiftCard = (LinearLayout) itemView.findViewById(R.id.llGiftCard);
      llGiftCard.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
      int i = getAdapterPosition();
      if (view.getId() == R.id.llGiftCard) {
        Intent giftCardItemIntent = new Intent(context, GiftCardListActivity.class);
        giftCardItemIntent.putExtra("Model", itemList.get(i));
        context.startActivity(giftCardItemIntent);
      }
    }
  }


  @Override
  public Filter getFilter() {
    return new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence charSequence) {
        String charString = charSequence.toString();
        if (charString.isEmpty()) {
          itemList = orgiNALitemList;
        } else {
          List<GiftCardCatModel> filteredList = new ArrayList<>();
          for (GiftCardCatModel row : orgiNALitemList) {

            // name match condition. this might differ depending on your requirement
            // here we are looking for name or phone number match
            if (row.getBrand_name().toLowerCase().contains(charString.toLowerCase())) {
              filteredList.add(row);
            }
          }

          itemList = filteredList;
        }

        FilterResults filterResults = new FilterResults();
        filterResults.values = itemList;
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        itemList = (ArrayList<GiftCardCatModel>) filterResults.values;
        // refresh the list with filtered data
        notifyDataSetChanged();
      }
    };
  }


}

