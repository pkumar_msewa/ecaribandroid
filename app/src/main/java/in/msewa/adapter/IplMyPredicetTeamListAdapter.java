package in.msewa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import in.msewa.custom.CustomTermAlertDialog;
import in.msewa.model.IplMyPredicationTeamModel;
import in.msewa.ecarib.R;


public class IplMyPredicetTeamListAdapter extends RecyclerView.Adapter<IplMyPredicetTeamListAdapter.RecyclerViewHolders> {

  private List<IplMyPredicationTeamModel> itemList;
  private Context context;


  public IplMyPredicetTeamListAdapter(Context context, List<IplMyPredicationTeamModel> itemList) {
    this.itemList = itemList;
    this.context = context;
  }

  @Override
  public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

    View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ipl_team_predicet_list, null);
    return new RecyclerViewHolders(layoutView);
  }

  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
  @Override
  public void onBindViewHolder(final RecyclerViewHolders holder, @SuppressLint("RecyclerView") final int position) {

    Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Myriad-Pro-Semibold.ttf");
    Typeface typeface1 = Typeface.create(typeface, Typeface.BOLD);
    holder.iplVs.setText("VS");
    holder.ivTeam1name.setText(itemList.get(position).getFirstTeam());
    holder.ivTeam1name.setTypeface(typeface1);
    holder.ivTeam2name.setTypeface(typeface1);
    holder.iplVs.setTypeface(typeface1);
    holder.ivTeam2name.setText(itemList.get(position).getSecondTeam());
    SimpleDateFormat  simpleDateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
    try {
      Log.i("times",itemList.get(position).getMatchDate());
      Date time = simpleDateFormat.parse(itemList.get(position).getMatchDate());
      SimpleDateFormat  date = new SimpleDateFormat("dd-MM-yyyy");
      SimpleDateFormat  timeHour = new SimpleDateFormat("hh:mm aa");
      holder.time.setText("Start on : " + timeHour.format(time));
      holder.timeDate.setText("Match Date : " + date.format(time));

    } catch (ParseException e) {
      e.printStackTrace();
    }
    if (!itemList.get(position).getWinningResult()) {
      holder.cvResult.setCardBackgroundColor(context.getResources().getColor(R.color.mark_yellow));
      holder.tvMatchInfo.setTextColor(context.getResources().getColor(R.color.menu6));
    } else {
      holder.cvResult.setCardBackgroundColor(context.getResources().getColor(R.color.menu4));
      holder.tvMatchInfo.setTextColor(context.getResources().getColor(R.color.white_text));
    }

    holder.tvMatchInfo.setText(itemList.get(position).getAdditionalInfo());
    if ((TextUtils.isEmpty(itemList.get(position).getFirstTeamLogo()))) {
      Picasso.with(context).load("new").placeholder(R.drawable.no_image_available).into(holder.ivFirstTeam);
    } else {
      Picasso.with(context).load(itemList.get(position).getFirstTeamLogo()).placeholder(R.drawable.ic_loading_image).into(holder.ivFirstTeam);
    }
    if ((TextUtils.isEmpty(itemList.get(position).getSecondTeamLogo()))) {
      Picasso.with(context).load("new").placeholder(R.drawable.ic_loading_image).into(holder.ivSecondTeam);
    } else {
      Picasso.with(context).load(itemList.get(position).getSecondTeamLogo()).placeholder(R.drawable.ic_loading_image).into(holder.ivSecondTeam);
    }
//    holder.tvMatchInfo.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.ic_keyboard_arrow_right_black_24dp), null);
//    holder.cvIplTeamList.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        Intent intent = new Intent(context, MainMenuDetailActivity.class);
//        intent.putExtra(AppMetadata.FRAGMENT_TYPE, "IPLPredication");
//        intent.putExtra("IplDetails", itemList.get(position));
//        context.startActivity(intent);
//      }
//    });
  }


  @Override
  public int getItemCount() {
    return this.itemList.size();
  }

  public class RecyclerViewHolders extends RecyclerView.ViewHolder {
    public TextView tvTeam1name, tvTeam2name, tvMatchInfo;
    CardView cvIplTeamList, cvResult;
    TextView iplVs, ivTeam1name, ivTeam2name, time, timeDate;
    ImageView ivFirstTeam, ivSecondTeam;

    public RecyclerViewHolders(View itemView) {
      super(itemView);
      cvIplTeamList = (CardView) itemView.findViewById(R.id.cvIplTeamList);
      tvMatchInfo = (TextView) itemView.findViewById(R.id.tvMatchInfo);
      ivTeam1name = (TextView) itemView.findViewById(R.id.ivTeam1name);
      ivTeam2name = (TextView) itemView.findViewById(R.id.ivTeam2name);
      ivSecondTeam = (ImageView) itemView.findViewById(R.id.ivSecondTeam);
      ivFirstTeam = (ImageView) itemView.findViewById(R.id.ivFirstTeam);
      timeDate = (TextView) itemView.findViewById(R.id.timeDate);
      time = (TextView) itemView.findViewById(R.id.time);
      cvResult = (CardView) itemView.findViewById(R.id.cvResult);
      iplVs = (TextView) itemView.findViewById(R.id.iplVs);

    }

  }

  public void showSettingsAlert(String terms) {
    CustomTermAlertDialog customTermAlertDialog = new CustomTermAlertDialog(context, "Terms & Conditions", terms);
    customTermAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();

      }
    });

    customTermAlertDialog.show();
  }

  private void enableDisableView(View view, boolean enabled) {
    view.setEnabled(enabled);

    if (view instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) view;

      for (int idx = 0; idx < group.getChildCount(); idx++) {
        enableDisableView(group.getChildAt(idx), enabled);
      }
    }
  }
}
