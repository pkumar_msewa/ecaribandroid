package in.msewa.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.List;

public class filtterAdapter extends ArrayAdapter<String> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<String> values;
    private String type;

    public filtterAdapter(Context context, int textViewResourceId, List<String> values, String s) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
        this.type = s;
    }

    public int getCount() {
        return values.size();
    }


    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item

        if (type.equals("product")) {
            TextView label = new TextView(context);

            label.setTextColor(Color.BLACK);
            label.setPadding(20, 16, 16, 16);
            label.setEms(8);
            label.setTextSize(20);
            label.setGravity(Gravity.CENTER);
            label.setText(values.get(position));

            // And finally return your dynamic (or custom) view for each spinner item
            return label;
        } else {
            CheckedTextView checkBox = new CheckedTextView(context);
            checkBox.setText(values.get(position));
            checkBox.setTextColor(Color.BLACK);
            checkBox.setPadding(16, 16, 16, 16);
            checkBox.setGravity(Gravity.CENTER_VERTICAL);
            checkBox.setCheckMarkDrawable(android.R.drawable.checkbox_off_background);
            checkBox.setTextSize(20);
            return checkBox;
        }
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        return convertView;
    }
}



