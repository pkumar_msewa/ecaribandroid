package in.msewa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.msewa.model.BillPaymentOperatorsModel;
import in.msewa.model.BusCityModel;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 10/6/2016.
 */
public class ElectryFilterAdapter extends BaseAdapter implements Filterable {

  private Context context;
  private List<BillPaymentOperatorsModel> flightCityArray;
  private List<BillPaymentOperatorsModel> filteredData;
  private ViewHolder viewHolder;

  public ElectryFilterAdapter(Context context, List<BillPaymentOperatorsModel> flightCityArray) {
    this.context = context;
    this.flightCityArray = flightCityArray;
    this.filteredData = flightCityArray;
  }

  @Override
  public int getCount() {
    if (flightCityArray == null) {
      return 0;
    }
    return flightCityArray.size();
  }

  @Override
  public Object getItem(int index) {
    return flightCityArray.get(index);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      convertView = LayoutInflater.from(context).inflate(R.layout.gas_item_list, parent, false);
      viewHolder = new ViewHolder();
      viewHolder.tvTittle = (TextView) convertView.findViewById(R.id.tvTittle);
      viewHolder.ivOperator = (ImageView) convertView.findViewById(R.id.ivOperator);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }

    viewHolder.tvTittle.setText(flightCityArray.get(position).getName());

    Picasso.with(context).load(flightCityArray.get(position).getImages()).placeholder(R.drawable.ic_loading_image).error(R.drawable.no_image_available).into(viewHolder.ivOperator);

    return convertView;
  }

  @Override
  public Filter getFilter() {
    return new ItemFilter();
  }


  static class ViewHolder {
    TextView tvTittle;
    ImageView ivOperator;
  }


  private class ItemFilter extends Filter {
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

      String filterString = constraint.toString().toLowerCase();

      FilterResults results = new FilterResults();

      final List<BillPaymentOperatorsModel> list = filteredData;

      int count = list.size();
      final ArrayList<Object> nlist = new ArrayList<Object>(count);

      String filterableString, filterableStringcode, filterableStringAreo;

      for (int i = 0; i < count; i++) {
        filterableString = list.get(i).getName();

        if (filterableString.toLowerCase().contains(filterString)) {
          nlist.add(list.get(i));
        }
      }
      results.values = nlist;
      results.count = nlist.size();
      return results;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
      flightCityArray = (ArrayList<BillPaymentOperatorsModel>) results.values;
      notifyDataSetChanged();
    }

  }
}

