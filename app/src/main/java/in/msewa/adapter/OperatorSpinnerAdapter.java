package in.msewa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import in.msewa.model.OperatorsModel;


public class OperatorSpinnerAdapter extends ArrayAdapter<OperatorsModel> {

  // Your sent context
  private Context context;
  // Your custom values for the spinner (User)
  private List<OperatorsModel> values;
  private int textViewResourceId;

  public OperatorSpinnerAdapter(Context context, int textViewResourceId, List<OperatorsModel> values, String topup) {
    super(context, textViewResourceId, values);
    this.context = context;
    this.values = values;
    this.textViewResourceId = textViewResourceId;
  }

  public int getCount() {
    return values.size();
  }


  public long getItemId(int position) {
    return position;
  }


  // And the "magic" goes here
  // This is for the "passive" state of the spinner
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    TextView label = new TextView(context);
    label.setTextColor(Color.BLACK);
    label.setPadding(16, 8, 8, 8);
    label.setTextSize(16);
    label.setText(values.get(position).getName());
    return label;


//    View row = convertView;
//    OperatorGasSpinnerAdapter.SimpleHolder viewHolder;
//    if (row == null) {
//      LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//      row = inflater.inflate(textViewResourceId, parent, false);
//      viewHolder = new OperatorGasSpinnerAdapter.SimpleHolder();
//      viewHolder.ivOperator = (ImageView) row.findViewById(R.id.ivOperator);
//      viewHolder.tvTittle = (TextView) row.findViewById(R.id.tvTittle);
//      row.setTag(viewHolder);
//    } else {
//      viewHolder = (OperatorGasSpinnerAdapter.SimpleHolder) row.getTag();
//    }
//    OperatorsModel gas = getItem(position);
//    if (position != 0) {
//      Log.i("iokmage",ApiUrl.URL_DOMAIN_ + "resources/images/topup/" + gas.getCode().substring(1) + ".png");
//      Picasso.with(context).load(ApiUrl.URL_DOMAIN_ + "resources/images/topup/" + gas.getCode().substring(1) + ".png").into(viewHolder.ivOperator);
//    }
//    viewHolder.tvTittle.setText(gas.getName());
//
//    return row;
  }

  // And here is when the "chooser" is popped up
  // Normally is the same view, but you can customize it if you want
  @Override
  public View getDropDownView(int position, View convertView, ViewGroup parent) {
//    View row = convertView;
//    OperatorGasSpinnerAdapter.SimpleHolder viewHolder;
//    if (row == null) {
//      LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//      row = inflater.inflate(textViewResourceId, parent, false);
//      viewHolder = new OperatorGasSpinnerAdapter.SimpleHolder();
//      viewHolder.ivOperator = (ImageView) row.findViewById(R.id.ivOperator);
//      viewHolder.tvTittle = (TextView) row.findViewById(R.id.tvTittle);
//      row.setTag(viewHolder);
//    } else {
//      viewHolder = (OperatorGasSpinnerAdapter.SimpleHolder) row.getTag();
//    }
//    OperatorsModel gas = getItem(position);
//    viewHolder.ivOperator.setVisibility(View.GONE);
////    if (position != 0) {
////      Log.i("iokmage",ApiUrl.URL_DOMAIN_ + "resources/images/topup/" + gas.getCode().substring(1) + ".png");
////      Picasso.with(context).load(ApiUrl.URL_DOMAIN_ + "resources/images/topup/" + gas.getCode().substring(1) + ".png").into(viewHolder.ivOperator);
////    }
//    viewHolder.tvTittle.setText(gas.getName());
//
//    return row;
    TextView label = new TextView(context);
    label.setTextColor(Color.BLACK);
    label.setPadding(16, 8, 8, 8);
    label.setTextSize(16);
    label.setText(values.get(position).getName());
    return label;
  }
}



