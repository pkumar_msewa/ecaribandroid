package in.msewa.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import in.msewa.model.ListIssueModel;
import in.msewa.ecarib.R;


/**
 * Created by acer on 24-06-2017.
 */

public class OpenIssuesAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;

    private List<ListIssueModel> statementList;
    TextView tvError;
    ListView lvCustomer;

    public OpenIssuesAdapter(Context context, List<ListIssueModel> statementList, TextView tvError, ListView lvCustomer) {
        this.context = context;
        this.lvCustomer = lvCustomer;
        this.tvError = tvError;
        this.statementList = statementList;
    }


    public int getCount() {
        return statementList.size();
    }


    public Object getItem(int position) {
        return statementList.get(position);
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_openissue_list, null);
            viewHolder = new OpenIssuesAdapter.ViewHolder();

            //viewHolder.tvId = (TextView) convertView.findViewById(R.id.tvId);
            viewHolder.tvTicketNo = (TextView) convertView.findViewById(R.id.tvTicketNo);
            //viewHolder.tvNew = (TextView) convertView.findViewById(R.id.tvNew);
            // viewHolder.tvDetails = (TextView) convertView.findViewById(R.id.tvDetails);
            viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
            viewHolder.tvTicketNum = (TextView) convertView.findViewById(R.id.tvTicketNum);
            viewHolder.tvcomponent = (TextView) convertView.findViewById(R.id.tvcomponent);
            viewHolder.tvdate = (TextView) convertView.findViewById(R.id.tvdate);
            viewHolder.llvas = (LinearLayout) convertView.findViewById(R.id.llvas);

            //viewHolder.tvNews = (TextView) convertView.findViewById(R.id.tvNews);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();

        }
//        if (statementList.get(position).getStatus().equalsIgnoreCase("Open")) {
//            tvError.setVisibility(View.GONE);
//            lvCustomer.setVisibility(View.VISIBLE);
            viewHolder.tvTicketNo.setText("Ref" + "No.: " + statementList.get(position).getTicketNo());
            viewHolder.tvStatus.setText(statementList.get(position).getStatus());
            viewHolder.tvdate.setText(getDate(statementList.get(position).getDate()));
            viewHolder.tvcomponent.setText(statementList.get(position).getComponent());
            notifyDataSetChanged();
//        }
//        else {
//            tv  Error.setVisibility(View.VISIBLE);
//            lvCustomer.setVisibility(View.GONE);
//            notifyDataSetChanged();
//        }

        return convertView;
    }


    static class ViewHolder {
        TextView tvId, tvTicketNo, tvNew, tvDetails, tvStatus;
        TextView tvTicketNum, tvcomponent, tvdate, tvNews;
        LinearLayout llvas;

    }

    private CharSequence getDate(long timeStamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }

    }
}
