package in.msewa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.msewa.util.AddRemoveCartListner;
import in.msewa.ecarib.R;

public class AmountAdapter extends RecyclerView.Adapter<AmountAdapter.ViewHolder> {

  private String[] alName;
  ArrayList<Integer> alImage;
  Context context;
  private AddRemoveCartListner addRemoveCartListner;
  int row_index = 0;
  private final ArrayList<Integer> seleccionados = new ArrayList<>();

  public AmountAdapter(Context context, String[] alName, AddRemoveCartListner addRemoveCartListner) {
    super();
    this.context = context;
    this.alName = alName;
    this.addRemoveCartListner = addRemoveCartListner;

  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
    View v = LayoutInflater.from(viewGroup.getContext())
      .inflate(R.layout.grid_item, viewGroup, false);
    ViewHolder viewHolder = new ViewHolder(v);
    return viewHolder;
  }

  @Override

  public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
//        if (!seleccionados.contains(i)) {
//            viewHolder.tvSpecies.setBackgroundColor(Color.LTGRAY);
//        } else {
//            viewHolder.tvSpecies.setBackgroundColor(Color.BLUE);
//        }
    viewHolder.tvSpecies.setText(context.getResources().getString(R.string.rupease) + "" + String.valueOf(alName[i]));
    if (row_index == i) {
      viewHolder.tvSpecies.setBackgroundColor(Color.parseColor("#08bbc7"));
      viewHolder.tvSpecies.setTextColor(Color.WHITE);
      addRemoveCartListner.taskCompleted(alName[i]);
    } else {
      viewHolder.tvSpecies.setBackgroundColor(Color.parseColor("#ffffff"));
      viewHolder.tvSpecies.setTextColor(Color.BLACK);
    }
    viewHolder.tvSpecies.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        row_index = i;
        notifyDataSetChanged();
        addRemoveCartListner.taskCompleted(alName[i]);

      }
    });
  }

  @Override
  public int getItemCount() {
    return alName.length;
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    public TextView tvSpecies;

    public ViewHolder(View itemView) {
      super(itemView);
      tvSpecies = (TextView) itemView.findViewById(R.id.tv_species);

    }

  }

}
