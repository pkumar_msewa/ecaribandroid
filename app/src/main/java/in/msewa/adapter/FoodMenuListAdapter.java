package in.msewa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import in.msewa.model.TravelKhanaMenuModel;
import in.msewa.util.AddRemoveMenuListner;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class FoodMenuListAdapter extends BaseAdapter {

    private Context context;
    private List<TravelKhanaMenuModel> menuArray;

    private HashMap<Long, ArrayList<TravelKhanaMenuModel>> menuMap;
    private ArrayList<TravelKhanaMenuModel> selectedMenuList;
    private AddRemoveMenuListner addRemoveMenuListner;

    private long destinationCode, sourceCode;
    private String dateOfDep;
    Date arrTime, depTime = null;


    public FoodMenuListAdapter(Context context, List<TravelKhanaMenuModel> busArray, AddRemoveMenuListner addRemoveMenuListner) {
        this.context = context;
        this.menuArray = busArray;
        this.addRemoveMenuListner = addRemoveMenuListner;
        menuMap = new HashMap<>();
        selectedMenuList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return menuArray.size();
    }

    @Override
    public Object getItem(int index) {
        return menuArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final int currentPosition = position;
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_food_menu_list, null);
            viewHolder = new ViewHolder();
            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
            viewHolder.ivImage = (ImageView) convertView.findViewById(R.id.ivImage);
            viewHolder.btnAddRemove = (Button) convertView.findViewById(R.id.btnAddRemove);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        AQuery aq = new AQuery(context);
        if (menuArray.get(position).getImage() != null && !menuArray.get(position).getImage().isEmpty()) {
            aq.id(viewHolder.ivImage).background(R.drawable.loading_image).image(menuArray.get(position).getImage(), true, true);
        }

        if (menuArray.get(position).getMenuTag().equals("Veg")) {
            viewHolder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_veg, 0);
        } else {
            viewHolder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_non_veg, 0);
        }
        viewHolder.tvPrice.setText(String.valueOf(menuArray.get(position).getCustomerPayable()));
        viewHolder.tvName.setText(menuArray.get(position).getItemName());
        if(menuArray.get(position).getDescription()==null || menuArray.get(position).getDescription().equalsIgnoreCase("null")){
            viewHolder.tvDescription.setVisibility(View.GONE);
        }else {
          viewHolder.tvDescription.setVisibility(View.VISIBLE);
            viewHolder.tvDescription.setText(menuArray.get(position).getDescription());
        }
        viewHolder.btnAddRemove.setText(menuArray.get(position).getLabel());

        viewHolder.btnAddRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuMap.containsKey(menuArray.get(position).getItemId())) {
                    menuMap.remove(menuArray.get(position).getItemId());
                    selectedMenuList.remove(menuArray.get(position));
                    addRemoveMenuListner.taskCompleted(selectedMenuList);
                    menuArray.get(position).setLabel("Add");
                } else {
                    selectedMenuList.add(menuArray.get(position));
                    menuMap.put(menuArray.get(position).getItemId(), selectedMenuList);
                    addRemoveMenuListner.taskCompleted(selectedMenuList);
                    menuArray.get(position).setLabel("Remove");
                }
                notifyDataSetChanged();
//                for(Map.Entry<Long,ArrayList<TravelKhanaMenuModel>> map : menuMap.entrySet()){
//                    arList.add(map.getValue());
////                }
            }
        });

        return convertView;
    }

    private void changeName(final int position, final Button currentView, final String label){
       currentView.setText(String.valueOf(menuArray.get(position).getQuantity()));
        notifyDataSetChanged();
    }

    static class ViewHolder {
        private TextView tvName;
        private TextView tvDescription;
        private TextView tvPrice;
        private ImageView ivImage;
        private Button btnAddRemove;
    }

}
