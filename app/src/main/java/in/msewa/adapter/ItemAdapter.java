package in.msewa.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import in.msewa.model.contract;
import in.msewa.ecarib.R;

public class ItemAdapter extends ArrayAdapter<contract> implements Filterable {

    // declaring our ArrayList of items
    private ArrayList<contract> objects;
    private Context context;
    private ItemFilter mFilter = new ItemFilter();

    /* here we must override the constructor for ArrayAdapter
    * the only variable we care about now is ArrayList<Item> objects,
    * because it is the list of objects we want to display.
    */
    public ItemAdapter(Context context, int textViewResourceId, ArrayList<contract> objects) {
        super(context, textViewResourceId, objects);
        this.objects = objects;
        this.context = context;
    }

    /*
     * we are overriding the getView method here - this is what defines how each
     * list item will look.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        CheckedTextView label = new CheckedTextView(context);

        label.setTextColor(Color.BLACK);
        label.setPadding(16, 8, 8, 8);
        label.setTextSize(16);
        label.setText(String.valueOf(objects.get(position).getPcName()));

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        CheckedTextView label = new CheckedTextView(context);
        label.setTextColor(Color.BLACK);
        label.setPadding(16, 8, 8, 8);
        label.setTextSize(16);
        label.setCheckMarkTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.text_dark)));
        label.setText(String.valueOf(objects.get(position).getPcName()));
        return label;
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return mFilter;
    }

//    Filter myFilter = new Filter() {
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            FilterResults filterResults = new FilterResults();
//            ArrayList<contract> tempList = new ArrayList<contract>();
//            //constraint is the result from text you want to filter against.
//            //objects is your data set you will filter from
//            if (constraint != null && objects != null) {
//                int length = objects.size();
//                int i = 0;
//                while (i < length) {
//
//                    contract item = objects.get(i);
//                    Log.i("Length",item.getPcName());
//                    //do whatever you wanna do here
//                    //adding result set output array
//
//                    tempList.add(item);
//
//                    i++;
//                }
//                //following two lines is very important
//                //as publish result can only take FilterResults objects
//                filterResults.values = tempList;
//                filterResults.count = tempList.size();
//            }
//            return filterResults;
//        }
//
//        @SuppressWarnings("unchecked")
//        @Override
//        protected void publishResults(CharSequence contraint, FilterResults results) {
//            objects = (ArrayList<contract>) results.values;
//            if (results.count > 0) {
//                notifyDataSetChanged();
//            } else {
//                notifyDataSetInvalidated();
//            }
//        }
//    };


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<contract> list = objects;

            int count = list.size();
            final ArrayList<contract> nlist = new ArrayList<>(count);

            contract filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i);
                if (filterableString.getPcName().toLowerCase().contains(filterString)) {
                    nlist.add(filterableString);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                objects = (ArrayList<contract>) results.values;
                notifyDataSetChanged();
            }
        }

    }
//
}