package in.msewa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.msewa.model.GasModel;
import in.msewa.model.OperatorsModel;
import in.msewa.ecarib.R;


public class OperatorGasSpinnerAdapter extends ArrayAdapter<GasModel>{

  private final int textViewResourceId;
  // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<GasModel> values;

    public OperatorGasSpinnerAdapter(Context context, int textViewResourceId, List<GasModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
        this.textViewResourceId=textViewResourceId;
    }

    public int getCount(){
        return values.size();
    }


    public long getItemId(int position){
        return position;
    }

  static class SimpleHolder {
    ImageView ivOperator;
    TextView tvTittle;
  }
    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

      View row = convertView;
      SimpleHolder viewHolder;
      if (row == null) {
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(textViewResourceId, parent, false);
        viewHolder = new SimpleHolder();
        viewHolder.ivOperator = (ImageView) row.findViewById(R.id.ivOperator);
        viewHolder.tvTittle = (TextView) row.findViewById(R.id.tvTittle);
        row.setTag(viewHolder);
      } else {
        viewHolder = (SimpleHolder) row.getTag();
      }
      GasModel gas = getItem(position);
      Picasso.with(context).load(gas.getGasImages()).into(viewHolder.ivOperator);
      viewHolder.tvTittle.setText(gas.getOperatorName());

      return row;
//        TextView label = new TextView(context);
//        label.setTextColor(Color.BLACK);
//        label.setPadding(16,8,8,8);
//        label.setTextSize(16);
//        label.setText(values.get(position).getOperatorName());
//        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
      View row = convertView;
      SimpleHolder viewHolder;
      if (row == null) {
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(textViewResourceId, parent, false);
        viewHolder = new SimpleHolder();
        viewHolder.ivOperator = (ImageView) row.findViewById(R.id.ivOperator);
        viewHolder.tvTittle = (TextView) row.findViewById(R.id.tvTittle);
        row.setTag(viewHolder);
      } else {
        viewHolder = (SimpleHolder) row.getTag();
      }
      GasModel gas = getItem(position);
      Picasso.with(context).load(gas.getGasImages()).into(viewHolder.ivOperator);
      viewHolder.tvTittle.setText(gas.getOperatorName());

      return row;
    }
}



