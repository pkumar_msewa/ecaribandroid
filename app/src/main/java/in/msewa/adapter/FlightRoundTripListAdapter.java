package in.msewa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.msewa.model.FlightListModel;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 11/26/2016.
 */
public class FlightRoundTripListAdapter extends RecyclerView.Adapter<FlightRoundTripListAdapter.RecyclerViewHolders> {

  private Context context;
  private List<FlightListModel> flightArray;
  private String destinationCode, sourceCode, dateOfJourney;
  private int adultNo, childNo, infantNo;

  private int selectedPosition = 0, bondsLength;

  public FlightRoundTripListAdapter(Context context, List<FlightListModel> flightArray, String sourceCode, String
    destinationCode, String dateOfJourney, int adultNo, int childNo, int infantNo, int bondsLength) {
    this.context = context;
    this.flightArray = flightArray;
    this.sourceCode = sourceCode;
    this.destinationCode = destinationCode;
    this.dateOfJourney = dateOfJourney;
    this.bondsLength = bondsLength;
    this.adultNo = adultNo;
    this.childNo = childNo;
    this.infantNo = infantNo;
  }

  @Override
  public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
    View layoutView;
    if (bondsLength > 1) {
      layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_intentional_adapter, null);
    } else {
      layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_roundtrip_flight_list, null);
    }
    RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
    return rcv;
  }

  @SuppressLint("SetTextI18n")
  @Override
  public void onBindViewHolder(final RecyclerViewHolders viewHolder, int position) {
    String totalDuration;
    final int currentPosition = position;
    viewHolder.tvFlightFareRound.setText(context.getResources().getString(R.string.rupease) + " " + flightArray.get(position).getFlightNetFare());

    if (flightArray.get(position).getRefund().equalsIgnoreCase("true")) {
      viewHolder.tvFlightRefund.setText("Refundable");
    } else {
      viewHolder.tvFlightRefund.setText("Non-Refundable");
    }
    viewHolder.tvFightTypeList.setText(flightArray.get(position).getFlightListArray().get(0).getFligtType());
    viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    if (flightArray.get(position).getFlightListArray().size() == 1) {
      if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("6E")) {
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        Picasso.with(context).load(R.drawable.flight_indigo).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("SG")) {
        Picasso.with(context).load(R.drawable.flight_spice_jet).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("UK")) {
        Picasso.with(context).load(R.drawable.flight_vistara).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFightTypeList.setText("Vistara ");
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("9W")) {
        Picasso.with(context).load(R.drawable.flight_jet_airways).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName());
        viewHolder.tvFightTypeList.setText("JetAirWays ");
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("AI")) {
        Picasso.with(context).load(R.drawable.flight_air_india).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        viewHolder.tvFightTypeList.setText("AirIndia ");
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("I5")) {
        Picasso.with(context).load(R.drawable.flight_air_asia).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("XT")) {
        Picasso.with(context).load(R.drawable.skystar).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("G8")) {
        Picasso.with(context).load(R.drawable.flight_go_air).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else {
        Picasso.with(context).load(R.drawable.ic_no_flight).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      }

      viewHolder.tvFlightVia.setText("Non-Stop");
      if (flightArray.get(position).getFlightListRoundArray().size() != 0) {
        if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("6E")) {
          Picasso.with(context).load(R.drawable.flight_indigo).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightNo());
          viewHolder.tvFightTypeList.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
        } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("SG")) {
          Picasso.with(context).load(R.drawable.flight_spice_jet).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("UK")) {
          Picasso.with(context).load(R.drawable.flight_vistara).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("9W")) {
          Picasso.with(context).load(R.drawable.flight_jet_airways).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
          viewHolder.tvFightTypeList.setText("JetAirWays");
        } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("AI")) {
          Picasso.with(context).load(R.drawable.flight_air_india).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
          viewHolder.tvFightTypeList.setText("AirIndia");
        } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("I5")) {
          Picasso.with(context).load(R.drawable.flight_air_asia).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("XT")) {
          Picasso.with(context).load(R.drawable.skystar).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("G8")) {
          Picasso.with(context).load(R.drawable.flight_go_air).into(viewHolder.ivFightList);
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        } else {
          Picasso.with(context).load(R.drawable.ic_no_flight).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
          viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
          viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        }
//                viewHolder.llFlightListRound.setVisibility(View.VISIBLE);
//                viewHolder.llFlightListMain.setOrientation(LinearLayout.VERTICAL);
//                viewHolder.tvFightListNameRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + "-" + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
        viewHolder.tvFlightDepTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getDepTime());
        viewHolder.tvFlightArrivalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get((flightArray.get(position).getFlightListRoundArray().size() - 1)).getArrivalTime());
        viewHolder.tvFlightTotalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getJourneyTime());
        viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
        viewHolder.tvFlightRuleRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightRule());
        viewHolder.tvFightTypeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightCode() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());


        viewHolder.tvFightTypeList.setText(flightArray.get(position).getFlightListArray().get(0).getFligtType());
      }


      viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());
      viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
      viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getJourneyTime());
      viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightType.setText(flightArray.get(position).getFlightListArray().get(0).getFlightCode() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightTypeList.setText(flightArray.get(position).getFlightListArray().get(0).getFligtType());
    } else {
      viewHolder.tvFlightVia.setVisibility(View.VISIBLE);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());
      viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightTypeList.setText(flightArray.get(position).getFlightListArray().get(0).getFligtType());
      viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
      if (flightArray.get(position).getFlightListRoundArray().size() != 0) {
        viewHolder.tvFlightViaRound.setVisibility(View.VISIBLE);
        viewHolder.tvFlightViaRound.setText("Non-Stop");
        viewHolder.tvFlightFareRound.setText(context.getResources().getString(R.string.rupease) + " " + flightArray.get(position).getFlightNetFare());
        viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
//                viewHolder.llFlightListRound.setVisibility(View.VISIBLE);
//                viewHolder.tvFightListNameRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + "-" + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
        viewHolder.tvFlightDepTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getDepTime());

      }

      if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("6E")) {
        Picasso.with(context).load(R.drawable.flight_indigo).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("SG")) {
        Picasso.with(context).load(R.drawable.flight_spice_jet).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("UK")) {
        Picasso.with(context).load(R.drawable.flight_vistara).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        viewHolder.tvFightTypeList.setText("Vistara");
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("9W")) {
        Picasso.with(context).load(R.drawable.flight_jet_airways).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        viewHolder.tvFightTypeList.setText("JetAirWays");
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("AI")) {
        Picasso.with(context).load(R.drawable.flight_air_india).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        viewHolder.tvFightTypeList.setText("AirIndia");
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("I5")) {
        Picasso.with(context).load(R.drawable.flight_air_asia).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("XT")) {
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        Picasso.with(context).load(R.drawable.skystar).into(viewHolder.ivFightList);
      } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("G8")) {
        Picasso.with(context).load(R.drawable.flight_go_air).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      } else {
        Picasso.with(context).load(R.drawable.ic_no_flight).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
        viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      }
      //Checking connectivity
      if (flightArray.get(position).getFlightListArray().size() == 2) {
        //Stoppage
        if (flightArray.get(position).getFlightListRoundArray().size() != 0 && flightArray.get(position).getFlightListRoundArray().size() == 2) {
          viewHolder.tvFlightViaRound.setVisibility(View.VISIBLE);
          viewHolder.tvFlightViaRound.setText("1 Stop");
          totalDuration = compareDate(flightArray.get(position).getFlightListRoundArray().get(0).getDepTime(), flightArray.get(position).getFlightListRoundArray().get(1).getArrivalTime());
//                    viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
          viewHolder.tvFlightTotalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getJourneyTime());
//                    viewHolder.tvFlightArrivalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get((flightArray.get(position).getFlightListRoundArray().size() - 1)).getArrivalTime());
//                    viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());

        }
        viewHolder.tvFlightVia.setText("1 Stop");
        totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(1).getArrivalTime());
        viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getJourneyTime());

      } else if (flightArray.get(position).getFlightListArray().size() == 3) {
        if (flightArray.get(position).getFlightListRoundArray().size() != 0 && flightArray.get(position)
          .getFlightListRoundArray().size() == 3) {
          viewHolder.tvFlightViaRound.setText("2 Stops");
          totalDuration = compareDate(flightArray.get(position).getFlightListRoundArray().get(0).getDepTime(), flightArray.get(position).getFlightListRoundArray().get(2).getArrivalTime());
          viewHolder.tvFlightTotalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getJourneyTime());
//                    viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
//                    viewHolder.tvFlightArrivalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get((flightArray.get(position).getFlightListRoundArray().size() - 1)).getArrivalTime());
//                    viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());

        }
        viewHolder.tvFlightVia.setText("2 Stops");
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
        totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(2).getArrivalTime());
        viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getJourneyTime());

      } else if (flightArray.get(position).getFlightListArray().size() == 4) {
        if (flightArray.get(position).getFlightListRoundArray().size() != 0 && flightArray.get(position).getFlightListRoundArray().size() == 4) {
          viewHolder.tvFlightViaRound.setText("3 Stops");
          viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
          totalDuration = compareDate(flightArray.get(position).getFlightListRoundArray().get(0).getDepTime(), flightArray.get(position).getFlightListRoundArray().get(3).getArrivalTime());
          viewHolder.tvFlightTotalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getJourneyTime());
//                    viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
//                    viewHolder.tvFlightArrivalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get((flightArray.get(position).getFlightListRoundArray().size() - 1)).getArrivalTime());

        }
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
        viewHolder.tvFlightVia.setText("3 Stops");
        totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(3).getArrivalTime());
        viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getJourneyTime());
      } else if (flightArray.get(position).getFlightListArray().size() == 5) {
        if (flightArray.get(position).getFlightListRoundArray().size() != 0 && flightArray.get(position).getFlightListRoundArray().size() == 5) {
          viewHolder.tvFlightViaRound.setText("4 Stops");
          totalDuration = compareDate(flightArray.get(position).getFlightListRoundArray().get(0).getDepTime(), flightArray.get(position).getFlightListRoundArray().get(4).getArrivalTime());
          viewHolder.tvFlightTotalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getJourneyTime());
//                    viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
//                    viewHolder.tvFlightArrivalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get((flightArray.get(position).getFlightListRoundArray().size() - 1)).getArrivalTime());
//                    viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
        }
        viewHolder.tvFlightVia.setText("4 Stops");
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
        totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(4).getArrivalTime());
        viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getJourneyTime());
      } else {
        if (flightArray.get(position).getFlightListRoundArray().size() != 0) {
          viewHolder.tvFlightViaRound.setText("Non-Stop");
          viewHolder.tvFlightTotalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getJourneyTime());
          viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
          viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
        }
        viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get((flightArray.get(position).getFlightListArray().size() - 1)).getArrivalTime());
        viewHolder.tvFlightVia.setText((flightArray.get(position).getFlightListArray().size() - 1) + " Stops");
        viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getJourneyTime());
      }
      if (flightArray.get(position).getFlightListRoundArray().size() != 0) {
        viewHolder.tvFlightRuleRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightRule());
        viewHolder.tvFightTypeRound.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightCode() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
        viewHolder.tvFlightArrivalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get((flightArray.get(position).getFlightListRoundArray().size() - 1)).getArrivalTime());
        viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
      }

      viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightType.setText(flightArray.get(position).getFlightListArray().get(0).getFlightCode() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightTypeList.setText(flightArray.get(position).getFlightListArray().get(0).getFligtType());
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    }

    if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("6E")) {
      Picasso.with(context).load(R.drawable.flight_indigo).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("SG")) {
      Picasso.with(context).load(R.drawable.flight_spice_jet).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("UK")) {
      Picasso.with(context).load(R.drawable.flight_vistara).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightTypeList.setText("Vistara");
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("9W")) {
      Picasso.with(context).load(R.drawable.flight_jet_airways).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightTypeList.setText("JetAirWays");
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("AI")) {
      Picasso.with(context).load(R.drawable.flight_air_india).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
      viewHolder.tvFightTypeList.setText("AirIndia");
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("I5")) {
      Picasso.with(context).load(R.drawable.flight_air_asia).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("XT")) {
      Picasso.with(context).load(R.drawable.skystar).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else if (flightArray.get(position).getFlightListArray().get(0).getFlightName().equalsIgnoreCase("G8")) {
      Picasso.with(context).load(R.drawable.flight_go_air).into(viewHolder.ivFightList);
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
    } else {

      Picasso.with(context).load(R.drawable.ic_no_flight).placeholder(R.drawable.ic_no_flight).into(viewHolder.ivFightList);
      viewHolder.tvFightTypeList.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFligtType());
      viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());

    }

    if (flightArray.get(position).getFlightListRoundArray().size() != 0) {
//            viewHolder.ivFightListRound.setImageResource(AppMetadata.getFLightImage(flightArray.get(position).getFlightListRoundArray().get(0).getFlightCode(), viewHolder.tvFlightListName));
      viewHolder.tvFlightFareRound.setText(context.getResources().getString(R.string.rupease) + " " + flightArray.get(position).getFlightNetFare());
      viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
      viewHolder.tvFlightArrivalTimeRound.setText(flightArray.get(position).getFlightListRoundArray().get((flightArray.get(position).getFlightListRoundArray().size() - 1)).getArrivalTime());
      viewHolder.tvFightListNameSecond.setText(flightArray.get(position).getFlightListRoundArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListRoundArray().get(0).getFlightNo());
      ;
    }

//        viewHolder.ivFightList.setImageResource(AppMetadata.getFLightImage(flightArray.get(position).getFlightListArray().get(0).getFlightCode(), viewHolder.tvFlightListName));
    viewHolder.tvFlightFare.setText(context.getResources().getString(R.string.rupease) + " " + flightArray.get(position).getFlightNetFare());

    if (isSelected(position)) {
      try {
        if (bondsLength > 1) {
          sendUpdate(flightArray.get(position).getFlightListRoundArray().get(0).getTripType(), flightArray.get(position).getFlightNetFare(), true);
        } else {
          sendUpdate(flightArray.get(position).getFlightListArray().get(0).getTripType(), flightArray.get(position).getFlightNetFare(), false);
        }
      }catch (IllegalStateException e){
        e.printStackTrace();
      }
      viewHolder.llFlightListMain.setSelected(true);
    } else {
      viewHolder.llFlightListMain.setSelected(false);
    }

//        sendUpdate(flightArray.get(currentPosition).getFlightListArray().get(0).getTripType(),flightArray.get(currentPosition).getFlightNetFare());

    viewHolder.llFlightListMain.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        selectedPosition = currentPosition;
        if (view.isSelected()) {
          view.setSelected(false);
        } else {
          view.setSelected(true);
        }
        if (bondsLength > 1) {
          sendUpdate(flightArray.get(currentPosition).getFlightListRoundArray().get(0).getTripType(), flightArray.get(currentPosition).getFlightNetFare(), true);
        } else {
          sendUpdate(flightArray.get(currentPosition).getFlightListArray().get(0).getTripType(), flightArray.get(currentPosition).getFlightNetFare(), false);
        }
        notifyDataSetChanged();

      }
    });

  }

  @Override
  public int getItemCount() {
    return this.flightArray.size();
  }

  private void sendUpdate(int tripType, double price, boolean flightType) {
    Intent intent = new Intent("flight-price");
    intent.putExtra("tripType", tripType);
    intent.putExtra("price-update", price);
    intent.putExtra("intentional", flightType);
    intent.putExtra("flightArray", flightArray.get(selectedPosition));
    intent.putExtra("position", selectedPosition);
    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
  }

  private boolean isSelected(int position) {
    if (position != selectedPosition) {
      return false;
    } else {
      return true;
    }
  }

  public List<FlightListModel> getFlightArray() {
    return flightArray;
  }

  private String compareDate(String depTime, String arrTime) {
    //HH converts hour in 24 hours format (0-23), day calculation
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");

    Date d1 = null;
    Date d2 = null;


    try {
      d1 = format.parse(depTime);
      d2 = format.parse(arrTime);

      long difference = d2.getTime() - d1.getTime();

      if (difference < 0) {
        Date dateMax = format.parse("24:00");
        Date dateMin = format.parse("00:00");
        difference = (dateMax.getTime() - d1.getTime()) + (d2.getTime() - dateMin.getTime());
      }
      int days = (int) (difference / (1000 * 60 * 60 * 24));
      int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
      int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

      Calendar cal = Calendar.getInstance();
      cal.setTime(d1);
      cal.add(Calendar.HOUR, (int) hours);
      cal.add(Calendar.MINUTE, (int) min);
      String newTime = format.format(cal.getTime());
      if (days >= 1) {
        String date2 = String.valueOf(days + "d" + hours + "h" + min + "m");
        return date2.replace("-", "");
      } else {
        String date2 = String.valueOf(hours + " h" + min + " m");
        return date2.replace("-", "");
      }


    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;

  }

  public class RecyclerViewHolders extends RecyclerView.ViewHolder {
    public TextView tvFlightListName, tvFightListNameRound;
    public TextView tvFlightArrivalTime, tvFlightArrivalTimeRound;
    public TextView tvFlightDepTime, tvFlightDepTimeRound;
    public TextView tvFlightFare, tvFlightFareRound;
    public TextView tvFlightTotalTime, tvFlightTotalTimeRound;
    public TextView tvFightType, tvFightTypeRound, tvFightListNameSecond;
    public TextView tvFlightRule, tvFlightRuleRound;
    public TextView tvFlightVia, tvFlightViaRound;
    public LinearLayout llFlightListMain;
    public ImageView ivFightList, ivFightListRound;
    public LinearLayout llFlightListRound;
    public TextView tvFightTypeList, tvFlightRefund;

    public RecyclerViewHolders(View convertView) {
      super(convertView);
      tvFlightListName = (TextView) convertView.findViewById(R.id.tvFightListName);
      tvFlightArrivalTime = (TextView) convertView.findViewById(R.id.tvFlightArrivalTime);
      tvFlightDepTime = (TextView) convertView.findViewById(R.id.tvFlightDepTime);
      tvFlightFare = (TextView) convertView.findViewById(R.id.tvFlightFare);
      tvFlightTotalTime = (TextView) convertView.findViewById(R.id.tvFlightTotalTime);
      llFlightListMain = (LinearLayout) convertView.findViewById(R.id.llFlightListMain);
      tvFightType = (TextView) convertView.findViewById(R.id.tvFightType);
      tvFlightRule = (TextView) convertView.findViewById(R.id.tvFlightRule);
      tvFlightVia = (TextView) convertView.findViewById(R.id.tvFlightVia);
      ivFightList = (ImageView) convertView.findViewById(R.id.ivFightList);

      tvFightListNameSecond = (TextView) convertView.findViewById(R.id.tvFightListNameSecond);
      tvFightListNameRound = (TextView) convertView.findViewById(R.id.tvFightListNameRound);
      tvFlightArrivalTimeRound = (TextView) convertView.findViewById(R.id.tvFlightArrivalTimeRound);
      tvFlightDepTimeRound = (TextView) convertView.findViewById(R.id.tvFlightDepTimeRound);
      tvFlightFareRound = (TextView) convertView.findViewById(R.id.tvFlightFareRound);
      tvFlightTotalTimeRound = (TextView) convertView.findViewById(R.id.tvFlightTotalTimeRound);
//            llFlightListRound = (LinearLayout) convertView.findViewById(R.id.llFlightListRound);
      tvFightTypeRound = (TextView) convertView.findViewById(R.id.tvFightTypeRound);
      tvFlightRuleRound = (TextView) convertView.findViewById(R.id.tvFlightRuleRound);
      tvFlightViaRound = (TextView) convertView.findViewById(R.id.tvFlightViaRound);
      ivFightListRound = (ImageView) convertView.findViewById(R.id.ivFightListRound);
//            llFlightListRound = (LinearLayout) convertView.findViewById(R.id.llFlightListRound);
      tvFightTypeList = (TextView) convertView.findViewById(R.id.tvFightTypeList);
      tvFlightRefund = (TextView) convertView.findViewById(R.id.tvFlightRefund);
    }
  }
}
