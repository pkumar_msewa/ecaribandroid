package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 3/27/2016.
 */
public class ShoppingModel extends SugarRecord implements Parcelable {

    private long pid;
    private String pName;
    private String pDesc;
    private long pPrice;
    private String pImage;
    private String pStatus;
    private long pWeight;
    private String pCat;
    private String pSubCat;
    private String pBrand;
    private long pQty;

    public ShoppingModel() {
    }

    public ShoppingModel(long pid, String pName, String pDesc, long pPrice, String pImage, String pStatus, long pWeight, String pCat, String pSubCat, String pBrand, long pQty) {
        this.pid = pid;
        this.pName = pName;
        this.pDesc = pDesc;
        this.pPrice = pPrice;
        this.pImage = pImage;
        this.pStatus = pStatus;
        this.pWeight = pWeight;
        this.pCat = pCat;
        this.pSubCat = pSubCat;
        this.pBrand = pBrand;
        this.pQty = pQty;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpDesc() {
        return pDesc;
    }

    public void setpDesc(String pDesc) {
        this.pDesc = pDesc;
    }

    public long getpPrice() {
        return pPrice;
    }

    public void setpPrice(long pPrice) {
        this.pPrice = pPrice;
    }

    public String getpImage() {
        return pImage;
    }

    public void setpImage(String pImage) {
        this.pImage = pImage;
    }


    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public long getpWeight() {
        return pWeight;
    }

    public void setpWeight(long pWeight) {
        this.pWeight = pWeight;
    }

    public String getpCat() {
        return pCat;
    }

    public void setpCat(String pCat) {
        this.pCat = pCat;
    }

    public String getpSubCat() {
        return pSubCat;
    }

    public void setpSubCat(String pSubCat) {
        this.pSubCat = pSubCat;
    }

    public String getpBrand() {
        return pBrand;
    }

    public void setpBrand(String pBrand) {
        this.pBrand = pBrand;
    }
    public long getpQty() {
        return pQty;
    }

    public void setpQty(long pQty) {
        this.pQty = pQty;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.pid);
        dest.writeString(this.pName);
        dest.writeString(this.pDesc);
        dest.writeLong(this.pPrice);
        dest.writeString(this.pImage);
        dest.writeString(this.pStatus);
        dest.writeLong(this.pWeight);
        dest.writeString(this.pCat);
        dest.writeString(this.pSubCat);
        dest.writeString(this.pBrand);
        dest.writeLong(this.pQty);

    }

    protected ShoppingModel(Parcel in) {
        this.pid = in.readLong();
        this.pName = in.readString();
        this.pDesc = in.readString();
        this.pPrice = in.readLong();
        this.pImage = in.readString();
        this.pStatus = in.readString();
        this.pWeight = in.readLong();
        this.pCat = in.readString();
        this.pSubCat = in.readString();
        this.pBrand = in.readString();
        this.pQty = in.readLong();

    }

    public static final Creator<ShoppingModel> CREATOR = new Creator<ShoppingModel>() {
        @Override
        public ShoppingModel createFromParcel(Parcel source) {
            return new ShoppingModel(source);
        }

        @Override
        public ShoppingModel[] newArray(int size) {
            return new ShoppingModel[size];
        }
    };
}
