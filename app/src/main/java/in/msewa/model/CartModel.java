package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 4/6/2016.
 */
public class CartModel implements Parcelable {

    public String pcId;
    public String pcName;
    public String pcPrice;


    public CartModel(){

    }

    public CartModel(String pcId, String pcName, String pcPrice){
        this.pcId = pcId;
        this.pcName = pcName;
        this.pcPrice = pcPrice;
    }

    public String getPcId() {
        return pcId;
    }

    public void setPcId(String pcId) {
        this.pcId = pcId;
    }

    public String getPcName() {
        return pcName;
    }

    public void setPcName(String pcName) {
        this.pcName = pcName;
    }

    public String getPcPrice() {
        return pcPrice;
    }

    public void setPcPrice(String pcPrice) {
        this.pcPrice = pcPrice;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.pcId);
        dest.writeString(this.pcName);
        dest.writeString(this.pcPrice);
    }

    protected CartModel(Parcel in) {
        this.pcId = in.readString();
        this.pcName = in.readString();
        this.pcPrice = in.readString();
    }

    public static final Creator<CartModel> CREATOR = new Creator<CartModel>() {
        @Override
        public CartModel createFromParcel(Parcel source) {
            return new CartModel(source);
        }

        @Override
        public CartModel[] newArray(int size) {
            return new CartModel[size];
        }
    };
}
