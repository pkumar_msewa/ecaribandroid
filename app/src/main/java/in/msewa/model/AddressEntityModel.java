package in.msewa.model;

public class AddressEntityModel {

	private int index;
	private String id;
	private String desc;

	public AddressEntityModel(int position, String id, String desc) {
		this.index = position;
		this.id = id;
		this.desc = desc;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
