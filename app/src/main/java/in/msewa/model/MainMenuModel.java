package in.msewa.model;

/**
 * Created by Ksf on 3/16/2016.
 */
public class MainMenuModel {
  private String menuTitle;
  private int menuImage;
  private String type;

  public MainMenuModel(String menuTitle, int menuImage, String type) {
    this.menuImage = menuImage;
    this.menuTitle = menuTitle;
    this.type = type;

  }

  public String getMenuTitle() {
    return menuTitle;
  }

  public void setMenuTitle(String menuTitle) {
    this.menuTitle = menuTitle;
  }

  public int getMenuImage() {
    return menuImage;
  }

  public void setMenuImage(int menuImage) {
    this.menuImage = menuImage;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
