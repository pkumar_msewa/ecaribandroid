package in.msewa.model;

/**
 * Created by Ksf on 5/8/2016.
 */
public class StatementModel {
    public String currentBalance;
    public String amountPaid;
    public String dateTime;
    public String servicesType;
    public String serviceStatus;
    public String refNo;
    public String description;
    public boolean isDebited;
    public String authRefNo;
    public String retrievRefNo;


    public StatementModel(String currentBalance, String amountPaid, String dateTime, String servicesType, String serviceStatus, String refNo, String description,boolean isDebited, String authRefNo, String retrievRefNo){
        this.currentBalance = currentBalance;
        this.amountPaid = amountPaid;
        this.dateTime = dateTime;
        this.servicesType = servicesType;
        this.serviceStatus = serviceStatus;
        this.refNo = refNo;
        this.description = description;
        this.isDebited = isDebited;
        this.authRefNo = authRefNo;
        this.retrievRefNo = retrievRefNo;
    }

    public String getAuthRefNo() {
        return authRefNo;
    }

    public void setAuthRefNo(String authRefNo) {
        this.authRefNo = authRefNo;
    }

    public String getRetrievRefNo() {
        return retrievRefNo;
    }

    public void setRetrievRefNo(String retrievRefNo) {
        this.retrievRefNo = retrievRefNo;
    }

    public boolean isDebited() {
        return isDebited;
    }

    public void setDebited(boolean debited) {
        isDebited = debited;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getServicesType() {
        return servicesType;
    }

    public void setServicesType(String servicesType) {
        this.servicesType = servicesType;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
