package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Azhar on 22-06-2017.
 */


public class ImagicaTicketModel implements Parcelable {
    private String tdescription, ttitle, tproductType, tcurrency, timageURL, ttype;
    int tterms, tquantity;
    double tcostPerQty, ttotalCost;
    long tproductId, tplu;
    private ArrayList<TicketDetail> ticketDetail;

    public ImagicaTicketModel(String tdescription, String ttitle, String tproductType, String tcurrency, String timageURL, String ttype, int tterms, ArrayList<TicketDetail> ticketDetail) {
        this.tdescription = tdescription;
        this.ttitle = ttitle;
        this.tproductType = tproductType;
        this.tcurrency = tcurrency;
        this.timageURL = timageURL;
        this.ttype = ttype;
        this.tterms = tterms;
        this.ticketDetail = ticketDetail;
    }


    protected ImagicaTicketModel(Parcel in) {
        tdescription = in.readString();
        ttitle = in.readString();
        tproductType = in.readString();
        tcurrency = in.readString();
        timageURL = in.readString();
        ttype = in.readString();
        tterms = in.readInt();
//        tquantity = in.readInt();
//        tcostPerQty = in.readDouble();
//        ttotalCost = in.readDouble();
//        tproductId = in.readLong();
        tplu = in.readLong();
        ticketDetail = in.createTypedArrayList(TicketDetail.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tdescription);
        dest.writeString(ttitle);
        dest.writeString(tproductType);
        dest.writeString(tcurrency);
        dest.writeString(timageURL);
        dest.writeString(ttype);
        dest.writeInt(tterms);
        dest.writeInt(tquantity);
        dest.writeDouble(tcostPerQty);
        dest.writeDouble(ttotalCost);
        dest.writeLong(tproductId);
        dest.writeLong(tplu);
        dest.writeTypedList(ticketDetail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ImagicaTicketModel> CREATOR = new Creator<ImagicaTicketModel>() {
        @Override
        public ImagicaTicketModel createFromParcel(Parcel in) {
            return new ImagicaTicketModel(in);
        }

        @Override
        public ImagicaTicketModel[] newArray(int size) {
            return new ImagicaTicketModel[size];
        }
    };

    public String getTdescription() {
        return tdescription;
    }

    public void setTdescription(String tdescription) {
        this.tdescription = tdescription;
    }

    public String getTtitle() {
        return ttitle;
    }

    public void setTtitle(String ttitle) {
        this.ttitle = ttitle;
    }

    public String getTproductType() {
        return tproductType;
    }

    public void setTproductType(String tproductType) {
        this.tproductType = tproductType;
    }

    public String getTcurrency() {
        return tcurrency;
    }

    public void setTcurrency(String tcurrency) {
        this.tcurrency = tcurrency;
    }

    public String getTimageURL() {
        return timageURL;
    }

    public void setTimageURL(String timageURL) {
        this.timageURL = timageURL;
    }

    public String getTtype() {
        return ttype;
    }

    public void setTtype(String ttype) {
        this.ttype = ttype;
    }

    public int getTterms() {
        return tterms;
    }

    public void setTterms(int tterms) {
        this.tterms = tterms;
    }

    public int getTquantity() {
        return tquantity;
    }

    public void setTquantity(int tquantity) {
        this.tquantity = tquantity;
    }

    public double getTcostPerQty() {
        return tcostPerQty;
    }

    public void setTcostPerQty(double tcostPerQty) {
        this.tcostPerQty = tcostPerQty;
    }

    public double getTtotalCost() {
        return ttotalCost;
    }

    public void setTtotalCost(double ttotalCost) {
        this.ttotalCost = ttotalCost;
    }

    public long getTproductId() {
        return tproductId;
    }

    public void setTproductId(long tproductId) {
        this.tproductId = tproductId;
    }

    public long getTplu() {
        return tplu;
    }

    public void setTplu(long tplu) {
        this.tplu = tplu;
    }

    public List<TicketDetail> getTicketDetail() {
        return ticketDetail;
    }

    public void setTicketDetail(ArrayList<TicketDetail> ticketDetail) {
        this.ticketDetail = ticketDetail;
    }

    public static class TaxDetailList implements Parcelable{
        private String name;
        private double pricePerQty;

        public TaxDetailList(){

        }

        protected TaxDetailList(Parcel in) {
            name = in.readString();
            pricePerQty = in.readDouble();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeDouble(pricePerQty);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<TaxDetailList> CREATOR = new Creator<TaxDetailList>() {
            @Override
            public TaxDetailList createFromParcel(Parcel in) {
                return new TaxDetailList(in);
            }

            @Override
            public TaxDetailList[] newArray(int size) {
                return new TaxDetailList[size];
            }
        };

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPricePerQty() {
            return pricePerQty;
        }

        public void setPricePerQty(double pricePerQty) {
            this.pricePerQty = pricePerQty;
        }
    }
}