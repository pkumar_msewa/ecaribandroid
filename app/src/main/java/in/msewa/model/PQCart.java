package in.msewa.model;

import android.util.Log;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PQCart {

    private volatile static PQCart uniqueInstance;
    List<ShoppingModel> product;
    /*
     * HashMap<ProductId, ProductModel>
     */
    private HashMap<String, ShoppingModel> productsInCart = new HashMap<>();
    private double totalCost;
    public static boolean isCartCreated = false;
//	public static final String MOBILE_NUMBER = "9740116671";

    private boolean isCartUpdated = false;

    /*
     * Singleton
     */
    public PQCart() {
    }

    /*
     * Singleton getInstance()
     */
    public static PQCart getInstance() {
        if (uniqueInstance == null) {
            synchronized (PQCart.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new PQCart();
                }
            }
        }
        return uniqueInstance;
    }

//    public PQCart(List<ShoppingModel> product) {
//        this.product = product;
//        createNewCart(product);
//    }

    public HashMap<String, ShoppingModel> getProductsInCart() {
        return productsInCart;
    }

    public ArrayList<ShoppingModel> getProductsInCartArray() {
        return new ArrayList<>(productsInCart.values());
    }

    public void setProductsInCart(HashMap<String, ShoppingModel> productsInCart) {
        this.productsInCart = productsInCart;
    }

    public static PQCart getUniqueInstance() {
        return uniqueInstance;
    }

    public String getTotalCost() {
        totalCost = 0;
        for (ShoppingModel product : getProductsInCart().values()) {
            if (product.getpQty() != 0) {
                String correctedPrice = String.valueOf(product.getpPrice() * product.getpQty());
                Log.i("total", correctedPrice);
                totalCost = Double.parseDouble(correctedPrice) + totalCost;
            }
        }
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        return formatter.format(totalCost);
    }

    public String getProductInCartNames() {
        String productsName = "";
        for (ShoppingModel product : getProductsInCart().values()) {
            productsName = productsName + product.getpName();
        }
        return productsName;
    }

    public String getTotalCostNumber() {
        double cost = 0;
        for (ShoppingModel product : getProductsInCart().values()) {
            String correctedPrice = String.valueOf(product.getpPrice());
            cost = Double.parseDouble(correctedPrice) + cost;
        }
        return String.valueOf(cost);
    }

    /*
     * Automatically handle add and remove from cart
     */
    public boolean addRemoveProductsInCart(ShoppingModel product, long qty) {

        Log.i("latestqty", String.valueOf(qty));
        if (product != null) {
            if (productsInCart.containsKey(String.valueOf(product.getPid()))) {
                productsInCart.remove(String.valueOf(product.getPid()));
                return false;
            } else {
                product.setpQty(1);
                productsInCart.put(String.valueOf(product.getPid()), product);
                return false;
            }
        } else {
            return false;
        }
    }

    public HashMap<String, ShoppingModel> createNewCart(List<ShoppingModel> products) {
        HashMap<String, ShoppingModel> productsInCart = new HashMap<>();
        try {
            if (products.size() != 0) {
                for (ShoppingModel product : products) {
                    if (product.getPid() != 0) {
                        productsInCart.put(String.valueOf(product.getPid()), product);
                        Log.i(" size", String.valueOf(productsInCart.size()));
                    }
                }
            }
            Log.i(" size", String.valueOf(productsInCart.size()));
            return productsInCart;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isProductInCart(ShoppingModel product) {

        Log.i("product", String.valueOf((this.productsInCart.size())) + "producsst::" + product.getPid());
        return productsInCart.containsKey(String.valueOf(product.getPid()));
    }

    public boolean isCartUpdated() {
        return isCartUpdated;
    }

    public void setCartUpdated(boolean isCartUpdated) {
        this.isCartUpdated = isCartUpdated;
    }

    public void clearCart() {
        isCartUpdated = false;
        productsInCart = new HashMap<>();
    }

}
