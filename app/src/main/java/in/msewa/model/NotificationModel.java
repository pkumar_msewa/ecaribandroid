package in.msewa.model;

/**
 * Created by Msewa-Admin on 6/10/2017.
 */

public class NotificationModel {
    private String msg;
    private String date;

    public NotificationModel(String msg, String date) {
        this.msg = msg;
        this.date = date;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
