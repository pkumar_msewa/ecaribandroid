package in.msewa.model;

/**
 * Created by acer on 10-07-2017.
 */

public class Country {


  private String country;
  private String countryCode;
  private String countryImageCode;

  public Country(String country, String countryCode, String countryImageCode) {
    this.country = country;
    this.countryCode = countryCode;
    this.countryImageCode = countryImageCode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCountryImageCode() {
    return countryImageCode;
  }

  public void setCountryImageCode(String countryImageCode) {
    this.countryImageCode = countryImageCode;
  }
}



