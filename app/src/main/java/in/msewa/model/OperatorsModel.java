package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class OperatorsModel extends SugarRecord implements Parcelable {
	
	public String code;
	public String name;
	public String defaultCode;

    public OperatorsModel(){

    }

	public OperatorsModel(String code, String name, String defaultCode) {
		this.code = code;
		this.name = name;
		this.defaultCode = defaultCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefaultCode() {
		return defaultCode;
	}

	public void setDefaultCode(String defaultCode) {
		this.defaultCode = defaultCode;
	}


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.name);
        dest.writeString(this.defaultCode);
    }

    protected OperatorsModel(Parcel in) {
        this.code = in.readString();
        this.name = in.readString();
        this.defaultCode = in.readString();
    }

    public static final Creator<OperatorsModel> CREATOR = new Creator<OperatorsModel>() {
        @Override
        public OperatorsModel createFromParcel(Parcel source) {
            return new OperatorsModel(source);
        }

        @Override
        public OperatorsModel[] newArray(int size) {
            return new OperatorsModel[size];
        }
    };
}
