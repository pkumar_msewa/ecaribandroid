package in.msewa.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 6/22/2016.
 */
public class SplitMoneyModel extends SugarRecord{
    private String splitMoneyTitle;
    private String splitGroupType;
    private String splitMoneyAmount;
    private int splitId;


    public SplitMoneyModel(){

    }

    public SplitMoneyModel(int splitId,String splitMoneyTitle, String splitGroupType, String splitMoneyAmount){
        this.splitMoneyAmount = splitMoneyAmount;
        this.splitGroupType = splitGroupType;
        this.splitMoneyTitle = splitMoneyTitle;
        this.splitId = splitId;
    }

    public String getSplitMoneyTitle() {
        return splitMoneyTitle;
    }

    public void setSplitMoneyTitle(String splitMoneyTitle) {
        this.splitMoneyTitle = splitMoneyTitle;
    }

    public String getSplitGroupType() {
        return splitGroupType;
    }

    public void setSplitGroupType(String splitGroupType) {
        this.splitGroupType = splitGroupType;
    }

    public String getSplitMoneyAmount() {
        return splitMoneyAmount;
    }

    public void setSplitMoneyAmount(String splitMoneyAmount) {
        this.splitMoneyAmount = splitMoneyAmount;
    }

    public int getSplitId() {
        return splitId;
    }

    public void setSplitId(int splitId) {
        this.splitId = splitId;
    }
}
