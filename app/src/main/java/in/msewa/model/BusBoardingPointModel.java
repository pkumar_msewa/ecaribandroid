package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class BusBoardingPointModel implements Parcelable {

    private String boardingLocation;
    private String boardingTime;
    private String boardingId;
    private String boardingTimePrime;
    private String boardingTypeID;
    private boolean seater;
    private boolean sleaper;
    private boolean ac;
    private String bodingType;


    protected BusBoardingPointModel(Parcel in) {
        boardingLocation = in.readString();
        boardingTime = in.readString();
        boardingId = in.readString();
        boardingTimePrime = in.readString();
        boardingTypeID = in.readString();
        seater = in.readByte() != 0;
        sleaper = in.readByte() != 0;
        ac = in.readByte() != 0;
        bodingType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(boardingLocation);
        dest.writeString(boardingTime);
        dest.writeString(boardingId);
        dest.writeString(boardingTimePrime);
        dest.writeString(boardingTypeID);
        dest.writeByte((byte) (seater ? 1 : 0));
        dest.writeByte((byte) (sleaper ? 1 : 0));
        dest.writeByte((byte) (ac ? 1 : 0));
        dest.writeString(bodingType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusBoardingPointModel> CREATOR = new Creator<BusBoardingPointModel>() {
        @Override
        public BusBoardingPointModel createFromParcel(Parcel in) {
            return new BusBoardingPointModel(in);
        }

        @Override
        public BusBoardingPointModel[] newArray(int size) {
            return new BusBoardingPointModel[size];
        }
    };

    public String getBoardingLocation() {
        return boardingLocation;
    }

    public void setBoardingLocation(String boardingLocation) {
        this.boardingLocation = boardingLocation;
    }

    public String getBoardingTime() {
        return boardingTime;
    }

    public void setBoardingTime(String boardingTime) {
        this.boardingTime = boardingTime;
    }

    public String getBoardingId() {
        return boardingId;
    }

    public void setBoardingId(String boardingId) {
        this.boardingId = boardingId;
    }

    public String getBoardingTimePrime() {
        return boardingTimePrime;
    }

    public void setBoardingTimePrime(String boardingTimePrime) {
        this.boardingTimePrime = boardingTimePrime;
    }

    public String getBoardingTypeID() {
        return boardingTypeID;
    }

    public void setBoardingTypeID(String boardingTypeID) {
        this.boardingTypeID = boardingTypeID;
    }

    public boolean isSeater() {
        return seater;
    }

    public void setSeater(boolean seater) {
        this.seater = seater;
    }

    public boolean isSleaper() {
        return sleaper;
    }

    public void setSleaper(boolean sleaper) {
        this.sleaper = sleaper;
    }

    public boolean isAc() {
        return ac;
    }

    public void setAc(boolean ac) {
        this.ac = ac;
    }

    public String getBodingType() {
        return bodingType;
    }

    public void setBodingType(String bodingType) {
        this.bodingType = bodingType;
    }

    public BusBoardingPointModel(String boardingLocation, String boardingTime, String boardingId, String boardingTimePrime, String boardingTypeID, boolean seater, boolean sleaper, boolean ac, String bodingType) {
        this.boardingId = boardingId;
        this.boardingLocation = boardingLocation;
        this.boardingTime = boardingTime;
        this.boardingTimePrime = boardingTimePrime;

        this.boardingTypeID = boardingTypeID;
        this.seater = seater;
        this.sleaper = sleaper;
        this.ac = ac;
        this.bodingType=bodingType;
    }


}
