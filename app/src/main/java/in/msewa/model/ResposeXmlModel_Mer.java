package in.msewa.model;

public class ResposeXmlModel_Mer {

    String ResDesc;

    String Timestamp;

    String ResCode;

    String MsgId;

    String OrgId;

    String Modulus;

    String RegStatus;

    String Balance;

    String TxnType;

    String RespListKeys;

    String GetToken;

    String Mobile;

    String ApprovalRefNo;

    String DefaultAddressLimit;

    String DefaultInActivePeriod;

    String DefaultInActiveDays;

    String RegType;

    String DefaultVirtualAddress;

    String PayeeVir_CustomerName;

    public String getDEKKey() {
        return DEKKey;
    }

    public void setDEKKey(String DEKKey) {
        this.DEKKey = DEKKey;
    }

    String DEKKey;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    String Token;

    public String getPayeeVir_CustomerName() {
        return PayeeVir_CustomerName;
    }

    public void setPayeeVir_CustomerName(String payeeVir_CustomerName) {
        PayeeVir_CustomerName = payeeVir_CustomerName;
    }

    public String getDefaultVirtualAddress() {
        return DefaultVirtualAddress;
    }

    public void setDefaultVirtualAddress(String defaultVirtualAddress) {
        DefaultVirtualAddress = defaultVirtualAddress;
    }

    public String getRegType() {
        return RegType;
    }

    public void setRegType(String regType) {
        RegType = regType;
    }

    public String getDefaultInActiveDays() {
        return DefaultInActiveDays;
    }

    public void setDefaultInActiveDays(String defaultInActiveDays) {
        DefaultInActiveDays = defaultInActiveDays;
    }

    public String getDefaultAddressLimit() {
        return DefaultAddressLimit;
    }

    public void setDefaultAddressLimit(String defaultAddressLimit) {
        DefaultAddressLimit = defaultAddressLimit;
    }

    public String getDefaultInActivePeriod() {
        return DefaultInActivePeriod;
    }

    public void setDefaultInActivePeriod(String defaultInActivePeriod) {
        DefaultInActivePeriod = defaultInActivePeriod;
    }

    public String getApprovalRefNo() {
        return ApprovalRefNo;
    }

    public void setApprovalRefNo(String approvalRefNo) {
        ApprovalRefNo = approvalRefNo;
    }

    public String getPayeeRespCode() {
        return PayeeRespCode;
    }

    public void setPayeeRespCode(String payeeRespCode) {
        PayeeRespCode = payeeRespCode;
    }

    String PayeeRespCode;


    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getGetToken() {
        return GetToken;
    }

    public void setGetToken(String getToken) {
        GetToken = getToken;
    }

    public String getTxnType() {
        return TxnType;
    }

    public void setTxnType(String txnType) {
        TxnType = txnType;
    }

    public String getRespListKeys() {
        return RespListKeys;
    }

    public void setRespListKeys(String respListKeys) {
        RespListKeys = respListKeys;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }

    public String getRegStatus() {

        return RegStatus;
    }

    public void setRegStatus(String regStatus) {

        RegStatus = regStatus;
    }

    public String getModulus() {

        return Modulus;
    }

    public void setModulus(String modulus) {

        Modulus = modulus;
    }

    public String getResDesc() {

        return ResDesc;
    }

    public void setResDesc(String resDesc) {

        ResDesc = resDesc;
    }

    public String getResCode() {

        return ResCode;
    }

    public void setResCode(String resCode) {
        ResCode = resCode;
    }

    public String getMsgId() {
        return MsgId;
    }

    public void setMsgId(String msgId) {
        MsgId = msgId;
    }

    public String getOrgId() {
        return OrgId;
    }

    public void setOrgId(String orgId) {
        OrgId = orgId;
    }

    public String getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(String timestamp) {
        Timestamp = timestamp;
    }

    String Handle;
    String SMSConfirmStatus;


    public String getSMSConfirmStatus() {
        return SMSConfirmStatus;
    }

    public void setSMSConfirmStatus(String sMSConfirmStatus) {
        SMSConfirmStatus = sMSConfirmStatus;
    }

    public String getHandle() {
        return Handle;
    }

    public void setHandle(String handle) {
        Handle = handle;
    }


}
