package in.msewa.model;

/**
 * Created by Azhar on 10/30/2017.
 */

public class MenuOrderListModel {
    private String orderDate;
    private String txnDate;
    private String trainNumber;
    private String orderStatus;
    private String userOrderId;
    private String seat;
    private String coach;
    private String contactNo;
    private String eta;
    private String pnr;
    private String name;
    private String totalCustomerPayable;

    public MenuOrderListModel(String orderDate, String txnDate, String trainNumber, String orderStatus, String userOrderId, String seat, String coach, String contactNo, String eta, String pnr, String name, String totalCustomerPayable) {
        this.orderDate = orderDate;
        this.txnDate = txnDate;
        this.trainNumber = trainNumber;
        this.orderStatus = orderStatus;
        this.userOrderId = userOrderId;
        this.seat = seat;
        this.coach = coach;
        this.contactNo = contactNo;
        this.eta = eta;
        this.pnr = pnr;
        this.name = name;
        this.totalCustomerPayable = totalCustomerPayable;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUserOrderId() {
        return userOrderId;
    }

    public void setUserOrderId(String userOrderId) {
        this.userOrderId = userOrderId;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalCustomerPayable() {
        return totalCustomerPayable;
    }

    public void setTotalCustomerPayable(String totalCustomerPayable) {
        this.totalCustomerPayable = totalCustomerPayable;
    }
}
