package in.msewa.model;

public class GasModel {
  String accountNumberPattern;
  String accountNumberType;
  String gasImages;
  String operatorCode;
  String operatorName;

  public GasModel(String accountNumberPattern, String accountNumberType, String gasImages, String operatorCode, String operatorName) {
    this.accountNumberPattern = accountNumberPattern;
    this.accountNumberType = accountNumberType;
    this.gasImages = gasImages;
    this.operatorCode = operatorCode;
    this.operatorName = operatorName;
  }

  public String getAccountNumberPattern() {
    return accountNumberPattern;
  }

  public void setAccountNumberPattern(String accountNumberPattern) {
    this.accountNumberPattern = accountNumberPattern;
  }

  public String getAccountNumberType() {
    return accountNumberType;
  }

  public void setAccountNumberType(String accountNumberType) {
    this.accountNumberType = accountNumberType;
  }

  public String getGasImages() {
    return gasImages;
  }

  public void setGasImages(String gasImages) {
    this.gasImages = gasImages;
  }

  public String getOperatorCode() {
    return operatorCode;
  }

  public void setOperatorCode(String operatorCode) {
    this.operatorCode = operatorCode;
  }

  public String getOperatorName() {
    return operatorName;
  }

  public void setOperatorName(String operatorName) {
    this.operatorName = operatorName;
  }
}
