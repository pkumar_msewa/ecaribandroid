package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * An object representing a cluster of items (markers) on the map.
 */
public class MerchantAgent  implements Parcelable {

  private double latitude;
  private double longitude;
  private String merchantName;
  private String kms;
  private String address;
  private double currentlatitude;
  private double currentlongitude;

  public MerchantAgent(double latitude, double longitude, String merchantName, String kms, String address, double currentlatitude, double currentlongitude) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.merchantName = merchantName;
    this.kms = kms;
    this.address = address;
    this.currentlatitude = currentlatitude;
    this.currentlongitude = currentlongitude;
  }

  protected MerchantAgent(Parcel in) {
    latitude = in.readDouble();
    longitude = in.readDouble();
    merchantName = in.readString();
    kms = in.readString();
    address = in.readString();
    currentlatitude = in.readDouble();
    currentlongitude = in.readDouble();
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeDouble(latitude);
    dest.writeDouble(longitude);
    dest.writeString(merchantName);
    dest.writeString(kms);
    dest.writeString(address);
    dest.writeDouble(currentlatitude);
    dest.writeDouble(currentlongitude);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<MerchantAgent> CREATOR = new Creator<MerchantAgent>() {
    @Override
    public MerchantAgent createFromParcel(Parcel in) {
      return new MerchantAgent(in);
    }

    @Override
    public MerchantAgent[] newArray(int size) {
      return new MerchantAgent[size];
    }
  };

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public String getMerchantName() {
    return merchantName;
  }

  public void setMerchantName(String merchantName) {
    this.merchantName = merchantName;
  }

  public String getKms() {
    return kms;
  }

  public void setKms(String kms) {
    this.kms = kms;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public double getCurrentlatitude() {
    return currentlatitude;
  }

  public void setCurrentlatitude(double currentlatitude) {
    this.currentlatitude = currentlatitude;
  }

  public double getCurrentlongitude() {
    return currentlongitude;
  }

  public void setCurrentlongitude(double currentlongitude) {
    this.currentlongitude = currentlongitude;
  }
}
