package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 6/22/2016.
 */
public class SplitMoneyGroupModel extends SugarRecord implements Parcelable {
    private String namePerson;
    private String amountPerson;
    private String identityPerson;
    private int groupid;


    public SplitMoneyGroupModel(){

    }

    public SplitMoneyGroupModel(int groupid,String namePerson, String amountPerson, String identityPerson){
        this.namePerson = namePerson;
        this.identityPerson = identityPerson;
        this.amountPerson = amountPerson;
        this.groupid = groupid;
    }



    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public String getIdentityPerson() {
        return identityPerson;
    }

    public void setIdentityPerson(String identityPerson) {
        this.identityPerson = identityPerson;
    }

    public String getNamePerson() {
        return namePerson;
    }

    public void setNamePerson(String namePerson) {
        this.namePerson = namePerson;
    }

    public String getAmountPerson() {
        return amountPerson;
    }

    public void setAmountPerson(String amountPerson) {
        this.amountPerson = amountPerson;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.namePerson);
        dest.writeString(this.amountPerson);
        dest.writeString(this.identityPerson);
        dest.writeInt(this.groupid);
    }

    protected SplitMoneyGroupModel(Parcel in) {
        this.namePerson = in.readString();
        this.amountPerson = in.readString();
        this.identityPerson = in.readString();
        this.groupid = in.readInt();
    }

    public static final Creator<SplitMoneyGroupModel> CREATOR = new Creator<SplitMoneyGroupModel>() {
        @Override
        public SplitMoneyGroupModel createFromParcel(Parcel source) {
            return new SplitMoneyGroupModel(source);
        }

        @Override
        public SplitMoneyGroupModel[] newArray(int size) {
            return new SplitMoneyGroupModel[size];
        }
    };
}
