package in.msewa.model;

/**
 * Created by Ksf on 3/16/2016.
 */
public class MobilePlansModel {

    private String validity;
    private String amount;
    private String talkTime;
    private String description;
    private String operatorCode;


    public MobilePlansModel(String validity, String amount, String talktime, String description, String operatorCode){

        this.amount = amount.replaceAll("[^\\d.]", "");
        this.description = description;
        this.validity = validity;
        this.talkTime = talktime;
        this.operatorCode = operatorCode;
    }

    public String getTalkTime(){
        return talkTime;
    }

    public String getAmount(){
        return amount;
    }

    public String getDescription(){
        return description;
    }

    public String getValidity(){
        return validity;
    }

    public void setAmount(String amount){
        this.amount = amount;
    }

    public void setValidity(String validity){
        this.validity= validity;
    }

    public void setTalkTime (String talktime){
        this.talkTime = talktime;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

}
