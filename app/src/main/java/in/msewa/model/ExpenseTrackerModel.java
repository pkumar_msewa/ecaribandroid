package in.msewa.model;

/**
 * Created by Azhar on 15-07-2017.
 */

public class ExpenseTrackerModel {

    private String serviceName;
    private float serviceAmount;
    

    public ExpenseTrackerModel(String serviceName, float serviceAmount) {
        this.serviceName = serviceName;
        this.serviceAmount = serviceAmount;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public float getServiceAmount() {
        return serviceAmount;
    }

    public void setServiceAmount(float serviceAmount) {
        this.serviceAmount = serviceAmount;
    }
}
