package in.msewa.model;

/**
 * Created by Santosh Kumar on 8/1/2017.
 */

public class ShoppingMyOrderModel {

    private String date;
    private String totAmount;
    private String TranxNo;
    private String pName;
    private String pPrice;
    private String pImage;
    private String OrderId;
    private String pCgst;
    private String pSgst;

    public ShoppingMyOrderModel() {
    }


    public ShoppingMyOrderModel(String pName, String pPrice, String pImage, String pCgst, String pSgst ) {
        this.pName = pName;
        this.pPrice = pPrice;
        this.pImage = pImage;
        this.pCgst = pCgst;
        this.pSgst = pSgst;
    }


//    public ShoppingMyOrderModel(String date, String totAmount, String tranxNo) {
//        this.date = date;
//        this.totAmount = totAmount;
//        TranxNo = tranxNo;
//    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTotAmount() {
        return totAmount;
    }

    public void setTotAmount(String totAmount) {
        this.totAmount = totAmount;
    }

    public String getTranxNo() {
        return TranxNo;
    }

    public void setTranxNo(String tranxNo) {
        TranxNo = tranxNo;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpPrice() {
        return pPrice;
    }

    public void setpPrice(String pPrice) {
        this.pPrice = pPrice;
    }

    public String getpImage() {
        return pImage;
    }

    public void setpImage(String pImage) {
        this.pImage = pImage;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getpCgst() {
        return pCgst;
    }

    public void setpCgst(String pCgst) {
        this.pCgst = pCgst;
    }

    public String getpSgst() {
        return pSgst;
    }

    public void setpSgst(String pSgst) {
        this.pSgst = pSgst;
    }
}
