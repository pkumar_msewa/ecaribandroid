package in.msewa.model;

import java.util.List;

/**
 * Created by acer on 10-07-2017.
 */

public class AddOnsModel {


    private List <CarAdons> carAdons;
    private List <BusAdons> busAdons;
    private photoAddonDetail photoAddonDetail;
    private snowMagicaAddOn snowMagicaAddOn;
    private List <BrunchAdons> brunchAdons;

    public AddOnsModel(List<CarAdons> carAdons, List<BusAdons> busAdons, photoAddonDetail photoAddonDetail, snowMagicaAddOn snowMagicaAddOn, List<BrunchAdons> brunAdons) {
        this.carAdons = carAdons;
        this.busAdons = busAdons;
        this.photoAddonDetail = photoAddonDetail;
        this.snowMagicaAddOn = snowMagicaAddOn;
        this.brunchAdons = brunAdons;

    }

    public AddOnsModel.photoAddonDetail getPhotoAddonDetail() {
        return photoAddonDetail;
    }

    public void setPhotoAddonDetail(AddOnsModel.photoAddonDetail photoAddonDetail) {
        this.photoAddonDetail = photoAddonDetail;
    }

    public AddOnsModel.snowMagicaAddOn getSnowMagicaAddOn() {
        return snowMagicaAddOn;
    }

    public void setSnowMagicaAddOn(AddOnsModel.snowMagicaAddOn snowMagicaAddOn) {
        this.snowMagicaAddOn = snowMagicaAddOn;
    }

    public List<CarAdons> getCarAdons() {
        return carAdons;
    }

    public void setCarAdons(List<CarAdons> carAdons) {
        this.carAdons = carAdons;
    }

    public List<BusAdons> getBusAdons() {
        return busAdons;
    }

    public void setBusAdons(List<BusAdons> busAdons) {
        this.busAdons = busAdons;
    }


    public List<BrunchAdons> getBrunAdons() {
        return brunchAdons;
    }

    public void setBrunAdons(List<BrunchAdons> brunAdons) {
        this.brunchAdons = brunAdons;
    }


    public static class CarAdons{
         String typeId,type,currency,quantity,totalAddOnCost,addOnCost,ctitle,aid,imageUr, carAdTitle;
        private List <taxDetails> taxdetails;

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getTotalAddOnCost() {
            return totalAddOnCost;
        }

        public void setTotalAddOnCost(String totalAddOnCost) {this.totalAddOnCost = totalAddOnCost;}

        public String getAddOnCost() {
            return addOnCost;
        }

        public void setAddOnCost(String addOnCost) {
            this.addOnCost = addOnCost;
        }

        public String getcTitle() {return ctitle;}

        public void setcTitle(String ctitle) {
            this.ctitle = ctitle;
        }

        public String getAid() {
            return aid;
        }

        public void setAid(String aid) {
            this.aid = aid;
        }

        public String getImageUr() {
            return imageUr;
        }

        public void setImageUr(String imageUr) {
            this.imageUr = imageUr;
        }

        public String getCarAdTitle() {
            return carAdTitle;
        }

        public void setCarAdTitle(String carAdTitle) {
            this.carAdTitle = carAdTitle;
        }

        public List<taxDetails> getTaxdetails() {
            return taxdetails;
        }

        public void setTaxdetails(List<taxDetails> taxdetails) {
            this.taxdetails = taxdetails;
        }

        public static class taxDetails{
            String name,pricePerQty;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPricePerQty() {
                return pricePerQty;
            }

            public void setPricePerQty(String pricePerQty) {
                this.pricePerQty = pricePerQty;
            }
        }
    }
    public static class BusAdons {
        String typeId, type, currency, quantity, totalAddOnCost, addOnCost, title, aid, imageUr,placeId,placeName;
        private List<taxDetails> taxdetails;
        private List<locationList> locationList;

       public String getPlaceId() {
           return placeId;
       }

       public void setPlaceId(String placeId) {
           this.placeId = placeId;
       }

       public String getPlaceName() {
           return placeName;
       }

       public void setPlaceName(String placeName) {
           this.placeName = placeName;
       }

       public String getTypeId() {
           return typeId;
       }

       public void setTypeId(String typeId) {
           this.typeId = typeId;
       }

       public String getType() {
           return type;
       }

       public void setType(String type) {
           this.type = type;
       }

       public String getCurrency() {
           return currency;
       }

       public void setCurrency(String currency) {
           this.currency = currency;
       }

       public String getQuantity() {
           return quantity;
       }

       public void setQuantity(String quantity) {
           this.quantity = quantity;
       }

       public String getTotalAddOnCost() {
           return totalAddOnCost;
       }

       public void setTotalAddOnCost(String totalAddOnCost) {
           this.totalAddOnCost = totalAddOnCost;
       }

       public String getAddOnCost() {
           return addOnCost;
       }

       public void setAddOnCost(String addOnCost) {
           this.addOnCost = addOnCost;
       }

       public String getTitle() {
           return title;
       }

       public void setTitle(String title) {
           this.title = title;
       }

       public String getAid() {
           return aid;
       }

       public void setAid(String aid) {
           this.aid = aid;
       }

       public String getImageUr() {
           return imageUr;
       }

       public void setImageUr(String imageUr) {
           this.imageUr = imageUr;
       }

       public List<taxDetails> getTaxdetails() {
           return taxdetails;
       }

       public void setTaxdetails(List<taxDetails> taxdetails) {
           this.taxdetails = taxdetails;
       }

       public List<BusAdons.locationList> getLocationList() {
           return locationList;
       }

       public void setLocationList(List<BusAdons.locationList> locationList) {
           this.locationList = locationList;
       }

       public static class taxDetails {
            String name, pricePerQty;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPricePerQty() {
                return pricePerQty;
            }

            public void setPricePerQty(String pricePerQty) {
                this.pricePerQty = pricePerQty;
            }
        }
        public static class locationList {
            String locationId,locationName;

            public String getLocationId() {
                return locationId;
            }

            public void setLocationId(String locationId) {
                this.locationId = locationId;
            }

            public String getLocationName() {
                return locationName;
            }

            public void setLocationName(String locationName) {
                this.locationName = locationName;
            }
        }
    }

    public static class photoAddonDetail{
      String typeId,type,Photo,currency,quantity,totalAddOnCost,addOnCost,title,aid,imageUrl;
        private List<taxDetails> taxdetails;

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String photo) {
            Photo = photo;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getTotalAddOnCost() {
            return totalAddOnCost;
        }

        public void setTotalAddOnCost(String totalAddOnCost) {this.totalAddOnCost = totalAddOnCost;}

        public String getAddOnCost() {
            return addOnCost;
        }

        public void setAddOnCost(String addOnCost) {
            this.addOnCost = addOnCost;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAid() {
            return aid;
        }

        public void setAid(String aid) {
            this.aid = aid;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public List<taxDetails> getTaxdetails() {
            return taxdetails;
        }

        public void setTaxdetails(List<taxDetails> taxdetails) {
            this.taxdetails = taxdetails;
        }

        public static class taxDetails{
           String name, pricePerQty;

            public String getName() {return name;}

            public void setName(String name) {this.name = name;}

            public String getPricePerQty() {return pricePerQty;}

            public void setPricePerQty(String pricePerQty) {this.pricePerQty = pricePerQty;}
        }
    }

    public static class snowMagicaAddOn{
        String prodType,ticketDescription,title,imageUrl,currency;
        private ticketType ticketType;
        private List<sessionList> sessionlist;

        public String getProdType() {
            return prodType;
        }

        public void setProdType(String prodType) {
            this.prodType = prodType;
        }

        public String getTicketDescription() {
            return ticketDescription;
        }

        public void setTicketDescription(String ticketDescription) {
            this.ticketDescription = ticketDescription;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public AddOnsModel.snowMagicaAddOn.ticketType getTicketType() {
            return ticketType;
        }

        public void setTicketType(AddOnsModel.snowMagicaAddOn.ticketType ticketType) {
            this.ticketType = ticketType;
        }

        public List<sessionList> getSessionlist() {
            return sessionlist;
        }

        public void setSessionlist(List<sessionList> sessionlist) {
            this.sessionlist = sessionlist;
        }

        public static class sessionList{
           String sessionId,sessionName;
            private List<timeSlots> timelots;

            public String getSessionId() {
                return sessionId;
            }

            public void setSessionId(String sessionId) {
                this.sessionId = sessionId;
            }

            public String getSessionName() {
                return sessionName;
            }

            public void setSessionName(String sessionName) {
                this.sessionName = sessionName;
            }

            public List<timeSlots> getTimelots() {
                return timelots;
            }

            public void setTimelots(List<timeSlots> timelots) {
                this.timelots = timelots;
            }

            public static class timeSlots{
              String  slotId,slotName;

                public String getSlotId() {
                    return slotId;
                }

                public void setSlotId(String slotId) {
                    this.slotId = slotId;
                }

                public String getSlotName() {
                    return slotName;
                }

                public void setSlotName(String slotName) {
                    this.slotName = slotName;
                }
            }
        }

        public static class ticketType{
           String type,quantity,costPerQty,totalCost,productId,plu;
            private List<taxDetailsList>taxDetailsList;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getCostPerQty() {
                return costPerQty;
            }

            public void setCostPerQty(String costPerQty) {
                this.costPerQty = costPerQty;
            }

            public String getTotalCost() {
                return totalCost;
            }

            public void setTotalCost(String totalCost) {
                this.totalCost = totalCost;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getPlu() {
                return plu;
            }

            public void setPlu(String plu) {
                this.plu = plu;
            }

            public List<AddOnsModel.snowMagicaAddOn.ticketType.taxDetailsList> getTaxDetailsList() {
                return taxDetailsList;
            }

            public void setTaxDetailsList(List<AddOnsModel.snowMagicaAddOn.ticketType.taxDetailsList> taxDetailsList) {
                this.taxDetailsList = taxDetailsList;
            }

            public static class taxDetailsList{
                String name,pricePerQty;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getPricePerQty() {
                    return pricePerQty;
                }

                public void setPricePerQty(String pricePerQty) {
                    this.pricePerQty = pricePerQty;
                }
            }
        }
    }

    public static class BrunchAdons{
        String currencyCode,productId,quantity,totalCost,costPer,imageUrl,description,title,aid,bruchTitle;
        private List <taxDetailList> taxDetailList;

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(String totalCost) {
            this.totalCost = totalCost;
        }

        public String getCostPer() {
            return costPer;
        }

        public void setCostPer(String costPer) {
            this.costPer = costPer;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAid() {
            return aid;
        }

        public void setAid(String aid) {
            this.aid = aid;
        }

        public String getBruchTitle() {
            return bruchTitle;
        }

        public void setBruchTitle(String bruchTitle) {
            this.bruchTitle = bruchTitle;
        }

        public List<BrunchAdons.taxDetailList> getTaxDetailList() {
            return taxDetailList;
        }

        public void setTaxDetailList(List<BrunchAdons.taxDetailList> taxDetailList) {
            this.taxDetailList = taxDetailList;
        }

        public static class taxDetailList{
            String name,pricePerQty;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPricePerQty() {
                return pricePerQty;
            }

            public void setPricePerQty(String pricePerQty) {
                this.pricePerQty = pricePerQty;
            }
        }
    }
}



