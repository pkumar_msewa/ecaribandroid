package in.msewa.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kashif-PC on 12/21/2016.
 */
public class FoodCart {
    private volatile static FoodCart uniqueInstance;

    /*
     * HashMap<ProductId, ProductModel>
     */
    private HashMap<Long, FoodModel> productsInCart = new HashMap<>();
    private double totalCost;
    public static boolean isCartCreated = false;

    private boolean isCartUpdated = false;

    /*
     * Singleton
     */
    private FoodCart() {}

    /*
     * Singleton getInstance()
     */
    public static FoodCart getInstance() {
        if (uniqueInstance == null) {
            synchronized (FoodCart.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new FoodCart();
                }
            }
        }
        return uniqueInstance;
    }

    public HashMap<Long, FoodModel> getProductsInCart() {
        return productsInCart;
    }

    public ArrayList<FoodModel> getProductsInCartArray() {
        return new ArrayList<FoodModel>(productsInCart.values());
    }

    public void setProductsInCart(HashMap<Long, FoodModel> productsInCart) {
        this.productsInCart = productsInCart;
    }

    public static FoodCart getUniqueInstance() {
        return uniqueInstance;
    }

    public String getTotalCost() {
        totalCost = 0;
        for (FoodModel product : getProductsInCart().values()) {
            String correctedPrice = product.getpApplicablePrice().replaceAll("[^\\d]", "");
            totalCost = Double.parseDouble(correctedPrice) + totalCost;
        }
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        return formatter.format(totalCost);
    }

    public String getProductInCartNames() {
        String productsName = "";
        for (FoodModel product : getProductsInCart().values()) {
            productsName = productsName + product.getpName();
        }
        return productsName;
    }

    public String getTotalCostNumber() {
        double cost = 0;
        for (FoodModel product : getProductsInCart().values()) {
            String correctedPrice = product.getpApplicablePrice().replaceAll("[^\\d]", "");
            cost = Double.parseDouble(correctedPrice) + cost;
        }
        return String.valueOf(cost);
    }

    /*
     * Automatically handle add and remove from cart
     */
    public boolean addProductsInCart(FoodModel product) {
        if (product != null) {
            if (productsInCart.containsKey(product.getpId())) {
                productsInCart.remove(product.getpId());
                return false;
            } else {
                productsInCart.put(product.getpId(), product);
                return true;
            }
        } else {
            return false;
        }
    }

    public void createNewCart(List<FoodModel> products) {
        productsInCart = new HashMap<Long, FoodModel>();
        try {
            if (products != null && products.size() != 0) {
                for (FoodModel product : products) {
                    if (product.getpId() != 0) {
                        productsInCart.put(product.getpId(), product);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isProductInCart(FoodModel product) {
        return productsInCart.containsKey(product.getpId());
    }

    public boolean isCartUpdated() {
        return isCartUpdated;
    }

    public void setCartUpdated(boolean isCartUpdated) {
        this.isCartUpdated = isCartUpdated;
    }

    public void clearCart() {
        isCartUpdated = false;
        productsInCart = new HashMap<Long, FoodModel>();
    }

}
