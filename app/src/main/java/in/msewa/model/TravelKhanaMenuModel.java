package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Azhar on 10/7/2017.
 */

public class TravelKhanaMenuModel implements Parcelable {
  public String itemName;
  public String image;
  public double customerPayable;
  public long itemId;
  public String menuTag;
  public String description;
  public int quantity;
  public String time;
  public String label;

  public TravelKhanaMenuModel(String itemName, String image, double customerPayable, long itemId, String menuTag, String description, int quantity, String time, String label) {
    this.itemName = itemName;
    this.image = image;
    this.customerPayable = customerPayable;
    this.itemId = itemId;
    this.menuTag = menuTag;
    this.description = description;
    this.quantity = quantity;
    this.time = time;
    this.label = label;
  }

  protected TravelKhanaMenuModel(Parcel in) {
    itemName = in.readString();
    image = in.readString();
    customerPayable = in.readDouble();
    itemId = in.readLong();
    menuTag = in.readString();
    description = in.readString();
    quantity = in.readInt();
    time = in.readString();
    label = in.readString();
  }

  public static final Creator<TravelKhanaMenuModel> CREATOR = new Creator<TravelKhanaMenuModel>() {
    @Override
    public TravelKhanaMenuModel createFromParcel(Parcel in) {
      return new TravelKhanaMenuModel(in);
    }

    @Override
    public TravelKhanaMenuModel[] newArray(int size) {
      return new TravelKhanaMenuModel[size];
    }
  };

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public double getCustomerPayable() {
    return customerPayable;
  }

  public void setCustomerPayable(double customerPayable) {
    this.customerPayable = customerPayable;
  }

  public long getItemId() {
    return itemId;
  }

  public void setItemId(long itemId) {
    this.itemId = itemId;
  }

  public String getMenuTag() {
    return menuTag;
  }

  public void setMenuTag(String menuTag) {
    this.menuTag = menuTag;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(itemName);
    dest.writeString(image);
    dest.writeDouble(customerPayable);
    dest.writeLong(itemId);
    dest.writeString(menuTag);
    dest.writeString(description);
    dest.writeInt(quantity);
    dest.writeString(time);
    dest.writeString(label);
  }
}
