package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 3/16/2016.
 */
public class UserModel extends SugarRecord implements Parcelable {
  public String userSessionId;
  public String userFirstName;
  public String userLastName;
  public String userMobileNo;
  public String userEmail;
  public String userAddress;
  public String emailIsActive;

  public String userAcNo;
  public String userBalance;
  public String userDailyLimit;
  public String userMonthlyLimit;
  public String userBalanceLimit;
  public String userAcName;
  public String userAcCode;
  public String userImage;
  public String userPoints;
  public int isValid;

  public String userDob;
  public String userGender, nikiOffer;
  public boolean isMPin, hasRefer, ebsEnable, vnetEnable, razorPayEnable;
  public boolean iplEnable;
  public String mdexToken, mdexKey, iplPrediction, iplSchedule, iplMyPrediction;

  private volatile static UserModel uniqueInstance;

  public UserModel() {
  }


  public UserModel(String userSessionId, String userFirstName, String userLastName, String userMobileNo, String userEmail, String emailIsActive, int isValid, String userAcNo, String userAcName, String userAcCode, String userBalance, String userBalanceLimit, String userDailyLimit, String userMonthlyLimit, String userAddress, String userImage, String userDob, String userGender, boolean isMPin, String userPoints, boolean hasRefer, String nikiOffer, boolean iplEnable, String iplMyPrediction, String iplPrediction, String iplSchedule, String mdexKey, String mdexToken, boolean ebsEnable, boolean vnetEnable, boolean razorPayEnable) {
    this.userSessionId = userSessionId;
    this.userFirstName = userFirstName;
    this.userLastName = userLastName;
    this.userMobileNo = userMobileNo;
    this.userEmail = userEmail;
    this.emailIsActive = emailIsActive;
    this.isValid = isValid;
    this.isMPin = isMPin;
    this.userDob = userDob;
    this.userGender = userGender;
    this.userAcNo = userAcNo;
    this.userBalance = userBalance;
    this.userBalanceLimit = userBalanceLimit;
    this.userDailyLimit = userDailyLimit;
    this.userMonthlyLimit = userMonthlyLimit;
    this.userAcCode = userAcCode;
    this.userAcName = userAcName;
    this.userAddress = userAddress;
    this.userImage = userImage;
    this.userPoints = userPoints;
    this.hasRefer = hasRefer;
    this.nikiOffer = nikiOffer;
    this.iplEnable = iplEnable;
    this.iplSchedule = iplSchedule;
    this.iplPrediction = iplPrediction;
    this.iplMyPrediction = iplMyPrediction;
    this.mdexKey = mdexKey;
    this.mdexToken = mdexToken;
    this.ebsEnable = ebsEnable;
    this.vnetEnable = vnetEnable;
    this.razorPayEnable = razorPayEnable;
  }


  public static UserModel getInstance() {
    if (uniqueInstance == null)
      uniqueInstance = new UserModel();

    return uniqueInstance;
  }

  protected UserModel(Parcel in) {
    userSessionId = in.readString();
    userFirstName = in.readString();
    userLastName = in.readString();
    userMobileNo = in.readString();
    userEmail = in.readString();
    userAddress = in.readString();
    emailIsActive = in.readString();
    userAcNo = in.readString();
    userBalance = in.readString();
    userDailyLimit = in.readString();
    userMonthlyLimit = in.readString();
    userBalanceLimit = in.readString();
    userAcName = in.readString();
    userAcCode = in.readString();
    userImage = in.readString();
    userPoints = in.readString();
    isValid = in.readInt();
    userDob = in.readString();
    userGender = in.readString();
    nikiOffer = in.readString();
    isMPin = in.readByte() != 0;
    hasRefer = in.readByte() != 0;
    ebsEnable = in.readByte() != 0;
    vnetEnable = in.readByte() != 0;
    razorPayEnable = in.readByte() != 0;
    iplEnable = in.readByte() != 0;
    mdexToken = in.readString();
    mdexKey = in.readString();
    iplPrediction = in.readString();
    iplSchedule = in.readString();
    iplMyPrediction = in.readString();
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(userSessionId);
    dest.writeString(userFirstName);
    dest.writeString(userLastName);
    dest.writeString(userMobileNo);
    dest.writeString(userEmail);
    dest.writeString(userAddress);
    dest.writeString(emailIsActive);
    dest.writeString(userAcNo);
    dest.writeString(userBalance);
    dest.writeString(userDailyLimit);
    dest.writeString(userMonthlyLimit);
    dest.writeString(userBalanceLimit);
    dest.writeString(userAcName);
    dest.writeString(userAcCode);
    dest.writeString(userImage);
    dest.writeString(userPoints);
    dest.writeInt(isValid);
    dest.writeString(userDob);
    dest.writeString(userGender);
    dest.writeString(nikiOffer);
    dest.writeByte((byte) (isMPin ? 1 : 0));
    dest.writeByte((byte) (hasRefer ? 1 : 0));
    dest.writeByte((byte) (ebsEnable ? 1 : 0));
    dest.writeByte((byte) (vnetEnable ? 1 : 0));
    dest.writeByte((byte) (razorPayEnable ? 1 : 0));
    dest.writeByte((byte) (iplEnable ? 1 : 0));
    dest.writeString(mdexToken);
    dest.writeString(mdexKey);
    dest.writeString(iplPrediction);
    dest.writeString(iplSchedule);
    dest.writeString(iplMyPrediction);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
    @Override
    public UserModel createFromParcel(Parcel in) {
      return new UserModel(in);
    }

    @Override
    public UserModel[] newArray(int size) {
      return new UserModel[size];
    }
  };

  public String getUserSessionId() {
    return userSessionId;
  }

  public void setUserSessionId(String userSessionId) {
    this.userSessionId = userSessionId;
  }

  public String getUserFirstName() {
    return userFirstName;
  }

  public void setUserFirstName(String userFirstName) {
    this.userFirstName = userFirstName;
  }

  public String getUserLastName() {
    return userLastName;
  }

  public void setUserLastName(String userLastName) {
    this.userLastName = userLastName;
  }

  public String getUserMobileNo() {
    return userMobileNo;
  }

  public void setUserMobileNo(String userMobileNo) {
    this.userMobileNo = userMobileNo;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  public String getUserAddress() {
    return userAddress;
  }

  public void setUserAddress(String userAddress) {
    this.userAddress = userAddress;
  }

  public String getEmailIsActive() {
    return emailIsActive;
  }

  public void setEmailIsActive(String emailIsActive) {
    this.emailIsActive = emailIsActive;
  }

  public String getUserAcNo() {
    return userAcNo;
  }

  public void setUserAcNo(String userAcNo) {
    this.userAcNo = userAcNo;
  }

  public String getUserBalance() {
    return userBalance;
  }

  public void setUserBalance(String userBalance) {
    this.userBalance = userBalance;
  }

  public String getUserDailyLimit() {
    return userDailyLimit;
  }

  public void setUserDailyLimit(String userDailyLimit) {
    this.userDailyLimit = userDailyLimit;
  }

  public String getUserMonthlyLimit() {
    return userMonthlyLimit;
  }

  public void setUserMonthlyLimit(String userMonthlyLimit) {
    this.userMonthlyLimit = userMonthlyLimit;
  }

  public String getUserBalanceLimit() {
    return userBalanceLimit;
  }

  public void setUserBalanceLimit(String userBalanceLimit) {
    this.userBalanceLimit = userBalanceLimit;
  }

  public String getUserAcName() {
    return userAcName;
  }

  public void setUserAcName(String userAcName) {
    this.userAcName = userAcName;
  }

  public String getUserAcCode() {
    return userAcCode;
  }

  public void setUserAcCode(String userAcCode) {
    this.userAcCode = userAcCode;
  }

  public String getUserImage() {
    return userImage;
  }

  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  public String getUserPoints() {
    return userPoints;
  }

  public void setUserPoints(String userPoints) {
    this.userPoints = userPoints;
  }

  public int getIsValid() {
    return isValid;
  }

  public void setIsValid(int isValid) {
    this.isValid = isValid;
  }

  public String getUserDob() {
    return userDob;
  }

  public void setUserDob(String userDob) {
    this.userDob = userDob;
  }

  public String getUserGender() {
    return userGender;
  }

  public void setUserGender(String userGender) {
    this.userGender = userGender;
  }

  public String getNikiOffer() {
    return nikiOffer;
  }

  public void setNikiOffer(String nikiOffer) {
    this.nikiOffer = nikiOffer;
  }

  public boolean isMPin() {
    return isMPin;
  }

  public void setMPin(boolean MPin) {
    isMPin = MPin;
  }

  public boolean isHasRefer() {
    return hasRefer;
  }

  public void setHasRefer(boolean hasRefer) {
    this.hasRefer = hasRefer;
  }

  public boolean isEbsEnable() {
    return ebsEnable;
  }

  public void setEbsEnable(boolean ebsEnable) {
    this.ebsEnable = ebsEnable;
  }

  public boolean isVnetEnable() {
    return vnetEnable;
  }

  public void setVnetEnable(boolean vnetEnable) {
    this.vnetEnable = vnetEnable;
  }

  public boolean isRazorPayEnable() {
    return razorPayEnable;
  }

  public void setRazorPayEnable(boolean razorPayEnable) {
    this.razorPayEnable = razorPayEnable;
  }

  public boolean isIplEnable() {
    return iplEnable;
  }

  public void setIplEnable(boolean iplEnable) {
    this.iplEnable = iplEnable;
  }

  public String getMdexToken() {
    return mdexToken;
  }

  public void setMdexToken(String mdexToken) {
    this.mdexToken = mdexToken;
  }

  public String getMdexKey() {
    return mdexKey;
  }

  public void setMdexKey(String mdexKey) {
    this.mdexKey = mdexKey;
  }

  public String getIplPrediction() {
    return iplPrediction;
  }

  public void setIplPrediction(String iplPrediction) {
    this.iplPrediction = iplPrediction;
  }

  public String getIplSchedule() {
    return iplSchedule;
  }

  public void setIplSchedule(String iplSchedule) {
    this.iplSchedule = iplSchedule;
  }

  public String getIplMyPrediction() {
    return iplMyPrediction;
  }

  public void setIplMyPrediction(String iplMyPrediction) {
    this.iplMyPrediction = iplMyPrediction;
  }
}
