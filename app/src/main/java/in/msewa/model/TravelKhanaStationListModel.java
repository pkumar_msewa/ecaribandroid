package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Azhar on 10/7/2017.
 */

public class TravelKhanaStationListModel implements Parcelable {

    public String stationCode;
    public String arrivalTime;
    public String dateAtStation;
    public String departureTime;
    public String stationName;

    public TravelKhanaStationListModel(String stationCode, String arrivalTime, String dateAtStation, String departureTime, String stationName) {
        this.stationCode = stationCode;
        this.arrivalTime = arrivalTime;
        this.dateAtStation = dateAtStation;
        this.departureTime = departureTime;
        this.stationName = stationName;
    }

    protected TravelKhanaStationListModel(Parcel in) {
        stationCode = in.readString();
        arrivalTime = in.readString();
        dateAtStation = in.readString();
        departureTime = in.readString();
        stationName = in.readString();
    }

    public static final Creator<TravelKhanaStationListModel> CREATOR = new Creator<TravelKhanaStationListModel>() {
        @Override
        public TravelKhanaStationListModel createFromParcel(Parcel in) {
            return new TravelKhanaStationListModel(in);
        }

        @Override
        public TravelKhanaStationListModel[] newArray(int size) {
            return new TravelKhanaStationListModel[size];
        }
    };

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDateAtStation() {
        return dateAtStation;
    }

    public void setDateAtStation(String dateAtStation) {
        this.dateAtStation = dateAtStation;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stationCode);
        dest.writeString(arrivalTime);
        dest.writeString(dateAtStation);
        dest.writeString(departureTime);
        dest.writeString(stationName);
    }
}
