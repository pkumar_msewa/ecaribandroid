package in.msewa.model;

import com.orm.SugarRecord;

/**
 * Created by acer on 20-06-2017.
 */

public class ListcomponentModel extends SugarRecord{

    private long id;
    private String name;
    private boolean valueNew;

    public ListcomponentModel(long id, String name, boolean valueNew) {
        this.id = id;
        this.name = name;
        this.valueNew = valueNew;
    }


    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isValueNew() {
        return valueNew;
    }

    public void setValueNew(boolean valueNew) {
        this.valueNew = valueNew;
    }
}
