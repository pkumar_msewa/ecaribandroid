package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import java.util.ArrayList;

/**
 * Created by Ksf on 9/30/2016.
 */
public class BusListModel extends SugarRecord implements Parcelable {
  private String busName;
  private String busDepTime;
  private String busArrivalTime;
  private String busType;
  private String busFare;
  private String busAvailableSeat;
  private String busDuration;
  private String busTravel;
  private String jnrDate;
  private int engineId;
  private boolean seater;
  private boolean sleeper;

  //Other
  private String busProvider;
  private String tripId;

  private String busSourceName;
  private String busDesName;
  private String busConvenienceFee;
  private String busCancellationPolicy;
  private String busPartialCancellation;

  private ArrayList<BusBoardingPointModel> busBoardingPointModels;
  private ArrayList<BusBoardingPointModel> busDroppingPointModels;

  //more params
  private String routeId;
  private double discount;
  private double commission;
  private double markup;
  private boolean ac;
  private boolean nonAC;

  private String bpId, dpId;
  private Boolean bpDpLayout;

  public BusListModel(String busName, String busDepTime, String busArrivalTime, String busType, String busFare, String busAvailableSeat, String busDuration, String busProvider, String tripId, String busTravel, ArrayList<BusBoardingPointModel> busBoardingPointModels, ArrayList<BusBoardingPointModel> busDroppingPointModels, String busSourceName, String busDesName, String busConvenienceFee, String busCancellationPolicy, String busPartialCancellation, String jnrDate, int engineId, boolean seater, boolean sleeper, String routeId, double discount, double commission, double markup, boolean ac, boolean nonAC, String bpId, String dpId, Boolean bpDpLayout) {
    this.busName = busName;
    this.busArrivalTime = busArrivalTime;
    this.busDepTime = busDepTime;
    this.busType = busType;
    this.busFare = busFare;
    this.busAvailableSeat = busAvailableSeat;
    this.busDuration = busDuration;
    this.tripId = tripId;
    this.busProvider = busProvider;
    this.busTravel = busTravel;
    this.busBoardingPointModels = busBoardingPointModels;
    this.busDroppingPointModels = busDroppingPointModels;
    this.busSourceName = busSourceName;
    this.busDesName = busDesName;
    this.busConvenienceFee = busConvenienceFee;
    this.busCancellationPolicy = busCancellationPolicy;
    this.busPartialCancellation = busPartialCancellation;
    this.jnrDate = jnrDate;
    this.engineId = engineId;
    this.seater = seater;
    this.sleeper = sleeper;
    this.routeId = routeId;
    this.discount = discount;
    this.commission = commission;
    this.markup = markup;
    this.ac = ac;
    this.nonAC = nonAC;
    this.bpDpLayout = bpDpLayout;
    this.bpId = bpId;
    this.dpId = dpId;
  }

  protected BusListModel(Parcel in) {
    busName = in.readString();
    busDepTime = in.readString();
    busArrivalTime = in.readString();
    busType = in.readString();
    busFare = in.readString();
    busAvailableSeat = in.readString();
    busDuration = in.readString();
    busTravel = in.readString();
    jnrDate = in.readString();
    engineId = in.readInt();
    seater = in.readByte() != 0;
    sleeper = in.readByte() != 0;
    busProvider = in.readString();
    tripId = in.readString();
    busSourceName = in.readString();
    busDesName = in.readString();
    busConvenienceFee = in.readString();
    busCancellationPolicy = in.readString();
    busPartialCancellation = in.readString();
    busBoardingPointModels = in.createTypedArrayList(BusBoardingPointModel.CREATOR);
    busDroppingPointModels = in.createTypedArrayList(BusBoardingPointModel.CREATOR);
    routeId = in.readString();
    discount = in.readDouble();
    commission = in.readDouble();
    markup = in.readDouble();
    ac = in.readByte() != 0;
    nonAC = in.readByte() != 0;
    bpId = in.readString();
    dpId = in.readString();
    byte tmpBpDpLayout = in.readByte();
    bpDpLayout = tmpBpDpLayout == 0 ? null : tmpBpDpLayout == 1;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(busName);
    dest.writeString(busDepTime);
    dest.writeString(busArrivalTime);
    dest.writeString(busType);
    dest.writeString(busFare);
    dest.writeString(busAvailableSeat);
    dest.writeString(busDuration);
    dest.writeString(busTravel);
    dest.writeString(jnrDate);
    dest.writeInt(engineId);
    dest.writeByte((byte) (seater ? 1 : 0));
    dest.writeByte((byte) (sleeper ? 1 : 0));
    dest.writeString(busProvider);
    dest.writeString(tripId);
    dest.writeString(busSourceName);
    dest.writeString(busDesName);
    dest.writeString(busConvenienceFee);
    dest.writeString(busCancellationPolicy);
    dest.writeString(busPartialCancellation);
    dest.writeTypedList(busBoardingPointModels);
    dest.writeTypedList(busDroppingPointModels);
    dest.writeString(routeId);
    dest.writeDouble(discount);
    dest.writeDouble(commission);
    dest.writeDouble(markup);
    dest.writeByte((byte) (ac ? 1 : 0));
    dest.writeByte((byte) (nonAC ? 1 : 0));
    dest.writeString(bpId);
    dest.writeString(dpId);
    dest.writeByte((byte) (bpDpLayout == null ? 0 : bpDpLayout ? 1 : 2));
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<BusListModel> CREATOR = new Creator<BusListModel>() {
    @Override
    public BusListModel createFromParcel(Parcel in) {
      return new BusListModel(in);
    }

    @Override
    public BusListModel[] newArray(int size) {
      return new BusListModel[size];
    }
  };

  public String getBpId() {
    return bpId;
  }

  public void setBpId(String bpId) {
    this.bpId = bpId;
  }

  public String getDpId() {
    return dpId;
  }

  public void setDpId(String dpId) {
    this.dpId = dpId;
  }

  public Boolean getBpDpLayout() {
    return bpDpLayout;
  }

  public void setBpDpLayout(Boolean bpDpLayout) {
    this.bpDpLayout = bpDpLayout;
  }

  public String getBusName() {
    return busName;
  }

  public void setBusName(String busName) {
    this.busName = busName;
  }

  public String getBusDepTime() {
    return busDepTime;
  }

  public void setBusDepTime(String busDepTime) {
    this.busDepTime = busDepTime;
  }

  public String getBusArrivalTime() {
    return busArrivalTime;
  }

  public void setBusArrivalTime(String busArrivalTime) {
    this.busArrivalTime = busArrivalTime;
  }

  public String getBusType() {
    return busType;
  }

  public void setBusType(String busType) {
    this.busType = busType;
  }

  public String getBusFare() {
    return busFare;
  }

  public void setBusFare(String busFare) {
    this.busFare = busFare;
  }

  public String getBusAvailableSeat() {
    return busAvailableSeat;
  }

  public void setBusAvailableSeat(String busAvailableSeat) {
    this.busAvailableSeat = busAvailableSeat;
  }

  public String getBusDuration() {
    return busDuration;
  }

  public void setBusDuration(String busDuration) {
    this.busDuration = busDuration;
  }

  public String getBusTravel() {
    return busTravel;
  }

  public void setBusTravel(String busTravel) {
    this.busTravel = busTravel;
  }

  public String getJnrDate() {
    return jnrDate;
  }

  public void setJnrDate(String jnrDate) {
    this.jnrDate = jnrDate;
  }

  public String getBusProvider() {
    return busProvider;
  }

  public void setBusProvider(String busProvider) {
    this.busProvider = busProvider;
  }

  public String getTripId() {
    return tripId;
  }

  public void setTripId(String tripId) {
    this.tripId = tripId;
  }

  public String getBusSourceName() {
    return busSourceName;
  }

  public void setBusSourceName(String busSourceName) {
    this.busSourceName = busSourceName;
  }

  public String getBusDesName() {
    return busDesName;
  }

  public void setBusDesName(String busDesName) {
    this.busDesName = busDesName;
  }

  public String getBusConvenienceFee() {
    return busConvenienceFee;
  }

  public void setBusConvenienceFee(String busConvenienceFee) {
    this.busConvenienceFee = busConvenienceFee;
  }

  public String getBusCancellationPolicy() {
    return busCancellationPolicy;
  }

  public void setBusCancellationPolicy(String busCancellationPolicy) {
    this.busCancellationPolicy = busCancellationPolicy;
  }

  public String getBusPartialCancellation() {
    return busPartialCancellation;
  }

  public void setBusPartialCancellation(String busPartialCancellation) {
    this.busPartialCancellation = busPartialCancellation;
  }

  public ArrayList<BusBoardingPointModel> getBusBoardingPointModels() {
    return busBoardingPointModels;
  }

  public void setBusBoardingPointModels(ArrayList<BusBoardingPointModel> busBoardingPointModels) {
    this.busBoardingPointModels = busBoardingPointModels;
  }

  public ArrayList<BusBoardingPointModel> getBusDroppingPointModels() {
    return busDroppingPointModels;
  }

  public void setBusDroppingPointModels(ArrayList<BusBoardingPointModel> busDroppingPointModels) {
    this.busDroppingPointModels = busDroppingPointModels;
  }

  public int getEngineId() {
    return engineId;
  }

  public void setEngineId(int engineId) {
    this.engineId = engineId;
  }

  public boolean isSeater() {
    return seater;
  }

  public void setSeater(boolean seater) {
    this.seater = seater;
  }

  public boolean isSleeper() {
    return sleeper;
  }

  public void setSleeper(boolean sleeper) {
    this.sleeper = sleeper;
  }

  public String getRouteId() {
    return routeId;
  }

  public void setRouteId(String routeId) {
    this.routeId = routeId;
  }

  public double getDiscount() {
    return discount;
  }

  public void setDiscount(double discount) {
    this.discount = discount;
  }

  public double getCommission() {
    return commission;
  }

  public void setCommission(double commission) {
    this.commission = commission;
  }

  public double getMarkup() {
    return markup;
  }

  public void setMarkup(double markup) {
    this.markup = markup;
  }

  public boolean isAc() {
    return ac;
  }

  public void setAc(boolean ac) {
    this.ac = ac;
  }

  public boolean isNonAC() {
    return nonAC;
  }

  public void setNonAC(boolean nonAC) {
    this.nonAC = nonAC;
  }


}
