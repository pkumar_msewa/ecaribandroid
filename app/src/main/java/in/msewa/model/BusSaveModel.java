package in.msewa.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/29/2016.
 */
public class BusSaveModel extends SugarRecord {
    private long cityId;
    private String cityname;

    public BusSaveModel() {

    }

    public BusSaveModel(String cityname) {

        this.cityname = cityname;
    }


    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }
}
