package in.msewa.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/29/2016.
 */
public class TravelCityModel extends SugarRecord {
  private String cityId;
  private String cityname;

  public TravelCityModel() {

  }

  public TravelCityModel(String cityId, String cityname) {

    this.cityId = cityId;
    this.cityname = cityname;

  }

  public String getCityId() {
    return cityId;
  }

  public void setCityId(String cityId) {
    this.cityId = cityId;
  }

  public String getCityname() {
    return cityname;
  }

  public void setCityname(String cityname) {
    this.cityname = cityname;
  }
}
