package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 10/11/2016.
 */
public class FlightModel implements Parcelable {

  //Arrival and Departure
  private String depAirportCode;
  private String depTime;
  private String arrivalAirportCode;
  private String arrivalTime;
  private String flightDuration;

  private String flightArrivalTimeZone;
  private String flightDepartureTimeZone;

  private String flightArrivalAirportName;
  private String flightDepartureAirportName;

  //Flight name and no
  private String flightName;
  private String flightNo;
  private String flightImage;
  private String flightRule;
  private String flightCode;

  private int tripType;
  private String fligtType;

  private String jsrequestsegment;
  private String jsonFareString;
  private String baggageUnit, baggageWeght;
  private String journeyTime;

  public FlightModel(String depAirportCode, String depTime, String arrivalAirportCode, String arrivalTime, String flightDuration, String flightName,
                     String flightNo, String flightImage, String flightRule, String flightCode, String flightArrivalTimeZone, String flightDepartureTimeZone, String flightArrivalAirportName, String flightDepartureAirportName, int tripType, String flightType, String jsrequestsegment, String jsonFareString, String baggageUnit, String baggageWeght,String journeyTime) {

    this.depAirportCode = depAirportCode;
    this.depTime = depTime;
    this.arrivalAirportCode = arrivalAirportCode;
    this.arrivalTime = arrivalTime;
    this.flightDuration = flightDuration;
    this.flightName = flightName;
    this.flightArrivalTimeZone = flightArrivalTimeZone;
    this.flightDepartureTimeZone = flightDepartureTimeZone;
    this.fligtType = flightType;
    this.flightNo = flightNo;
    this.flightImage = flightImage;
    this.flightRule = flightRule;
    this.jsonFareString = jsonFareString;
    this.flightCode = flightCode;
    this.jsrequestsegment = jsrequestsegment;
    this.flightArrivalAirportName = flightArrivalAirportName;
    this.flightDepartureAirportName = flightDepartureAirportName;
    this.tripType = tripType;
    this.baggageUnit = baggageUnit;
    this.baggageWeght = baggageWeght;
    this.journeyTime=journeyTime;
  }

  protected FlightModel(Parcel in) {
    depAirportCode = in.readString();
    depTime = in.readString();
    arrivalAirportCode = in.readString();
    arrivalTime = in.readString();
    flightDuration = in.readString();
    flightArrivalTimeZone = in.readString();
    flightDepartureTimeZone = in.readString();
    flightArrivalAirportName = in.readString();
    flightDepartureAirportName = in.readString();
    flightName = in.readString();
    flightNo = in.readString();
    flightImage = in.readString();
    flightRule = in.readString();
    flightCode = in.readString();
    tripType = in.readInt();
    fligtType = in.readString();
    jsrequestsegment = in.readString();
    jsonFareString = in.readString();
    baggageUnit = in.readString();
    baggageWeght = in.readString();
    journeyTime = in.readString();
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(depAirportCode);
    dest.writeString(depTime);
    dest.writeString(arrivalAirportCode);
    dest.writeString(arrivalTime);
    dest.writeString(flightDuration);
    dest.writeString(flightArrivalTimeZone);
    dest.writeString(flightDepartureTimeZone);
    dest.writeString(flightArrivalAirportName);
    dest.writeString(flightDepartureAirportName);
    dest.writeString(flightName);
    dest.writeString(flightNo);
    dest.writeString(flightImage);
    dest.writeString(flightRule);
    dest.writeString(flightCode);
    dest.writeInt(tripType);
    dest.writeString(fligtType);
    dest.writeString(jsrequestsegment);
    dest.writeString(jsonFareString);
    dest.writeString(baggageUnit);
    dest.writeString(baggageWeght);
    dest.writeString(journeyTime);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<FlightModel> CREATOR = new Creator<FlightModel>() {
    @Override
    public FlightModel createFromParcel(Parcel in) {
      return new FlightModel(in);
    }

    @Override
    public FlightModel[] newArray(int size) {
      return new FlightModel[size];
    }
  };

  public String getDepAirportCode() {
    return depAirportCode;
  }

  public void setDepAirportCode(String depAirportCode) {
    this.depAirportCode = depAirportCode;
  }

  public String getDepTime() {
    return depTime;
  }

  public void setDepTime(String depTime) {
    this.depTime = depTime;
  }

  public String getArrivalAirportCode() {
    return arrivalAirportCode;
  }

  public void setArrivalAirportCode(String arrivalAirportCode) {
    this.arrivalAirportCode = arrivalAirportCode;
  }

  public String getArrivalTime() {
    return arrivalTime;
  }

  public void setArrivalTime(String arrivalTime) {
    this.arrivalTime = arrivalTime;
  }

  public String getFlightDuration() {
    return flightDuration;
  }

  public void setFlightDuration(String flightDuration) {
    this.flightDuration = flightDuration;
  }

  public String getFlightArrivalTimeZone() {
    return flightArrivalTimeZone;
  }

  public void setFlightArrivalTimeZone(String flightArrivalTimeZone) {
    this.flightArrivalTimeZone = flightArrivalTimeZone;
  }

  public String getFlightDepartureTimeZone() {
    return flightDepartureTimeZone;
  }

  public void setFlightDepartureTimeZone(String flightDepartureTimeZone) {
    this.flightDepartureTimeZone = flightDepartureTimeZone;
  }

  public String getFlightArrivalAirportName() {
    return flightArrivalAirportName;
  }

  public void setFlightArrivalAirportName(String flightArrivalAirportName) {
    this.flightArrivalAirportName = flightArrivalAirportName;
  }

  public String getFlightDepartureAirportName() {
    return flightDepartureAirportName;
  }

  public void setFlightDepartureAirportName(String flightDepartureAirportName) {
    this.flightDepartureAirportName = flightDepartureAirportName;
  }

  public String getFlightName() {
    return flightName;
  }

  public void setFlightName(String flightName) {
    this.flightName = flightName;
  }

  public String getFlightNo() {
    return flightNo;
  }

  public void setFlightNo(String flightNo) {
    this.flightNo = flightNo;
  }

  public String getFlightImage() {
    return flightImage;
  }

  public void setFlightImage(String flightImage) {
    this.flightImage = flightImage;
  }

  public String getFlightRule() {
    return flightRule;
  }

  public void setFlightRule(String flightRule) {
    this.flightRule = flightRule;
  }

  public String getFlightCode() {
    return flightCode;
  }

  public void setFlightCode(String flightCode) {
    this.flightCode = flightCode;
  }

  public int getTripType() {
    return tripType;
  }

  public void setTripType(int tripType) {
    this.tripType = tripType;
  }

  public String getFligtType() {
    return fligtType;
  }

  public void setFligtType(String fligtType) {
    this.fligtType = fligtType;
  }

  public String getJsrequestsegment() {
    return jsrequestsegment;
  }

  public void setJsrequestsegment(String jsrequestsegment) {
    this.jsrequestsegment = jsrequestsegment;
  }

  public String getJsonFareString() {
    return jsonFareString;
  }

  public void setJsonFareString(String jsonFareString) {
    this.jsonFareString = jsonFareString;
  }

  public String getBaggageUnit() {
    return baggageUnit;
  }

  public void setBaggageUnit(String baggageUnit) {
    this.baggageUnit = baggageUnit;
  }

  public String getBaggageWeght() {
    return baggageWeght;
  }

  public void setBaggageWeght(String baggageWeght) {
    this.baggageWeght = baggageWeght;
  }

  public String getJourneyTime() {
    return journeyTime;
  }

  public void setJourneyTime(String journeyTime) {
    this.journeyTime = journeyTime;
  }
}
