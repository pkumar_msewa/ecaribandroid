package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TicketDetail implements Parcelable {
    private String tittle;
    private String tdtype;
    private long tdquantity;
    private double tdcostPerQty;
    private double tdcostTotalQty;
    private long tdproductID;
    private long tdplu;
    private String taxNname;

    public TicketDetail() {
        super();
    }

    protected TicketDetail(Parcel in) {
        tittle = in.readString();
        tdtype = in.readString();
        tdquantity = in.readLong();
        tdcostPerQty = in.readDouble();
        tdcostTotalQty = in.readDouble();
        tdproductID = in.readLong();
        tdplu = in.readLong();
        taxNname = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tittle);
        dest.writeString(tdtype);
        dest.writeLong(tdquantity);
        dest.writeDouble(tdcostPerQty);
        dest.writeDouble(tdcostTotalQty);
        dest.writeLong(tdproductID);
        dest.writeLong(tdplu);
        dest.writeString(taxNname);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TicketDetail> CREATOR = new Creator<TicketDetail>() {
        @Override
        public TicketDetail createFromParcel(Parcel in) {
            return new TicketDetail(in);
        }

        @Override
        public TicketDetail[] newArray(int size) {
            return new TicketDetail[size];
        }
    };

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getTdtype() {
        return tdtype;
    }

    public void setTdtype(String tdtype) {
        this.tdtype = tdtype;
    }

    public long getTdquantity() {
        return tdquantity;
    }

    public void setTdquantity(long tdquantity) {
        this.tdquantity = tdquantity;
    }

    public double getTdcostPerQty() {
        return tdcostPerQty;
    }

    public void setTdcostPerQty(double tdcostPerQty) {
        this.tdcostPerQty = tdcostPerQty;
    }

    public double getTdcostTotalQty() {
        return tdcostTotalQty;
    }

    public void setTdcostTotalQty(double tdcostTotalQty) {
        this.tdcostTotalQty = tdcostTotalQty;
    }

    public long getTdproductID() {
        return tdproductID;
    }

    public void setTdproductID(long tdproductID) {
        this.tdproductID = tdproductID;
    }

    public long getTdplu() {
        return tdplu;
    }

    public void setTdplu(long tdplu) {
        this.tdplu = tdplu;
    }

    public String getTaxNname() {
        return taxNname;
    }

    public void setTaxNname(String taxNname) {
        this.taxNname = taxNname;
    }
}