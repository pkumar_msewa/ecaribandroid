package in.msewa.model;

import java.util.ArrayList;

/**
 * Created by kashifimam on 17/02/17.
 */

public class QRScanModel {

    private String Card_No;
    private String MerchantName;
    private String CountryCode;
    private String IndianRupeeCode;
    private String TransAmount;
    private String TerminalID;
    private String MvisaId;
    private String DefaultValue;
    private String MasterID;
    private String RuPayId;
    private String MCC;
    private String CityName;
    private String ExtraFields;
    private String TipID;
    private String FixedTipAmount;
    private String FixedTipPercentage;

    private ArrayList<String> ExtraFieldsList;

    public QRScanModel(){


    }

    public String getFixedTipAmount() {
        return FixedTipAmount;
    }

    public void setFixedTipAmount(String fixedTipAmount) {
        FixedTipAmount = fixedTipAmount;
    }

    public String getFixedTipPercentage() {
        return FixedTipPercentage;
    }

    public void setFixedTipPercentage(String fixedTipPercentage) {
        FixedTipPercentage = fixedTipPercentage;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getExtraFields() {
        return ExtraFields;
    }

    public void setExtraFields(String extraFields) {
        ExtraFields = extraFields;
    }

    public ArrayList<String> getExtraFieldsList() {
        return ExtraFieldsList;
    }

    public void setExtraFieldsList(ArrayList<String> extraFieldsList) {
        ExtraFieldsList = extraFieldsList;
    }

    public String getTipID() {
        return TipID;
    }

    public void setTipID(String tipID) {
        TipID = tipID;
    }

    public String getCard_No() {
        return Card_No;
    }

    public void setCard_No(String card_No) {
        Card_No = card_No;
    }

    public String getMerchantName() {
        return MerchantName;
    }

    public void setMerchantName(String merchantName) {
        MerchantName = merchantName;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getIndianRupeeCode() {
        return IndianRupeeCode;
    }

    public void setIndianRupeeCode(String indianRupeeCode) {
        IndianRupeeCode = indianRupeeCode;
    }

    public String getTransAmount() {
        return TransAmount;
    }

    public void setTransAmount(String transAmount) {
        TransAmount = transAmount;
    }

    public String getTerminalID() {
        return TerminalID;
    }

    public void setTerminalID(String terminalID) {
        TerminalID = terminalID;
    }

    public String getMvisaId() {
        return MvisaId;
    }

    public void setMvisaId(String mvisaId) {
        MvisaId = mvisaId;
    }

    public String getDefaultValue() {
        return DefaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        DefaultValue = defaultValue;
    }

    public String getMasterID() {
        return MasterID;
    }

    public void setMasterID(String masterID) {
        MasterID = masterID;
    }

    public String getRuPayId() {
        return RuPayId;
    }

    public void setRuPayId(String ruPayId) {
        RuPayId = ruPayId;
    }

    public String getMCC() {
        return MCC;
    }

    public void setMCC(String MCC) {
        this.MCC = MCC;
    }
}
