package in.msewa.model;

/**
 * Created by Azhar on 10/28/2017.
 */

public class TravelMyOrderModel {
    private String itemName;
    private String itemQuantity;

    public TravelMyOrderModel(String itemName, String itemQuantity) {
        this.itemName = itemName;
        this.itemQuantity = itemQuantity;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(String itemQuantity) {
        this.itemQuantity = itemQuantity;
    }
}
