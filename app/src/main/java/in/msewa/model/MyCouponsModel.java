package in.msewa.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 5/30/2016.
 */
public class MyCouponsModel extends SugarRecord {
    private String couponTitle;
    private String couponImage;
    private String couponEndDate;
    private String couponURL;

    private String couponAddress;
    private String couponVendorName;
    private String couponVendorID;
    private String couponDiscount;


    public MyCouponsModel(){

    }

    public MyCouponsModel(String couponTitle, String couponImage, String couponAddress, String couponVendorName, String couponEndDate, String couponURL, String couponVendorID, String couponDiscount){
        this.couponAddress = couponAddress;
        this.couponEndDate = couponEndDate;
        this.couponImage = couponImage;
        this.couponTitle = couponTitle;
        this.couponURL = couponURL;
        this.couponVendorID = couponVendorID;
        this.couponVendorName = couponVendorName;
        this.couponDiscount = couponDiscount;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getCouponTitle() {
        return couponTitle;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public String getCouponImage() {
        return couponImage;
    }

    public void setCouponImage(String couponImage) {
        this.couponImage = couponImage;
    }

    public String getCouponAddress() {
        return couponAddress;
    }

    public void setCouponAddress(String couponAddress) {
        this.couponAddress = couponAddress;
    }

    public String getCouponVendorName() {
        return couponVendorName;
    }

    public void setCouponVendorName(String couponVendorName) {
        this.couponVendorName = couponVendorName;
    }

    public String getCouponEndDate() {
        return couponEndDate;
    }

    public void setCouponEndDate(String couponEndDate) {
        this.couponEndDate = couponEndDate;
    }

    public String getCouponURL() {
        return couponURL;
    }

    public void setCouponURL(String couponURL) {
        this.couponURL = couponURL;
    }

    public String getCouponVendorID() {
        return couponVendorID;
    }

    public void setCouponVendorID(String couponVendorID) {
        this.couponVendorID = couponVendorID;
    }
}
