package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 3/31/2017.
 */
public class IplTeamModel implements Parcelable {
  private String matchDate;
  private String venue;
  private String firstTeam;
  private String secondTeam;
  private Object winningTeam;
  private String firstTeamId;
  private String secondTeamId;
  private String winningTeamId;
  private Boolean predictionEnable;
  private String additionalInfo;
  private String firstTeamLogo;
  private String secondTeamLogo;
  private String matchCode;
  private Boolean active;

  public IplTeamModel(String matchDate, String venue, String firstTeam, String secondTeam, Object winningTeam, String firstTeamId, String secondTeamId, String winningTeamId, Boolean predictionEnable, String additionalInfo, String firstTeamLogo, String secondTeamLogo, String matchCode, Boolean active) {
    this.matchDate = matchDate;
    this.venue = venue;
    this.firstTeam = firstTeam;
    this.secondTeam = secondTeam;
    this.winningTeam = winningTeam;
    this.firstTeamId = firstTeamId;
    this.secondTeamId = secondTeamId;
    this.winningTeamId = winningTeamId;
    this.predictionEnable = predictionEnable;
    this.additionalInfo = additionalInfo;
    this.firstTeamLogo = firstTeamLogo;
    this.secondTeamLogo = secondTeamLogo;
    this.matchCode = matchCode;
    this.active = active;
  }

  protected IplTeamModel(Parcel in) {
    matchDate = in.readString();
    venue = in.readString();
    firstTeam = in.readString();
    secondTeam = in.readString();
    firstTeamId = in.readString();
    secondTeamId = in.readString();
    winningTeamId = in.readString();
    byte tmpPredictionEnable = in.readByte();
    predictionEnable = tmpPredictionEnable == 0 ? null : tmpPredictionEnable == 1;
    additionalInfo = in.readString();
    firstTeamLogo = in.readString();
    secondTeamLogo = in.readString();
    matchCode = in.readString();
    byte tmpActive = in.readByte();
    active = tmpActive == 0 ? null : tmpActive == 1;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(matchDate);
    dest.writeString(venue);
    dest.writeString(firstTeam);
    dest.writeString(secondTeam);
    dest.writeString(firstTeamId);
    dest.writeString(secondTeamId);
    dest.writeString(winningTeamId);
    dest.writeByte((byte) (predictionEnable == null ? 0 : predictionEnable ? 1 : 2));
    dest.writeString(additionalInfo);
    dest.writeString(firstTeamLogo);
    dest.writeString(secondTeamLogo);
    dest.writeString(matchCode);
    dest.writeByte((byte) (active == null ? 0 : active ? 1 : 2));
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<IplTeamModel> CREATOR = new Creator<IplTeamModel>() {
    @Override
    public IplTeamModel createFromParcel(Parcel in) {
      return new IplTeamModel(in);
    }

    @Override
    public IplTeamModel[] newArray(int size) {
      return new IplTeamModel[size];
    }
  };

  public String getMatchDate() {
    return matchDate;
  }

  public void setMatchDate(String matchDate) {
    this.matchDate = matchDate;
  }

  public String getVenue() {
    return venue;
  }

  public void setVenue(String venue) {
    this.venue = venue;
  }

  public String getFirstTeam() {
    return firstTeam;
  }

  public void setFirstTeam(String firstTeam) {
    this.firstTeam = firstTeam;
  }

  public String getSecondTeam() {
    return secondTeam;
  }

  public void setSecondTeam(String secondTeam) {
    this.secondTeam = secondTeam;
  }

  public Object getWinningTeam() {
    return winningTeam;
  }

  public void setWinningTeam(Object winningTeam) {
    this.winningTeam = winningTeam;
  }

  public String getFirstTeamId() {
    return firstTeamId;
  }

  public void setFirstTeamId(String firstTeamId) {
    this.firstTeamId = firstTeamId;
  }

  public String getSecondTeamId() {
    return secondTeamId;
  }

  public void setSecondTeamId(String secondTeamId) {
    this.secondTeamId = secondTeamId;
  }

  public String getWinningTeamId() {
    return winningTeamId;
  }

  public void setWinningTeamId(String winningTeamId) {
    this.winningTeamId = winningTeamId;
  }

  public Boolean getPredictionEnable() {
    return predictionEnable;
  }

  public void setPredictionEnable(Boolean predictionEnable) {
    this.predictionEnable = predictionEnable;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getFirstTeamLogo() {
    return firstTeamLogo;
  }

  public void setFirstTeamLogo(String firstTeamLogo) {
    this.firstTeamLogo = firstTeamLogo;
  }

  public String getSecondTeamLogo() {
    return secondTeamLogo;
  }

  public void setSecondTeamLogo(String secondTeamLogo) {
    this.secondTeamLogo = secondTeamLogo;
  }

  public String getMatchCode() {
    return matchCode;
  }

  public void setMatchCode(String matchCode) {
    this.matchCode = matchCode;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }
}
