package in.msewa.model;

/**
 * Created by Kashif-PC on 1/3/2017.
 */
public class EventCategoryModel {
    private long catId;
    private String catName;
    private String catTicketSetting;
    private String catImage;


    public EventCategoryModel(long catId,String catName,String catTicketSetting,String catImage){
        this.catId = catId;
        this.catName = catName;
        this.catTicketSetting = catTicketSetting;
        this.catImage = catImage;
    }

    public long getCatId() {
        return catId;
    }

    public void setCatId(long catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatTicketSetting() {
        return catTicketSetting;
    }

    public void setCatTicketSetting(String catTicketSetting) {
        this.catTicketSetting = catTicketSetting;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }
}
