package in.msewa.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/8/2016.
 */
public class BrandListModel extends SugarRecord{
    private String brandCode;
    private String brandName;

    public BrandListModel(){

    }

    public BrandListModel(String brandCode, String brandName){
        this.brandCode = brandCode;
        this.brandName = brandName;

    }

    public String getbrandCode() {
        return brandCode;
    }

    public void setbrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getbrandName() {
        return brandName;
    }

    public void setbrandName(String brandName) {
        this.brandName = brandName;
    }
}
