package in.msewa.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/8/2016.
 */
public class SubCategoryListModel extends SugarRecord{
    private String subCategoryCode;
    private String subCategoryName;

    public SubCategoryListModel(){

    }

    public SubCategoryListModel(String subCategoryCode, String subCategoryName){
        this.subCategoryCode = subCategoryCode;
        this.subCategoryName = subCategoryName;

    }

    public String getsubCategoryCode() {
        return subCategoryCode;
    }

    public void setsubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public String getsubCategoryName() {
        return subCategoryName;
    }

    public void setsubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }
}
