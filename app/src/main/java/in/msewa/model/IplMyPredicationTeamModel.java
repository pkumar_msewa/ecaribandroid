package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 3/31/2017.
 */
public class IplMyPredicationTeamModel implements Parcelable {

  String matchCode;
  String firstTeam;
  String mobileNumber;
  String secondTeam;
  String predictionDate;
  String predictedTeam;
  String predictId;
  String matchDate;
  Boolean winningResult;
  String additionalInfo;
  String firstTeamLogo, secondTeamLogo;


  public IplMyPredicationTeamModel(String matchCode, String firstTeam, String mobileNumber, String secondTeam, String predictionDate, String predictedTeam, String predictId, String matchDate, Boolean winningResult, String additionalInfo, String firstTeamLogo, String secondTeamLogo) {
    this.matchCode = matchCode;
    this.firstTeam = firstTeam;
    this.mobileNumber = mobileNumber;
    this.secondTeam = secondTeam;
    this.predictionDate = predictionDate;
    this.predictedTeam = predictedTeam;
    this.predictId = predictId;
    this.matchDate = matchDate;
    this.winningResult = winningResult;
    this.additionalInfo = additionalInfo;
    this.firstTeamLogo = firstTeamLogo;
    this.secondTeamLogo = secondTeamLogo;
  }

  protected IplMyPredicationTeamModel(Parcel in) {
    matchCode = in.readString();
    firstTeam = in.readString();
    mobileNumber = in.readString();
    secondTeam = in.readString();
    predictionDate = in.readString();
    predictedTeam = in.readString();
    predictId = in.readString();
    matchDate = in.readString();
    byte tmpWinningResult = in.readByte();
    winningResult = tmpWinningResult == 0 ? null : tmpWinningResult == 1;
    additionalInfo = in.readString();
    firstTeamLogo = in.readString();
    secondTeamLogo = in.readString();
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(matchCode);
    dest.writeString(firstTeam);
    dest.writeString(mobileNumber);
    dest.writeString(secondTeam);
    dest.writeString(predictionDate);
    dest.writeString(predictedTeam);
    dest.writeString(predictId);
    dest.writeString(matchDate);
    dest.writeByte((byte) (winningResult == null ? 0 : winningResult ? 1 : 2));
    dest.writeString(additionalInfo);
    dest.writeString(firstTeamLogo);
    dest.writeString(secondTeamLogo);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<IplMyPredicationTeamModel> CREATOR = new Creator<IplMyPredicationTeamModel>() {
    @Override
    public IplMyPredicationTeamModel createFromParcel(Parcel in) {
      return new IplMyPredicationTeamModel(in);
    }

    @Override
    public IplMyPredicationTeamModel[] newArray(int size) {
      return new IplMyPredicationTeamModel[size];
    }
  };

  public String getMatchCode() {
    return matchCode;
  }

  public void setMatchCode(String matchCode) {
    this.matchCode = matchCode;
  }

  public String getFirstTeam() {
    return firstTeam;
  }

  public void setFirstTeam(String firstTeam) {
    this.firstTeam = firstTeam;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getSecondTeam() {
    return secondTeam;
  }

  public void setSecondTeam(String secondTeam) {
    this.secondTeam = secondTeam;
  }

  public String getPredictionDate() {
    return predictionDate;
  }

  public void setPredictionDate(String predictionDate) {
    this.predictionDate = predictionDate;
  }

  public String getPredictedTeam() {
    return predictedTeam;
  }

  public void setPredictedTeam(String predictedTeam) {
    this.predictedTeam = predictedTeam;
  }

  public String getPredictId() {
    return predictId;
  }

  public void setPredictId(String predictId) {
    this.predictId = predictId;
  }

  public String getMatchDate() {
    return matchDate;
  }

  public void setMatchDate(String matchDate) {
    this.matchDate = matchDate;
  }

  public Boolean getWinningResult() {
    return winningResult;
  }

  public void setWinningResult(Boolean winningResult) {
    this.winningResult = winningResult;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getFirstTeamLogo() {
    return firstTeamLogo;
  }

  public void setFirstTeamLogo(String firstTeamLogo) {
    this.firstTeamLogo = firstTeamLogo;
  }

  public String getSecondTeamLogo() {
    return secondTeamLogo;
  }

  public void setSecondTeamLogo(String secondTeamLogo) {
    this.secondTeamLogo = secondTeamLogo;
  }
}
