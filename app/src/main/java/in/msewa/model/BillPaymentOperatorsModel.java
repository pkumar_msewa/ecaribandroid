package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class BillPaymentOperatorsModel extends SugarRecord implements Parcelable {

  public String code;
  public String name;
  public String defaultCode;
  public String images;

  public BillPaymentOperatorsModel(String code, String name, String defaultCode, String images) {
    this.code = code;
    this.name = name;
    this.defaultCode = defaultCode;
    this.images = images;
  }

  protected BillPaymentOperatorsModel(Parcel in) {
    code = in.readString();
    name = in.readString();
    defaultCode = in.readString();
    images = in.readString();
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(code);
    dest.writeString(name);
    dest.writeString(defaultCode);
    dest.writeString(images);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<BillPaymentOperatorsModel> CREATOR = new Creator<BillPaymentOperatorsModel>() {
    @Override
    public BillPaymentOperatorsModel createFromParcel(Parcel in) {
      return new BillPaymentOperatorsModel(in);
    }

    @Override
    public BillPaymentOperatorsModel[] newArray(int size) {
      return new BillPaymentOperatorsModel[size];
    }
  };

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDefaultCode() {
    return defaultCode;
  }

  public void setDefaultCode(String defaultCode) {
    this.defaultCode = defaultCode;
  }

  public String getImages() {
    return images;
  }

  public void setImages(String images) {
    this.images = images;
  }
}
