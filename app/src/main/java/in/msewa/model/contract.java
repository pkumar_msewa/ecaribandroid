package in.msewa.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 4/6/2016.
 */
public class contract implements Parcelable {
    public String pcId;
    public String pcName;
    private boolean isSelected;

    public contract(String pcId, String pcName, boolean isSelected) {
        this.pcId = pcId;
        this.pcName = pcName;
        this.isSelected = isSelected;
    }

    protected contract(Parcel in) {
        pcId = in.readString();
        pcName = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pcId);
        dest.writeString(pcName);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<contract> CREATOR = new Creator<contract>() {
        @Override
        public contract createFromParcel(Parcel in) {
            return new contract(in);
        }

        @Override
        public contract[] newArray(int size) {
            return new contract[size];
        }
    };

    public String getPcId() {
        return pcId;
    }

    public void setPcId(String pcId) {
        this.pcId = pcId;
    }

    public String getPcName() {
        return pcName;
    }

    public void setPcName(String pcName) {
        this.pcName = pcName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}