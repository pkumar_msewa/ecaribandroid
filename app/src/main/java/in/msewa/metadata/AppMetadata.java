package in.msewa.metadata;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.text.Html;
import android.widget.TextView;

import java.util.ArrayList;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.model.BankListModel;
import in.msewa.model.BusCityModel;
import in.msewa.model.QwikPayModel;
import in.msewa.model.StatementModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.SplashActivity;


/**
 * Created by Ksf on 3/27/2016.
 */
public class AppMetadata {


  public static String FRAGMENT_TYPE = "NAVIGATION";
  public static String YOUTUBE_DEVELOPER_KEY = "AIzaSyDgA5zCjZAOgR4Ccp9gthSJvI4XwaCwgAI";
  public static String TELEBUY_ID = "";
  public static String appKey = "1478673778";
  public static String appSecret = "H/79xKHJBWCubOl9vjkpEBkcpDlVGPNssD6FP6sVY4c=";
//    public static String appKey = "1495104428";
//    public static String appSecret = "PbytNScI5BCga4E7zcABSLjX3vp1mnkv";

  //houejoy sdk
  public static String hj_appkey = "9c3842ee-7506-4673-9f3c-87f4e49d73b4";
  public static String hj_microapp_id = "in.housejoy.housejoy-services-v3";
  public static String hj_execution_url = "https://hub.appsfly.io/executor/fetch-build";
  public static String hj_intent_string = "access_all_housejoy_services";
  public static String hj_payment_intent_string = "payment_success";


  //Flight And Images

  public static int getFLightImage(String imageName, TextView tvFlightListName) {
    switch (imageName) {
      case "SG":
        tvFlightListName.setText("SpiceJet");
        return R.drawable.flight_spice_jet;
      case "9W":
        tvFlightListName.setText("JetAirways");
        return R.drawable.flight_jet_airways;
      case "6E":
        tvFlightListName.setText("Indigo");
        return R.drawable.flight_indigo;
      case "AI":
        tvFlightListName.setText("AirIndia");
        return R.drawable.flight_air_india;
      case "I5":
        tvFlightListName.setText("AirAsia");
        return R.drawable.flight_air_asia;
      case "UK":
        tvFlightListName.setText("Vistara");
        return R.drawable.flight_vistara;
      case "G8":
        tvFlightListName.setText("GoAir");
        return R.drawable.flight_go_air;
      default:
        tvFlightListName.setText("");
        return R.drawable.ic_no_flight;

    }
  }


  public static ArrayList<QwikPayModel> getQwikPay() {
    ArrayList<QwikPayModel> payModel = new ArrayList<>();
    payModel.add(new QwikPayModel("Fund Transfer", "1472901067000", "Fund transfer to no. 8800241972", "15", true, "Prepaid"));
    payModel.add(new QwikPayModel("Prepaid Mobile Topup", "1472901067000", "Mobile topup to no. 7204943677", "200", false, "PostPaid"));
    payModel.add(new QwikPayModel("Data Pack Topup", "1472901067000", "Data pack topup to no. 8800241872", "200", false, "Dth"));
    payModel.add(new QwikPayModel("DTH Recharge", "1472901067000", "DTH recharge to a/c no 2900023923", "500", false, "Electricity"));
    payModel.add(new QwikPayModel("Fund Transfer", "1472901067000", "Amount transfer to no. 7204943677", "3000", true, "landline"));
    payModel.add(new QwikPayModel("Postpaid Mobile Topup", "1472901067000", "Mobile topup to no. 9841555787", "2000", false, "load Money"));
    return payModel;
  }

  public static ArrayList<BusCityModel> getBusCities() {
    ArrayList<BusCityModel> cityModel = new ArrayList<>();
    cityModel.add(new BusCityModel(5951, "Delhi"));
    cityModel.add(new BusCityModel(8794, "Mumbai"));
    cityModel.add(new BusCityModel(109, "Bangalore"));
    cityModel.add(new BusCityModel(114, "Pune"));
    cityModel.add(new BusCityModel(100, "Hyderabad"));
    cityModel.add(new BusCityModel(103, "Chennai"));
    cityModel.add(new BusCityModel(439, "Goa"));
    cityModel.add(new BusCityModel(1530, "Haridwar"));
    cityModel.add(new BusCityModel(1530, "Indore"));
    cityModel.add(new BusCityModel(1530, "Kolkata"));
    cityModel.add(new BusCityModel(1530, "Jaipur"));
    return cityModel;
  }

  public static ArrayList<BankListModel> getBankList() {
    ArrayList<BankListModel> cityModel = new ArrayList<>();
    cityModel.add(new BankListModel("1530", "Vijaya Bank"));
    cityModel.add(new BankListModel("5951", "State Bank of India"));
    cityModel.add(new BankListModel("8794", "HDFC Bank"));
    cityModel.add(new BankListModel("109", "Bangalore"));
    cityModel.add(new BankListModel("114", "Axis Bank"));
    cityModel.add(new BankListModel("100", "Bank of Baroda"));
    cityModel.add(new BankListModel("103", "ICICI Bank"));
    cityModel.add(new BankListModel("439", "Punjab National Bank"));
    cityModel.add(new BankListModel("1530", "IDBI Bank"));
    cityModel.add(new BankListModel("1530", "Canara Bank"));
    cityModel.add(new BankListModel("1530", "Bank of India"));
    return cityModel;
  }

  //  State Bank of India
  public static ArrayList<StatementModel> getStatement() {
    ArrayList<StatementModel> statementList = new ArrayList<StatementModel>();
//        statementList.add(new StatementModel("200.00", "100.00", "05-05-2016 05:39 PM", "TopUp", "Success", "A204", "Successfully recharged on 8800241872", true));
//        statementList.add(new StatementModel("120.00", "20.00", "05-05-2016 01:39 PM", "Transfer", "Success", "A205", "Successfully transferred to 8800241872", true));
//        statementList.add(new StatementModel("1200.00", "600.32", "05-05-2016 01:39 PM", "Bill Pay", "Pending", "A207", "Electricity payment to BSES Rajdhani- Delhi", true));
//        statementList.add(new StatementModel("1200.00", "2000.00", "05-05-2016 01:39 PM", "Load Money", "Failure", "F223", "Loading INR 2000 failed", false));
//        statementList.add(new StatementModel("1200.00", "2000.00", "05-05-2016 02:39 PM", "Load Money", "Success", "A223", "Loading INR 2000 sucess", true));
    return statementList;
  }

  public static String getInvalidSession() {
    String source = "<b><font color=#000000> Session Expired.</font></b>" +
      "<br><br><b><font color=#ff0000> Please login again to use Ecarib.</font></b><br></br>";
    return source;
  }


  public static String getSuccessMPinReset() {
    String source = "<b><font color=#000000> MPIN Reset Successfully.</font></b>" +
      "<br><br><b><font color=#ff0000> Please create new MPIN in next step.</font></b><br></br>";
    return source;
  }

  public static String getLogout() {
    String source = "<b><font color=#000000> This will clear your session.</font></b>" +
      "<br><br><b><font color=#ff0000> Are you sure you want to log out?</font></b><br></br>";
    return source;
  }


  public static String getBlockSession() {
    String source = "<b><font color=#000000> Account has been blocked.</font></b>" +
      "<br><br><b><font color=#ff0000> Suspicious behaviour detected, Please contact customer care</font></b><br></br>";
    return source;
  }

  public void showCustomDialog(final Context context) {
    CustomAlertDialog builder = new CustomAlertDialog(context, R.string.dialog_title2, Html.fromHtml(generateNoInternetMessage()));

    builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        context.startActivity(new Intent(Settings.ACTION_SETTINGS));

      }
    });

    builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

      }
    });
    builder.show();
  }

  public String generateNoInternetMessage() {
    String source = "<b><font color=#000000> No internet connection.</font></b>" +
      "<br><br><b><font color=#ff0000> Please check your internet connection and try again.</font></b><br></br>";
    return source;
  }

}
