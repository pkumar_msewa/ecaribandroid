package in.msewa.metadata;

public class ApiUrl {

//  private static final String URL_DOMAIN_ = "http://vpayqwik.msspayments.com/";
  private static final String URL_DOMAIN_ = "http://23.101.178.130/";
//    public static final String URL_DOMAIN_ = "http://192.168.43.145:8035/";
  private static final String URL_DOMAIN = URL_DOMAIN_ + "Api/";
  private static final String URL_SHOPPING = "http://www.qwikrpay.com/";
  private static final String VERSION = "v1";
  private static final String ROLE = "User";
  private static final String DEVICE = "Android";
  private static final String LANGUAGE = "en";
  private static final String SEPARATOR = "/";

  public static final String URL_MAIN = URL_DOMAIN + VERSION + SEPARATOR + ROLE + SEPARATOR + DEVICE + SEPARATOR + LANGUAGE + SEPARATOR;
  public static final String URL_LOAD_MONEY = URL_DOMAIN_ + "Api/v1/User/Android/en/LoadMoney/Process";
  public static final String URL_FLIGHT_MONEY = URL_DOMAIN + "v1/User/Android/en/Treval/Flight/Process";


  //USER
  public static final String URL_LOGIN = URL_MAIN + "Login/Process";
  public static final String URL_REGISTRATION = URL_MAIN + "Registration/Process";
  public static final String URL_FORGET_PWD = URL_MAIN + "Login/ForgotPassword";
  public static final String URL_CHANGE_PWD = URL_MAIN + "Login/ChangePasswordWithOTP";
  public static final String URL_RESEND_OTP = URL_MAIN + "Registration/ResendMobileOTP";
  public static final String URL_FORGOT_PASSWORD_RESEND_OTP = URL_MAIN + "Login/ResendForgotPasswordOTP";
  public static final String URL_EDIT_USER = URL_MAIN + "User/EditProfile";
  public static final String URL_VERIFY_PHONE = URL_MAIN + "Registration/MobileOTP";
  public static final String URL_LOGOUT = URL_MAIN + "Logout/Process";
  public static final String URL_CHANGE_CURRENT_PWD = URL_MAIN + "User/ChangePassword";
  public static final String URL_FORGET_MPIN = URL_MAIN + "User/ForgotMPIN";


  public static final String URL_GET_USER_DETAILS = URL_MAIN + "User/GetUserDetails";

  public static final String URL_GET_USER_BALANCE = URL_MAIN + "User/GetDetails/Balance";
  public static final String URL_GET_USER_POINTS = URL_MAIN + "User/GetDetails/Account";
  public static final String URL_GET_USER_SUB_DETAILS = URL_MAIN + "User/GetDetails/Basic";


  //Old
  public static final String URL_UPLOAD_IMAGE = URL_MAIN + "User/UploadPicture";
  //    public static final String URL_IMAGE_MAIN = "http://fgmtest.firstglobalmoney.com:8035/";
  public static final String URL_IMAGE_MAIN = "https://www.vpayqwik.com";


  //TOPUPS New
  public static final String URL_PREPAID_TOPUP = URL_MAIN + "Topup/PrePaid";
  public static final String URL_POSTPAID_TOPUP = URL_MAIN + "Topup/PostPaid";
  public static final String URL_DATACARD_TOPUP = URL_MAIN + "Topup/DataCard";

  //GENERATE TOP_UP NO AND PLANS new
  public static final String URL_LOAD_ACCORDING_NO = URL_MAIN + "Topup/GetOperatorAndCircleForMobile";
  public static final String URL_BROWSE_PLAN = URL_MAIN + "Topup/GetPlans";
  public static final String URL_CIRCLE_OPERATOR = URL_MAIN + "Topup/GetOperatorAndCircle";
  public static final String URL_ELECTRICTY_OPERATOR = URL_MAIN + "BillPayment/GetServiceProvider";


  //MERCHANT API and pay at store
  public static final String URL_LIST_STORES = URL_MAIN + "User/GetMerchants";
  public static final String URL_PAY_AT_STORE = URL_MAIN + "SendMoney/Store";

  //SEnd Money.
  public static final String URL_FUND_TRANSFER = URL_MAIN + "SendMoney/Mobile";


  //BillPayment New
  public static final String URL_DTH_PAYMENT = URL_MAIN + "BillPayment/DTH";
  public static final String URL_ELECTRICITY_PAYMENT = URL_MAIN + "BillPayment/Electricity";
  public static final String URL_LANDLINE_PAYMENT = URL_MAIN + "BillPayment/Landline";
  public static final String URL_GAS_PAYMENT = URL_MAIN + "BillPayment/Gas";
  public static final String URL_GAS_OPERATOR = URL_MAIN + "BillPayment/GetServiceProvider";
  public static final String URL_INSURANCE_PAYMENT = URL_MAIN + "BillPayment/Insurance";


  //TELEBUY
  public static final String URL_TELEBUY_MAIN = "http://122.183.176.98/Payqwikweb_web/PAGE_PayQwik.awp";
  public static final String URL_GET_ALL_PRODUCTS = URL_TELEBUY_MAIN + "?API=GETALLPRODUCTDETAILS";
  public static final String URL_GET_SHOPPING_VIDEO = URL_TELEBUY_MAIN + "?API=GETYOUTUBEURL&PID=";
  public static final String URL_GET_CART = URL_TELEBUY_MAIN + "?API=RetrieveOpenCartDetails&PHONE=";
  public static final String URL_GET_ADDRESS = URL_TELEBUY_MAIN + "?API=LISTCONTACTADDRESSES&PHONE=";
  public static final String URL_GET_STATE = URL_TELEBUY_MAIN + "?API=GETSTATE&CountryID=5";
  public static final String URL_GET_CITY = URL_TELEBUY_MAIN + "?API=GetCity&Stateid=";
  public static final String URL_ADD_CART = URL_TELEBUY_MAIN + "?API=UPDATEITEMQUANTITYINCART&PHONE=";
  public static final String URL_REMOVE_CART = URL_TELEBUY_MAIN + "?API=RemoveItemFromCart&PHONE=";


  //FOR LOADING MONEY old
  public static final String URL_GENERATE_RSA = URL_MAIN + "User/LoadMoney/RSAKey";
  public static final String REDIRECT_URL = URL_MAIN + "User/LoadMoney/Redirect";
  public static final String CANCEL_URL = URL_MAIN + "User/LoadMoney/Redirect";

  //MPIN NEW
  public static final String URL_GENERATE_M_PIN = URL_MAIN + "User/SetMPIN";
  public static final String URL_DELETE_M_PIN = URL_MAIN + "User/DeleteMPIN";
  public static final String URL_CHANGE_M_PIN = URL_MAIN + "User/ChangeMPIN";
  public static final String URL_VERIFY_M_PIN = URL_MAIN + "User/VerifyMPIN";

  //Other new
  public static final String URL_INVITE_FRIENDS = URL_MAIN + "User/Invite/Mobile";
  public static final String URL_RECEIPT = URL_MAIN + "User/GetReceipts";
  public static final String URL_RESEND_EMAIL = "https://www.vpayqwik.com/User/ReSendEmailOTP";

  public static final String URL_QUICK_GET_PAY_LIST = URL_MAIN + "User/STransactions";
  public static final String URL_QUICK_POST_PAY = URL_MAIN + "OneClickPay";
  public static final String URL_UPDATE_FAV = URL_MAIN + "User/UpdateFavourite";


  //Coupons API
  public static final String URL_GET_ZERCH_COUPONS = "http://52.74.231.38:8280/OfferList/1.0.0";

  public static final String URL_EMAIL_COUPONS = "http://10.1.1.56:7000/GetOffers/coupons?emailID=";

  //Redeeem Coupons
  public static final String URL_SEND_REDEEM_CODE = URL_MAIN + "User/Redeem/Code";

  //SMS
  public static final String URL_SEND_FREE_SMS = "https://www.vpayqwik.com/SendSMS";

  //Bank Transfer
  public static final String URL_LIST_BANK = URL_MAIN + "getAllBanks";
  public static final String URL_LIST_IFSC = URL_MAIN + "getIFSC/";
  public static final String URL_BANK_TRANSFER = URL_MAIN + "SendMoney/Bank";

  //Check DEU AMOUNT
  public static final String URL_CHECK_DEU = URL_DOMAIN_ + "InstantPay/GetAmount";

  public static final String URL_SHARE_POINTS = URL_MAIN + "User/SharePoints";

  //OTHER
  public static final String URL_CHECK_VERSION = "https://www.vpayqwik.com/Version/Check";
  public static final String URL_VALIDATE_TRANSACTION = URL_MAIN + "validateTransactionRequest";

  //LOAD MONEY SDK
  public static final String URL_INITIATE_LOAD_MONEY = URL_MAIN + "LoadMoney/InitiateLoadMoney";

  //Redirtect API EBS
  public static final String URL_VERIFY_TRANSACTION_EBS_KIT = URL_MAIN + "LoadMoney/RedirectSDK";
  public static final String URL_VERIFY_TRANSACTION_FLIGHT_EBS_KIT = URL_DOMAIN_ + "Api/v1/User/Android/en/Treval/Flight/RedirectSDK";

  //Travel
  //Bus
  public static final String URL_GET_CITY_BUS = URL_MAIN + "Travel/Bus/getSources";
  public static final String URL_GET_LIST_BUS = URL_MAIN + "Travel/Bus/getAvailableBuses";
  public static final String URL_GET_BUS_DETAILS = URL_MAIN + "Travel/Bus/getTripDetails";
  public static final String URL_POST_BOOK_BUS = URL_MAIN + "Travel/Bus/blockSeat";


  //Flight
//    public static final String URL_GET_FLIGHT_AIRPORT = "http://www.rupease.com/Api/v1/User/Mobile/en/ThirdParty/Travel/Flight/Getflightdeatail";
//    public static final String URL_GET_LIST_FLIGHT_ONE_WAY = "http://52.66.169.133/Api/v1/User/Mobile/en/ThirdParty/Travel/Flight/Getonewayallflightdetail";
//    public static final String URL_GET_FLIGHT_LATEST_PRICE = "http://52.66.169.133/Api/v1/User/Mobile/en/ThirdParty/Travel/Flight/Getonewayallflighttaxdeatail";

  public static final String URL_SEND_KYC_DATA = URL_MAIN + "User/KycRequest";
  public static final String URL_VERIFY_KYC_DATA = URL_MAIN + "User/KycRequest/OTPVerification";


  //ZEPO

  private static final String ZEPO_MAIN = "http://test.api.zepo.in/";
  private static final String ZEPO_STORE = "store/";
  private static final String ZEPO_ID = "63/";


  public static final String URL_ZEPO_FETCH_PRODUCTS = ZEPO_MAIN + ZEPO_STORE + ZEPO_ID;
  public static final String URL_M_VISA_PAY = URL_MAIN + "User/VisaRequest";
  public static final String URL_M_VISA_CARD_INFO = URL_MAIN + "User/GetAccountNumber";


  //Events
  public static final String URL_EVENT_AUTH = URL_MAIN + "MeraEvents/AuthCode";
  public static final String URL_EVENT_CAT_LIST = URL_MAIN + "MeraEvents/CategoryList";
  public static final String URL_EVENT_LIST = URL_MAIN + "MeraEvents/EventList";


  //GCI- Gift Card
  public static final String URL_GIFT_CARD_CAT = URL_MAIN + "GiftCard/GetBrands";
  public static final String URL_GIFT_CARD_LIST = URL_MAIN + "GiftCard/GetBrandDenominations";
  public static final String URL_GIFT_CARD_ORDER = URL_MAIN + "GiftCard/AddOrder";

  //IPL-Predict n win
//  public static final String URL_TEAM_LIST = URL_MAIN + "User/IPL/GetIPLMatchList";
//  public static final String URL_TEAM_PREDICT = URL_MAIN + "User/IPL/TeamPrediction";
  public static final String URL_TEAM_CHECK_PREDICTION = URL_MAIN + "User/IPL/CheckPrediction";

  //Donation
  public static final String URL_DONATION_TIRUPATI = URL_MAIN + "SendMoney/Donate";

  //API HEADER
  public static final String H_PREPAID = "prepaidMobile|";
  public static final String H_POSTPAID = "postpaidMobile|";
  public static final String H_DTH = "dthMobile|";
  public static final String H_DATACARD = "datacardBill|";
  public static final String H_LANDLINE = "landlineBill|";
  public static final String H_ELECTRICITY = "electricityBill|";
  public static final String H_NSURANCE = "insuranceBill|";
  public static final String H_GAS = "gasBill|";
  public static final String H_SENDMONEY = "sendMoneyPay|";
  public static final String H_BANKTRANSFER = "bankTransferPay|";


  //API REPOSNE CODE
  public static final String C_SUCCESS = "S00";
  public static final String C_SESSION_EXPIRE = "F03";
  public static final String C_SESSION_BLOCKED = "H01";


  public static final String URL_LIST_QUES = URL_MAIN + "Registration/GetQuestion";

  //split payment
  public static final String URL_CHECK_BALANCE_FOR_SPLIT = URL_MAIN + "Topup/checkForSplitPay";
  public static final String URL_CHECK_BALANCE_FOR_SPLIT_BILL = URL_MAIN + "BillPayment/checkForSplitPayBillPay";

  //Niki
  public static final String URL_NIKKI_GET_TOKEN = URL_MAIN + "nikki/User/GetNikkiToken";

  //Imagica
  public static final String URL_IMAGICA_LOGIN = URL_MAIN + "Adlabs/AuthMobile";
  public static final java.lang.String URL_IMAGICA_THEME_PARK_LIST = URL_MAIN + "Imagica/SearchTickets";
  public static final java.lang.String URL_IMAGICA_CREATE_ORDER = URL_MAIN + "Adlabs/OrderCreateMobile";
  public static final java.lang.String URL_IMAGICA_PAYMENT = URL_MAIN + "Adlabs/PaymentMobile";
  public static final String URL_RESEND_OTP_DEVICE = URL_MAIN + "Login/ResendDeviceBindingOTP";

  public static final String URL_CHECK_BALANCE_FOR_QWIKPAY = URL_MAIN + "checkForSplitOneClickPay";
  public static final String URL_VALIDATE_TRX_TIME = URL_MAIN + "LoadMoney/getTrxTimeDiff";

  public static final String URL_GET_TICKET_LIST = "https://www.letsmanageit.in/Api/V1/MobileTicket/GetTicket/Email";
  public static final String URL_VER_CHECK = URL_MAIN + "User/checkVersion";
  public static final String URL_PLACEORDER = URL_MAIN + "Imagica/PlaceOrder";
  public static final String URL_PLACEPAYMENT = URL_MAIN + "Imagica/ProcessPayment";
  public static final String URL_REFER_AND_EARN = URL_MAIN + "User/getMobileList";

  //Bus new
  public static final String URL_BUS_GET_CITY = URL_MAIN + "Treval/Bus/GetAllCityList";
  public static final String URL_GET_LIST_BUS_NEW = URL_MAIN + "Treval/Bus/GetAllAvailableTrips";
  public static final String URL_BUS_SEAT_DETAILS = URL_MAIN + "Treval/Bus/GetSeatDetails";
  public static final String URL_BUS_SEAT_TRANXID = URL_MAIN + "Treval/Bus/GetTransactionId";
  public static final String URL_BUS_SEAT_TRANXID_NEW = URL_MAIN + "Treval/Bus/GetTransactionIdUpdated";
  public static final String URL_BUS_SEAT_BOOKING = URL_MAIN + "Treval/Bus/BookTicket";
  public static final String URL_BUS_SEAT_BOOKING_NEW = URL_MAIN + "Treval/Bus/BookTicketUpdated";
  public static final String URL_BUS_CANCEL_INITIATION = URL_MAIN + "Treval/Bus/cancelInitPayment";
  public static final String URL_BUS_GET_BOOKED_TICKETS = URL_MAIN + "Treval/Bus/getAllTicketsByUser";
  public static final String URL_BUS_GET_SEAT_SAVE_DETAILS = URL_MAIN + "Treval/Bus/saveSeatDetails";

  //Flight
  public static final String URL_GET_LIST_FLIGHT_ONE_WAY = URL_MAIN + "Treval/Flight/OneWay";
  public static final String URL_FLIGHT_BOOK_ONEWAY = URL_MAIN + "Treval/Flight/OneWay";

  public static final String URL_FETCH_ONE_WAY_PRICE = URL_MAIN + "Treval/Flight/ShowPriceDetail";
  public static final String URL_FETCH_ONE_WAY_CONNCTING_PRICE = URL_MAIN + "Treval/Flight/ConnectingShowPriceDetail";
  public static final String URL_FLIGHT_CHECKOUT = URL_MAIN + "Treval/Flight/CheckOut";
  public static final String URL_FLIGHT_CHECKOUT_CONTING = URL_MAIN + "Treval/Flight/Conecting/CheckOut";
  public static final String URL_FLIGHT_CHECKOUT_ROUNDCONTING = URL_MAIN + "Treval/Flight/Roundway/Connecting/CheckOut";
  public static final String URL_FLIGHT_CHECKOUT_ROUND = URL_MAIN + "Treval/Flight/RoundwayCheckOut";
  public static final String URL_SOURCE_FLIGHT = URL_MAIN + "Treval/Flight/Source";
  public static final String URL_FLIGHT_INITIATE_LOAD_MONEY = URL_MAIN + "Treval/Flight/InitiateLoadMoney";
  public static final String URL_FLIGHT_INITIATE_LOAD_MONEY_FLIGHT_BOOKING = URL_MAIN + "Treval/Flight/BookFlight";

  public static final String URL_FLIGHT_VNET = URL_MAIN + "Flight/BookFlight/Vnet";

  //new flight api url provide by subir

  public static final String URL_GET_LIST_FLIGHT_SOURCE_DETAILS = URL_MAIN + "Treval/Flight/AirLineNames";
  public static final String URL_FLIGHT_LIST_DETAILS = URL_MAIN + "Treval/Flight/AirLineList";

  public static final String URL_FETCH_RECHECK_PRICE = URL_MAIN + "Treval/Flight/AirPriceRecheck";
  public static final String URL_FLIGHT_BOOKING = URL_MAIN + "Treval/Flight/BookTicket";

  public static final String URL_ENCASH_POINTS = URL_MAIN + "RedeemPoints/Process";
  public static final String URL_FLIGHT_GET_BOOKED_TICKETS = URL_MAIN + "Treval/Flight/MyTickets";
  public static final String URL_TREAT_CARD = URL_MAIN + "TreatCard/GetTreatCards";
  public static final String URL_TREAT_CARD_PLANS = URL_MAIN + "TreatCard/TreatCardPlans";
  public static final String URL_TREAT_CARD_UPDATE = URL_MAIN + "TreatCard/UpdateTreatCard";

  public static final String URL_GET_ALL_SHOPPING_PRODUCTS = URL_SHOPPING + "Api/v1/User/Android/en/ShowProduct";
  public static final String URL_SHOPPONG_REMOVE_ITEM_CART = URL_SHOPPING + "Api/v1/User/Android/en/removeCartItem";
  public static final String URL_SHOPPONG_ADD_ITEM_CART = URL_SHOPPING + "Api/v1/User/Android/en/itemAddToCart";
  public static final String URL_SHOPPONG_SHOW_CART = URL_SHOPPING + "Api/v1/User/Android/en/showCartData";
  public static final String URL_SHOPPONG_ADD_DELIVERY_ADDRESS = URL_SHOPPING + "Api/v1/User/Android/en/addAddress";
  public static final String URL_SHOPPONG_DELETE_DELIVERY_ADDRESS = URL_SHOPPING + "Api/v1/User/Android/en/deleteAddress";
  public static final String URL_SHOPPONG_CHECKOUT_PROCESS = URL_SHOPPING + "Api/v1/User/Android/en/checkOutProcess";
  public static final String URL_EXPENSE_TRACKER = URL_MAIN + "User/UpdateReceipt";
  public static final String URL_FETCH_NOTIFICATIONS = URL_MAIN + "/User/getNotifications";
  public static final String URL_SHOPPING_CATEGORY_LIST = URL_SHOPPING + "Api/v1/User/Android/en/country/categoryList";
  public static final String URL_SHOPPING_SUB_CATEGORY_LIST = URL_SHOPPING + "Api/v1/User/Android/en/category/SubcategoryList";
  public static final String URL_SHOPPING_BRAND_LIST = URL_SHOPPING + "Api/v1/User/Android/en/subCategory/brandList";

  public static final String URL_SHOPPONG_GO_FOR_DELIVERY = URL_SHOPPING + "Api/v1/User/Android/en/gofordelivery";
  public static final String URL_YUP_TV_REGISTER = URL_MAIN + "YuppTV/Register";
  public static final String URL_GET_ALL_SHOPPING_PAYMENT = URL_SHOPPING + "Api/v1/User/Android/en/Payment";
  public static final String URL_SHOPPONG_EDIT_DELIVERY_ADDRESS = URL_SHOPPING + "Api/v1/User/Android/en/updateAddress";
  public static final String URL_SHOPPING_MY_ORDER = URL_SHOPPING + "Api/v1/User/Android/en/myOrder";

  public static final String URL_GET_LIST_COMPONTENTS_CUSTOMRER = "https://www.letsmanageit.in/Api/V1/MobileTicket/Component";
  public static final String URL_CREAT_TICKET = "https://www.letsmanageit.in/Api/V1/MobileTicket/Ticket";


  public static final String URL_TRAVELKHANA_GET_CITY = URL_MAIN + "Travelkhana/Trainslist";
  public static final String URL_TRAVELKHANA_STATION_LIST = URL_MAIN + "Travelkhana/StationlistByTrainNumber";
  public static final String URL_TRAVELKHANA_TRAIN_MENU_LIST = URL_MAIN + "Travelkhana/TrainRoutesMenu";
  public static final String URL_TRAVELKHANA_MENU_LIST = URL_MAIN + "Travelkhana/OutletInTime";
  public static final String URL_TRAVELKHANA_MENU_PRICE_CAL = URL_MAIN + "Travelkhana/MenuPriceCalculation";
  public static final String URL_TRAVELKHANA_ORDER = URL_MAIN + "Travelkhana/ApiOrder";
  public static final String URL_TRAVELKHANA_TRACKER = URL_MAIN + "Travelkhana/TrackUserOrder";
  public static final String URL_TRAVELKHANA_MY_ORDERS = URL_MAIN + "Travelkhana/MyOrder";
  public static final String URL_TRAVELKHANA_APPLY_COUPAN = URL_MAIN + "Travelkhana/applycoupon";

  //HouseJoy
  public static final String URL_HJ_INITIATE_TXN = URL_MAIN + "HouseJoy/Initiate";
  public static final String URL_HJ_SUCCESS_TXN = URL_MAIN + "HouseJoy/Success";
  public static final String URL_HJ_CANCEL_TXN = URL_MAIN + "HouseJoy/cancel";

  public static final String URL_WHOOHOO_CATEGORIES = URL_DOMAIN_ + "Api/v1/User/Android/en/Woohoo/productList";
  public static final String URL_WHOOHOO_DESCRIPTION = URL_DOMAIN_ + "Api/v1/User/Android/en/Woohoo/products/";
  public static final String URL_WHOOHOO_PRICE_RECHECK = URL_DOMAIN_ + "Api/v1/User/Android/en/Woohoo/recheckPrice";
  public static final String URL_WHOOHOO_PROCESS_TRANSACTION = URL_DOMAIN_ + "Api/v1/User/Android/en/Woohoo/processTransaction";

  //Aadhar Card
  public static final String URL_KYC_ADHAAR = URL_DOMAIN_ + "Api/v1/User/Android/en/AadharService/GetAadharOTP";
  public static final String URL_KYC_ADDAHAR_OTP = URL_DOMAIN_ + "Api/v1/User/Android/en/AadharService/GetAadharOTPValidate";


  //IPLNEWURL
  public static final String URL_TEAM_LIST = "http://66.207.206.54:8089/MDEX/Api/v1/Client/Android/en/ipl2018/schedule";
  public static final String URL_TEAM_PREDICT = "http://66.207.206.54:8089/MDEX/Api/v1/Client/Android/en/ipl2018/predictmatch";
  public static final String URL_TEAM_MY_PREDICT = "http://66.207.206.54:8089/MDEX/Api/v1/Client/Android/en/ipl2018/mypredictions";

  //  User/findNearByAgents
  public static final String URL_NEAR_BY_MERCHANT = URL_MAIN + "User/findNearByMerchants";
  public static final String URL_NEAR_BY_AGENT = URL_MAIN + "User/findNearByAgents";

  //terms and condition
  public static final String URL_TERMS_CONDITIONS = URL_DOMAIN_.concat("Terms&Conditions");
  //
  public static final String URL_TREAT_CARD_WEBSITE = "http://treatcard.in/restaurants/";
  public static final String URL_CAMPAGIN = URL_DOMAIN_ + "Campaign";


  public static String URL_UPI_MER_GEN_TOKEN = URL_MAIN + "LoadMoney/UpiInitiateProcess";

  //Razopay
  public static String URL_LOAD_MONEY_ROZORPAY = URL_MAIN + "LoadMoney/InitiateLoadMoneyRazorPay";
  public static String URL_LOAD_MONEY_REDIRCET = URL_MAIN + "LoadMoney/RedirectLoadMoneyRazorPay";

  //UpdateTranscationLimit
  public static String URL_UPDATE_TRANSATION_LIMIT = URL_MAIN + "User/updateUserMaxLimit";

  public static String URL_GET_TRANSATION_LIMIT = URL_MAIN + "User/getUserMaxLimit";
  //verifyPassword
  public static String URL_VERIFY_PASSWORD_LIMIT = URL_MAIN + "/User/validatePassword";


}
