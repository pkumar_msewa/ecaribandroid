package in.msewa.metadata;

import android.content.Context;

import java.util.ArrayList;

import in.msewa.model.MainMenuModel;
import in.msewa.model.OperatorsModel;
import in.msewa.ecarib.R;

public class MenuMetadata {

  public static ArrayList<MainMenuModel> getMenu(Context context) {
    ArrayList<MainMenuModel> catItems = new ArrayList<>();
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu1) + "", R.drawable.menu_new_mobile_topup, "MobileTopUp"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu2) + "", R.drawable.menu_new_pay_bills,"BillPayment"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu3) + "", R.drawable.menu_new_fund_transfer, "FundTransfer"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu9) + "", R.drawable.menu_new_qr_scan,"QRPay"));

    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu25) + "", R.drawable.menu_bharat_qr, "MVisa"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu4) + "", R.drawable.ic_travel, "Travel"));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu28) + "", R.drawable.menu_new_pay_bills, R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu27) + "", R.drawable.menu_new_entertainment, R.color.menuG));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu15) + "", R.drawable.menu_new_bank_transfer, "BankTransfer"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu29) + "", R.drawable.menu_new_qr_donate, "Donation"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu30) + "", R.drawable.menu_new_chat_new, "ChatBot"));

    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu32) + "", R.drawable.ic_treatcard_menu, "TreatCard"));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu22)+"", R.drawable.menu_bus,R.color.menu7));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu34) + "", R.drawable.menu_housejoy, "housejoy"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu33) + "", R.drawable.ic_travelkhana, "travelkhana"));
//    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu15) + "", R.drawable.menu_new_bank_transfer, R.color.menu5));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu28) + "", R.drawable.menu_new_adventure_new, "Adventure"));
    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu17) + "", R.drawable.menu_new_gift, "giftcard"));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu5) + "", R.drawable.menu_new_qwikpay, R.color.menuB));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu26) + "", R.drawable.menu_new_invite, R.color.menu7));


//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu16)+"", R.drawable.menu_new_health_care,R.color.menu7));


//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu13)+"", R.drawable.menu_new_entertainment,R.color.menu3));


    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu7) + "", R.drawable.menu_new_redeem_coupon,"CouponsCode"));
//    catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu12) + "", R.drawable.menu_new_shopping, R.color.menu2));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu6)+"", R.drawable.menu_new_coupns,R.color.menuG));


//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu24) + "", R.drawable.menu_new_pay_store, R.color.menu9));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu17)+"", R.drawable.menu_new_gift,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu19)+"", R.drawable.menu_split_money,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu18) + "", R.drawable.menu_request_money, R.color.menu7));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu16) + "", R.drawable.menu_new_health_care, R.color.menuR));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu21)+"", R.drawable.menu_share_points,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu26) + "", R.drawable.menu_new_invite, R.color.menu7));


//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu22)+"", R.drawable.menu_bus,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu11)+"", R.drawable.menu_new_food,R.color.menu8));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu11)+"", R.drawable.menu_new_food,R.color.menu1));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu29) + "", R.drawable.menu_new_qr_donate, R.color.menu9));

//                catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu12)+"", R.drawable.menu_new_shopping_new,"shopping"));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu31) + "", R.drawable.menu_new_yupptv_new, R.color.menu8));
    return catItems;

  }


  public static ArrayList<OperatorsModel> getPostOperator() {
    ArrayList<OperatorsModel> opItems = new ArrayList<>();
    opItems.add(new OperatorsModel("", "Select your operator", ""));
    opItems.add(new OperatorsModel("VACC", "Aircel", "VACC"));
    opItems.add(new OperatorsModel("VATC", "Airtel", "VATC"));
    opItems.add(new OperatorsModel("VBGC", "BSNL", "VBGC"));
    opItems.add(new OperatorsModel("VIDC", "Idea", "VIDC"));
//    opItems.add(new OperatorsModel("VRGC", "Reliance", "VRGC"));
    opItems.add(new OperatorsModel("VTDC", "Tata Docomo", "VTDC"));
    opItems.add(new OperatorsModel("VVFC", "Vodafone", "VVFC"));
    return opItems;

  }


  public static ArrayList<OperatorsModel> getDTH() {
    ArrayList<OperatorsModel> opItems = new ArrayList<>();
    opItems.add(new OperatorsModel("", "Select DTH Provider", ""));
    opItems.add(new OperatorsModel("VATV", "Airtel Digital TV", "VATV"));
    opItems.add(new OperatorsModel("VSTV", "Sun Direct", "VSTV"));
    opItems.add(new OperatorsModel("VRTV", "Reliance Digital TV", "VRTV"));
    opItems.add(new OperatorsModel("VDTV", "Dish TV", "VDTV"));
    opItems.add(new OperatorsModel("VOTV", "Tata Sky", "VOTV"));
    opItems.add(new OperatorsModel("VVTV", "Videocon D2H", "VVTV"));
    return opItems;
  }

  public static ArrayList<OperatorsModel> getInsurance() {
    ArrayList<OperatorsModel> opItems = new ArrayList<>();
    opItems.add(new OperatorsModel("", "Select Insurance Provider", ""));
    opItems.add(new OperatorsModel("VIPI", "ICICI Prudential Life Insurance", "VIPI"));
    opItems.add(new OperatorsModel("VTAI", "Tata AIA Life Insurance", "VTAI"));
    opItems.add(new OperatorsModel("VILI", "IndiaFirst Life Insurance", "VILI"));
    opItems.add(new OperatorsModel("VBAI", "Bharti AXA Life Insurance", "VBAI"));

    return opItems;
  }

  public static ArrayList<OperatorsModel> getLandLine() {
    ArrayList<OperatorsModel> opItems = new ArrayList<>();
    opItems.add(new OperatorsModel("", "Select Landline Provider", ""));
    opItems.add(new OperatorsModel("VATL", "Airtel", "VATL"));
    opItems.add(new OperatorsModel("VBGL", "BSNL", "VBGL"));
    opItems.add(new OperatorsModel("VMDL", "MTNL Delhi", "VMDL"));
    opItems.add(new OperatorsModel("VRGL", "Reliance", "VRGL"));
    opItems.add(new OperatorsModel("VTCL", "Tata Docomo", "VTCL"));
    return opItems;

  }

  public static ArrayList<OperatorsModel> getGas() {
    ArrayList<OperatorsModel> opItems = new ArrayList<>();
    opItems.add(new OperatorsModel("", "Select Gas Provider", ""));
    opItems.add(new OperatorsModel("VIPG", "Indraprastha Gas", "VIPG"));
    opItems.add(new OperatorsModel("VMMG", "Mahanagar Gas", "VMMG"));
    opItems.add(new OperatorsModel("VGJG", "Gujarat Gas", "VGJG"));
    opItems.add(new OperatorsModel("VADG", "Adani Gas", "VADG"));
    return opItems;

  }


  public static ArrayList<OperatorsModel> getElectricity() {
    ArrayList<OperatorsModel> opItems = new ArrayList<>();
    opItems.add(new OperatorsModel("VTTE", "TSECL - TRIPURA", "VTTE"));
    opItems.add(new OperatorsModel("VTPE", "Torrent Power", "VTPE"));
    opItems.add(new OperatorsModel("VNDE", "Tata Power - DELHI", "VNDE"));
    opItems.add(new OperatorsModel("VREE", "Reliance Energy - MUMBAI", "VREE"));
    opItems.add(new OperatorsModel("VMPE", "Paschim Kshetra Vitaran - MADHYA PRADESH", "VMPE"));
    opItems.add(new OperatorsModel("VNUE", "Noida Power - NOIDA", "VNUE"));

    opItems.add(new OperatorsModel("VMDE", "MSEDC - MAHARASHTRA", "VMDE"));
    opItems.add(new OperatorsModel("VMME", "Madhya Kshetra Vitaran - MADHYA PRADESH", "VMME"));
    opItems.add(new OperatorsModel("VDRE", "Jodhpur Vidyut Vitran Nigam - RAJASTHAN", "VDRE"));

    opItems.add(new OperatorsModel("VJUE", "Jamshedpur Utilities & Services (JUSCO)", "VJUE"));
    opItems.add(new OperatorsModel("VIPE", "India Power", "VIPE"));
    opItems.add(new OperatorsModel("VARE", "Ajmer Vidyut Vitran Nigam - RAJASTHAN", "VARE"));

    opItems.add(new OperatorsModel("VCCE", "CSEB - CHHATTISGARH", "VCCE"));
    opItems.add(new OperatorsModel("VCWE", "CESC - WEST BENGAL", "VCWE"));
    opItems.add(new OperatorsModel("VBYE", "BSES Yamuna - DELHI", "VBYE"));
    opItems.add(new OperatorsModel("VBRE", "BSES Rajdhani - DELHI", "VBRE"));
    opItems.add(new OperatorsModel("VBME", "BEST Undertaking - MUMBAI", "VBME"));
    opItems.add(new OperatorsModel("VBBE", "BESCOM - BENGALURU", "VBBE"));
    opItems.add(new OperatorsModel("VJRE", "Jaipur Vidyut Vitran Nigam - RAJASTHAN", "VJRE"));
    opItems.add(new OperatorsModel("VAAE", "APDCL - ASSAM", "VAAE"));

    return opItems;
  }
}
