package in.msewa.fragment.fragmentmobilerecharge;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.msewa.adapter.CircleSpinnerAdapter;
import in.msewa.adapter.OperatorSpinnerAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.CircleModel;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.BrowsePrepaidPlanActivity;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.PayingDetailsValidation;


public class PrepaidFragment extends Fragment implements MPinVerifiedListner {

  private static final int PICK_CONTACT = 1;
  private static final int PICK_PLAN = 2;

  private static final int PLUS_SIGN_POS = 0;
  private static final int MOBILE_DIGITS = 10;

  private Spinner spinnerPrepaidCircle;
  private Spinner spinnerPrepaidProvider;
  private MaterialEditText etPrepaidAmount;
  private MaterialEditText etPrepaidNo;
  private Button btnPrepaidPay;
  private View rootView;

  private View focusView = null;
  private String fixedAmountPlanValue;
  private String amount, toMobileNumber, serviceProvider, serviceProviderName, area;

  private ImageButton ibtnPhoneBook;
  private Button btnMobileBrowsePlanPrepaid;
  private boolean cancel;


  private static HashMap<String, Integer> operatorIdCode;
  private static HashMap<String, Integer> circleIdCode;

  private List<CircleModel> circleList;
  private List<OperatorsModel> operatorList;

  private CircleSpinnerAdapter spinnerAdapterCircle;
  private OperatorSpinnerAdapter spinnerAdapterOperator;


  private String operatorCode;

  private RadioGroup rgPlansType;
  private RadioButton rbtnPlan1;
  private RadioButton rbtnPlan2;

  private LoadingDialog loadDlg;

  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;

  //Volley Tag
  private String tag_json_obj = "json_topup_pay";
  private MyReceiver r;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());

    circleList = Select.from(CircleModel.class).list();
    operatorList = Select.from(OperatorsModel.class).list();
    operatorIdCode = new HashMap<>();
    circleIdCode = new HashMap<>();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_mobile_prepaid, container, false);
    spinnerPrepaidProvider = (Spinner) rootView.findViewById(R.id.spinner_mob_provider_prepaid);
    spinnerPrepaidCircle = (Spinner) rootView.findViewById(R.id.spinner_mobile_circle_prepaid);

    rgPlansType = (RadioGroup) rootView.findViewById(R.id.rgPlansType);
    rbtnPlan1 = (RadioButton) rootView.findViewById(R.id.rbtnPlan1);
    rbtnPlan2 = (RadioButton) rootView.findViewById(R.id.rbtnPlan2);
    rgPlansType.setVisibility(View.GONE);

    if (circleList != null && circleList.size() != 0) {
      circleList.add(0, new CircleModel("", "Select your circle"));
      spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
      spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
    }

    if (operatorList != null && operatorList.size() != 0) {
      operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
      spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList,"topup");
      spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
    }
    if (operatorList.size() == 0 || circleList.size() == 0) {
      loadOperatorCircleNew();
    } else {

      for (int i = 1; i < operatorList.size(); i++) {
        operatorIdCode.put(operatorList.get(i).getCode(), i);
      }
      for (int i = 1; i < circleList.size(); i++) {
        circleIdCode.put(circleList.get(i).getCode(), i);
      }
    }

    declare();

    return rootView;
  }


  private void declare() {
    ibtnPhoneBook = (ImageButton) rootView.findViewById(R.id.ibtnPhoneBook);
    etPrepaidNo = (MaterialEditText) rootView.findViewById(R.id.edt_mobile_number_prepaid);
    etPrepaidNo.requestFocus();
    etPrepaidAmount = (MaterialEditText) rootView.findViewById(R.id.edt_mob_amount_prepaid);
    btnPrepaidPay = (Button) rootView.findViewById(R.id.btn_pay_mob_prepaid);
    btnMobileBrowsePlanPrepaid = (Button) rootView.findViewById(R.id.btnMobileBrowsePlanPrepaid);


    etPrepaidNo.addTextChangedListener(new TextWatcher() {

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        String number = etPrepaidNo.getText().toString();
        if (number.length() == 10) {
          loadAccordingToNo(number);
        }
      }
    });


    spinnerPrepaidCircle.setOnItemSelectedListener(new OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        area = ((CircleModel) spinnerPrepaidCircle.getItemAtPosition(position)).code;
      }

      @Override
      public void onNothingSelected(AdapterView<?> arg0) {

      }
    });

    spinnerPrepaidProvider.setOnItemSelectedListener(new OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        serviceProvider = ((OperatorsModel) spinnerPrepaidProvider.getItemAtPosition(i)).code;
        operatorCode = ((OperatorsModel) spinnerPrepaidProvider.getItemAtPosition(i)).defaultCode;
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });


    btnMobileBrowsePlanPrepaid.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View arg0) {
        if (spinnerPrepaidProvider == null || spinnerPrepaidCircle == null || spinnerPrepaidCircle.getSelectedItemPosition() == 0 || spinnerPrepaidProvider.getSelectedItemPosition() == 0) {
          CustomToast.showMessage(getActivity(), "Select provider and area to browse");
        } else {
          Intent planIntent = new Intent(getActivity(), BrowsePrepaidPlanActivity.class);
          planIntent.putExtra("operatorCode", operatorCode);
          planIntent.putExtra("circleCode", area);
          startActivityForResult(planIntent, PICK_PLAN);
        }
      }
    });


    //DONE CLICK ON VIRTUAL KEYPAD
    etPrepaidAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });


    btnPrepaidPay.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View arg0) {
        attemptPayment();

      }
    });

    ibtnPhoneBook.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK, Phone.CONTENT_URI);
        intent.setType(Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
      }
    });
  }


  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    String[] projection = {Phone.DISPLAY_NAME, Phone.NUMBER};
    switch (requestCode) {
      case (PICK_CONTACT):
        if (resultCode == Activity.RESULT_OK) {
          Uri contactUri = data.getData();
          Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
          if (c != null) {
            if (c.moveToFirst()) {
              String phoneNumber = c.getString(c.getColumnIndex(Phone.NUMBER));
              String finalNumber = phoneNumber.replaceAll("[^0-9+]", "");

              if (finalNumber != null && !finalNumber.isEmpty()) {
                removeCountryCode(finalNumber);
              }
            }
            c.close();
          }
        }
        break;
      case (PICK_PLAN):
        if (resultCode == Activity.RESULT_OK) {
          String newText = data.getStringExtra("amount");
          etPrepaidAmount.setText(newText);
        }
        break;
      case (651):
        etPrepaidAmount.setText("");
        etPrepaidNo.setText("");
        break;

    }
  }

  private void removeCountryCode(String number) {
    if (hasCountryCode(number)) {
      int country_digits = number.length() - MOBILE_DIGITS;
      number = number.substring(country_digits);
      etPrepaidNo.setText(number);
    } else if (hasZero(number)) {
      if (number.length() >= 10) {
        int country_digits = number.length() - MOBILE_DIGITS;
        number = number.substring(country_digits);
        etPrepaidNo.setText(number);
      } else {
        CustomToast.showMessage(getActivity(), "Please select 10 digit no");
      }

    } else {
      etPrepaidNo.setText(number);
    }

  }

  private boolean hasCountryCode(String number) {
    return number.charAt(PLUS_SIGN_POS) == '+';
  }

  private boolean hasZero(String number) {
    return number.charAt(PLUS_SIGN_POS) == '0';
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    System.gc();
  }

  public void loadOperatorCircleNew() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      loadDlg.show();
      AndroidNetworking.post(ApiUrl.URL_CIRCLE_OPERATOR)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject JsonObj) {
          loadDlg.dismiss();
          Log.i("LOAD CIRCLE OP ", JsonObj.toString());
          try {
            operatorList.clear();
            circleList.clear();
            loadDlg.dismiss();
            operatorIdCode.clear();
            circleIdCode.clear();

            OperatorsModel.deleteAll(OperatorsModel.class);
            CircleModel.deleteAll(CircleModel.class);
            //New Implementation

            String jsonString = JsonObj.getString("response");

            JSONObject response = new JSONObject(jsonString);

            JSONArray operatorArray = response.getJSONArray("operators");

            operatorList.add(new OperatorsModel("", "Select your operator", ""));

            for (int i = 0; i < operatorArray.length(); i++) {
              JSONObject c = operatorArray.getJSONObject(i);
              String op_code = c.getString("serviceCode");
              String op_name = c.getString("name");
              String op_default_code = c.getString("code");
              OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
              oModel.save();
              operatorList.add(oModel);
              operatorIdCode.put(op_code, i + 1);
            }


            JSONArray circleArray = response.getJSONArray("circles");

            circleList.add(new CircleModel("", "Select your circle"));
            for (int i = 0; i < circleArray.length(); i++) {
              JSONObject c = circleArray.getJSONObject(i);
              String op_code = c.getString("code");
              String op_name = c.getString("name");
              CircleModel cModel = new CircleModel(op_code, op_name);
              cModel.save();
              circleList.add(cModel);
              circleIdCode.put(op_code, i + 1);
            }


            if (operatorList != null && operatorList.size() != 0) {
              try {
                spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList, "topup");
                spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
              } catch (Exception e) {
                e.printStackTrace();
              }
            }

            if (circleList != null && circleList.size() != 0) {
              try {
                spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
                spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);

              } catch (Exception e) {
                e.printStackTrace();
              }
            }
            loadDlg.dismiss();


          } catch (JSONException e) {
            e.printStackTrace();
            loadDlg.dismiss();
            getActivity().finish();
            Toast.makeText(getActivity(), getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          getActivity().finish();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
    }

  }

  public void loadAccordingToNo(String number) {
    jsonRequest = new JSONObject();
    loadDlg.show();
    try {
      jsonRequest.put("mobileNumber", number);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());

      AndroidNetworking.post(ApiUrl.URL_LOAD_ACCORDING_NO)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          String op_code, circle_code;
          try {
            loadDlg.dismiss();
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              String responseString = response.getString("response");
              JSONObject responseObj = new JSONObject(responseString);

              JSONObject jsonOperator = responseObj.getJSONObject("operator");
              op_code = jsonOperator.getString("serviceCode");

              JSONObject jsonCircle = responseObj.getJSONObject("circle");
              circle_code = jsonCircle.getString("code");

              if (operatorIdCode != null && op_code != null && !op_code.isEmpty()) {
                int op_selection = operatorIdCode.get(op_code);
                spinnerPrepaidProvider.setSelection(op_selection);

              }
              if (circleIdCode != null && circle_code != null && !circle_code.isEmpty()) {
                int circle_selection = circleIdCode.get(circle_code);
                spinnerPrepaidCircle.setSelection(circle_selection);

              }
              loadDlg.dismiss();
            }

          } catch (JSONException e) {
            e.printStackTrace();
            loadDlg.dismiss();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
        }
      });
    }
  }


  private void attemptPayment() {
    etPrepaidAmount.setError(null);
    etPrepaidNo.setError(null);
    cancel = false;
    amount = etPrepaidAmount.getText().toString();
    toMobileNumber = etPrepaidNo.getText().toString();

    serviceProvider = ((OperatorsModel) spinnerPrepaidProvider.getSelectedItem()).code;
    serviceProviderName = ((OperatorsModel) spinnerPrepaidProvider.getSelectedItem()).name;
    area = ((CircleModel) spinnerPrepaidCircle.getSelectedItem()).code;

    checkPayAmount(amount);
    checkPhone(toMobileNumber);
    View view = getActivity().getCurrentFocus();
    if (view != null) {
      InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    if (spinnerPrepaidCircle.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select area to continue");
      return;
    }

    if (spinnerPrepaidProvider.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();

    }
  }


  private void checkPhone(String phNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
    if (!gasCheckLog.isValid) {
      etPrepaidNo.setError(getString(gasCheckLog.msg));
      focusView = etPrepaidNo;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etPrepaidAmount.setError(getString(gasCheckLog.msg));
      focusView = etPrepaidAmount;
      cancel = true;
    } else if (Integer.valueOf(etPrepaidAmount.getText().toString()) < 10) {
      etPrepaidAmount.setError(getString(R.string.lessAmount));
      focusView = etPrepaidAmount;
      cancel = true;
    }
  }

  public void checkUpBalance() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("topupType", "Prepaid");
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("mobileNo", etPrepaidNo.getText().toString());
      jsonRequest.put("amount", etPrepaidAmount.getText().toString());
      jsonRequest.put("area", area);
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_CHECK_BALANCE_FOR_SPLIT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .build().getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("Prepaid RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              promoteTopUpNew();
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              JSONObject dtoObject = response.getJSONObject("dto");
              String splitAmount = dtoObject.getString("splitAmount");
              String message = response.getString("message");
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "TOPUP");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }


            } else if (code != null && code.equalsIgnoreCase("F03")) {
              loadDlg.dismiss();
              sessionInvalid(response.getString("message"));
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
    }

  }

  public void promoteTopUpNew() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("topupType", "Prepaid");
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("mobileNo", etPrepaidNo.getText().toString());
      jsonRequest.put("amount", etPrepaidAmount.getText().toString());
      jsonRequest.put("area", area);
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_PREPAID_TOPUP)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .build().getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("Prepaid RESPONSE", response.toString());
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String sucessMessage = jsonObject.getString("balance");

//                            sendRefresh();
              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }
              loadDlg.dismiss();

              session.setUserBalance(sucessMessage);
              session.save();
              showSuccessDialog();
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              String splitAmount = String.valueOf(Double.parseDouble(etPrepaidAmount.getText().toString()) - Double.parseDouble(session.getUserBalance()));
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
//              if (Double.parseDouble(splitAmount) >= Double.parseDouble("10")) {
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "TOPUP");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }
//              } else {
//                if (!splitAmount.isEmpty()) {
//                  splitAmount = "10";
//                  mainMenuDetailActivity.putExtra("TYPE", "TOPUP");
//                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
//                  mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
//                  mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
//                  startActivity(mainMenuDetailActivity);
//                } else {
//                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
//                }
//              }


            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(response.getString("message"));
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

        }
      });
    }
  }

  public void showInvalidSessionDialog(String message) {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }


  public void showCustomDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(generateMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(generateMessage());
    }
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          promoteTopUpNew();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public String generateMessage() {
    return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }


  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Recharge Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

        sendRefresh();
      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
  }


  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }

  @Override
  public void verifiedCompleted() {
    promoteTopUpNew();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog(message);
  }


  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putParcelableArrayList("curChoice", (ArrayList<? extends Parcelable>) circleList);
    outState.putString("curChoice1", String.valueOf(operatorIdCode));
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    CustomToast.cancelToast();
  }

  public void setText(String text) {
    etPrepaidNo.setText("");
    etPrepaidAmount.setText("");
  }


  public void refresh() {
    //yout code in refresh.
    Log.i("Refresh", "YES");
    etPrepaidAmount.setText("");
    etPrepaidNo.setText("");
    etPrepaidNo.setError(null);
    etPrepaidAmount.setError(null);
//        spinnerAdapterCircle.notifyDataSetChanged();
//        spinnerAdapterOperator.notifyDataSetChanged();
    if (circleList != null && circleList.size() != 0) {
      spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
      spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
    }

    if (operatorList != null && operatorList.size() != 0) {
      spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList, "topup");
      spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
    }

  }

  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
  }

  public void onResume() {
    super.onResume();
    r = new MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r, new IntentFilter("TAG_REFRESH"));
  }

  private class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      PrepaidFragment.this.refresh();
    }
  }

}
