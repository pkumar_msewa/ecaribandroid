package in.msewa.fragment.fragmentmobilerecharge;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.msewa.adapter.OperatorSpinnerAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.metadata.MenuMetadata;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.PayingDetailsValidation;


/**
 * Created by Ksf on 3/16/2016.
 */
public class PostpaidFragment extends Fragment implements MPinVerifiedListner {
  private View rootView;
  private MaterialEditText edt_mobile_number_postpaid;
  private ImageButton ibtnPhoneBook;
  private Spinner spinner_mob_provider_postpaid;
  private MaterialEditText edt_mob_amount_postpaid;
  private Button btn_pay_mob_postpaid;

  private OperatorSpinnerAdapter operatorSpinnerAdapter;
  private LoadingDialog loadDlg;

  private UserModel session = UserModel.getInstance();
  private boolean cancel;
  private View focusView = null;

  private String amount, toMobileNumber, serviceProvider, serviceProviderName;
  private ArrayList<OperatorsModel> operator;
  private JSONObject jsonRequest;

  private static final int PICK_CONTACT = 1;
  private static final int PLUS_SIGN_POS = 0;
  private static final int MOBILE_DIGITS = 10;

  //Volley Tag
  private String tag_json_obj = "json_topup_pay";
  private MyReceiver r;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
    operator = MenuMetadata.getPostOperator();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_mobile_postpaid, container, false);
    edt_mobile_number_postpaid = (MaterialEditText) rootView.findViewById(R.id.edt_mobile_number_postpaid);
    ibtnPhoneBook = (ImageButton) rootView.findViewById(R.id.ibtnPhoneBook);
    spinner_mob_provider_postpaid = (Spinner) rootView.findViewById(R.id.spinner_mob_provider_postpaid);
    edt_mob_amount_postpaid = (MaterialEditText) rootView.findViewById(R.id.edt_mob_amount_postpaid);
    btn_pay_mob_postpaid = (Button) rootView.findViewById(R.id.btn_pay_mob_postpaid);

    operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operator, "topup");
    spinner_mob_provider_postpaid.setAdapter(operatorSpinnerAdapter);

    //DONE CLICK ON VIRTUAL KEYPAD
    edt_mob_amount_postpaid.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });

    btn_pay_mob_postpaid.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });

    ibtnPhoneBook.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
      }
    });

    return rootView;
  }


  private void attemptPayment() {
    edt_mob_amount_postpaid.setError(null);
    edt_mobile_number_postpaid.setError(null);
    cancel = false;

    amount = edt_mob_amount_postpaid.getText().toString();
    toMobileNumber = edt_mobile_number_postpaid.getText().toString();

    checkPayAmount(amount);
    checkPhone(toMobileNumber);

    if (spinner_mob_provider_postpaid.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select area to continue");
      return;
    } else {
      serviceProviderName = operator.get(spinner_mob_provider_postpaid.getSelectedItemPosition()).getName();
      serviceProvider = operator.get(spinner_mob_provider_postpaid.getSelectedItemPosition()).getCode();
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();

    }
  }


  private void checkPhone(String phNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
    if (!gasCheckLog.isValid) {
      edt_mobile_number_postpaid.setError(getString(gasCheckLog.msg));
      focusView = edt_mobile_number_postpaid;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      edt_mob_amount_postpaid.setError(getString(gasCheckLog.msg));
      focusView = edt_mob_amount_postpaid;
      cancel = true;
    } else if (Integer.valueOf(edt_mob_amount_postpaid.getText().toString()) < 10) {
      edt_mob_amount_postpaid.setError(getString(R.string.lessAmount));
      focusView = edt_mob_amount_postpaid;
      cancel = true;
    }
  }

  public void checkUpBalance() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("topupType", "Postpaid");
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("mobileNo", edt_mobile_number_postpaid.getText().toString());
      jsonRequest.put("amount", edt_mob_amount_postpaid.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_CHECK_BALANCE_FOR_SPLIT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("Prepaid RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              promoteTopUpNew();
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              JSONObject dtoObject = response.getJSONObject("dto");
              String splitAmount = dtoObject.getString("splitAmount");
              String message = response.getString("message");
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "TOPUP");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
//                                getActivity().finish();
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }


            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
    }

  }

  public void promoteTopUpNew() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("topupType", "Postpaid");
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("mobileNo", edt_mobile_number_postpaid.getText().toString());
      jsonRequest.put("amount", edt_mob_amount_postpaid.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      AndroidNetworking.post(ApiUrl.URL_POSTPAID_TOPUP)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String sucessMessage = jsonObject.getString("balance");

              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }
              loadDlg.dismiss();
              session.setUserBalance(sucessMessage);
              session.save();
              showSuccessDialog();
            }else
            if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              String splitAmount = String.valueOf(Double.parseDouble(edt_mob_amount_postpaid.getText().toString()) - Double.parseDouble(session.getUserBalance()));

              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "TOPUP");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
//                                getActivity().finish();
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }


            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(response.getString("message"));
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });

    }
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }


  public void showCustomDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          promoteTopUpNew();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public String generateMessage() {
    return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
    switch (requestCode) {
      case (PICK_CONTACT):
        if (resultCode == Activity.RESULT_OK) {
          Uri contactUri = data.getData();
          Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
          if (c != null) {
            if (c.moveToFirst()) {
              String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
              String finalNumber = phoneNumber.replaceAll("[^0-9+]", "");

              if (finalNumber != null && !finalNumber.isEmpty()) {
                removeCountryCode(finalNumber);
              }
            }
            c.close();
          }
        }
        break;
    }
  }

  private void removeCountryCode(String number) {
    if (hasCountryCode(number)) {
      int country_digits = number.length() - MOBILE_DIGITS;
      number = number.substring(country_digits);
      edt_mobile_number_postpaid.setText(number);
    } else if (hasZero(number)) {
      if (number.length() >= 10) {
        int country_digits = number.length() - MOBILE_DIGITS;
        number = number.substring(country_digits);
        edt_mobile_number_postpaid.setText(number);
      } else {
        CustomToast.showMessage(getActivity(), "Please select 10 digit no");
      }
    } else {
      edt_mobile_number_postpaid.setText(number);
    }

  }

  private boolean hasCountryCode(String number) {
    return number.charAt(PLUS_SIGN_POS) == '+';
  }

  private boolean hasZero(String number) {
    return number.charAt(PLUS_SIGN_POS) == '0';
  }


  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }


  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Recharge Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();

      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source =
      "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
    return source;
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }

  @Override
  public void verifiedCompleted() {
    promoteTopUpNew();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog(message);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  public void refresh() {
    //yout code in refresh.
    edt_mobile_number_postpaid.setText("");
    edt_mob_amount_postpaid.setText("");
    edt_mobile_number_postpaid.setError(null);
    edt_mob_amount_postpaid.setError(null);
    operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operator, "topup");
    spinner_mob_provider_postpaid.setAdapter(operatorSpinnerAdapter);
  }

  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
  }

  public void onResume() {
    super.onResume();
    r = new MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r, new IntentFilter("TAG_REFRESH"));
  }

  private class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      PostpaidFragment.this.refresh();
    }
  }
}
