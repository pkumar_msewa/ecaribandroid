package in.msewa.fragment.fragmentpaybills;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.JsonArray;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.OperatorGasSpinnerAdapter;
import in.msewa.adapter.OperatorSpinnerAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.metadata.MenuMetadata;
import in.msewa.model.BillPaymentOperatorsModel;
import in.msewa.model.GasModel;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.GenerateAPIHeader;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.PayingDetailsValidation;


/**
 * Created by Ksf on 3/16/2016.
 */
public class GasFragment extends Fragment implements MPinVerifiedListner {
  private View rootView;
  private Spinner spinner_dth_provider;
  private OperatorGasSpinnerAdapter operatorSpinnerAdapter;
  private Button btn_pay_dth;
  private MaterialEditText edt_dth_amount;
  private MaterialEditText edt_dth_number;
  private ArrayList<GasModel> gasModels;
  private View focusView = null;
  private boolean cancel;

  //User Values

  private String amount, operator, customerNo, serviceProviderName;
  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;

  private String additionalCharges = "0";
  private String payAmount;
  //Volley Tag
  private String tag_json_obj = "json_bill_pay";
  private MyReceiver r;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
    gasModels = new ArrayList<>();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_gas, container, false);
    spinner_dth_provider = (Spinner) rootView.findViewById(R.id.spinner_dth_provider);
    btn_pay_dth = (Button) rootView.findViewById(R.id.btn_pay_dth);
    edt_dth_amount = (MaterialEditText) rootView.findViewById(R.id.edt_dth_amount);
    edt_dth_number = (MaterialEditText) rootView.findViewById(R.id.edt_dth_number);

//    edt_dth_number.setHint("Enter customer a/c no");
    edt_dth_amount.setHint("Enter amount in INR");
    getGasOperators();
    if (((GasModel) spinner_dth_provider.getSelectedItem() != null)) {
      GasModel gasModel = ((GasModel) spinner_dth_provider.getSelectedItem());
      edt_dth_number.setHint("Enter".concat(gasModel.getAccountNumberPattern()));
      edt_dth_number.setFilters(new InputFilter[]{new NumericInputFilter(gasModel)});
    }
    spinner_dth_provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });
    //DONE CLICK ON VIRTUAL KEYPAD
    edt_dth_amount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });

    btn_pay_dth.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();

      }
    });


    spinner_dth_provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
          GasModel gasModel = ((GasModel) adapterView.getItemAtPosition(i));
          edt_dth_number.setHint("Enter".concat(gasModel.getAccountNumberPattern()));
          edt_dth_number.setFilters(new InputFilter[]{new NumericInputFilter(gasModel)});
//        if (i != 0) {
////          showExactAmountDialog();
//
//        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });

    return rootView;
  }


  private void attemptPayment() {
    edt_dth_amount.setError(null);
    edt_dth_number.setError(null);
    cancel = false;

    amount = edt_dth_amount.getText().toString();
    customerNo = edt_dth_number.getText().toString();
    operator = ((GasModel) spinner_dth_provider.getSelectedItem()).getOperatorCode();
    serviceProviderName = ((GasModel) spinner_dth_provider.getSelectedItem()).getOperatorName();


    checkPayAmount(amount);
    checkCustomerNo(customerNo);

    if (spinner_dth_provider.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    } else if (spinner_dth_provider.getSelectedItemPosition() == 3) {
      additionalCharges = "2";
    } else if (spinner_dth_provider.getSelectedItemPosition() == 4) {
      additionalCharges = "2";
    } else {
      additionalCharges = "0";
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();
    }
  }


  private void checkCustomerNo(String acNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(acNo);
    if (!gasCheckLog.isValid) {
      edt_dth_number.setError(getString(gasCheckLog.msg));
      focusView = edt_dth_number;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      edt_dth_amount.setError(getString(gasCheckLog.msg));
      focusView = edt_dth_amount;
      cancel = true;
    } else if (!TextUtils.isEmpty(amount)) {
      if (Integer.valueOf(amount) < 10) {
        edt_dth_amount.setError(getString(R.string.lessAmount));
        focusView = edt_dth_amount;
        cancel = true;
      }
    }
  }


  private void getGasOperators() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("operatorName", "Gas");
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    Log.i("JsonRequest", jsonRequest.toString());
    AndroidNetworking.post(ApiUrl.URL_GAS_OPERATOR)
      .addJSONObjectBody(jsonRequest)
      .setPriority(Priority.HIGH)
      .setTag("test")
      .build().getAsJSONObject(new JSONObjectRequestListener() {
      @Override
      public void onResponse(JSONObject response) throws JSONException {
        loadDlg.dismiss();
        String code = response.getString("code");
        if (code != null && code.equalsIgnoreCase("S00")) {
          loadDlg.dismiss();
          String res = response.getString("detail");
          JSONArray gasOperators = new JSONArray(res);
          for (int i = 0; i < gasOperators.length(); i++) {
            gasModels.add(new GasModel(gasOperators.getJSONObject(i).getString("accountNumberPattern"), gasOperators.getJSONObject(i).getString("accountNumberType"), gasOperators.getJSONObject(i).getString("gasImages"), gasOperators.getJSONObject(i).getString("operatorCode"), gasOperators.getJSONObject(i).getString("operatorName")));
          }
          if (gasModels.size() != 0) {
            operatorSpinnerAdapter = new OperatorGasSpinnerAdapter(getActivity(), R.layout.gas_item_list, gasModels);
            spinner_dth_provider.setAdapter(operatorSpinnerAdapter);

          }
        } else

        {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), response.getString("message"));
        }
      }


      @Override
      public void onError(ANError anError) {
        loadDlg.dismiss();
        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

      }
    });

  }

  private void promotePayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", operator);
      jsonRequest.put("accountNumber", edt_dth_number.getText().toString());
      jsonRequest.put("amount", amount);
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GAS_PAYMENT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String sucessMessage = jsonObject.getString("balance");
              session.setUserBalance(sucessMessage);
              session.save();
              loadDlg.dismiss();

              showSuccessDialog();
              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }

            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(message);
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              String splitAmount = String.valueOf(Double.parseDouble(edt_dth_amount.getText().toString()) - Double.parseDouble(session.getUserBalance()));
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (Double.parseDouble(splitAmount) >= Double.parseDouble("10")) {
                if (!splitAmount.isEmpty()) {
                  mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE", "GAS");
                  mainMenuDetailActivity.putExtra("payAmount", payAmount);
                  mainMenuDetailActivity.putExtra("AddCharge", additionalCharges);
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_GAS_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              } else {
                if (!splitAmount.isEmpty()) {
                  splitAmount = "10";
                  mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE", "GAS");
                  mainMenuDetailActivity.putExtra("payAmount", payAmount);
                  mainMenuDetailActivity.putExtra("AddCharge", additionalCharges);
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_GAS_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              }


            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {

                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
          error.printStackTrace();


        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_GAS + session.getUserSessionId()));
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void showCustomDialog() {
    payAmount = Integer.valueOf(Integer.valueOf(additionalCharges) + Integer.valueOf(amount)) + "";

    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          promotePayment();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public String generateMessage() {
    String source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> Account No: </font></b>" + "<font>" + customerNo + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br>" +
      "<b><font color=#000000> Service Charge: </font></b>" + "<font>" + additionalCharges + "</font><br>" +
      "<b><font color=#000000> Total Charge: </font></b>" + "<font>" + payAmount + "</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    return source;
  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog(String messgae) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(messgae));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void showExactAmountDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, getActivity().getResources().getString(R.string.exactAmount));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();

      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> Account No: </font></b>" + "<font>" + customerNo + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br>" +
      "<b><font color=#000000> Service Charge: </font></b>" + "<font>" + additionalCharges + "</font><br>" +
      "<b><font color=#000000> Total Charge: </font></b>" + "<font>" + payAmount + "</font><br><br>";
    return source;
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }

  @Override
  public void verifiedCompleted() {
    promotePayment();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog(message);
  }


  public void refresh() {
    //yout code in refresh.
    Log.i("Refresh", "YES");
    edt_dth_amount.setError(null);
    edt_dth_number.setError(null);
    edt_dth_amount.setText("");
    edt_dth_number.setText("");

//    operatorSpinnerAdapter.notifyDataSetChanged();

//    spinner_dth_provider.setAdapter(operatorSpinnerAdapter);
  }

  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
  }

  public void onResume() {
    super.onResume();
    r = new MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r,
      new IntentFilter("TAG_REFRESH"));
  }

  private class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      GasFragment.this.refresh();
    }
  }

  public static class NumericInputFilter implements InputFilter {
    GasModel model;

    public NumericInputFilter(GasModel operatorsModel) {
      this.model = operatorsModel;
    }

    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {

      // Only keep characters that are alphanumeric
      StringBuilder builder = new StringBuilder();
      for (int i = start; i < end; i++) {
        char c = source.charAt(i);
        if (model.getAccountNumberType().equalsIgnoreCase("numeric")) {
          if (Character.isDigit(c)) {
            builder.append(c);
          }
        }
        if (model.getAccountNumberType().equalsIgnoreCase("alphanumeric")) {
          if (Character.isLetterOrDigit(c)) {
            builder.append(c);
          }
        }
      }

      // If all characters are valid, return null, otherwise only return the filtered characters
      boolean allCharactersValid = (builder.length() == end - start);
      return allCharactersValid ? null : builder.toString();
    }
  }

}
