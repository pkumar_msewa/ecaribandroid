package in.msewa.fragment.fragmentpaybills;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import in.msewa.adapter.OperatorSpinnerBillpaymentAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomAlertDialogElectrictySearch;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BillPaymentOperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.util.ElectrySelectedListener;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.PayingDetailsValidation;


/**
 * Created by Ksf on 3/16/2016.
 */
public class ElectricityFragment extends Fragment implements MPinVerifiedListner {
  private View rootView;
  private MaterialEditText electricity_operator;
  private MaterialEditText etElecAmount;
  private MaterialEditText etElecCycleNo;
  private MaterialEditText etElecACNo;
  private MaterialEditText etElecCityName;
  private Button btnElecPay;
  private ImageButton btnDeuElec;


  private View focusView = null;
  private boolean cancel;

  //User Values

  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();
  private String amount, serviceProvider, customerNo, serviceProviderName;
  private String cycleNo = "";
  private String cityName = "";

  private JSONObject jsonRequest;

  private String additionalCharges = "0";
  private String payAmount;

  //Volley Tag
  private String tag_json_obj = "json_bill_pay";
  private List<BillPaymentOperatorsModel> operatorList;
  private OperatorSpinnerBillpaymentAdapter spinnerAdapterOperator;
  private boolean isValidDueAmount = false;
  private int currentPostion = 0;
  private LinearLayout llBillInfom;
  private MaterialEditText etElecBillMonth;
  private MaterialEditText etElecBillAddress, etElecBillYear, etElecBillTransction;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
    loadDlg = new LoadingDialog(getActivity());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_elecricty, container, false);
    electricity_operator = (MaterialEditText) rootView.findViewById(R.id.spinner_electricity_operator);
    etElecACNo = (MaterialEditText) rootView.findViewById(R.id.etElecACNo);
    etElecCycleNo = (MaterialEditText) rootView.findViewById(R.id.etElecCycleNo);
    etElecAmount = (MaterialEditText) rootView.findViewById(R.id.etElecAmount);
    etElecCityName = (MaterialEditText) rootView.findViewById(R.id.etElecCityName);
    llBillInfom = (LinearLayout) rootView.findViewById(R.id.llBillInfom);
    etElecBillMonth = (MaterialEditText) rootView.findViewById(R.id.etElecBillMonth);
    etElecBillAddress = (MaterialEditText) rootView.findViewById(R.id.etElecBillAddress);
    etElecBillTransction = (MaterialEditText) rootView.findViewById(R.id.etElecBillTransction);
    etElecBillYear = (MaterialEditText) rootView.findViewById(R.id.etElecBillYear);
    btnElecPay = (Button) rootView.findViewById(R.id.btnElecPay);
    btnDeuElec = (ImageButton) rootView.findViewById(R.id.btnDeuElec);
    operatorList = Select.from(BillPaymentOperatorsModel.class).list();

//    if (operatorList.size() == 0) {
    loadElectricityOperators();
//    }
    electricity_operator.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Collections.sort(operatorList, new Comparator<BillPaymentOperatorsModel>() {
          @Override
          public int compare(BillPaymentOperatorsModel lhs, BillPaymentOperatorsModel rhs) {
            return lhs.getName().compareTo(rhs.getName());
          }
        });
        final CustomAlertDialogElectrictySearch search = new CustomAlertDialogElectrictySearch(getActivity(), operatorList, "from", new ElectrySelectedListener() {
          @Override
          public void Selected(BillPaymentOperatorsModel type, int postion) {
            currentPostion = postion;
            electricity_operator.setText(type.getName());
            serviceProvider = type.getCode();
            serviceProviderName = type.getName();
            llBillInfom.setVisibility(View.GONE);
            etElecACNo.setText("");
            if (serviceProvider.equals("VREE")) {
              etElecCycleNo.setVisibility(View.VISIBLE);
              etElecCycleNo.setText("");
              etElecACNo.setFilters(new InputFilter[]{new NumericInputFilter(type)});
              etElecACNo.setHint("Account Number");

            } else if (serviceProvider.equals("VTPE")) {
              etElecCityName.setVisibility(View.VISIBLE);
              etElecCityName.setText("");
              etElecACNo.setHint("Service Connection Number");
            } else if (serviceProvider.equals("VTPE")) {
              etElecACNo.setHint("Service Connection Number");
            } else if (serviceProvider.equals("VCCE")) {
              etElecACNo.setHint("BP Number");
            } else if (serviceProvider.equals("VDHE")) {
              etElecACNo.setHint("Account Number");
              etElecACNo.setFilters(new InputFilter[]{new NumericInputFilter(type)});
            } else if (serviceProvider.equals("VSAE") || serviceProvider.equals("VDNE")) {
              etElecACNo.setHint("Service Connection Number");
            } else if (serviceProvider.equals("VJUE")) {
              etElecACNo.setHint("Business Partner Number");
            } else if (serviceProvider.equals("VARE") || serviceProvider.equals("VJRE") || serviceProvider.equals("VDRE")) {
              etElecACNo.setHint("BK Number");
              etElecACNo.setFilters(new InputFilter[]{new NumericInputFilter(type)});
            } else if (serviceProvider.equalsIgnoreCase("VMDE")) {
              etElecACNo.setHint("Consumer Number");
              etElecCityName.setHint("Billing Unit");
              etElecACNo.setFilters(new InputFilter[]{new NumericInputFilter(type)});
              etElecCityName.setVisibility(View.VISIBLE);
            } else if (serviceProvider.equalsIgnoreCase("VUKE")) {
              llBillInfom.setVisibility(View.GONE);
              etElecACNo.setFilters(new InputFilter[]{new NumericInputFilter(type)});
              etElecACNo.setHint("Account Number");
            } else {
              etElecACNo.setFilters(new InputFilter[]{new NumericInputFilter(type)});
              etElecACNo.setHint("Consumer Number");
              cycleNo = "";
              etElecCycleNo.setVisibility(View.GONE);
            }
            isValidDueAmount = false;
          }


        });
        search.show();
      }
    });

    etElecAmount.setFocusable(false);
    etElecAmount.setFocusableInTouchMode(false);
    etElecAmount.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (isValidDueAmount) {

        } else {
          showExactAmountDialog();
        }

      }
    });

    //DONE CLICK ON VIRTUAL KEYPAD
    etElecAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });

    btnElecPay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });


    btnDeuElec.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptDeu();
      }
    });
    return rootView;
  }

  public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
    int width = bm.getWidth();
    int height = bm.getHeight();
    float scaleWidth = ((float) newWidth) / width;
    float scaleHeight = ((float) newHeight) / height;
    Matrix matrix = new Matrix();
// RESIZE THE BIT MAP
    matrix.postScale(scaleWidth, scaleHeight);
    // RECREATE THE NEW BITMAP
    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
      matrix, false);
    return resizedBitmap;
  }

  private void attemptPayment() {

    etElecACNo.setError(null);
    etElecCycleNo.setError(null);
    etElecAmount.setError(null);
    etElecCityName.setError(null);
    if (TextUtils.isEmpty(electricity_operator.getText().toString())) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    } else if (currentPostion == 1) {
      additionalCharges = "2";
    } else if (currentPostion == 2) {
      additionalCharges = "2";
    } else if (currentPostion == 4) {
      additionalCharges = "2";
    } else if (currentPostion == 5) {
      additionalCharges = "2";
    } else if (currentPostion == 7) {
      additionalCharges = "2";
    } else if (currentPostion == 8) {
      additionalCharges = "2";
    } else if (currentPostion == 9) {
      additionalCharges = "2";
    } else if (currentPostion == 10) {
      additionalCharges = "2";
    } else if (currentPostion == 11) {
      additionalCharges = "2";
    } else if (currentPostion == 12) {
      additionalCharges = "2";
    } else if (currentPostion == 13) {
      additionalCharges = "2";
    } else if (currentPostion == 14) {
      additionalCharges = "2";
    } else if (currentPostion == 15) {
      additionalCharges = "2";
    } else if (currentPostion == 16) {
      additionalCharges = "2";
    } else if (currentPostion == 17) {
      additionalCharges = "2";
    } else if (currentPostion == 18) {
      additionalCharges = "2";
    } else if (currentPostion == 19) {
      additionalCharges = "2";
    } else if (currentPostion == 22) {
      additionalCharges = "2";
    } else if (currentPostion == 23) {
      additionalCharges = "2";
    } else if (currentPostion == 24) {
      additionalCharges = "2";
    } else if (currentPostion == 25) {
      additionalCharges = "2";
    } else {
      additionalCharges = "0";
    }

    cancel = false;

    amount = etElecAmount.getText().toString();
    customerNo = etElecACNo.getText().toString();
    cycleNo = etElecCycleNo.getText().toString();
    cityName = etElecCityName.getText().toString();

    checkPayAmount(amount);
    checkCustomerNo(customerNo);

    if (etElecCycleNo.getVisibility() == View.VISIBLE) {
      checkCycle(cycleNo);
    }
    if (etElecCityName.getVisibility() == View.VISIBLE) {
      checkCityName(cityName);
    }

    if (llBillInfom.getVisibility() == View.VISIBLE) {
      checkName(etElecBillMonth.getText().toString(), etElecBillMonth);
      checkName(etElecBillAddress.getText().toString(), etElecBillAddress);
      checkName(etElecBillTransction.getText().toString(), etElecBillTransction);
      checkName(etElecBillYear.getText().toString(), etElecBillYear);
    }


    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();
    }
  }

  private void attemptDeu() {
    etElecACNo.setError(null);
    etElecCycleNo.setError(null);
    etElecCityName.setError(null);
    if (TextUtils.isEmpty(electricity_operator.getText().toString())) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    }

    cancel = false;

    customerNo = etElecACNo.getText().toString();
    cycleNo = etElecCycleNo.getText().toString();
    cityName = etElecCityName.getText().toString();
    checkCustomerNo(customerNo);

    if (etElecCycleNo.getVisibility() == View.VISIBLE) {
      checkCycle(cycleNo);
    }

    if (etElecCityName.getVisibility() == View.VISIBLE) {
      checkCityName(cityName);
    }


    if (cancel) {
      focusView.requestFocus();
    } else {
      getDeuAmount();
    }
  }

  private void checkCustomerNo(String acNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(acNo);
    if (!gasCheckLog.isValid) {
      etElecACNo.setError(getString(gasCheckLog.msg));
      focusView = etElecACNo;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etElecAmount.setError(getString(gasCheckLog.msg));
      focusView = etElecAmount;
      cancel = true;
    } else if (Integer.valueOf(etElecAmount.getText().toString()) < 10) {
      etElecAmount.setError(getString(R.string.lessAmount));
      focusView = etElecAmount;
      cancel = true;
    }
  }

  private void checkCityName(String cityName) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cityName);
    if (!gasCheckLog.isValid) {
      etElecCityName.setError(getString(gasCheckLog.msg));
      focusView = etElecCityName;
      cancel = true;
    }
  }

  private void checkName(String cityName, MaterialEditText etElecACNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkAddress(cityName);
    if (!gasCheckLog.isValid) {
      etElecACNo.setError(getString(gasCheckLog.msg));
      focusView = etElecACNo;
      cancel = true;
    }
  }

  private void checkCycle(String cycle) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cycle);
    if (!gasCheckLog.isValid) {
      etElecCycleNo.setError(getString(gasCheckLog.msg));
      focusView = etElecCycleNo;
      cancel = true;
    }
  }

  private void checkUpBalance() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("accountNumber", customerNo);
      jsonRequest.put("amount", amount);
      jsonRequest.put("sessionId", session.getUserSessionId());
      if (etElecCycleNo.getVisibility() == View.VISIBLE) {
        jsonRequest.put("cycleNumber", cycleNo);
      } else {
        jsonRequest.put("cycleNumber", "");
      }
      if (etElecCityName.getVisibility() == View.VISIBLE) {
        if (serviceProvider.equalsIgnoreCase("VMDE")) {
          jsonRequest.put("billingUnit", etElecCityName.getText().toString());
        } else {
          jsonRequest.put("cityName", cycleNo);
        }
      } else {
        jsonRequest.put("cityName", "");
      }

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());

      AndroidNetworking.post(ApiUrl.URL_CHECK_BALANCE_FOR_SPLIT_BILL)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              promotePayment();
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              JSONObject dtoObject = response.getJSONObject("dto");
              String splitAmount = dtoObject.getString("splitAmount");
              String message = response.getString("message");
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                mainMenuDetailActivity.putExtra("SUBTYPE", "ELECTRIC");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_ELECTRICITY_PAYMENT);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }


            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(response.getString("message"));
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }


            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
    }
  }

  private void promotePayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("accountNumber", customerNo);
      jsonRequest.put("amount", amount);
      jsonRequest.put("sessionId", session.getUserSessionId());
      if (etElecCycleNo.getVisibility() == View.VISIBLE) {
        jsonRequest.put("cycleNumber", cycleNo);
      } else {
        jsonRequest.put("cycleNumber", "");
      }
      if (etElecCityName.getVisibility() == View.VISIBLE) {
        if (serviceProvider.equalsIgnoreCase("VMDE")) {
          jsonRequest.put("billingUnit", etElecCityName.getText().toString());
        } else {
          jsonRequest.put("cityName", cycleNo);
        }
      } else {
        jsonRequest.put("cityName", "");
      }

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_ELECTRICITY_PAYMENT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String sucessMessage = jsonObject.getString("balance");

              session.setUserBalance(sucessMessage);
              session.save();
              loadDlg.dismiss();
              showSuccessDialog();
              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }

            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              String splitAmount = String.valueOf(Double.parseDouble(etElecAmount.getText().toString()) - Double.parseDouble(session.getUserBalance()));
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (Double.parseDouble(splitAmount) >= Double.parseDouble("10")) {
                if (!splitAmount.isEmpty()) {
                  mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE", "ELECTRIC");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_ELECTRICITY_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              } else {
                if (!splitAmount.isEmpty()) {
                  splitAmount = "10";
                  mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE", "ELECTRIC");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_ELECTRICITY_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              }


            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(response.getString("message"));
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }


            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

        }
      });
    }
  }

  public void showCustomDialog() {
    payAmount = Integer.valueOf(additionalCharges) + Integer.valueOf(amount) + "";
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          promotePayment();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public String generateMessage() {
    String source = "";
    if (cycleNo != null && !cycleNo.isEmpty()) {
      source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> Account No: </font></b>" + "<font>" + customerNo + "</font><br>" +
        "<b><font color=#000000> Cycle No: </font></b>" + "<font>" + cycleNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + amount + "</font><br>" +

        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    } else {
      source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> Account No: </font></b>" + "<font>" + customerNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br>" +

        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    }
    return source;
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void showExactAmountDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, getResources().getString(R.string.exactAmount));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        isValidDueAmount = true;
        etElecAmount.setFocusable(true);
        etElecAmount.setFocusableInTouchMode(true);
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }


  @Override
  public void verifiedCompleted() {
    promotePayment();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog(message);
  }

  public void getDeuAmount() {
    loadDlg.show();
    Map<String, String> params = new HashMap<>();
    params.put("serviceProvider", serviceProvider);
    params.put("customerID", customerNo);
    if (etElecCycleNo.getVisibility() == View.VISIBLE) {
      params.put("cycleNumber", cycleNo);
    }
    if (etElecCityName.getVisibility() == View.VISIBLE) {
      if (serviceProvider.equalsIgnoreCase("VMDE")) {
        params.put("billingUnit", etElecCityName.getText().toString());
      } else {
        params.put("cityName", cycleNo);
      }

    }
    AndroidNetworking.post(ApiUrl.URL_CHECK_DEU)
      .addBodyParameter(params) // posting json
      .setTag("test")
      .setPriority(Priority.HIGH)
      .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
      .build().setAnalyticsListener(new AnalyticsListener() {
      @Override
      public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
        Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
      }
    })
      .getAsString(new StringRequestListener() {
        @Override
        public void onResponse(String response) {
          Log.i("DEU Response", response.toString());
          if (checkIfDouble(response)) {
            String[] refactorNo = response.split("\\.");
            etElecAmount.setText(refactorNo[0]);
            CustomToast.showMessage(getActivity(), "Your Due amount is INR. " + refactorNo[0] + " ");

          } else {
            try {
              JSONObject resObj = new JSONObject(response);
              if (resObj.has("particulars") && resObj.getJSONObject("particulars").getString("dueamount") != null && !resObj.getJSONObject("particulars").getString("dueamount").equalsIgnoreCase("")) {
                final String dueamount = resObj.getJSONObject("particulars").getString("dueamount");
                String duedate = resObj.getJSONObject("particulars").getString("duedate");
                String customername = resObj.getJSONObject("particulars").getString("customername");
                String billnumber = resObj.getJSONObject("particulars").getString("billnumber");
                String billdate = resObj.getJSONObject("particulars").getString("billdate");
                String billperiod = resObj.getJSONObject("particulars").getString("billperiod");
                CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(getDueAmountHinit(dueamount, duedate, customername, billnumber, billdate, billperiod)));
                customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    etElecAmount.setText(dueamount);
                  }
                });
                customAlertDialog.show();
              } else if (resObj.has("ipay_errordesc") && resObj.getString("ipay_errordesc") != null) {
                String ipay_errordesc = resObj.getString("ipay_errordesc");
//                CustomToast.showMessage(getActivity(), ipay_errordesc);
                CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, ipay_errordesc);
                customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                  }
                });
                customAlertDialog.show();
              }
            } catch (JSONException e) {
              e.printStackTrace();
              CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            }
          }

          loadDlg.dismiss();
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
  }

  private boolean checkIfDouble(String value) {
    String decimalPattern = "([0-9]*)\\.([0-9]*)";
    return Pattern.matches(decimalPattern, value);
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
        getActivity().finish();
      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source = "";

    if (cycleNo != null && !cycleNo.isEmpty()) {
      source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> Account No: </font></b>" + "<font>" + customerNo + "</font><br>" +
        "<b><font color=#000000> Cycle No: </font></b>" + "<font>" + cycleNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + amount + "</font><br>";
    } else {
      source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> Account No: </font></b>" + "<font>" + customerNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br>";
    }
    return source;
  }

  public String getDueAmountHinit(String amount, String duedate, String name, String billnumber, String billdate, String period) {
    return "<b><font color=#000000> Due Amount: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
      "<b><font color=#000000> Due Date: </font></b>" + "<font>" + duedate + "</font><br>" +
      "<b><font color=#000000> Customer Name: </font></b>" + "<font>" + name + "</font><br>" +
      "<b><font color=#000000> Bill Number: </font></b>" + "<font>" + billnumber + "</font><br>" +
      "<b><font color=#000000> Bill Date: </font></b>" + "<font>" + billdate + "</font><br>" +
      "<b><font color=#000000> Bill Period: </font></b>" + "<font>" + period + "</font><br>";
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }


  public void loadElectricityOperators() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("operatorName", "billpayment");
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      loadDlg.show();
      AndroidNetworking.post(ApiUrl.URL_ELECTRICTY_OPERATOR)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject JsonObj) {
          Log.i("LOAD CIRCLE OP ", JsonObj.toString());
          loadDlg.dismiss();
          try {
            String code = JsonObj.getString("code");
            BillPaymentOperatorsModel.deleteAll(BillPaymentOperatorsModel.class);
            //New Implementation
            operatorList.clear();
            if (code != null && code.equalsIgnoreCase("S00")) {

              JSONArray operatorArray = new JSONArray(JsonObj.getString("detail"));

              for (int i = 0; i < operatorArray.length(); i++) {
                JSONObject c = operatorArray.getJSONObject(i);
                String op_code = c.getString("operatorCode");
                String op_name = c.getString("operatorName");
                String accountNumberType = c.getString("accountNumberType");
                String images = "dfds";
                if (c.has("images")) {
                  images = c.getString("images");
                }
                BillPaymentOperatorsModel oModel = new BillPaymentOperatorsModel(op_code, op_name, accountNumberType, images);
                oModel.save();
                operatorList.add(oModel);
              }

              if (operatorList != null && operatorList.size() != 0) {
                try {
                  Collections.sort(operatorList, new Comparator<BillPaymentOperatorsModel>() {
                    @Override
                    public int compare(BillPaymentOperatorsModel lhs, BillPaymentOperatorsModel rhs) {
                      return lhs.getName().compareTo(rhs.getName());
                    }
                  });
//                  operatorList.add(0, new BillPaymentOperatorsModel("", "Select your operator", ""));
//                  spinnerAdapterOperator = new OperatorSpinnerBillpaymentAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
//                  electricity_operator.setAdapter(spinnerAdapterOperator);
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }

              loadDlg.dismiss();

            } else if (code != null && code.equalsIgnoreCase("F03")) {
              loadDlg.dismiss();
              sessionInvalid(JsonObj.getString("message"));
            } else {
              String message = JsonObj.getString("message");
              CustomToast.showMessage(getActivity(), message);
            }
          } catch (JSONException e) {
            e.printStackTrace();
            loadDlg.dismiss();

            Toast.makeText(getActivity(), getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();

          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
    }

  }

//  public static class AlphaNumericInputFilter implements InputFilter {
//    public CharSequence filter(CharSequence source, int start, int end,
//                               Spanned dest, int dstart, int dend) {
//
//      // Only keep characters that are alphanumeric
//      StringBuilder builder = new StringBuilder();
//      for (int i = start; i < end; i++) {
//        char c = source.charAt(i);
//        if (Character.isLetterOrDigit(c)) {
//          builder.append(c);
//        }
//      }
//
//      // If all characters are valid, return null, otherwise only return the filtered characters
//      boolean allCharactersValid = (builder.length() == end - start);
//      return allCharactersValid ? null : builder.toString();
//    }
//  }

  public static class NumericInputFilter implements InputFilter {
    BillPaymentOperatorsModel model;

    public NumericInputFilter(BillPaymentOperatorsModel operatorsModel) {
      this.model = operatorsModel;
    }

    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {

      // Only keep characters that are alphanumeric
      StringBuilder builder = new StringBuilder();
      for (int i = start; i < end; i++) {
        char c = source.charAt(i);
        if (model.getDefaultCode().equalsIgnoreCase("numeric")) {
          if (Character.isDigit(c)) {
            builder.append(c);
          }
        } else if (model.getDefaultCode().equalsIgnoreCase("alphanumeric")) {
          if (Character.isLetterOrDigit(c)) {
            builder.append(c);
          }
        }
      }

      // If all characters are valid, return null, otherwise only return the filtered characters
      boolean allCharactersValid = (builder.length() == end - start);
      return allCharactersValid ? null : builder.toString();
    }
  }
}
