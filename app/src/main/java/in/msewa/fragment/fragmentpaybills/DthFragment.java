package in.msewa.fragment.fragmentpaybills;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import in.msewa.adapter.OperatorSpinnerAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.metadata.MenuMetadata;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.PayingDetailsValidation;


/**
 * Created by Ksf on 3/16/2016.
 */
public class DthFragment extends Fragment implements MPinVerifiedListner {
  private View rootView;
  private Spinner spinner_dth_provider;
  private OperatorSpinnerAdapter operatorSpinnerAdapter;
  private Button btn_pay_dth;
  private MaterialEditText edt_dth_amount;
  private MaterialEditText edt_dth_number;

  private View focusView = null;
  private boolean cancel;
  private JSONObject jsonRequest;
  //User ValuesTo1

  private String amount, serviceProvider, customerNo, serviceProviderName;

  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();
  //Volley Tag
  private String tag_json_obj = "json_bill_pay";
  private MyReceiver r;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_dth, container, false);
    loadDlg = new LoadingDialog(getActivity());
    spinner_dth_provider = (Spinner) rootView.findViewById(R.id.spinner_dth_provider);
    btn_pay_dth = (Button) rootView.findViewById(R.id.btn_pay_dth);
    edt_dth_amount = (MaterialEditText) rootView.findViewById(R.id.edt_dth_amount);
    edt_dth_number = (MaterialEditText) rootView.findViewById(R.id.edt_dth_number);
    operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, MenuMetadata.getDTH(), "dth");
    spinner_dth_provider.setAdapter(operatorSpinnerAdapter);

    //DONE CLICK ON VIRTUAL KEYPAD
    edt_dth_amount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });

    btn_pay_dth.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });


    return rootView;
  }

  private void attemptPayment() {
    edt_dth_amount.setError(null);
    edt_dth_number.setError(null);
    cancel = false;

    amount = edt_dth_amount.getText().toString();
    customerNo = edt_dth_number.getText().toString();
    serviceProvider = ((OperatorsModel) spinner_dth_provider.getSelectedItem()).code;
    serviceProviderName = ((OperatorsModel) spinner_dth_provider.getSelectedItem()).name;

    checkPayAmount(amount);
    checkCustomerNo(customerNo);

    if (spinner_dth_provider.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();
    }
  }


  private void checkCustomerNo(String acNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(acNo);
    if (!gasCheckLog.isValid) {
      edt_dth_number.setError(getString(gasCheckLog.msg));
      focusView = edt_dth_number;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      edt_dth_amount.setError(getString(gasCheckLog.msg));
      focusView = edt_dth_amount;
      cancel = true;
    } else if (!TextUtils.isEmpty(amount)) {
      if (Integer.valueOf(amount) < 10) {
        edt_dth_amount.setError(getString(R.string.lessAmount));
        focusView = edt_dth_amount;
        cancel = true;
      }
    }
  }

  private void checkUpBalance() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("dthNo", edt_dth_number.getText().toString());
      jsonRequest.put("amount", amount);
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_CHECK_BALANCE_FOR_SPLIT_BILL)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals(ApiUrl.C_SUCCESS)) {
              promotePayment();

            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              JSONObject dtoObject = response.getJSONObject("dto");
              String splitAmount = dtoObject.getString("splitAmount");
              String message = response.getString("message");
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                mainMenuDetailActivity.putExtra("SUBTYPE", "DTH");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_DTH_PAYMENT);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }


            } else if (code != null && code.equals(ApiUrl.C_SESSION_EXPIRE)) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }

            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
    }
  }

  private void promotePayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("dthNo", edt_dth_number.getText().toString());
      jsonRequest.put("amount", amount);
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_DTH_PAYMENT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");
            loadDlg.dismiss();
            if (code != null && code.equals(ApiUrl.C_SUCCESS)) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String sucessMessage = jsonObject.getString("balance");
              try {
                session.setUserBalance(sucessMessage);
                session.save();
              } catch (IllegalStateException e) {
              }
              loadDlg.dismiss();
              showSuccessDialog();
              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }

            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              String splitAmount = String.valueOf(Double.parseDouble(amount) - Double.parseDouble(session.getUserBalance()));
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (Double.parseDouble(splitAmount) >= Double.parseDouble("10")) {
                if (!splitAmount.isEmpty()) {
                  mainMenuDetailActivity.putExtra("TYPE","BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE","DTH");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL",ApiUrl.URL_DTH_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT",jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              } else {
                if (!splitAmount.isEmpty()) {
                  splitAmount = "10";
                  mainMenuDetailActivity.putExtra("TYPE","BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE","DTH");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL",ApiUrl.URL_DTH_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT",jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              }


            } else if (code != null && code.equals(ApiUrl.C_SESSION_EXPIRE)) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }

            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
    }
  }


  public void showCustomDialog() {

    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          promotePayment();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }

  public String generateMessage() {
    return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> DTH No: </font></b>" + "<font>" + customerNo + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }


  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();

      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source =
      "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> DTH No: </font></b>" + "<font>" + customerNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
    return source;
  }


  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }


  @Override
  public void verifiedCompleted() {
    promotePayment();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog();
  }


  public void refresh() {
    //yout code in refresh.
    Log.i("Refresh", "YES");
    edt_dth_amount.setText("");
    edt_dth_number.setText("");
    edt_dth_amount.setError(null);
    edt_dth_number.setError(null);
    operatorSpinnerAdapter.notifyDataSetChanged();

    spinner_dth_provider.setAdapter(operatorSpinnerAdapter);
  }

  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
  }

  public void onResume() {
    super.onResume();
    r = new MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r,
      new IntentFilter("TAG_REFRESH"));
  }

  private class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      DthFragment.this.refresh();
    }
  }


}
