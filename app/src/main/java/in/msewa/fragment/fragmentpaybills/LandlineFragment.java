package in.msewa.fragment.fragmentpaybills;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import in.msewa.adapter.OperatorSpinnerAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.metadata.MenuMetadata;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.GenerateAPIHeader;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.PayingDetailsValidation;


/**
 * Created by Ksf on 3/16/2016.
 */
public class LandlineFragment extends Fragment implements MPinVerifiedListner {
  private View rootView;
  private Spinner spinner_land_line_operator;
  private MaterialEditText etLandLineStd;
  private MaterialEditText etLandLineNo;
  private MaterialEditText etLandLineAcNo;
  private MaterialEditText etLandLineAmount;
  private Button btnLandLinePay;
  private ImageButton btnDeuLandLine;
  private OperatorSpinnerAdapter operatorSpinnerAdapter;

  private View focusView = null;
  private boolean cancel;

  //User Values

  private String amount, serviceProvider, phoneNo, stdCode, serviceProviderName;
  private String acNo = "";

  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();

  private JSONObject jsonRequest;

  //Volley Tag
  private String tag_json_obj = "json_bill_pay";
  private MyReceiver r;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
  }


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_land_line, container, false);
    spinner_land_line_operator = (Spinner) rootView.findViewById(R.id.spinner_land_line_operator);

    etLandLineStd = (MaterialEditText) rootView.findViewById(R.id.etLandLineStd);
    etLandLineAcNo = (MaterialEditText) rootView.findViewById(R.id.etLandLineAcNo);
    etLandLineNo = (MaterialEditText) rootView.findViewById(R.id.etLandLineNo);
    etLandLineAmount = (MaterialEditText) rootView.findViewById(R.id.etLandLineAmount);

    btnLandLinePay = (Button) rootView.findViewById(R.id.btnLandLinePay);
    btnDeuLandLine = (ImageButton) rootView.findViewById(R.id.btnDeuLandLine);

    operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, MenuMetadata.getLandLine(), "landline");
    spinner_land_line_operator.setAdapter(operatorSpinnerAdapter);

    spinner_land_line_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i != 0) {
          showExactAmountDialog();

          serviceProvider = ((OperatorsModel) spinner_land_line_operator.getSelectedItem()).code;
          serviceProviderName = ((OperatorsModel) spinner_land_line_operator.getSelectedItem()).name;

          Log.i("Provider", serviceProvider);
          Log.i("Name", serviceProviderName);


          if (serviceProvider.equals("VBGL")) {
            etLandLineAcNo.setVisibility(View.VISIBLE);
            btnDeuLandLine.setVisibility(View.VISIBLE);
          } else {
            btnDeuLandLine.setVisibility(View.GONE);
            etLandLineAcNo.setVisibility(View.GONE);
            acNo = "";
            etLandLineAcNo.setText("");
          }
        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });

    //DONE CLICK ON VIRTUAL KEYPAD
    etLandLineAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });

    btnLandLinePay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });

    btnDeuLandLine.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptDeuCheck();
      }
    });


    return rootView;
  }

  private void attemptDeuCheck() {
    etLandLineAcNo.setError(null);
    etLandLineNo.setError(null);
    etLandLineStd.setError(null);
    cancel = false;

    acNo = etLandLineAcNo.getText().toString();
    phoneNo = etLandLineNo.getText().toString();
    stdCode = etLandLineStd.getText().toString();

    checkCustomerNo(phoneNo);
    checkStd(stdCode);

    if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
      checkAcc(acNo);
    }

    if (spinner_land_line_operator.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    }


    if (cancel) {
      focusView.requestFocus();
    } else {
      getDeuAmount();
    }
  }


  private void attemptPayment() {
    etLandLineAcNo.setError(null);
    etLandLineAmount.setError(null);
    etLandLineNo.setError(null);
    etLandLineStd.setError(null);
    cancel = false;

    amount = etLandLineAmount.getText().toString();
    acNo = etLandLineAcNo.getText().toString();
    phoneNo = etLandLineNo.getText().toString();
    stdCode = etLandLineStd.getText().toString();

    checkPayAmount(amount);
    checkCustomerNo(phoneNo);
    checkStd(stdCode);

    if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
      checkAcc(acNo);
    }

    if (spinner_land_line_operator.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    }


    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();
    }
  }


  private void checkCustomerNo(String acNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(acNo);
    if (!gasCheckLog.isValid) {
      etLandLineNo.setError(getString(gasCheckLog.msg));
      focusView = etLandLineAcNo;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etLandLineAmount.setError(getString(gasCheckLog.msg));
      focusView = etLandLineAmount;
      cancel = true;
    } else if (Integer.valueOf(etLandLineAmount.getText().toString()) < 10) {
      etLandLineAmount.setError(getString(R.string.lessAmount));
      focusView = etLandLineAmount;
      cancel = true;
    }
  }


  private void checkStd(String cycle) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cycle);
    if (!gasCheckLog.isValid) {
      etLandLineStd.setError(getString(gasCheckLog.msg));
      focusView = etLandLineStd;
      cancel = true;
    }
  }

  private void checkAcc(String cycle) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cycle);
    if (!gasCheckLog.isValid) {
      etLandLineAcNo.setError(getString(gasCheckLog.msg));
      focusView = etLandLineAcNo;
      cancel = true;
    }
  }


  private void checkUpBalance() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("stdCode", stdCode);
      jsonRequest.put("landlineNumber", phoneNo);
      jsonRequest.put("sessionId", session.getUserSessionId());
      if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
        jsonRequest.put("accountNumber", acNo);
      } else {
        jsonRequest.put("accountNumber", "");
      }

      jsonRequest.put("amount", amount);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CHECK_BALANCE_FOR_SPLIT_BILL, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              promotePayment();
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              JSONObject dtoObject = response.getJSONObject("dto");
              String splitAmount = dtoObject.getString("splitAmount");
              String message = response.getString("message");
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                mainMenuDetailActivity.putExtra("SUBTYPE", "LANDLINE");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_LANDLINE_PAYMENT);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }
            }

          } catch (JSONException e) {
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            loadDlg.dismiss();
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_LANDLINE + session.getUserSessionId()));
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void promotePayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("stdCode", stdCode);
      jsonRequest.put("landlineNumber", phoneNo);
      jsonRequest.put("sessionId", session.getUserSessionId());
      if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
        jsonRequest.put("accountNumber", acNo);
      } else {
        jsonRequest.put("accountNumber", "");
      }

      jsonRequest.put("amount", amount);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LANDLINE_PAYMENT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");


            if (code != null && code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String sucessMessage = jsonObject.getString("balance");

              session.setUserBalance(sucessMessage);
              session.save();
              loadDlg.dismiss();

              showSuccessDialog();
              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }

            }else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              String splitAmount = String.valueOf(Double.parseDouble(etLandLineAmount.getText().toString()) - Double.parseDouble(session.getUserBalance()));
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (Double.parseDouble(splitAmount) >= Double.parseDouble("10")) {
                if (!splitAmount.isEmpty()) {
                  mainMenuDetailActivity.putExtra("TYPE","BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE","LANDLINE");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL",ApiUrl.URL_LANDLINE_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT",jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              } else {
                if (!splitAmount.isEmpty()) {
                  splitAmount = "10";
                  mainMenuDetailActivity.putExtra("TYPE","BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE","LANDLINE");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL",ApiUrl.URL_LANDLINE_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT",jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              }


            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }
            }

          } catch (JSONException e) {
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            loadDlg.dismiss();
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));


        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_LANDLINE + session.getUserSessionId()));
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  public void showCustomDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          promotePayment();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public String generateMessage() {
    if (acNo != null && !acNo.isEmpty()) {
      return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> STD Code: </font></b>" + "<font>" + stdCode + "</font><br>" +
        "<b><font color=#000000> Phone No: </font></b>" + "<font>" + phoneNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br>" +
        "<b><font color=#000000> Ac No: </font></b>" + "<font>" + acNo + "</font><br><br>" +
        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    } else {
      return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> STD Code: </font></b>" + "<font>" + stdCode + "</font><br>" +
        "<b><font color=#000000> Phone No: </font></b>" + "<font>" + phoneNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br>" +
        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    }

  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void showExactAmountDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, getActivity().getResources().getString(R.string.exactAmount));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }


  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }

  @Override
  public void verifiedCompleted() {
    promotePayment();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog();
  }

  public void getDeuAmount() {
    StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_CHECK_DEU, new Response.Listener<String>() {
      @Override
      public void onResponse(String response) {
        Log.i("DEU Response", response.toString());
        if (checkIfDouble(response)) {
          String[] refactorNo = response.split("\\.");
          etLandLineAmount.setText(refactorNo[0]);
          CustomToast.showMessage(getActivity(), "Your Due amount is INR. " + refactorNo[0] + " ");
        } else {
          try {
            JSONObject resObj = new JSONObject(response);
            if (resObj.has("ipay_errordesc")) {
              String ipay_errordesc = resObj.getString("ipay_errordesc");
              CustomToast.showMessage(getActivity(), ipay_errordesc);
            }
          } catch (JSONException e) {
            e.printStackTrace();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

          }
        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
      }
    }) {
      @Override
      protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("serviceProvider", serviceProvider);
        params.put("stdCode", stdCode);
        params.put("landlineNumber", phoneNo);
        if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
          params.put("accountNumber", acNo);
        }
        Log.i("HEADER", params.toString());
        return params;
      }

      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("hash", "123");
        return map;
      }
    };

    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
  }

  private boolean checkIfDouble(String value) {
    String decimalPattern = "([0-9]*)\\.([0-9]*)";
    return Pattern.matches(decimalPattern, value);
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
      }
    });
    builder.show();
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    if (acNo != null && !acNo.isEmpty()) {
      return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> STD Code: </font></b>" + "<font>" + stdCode + "</font><br>" +
        "<b><font color=#000000> Phone No: </font></b>" + "<font>" + phoneNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br>" +
        "<b><font color=#000000> Ac No: </font></b>" + "<font>" + acNo + "</font><br><br>";
    } else {
      return "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> STD Code: </font></b>" + "<font>" + stdCode + "</font><br>" +
        "<b><font color=#000000> Phone No: </font></b>" + "<font>" + phoneNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br>";
    }
  }


  public void refresh() {
    //yout code in refresh.
    Log.i("Refresh", "YES");
    etLandLineAcNo.setText("");
    etLandLineAmount.setText("");
    etLandLineNo.setText("");
    etLandLineStd.setText("");
    etLandLineAcNo.setError(null);
    etLandLineAmount.setError(null);
    etLandLineNo.setError(null);
    etLandLineStd.setError(null);

    operatorSpinnerAdapter.notifyDataSetChanged();

    spinner_land_line_operator.setAdapter(operatorSpinnerAdapter);
  }

  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
  }

  public void onResume() {
    super.onResume();
    r = new MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r,
      new IntentFilter("TAG_REFRESH"));
  }

  private class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      LandlineFragment.this.refresh();
    }
  }

}
