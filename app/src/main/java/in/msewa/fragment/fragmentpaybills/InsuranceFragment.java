package in.msewa.fragment.fragmentpaybills;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.OperatorSpinnerAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.metadata.MenuMetadata;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.GenerateAPIHeader;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;

/**
 * Created by Ksf on 3/16/2016.
 */
public class InsuranceFragment extends Fragment implements MPinVerifiedListner {
  private View rootView;
  private Spinner spinner_insurance_provider;
  private MaterialEditText etPolicyNo;
  private MaterialEditText etPolicyDob;
  private MaterialEditText etPolicyAmount;
  private Button btnPayInsurance;


  private OperatorSpinnerAdapter operatorSpinnerAdapter;

  private View focusView = null;
  private boolean cancel;

  //User Values
  private String amount, serviceProvider, dob, policyNo, serviceProviderName;
  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;

  //Volley Tag
  private String tag_json_obj = "json_bill_pay";
  private MyReceiver r;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_insurance, container, false);
    spinner_insurance_provider = (Spinner) rootView.findViewById(R.id.spinner_insurance_provider);
    etPolicyNo = (MaterialEditText) rootView.findViewById(R.id.etPolicyNo);
    etPolicyDob = (MaterialEditText) rootView.findViewById(R.id.etPolicyDob);
    etPolicyAmount = (MaterialEditText) rootView.findViewById(R.id.etPolicyAmount);
    btnPayInsurance = (Button) rootView.findViewById(R.id.btnPayInsurance);

    etPolicyDob.setFocusable(false);

    operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, MenuMetadata.getInsurance(), "insurance");
    spinner_insurance_provider.setAdapter(operatorSpinnerAdapter);

    etPolicyDob.setOnClickListener(new View.OnClickListener() {
      @SuppressLint("NewApi")
      @Override
      public void onClick(View arg0) {
        String[] birthSplit = etPolicyDob.getText().toString().split("-");
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            etPolicyDob.setText(String.valueOf(year) + "-"
              + String.valueOf(monthOfYear + 1) + "-"
              + String.valueOf(dayOfMonth));
          }
        }, 2015, 12, 12);
        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

        {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
        dialog.show();
      }
    });


    //DONE CLICK ON VIRTUAL KEYPAD
    etPolicyAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });

    btnPayInsurance.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });


    return rootView;
  }

  private void attemptPayment() {
    etPolicyNo.setError(null);
    etPolicyAmount.setError(null);
    etPolicyDob.setError(null);

    cancel = false;

    amount = etPolicyAmount.getText().toString();
    policyNo = etPolicyNo.getText().toString();
    dob = etPolicyDob.getText().toString();
    serviceProvider = ((OperatorsModel) spinner_insurance_provider.getSelectedItem()).code;
    serviceProviderName = ((OperatorsModel) spinner_insurance_provider.getSelectedItem()).name;

    checkPayAmount(amount);
    checkDob(dob);
    checkPolicyNo(policyNo);

    if (spinner_insurance_provider.getSelectedItemPosition() == 0) {
      CustomToast.showMessage(getActivity(), "Select provider to continue");
      return;
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();
    }
  }

  private void checkPolicyNo(String acNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(acNo);
    if (!gasCheckLog.isValid) {
      etPolicyNo.setError(getString(gasCheckLog.msg));
      focusView = etPolicyNo;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etPolicyAmount.setError(getString(gasCheckLog.msg));
      focusView = etPolicyAmount;
      cancel = true;
    } else if (Integer.valueOf(etPolicyAmount.getText().toString()) < 10) {
      etPolicyAmount.setError(getString(R.string.lessAmount));
      focusView = etPolicyAmount;
      cancel = true;
    }
  }


  private void checkDob(String cycle) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cycle);
    if (!gasCheckLog.isValid) {
      etPolicyDob.setError(getString(gasCheckLog.msg));
      focusView = etPolicyDob;
      cancel = true;
    }
  }

  private void checkUpBalance() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("policyNumber", etPolicyNo.getText().toString());
      jsonRequest.put("policyDate", etPolicyAmount.getText().toString());
      jsonRequest.put("amount", etPolicyAmount.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CHECK_BALANCE_FOR_SPLIT_BILL, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              promotePayment();
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              JSONObject dtoObject = response.getJSONObject("dto");
              String splitAmount = dtoObject.getString("splitAmount");
              String message = response.getString("message");
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (!splitAmount.isEmpty()) {
                mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                mainMenuDetailActivity.putExtra("SUBTYPE", "INSURANCE");
                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_INSURANCE_PAYMENT);
                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                startActivity(mainMenuDetailActivity);
              } else {
                CustomToast.showMessage(getActivity(), "split amount can't be empty");
              }
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(response.getString("message"));
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }

            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_NSURANCE + session.getUserSessionId()));
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void promotePayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("serviceProvider", serviceProvider);
      jsonRequest.put("policyNumber", etPolicyNo.getText().toString());
      jsonRequest.put("policyDate", etPolicyAmount.getText().toString());
      jsonRequest.put("amount", etPolicyAmount.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_INSURANCE_PAYMENT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("DTH RESPONSE", response.toString());
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String sucessMessage = jsonObject.getString("balance");

              session.setUserBalance(sucessMessage);
              session.save();
              loadDlg.dismiss();

              showSuccessDialog();
              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }
            } else if (code != null && code.equals("T01")) {
              loadDlg.dismiss();
              String splitAmount = String.valueOf(Double.parseDouble(etPolicyAmount.getText().toString()) - Double.parseDouble(session.getUserBalance()));
              CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
              Intent mainMenuDetailActivity = new Intent(getActivity(), MainMenuDetailActivity.class);
              mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
              mainMenuDetailActivity.putExtra("AutoFill", "yes");
              if (Double.parseDouble(splitAmount) >= Double.parseDouble("10")) {
                if (!splitAmount.isEmpty()) {
                  mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE", "INSURANCE");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_INSURANCE_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              } else {
                if (!splitAmount.isEmpty()) {
                  splitAmount = "10";
                  mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                  mainMenuDetailActivity.putExtra("SUBTYPE", "INSURANCE");
                  mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                  mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_INSURANCE_PAYMENT);
                  mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                  startActivity(mainMenuDetailActivity);
                } else {
                  CustomToast.showMessage(getActivity(), "split amount can't be empty");
                }
              }


            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(response.getString("message"));
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              } else {
                CustomToast.showMessage(getActivity(), "Error message is null");
              }

            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_NSURANCE + session.getUserSessionId()));
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  public void showCustomDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          promotePayment();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public String generateMessage() {
    String source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> Policy No: </font></b>" + "<font>" + policyNo + "</font><br>" +
      "<b><font color=#000000> Date: </font></b>" + "<font>" + dob + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    return source;
  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();

      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
      "<b><font color=#000000> Policy No: </font></b>" + "<font>" + policyNo + "</font><br>" +
      "<b><font color=#000000> Date: </font></b>" + "<font>" + dob + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getActivity().getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
    return source;
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }

  @Override
  public void verifiedCompleted() {
    promotePayment();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog(message);
  }

  public void refresh() {
    //yout code in refresh.
    Log.i("Refresh", "YES");
    etPolicyNo.setError(null);
    etPolicyAmount.setError(null);
    etPolicyDob.setError(null);
    etPolicyNo.setText(null);
    etPolicyAmount.setText(null);
    etPolicyDob.setText(null);
    operatorSpinnerAdapter.notifyDataSetChanged();

    spinner_insurance_provider.setAdapter(operatorSpinnerAdapter);
  }

  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
  }

  public void onResume() {
    super.onResume();
    r = new MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r, new IntentFilter("TAG_REFRESH"));
  }

  private class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      InsuranceFragment.this.refresh();
    }
  }


}
