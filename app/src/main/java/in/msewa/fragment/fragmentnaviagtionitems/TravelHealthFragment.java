package in.msewa.fragment.fragmentnaviagtionitems;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import in.msewa.ecarib.R;

/**
 * Created by Ksf on 3/27/2016.
 */
public class TravelHealthFragment extends Fragment {
  private View rootView;
  private WebView webview;
  private String url = "";
  private ProgressBar pbWebView;
  private LinearLayout llWebError;
  private TextView tvWebError;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    url = getArguments().getString("URL");
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_travel, container, false);
    webview = (WebView) rootView.findViewById(R.id.wvMain);
    pbWebView = (ProgressBar) rootView.findViewById(R.id.pbWebView);
    llWebError = (LinearLayout) rootView.findViewById(R.id.llWebError);
    tvWebError = (TextView) rootView.findViewById(R.id.tvWebError);

    webview.setVisibility(View.GONE);
    webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
    webview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
    webview.getSettings().setDomStorageEnabled(true);


    webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

    webview.setWebChromeClient(new WebChromeClient() {

      public void onProgressChanged(WebView view, int progress) {
        if (progress == 100) {
          webview.setVisibility(View.VISIBLE);
          pbWebView.setVisibility(View.GONE);
        } else {
          webview.setVisibility(View.GONE);
          pbWebView.setVisibility(View.VISIBLE);
        }
      }
    });

    webview.setWebViewClient(new WebViewClient() {

      public void onPageFinished(WebView view, String url) {
        webview.loadUrl("javascript:(function() { " + "var head = document.getElementById('header-one-line').style.display='none'; " + "})()");
        webview.loadUrl("javascript:(function() { " +
          "document.getElementsByClassName('navbar-mobile-wrap')[0].style.display='none'; })()");
        webview.setVisibility(View.VISIBLE);
        pbWebView.setVisibility(View.GONE);
      }

      @Override
      public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                 Handle the error\

        view.loadData("", "text/html", "UTF-8");
        pbWebView.setVisibility(View.GONE);
        llWebError.setVisibility(View.VISIBLE);
        tvWebError.setText(description);
      }

      @TargetApi(android.os.Build.VERSION_CODES.M)
      @Override
      public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
        // Redirect to deprecated method, so you can use it in all SDK versions
        view.loadData("", "text/html", "UTF-8");
        pbWebView.setVisibility(View.GONE);
        llWebError.setVisibility(View.VISIBLE);
        tvWebError.setText(rerr.getDescription());
        onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
      }

      @Override
      public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        super.onReceivedSslError(view, handler, error);
        pbWebView.setVisibility(View.VISIBLE);
        llWebError.setVisibility(View.GONE);
//        SslCertificate sslCertificateServer = error.getCertificate();
//        Certificate pinnedCert = getCertificateForRawResource(R.raw.www_vpayqwik_com, getActivity());
//        Certificate serverCert = convertSSLCertificateToCertificate(sslCertificateServer);
//
//        if (pinnedCert.equals(serverCert)) {
        handler.proceed();
//        } else {
        super.onReceivedSslError(view, handler, error);
//        }
      }
    });
    webview.loadUrl(url);
    return rootView;
  }

  public static Certificate getCertificateForRawResource(int resourceId, Context context) {
    CertificateFactory cf = null;
    Certificate ca = null;
    Resources resources = context.getResources();
    InputStream caInput = resources.openRawResource(resourceId);

    try {
      cf = CertificateFactory.getInstance("X.509");
      ca = cf.generateCertificate(caInput);
    } catch (CertificateException e) {

    } finally {
      try {
        caInput.close();
      } catch (IOException e) {

      }
    }

    return ca;
  }

  public static Certificate convertSSLCertificateToCertificate(SslCertificate sslCertificate) {
    CertificateFactory cf = null;
    Certificate certificate = null;
    Bundle bundle = sslCertificate.saveState(sslCertificate);
    byte[] bytes = bundle.getByteArray("x509-certificate");

    if (bytes != null) {
      try {
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        Certificate cert = certFactory.generateCertificate(new ByteArrayInputStream(bytes));
        certificate = cert;
      } catch (CertificateException e) {

      }
    }

    return certificate;
  }

}
