package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.msewa.custom.CustomToast;
import in.msewa.fragment.fragmentpaybills.DthFragment;
import in.msewa.fragment.fragmentpaybills.ElectricityFragment;
import in.msewa.fragment.fragmentpaybills.GasFragment;
import in.msewa.fragment.fragmentpaybills.InsuranceFragment;
import in.msewa.fragment.fragmentpaybills.LandlineFragment;
import in.msewa.metadata.AppMetadata;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 3/27/2016.
 */
public class BillPaymentFragment extends Fragment {

    CharSequence TitlesEnglish[] = null;

    /*
     * Sliding tabs Setup
	 */
    int NumbOfTabs = 5;
    private View rootView;
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    private String navType = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TitlesEnglish = new CharSequence[]{getResources().getString(R.string.bill_dth), getResources().getString(R.string.bill_landline), getResources().getString(R.string.bill_electricity), getResources().getString(R.string.bill_gas), getResources().getString(R.string.bill_insurance)};
        Bundle navBundle = getArguments();
        if (navBundle != null) {
            navType = navBundle.getString(AppMetadata.FRAGMENT_TYPE);
        }
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_scroll_tab, container, false);
        mainPager = (ViewPager) rootView.findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayoutMobileTopUp);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);
        mSlidingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mainPager.setCurrentItem(tab.getPosition());


//                if (tab.getPosition() == 0) {

                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getActivity());
                Intent i = new Intent("TAG_REFRESH");
                lbm.sendBroadcast(i);

//                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Bill Payment");
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                DthFragment tab1 = new DthFragment();
                return tab1;
            } else if (position == 1) {
                LandlineFragment tab2 = new LandlineFragment();
                return tab2;
            } else if (position == 2) {
                ElectricityFragment tab2 = new ElectricityFragment();
                return tab2;
            } else if (position == 3) {
                GasFragment tab2 = new GasFragment();
                return tab2;
            } else {
                InsuranceFragment tab3 = new InsuranceFragment();
                return tab3;
            }


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomToast.cancelToast();
    }


}
