package in.msewa.fragment.fragmentnaviagtionitems;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.CreateIssuesActivity;


/**
 * Created by acer on 20-06-2017.
 */

public class CustomerSupportFragment extends Fragment {

    private View rootView;
    private CardView cvallissues, cvcreateissues, cvstatus, cvhelpline, cvemail;
    private TextView tvcreateissues, tvhelpline, tvEmails;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_customer_suport, container, false);

        tvcreateissues = (TextView) rootView.findViewById(R.id.tvcreateissues);
        tvhelpline = (TextView) rootView.findViewById(R.id.tvhelpline);
        tvEmails = (TextView) rootView.findViewById(R.id.tvEmails);

        cvallissues = (CardView) rootView.findViewById(R.id.cvallissues);
        cvcreateissues = (CardView) rootView.findViewById(R.id.cvcreateissues);
        //cvstatus = (CardView)rootView.findViewById(R.id.cvstatus);
        cvhelpline = (CardView) rootView.findViewById(R.id.cvhelpline);
        cvemail = (CardView) rootView.findViewById(R.id.cvemail);


        cvcreateissues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CreateIssuesActivity.class));
            }
        });
        cvallissues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), IssuesActivity.class));
            }
        });

        cvhelpline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + "+918025011300"));
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        cvemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "vpayqwik@VIJAYABANK.co.in", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");
                emailIntent.putExtra(Intent.EXTRA_CC, "vpayqwik@VIJAYABANK.co.in");
                emailIntent.putExtra(Intent.EXTRA_CC, "vpayqwikcare@VIJAYABANK.co.in");
                emailIntent.putExtra(Intent.EXTRA_CC, "vpayqwiktravel@VIJAYABANK.co.in");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });


        return rootView;
    }

}
