package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;

public class MerchantPayByQrFragment extends Fragment {

    private View rootView;
    private Button btnQRCodeScan, btnQRPay;
    private TextView tvPleaseScan, tvScanTitle;
    private TableLayout tlQRScannedResult;
    private ImageView ivQRCode;
    private MaterialEditText etQrScanAmount;

    private UserModel session = UserModel.getInstance();
    private View focusView = null;
    private boolean cancel;

    private String merchantId = "";
    private JSONObject jsonRequest;

    private LoadingDialog loadDlg;
    //Volley Tag
    private String tag_json_obj = "json_merchant_pay";
    private MaterialEditText etQrRemarks;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_scan_to_pay, container, false);
        btnQRCodeScan = (Button) rootView.findViewById(R.id.btnQRCodeScan);
        btnQRPay = (Button) rootView.findViewById(R.id.btnQRPay);
        tvScanTitle = (TextView) rootView.findViewById(R.id.tvScanTitle);
        tvScanTitle.setText("FOR MERCHANT PAYMENT\nSCAN QR CODE");
        etQrScanAmount = (MaterialEditText) rootView.findViewById(R.id.etQrScanAmount);
        tvPleaseScan = (TextView) rootView.findViewById(R.id.tvPleaseScan);
        tvPleaseScan.setText("Please scan QR code of Merchant.");
        etQrRemarks = (MaterialEditText) rootView.findViewById(R.id.etQrRemarks);
        tlQRScannedResult = (TableLayout) rootView.findViewById(R.id.tlQRScannedResult);
        ivQRCode = (ImageView) rootView.findViewById(R.id.ivQRCode);

        btnQRCodeScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator.forSupportFragment(MerchantPayByQrFragment.this)
                        .setOrientationLocked(false)
                        .setBeepEnabled(true)
                        .addExtra("PROMPT_MESSAGE", "Scan QR Code")
                        .initiateScan(IntentIntegrator.QR_CODE_TYPES);
            }
        });

        btnQRPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });

        return rootView;
    }

    private void attemptPayment() {
        etQrScanAmount.setError(null);
        cancel = false;
        checkPayAmount(etQrScanAmount.getText().toString());
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteMerchantPay();
        }
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etQrScanAmount.setError(getString(gasCheckLog.msg));
            focusView = etQrScanAmount;
            cancel = true;
        } else if (Integer.valueOf(etQrScanAmount.getText().toString()) < 5) {
            etQrScanAmount.setError(getString(R.string.lessAmount1));
            focusView = etQrScanAmount;
            cancel = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                CustomToast.showMessage(getActivity(), "Cancelled");
            } else {
                try {
                    String[] splitResult = result.getContents().split(":");
                    String encryptValue = splitResult[1].trim();
                    String cardData = splitResult[0].trim();
                    Log.i("encryptValuenew", cardData);
//                    String finalResult = "";

                    if (encryptValue.contains("VPayQwikMerchantQR")) {
                        try {
                            addingResultToTableView(cardData);
                        } catch (Exception e) {
                            e.printStackTrace();
                            CustomToast.showMessage(getActivity(), "Invalid  QR Code");
                        }

                    } else {
                        CustomToast.showMessage(getActivity(), "Invalid  QR Code");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomToast.showMessage(getActivity(), "Invalid VPayqwik QR Code");
                }
            }
        } else {
            CustomToast.showMessage(getActivity(), "Result is null");
        }
    }

    private void addingResultToTableView(String result) {
        tlQRScannedResult.removeAllViews();

        String[] resultArray = result.trim().split("-");
        result = result.replaceAll("\t", "");
        result = result.replaceAll("\\n", "");
        result = result.replaceAll("\r", "");
        result = result.replace("Raw bytes", "");
        Log.i("resultArray5", resultArray[4]);
        Log.i("resultArray3", resultArray[2]);
        Log.i("resultArray4", resultArray[3]);
        Log.i("resultArray1", resultArray[0]);
        Log.i("resultArray2", resultArray[1]);

        ArrayList<String> arrayListHead = new ArrayList<>();
        arrayListHead.add("Receiver Name");
        arrayListHead.add("Receiver Mobile");
        arrayListHead.add("Receiver Email");

        ArrayList<String> arrayListResult = new ArrayList<>();
        merchantId = resultArray[4].trim();


        merchantId = merchantId.replace("Raw bytes", "");


        arrayListResult.add(resultArray[1]);
        arrayListResult.add(resultArray[2]);
        arrayListResult.add(resultArray[3]);

        for (int i = 0; i < arrayListHead.size(); i++) {
            TableRow row = new TableRow(getActivity());

            TextView textH = new TextView(getActivity());
            TextView textC = new TextView(getActivity());
            TextView textV = new TextView(getActivity());

            textH.setText(arrayListHead.get(i));
            textC.setText(":  ");
            textV.setText(arrayListResult.get(i));
            textV.setTypeface(null, Typeface.BOLD);

            row.addView(textH);
            row.addView(textC);
            row.addView(textV);

            tlQRScannedResult.addView(row);
        }
        tlQRScannedResult.setVisibility(View.VISIBLE);
        ivQRCode.setVisibility(View.GONE);
        btnQRCodeScan.setText("Re-Scan");
        tvPleaseScan.setText("Are you sure you want to proceed to pay?");
        tvScanTitle.setText("Scanned successfully");
        etQrScanAmount.setVisibility(View.VISIBLE);
        btnQRPay.setVisibility(View.VISIBLE);
    }


    public void promoteMerchantPay() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("id", merchantId);
            jsonRequest.put("netAmount", etQrScanAmount.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PAY_AT_STORE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Send Money", response.toString());
                    try {
                        if (!response.isNull("code")) {
                            String code = response.getString("code");
                            if (code != null && code.equals("S00")) {

                                loadDlg.dismiss();
                                String jsonString = response.getString("response");
                                JSONObject jsonObject = new JSONObject(jsonString);
                                String successMessage = jsonObject.getString("message");
                                showSuccessDialog("Merchant Receipt", successMessage);

                            } else if (code != null && code.equals("F03")) {
                                loadDlg.dismiss();
                                showInvalidSessionDialog();
                            } else {
                                assert code != null;
                                if (code.equalsIgnoreCase("F06")) {
                                    CustomToast.showMessage(getActivity(), "Invalid QRCode details");
                                } else {
                                    loadDlg.dismiss();
                                    String jsonString = response.getString("response");
                                    JSONObject jsonObject = new JSONObject(jsonString);
                                    String errorMessage = jsonObject.getString("message");
                                    CustomToast.showMessage(getActivity(), errorMessage);
                                }
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showSuccessDialog(String message, String Title) {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), Title, message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
              sendRefresh();
                Intent mainActivityIntent = new Intent(getActivity(), HomeMainActivity.class);
                startActivity(mainActivityIntent);
                getActivity().finish();
            }
        });
        builder.show();
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

//    public String getSuccessMessage() {
//        String source =
//                "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
//                        "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
//                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
//        return source;
//    }

//    public void showSuccessDialog() {
//        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
//            result = Html.fromHtml(getSuccessMessage());
//        }
//        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Recharge Successful", result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
////                getActivity().finish();
//            }
//        });
//        builder.show();
//    }

}

