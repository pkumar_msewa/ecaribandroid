package in.msewa.fragment.fragmentnaviagtionitems;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

//import in.msewa.adapter.ShoppingAdapter;
//import in.msewa.custom.CustomToast;
import javax.net.ssl.SSLSocketFactory;

import in.msewa.model.PQCart;
import in.msewa.model.ShoppingModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.TLSSocketFactory;

/**
 * Created by Ksf on 3/27/2016.
 */
public class ShoppingFragment extends Fragment  {

    private View rootView;
    private GridView gvShopping;
    private TextView tvcart_point;

    private List<ShoppingModel> shoppingArray;
    private RequestQueue rq;

    private String cartValue = "0";

    private PQCart pqCart = PQCart.getInstance();


//    private ShoppingAdapter shoppingAdapter;
    private UserModel session = UserModel.getInstance();

    private Button btnGoCart;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
            rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
//        Select specificProductQueryGt = Select.from(ShoppingModel.class);
//        shoppingArray = specificProductQueryGt.list();
//        cartValue =  String.valueOf(pqCart.getProductsInCartArray().size());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shopping, container, false);
        ImageView ivNoPorducts=(ImageView)rootView.findViewById(R.id.ivNoPorducts);
        Picasso.with(getActivity()).load(R.drawable.travel_banner).placeholder(R.drawable.travel_banner).into(ivNoPorducts);
        return rootView;
    }


    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Shopping");
    }


}
