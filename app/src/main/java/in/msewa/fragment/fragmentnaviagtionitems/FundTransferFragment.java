package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.msewa.custom.CustomToast;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 12/7/2016.
 */
public class FundTransferFragment extends Fragment {
    CharSequence TitlesEnglish[] = null;
    int NumbOfTabs = 2;
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TitlesEnglish = new CharSequence[]{String.valueOf(getResources().getString(R.string.tab_fund_by_no)), String.valueOf(getResources().getString(R.string.tab_fund_by_qr))};
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mobile_topup, container, false);
        mainPager = (ViewPager) rootView.findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayoutMobileTopUp);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);
        mSlidingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mainPager.setCurrentItem(tab.getPosition());

                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getActivity());
                Intent i = new Intent("TAG_REFRESH");
                lbm.sendBroadcast(i);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Mobile TopUp");
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new FundTransferByMobileFragment();
            } else {
                return new FundTransferQrFragment();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomToast.cancelToast();
    }
}

