package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;


/**
 * Created by Ksf on 4/9/2016.
 */
public class DonationByQrFragment extends Fragment {

    private View rootView;
    private Button btnQRCodeScan, btnQRPay;
    private TextView tvPleaseScan, tvScanTitle;
    private TableLayout tlQRScannedResult;
    private ImageView ivQRCode;
    private MaterialEditText etQrScanAmount;
    private String donateNo;

    private UserModel session = UserModel.getInstance();
    private View focusView = null;
    private boolean cancel;

    private String merchantId = "";
    private JSONObject jsonRequest;

    private LoadingDialog loadDlg;
    //Volley Tag
    private String tag_json_obj = "json_merchant_pay";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_scan_to_pay, container, false);
        btnQRCodeScan = (Button) rootView.findViewById(R.id.btnQRCodeScan);
        btnQRPay = (Button) rootView.findViewById(R.id.btnQRPay);
        tvScanTitle = (TextView) rootView.findViewById(R.id.tvScanTitle);
        tvScanTitle.setText("FOR MAKING DONATION \nSCAN QR CODE");
        etQrScanAmount = (MaterialEditText) rootView.findViewById(R.id.etQrScanAmount);
        tvPleaseScan = (TextView) rootView.findViewById(R.id.tvPleaseScan);
        tvPleaseScan.setText("Please scan QR code of temple for making donation.");
        tlQRScannedResult = (TableLayout) rootView.findViewById(R.id.tlQRScannedResult);
        ivQRCode = (ImageView) rootView.findViewById(R.id.ivQRCode);

        btnQRCodeScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator.forSupportFragment(DonationByQrFragment.this)
                        .setOrientationLocked(false)
                        .setBeepEnabled(true)
                        .addExtra("PROMPT_MESSAGE", "Scan QR Code")
                        .initiateScan(IntentIntegrator.QR_CODE_TYPES);
            }
        });

        btnQRPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });

        return rootView;
    }

    private void attemptPayment() {
        etQrScanAmount.setError(null);
        cancel = false;
        checkPayAmount(etQrScanAmount.getText().toString());
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteDonationPay();
        }
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etQrScanAmount.setError(getString(gasCheckLog.msg));
            focusView = etQrScanAmount;
            cancel = true;
        } else if (Integer.valueOf(etQrScanAmount.getText().toString()) < 10) {
            etQrScanAmount.setError(getString(R.string.lessAmount));
            focusView = etQrScanAmount;
            cancel = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                CustomToast.showMessage(getActivity(), "Cancelled");
            } else {
                try {
                    String[] splitResult = result.getContents().split(":");
                    String encryptValue = splitResult[1].trim();
                    String cardData = splitResult[0].trim();
//                    String finalResult = "";

                    if (encryptValue.contains("VPayQwikDonationQR")) {
                        try {
                            addingResultToTableView(cardData);
                        } catch (Exception e) {
                            e.printStackTrace();
                            CustomToast.showMessage(getActivity(), "Invalid QR");
                        }

                    } else {
                        CustomToast.showMessage(getActivity(), "Invalid QR");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomToast.showMessage(getActivity(), "Invalid QR");
                }
            }
        } else {
            CustomToast.showMessage(getActivity(), "Result is null");
        }
    }

    private void addingResultToTableView(String result) {
        tlQRScannedResult.removeAllViews();

        String[] resultArray = result.trim().split("-");
        result = result.replaceAll("\t", "");
        result = result.replaceAll("\\n", "");
        result = result.replaceAll("\r", "");
        result = result.replace("Raw bytes", "");

        ArrayList<String> arrayListHead = new ArrayList<>();
        arrayListHead.add("Receiver Name");
        arrayListHead.add("Receiver Mobile");
        arrayListHead.add("Receiver Email");

        ArrayList<String> arrayListResult = new ArrayList<>();
        merchantId = resultArray[4].trim();


        merchantId = merchantId.replace("Raw bytes", "");


        arrayListResult.add(resultArray[1]);
        arrayListResult.add(resultArray[2]);
        arrayListResult.add(resultArray[3]);

        donateNo = resultArray[2];

        for (int i = 0; i < arrayListHead.size(); i++) {
            TableRow row = new TableRow(getActivity());

            TextView textH = new TextView(getActivity());
            TextView textC = new TextView(getActivity());
            TextView textV = new TextView(getActivity());

            textH.setText(arrayListHead.get(i));
            textC.setText(":  ");
            textV.setText(arrayListResult.get(i));
            textV.setTypeface(null, Typeface.BOLD);

            row.addView(textH);
            row.addView(textC);
            row.addView(textV);

            tlQRScannedResult.addView(row);
        }
        tlQRScannedResult.setVisibility(View.VISIBLE);
        ivQRCode.setVisibility(View.GONE);
        btnQRCodeScan.setText("Re-Scan");
        tvPleaseScan.setText("Are you sure you want to proceed to pay?");
        tvScanTitle.setText("Scanned successfully");
        etQrScanAmount.setVisibility(View.VISIBLE);
        btnQRPay.setVisibility(View.VISIBLE);
    }


    public void promoteDonationPay() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", "D" + donateNo);
            jsonRequest.put("amount", etQrScanAmount.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("message", "Donation done by " + session.getUserFirstName());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_DONATION_TIRUPATI, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Send Money", response.toString());
                    try {
                        Log.i("Merchant Pay Response", response.toString());
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            String jsonString = response.getString("response");
                            JSONObject jsonObject = new JSONObject(jsonString);
                            String successMessage = jsonObject.getString("message");
                            etQrScanAmount.getText().clear();
                            sendRefresh();
                            showSuccessDialog();

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            String errorMessage = response.getString("message");
                            CustomToast.showMessage(getActivity(), errorMessage);
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


    public void showSuccessDialog() {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", "Donation Successful. \nThank you for your kind Donation");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }

}

