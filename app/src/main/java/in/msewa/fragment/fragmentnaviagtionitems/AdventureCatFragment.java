package in.msewa.fragment.fragmentnaviagtionitems;


import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;

import com.android.volley.toolbox.JsonObjectRequest;

import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.adventureActivity.SnowCityList;
//import in.payqwik.payqwik.activity.AdventureActivity.VideoPlayerActivity;
import in.msewa.ecarib.activity.imagicaActivity.ImagicaList;

/**
 * Created by kashifimam on 21/02/17.
 */

public class AdventureCatFragment extends Fragment implements View.OnClickListener {

    private CardView llImagica, llSnowCity;
    private Button btnImagicaExplore, btnSnowCity;
    private ScrollView svAdventure;
    private UserModel session = UserModel.getInstance();

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_adventure_list, container, false);
        llImagica = (CardView) rootView.findViewById(R.id.llImagica);
        btnImagicaExplore = (Button) rootView.findViewById(R.id.btnImagicaExplore);
        llSnowCity = (CardView) rootView.findViewById(R.id.llSnowCity);
        btnSnowCity = (Button) rootView.findViewById(R.id.btnSnowCity);
        svAdventure = (ScrollView) rootView.findViewById(R.id.svAdventure);
        llImagica.setOnClickListener(this);
        btnImagicaExplore.setOnClickListener(this);
        llSnowCity.setOnClickListener(this);
        btnSnowCity.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llImagica:
//                Intent videoIntent = new Intent(getActivity(), VideoPlayerActivity.class);
//                videoIntent.putExtra("URL", "xVNb2ZHXzPw");
//                startActivity(videoIntent);
                Log.i("Video", "Video Playing....");
                break;
            case R.id.btnImagicaExplore:
                Intent imagicaIntent = new Intent(getActivity(), ImagicaList.class);
                startActivity(imagicaIntent);
                break;
            case R.id.llSnowCity:
//                Intent videoIntentSnow = new Intent(getActivity(), VideoPlayerActivity.class);
//                videoIntentSnow.putExtra("URL", "7yi6pVrx_0I");
//                startActivity(videoIntentSnow);
                Log.i("Video", "Video Playing....");
                break;
            case R.id.btnSnowCity:
                Intent snowIntent = new Intent(getActivity(), SnowCityList.class);
                startActivity(snowIntent);
                break;
        }
    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }


    @Override
    public void onDetach() {

        super.onDetach();
    }


}
