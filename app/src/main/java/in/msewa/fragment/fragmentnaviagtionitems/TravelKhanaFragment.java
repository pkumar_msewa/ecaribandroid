package in.msewa.fragment.fragmentnaviagtionitems;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import in.msewa.adapter.HomeSliderAdapter;
import in.msewa.adapter.MenuBookListAdapter;
import in.msewa.custom.CirclePageIndicator;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomAlertDialogSearchT;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BankListModel;
import in.msewa.model.BusCityModel;
import in.msewa.model.BusPassengerModel;
import in.msewa.model.MenuOrderListModel;
import in.msewa.model.TravelCityModel;
import in.msewa.model.UserModel;
import in.msewa.util.CitySelectedListener;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.travelKhanaAvtivity.FoodListActivity;

/**
 * Created by Ksf on 9/26/2016.
 */
public class TravelKhanaFragment extends Fragment implements ViewPager.OnPageChangeListener {
  private View rootView;

  //Views
//    private RadioButton rbBusOneWay, rbBusRoundTrip;
  private MaterialEditText etBusDepartDate;
  //    private MaterialEditText etBusReturnDate;
  private Button btnSearchBus;
//    private RadioGroup rgBusTripType;

  private View focusView = null;
  private boolean cancel;
  private MaterialEditText spBusFrom, spBusTo;


  //Variables
  private String selectedFromCityCode, selectedToCityCode;
  private String selectedFromCityName, selectedToCityName;
  private ArrayList<String> cities = new ArrayList<>();

  //search city
  private JSONObject jsonRequest;
  private LoadingDialog loadingDialog;
  private UserModel session = UserModel.getInstance();
  private ArrayList<TravelCityModel> trainModellist;
  private ArrayList<TravelCityModel> stationModelList;
  private String tag_json_obj = "json_bus_city_search";
  private ArrayAdapter cityAdapter;
  private boolean lock = false;
  //image slider
  private static int currentPage = 0;
  private static int NUM_PAGES = 0;
  private int[] IMAGES;
  public ViewPager mPager;
  public CirclePageIndicator indicator;

  //Previous book tickets
  private RecyclerView rvPreviousTickets;
  private LinearLayout llpbPreviousTickets;
  private ArrayList<BusPassengerModel> busPassengerArrayList;
  private TextView tvHeaderBookedTickets;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_travel_khana, container, false);
    btnSearchBus = (Button) rootView.findViewById(R.id.btnSearchBus);
    etBusDepartDate = (MaterialEditText) rootView.findViewById(R.id.etBusDepartDate);
    spBusFrom = (MaterialEditText) rootView.findViewById(R.id.spBusFrom);
    spBusTo = (MaterialEditText) rootView.findViewById(R.id.spBusTo);
    mPager = (ViewPager) rootView.findViewById(R.id.pagerHome);
    indicator = (CirclePageIndicator) rootView.findViewById(R.id.productHeaderImageSlider);
    loadingDialog = new LoadingDialog(getActivity());
    tvHeaderBookedTickets = (TextView) rootView.findViewById(R.id.tvHeaderBookedTickets);
    rvPreviousTickets = (RecyclerView) rootView.findViewById(R.id.rvPreviousTickets);
    llpbPreviousTickets = (LinearLayout) rootView.findViewById(R.id.llpbPreviousTickets);
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(bMessageReceiver, new IntentFilter("food-book-done"));
    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    String formattedDate = df.format(c.getTime());
    etBusDepartDate.setText(formattedDate);
    stationModelList = new ArrayList<>();
    trainModellist = new ArrayList<>();
    getTrain();
    getImageSlider();
    getBookedOrderList();

//        spBusFrom.setTitle("Select Source City");
    spBusFrom.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final CustomAlertDialogSearchT search = new CustomAlertDialogSearchT(getActivity(), trainModellist, new CitySelectedListener() {
          @Override
          public void citySelected(String type, long cityCode, String cityName) {

          }

          @Override
          public void cityFilter(BusCityModel type) {

          }

          @Override
          public void bankFilter(BankListModel type) {

          }

          @Override
          public void travelFilter(TravelCityModel type) {
            spBusFrom.setText(type.getCityname());
            selectedFromCityCode = type.getCityId();
            selectedFromCityName = type.getCityname();
//                        if(selectedFromCityName)
            getStation(selectedFromCityCode);
            Log.i("LOG2", type.getCityname() + " - " + type.getCityId());
            spBusTo.getText().clear();

          }
        });
        search.show();
      }
    });

    spBusTo.setOnClickListener(new View.OnClickListener()

    {
      @Override
      public void onClick(View v) {
        if (spBusFrom.getText().toString().equals("")) {
          CustomToast.showMessage(getActivity(), "Please first select Train");
        } else {
          CustomAlertDialogSearchT search = new CustomAlertDialogSearchT(getActivity(), stationModelList, new CitySelectedListener() {
            @Override
            public void citySelected(String type, long cityCode, String cityName) {

            }

            @Override
            public void cityFilter(BusCityModel type) {


            }

            @Override
            public void bankFilter(BankListModel type) {

            }

            @Override
            public void travelFilter(TravelCityModel type) {
              spBusTo.setText(type.getCityname());
              selectedToCityCode = type.getCityId();
              selectedToCityName = type.getCityname();
              if (selectedFromCityCode.length() != 0) {
                if (selectedToCityCode == selectedFromCityCode) {
                  spBusTo.setText("");
                  CustomToast.showMessage(getActivity(), "source city could not be same as destination city.");
                }
              }

            }
          });
          search.show();
        }
      }
    });
//
    LocalBroadcastManager.getInstance(

      getActivity()).

      registerReceiver(mMessageReceiver, new IntentFilter("search-complete"));

    etBusDepartDate.setOnClickListener(new View.OnClickListener()

    {
      @Override
      public void onClick(View arg0) {
        if (etBusDepartDate.getText().toString() != null && !etBusDepartDate.getText().toString().isEmpty()) {
          String[] dateSplit = etBusDepartDate.getText().toString().split("-");

          int mYear = Integer.valueOf(dateSplit[2]);
          int mMonth = Integer.valueOf(dateSplit[1]) - 1;
          int mDay = Integer.valueOf(dateSplit[0]);
          final Calendar c = Calendar.getInstance();
          int year = c.get(Calendar.YEAR);
          int month = c.get(Calendar.MONTH);
          int day = c.get(Calendar.DAY_OF_MONTH);
          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0" + dayOfMonth;
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etBusDepartDate.setText(String.valueOf(formattedDay) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(year));
            }
          }, mYear, mMonth, mDay);
          dialog.getDatePicker().setMinDate(System.currentTimeMillis());
          dialog.setCustomTitle(null);
          dialog.setTitle("");
          dialog.show();
        } else {
          final Calendar c = Calendar.getInstance();
          int mYear = c.get(Calendar.YEAR);
          int mMonth = c.get(Calendar.MONTH);
          int mDay = c.get(Calendar.DAY_OF_MONTH);

          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0" + dayOfMonth;
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etBusDepartDate.setText(String.valueOf(formattedDay) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(year));
            }
          }, mYear, mMonth, mDay);

          dialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);
          dialog.setTitle("");
          dialog.setCustomTitle(null);
          dialog.show();
        }

      }
    });

    btnSearchBus.setOnClickListener(new View.OnClickListener()

    {
      @Override
      public void onClick(View view) {
        attemptSearch();
      }
    });
    return rootView;
  }

  private void attemptSearch() {

    etBusDepartDate.setError(null);
    if (!spBusFrom.getText().toString().equals("")) {
      if (!spBusTo.getText().toString().equals("") && !spBusTo.getText().toString().equals("")) {
        if (etBusDepartDate.getText().toString().isEmpty()) {
          etBusDepartDate.setError("Please select departure date");
          focusView = etBusDepartDate;
          focusView.requestFocus();
        } else {
//                    if (lock) {
//                        CustomToast.showMessage(getActivity(), "Please select desitination city other then source city");
//                    } else {
          promoteSearch();
//                        CustomToast.showMessage(getActivity(), "Search Availability");
//                    }
        }
      } else {
        CustomToast.showMessage(getActivity(), "Select Destination City");
      }
    } else {
      CustomToast.showMessage(getActivity(), "Select Source City");
    }
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("searchType");

      if (action.equals("To")) {
        selectedToCityCode = intent.getStringExtra("selectedCityCode");
        selectedToCityName = intent.getStringExtra("selectedCityName");
        if (selectedToCityCode == selectedFromCityCode) {
          CustomToast.showMessage(getActivity(), "Please destination city other than source city.");
        } else {
          spBusTo.setText(selectedToCityName);
        }

      } else {
        selectedFromCityCode = intent.getStringExtra("selectedCityCode");
        selectedFromCityName = intent.getStringExtra("selectedCityName");
        spBusFrom.setText(selectedFromCityName);
      }
    }
  };

  private void getImageSlider() {
    IMAGES = new int[]{R.drawable.bus_img_slider_1, R.drawable.bus_img_slider_2, R.drawable.bus_img_slider_3};
    mPager.setAdapter(new HomeSliderAdapter(getActivity(), IMAGES));
    indicator.setViewPager(mPager);
    final float density = getActivity().getResources().getDisplayMetrics().density;
    indicator.setRadius(5 * density);

    NUM_PAGES = IMAGES.length;

    // Auto start of viewpager
    final Handler handler = new Handler();
    final Runnable Update = new Runnable() {
      public void run() {
        if (currentPage == NUM_PAGES) {
          currentPage = 0;
        }
        mPager.setCurrentItem(currentPage++, true);
      }
    };
    Timer swipeTimer = new Timer();
    swipeTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        handler.post(Update);
      }
    }, 3000, 3000);

    // Pager listener over indicator
    indicator.setOnPageChangeListener(this);
//        {
//
//            @Override
//            public void onPageSelected(int position) {
//                currentPage = position;
//
//            }
//
//            @Override
//            public void onPageScrolled(int pos, float arg1, int arg2) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int pos) {
//
//            }
//        });
//    }
  }

  @Override
  public void onDestroyView() {
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(bMessageReceiver);
    super.onDestroyView();
  }

  private void promoteSearch() {
    Intent getBusIntent = new Intent(getActivity(), FoodListActivity.class);
    String[] splitArray = selectedToCityName.split("/");
    getBusIntent.putExtra("station", splitArray[0]);
    String[] dateFrom = etBusDepartDate.getText().toString().split("-");
    getBusIntent.putExtra("date", dateFrom[2] + "-" + dateFrom[1] + "-" + dateFrom[0]);
    getBusIntent.putExtra("trainNumber", selectedFromCityCode);
    startActivity(getBusIntent);

  }

  private void getTrain() {
    try {
      loadingDialog.show();

      jsonRequest = new JSONObject();
      try {
        jsonRequest.put("sessionId", session.getUserSessionId());

      } catch (JSONException e) {
        e.printStackTrace();
        jsonRequest = null;
      }
      Log.i("TRAINURL", ApiUrl.URL_TRAVELKHANA_GET_CITY);
      Log.i("TRAINREQ", jsonRequest.toString());
      if (jsonRequest != null) {
        Log.i("CityRequest", jsonRequest.toString());
        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_GET_CITY, jsonRequest, new Response.Listener<JSONObject>() {
          @Override
          public void onResponse(JSONObject response) {
            Log.i("TRAINRES", response.toString());
            try {
              loadingDialog.dismiss();
            } catch (NullPointerException e) {

            }
            try {
              if (response.has("code")) {
                String code = response.getString("code");
                if (code != null && code.equals("S00")) {
                  JSONArray citydata = response.getJSONArray("listOfTrain");
                  for (int i = 0; i < citydata.length(); i++) {
                    JSONObject c = citydata.getJSONObject(i);
                    String d = c.getString("trainNumber");
                    String cityName = c.getString("trainName");
                    TravelCityModel cModel = new TravelCityModel(d, cityName);
                    trainModellist.add(cModel);
                  }
                  if (trainModellist.size() != 0) {
//                                        ArrayList<BusCityModel> busCityModels=AppMetadata.getBusCities();
//                                        for (int i = 0; i < trainModellist.size(); i++) {
//                                            for (int j=0;j<busCityModels.size();j++){
//                                                if(trainModellist.get(i).getCityname().equalsIgnoreCase(busCityModels.get(j).getCityname())) {
//                                                    Collections.swap(trainModellist,j,i);
//                                                }
//
//                                            }
//                                        }

//                                        cityAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, cities);
//                                        spBusFrom.setAdapter(cityAdapter);
//                                        spBusTo.setAdapter(cityAdapter);
                  }

                } else if (code != null && code.equals("F03")) {
                  showInvalidSessionDialog();

                } else {
                  if (response.has("message") && response.getString("message") != null) {
                    String message = response.getString("message");
                    CustomToast.showMessage(getActivity(), message);
                  }
                }
              }
            } catch (JSONException e) {
              CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
              e.printStackTrace();
              try {
                loadingDialog.dismiss();
              } catch (NullPointerException e1) {

              }
            }
          }
        }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            try {
              loadingDialog.dismiss();
            } catch (NullPointerException e) {

            }
            CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

          }
        }) {
          @Override
          public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> map = new HashMap<>();
            map.put("hash", "1234");
            return map;
          }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
      }
    } catch (NullPointerException e) {

    }
  }

  public void getStation(String trainNumber) {
    try {
      loadingDialog.show();
      jsonRequest = new JSONObject();
      try {
        jsonRequest.put("sessionId", session.getUserSessionId());
        jsonRequest.put("trainNumber", trainNumber);

      } catch (JSONException e) {
        e.printStackTrace();
        jsonRequest = null;
      }
      Log.i("STATIONURL", ApiUrl.URL_TRAVELKHANA_STATION_LIST);
      Log.i("STATIONREQ", jsonRequest.toString());
      if (jsonRequest != null) {
        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_STATION_LIST, jsonRequest, new Response.Listener<JSONObject>() {
          @Override
          public void onResponse(JSONObject response) {
            Log.i("STATIONS", response.toString());
            loadingDialog.dismiss();
            try {
              if (response.has("code")) {
                String code = response.getString("code");
                if (code != null && code.equals("S00")) {
                  stationModelList.clear();
                  JSONArray citydata = response.getJSONArray("stations");
                  for (int i = 0; i < citydata.length(); i++) {
                    JSONObject c = citydata.getJSONObject(i);
                    String cityName = c.getString("station");
                    TravelCityModel cModel = new TravelCityModel("", cityName);
                    stationModelList.add(cModel);
                  }
                  if (stationModelList.size() != 0) {
//                                        ArrayList<BusCityModel> busCityModels=AppMetadata.getBusCities();
//                                        for (int i = 0; i < trainModellist.size(); i++) {
//                                            for (int j=0;j<busCityModels.size();j++){
//                                                if(trainModellist.get(i).getCityname().equalsIgnoreCase(busCityModels.get(j).getCityname())) {
//                                                    Collections.swap(trainModellist,j,i);
//                                                }
//
//                                            }
//                                        }

//                                        cityAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, cities);
//                                        spBusFrom.setAdapter(cityAdapter);
//                                        spBusTo.setAdapter(cityAdapter);
                  }

                } else if (code != null && code.equals("F03")) {
                  showInvalidSessionDialog();

                } else {
                  if (response.has("message") && response.getString("message") != null) {
                    String message = response.getString("message");
                    CustomToast.showMessage(getActivity(), message);
                  }
                }
              }
            } catch (JSONException e) {
              loadingDialog.dismiss();
              CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }
          }
        }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            try {
              loadingDialog.dismiss();
            } catch (NullPointerException e) {

            }
            CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

          }
        }) {
          @Override
          public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> map = new HashMap<>();
            map.put("hash", "1234");
            return map;
          }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
      }
    } catch (NullPointerException e) {
      e.printStackTrace();
    }
  }

  private BroadcastReceiver bMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      getActivity().finish();
    }
  };

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }

  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {
    currentPage = position;
  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  public void showInvalidSessionDialog() {
    if (isAdded()) {
      CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
      builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          sendLogout();
        }
      });
      builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.dismiss();

        }
      });
      builder.show();
    }
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void getBookedOrderList() {
    llpbPreviousTickets.setVisibility(View.VISIBLE);
    rvPreviousTickets.setVisibility(View.GONE);
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("MYORDERURL", ApiUrl.URL_TRAVELKHANA_MY_ORDERS);
      Log.i("MYORDERREQ", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_MY_ORDERS, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            Log.i("MYORDERRES", jsonObj.toString());
            String code = jsonObj.getString("code");
            String message = jsonObj.getString("message");

            if (code != null && code.equals("S00")) {
              ArrayList<MenuOrderListModel> menuOrderListModelList = new ArrayList<>();
              JSONArray bookedMenuOrderArray = jsonObj.getJSONArray("orderDetails");
              for (int i = 0; i < bookedMenuOrderArray.length(); i++) {
                JSONObject c = bookedMenuOrderArray.getJSONObject(i);
                String orderDate = c.getString("orderDate");
                String txnDate = c.getString("txnDate");
                String train_number = c.getString("train_number");
                String orderStatus = c.getString("orderStatus");
                String userOrderId = c.getString("userOrderId");
                String seat = c.getString("seat");
                String coach = c.getString("coach");
                String contact_no = c.getString("contact_no");
                String eta = c.getString("eta");
                String pnr = c.getString("pnr");
                String name = c.getString("name");
                long totalCustomerPayable = c.getLong("totalCustomerPayable");

                MenuOrderListModel menuOrderListModel = new MenuOrderListModel(orderDate, txnDate, train_number, orderStatus, userOrderId, seat, coach, contact_no, eta, pnr, name, String.valueOf(totalCustomerPayable));
                menuOrderListModelList.add(menuOrderListModel);

              }
              if (menuOrderListModelList != null && menuOrderListModelList.size() != 0) {
//                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
                rvPreviousTickets.addItemDecoration(new SpacesItemDecoration(4));
                GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
                rvPreviousTickets.setLayoutManager(manager);
                rvPreviousTickets.setHasFixedSize(true);

                MenuBookListAdapter itemAdp = new MenuBookListAdapter(getActivity(), menuOrderListModelList);
                rvPreviousTickets.setAdapter(itemAdp);
                tvHeaderBookedTickets.setVisibility(View.VISIBLE);
                llpbPreviousTickets.setVisibility(View.GONE);
                rvPreviousTickets.setVisibility(View.VISIBLE);

              } else {

                llpbPreviousTickets.setVisibility(View.GONE);
              }
            } else if (code != null && code.equals("F03")) {
              llpbPreviousTickets.setVisibility(View.GONE);
              showInvalidSessionDialog();

            } else {
              llpbPreviousTickets.setVisibility(View.GONE);
//                            CustomToast.showMessage(getActivity(), message);

            }

          } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Oops, something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
            llpbPreviousTickets.setVisibility(View.GONE);
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          error.printStackTrace();
          llpbPreviousTickets.setVisibility(View.GONE);
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

}

