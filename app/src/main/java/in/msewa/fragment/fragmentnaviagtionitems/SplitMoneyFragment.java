package in.msewa.fragment.fragmentnaviagtionitems;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.msewa.adapter.SplitMoneyAdapter;
import in.msewa.custom.LoadingDialog;
import in.msewa.fragment.AddGroupsFragmentDialog;
import in.msewa.model.SplitMoneyGroupModel;
import in.msewa.model.SplitMoneyModel;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 6/21/2016.
 */
public class SplitMoneyFragment extends Fragment {
    private View rootView;

    private List<SplitMoneyModel> splitArray;
    private List<SplitMoneyGroupModel> groupArray;
    private HashMap<Integer, List<SplitMoneyGroupModel>> groupHash;

    private SplitMoneyAdapter groupAdapter;
    private ExpandableListView lvGroupList;
    private FloatingActionButton btnAddGroups;
    private TextView tvNoGroups;
    private LoadingDialog loadingDialog;
    private int nextId = 1;
    private SaveSplitTask saveSplitTask;
    private List<SplitMoneyGroupModel> receivedPeopleToSave;

    private int totalAmount, netAmount, currentPersonAmount, extraAmount = 0;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splitArray = Select.from(SplitMoneyModel.class).list();
        groupArray = new ArrayList<>();
        groupHash = new HashMap<>();

        if (splitArray.size() != 0) {
            for (SplitMoneyModel split : splitArray) {
                split.getSplitId();
                Select specificAuthorQueryGt = Select.from(SplitMoneyGroupModel.class).where(Condition.prop("groupid").eq(split.getSplitId()));
                groupArray = specificAuthorQueryGt.list();
                groupHash.put(split.getSplitId(), groupArray);
                nextId = split.getSplitId() + 1;
            }
        }

        loadingDialog = new LoadingDialog(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_split_money, container, false);
        btnAddGroups = (FloatingActionButton) rootView.findViewById(R.id.btnAddGroups);
        tvNoGroups = (TextView) rootView.findViewById(R.id.tvNoGroups);
        lvGroupList = (ExpandableListView) rootView.findViewById(R.id.lvGroupList);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            lvGroupList.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));

        } else {
            lvGroupList.setIndicatorBoundsRelative(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));

        }

        if (splitArray.size() != 0) {
            groupAdapter = new SplitMoneyAdapter(getActivity(), groupHash, splitArray);
            lvGroupList.setAdapter(groupAdapter);
            lvGroupList.setVisibility(View.VISIBLE);
            tvNoGroups.setVisibility(View.GONE);
        } else {
            lvGroupList.setVisibility(View.GONE);
            tvNoGroups.setVisibility(View.VISIBLE);
        }

        btnAddGroups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDialog();

            }
        });

        if (getArguments().get("Type").equals("Add")) {
            String groupName = getArguments().getString("GroupName");
            String groupAmount = getArguments().getString("GroupAmount");
            String groupType = getArguments().getString("GroupType");

            receivedPeopleToSave = getArguments().getParcelableArrayList("PeopleArray");

            saveSplitTask = new SaveSplitTask();
            saveSplitTask.execute(groupName, groupAmount, groupType);
        }


        return rootView;
    }

    private void showAddDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        AddGroupsFragmentDialog addGroupDialog = new AddGroupsFragmentDialog();
        addGroupDialog.show(fm, "fragment_add_group");

    }


    @Override
    public void onResume() {
        super.onResume();

    }

    public int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private class SaveSplitTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            loadingDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            SplitMoneyModel myMoneyModel = new SplitMoneyModel(nextId, params[0], params[2], params[1]);
            myMoneyModel.save();
            splitArray.add(myMoneyModel);
            groupArray.clear();

            totalAmount = Integer.parseInt(params[1]);


            for (int i = 0; i < receivedPeopleToSave.size(); i++) {
                SplitMoneyGroupModel myGroupModel = new SplitMoneyGroupModel(nextId, receivedPeopleToSave.get(i).getNamePerson(), receivedPeopleToSave.get(i).getAmountPerson(), receivedPeopleToSave.get(i).getIdentityPerson());
                myGroupModel.save();
                groupArray.add(myGroupModel);

                currentPersonAmount = Integer.parseInt(receivedPeopleToSave.get(i).getAmountPerson());
                netAmount = netAmount + currentPersonAmount;
            }

            extraAmount = totalAmount - netAmount;
            if (extraAmount > 0) {
                SplitMoneyGroupModel myGroupModel = new SplitMoneyGroupModel(nextId, "Extra Amount", String.valueOf(extraAmount), "null");
                myGroupModel.save();
                groupArray.add(myGroupModel);
            }
            groupHash.put(nextId, groupArray);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadingDialog.dismiss();

            groupAdapter = new SplitMoneyAdapter(getActivity(), groupHash, splitArray);
            lvGroupList.setAdapter(groupAdapter);

            lvGroupList.setVisibility(View.VISIBLE);
            tvNoGroups.setVisibility(View.GONE);

            nextId = nextId + 1;
        }
    }
}
