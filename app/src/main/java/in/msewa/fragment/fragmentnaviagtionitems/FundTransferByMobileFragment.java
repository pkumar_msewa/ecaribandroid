package in.msewa.fragment.fragmentnaviagtionitems;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.EmailCouponsUtil;
import in.msewa.util.GenerateAPIHeader;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 3/27/2016.
 */
public class FundTransferByMobileFragment extends Fragment implements MPinVerifiedListner {

  public static final int PICK_CONTACT = 1;
  private View rootView;
  private Button btnFundTransfer;
  private ImageButton ibFundTransferPhoneBook;
  private MaterialEditText etFundTransferNo;
  private MaterialEditText etFundTransferName, etFundTransferAmount;
  private UserModel session = UserModel.getInstance();

  private View focusView = null;
  private boolean cancel;
  private String transferNo, transferName, transferAmount;
  private LoadingDialog loadDlg;
  private JSONObject jsonRequest;

  //Volley Tag
  private String tag_json_obj = "json_fund_pay";
  private MyReceiver r;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_fund_transfer, container, false);
    etFundTransferAmount = (MaterialEditText) rootView.findViewById(R.id.etFundTransferAmount);
    etFundTransferName = (MaterialEditText) rootView.findViewById(R.id.etFundTransferName);
    etFundTransferNo = (MaterialEditText) rootView.findViewById(R.id.etFundTransferNo);
    btnFundTransfer = (Button) rootView.findViewById(R.id.btnFundTransfer);
    ibFundTransferPhoneBook = (ImageButton) rootView.findViewById(R.id.ibFundTransferPhoneBook);

    ibFundTransferPhoneBook.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
      }
    });


    btnFundTransfer.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });

    //DONE CLICK ON VIRTUAL KEYPAD
    etFundTransferAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });


    return rootView;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
    switch (requestCode) {
      case (PICK_CONTACT):
        if (resultCode == Activity.RESULT_OK) {
          Uri contactUri = data.getData();
          Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
          if (c.moveToFirst()) {
            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String finalNumber = phoneNumber.replaceAll("[^0-9+]", "");
            if (finalNumber != null && !finalNumber.isEmpty()) {
              removeCountryCode(finalNumber);
            }
          }
        }
    }
  }

  private void attemptPayment() {

    etFundTransferNo.setError(null);
    etFundTransferName.setError(null);
    etFundTransferAmount.setError(null);

    cancel = false;

    transferAmount = etFundTransferAmount.getText().toString();
    transferNo = etFundTransferNo.getText().toString();
    transferName = etFundTransferName.getText().toString();

    checkPayAmount(transferAmount);
    checkPhone(transferNo);
    checkName(transferName);

    if (cancel) {
      focusView.requestFocus();
    } else {
//            if (Double.parseDouble(session.getUserBalance()) <= 0) {
//                CustomToast.showMessage(getActivity(), "You don\'t have enough balance");
//            } else if (canSendIt()) {
      showCustomDialog();
//            } else {
//                CustomToast.showMessage(getActivity(), "Your balance is insufficient for transaction.");
//            }
    }
  }

  private void checkPhone(String phNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
    if (!gasCheckLog.isValid) {
      etFundTransferNo.setError(getString(gasCheckLog.msg));
      focusView = etFundTransferNo;
      cancel = true;
    }
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etFundTransferAmount.setError(getString(gasCheckLog.msg));
      focusView = etFundTransferAmount;
      cancel = true;
    } else if (Integer.valueOf(etFundTransferAmount.getText().toString()) < 10) {
      etFundTransferAmount.setError(getString(R.string.lessAmount));
      focusView = etFundTransferAmount;
      cancel = true;
    }
  }

  private void checkName(String name) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
    if (!gasCheckLog.isValid) {
      etFundTransferName.setError(getString(gasCheckLog.msg));
      focusView = etFundTransferName;
      cancel = true;
    }
  }

  private void removeCountryCode(String number) {
    if (hasCountryCode(number)) {
      int country_digits = number.length() - 10;
      number = number.substring(country_digits);
      etFundTransferNo.setText(number);
    } else if (hasZero(number)) {
      if (number.length() >= 10) {
        int country_digits = number.length() - 10;
        number = number.substring(country_digits);
        etFundTransferNo.setText(number);
      } else {
        CustomToast.showMessage(getActivity(), "Please select 10 digit no");
      }

    } else {
      etFundTransferNo.setText(number);
    }

  }

  private boolean hasZero(String number) {
    return number.charAt(0) == '0';
  }

  private boolean hasCountryCode(String number) {
    return number.charAt(0) == '+';
  }


  public void promoteSendMoney() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", etFundTransferNo.getText().toString());
      jsonRequest.put("amount", etFundTransferAmount.getText().toString());
      jsonRequest.put("message", etFundTransferName.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FUND_TRANSFER, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          Log.i("Send Money", response.toString());
          try {
            String code = response.getString("code");

//            String jsonString = response.getString("response");
//            JSONObject jsonObject = new JSONObject(jsonString);
            String sucessMessage = response.getString("message");
            if (code != null && code.equals("S00")) {

              etFundTransferNo.getText().clear();
              etFundTransferAmount.getText().clear();
              etFundTransferName.getText().clear();

              loadDlg.dismiss();


              try {
                EmailCouponsUtil.emailForCoupons(tag_json_obj);
              } catch (Exception e) {
                e.printStackTrace();
              }
              showSuccessDialog();
//                            CustomToast.showMessage(getActivity(), sucessMessage);
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(sucessMessage);
            } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
              loadDlg.dismiss();
              showBlockDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, message);
                customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                  }
                });
                customAlertDialog.show();
              }
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();

          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_SENDMONEY + session.getUserSessionId()));
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }

  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  @Override
  public void onResume() {
    super.onResume();
    r = new MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r,
      new IntentFilter("TAG_REFRESH"));
    PayQwikApplication.getInstance().trackScreenView("Fund transfer");
  }

  public void showCustomDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(transferAmount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          loadDlg.show();
          promoteSendMoney();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }

  public String generateMessage() {
    String source = "<b><font color=#000000> Receiver Message: </font></b>" + "<font color=#000000>" + transferName + "</font><br>" +
      "<b><font color=#000000> Receiver No: </font></b>" + "<font>" + transferNo + "</font><br>" +
      "<b><font color=#000000> Current Balance: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + session.getUserBalance() + "</font><br><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + transferAmount + "</font><br><br>" +
//                "<b><font color=#000000> below 25000: </font></b>" + "<font>" +"plus"+ getResources().getString(R.string.rupease) + " " + transferAmount + "</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    return source;
//        "<b><font color=#000000> Convenience Charge: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + getConvenienceCharge() + "</font><br><br>" +
  }


  private void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Transferred Successfully", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
//                getActivity().finish();
      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source = "<b><font color=#000000> Receiver Message: </font></b>" + "<font color=#000000>" + transferName + "</font><br>" +
      "<b><font color=#000000> Receiver No: </font></b>" + "<font>" + transferNo + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + transferAmount + "</font><br><br>";
    return source;
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }


  @Override
  public void verifiedCompleted() {
    promoteSendMoney();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog(message);
  }

  public void refresh() {
    //yout code in refresh.
    etFundTransferNo.setText("");
    etFundTransferName.setText("");
    etFundTransferAmount.setText("");
  }

  public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
  }

  private class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      FundTransferByMobileFragment.this.refresh();
    }
  }

}
