//package in.payqwik.fragment.fragmentnaviagtionitems;
//
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.content.LocalBroadcastManager;
//import android.text.Html;
//import android.text.Spanned;
//import android.util.Log;
//import android.util.TypedValue;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TableLayout;
//import android.widget.TableRow;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.StringRequest;
//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;
//import com.rengwuxian.materialedittext.MaterialEditText;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import in.payqwik.custom.CustomAlertDialog;
//import in.payqwik.custom.CustomSuccessDialog;
//import in.payqwik.custom.CustomToast;
//import in.payqwik.custom.LoadingDialog;
//import in.payqwik.custom.VerifyMPinDialog;
//import in.payqwik.metadata.ApiUrl;
//import in.payqwik.metadata.AppMetadata;
//import in.payqwik.model.QRScanModel;
//import in.payqwik.model.UserModel;
//import in.payqwik.payqwik.PayQwikApplication;
//import in.payqwik.payqwik.R;
//import in.payqwik.payqwik.activity.OtpVerificationKycAcitvity;
//import in.payqwik.util.CheckLog;
//import in.payqwik.util.EncryptDecryptMVisa;
//import in.payqwik.util.MPinVerifiedListner;
//import in.payqwik.util.PayingDetailsValidation;
//import in.payqwik.util.SecurityUtil;
//
///**
// * Created by Kashif-PC on 12/26/2016.
// */
//public class MVisaFragment extends Fragment implements MPinVerifiedListner {
//
//    QRScanModel obtainedModel;
//    private View rootView;
//    private Button btnQRCodeScan, btnQRPay;
//    private TextView tvPleaseScan, tvScanTitle;
//    private TableLayout tlQRScannedResult;
//    private LinearLayout llQrResult;
//    private LinearLayout llExtraField;
//    private ImageView ivQRCode;
//    private MaterialEditText etQrScanAmount;
//    private UserModel session = UserModel.getInstance();
//    private View focusView = null;
//    private boolean cancel;
//    private JSONObject jsonRequest;
//    private String encryptedMVisa = null;
//    private LoadingDialog loadDlg;
//    //Volley Tag
//    private String tag_json_obj = "json_merchant_pay";
//    private Matcher matcher;
//    private String MerchantNameSub;
//    private String qrRequest = "";
//    private LinearLayout llBharatHead;
//
//    //    private String merchantName = "";
////    private String merchantId = "";
////    private String merchantMCC = "";
////    private String merchantAddress = "";
//    private String amountToPay = "";
//
//    //Card View Info
//
//    private TextView tvmVisaCardNumber, tvmVisaCardValid, tvmVisaCardName, tvMVisaInfo;
//
//    private String mVisaCardNo = "";
//    private String mVisaKitNo = "";
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        loadDlg = new LoadingDialog(getActivity());
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_m_visa_qr, container, false);
//        btnQRCodeScan = (Button) rootView.findViewById(R.id.btnQRCodeScan);
//        btnQRPay = (Button) rootView.findViewById(R.id.btnQRPay);
//        tvScanTitle = (TextView) rootView.findViewById(R.id.tvScanTitle);
//        tvScanTitle.setText("FOR BHARAT QR PAYMENT\nSCAN QR CODE");
//        etQrScanAmount = (MaterialEditText) rootView.findViewById(R.id.etQrScanAmount);
//        tvPleaseScan = (TextView) rootView.findViewById(R.id.tvPleaseScan);
//        tvPleaseScan.setText("Please scan Bharat QR code");
//        llBharatHead = (LinearLayout) rootView.findViewById(R.id.llBharatHead);
//        tlQRScannedResult = (TableLayout) rootView.findViewById(R.id.tlQRScannedResult);
//        llQrResult = (LinearLayout) rootView.findViewById(R.id.llQrResult);
//        ivQRCode = (ImageView) rootView.findViewById(R.id.ivQRCode);
//        llExtraField = (LinearLayout) rootView.findViewById(R.id.llExtraField);
//
//        //Card Info
//        tvmVisaCardNumber = (TextView) rootView.findViewById(R.id.tvmVisaCardNumber);
//        tvmVisaCardValid = (TextView) rootView.findViewById(R.id.tvmVisaCardValid);
//        tvmVisaCardName = (TextView) rootView.findViewById(R.id.tvmVisaCardName);
//        tvMVisaInfo = (TextView) rootView.findViewById(R.id.tvMVisaInfo);
//
//        //Checking KYC or not
//
//        if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
//            tvMVisaInfo.setVisibility(View.GONE);
//            btnQRCodeScan.setClickable(true);
//            btnQRCodeScan.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    tlQRScannedResult.removeAllViews();
//                    IntentIntegrator.forSupportFragment(MVisaFragment.this)
//                            .setOrientationLocked(false)
//                            .setBeepEnabled(true)
//                            .addExtra("PROMPT_MESSAGE", "Scan QR Code")
//                            .initiateScan(IntentIntegrator.QR_CODE_TYPES);
//                }
//            });
//
//        } else {
//            tvMVisaInfo.setText(getResources().getString(R.string.bank_transfer_upgrade));
//            tvMVisaInfo.setVisibility(View.VISIBLE);
//            btnQRCodeScan.setClickable(false);
//            btnQRCodeScan.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.light_text));
//
//        }
//
//        btnQRPay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                attemptPayment();
//            }
//        });
//
//        return rootView;
//    }
//
//    private void attemptPayment() {
//
//        etQrScanAmount.setError(null);
//        cancel = false;
//        checkPayAmount(etQrScanAmount.getText().toString());
//        if (cancel) {
//            focusView.requestFocus();
//        } else if (tlQRScannedResult.getChildCount() == 0) {
//            CustomToast.showMessage(getActivity(), "Please re-scan");
//        } else {
//            amountToPay = etQrScanAmount.getText().toString();
//            showMPinDialog();
//
//        }
//    }
//
//
//    private void checkPayAmount(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!gasCheckLog.isValid) {
//            etQrScanAmount.setError(getString(gasCheckLog.msg));
//            focusView = etQrScanAmount;
//            cancel = true;
//        }
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
//
//        if (result != null) {
//            if (result.getContents() == null) {
//                CustomToast.showMessage(getActivity(), "Cancelled");
//            } else {
////                merchantName = "";
////                merchantId = "";
////                merchantMCC = "";
////                merchantAddress = "";
//
//                Log.i("QR result", result.getContents());
//                qrRequest = result.getContents();
//                getMerchantDetailsFromQrString(result.getContents());
//            }
//        } else {
//            CustomToast.showMessage(getActivity(), "Not a valid QR");
//        }
//    }
//
//
//    public void getMerchantDetailsFromQrString(String qrString) {
//
//        if (qrString.startsWith("00")) {
//            obtainedModel = getMerchantDetailsFromVersion2QrString(qrString);
//        } else {
//            obtainedModel = getMerchantDetailOld(qrString);
//        }
//
//        if (obtainedModel != null) {
//            loadDlg.show();
//            //Adding Obtained data in Table View
//            ArrayList<String> arrayListHead = new ArrayList<>();
//            ArrayList<String> arrayListResult = new ArrayList<>();
//
//            arrayListHead.add("Merchant Id");
//            arrayListHead.add("Merchant Name");
//            arrayListResult.add(obtainedModel.getCard_No());
//            arrayListResult.add(obtainedModel.getMerchantName());
//
//            if (obtainedModel.getCountryCode() != null && !obtainedModel.getCountryCode().isEmpty()) {
//                arrayListHead.add("Country Code");
//                arrayListResult.add(obtainedModel.getCountryCode());
//            }
//
//            if (obtainedModel.getCityName() != null && !obtainedModel.getCityName().isEmpty()) {
//                arrayListHead.add("Merchant Address");
//                arrayListResult.add(obtainedModel.getCityName());
//            }
//
//            if (obtainedModel.getExtraFieldsList() != null && obtainedModel.getExtraFieldsList().size() != 0) {
//
//                for (int i = 0; i < obtainedModel.getExtraFieldsList().size(); i++) {
//                    LinearLayout llDynamic = new LinearLayout(getActivity());
//                    llDynamic.setOrientation(LinearLayout.VERTICAL);
//                    TextView tvDynamic = new TextView(getActivity());
//                    TextView tvDynamicNo = new TextView(getActivity());
//                    float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
//                    tvDynamic.setTextSize(pixels);
//                    EditText etDynamic = new EditText(getActivity());
//                    etDynamic.setTextSize(pixels);
//                    etDynamic.setHint("Field is optional");
//                    tvDynamic.setTypeface(null, Typeface.BOLD);
//
//                    tvDynamicNo.setText(obtainedModel.getExtraFieldsList().get(i));
//                    tvDynamicNo.setVisibility(View.GONE);
//
//                    if (obtainedModel.getExtraFieldsList().get(i).equals("01")) {
//
//                        tvDynamic.setText("Bill no.");
//                    } else if (obtainedModel.getExtraFieldsList().get(i).equals("02")) {
//                        tvDynamic.setText("Mobile no.");
//                    } else if (obtainedModel.getExtraFieldsList().get(i).equals("03")) {
//                        tvDynamic.setText("Store ID");
//                    } else if (obtainedModel.getExtraFieldsList().get(i).equals("04")) {
//                        tvDynamic.setText("Loyalty no.");
//                    } else if (obtainedModel.getExtraFieldsList().get(i).equals("05")) {
//                        tvDynamic.setText("Reference ID");
//                    } else if (obtainedModel.getExtraFieldsList().get(i).equals("06")) {
//                        tvDynamic.setText("Consumer ID");
//                    } else if (obtainedModel.getExtraFieldsList().get(i).equals("07")) {
//                        tvDynamic.setText("Terminal ID");
//                    } else if (obtainedModel.getExtraFieldsList().get(i).equals("08")) {
//                        tvDynamic.setText("Purpose");
//                    } else {
//                        tvDynamic.setText("Extra Info");
//                    }
//                    llDynamic.addView(tvDynamicNo);
//                    llDynamic.addView(tvDynamic);
//                    llDynamic.addView(etDynamic);
//                    llExtraField.addView(llDynamic);
//                }
//
//            } else {
//
//                // CustomToast.showMessage(getActivity(), "Extra fields are empty");
//            }
//
//
//            for (int i = 0; i < arrayListResult.size(); i++) {
//                TableRow row = new TableRow(getActivity());
//
//                TextView textH = new TextView(getActivity());
//                TextView textC = new TextView(getActivity());
//                TextView textV = new TextView(getActivity());
//
//                textH.setText(arrayListHead.get(i));
//                textC.setText(":  ");
//                textV.setText(arrayListResult.get(i));
//                textV.setTypeface(null, Typeface.BOLD);
//                row.addView(textH);
//                row.addView(textC);
//                row.addView(textV);
//
//                tlQRScannedResult.addView(row);
//            }
//
//
////                mVisaCardNo = session.getUserAcNo();
////            Log.i("CARDLEN : ",mVisaCardNo);
////                mVisaKitNo = "VisaKI688";
////                String newAcNo = "6070-xxxx-xxxx-"+mVisaCardNo.substring(11);
////
////            tvmVisaCardNumber.setText(newAcNo);
////            tvmVisaCardName.setText(session.getUserFirstName() + " " + session.getUserLastName());
////            tvmVisaCardValid.setText("01/20");
//
//
//            //Making payment table and card ready
//            tlQRScannedResult.setVisibility(View.VISIBLE);
//            llQrResult.setVisibility(View.VISIBLE);
//            ivQRCode.setVisibility(View.GONE);
//            btnQRCodeScan.setText("Re-Scan");
//            llBharatHead.setVisibility(View.GONE);
//            tvPleaseScan.setText("Are you sure you want to proceed to pay?");
//            tvScanTitle.setText("Scanned successfully");
//            etQrScanAmount.setVisibility(View.VISIBLE);
//            btnQRPay.setVisibility(View.VISIBLE);
//            getCardInfo();
////            loadDlg.dismiss();
//
//
//        } else {
//            loadDlg.dismiss();
//            CustomToast.showMessage(getActivity(), "Not a valid QR");
//        }
//
//
//    }
//
//    private void getCardInfo() {
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("firstName", session.getUserFirstName());
//            jsonRequest.put("lastName", session.getUserLastName());
//            jsonRequest.put("sessionId", session.getUserSessionId());
//            jsonRequest.put("username", session.getUserMobileNo());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            Log.i("URL", ApiUrl.URL_M_VISA_CARD_INFO);
//
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_M_VISA_CARD_INFO, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        Log.i("KYC RESPONSE", response.toString());
//                        String code = response.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//
//                            String jsonResponse = response.getString("response");
//                            JSONObject jsonObjectResponse = new JSONObject(jsonResponse);
//                            JSONObject jsonResult = jsonObjectResponse.getJSONObject("result");
//
//                            JSONArray operatorArray = jsonResult.getJSONArray("cardDetails");
//                            for (int i = 0; i < operatorArray.length(); i++) {
//                                JSONObject c = operatorArray.getJSONObject(i);
//                                mVisaCardNo = c.getString("cardNo");
//                                mVisaKitNo = c.getString("kitNo");
//                                Log.i("CARD", "Card no : " + mVisaCardNo);
//                            }
//
//                            tvmVisaCardNumber.setText(mVisaCardNo);
//                            tvmVisaCardName.setText(session.getUserFirstName() + " " + session.getUserLastName());
//                            tvmVisaCardValid.setText("01/20");
//
//
//                            //Making payment table and card ready
//                            tlQRScannedResult.setVisibility(View.VISIBLE);
//                            llQrResult.setVisibility(View.VISIBLE);
//                            ivQRCode.setVisibility(View.GONE);
//                            btnQRCodeScan.setText("Re-Scan");
//                            llBharatHead.setVisibility(View.GONE);
//                            tvPleaseScan.setText("Are you sure you want to proceed to pay?");
//                            tvScanTitle.setText("Scanned successfully");
//                            etQrScanAmount.setVisibility(View.VISIBLE);
//                            btnQRPay.setVisibility(View.VISIBLE);
//                            loadDlg.dismiss();
//                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        } else {
//                            loadDlg.dismiss();
////                            String jsonString = response.getString("response");
////                            JSONObject jsonObject = new JSONObject(jsonString);
//                            String errorMessage = response.getString("message");
//                            CustomToast.showMessage(getActivity(), errorMessage);
//                        }
//
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
//
//
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
////    private void getCardInfo() {
////        jsonRequest = new JSONObject();
////        try {
////            jsonRequest.put("firstName", session.getUserFirstName());
////            jsonRequest.put("lastName", session.getUserLastName());
////            jsonRequest.put("sessionId", session.getUserSessionId());
////            jsonRequest.put("username", session.getUserMobileNo());
////
////        } catch (JSONException e) {
////            e.printStackTrace();
////            jsonRequest = null;
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        if (jsonRequest != null) {
////            Log.i("Card Param",jsonRequest.toString());
////            Log.i("Card URL",ApiUrl.URL_M_VISA_CARD_INFO);
////            StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_M_VISA_CARD_INFO, new Response.Listener<String>() {
////                @Override
////                public void onResponse(String stringResponse) {
////                    try {
////                        JSONObject response = new JSONObject(stringResponse);
////                        Log.i("mVisa Card", response.toString());
////                        String responseMain = response.getString("response");
////                        JSONObject responseJson = new JSONObject(responseMain);
////                        String code = responseJson.getString("code");
////
////                        if (code != null && code.equals("S00")) {
////                            tvmVisaCardNumber.setText("5726-XXXX-XXXX-4122");
////                            tvmVisaCardName.setText(session.getUserFirstName() + " " + session.getUserLastName());
////                            tvmVisaCardValid.setText("01/20");
////
////
////                            //Making payment table and card ready
////                            tlQRScannedResult.setVisibility(View.VISIBLE);
////                            llQrResult.setVisibility(View.VISIBLE);
////                            ivQRCode.setVisibility(View.GONE);
////                            btnQRCodeScan.setText("Re-Scan");
////                            llBharatHead.setVisibility(View.GONE);
////                            tvPleaseScan.setText("Are you sure you want to proceed to pay?");
////                            tvScanTitle.setText("Scanned successfully");
////                            etQrScanAmount.setVisibility(View.VISIBLE);
////                            btnQRPay.setVisibility(View.VISIBLE);
////                            loadDlg.dismiss();
////
////                        } else if (code != null && code.equals("F03")) {
////                            loadDlg.dismiss();
////                            showInvalidSessionDialog();
////                        } else {
////                            loadDlg.dismiss();
////                            String jsonString = response.getString("response");
////                            JSONObject jsonObject = new JSONObject(jsonString);
////                            String errorMessage = jsonObject.getString("message");
////                            CustomToast.showMessage(getActivity(), errorMessage);
////                        }
////                    } catch (JSONException e) {
////                        loadDlg.dismiss();
////                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
////                        e.printStackTrace();
////                    }
////                }
////            }, new Response.ErrorListener() {
////                @Override
////                public void onErrorResponse(VolleyError error) {
////                    loadDlg.dismiss();
////                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
////                    error.printStackTrace();
////                }
////            }) {
////                @Override
////                public Map<String, String> getHeaders() throws AuthFailureError {
////                    HashMap<String, String> map = new HashMap<>();
////                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
////                    map.put("Content-Type", "text/plain");
////                    return map;
////                }
////
////
////                @Override
////                public byte[] getBody() throws AuthFailureError {
////                    return String.valueOf(jsonRequest).trim().getBytes();
////                }
////            };
////            int socketTimeout = 60000;
////            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////            postReq.setRetryPolicy(policy);
////            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
////        }
////    }
//
//
//    public void promoteMVisaPayNew() {
//        loadDlg.show();
//        jsonRequest = new JSONObject();
//
//        try {
////            jsonRequest.put("request", "0B4444444483214Ajay24531139BANGALORE42In53356M2PYN76039201");
//            jsonRequest.put("request", qrRequest);
//            jsonRequest.put("data", etQrScanAmount.getText().toString());
//            jsonRequest.put("sessionId", session.getUserSessionId());
//            jsonRequest.put("merchantName", obtainedModel.getMerchantName());
//            jsonRequest.put("merchantId", obtainedModel.getCard_No());
//            jsonRequest.put("merchantAddress", obtainedModel.getCityName());
//            jsonRequest.put("additionalData", getExtraFieldData());
//            //TODO change with mVisaCardNo
////            jsonRequest.put("accountNumber", "5087070100075827");
//            jsonRequest.put("accountNumber", mVisaCardNo);
//
//            Log.i("Json Value", jsonRequest.toString());
//
//            encryptedMVisa = EncryptDecryptMVisa.encrypt(jsonRequest.toString());
//
//            Log.i("Encypted Value", encryptedMVisa.trim());
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (encryptedMVisa != null) {
//            StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_M_VISA_PAY, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String stringResponse) {
//                    try {
//                        JSONObject response = new JSONObject(stringResponse);
//                        Log.i("mVisa Response", response.toString());
////                        String responseMain = response.getString("response");
////                        JSONObject responseJson = new JSONObject(responseMain);
//                        String code = response.getString("code");
//                        if (code != null && code.equals("S00")) {
//                            loadDlg.dismiss();
//                            etQrScanAmount.getText().clear();
//                            showSuccessDialog();
//                            sendRefresh();
//
//                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        } else {
//                            loadDlg.dismiss();
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
//                            String errorMessage = jsonObject.getString("message");
//                            CustomToast.showMessage(getActivity(), errorMessage);
//                        }
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
//                    error.printStackTrace();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    map.put("Content-Type", "text/plain");
//                    return map;
//                }
//
//
//                @Override
//                public byte[] getBody() throws AuthFailureError {
//                    return encryptedMVisa.trim().getBytes();
//                }
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
//
////    public void promoteMVisaPay() {
////        Log.i("mVisa Url", ApiUrl.URL_M_VISA_PAY);
////        loadDlg.show();
////        jsonRequest = new JSONObject();
////        try {
//////            qrRequest to be replaced in request static param.
////            jsonRequest.put("request", "0B4444444483214Ajay24531139BANGALORE42In53356M2PYN76039201");
////            jsonRequest.put("data", etQrScanAmount.getText().toString());
////            jsonRequest.put("sessionId", session.getUserSessionId());
////
////            jsonRequest.put("merchantName", merchantName);
////            jsonRequest.put("merchantId", merchantId);
////            jsonRequest.put("merchantAddress", merchantAddress);
////        } catch (JSONException e) {
////            e.printStackTrace();
////            jsonRequest = null;
////        }
////
////        if (jsonRequest != null) {
////            Log.i("JsonRequest", jsonRequest.toString());
////
////
////            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_M_VISA_PAY, jsonRequest, new Response.Listener<JSONObject>() {
////                @Override
////                public void onResponse(JSONObject response) {
////                    try {
////                        Log.i("mVisa Response", response.toString());
////                        String code = response.getString("code");
////                        if (code != null && code.equals("S00")) {
////                            loadDlg.dismiss();
////
////                            etQrScanAmount.getText().clear();
////                            showSuccessDialog();
////                            sendRefresh();
////                        } else if (code != null && code.equals("F03")) {
////                            loadDlg.dismiss();
////                            showInvalidSessionDialog();
////                        } else {
////                            loadDlg.dismiss();
////                            String jsonString = response.getString("response");
////                            JSONObject jsonObject = new JSONObject(jsonString);
////                            String errorMessage = jsonObject.getString("message");
////                            CustomToast.showMessage(getActivity(), errorMessage);
////                        }
////                    } catch (JSONException e) {
////                        loadDlg.dismiss();
////                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
////                        e.printStackTrace();
////                    }
////                }
////            }, new Response.ErrorListener() {
////                @Override
////                public void onErrorResponse(VolleyError error) {
////                    loadDlg.dismiss();
////                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
////                    error.printStackTrace();
////                }
////            }) {
////                @Override
////                public Map<String, String> getHeaders() throws AuthFailureError {
////                    HashMap<String, String> map = new HashMap<>();
////                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
////                    return map;
////                }
////            };
////            int socketTimeout = 60000;
////            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////            postReq.setRetryPolicy(policy);
////            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
////        }
////    }
//
//    private void sendRefresh() {
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "1");
//        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
//    }
//
//    private void sendLogout() {
//        getActivity().finish();
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "4");
//        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
//    }
//
//    public void showInvalidSessionDialog() {
//        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
//            result = Html.fromHtml(AppMetadata.getInvalidSession());
//        }
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                sendLogout();
//            }
//        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//
//            }
//        });
//        builder.show();
//    }
//
//    public void showSuccessDialog() {
//        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(getMVisaSuccess(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
//            result = Html.fromHtml(getMVisaSuccess());
//        }
//        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//                getActivity().finish();
//            }
//        });
//        builder.show();
//    }
//
//
//    public String getMVisaSuccess() {
//        String source =
//                "<br><b><font color=#39145A> Receiver: " + obtainedModel.getMerchantName() + "</font></b></br>" +
//                        "<br><b><font color=#39145A> Address: " + obtainedModel.getCityName() + "</font></b></br>" +
//                        "<br><b><font color=#39145A> Amount: " + amountToPay + "</font></b></br>";
//        return source;
//    }
//
//
//    public void showMPinDialog() {
//        VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//    }
//
//
//    @Override
//    public void verifiedCompleted() {
//        promoteMVisaPayNew();
//    }
//
//    @Override
//    public void sessionInvalid() {
//        showInvalidSessionDialog();
//    }
//
//
//    ///Version 2
//    public QRScanModel getMerchantDetailsFromVersion2QrString(String qrString) {
//
//        Integer tagPosition[] = {0};
//        QRScanModel qrData = new QRScanModel();
//        try {
//            int i = 0; //n = str.length()
//
//            while (i < 63) {
//
//                int tagPosEndPos = tagPosition[0] + 2;
//                if (qrString.length() >= tagPosEndPos) {
//                    String tagStr = qrString.substring(tagPosition[0], tagPosEndPos);
//                    String tagValue = "";
//                    if (Integer.valueOf(tagStr).equals(i)) {
//
//                        String tagLenStr = qrString.substring(tagPosition[0] + 2, tagPosition[0] + 2 + 2);
//                        tagValue = qrString.substring(tagPosition[0] + 2 + 2,
//                                tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr));
//                        tagPosition[0] = tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr);
//                    }
//
//                    System.out.println("TAG STR :: " + tagStr + " ::: TAGVALUE ::" + tagValue);
//                    switch (Integer.valueOf(tagStr)) {
//
//                        case 0:
//                            // qrData.setCard_No(tagValue);
//                        case 2:
//                            qrData.setCard_No(tagValue);
//                            qrData.setTerminalID(tagValue);
//                            qrData.setMvisaId(tagValue);
//                            break;
//                        case 4:
//                            qrData.setCard_No(tagValue);
//                            qrData.setDefaultValue(tagValue);
//                            qrData.setMasterID(tagValue);
//                            break;
//                        case 6:
////                            actual card
//                            qrData.setCard_No(tagValue);
//                            qrData.setDefaultValue(tagValue);
//                            qrData.setRuPayId(tagValue);
//                            break;
//                        case 52:
//                            qrData.setMCC(tagValue);
//                            break;
//                        case 53:
//                            qrData.setIndianRupeeCode(tagValue);
//                            break;
//                        case 54:
//                            qrData.setTransAmount(tagValue);
//                            break;
//                        case 55:
//                            qrData.setTipID(tagValue);
//                            break;
//                        case 56:
//                            qrData.setFixedTipAmount(tagValue);
//                            break;
//                        case 57:
//                            qrData.setFixedTipPercentage(tagValue);
//                            break;
//                        case 58:
//                            qrData.setCountryCode(tagValue);
//                            break;
//                        case 59:
//                            qrData.setMerchantName(tagValue);
//                            break;
//                        case 60:
//                            qrData.setCityName(tagValue);
//                            break;
//                        case 62:
//                            qrData.setExtraFields(tagValue);
//                            qrData.setExtraFieldsList(getExtraFieldsFrom2QrStringTag62(tagValue));
//                            break;
//                    }
//                }
//                i++;
//                Log.d("QR TAG", String.valueOf(i));
//
//            }
//            return qrData;
//        } catch (Exception e) {
//            Log.d("QR Exception", String.valueOf(e.getStackTrace()));
//            return null;
//        }
//
//    }
//
//
//    public QRScanModel getMerchantDetailOld(String result) {
//
//        try {
//            QRScanModel qrData = new QRScanModel();
//            String sub = result.substring(2);
//            Log.i("Vlaue", result.substring(2));
//            String regex = "(\\d+)";
//            matcher = Pattern.compile(regex).matcher(sub);
//            if (matcher.find()) {
//                String merchantI = matcher.group(1);
//                qrData.setCard_No(merchantI.subSequence(0, merchantI.length() - 1).toString());
//                String last = merchantI.substring(merchantI.length() - 2);
//
//                if (String.valueOf(last.charAt(0)).equals("1")) {
//                    MerchantNameSub = sub.substring(merchantI.length());
//
//                } else if (String.valueOf(last.charAt(1)).equals("1")) {
//                    MerchantNameSub = sub.substring(merchantI.length() + 1);
//                }
//                Matcher matcher1 = Pattern.compile("[a-zA-Z\\s\'\"]+").matcher(MerchantNameSub);
//                if (matcher1.find()) {
//                    qrData.setMerchantName(matcher1.group(0));
//
//                    String MCC = MerchantNameSub.substring(matcher1.group(0).length());
//
//                    if (MCC.startsWith("2")) {
//                        String MCCode = MCC.substring(2);
//                        Log.i("MCCSUB", MCCode);
//                        Matcher matcher2 = Pattern.compile(regex).matcher(MCCode);
//                        if (matcher2.find()) {
//                            Log.i("MAtch", matcher2.group(1).subSequence(0, 4).toString());
//                            qrData.setMCC(matcher2.group(1).subSequence(0, 4).toString());
//
//                            String SubCountry = MCCode.substring(matcher2.group(1).subSequence(0, 4).toString().length());
//                            if (SubCountry.startsWith("3")) {
//                                String subLength = SubCountry.substring(2);
//                                Matcher matcher3 = Pattern.compile("[a-zA-Z\\s\'\"]+").matcher(subLength);
//                                if (matcher3.find()) {
//                                    qrData.setCityName(matcher3.group());
////                                    String Su = subLength.substring(merchantAddress.length());
////                                    if (Su.startsWith("4")) {
////                                        String INR = Su.substring(2);
////                                        String CurrencyCode = (String) INR.subSequence(0, 2);
////                                        if (INR.substring(CurrencyCode.length()).startsWith("5")) {
////                                            String CountryCurrency = INR.substring(CurrencyCode.length()).substring(2).subSequence(0, 3).toString();
////                                            if (INR.substring(CurrencyCode.length()).substring(5).length() > 0 && INR.substring(CurrencyCode.length()).substring(5).startsWith("6")) {
////
////                                                String Amount = INR.substring(CurrencyCode.length()).substring(7);
////                                                String currentAmount = String.valueOf(Amount.regionMatches(7, Amount, 0, 13));
////                                                Matcher matcher4 = Pattern.compile(regex).matcher(Amount);
////                                                if (matcher4.find()) {
////                                                    Log.i("lett", String.valueOf(matcher4.group().split("7").length));
////                                                    String[] s = matcher4.group().split("7");
////                                                }
////                                            } else {
//////                                                    CustomToast.showMessage(getActivity(), "Currency Code ::" + CountryCurrency);
////                                            }
////
////                                        }
////                                    }
//
//                                }
//                            }
//                        }
//                    }
//                }
//
//            }
//            return qrData;
//        } catch (Exception e) {
//            return null;
//        }
//
//    }
//
//    //Version 1
//    public QRScanModel getMerchantDetailsFromVersion1QrString(String qrString) {
//
//        try {
//            QRScanModel qrData = new QRScanModel();
//            Integer mvisaIdHexLenth = Integer.valueOf(qrString.substring(1, 2), 16);
//            String mvisaId = qrString.substring(2, mvisaIdHexLenth + 2);
//            Log.i("mvisaId", mvisaId);
//
//            qrData.setCard_No(mvisaId);
//
//            Integer nameHexLength = Integer.valueOf(qrString.substring(mvisaIdHexLenth + 3, mvisaIdHexLenth + 4), 16);
//            String merchantName = qrString.substring(mvisaIdHexLenth + 4, mvisaIdHexLenth + nameHexLength + 4);
//            qrData.setMerchantName(merchantName);
//
//            Integer mccLen = Integer.valueOf(
//                    qrString.substring(mvisaIdHexLenth + nameHexLength + 5, mvisaIdHexLenth + nameHexLength + 6), 16);
//
//
//            Integer locLength = Integer.valueOf(qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + 7,
//                    mvisaIdHexLenth + nameHexLength + mccLen + 8), 16);
//
//
//            Integer countryLen = Integer
//                    .valueOf(qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + 9,
//                            mvisaIdHexLenth + nameHexLength + mccLen + locLength + 10), 16);
//            String country = qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + 10,
//                    mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 10);
//            qrData.setCountryCode(country);
//
//            Integer currencyLen = Integer
//                    .valueOf(qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 11,
//                            mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 12));
//            String currencyCode = qrString.substring(
//                    mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 12,
//                    mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 12);
//            qrData.setIndianRupeeCode(currencyCode);
//
//            String amountStr = qrString
//                    .substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 12);
//
//            Integer amountLen = 0;
//            Double amount = null;
//
//            Integer referenceNoLen = 0;
//            String referenceNo = null;
//
//            Map<String, Object> response = new HashMap<String, Object>();
//
//            if (!amountStr.equals("") && !amountStr.startsWith("M2PY")
//                    && Integer.valueOf(amountStr.substring(0, 1)) == 6) {
//                amountLen = Integer.valueOf(qrString.substring(
//                        mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 13,
//                        mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 14));
//                amount = Double.valueOf(amountStr.substring(2, amountLen + 2));
//                qrData.setTransAmount(amountStr.substring(2, amountLen + 2));
//            }
//
//
//            if (qrString.contains("M2PY")) {
//                String terminalId = qrString.substring(
//                        mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 18);
//                qrData.setTerminalID(terminalId);
//            }
//
//            return qrData;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//
//    public ArrayList<String> getExtraFieldsFrom2QrStringTag62(String qrString) {
//
//        Integer tagPosition[] = {0};
//        ArrayList<String> qrDataExtraFields = new ArrayList<>();
//        try {
//            int i = 0; //n = str.length(),
//
//            while (i < 9) {
//
//                int tagPosEndPos = tagPosition[0] + 2;
//                if (qrString.length() >= tagPosEndPos) {
//                    String tagStr = qrString.substring(tagPosition[0], tagPosEndPos);
//                    String tagValue = "";
//                    if (Integer.valueOf(tagStr).equals(i)) {
//
//                        String tagLenStr = qrString.substring(tagPosition[0] + 2, tagPosition[0] + 2 + 2);
//                        tagValue = qrString.substring(tagPosition[0] + 2 + 2,
//                                tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr));
//                        tagPosition[0] = tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr);
//                    }
//
//                    System.out.println("TAG STR :: " + tagStr + " ::: TAGVALUE ::" + tagValue);
//                    switch (Integer.valueOf(tagStr)) {
//
//                        case 1:
//                            if (tagValue.contains("***")) {
//                                qrDataExtraFields.add("01");
//                            }
//                            break;
//                        case 2:
//                            if (tagValue.contains("***")) {
//                                qrDataExtraFields.add("02");
//                            }
//                            break;
//                        case 3:
//                            if (tagValue.contains("***")) {
//                                qrDataExtraFields.add("03");
//                            }
//                            break;
//                        case 4:
//                            if (tagValue.contains("***")) {
//                                qrDataExtraFields.add("04");
//                            }
//                            break;
//                        case 5:
//                            if (tagValue.contains("***")) {
//                                qrDataExtraFields.add("05");
//                            }
//                            break;
//                        case 6:
//                            if (tagValue.contains("***")) {
//                                qrDataExtraFields.add("06");
//                            }
//                            break;
//                        case 8:
//                            if (tagValue.contains("***")) {
//                                qrDataExtraFields.add("08");
//                            }
//                            break;
//                    }
//                }
//                i++;
//                Log.d("QR TAG", String.valueOf(i));
//
//            }
//            return qrDataExtraFields;
//        } catch (Exception e) {
//            Log.d("QR Exception", String.valueOf(e.getStackTrace()));
//            return null;
//        }
//
//    }
//
//
//    private String getExtraFieldData() {
//        String extraField = "";
//        for (int index = 0; index < (((ViewGroup) llExtraField).getChildCount()); ++index) {
//            LinearLayout lvChild = (LinearLayout) ((ViewGroup) llExtraField).getChildAt(index);
//            TextView tvChild = (TextView) ((ViewGroup) lvChild).getChildAt(0);
//            EditText etChild = (EditText) ((ViewGroup) lvChild).getChildAt(2);
//
//            extraField = extraField + tvChild.getText() + "=" + etChild.getText() + ";";
//            Log.i("ExtraOutput", extraField);
//
//        }
//        Log.i("FinalOutput", extraField);
//        return extraField;
//    }
//
//
//}


package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.QRScanModel;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.EncryptDecryptMVisa;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;

/**
 * Created by Kashif-PC on 12/26/2016.
 */
public class MVisaFragment extends Fragment implements MPinVerifiedListner {

  QRScanModel obtainedModel;
  private View rootView;
  private Button btnQRCodeScan, btnQRPay;
  private TextView tvPleaseScan, tvScanTitle;
  private TableLayout tlQRScannedResult;
  private LinearLayout llQrResult;
  private LinearLayout llExtraField;
  private ImageView ivQRCode;
  private MaterialEditText etQrScanAmount;
  private UserModel session = UserModel.getInstance();
  private View focusView = null;
  private boolean cancel;
  private JSONObject jsonRequest;
  private String encryptedMVisa = null;
  private LoadingDialog loadDlg;
  //Volley Tag
  private String tag_json_obj = "json_merchant_pay";
  private Matcher matcher;
  private String MerchantNameSub;
  private String qrRequest = "";
  private LinearLayout llBharatHead;

  //    private String merchantName = "";
//    private String merchantId = "";
//    private String merchantMCC = "";
//    private String merchantAddress = "";
  private String amountToPay = "";

  //Card View Info

  private TextView tvmVisaCardNumber, tvmVisaCardValid, tvmVisaCardName, tvMVisaInfo;

  private String mVisaCardNo = "";
  private String mVisaKitNo = "";


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_m_visa_qr, container, false);
    btnQRCodeScan = (Button) rootView.findViewById(R.id.btnQRCodeScan);
    btnQRPay = (Button) rootView.findViewById(R.id.btnQRPay);
    tvScanTitle = (TextView) rootView.findViewById(R.id.tvScanTitle);
    tvScanTitle.setText("FOR BHARAT QR PAYMENT\nSCAN QR CODE");
    etQrScanAmount = (MaterialEditText) rootView.findViewById(R.id.etQrScanAmount);
    tvPleaseScan = (TextView) rootView.findViewById(R.id.tvPleaseScan);
    tvPleaseScan.setText("Please scan Bharat QR code");
    llBharatHead = (LinearLayout) rootView.findViewById(R.id.llBharatHead);
    tlQRScannedResult = (TableLayout) rootView.findViewById(R.id.tlQRScannedResult);
    llQrResult = (LinearLayout) rootView.findViewById(R.id.llQrResult);
    ivQRCode = (ImageView) rootView.findViewById(R.id.ivQRCode);
    llExtraField = (LinearLayout) rootView.findViewById(R.id.llExtraField);

    //Card Info
    tvmVisaCardNumber = (TextView) rootView.findViewById(R.id.tvmVisaCardNumber);
    tvmVisaCardValid = (TextView) rootView.findViewById(R.id.tvmVisaCardValid);
    tvmVisaCardName = (TextView) rootView.findViewById(R.id.tvmVisaCardName);
    tvMVisaInfo = (TextView) rootView.findViewById(R.id.tvMVisaInfo);

    //Checking KYC or not

//        if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
    tvMVisaInfo.setVisibility(View.GONE);
    btnQRCodeScan.setClickable(true);
    btnQRCodeScan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        tlQRScannedResult.removeAllViews();
        IntentIntegrator.forSupportFragment(MVisaFragment.this)
          .setOrientationLocked(false)
          .setBeepEnabled(true)
          .addExtra("PROMPT_MESSAGE", "Scan QR Code")
          .initiateScan(IntentIntegrator.QR_CODE_TYPES);
      }
    });

//        } else {
//            tvMVisaInfo.setText(getResources().getString(R.string.bank_transfer_upgrade));
//            tvMVisaInfo.setVisibility(View.VISIBLE);
//            btnQRCodeScan.setClickable(false);
//            btnQRCodeScan.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.light_text));
//
//        }

    btnQRPay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });

    return rootView;
  }

  private void attemptPayment() {

    etQrScanAmount.setError(null);
    cancel = false;
    checkPayAmount(etQrScanAmount.getText().toString());
    if (cancel) {
      focusView.requestFocus();
    } else if (tlQRScannedResult.getChildCount() == 0) {
      CustomToast.showMessage(getActivity(), "Please re-scan");
    } else {
      amountToPay = etQrScanAmount.getText().toString();
      promoteMVisaPayNew();

    }
  }


  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etQrScanAmount.setError(getString(gasCheckLog.msg));
      focusView = etQrScanAmount;
      cancel = true;
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

    if (result != null) {
      if (result.getContents() == null) {
        CustomToast.showMessage(getActivity(), "Cancelled");
      } else {
        qrRequest = result.getContents();
        if (qrRequest.startsWith("0")) {
          getMerchantDetailsFromQrString(result.getContents());
        } else {
          CustomToast.showMessage(getActivity(), "Not a valid QR");
        }
      }
    } else {
      CustomToast.showMessage(getActivity(), "Not a valid QR");
    }
  }


  public void getMerchantDetailsFromQrString(String qrString) {

    if (qrString.startsWith("00")) {
      obtainedModel = getMerchantDetailsFromVersion2QrString(qrString);
    } else {
      obtainedModel = getMerchantDetailOld(qrString);
    }

    if (obtainedModel != null) {
      loadDlg.show();
      //Adding Obtained data in Table View
      ArrayList<String> arrayListHead = new ArrayList<>();
      ArrayList<String> arrayListResult = new ArrayList<>();

      arrayListHead.add("Merchant Id");
      arrayListHead.add("Merchant Name");
      arrayListResult.add(obtainedModel.getCard_No());
      arrayListResult.add(obtainedModel.getMerchantName());

      if (obtainedModel.getCountryCode() != null && !obtainedModel.getCountryCode().isEmpty()) {
        arrayListHead.add("Country Code");
        arrayListResult.add(obtainedModel.getCountryCode());
      }

      if (obtainedModel.getCityName() != null && !obtainedModel.getCityName().isEmpty()) {
        arrayListHead.add("Merchant Address");
        arrayListResult.add(obtainedModel.getCityName());
      }

      if (obtainedModel.getExtraFieldsList() != null && obtainedModel.getExtraFieldsList().size() != 0) {

        for (int i = 0; i < obtainedModel.getExtraFieldsList().size(); i++) {
          LinearLayout llDynamic = new LinearLayout(getActivity());
          llDynamic.setOrientation(LinearLayout.VERTICAL);
          TextView tvDynamic = new TextView(getActivity());
          TextView tvDynamicNo = new TextView(getActivity());
          float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
          tvDynamic.setTextSize(pixels);
          EditText etDynamic = new EditText(getActivity());
          etDynamic.setTextSize(pixels);
          etDynamic.setHint("Field is optional");
          tvDynamic.setTypeface(null, Typeface.BOLD);

          tvDynamicNo.setText(obtainedModel.getExtraFieldsList().get(i));
          tvDynamicNo.setVisibility(View.GONE);

          if (obtainedModel.getExtraFieldsList().get(i).equals("01")) {

            tvDynamic.setText("Bill no.");
          } else if (obtainedModel.getExtraFieldsList().get(i).equals("02")) {
            tvDynamic.setText("Mobile no.");
          } else if (obtainedModel.getExtraFieldsList().get(i).equals("03")) {
            tvDynamic.setText("Store ID");
          } else if (obtainedModel.getExtraFieldsList().get(i).equals("04")) {
            tvDynamic.setText("Loyalty no.");
          } else if (obtainedModel.getExtraFieldsList().get(i).equals("05")) {
            tvDynamic.setText("Reference ID");
          } else if (obtainedModel.getExtraFieldsList().get(i).equals("06")) {
            tvDynamic.setText("Consumer ID");
          } else if (obtainedModel.getExtraFieldsList().get(i).equals("07")) {
            tvDynamic.setText("Terminal ID");
          } else if (obtainedModel.getExtraFieldsList().get(i).equals("08")) {
            tvDynamic.setText("Purpose");
          } else {
            tvDynamic.setText("Extra Info");
          }
          llDynamic.addView(tvDynamicNo);
          llDynamic.addView(tvDynamic);
          llDynamic.addView(etDynamic);
          llExtraField.addView(llDynamic);
        }

      } else {

        // CustomToast.showMessage(getActivity(), "Extra fields are empty");
      }


      for (int i = 0; i < arrayListResult.size(); i++) {
        TableRow row = new TableRow(getActivity());

        TextView textH = new TextView(getActivity());
        TextView textC = new TextView(getActivity());
        TextView textV = new TextView(getActivity());

        textH.setText(arrayListHead.get(i));
        textC.setText(":  ");
        textV.setText(arrayListResult.get(i));
        textV.setTypeface(null, Typeface.BOLD);
        row.addView(textH);
        row.addView(textC);
        row.addView(textV);

        tlQRScannedResult.addView(row);
      }


//                mVisaCardNo = session.getUserAcNo();
//            Log.i("CARDLEN : ",mVisaCardNo);
//                mVisaKitNo = "VisaKI688";
//                String newAcNo = "6070-xxxx-xxxx-"+mVisaCardNo.substring(11);
//
//            tvmVisaCardNumber.setText(newAcNo);
//            tvmVisaCardName.setText(session.getUserFirstName() + " " + session.getUserLastName());
//            tvmVisaCardValid.setText("01/20");


      //Making payment table and card ready
      tlQRScannedResult.setVisibility(View.VISIBLE);
      llQrResult.setVisibility(View.VISIBLE);
      ivQRCode.setVisibility(View.GONE);
      btnQRCodeScan.setText("Re-Scan");
      llBharatHead.setVisibility(View.GONE);
      tvPleaseScan.setText("Are you sure you want to proceed to pay?");
      tvScanTitle.setText("Scanned successfully");
      etQrScanAmount.setVisibility(View.VISIBLE);
      btnQRPay.setVisibility(View.VISIBLE);
      getCardInfo();
//            loadDlg.dismiss();


    } else {
      loadDlg.dismiss();
      CustomToast.showMessage(getActivity(), "Not a valid QR");
    }


  }

  private void getCardInfo() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("firstName", session.getUserFirstName());
      jsonRequest.put("lastName", session.getUserLastName());
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("username", session.getUserMobileNo());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      Log.i("URL", ApiUrl.URL_M_VISA_CARD_INFO);

      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_M_VISA_CARD_INFO, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("KYC RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {

              String jsonResponse = response.getString("response");
              JSONObject jsonObjectResponse = new JSONObject(jsonResponse);
              JSONObject jsonResult = jsonObjectResponse.getJSONObject("result");

              JSONArray operatorArray = jsonResult.getJSONArray("cardDetails");
              for (int i = 0; i < operatorArray.length(); i++) {
                JSONObject c = operatorArray.getJSONObject(i);
                mVisaCardNo = c.getString("cardNo");
                mVisaKitNo = c.getString("kitNo");
                Log.i("CARD", "Card no : " + mVisaCardNo);
              }

              tvmVisaCardNumber.setText(mVisaCardNo);
              tvmVisaCardName.setText(session.getUserFirstName() + " " + session.getUserLastName());
              tvmVisaCardValid.setText("01/20");


              //Making payment table and card ready
              tlQRScannedResult.setVisibility(View.VISIBLE);
              llQrResult.setVisibility(View.VISIBLE);
              ivQRCode.setVisibility(View.GONE);
              btnQRCodeScan.setText("Re-Scan");
              llBharatHead.setVisibility(View.GONE);
              tvPleaseScan.setText("Are you sure you want to proceed to pay?");
              tvScanTitle.setText("Scanned successfully");
              etQrScanAmount.setVisibility(View.VISIBLE);
              btnQRPay.setVisibility(View.VISIBLE);
              loadDlg.dismiss();
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {
              loadDlg.dismiss();
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
              String errorMessage = response.getString("message");
              CustomToast.showMessage(getActivity(), errorMessage);
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));


        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  public void promoteMVisaPayNew() {
    loadDlg.show();
    jsonRequest = new JSONObject();

    try {
      jsonRequest.put("request", qrRequest);
      jsonRequest.put("data", etQrScanAmount.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("merchantName", obtainedModel.getMerchantName());
      jsonRequest.put("merchantId", obtainedModel.getCard_No());
      jsonRequest.put("merchantAddress", obtainedModel.getCityName());
      jsonRequest.put("additionalData", getExtraFieldData());
      //TODO change with mVisaCardNo
//            jsonRequest.put("accountNumber", "5087070100075827");
      jsonRequest.put("accountNumber", mVisaCardNo);

      Log.i("Json Value", jsonRequest.toString());

      encryptedMVisa = EncryptDecryptMVisa.encrypt(jsonRequest.toString());

      Log.i("Encypted Value", encryptedMVisa.trim());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (encryptedMVisa != null) {
      StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_M_VISA_PAY, new Response.Listener<String>() {
        @Override
        public void onResponse(String stringResponse) {
          try {
            JSONObject response = new JSONObject(stringResponse);
            Log.i("mVisa Response", response.toString());
//                        String responseMain = response.getString("response");
//                        JSONObject responseJson = new JSONObject(responseMain);
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              loadDlg.dismiss();
              etQrScanAmount.getText().clear();
              showSuccessDialog();


            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {
              loadDlg.dismiss();
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String errorMessage = jsonObject.getString("message");
              CustomToast.showMessage(getActivity(), errorMessage);
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          map.put("Content-Type", "text/plain");
          return map;
        }


        @Override
        public byte[] getBody() throws AuthFailureError {
          return encryptedMVisa.trim().getBytes();
        }
      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


//

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(getActivity(), HomeMainActivity.class));
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        getActivity().finish();
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getMVisaSuccess(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getMVisaSuccess());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
        getActivity().finish();
      }
    });
    builder.show();
  }


  public String getMVisaSuccess() {
    String source =
      "<br><b><font color=#39145A> Receiver: " + obtainedModel.getMerchantName() + "</font></b></br>" +
        "<br><b><font color=#39145A> Address: " + obtainedModel.getCityName() + "</font></b></br>" +
        "<br><b><font color=#39145A> Amount: " + amountToPay + "</font></b></br>";
    return source;
  }


  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }


  @Override
  public void verifiedCompleted() {
    promoteMVisaPayNew();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog();
  }


  ///Version 2
  public QRScanModel getMerchantDetailsFromVersion2QrString(String qrString) {

    Integer tagPosition[] = {0};
    QRScanModel qrData = new QRScanModel();
    try {
      int i = 0; //n = str.length()

      while (i < 63) {

        int tagPosEndPos = tagPosition[0] + 2;
        if (qrString.length() >= tagPosEndPos) {
          String tagStr = qrString.substring(tagPosition[0], tagPosEndPos);
          String tagValue = "";
          if (Integer.valueOf(tagStr).equals(i)) {

            String tagLenStr = qrString.substring(tagPosition[0] + 2, tagPosition[0] + 2 + 2);
            tagValue = qrString.substring(tagPosition[0] + 2 + 2,
              tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr));
            tagPosition[0] = tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr);
          }

          System.out.println("TAG STR :: " + tagStr + " ::: TAGVALUE ::" + tagValue);
          switch (Integer.valueOf(tagStr)) {

            case 0:
              // qrData.setCard_No(tagValue);
            case 2:
              qrData.setCard_No(tagValue);
              qrData.setTerminalID(tagValue);
              qrData.setMvisaId(tagValue);
              break;
            case 4:
              qrData.setCard_No(tagValue);
              qrData.setDefaultValue(tagValue);
              qrData.setMasterID(tagValue);
              break;
            case 6:
//                            actual card
              qrData.setCard_No(tagValue);
              qrData.setDefaultValue(tagValue);
              qrData.setRuPayId(tagValue);
              break;
            case 52:
              qrData.setMCC(tagValue);
              break;
            case 53:
              qrData.setIndianRupeeCode(tagValue);
              break;
            case 54:
              qrData.setTransAmount(tagValue);
              break;
            case 55:
              qrData.setTipID(tagValue);
              break;
            case 56:
              qrData.setFixedTipAmount(tagValue);
              break;
            case 57:
              qrData.setFixedTipPercentage(tagValue);
              break;
            case 58:
              qrData.setCountryCode(tagValue);
              break;
            case 59:
              qrData.setMerchantName(tagValue);
              break;
            case 60:
              qrData.setCityName(tagValue);
              break;
            case 62:
              qrData.setExtraFields(tagValue);
              qrData.setExtraFieldsList(getExtraFieldsFrom2QrStringTag62(tagValue));
              break;
          }
        }
        i++;
        Log.d("QR TAG", String.valueOf(i));

      }
      return qrData;
    } catch (Exception e) {
      Log.d("QR Exception", String.valueOf(e.getStackTrace()));
      return null;
    }

  }


  public QRScanModel getMerchantDetailOld(String result) {

    try {
      QRScanModel qrData = new QRScanModel();
      String sub = result.substring(2);
      Log.i("Vlaue", result.substring(2));
      String regex = "(\\d+)";
      matcher = Pattern.compile(regex).matcher(sub);
      if (matcher.find()) {
        String merchantI = matcher.group(1);
        qrData.setCard_No(merchantI.subSequence(0, merchantI.length() - 1).toString());
        String last = merchantI.substring(merchantI.length() - 2);

        if (String.valueOf(last.charAt(0)).equals("1")) {
          MerchantNameSub = sub.substring(merchantI.length());

        } else if (String.valueOf(last.charAt(1)).equals("1")) {
          MerchantNameSub = sub.substring(merchantI.length() + 1);
        }
        Matcher matcher1 = Pattern.compile("[a-zA-Z\\s\'\"]+").matcher(MerchantNameSub);
        if (matcher1.find()) {
          qrData.setMerchantName(matcher1.group(0));

          String MCC = MerchantNameSub.substring(matcher1.group(0).length());

          if (MCC.startsWith("2")) {
            String MCCode = MCC.substring(2);
            Log.i("MCCSUB", MCCode);
            Matcher matcher2 = Pattern.compile(regex).matcher(MCCode);
            if (matcher2.find()) {
              Log.i("MAtch", matcher2.group(1).subSequence(0, 4).toString());
              qrData.setMCC(matcher2.group(1).subSequence(0, 4).toString());

              String SubCountry = MCCode.substring(matcher2.group(1).subSequence(0, 4).toString().length());
              if (SubCountry.startsWith("3")) {
                String subLength = SubCountry.substring(2);
                Matcher matcher3 = Pattern.compile("[a-zA-Z\\s\'\"]+").matcher(subLength);
                if (matcher3.find()) {
                  qrData.setCityName(matcher3.group());
//                                    String Su = subLength.substring(merchantAddress.length());
//                                    if (Su.startsWith("4")) {
//                                        String INR = Su.substring(2);
//                                        String CurrencyCode = (String) INR.subSequence(0, 2);
//                                        if (INR.substring(CurrencyCode.length()).startsWith("5")) {
//                                            String CountryCurrency = INR.substring(CurrencyCode.length()).substring(2).subSequence(0, 3).toString();
//                                            if (INR.substring(CurrencyCode.length()).substring(5).length() > 0 && INR.substring(CurrencyCode.length()).substring(5).startsWith("6")) {
//
//                                                String Amount = INR.substring(CurrencyCode.length()).substring(7);
//                                                String currentAmount = String.valueOf(Amount.regionMatches(7, Amount, 0, 13));
//                                                Matcher matcher4 = Pattern.compile(regex).matcher(Amount);
//                                                if (matcher4.find()) {
//                                                    Log.i("lett", String.valueOf(matcher4.group().split("7").length));
//                                                    String[] s = matcher4.group().split("7");
//                                                }
//                                            } else {
////                                                    CustomToast.showMessage(getActivity(), "Currency Code ::" + CountryCurrency);
//                                            }
//
//                                        }
//                                    }

                }
              }
            }
          }
        }

      }
      return qrData;
    } catch (Exception e) {
      return null;
    }

  }

  //Version 1
  public QRScanModel getMerchantDetailsFromVersion1QrString(String qrString) {

    try {
      QRScanModel qrData = new QRScanModel();
      Integer mvisaIdHexLenth = Integer.valueOf(qrString.substring(1, 2), 16);
      String mvisaId = qrString.substring(2, mvisaIdHexLenth + 2);
      Log.i("mvisaId", mvisaId);

      qrData.setCard_No(mvisaId);

      Integer nameHexLength = Integer.valueOf(qrString.substring(mvisaIdHexLenth + 3, mvisaIdHexLenth + 4), 16);
      String merchantName = qrString.substring(mvisaIdHexLenth + 4, mvisaIdHexLenth + nameHexLength + 4);
      qrData.setMerchantName(merchantName);

      Integer mccLen = Integer.valueOf(
        qrString.substring(mvisaIdHexLenth + nameHexLength + 5, mvisaIdHexLenth + nameHexLength + 6), 16);


      Integer locLength = Integer.valueOf(qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + 7,
        mvisaIdHexLenth + nameHexLength + mccLen + 8), 16);


      Integer countryLen = Integer
        .valueOf(qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + 9,
          mvisaIdHexLenth + nameHexLength + mccLen + locLength + 10), 16);
      String country = qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + 10,
        mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 10);
      qrData.setCountryCode(country);

      Integer currencyLen = Integer
        .valueOf(qrString.substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 11,
          mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 12));
      String currencyCode = qrString.substring(
        mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + 12,
        mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 12);
      qrData.setIndianRupeeCode(currencyCode);

      String amountStr = qrString
        .substring(mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 12);

      Integer amountLen = 0;
      Double amount = null;

      Integer referenceNoLen = 0;
      String referenceNo = null;

      Map<String, Object> response = new HashMap<String, Object>();

      if (!amountStr.equals("") && !amountStr.startsWith("M2PY")
        && Integer.valueOf(amountStr.substring(0, 1)) == 6) {
        amountLen = Integer.valueOf(qrString.substring(
          mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 13,
          mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 14));
        amount = Double.valueOf(amountStr.substring(2, amountLen + 2));
        qrData.setTransAmount(amountStr.substring(2, amountLen + 2));
      }


      if (qrString.contains("M2PY")) {
        String terminalId = qrString.substring(
          mvisaIdHexLenth + nameHexLength + mccLen + locLength + countryLen + currencyLen + 18);
        qrData.setTerminalID(terminalId);
      }

      return qrData;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }


  public ArrayList<String> getExtraFieldsFrom2QrStringTag62(String qrString) {

    Integer tagPosition[] = {0};
    ArrayList<String> qrDataExtraFields = new ArrayList<>();
    try {
      int i = 0; //n = str.length(),

      while (i < 9) {

        int tagPosEndPos = tagPosition[0] + 2;
        if (qrString.length() >= tagPosEndPos) {
          String tagStr = qrString.substring(tagPosition[0], tagPosEndPos);
          String tagValue = "";
          if (Integer.valueOf(tagStr).equals(i)) {

            String tagLenStr = qrString.substring(tagPosition[0] + 2, tagPosition[0] + 2 + 2);
            tagValue = qrString.substring(tagPosition[0] + 2 + 2,
              tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr));
            tagPosition[0] = tagPosition[0] + 2 + 2 + Integer.valueOf(tagLenStr);
          }

          System.out.println("TAG STR :: " + tagStr + " ::: TAGVALUE ::" + tagValue);
          switch (Integer.valueOf(tagStr)) {

            case 1:
              if (tagValue.contains("***")) {
                qrDataExtraFields.add("01");
              }
              break;
            case 2:
              if (tagValue.contains("***")) {
                qrDataExtraFields.add("02");
              }
              break;
            case 3:
              if (tagValue.contains("***")) {
                qrDataExtraFields.add("03");
              }
              break;
            case 4:
              if (tagValue.contains("***")) {
                qrDataExtraFields.add("04");
              }
              break;
            case 5:
              if (tagValue.contains("***")) {
                qrDataExtraFields.add("05");
              }
              break;
            case 6:
              if (tagValue.contains("***")) {
                qrDataExtraFields.add("06");
              }
              break;
            case 8:
              if (tagValue.contains("***")) {
                qrDataExtraFields.add("08");
              }
              break;
          }
        }
        i++;
        Log.d("QR TAG", String.valueOf(i));

      }
      return qrDataExtraFields;
    } catch (Exception e) {
      Log.d("QR Exception", String.valueOf(e.getStackTrace()));
      return null;
    }

  }


  private String getExtraFieldData() {
    String extraField = "";
    for (int index = 0; index < (((ViewGroup) llExtraField).getChildCount()); ++index) {
      LinearLayout lvChild = (LinearLayout) ((ViewGroup) llExtraField).getChildAt(index);
      TextView tvChild = (TextView) ((ViewGroup) lvChild).getChildAt(0);
      EditText etChild = (EditText) ((ViewGroup) lvChild).getChildAt(2);

      extraField = extraField + tvChild.getText() + "=" + etChild.getText() + ";";
    }
    return extraField;
  }


}
