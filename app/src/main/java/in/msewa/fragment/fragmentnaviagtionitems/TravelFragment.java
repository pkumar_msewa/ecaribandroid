package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.fragment.fragmenttravel.BusTravelFragment;
import in.msewa.fragment.fragmenttravel.FlightTravelFragment;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BusCityModel;
import in.msewa.model.BusSaveModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 9/26/2016.
 */

public class TravelFragment extends Fragment {
    CharSequence TitlesEnglish[] = {"Bus", "Flight"};
    int NumbOfTabs = 4;
    private FragmentManager fragmentManager;
    private TabLayout mSlidingTabLayout;
    private LoadingDialog loadingDialog;
    private UserModel session = UserModel.getInstance();
    private List<BusCityModel> busCityModelList;
    private List<BusSaveModel> saveModels;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mobile_topup, container, false);
        ViewPager mainPager = (ViewPager) rootView.findViewById(R.id.mainPager);
        setupViewPager(mainPager);
        loadingDialog = new LoadingDialog(getActivity());
        busCityModelList = new ArrayList<>();

        mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayoutMobileTopUp);
        mSlidingTabLayout.setupWithViewPager(mainPager);
        mSlidingTabLayout.setOnTabSelectedListener(onTabSelectedListener(mainPager));

        return rootView;
    }


//    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
//
//        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
//        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
//
//
//        // Build a Constructor and assign the passed Values to appropriate values in the class
//        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
//            super(fm);
//            this.Titles = mTitles;
//            this.NumbOfTabs = mNumbOfTabsumb;
//
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            if (position == 0) {
//                return new BusTravelFragment();
//            }
//            else if (position == 1) {
//                return new FlightTravelFragment();
//            }
//            else {
//                return new BusTravelFragment();
//            }
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return Titles[position];
//        }
//
//        @Override
//        public int getCount() {
//            return Titles.length;
//        }
//    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager fragmentManager = getChildFragmentManager();
        ViewPagerAdapter adapter = new ViewPagerAdapter(fragmentManager);
        adapter.addFragment(new BusTravelFragment(), "Bus");
        adapter.addFragment(new FlightTravelFragment(), "Flight");
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mSlidingTabLayout));
        onTabSelectedListener(viewPager);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getCity() {
        try {
            loadingDialog.show();

            JSONObject jsonRequest = new JSONObject();
            try {
                jsonRequest.put("sessionId", session.getUserSessionId());

            } catch (JSONException e) {
                e.printStackTrace();
                jsonRequest = null;
            }

            if (jsonRequest != null) {
                Log.i("CityRequest", jsonRequest.toString());
//                JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_GET_CITY, jsonRequest, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
                AndroidNetworking.post(ApiUrl.URL_BUS_GET_CITY)
                        .addJSONObjectBody(jsonRequest) // posting json
                        .setTag("test")
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    loadingDialog.dismiss();
                                } catch (NullPointerException e) {

                                }
                                try {
                                    if (response.has("code")) {
                                        String code = response.getString("code");
                                        if (code != null && code.equals("S00")) {
                                            JSONArray citydata = response.getJSONArray("details");
                                            BusSaveModel.deleteAll(BusSaveModel.class);
                                            BusSaveModel busSaveModel = new BusSaveModel(citydata.toString());
                                            busSaveModel.save();
                                            for (int i = 0; i < citydata.length(); i++) {
                                                JSONObject c = citydata.getJSONObject(i);
                                                long cityId = c.getLong("cityId");
                                                String cityName = c.getString("cityName");
                                                BusCityModel cModel = new BusCityModel(cityId, cityName);
                                                busCityModelList.add(cModel);
                                            }
                                            if (busCityModelList.size() != 0) {
                                                ArrayList<BusCityModel> busCityModels = AppMetadata.getBusCities();
                                                for (int i = 0; i < busCityModelList.size(); i++) {
                                                    for (int j = 0; j < busCityModels.size(); j++) {
                                                        if (busCityModelList.get(i).getCityname().equalsIgnoreCase(busCityModels.get(j).getCityname())) {
                                                            Collections.swap(busCityModelList, j, i);
                                                        }

                                                    }
                                                }
//                                                getBookedTicketsList();
//                                        cityAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, cities);
//                                        spBusFrom.setAdapter(cityAdapter);
//                                        spBusTo.setAdapter(cityAdapter);
                                            }

                                        } else if (code != null && code.equals("F03")) {
//                                            showInvalidSessionDialog();

                                        } else {
                                            if (response.has("message") && response.getString("message") != null) {
                                                String message = response.getString("message");
                                                CustomToast.showMessage(getActivity(), message);
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                                    e.printStackTrace();
                                    try {
                                        loadingDialog.dismiss();
                                    } catch (NullPointerException e1) {

                                    }
                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                try {
                                    loadingDialog.dismiss();
                                } catch (NullPointerException e) {

                                }
                                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

                            }
                        });
//                {
//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        HashMap<String, String> map = new HashMap<>();
//                        map.put("hash", "1234");
//                        return map;
//                    }
//
//                };
//                int socketTimeout = 60000;
//                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                postReq.setRetryPolicy(policy);
//                PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
            }
        } catch (NullPointerException e) {

        }
    }

}

