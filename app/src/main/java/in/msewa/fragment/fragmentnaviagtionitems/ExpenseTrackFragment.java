package in.msewa.fragment.fragmentnaviagtionitems;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.ReceiptAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadMoreListView;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;

import in.msewa.model.ExpenseTrackerModel;
import in.msewa.model.StatementModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.NetworkErrorHandler;

/**
 * Created by Ksf on 5/8/2016.
 */
public class ExpenseTrackFragment extends Fragment implements OnChartValueSelectedListener {
  private View rootView;
  private ReceiptAdapter receiptAdapter;
  private LoadMoreListView lvReceipt;
  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;
  private ArrayList<StatementModel> receiptList;
  private ProgressBar pbReceipt;
  private LinearLayout llNoReceipt;
  private TextView tvNoReceipt;
  private LoadingDialog loadingDialog;
  //Volley
  private String tag_json_obj = "json_receipt";
  private JsonObjectRequest postReq;
  //Expense Tracker
  private PieChart mChart;
  //    private SeekBar mSeekBarX, mSeekBarY;
  protected Typeface tf;
  protected Typeface mTfRegular;
  private MaterialEditText etDateFrom, etDateTo;
  private Button btnSearch;
  private LinearLayout llInputDate;
  private ImageView ivFilter;
  private TextView tvTitle;
  private ExpenseTrackerModel expenseTrackerModel;
  private ArrayList<ExpenseTrackerModel> expenseTrackerModelArrayList = new ArrayList<>();

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    receiptList = new ArrayList<>();
    loadingDialog = new LoadingDialog(getActivity());
    mTfRegular = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Regular.ttf");
//        mTfLight = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Light.ttf");
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_expense_tracker, container, false);
    mChart = (PieChart) rootView.findViewById(R.id.chart1);
    llInputDate = (LinearLayout) rootView.findViewById(R.id.llInputDate);
    ivFilter = (ImageView) rootView.findViewById(R.id.ivFilter);
    etDateFrom = (MaterialEditText) rootView.findViewById(R.id.etDateFrom);
    etDateTo = (MaterialEditText) rootView.findViewById(R.id.etDateTo);
    tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);

    ivFilter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ivFilter.setVisibility(View.GONE);
        llInputDate.setVisibility(View.VISIBLE);
      }
    });
    btnSearch = (Button) rootView.findViewById(R.id.btnSearch);
    btnSearch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        submitForm();
      }
    });
    etDateFrom.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        if (etDateFrom.getText().toString() != null && !etDateFrom.getText().toString().isEmpty()) {
          String[] dateSplit = etDateFrom.getText().toString().split("-");

          int mYear = Integer.valueOf(dateSplit[2]);
          int mMonth = Integer.valueOf(dateSplit[1]) - 1;
          int mDay = Integer.valueOf(dateSplit[0]);

          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0" + dayOfMonth;
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etDateFrom.setText(String.valueOf(year) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(formattedDay));

            }
          }, mYear, mMonth, mDay);
          dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
          dialog.setCustomTitle(null);
          dialog.setTitle("Select Start Date");
          dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
          dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

          {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              dialogInterface.dismiss();
            }
          });
          dialog.show();
        } else {
          final Calendar c = Calendar.getInstance();
          int mYear = c.get(Calendar.YEAR);
          int mMonth = c.get(Calendar.MONTH);
          int mDay = c.get(Calendar.DAY_OF_MONTH);

          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0" + dayOfMonth;
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etDateFrom.setText(String.valueOf(year) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(formattedDay));
            }
          }, mYear, mMonth, mDay);

          dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
          dialog.setTitle("Select Start Date");
          dialog.setCustomTitle(null);
          dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
          dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

          {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              dialogInterface.dismiss();
            }
          });
          dialog.show();
        }

      }
    });

    etDateTo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        if (etDateTo.getText().toString() != null && !etDateTo.getText().toString().isEmpty()) {
          String[] dateSplit = etDateTo.getText().toString().split("-");

          int mYear = Integer.valueOf(dateSplit[2]);
          int mMonth = Integer.valueOf(dateSplit[1]) - 1;
          int mDay = Integer.valueOf(dateSplit[0]);

          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0" + dayOfMonth;
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etDateTo.setText(String.valueOf(year) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(formattedDay));

            }
          }, mYear, mMonth, mDay);
          dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
          dialog.setCustomTitle(null);
          dialog.setTitle("Select Ending Date");
          dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
          dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

          {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              dialogInterface.dismiss();
            }
          });
          dialog.show();
        } else {
          final Calendar c = Calendar.getInstance();
          int mYear = c.get(Calendar.YEAR);
          int mMonth = c.get(Calendar.MONTH);
          int mDay = c.get(Calendar.DAY_OF_MONTH);

          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0" + dayOfMonth;
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etDateTo.setText(String.valueOf(year) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(formattedDay));
            }
          }, mYear, mMonth, mDay);

          dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
          dialog.setTitle("Select Ending Date");
          dialog.setCustomTitle(null);
          dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
          dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

          {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              dialogInterface.dismiss();
            }
          });
          dialog.show();
        }

      }
    });
//        SimpleDateFormat formatDay, formatMonth;
//        formatDay = new SimpleDateFormat("dd");
//        formatMonth = new SimpleDateFormat("mm");
    final Calendar c = Calendar.getInstance();
    int mYear = c.get(Calendar.YEAR);
    int mMonth = c.get(Calendar.MONTH);
    int mDay = c.get(Calendar.DAY_OF_MONTH);
    int pMonth = mMonth - 1;
    if (pMonth == 0) {
      pMonth = 12;
      mYear--;
    }
//    String formattedDay, formattedMonth, formattedPMonth;
//    if (mDay < 10) {
//      formattedDay = "0" + mDay;
//    } else {
//      formattedDay = mDay + "";
//    }
//
//    if ((mMonth + 1) < 10) {
//      formattedMonth = "0" + String.valueOf(mMonth + 1);
//    } else {
//      formattedMonth = String.valueOf(mMonth + 1) + "";
//    }
//    if ((pMonth + 1) < 10) {
//      formattedPMonth = "0" + String.valueOf(pMonth + 1);
//    } else {
//      formattedPMonth = String.valueOf(pMonth + 1) + "";
//    }

//        String to = formattedDay + "-" + formattedMonth + "-" + mYear;
//        String from = formattedDay + "-" + formattedPMonth + "-" + mYear;
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
    String strDate = mdformat.format(calendar.getTime());
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MONTH, -1);


//        getUserExpenses("2017-04-01","2017-06-01");
    getUserExpenses(mdformat.format(cal.getTime()).trim(), strDate.trim());
    return rootView;
  }

  private void showPieChart(ArrayList<ExpenseTrackerModel> expenseTrackerModel) {


    mChart.getDescription().setEnabled(false);
    mChart.setExtraOffsets(20, 20, 20, 20);
    mChart.setDragDecelerationFrictionCoef(0.95f);

    tf = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Regular.ttf");

    mChart.setCenterTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Light.ttf"));
    mChart.setCenterText(generateCenterSpannableText());

//        mChart.setExtraOffsets(20.f, 0.f, 20.f, 0.f);

    mChart.setDrawHoleEnabled(true);
    mChart.setHoleColor(Color.WHITE);
    mChart.setEntryLabelColor(Color.BLACK);
    mChart.setTransparentCircleColor(Color.WHITE);
    mChart.setTransparentCircleAlpha(110);
    mChart.setHoleRadius(58f);
    mChart.setTransparentCircleRadius(61f);

    mChart.setDrawCenterText(true);

    mChart.setRotationAngle(0);
    // enable rotation of the chart by touch
    mChart.setRotationEnabled(true);
    mChart.setHighlightPerTapEnabled(true);

//         mChart.setUnit(" €");
    // mChart.setDrawUnitsInChart(true);

    // add a selection listener
    mChart.setOnChartValueSelectedListener(this);

    setData(expenseTrackerModel);


    mChart.animateXY(2000, 2000);
    // mChart.spin(2000, 0, 360);

    Legend l = mChart.getLegend();
    l.setWordWrapEnabled(true);
    l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
    l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
    l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
    l.setDrawInside(false);
    l.setEnabled(true);
  }

  private void setData(ArrayList<ExpenseTrackerModel> expenseTrackerModelArrayList1) {

    ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
    for (int i = 0; i < expenseTrackerModelArrayList1.size(); i++) {
      entries.add(new PieEntry(expenseTrackerModelArrayList1.get(i).getServiceAmount(), expenseTrackerModelArrayList1.get(i).getServiceName()));
    }

//    for (int i = 0; i < expenseTrackerModelArrayList1.s; i++) {
//      entries.add(new PieEntry((float) ((Math.random() * 6) + 6 / 5),
//        expenseTrackerModelArrayList1.get(i).getServiceName(),
//        getResources().getDrawable(R.drawable.star)));
//    }
    // NOTE: The order of the entries when being added to the entries array determines their position around the center of
    // the chart.
    PieDataSet dataSet = new PieDataSet(entries, "Expense Results");

    dataSet.setSliceSpace(0);
    dataSet.setSelectionShift(5f);
    dataSet.setValueTextColor(R.color.navigationBarColor);
    // add a lot of colors

    ArrayList<Integer> colors = new ArrayList<Integer>();

    final int[] Custom_Colors = {
      Color.rgb(10, 159, 140), Color.rgb(140, 200, 78), Color.rgb(255, 192, 0),
      Color.rgb(243, 88, 53)
    };

    for (int c : Custom_Colors)
      colors.add(c);
    for (int c : ColorTemplate.LIBERTY_COLORS)
      colors.add(c);

//        for (int c : ColorTemplate.PASTEL_COLORS)
//            colors.add(c);

    colors.add(ColorTemplate.getHoloBlue());

    dataSet.setColors(colors);
    //dataSet.setSelectionShift(0f);


    dataSet.setValueLinePart1OffsetPercentage(80.f);
    dataSet.setValueLinePart1Length(0.2f);
    dataSet.setValueLinePart2Length(0.4f);
//        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
    dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

    PieData data = new PieData(dataSet);
    data.setValueFormatter(new LargeValueFormatter());
    data.setValueTextSize(14f);
    data.setValueTextColor(Color.BLACK);

    data.setValueTypeface(tf);
    mChart.setData(data);

    // undo all highlights
    mChart.highlightValues(null);

    mChart.invalidate();
  }

  private SpannableString generateCenterSpannableText() {

    SpannableString s = new SpannableString("Your Expenses.\n developed by Msewa Software");
    s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
    s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
    s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
    s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
    s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
    s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
    return s;
  }

  private void submitForm() {

    if (!validateFromDate()) {
      return;
    }
    if (!validateToDate()) {
      return;
    }
    getUserExpenses(etDateFrom.getText().toString(), etDateTo.getText().toString());
  }

  //2017-06-01
  public void getUserExpenses(final String dFrom, final String dTo) {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    final String fromDate = dFrom;
    final String toDate = dTo;
    try {


      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("fromDate", fromDate);
      jsonRequest.put("toDate", toDate);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("valuee;kbdkjgv", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_EXPENSE_TRACKER, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject JsonObj) {
          try {
            Log.i("responseee", JsonObj.toString());
            String message = JsonObj.getString("message");
            String code = JsonObj.getString("code");

            if (code != null && code.equals("S00")) {
              expenseTrackerModelArrayList.clear();
              String jsonString = JsonObj.getString("response");
              JSONObject responseObj = new JSONObject(jsonString);
              JSONObject details = responseObj.getJSONObject("details");

              JSONArray creditArr = details.getJSONArray("credit");
              for (int i = 0; i < creditArr.length(); i++) {
                JSONObject c = creditArr.getJSONObject(i);
                String statusCode = c.getString("code");
                if (statusCode.equals("S00")) {
                  String name = c.getString("serviceName");
                  float amount = (float) c.getDouble("amount");
                  expenseTrackerModel = new ExpenseTrackerModel(name, amount);
                  expenseTrackerModelArrayList.add(expenseTrackerModel);
                }

              }

              JSONArray debitArr = details.getJSONArray("debit");
              for (int i = 0; i < debitArr.length(); i++) {
                JSONObject c = debitArr.getJSONObject(i);
                String statusCode = c.getString("code");
                if (statusCode.equals("S00")) {
                  String name;
                  float amount;
                  if (c.getString("serviceName").contains("QwikrPay")) {
                    name = "Shopping";
                    amount = (float) c.getDouble("amount");
                  } else {
                    name = c.getString("serviceName");
                    amount = (float) c.getDouble("amount");
                  }

                  expenseTrackerModel = new ExpenseTrackerModel(name, amount);
                  expenseTrackerModelArrayList.add(expenseTrackerModel);
                }
              }


              JSONArray debitTopUpArr = details.getJSONArray("DebitTopUp");
              for (int i = 0; i < debitTopUpArr.length(); i++) {
                JSONObject c = debitTopUpArr.getJSONObject(i);
                String statusCode = c.getString("code");
                if (statusCode.equals("S00")) {
                  String name = "Topup & Bill Pay";
                  float amount = (float) c.getDouble("amount");
                  expenseTrackerModel = new ExpenseTrackerModel(name, amount);
                  expenseTrackerModelArrayList.add(expenseTrackerModel);
                }
              }


              //format("dd-MM-yyyy");
//                            SimpleDateFormat writeFormat = new SimpleDateFormat("dd MMMM,yyyy");
//                            Date formatDate = new Date();
//                            try {
//                                Date rawDate = readFormat.parse(dFrom);
//                                formatDate = writeFormat.parse(String.valueOf(rawDate));
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
              tvTitle.setText("Showing results from " + dFrom + " to " + dTo);
              if (llInputDate.getVisibility() == View.VISIBLE) {
                llInputDate.setVisibility(View.GONE);
                ivFilter.setVisibility(View.VISIBLE);
              }
              loadingDialog.dismiss();
              if (!expenseTrackerModelArrayList.isEmpty()) {
                showPieChart(expenseTrackerModelArrayList);
              } else {
                expenseTrackerModel = new ExpenseTrackerModel("", 0);
                expenseTrackerModelArrayList.add(expenseTrackerModel);
                showPieChart(expenseTrackerModelArrayList);
                CustomToast.showMessage(getActivity(), "No Transaction found on selected dates.");
              }


            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog();
            } else {
              loadingDialog.dismiss();
              Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }

          } catch (JSONException e) {
            e.printStackTrace();
            loadingDialog.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          try {
            tvNoReceipt.setText(NetworkErrorHandler.getMessage(error, getActivity()));
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;

      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }

  }

  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }

  private boolean validateFromDate() {
    if (etDateFrom.getText().toString().trim().isEmpty()) {
      etDateFrom.setError("Enter Starting Date");
      requestFocus(etDateFrom);
      return false;
    }
    return true;
  }

  private boolean validateToDate() {
    if (etDateTo.getText().toString().trim().isEmpty()) {
      etDateTo.setError("Enter Ending Date");
      requestFocus(etDateTo);
      return false;
    }
    return true;
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void onDetach() {
    postReq.cancel();
    super.onDetach();
  }

  @Override
  public void onValueSelected(Entry e, Highlight h) {
    if (e == null)
      return;
    Log.i("VAL SELECTED",
      "Value: " + e.getY() + ", index: " + h.getX()
        + ", DataSet index: " + h.getDataSetIndex());
  }

  @Override
  public void onNothingSelected() {
    Log.i("PieChart", "nothing selected");
  }
}
