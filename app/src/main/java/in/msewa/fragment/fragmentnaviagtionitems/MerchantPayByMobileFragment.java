package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.activity.HomeMainActivity;

/**
 * Created by Kashif-PC on 12/2/2016.
 */
public class MerchantPayByMobileFragment extends Fragment {

    private MaterialEditText etMerchantId;
    private MaterialEditText etMerchantPayAmount;

    private LoadingDialog loadDlg;
    private UserModel session = UserModel.getInstance();
    private RequestQueue rq;
    private boolean cancel;
    private View focusView = null;

    //Variables
    private String amount;
    private JSONObject jsonRequest;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
            rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_merchant_pay_byno, container, false);
        Button btnMerchantPay = (Button) rootView.findViewById(R.id.btnMerchantPay);
        etMerchantPayAmount = (MaterialEditText) rootView.findViewById(R.id.etMerchantPayAmount);
        etMerchantId = (MaterialEditText) rootView.findViewById(R.id.etMerchantId);
showSuccessDialog();
        btnMerchantPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });
        return rootView;
    }

    private void attemptPayment() {
        etMerchantPayAmount.setError(null);
        etMerchantId.setError(null);
        cancel = false;

        amount = etMerchantPayAmount.getText().toString();
        checkPayAmount(amount);
        checkPhone(etMerchantId.getText().toString());

        if (cancel) {
            focusView.requestFocus();
        } else {
            showCustomDialog();

        }
    }

    private void checkPhone(String phNo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
        if (!gasCheckLog.isValid) {
            etMerchantId.setError(getString(gasCheckLog.msg));
            focusView = etMerchantId;
            cancel = true;
        }
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etMerchantPayAmount.setError(getString(gasCheckLog.msg));
            focusView = etMerchantPayAmount;
            cancel = true;
        } else if (Integer.valueOf(etMerchantPayAmount.getText().toString()) < 10) {
            etMerchantPayAmount.setError(getString(R.string.lessAmount));
            focusView = etMerchantPayAmount;
            cancel = true;
        }
    }

    public void showCustomDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(generateMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(generateMessage());
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                loadDlg.show();
                promoteMerchantPay();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public String generateMessage() {
        return "<b><font color=#000000> Merchant no.: </font></b>" + "<font color=#000000>" + etMerchantId.getText().toString() + "</font><br>" +
                "<b><font color=#000000> Amount: </font></b>" + "<font>" + amount + "</font><br><br>" +
                "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    }

    public void promoteMerchantPay() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("id", etMerchantId.getText().toString());
            jsonRequest.put("netAmount", etMerchantPayAmount.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PAY_AT_STORE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Merchant Pay Money", response.toString());
                    try {
                        Log.i("Merchant Pay Response", response.toString());
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
//                            sendRefresh();
                            showSuccessDialog();
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog(response.getString("response"));
                        } else {
                            loadDlg.dismiss();
                            String jsonString = response.getString("response");
                            JSONObject jsonObject = new JSONObject(jsonString);
                            String errorMessage = jsonObject.getString("message");
                            CustomToast.showMessage(getActivity(), errorMessage);
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
          PayQwikApplication.getInstance().addToRequestQueue(postReq,"test");
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog(String message) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(message, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(message);
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }
    public void showSuccessDialog() {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Payment Successful", "Your transaction had been completed successfully. \n Thank you for using VPayQwik.");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                getActivity().finish();
                Intent i = new Intent(getActivity(), HomeMainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(i);
            }
        });
        builder.show();
    }

}
