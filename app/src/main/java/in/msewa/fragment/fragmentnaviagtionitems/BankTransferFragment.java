package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomAlertDialogBankSearch;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyMPinDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BankListModel;
import in.msewa.model.BusCityModel;
import in.msewa.model.TravelCityModel;
import in.msewa.model.UserModel;
import in.msewa.util.CitySelectedListener;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.CheckLog;
import in.msewa.util.MPinVerifiedListner;
import in.msewa.util.PayingDetailsValidation;

/**
 * Created by Ksf on 6/24/2016.
 */


public class BankTransferFragment extends Fragment implements MPinVerifiedListner {
  //Views
  private View rootView;
  private MaterialEditText etBankTransferAccHolderName;
  private MaterialEditText etBankTransferAccNo;
  private MaterialEditText etBankTransferIFSC;
  private MaterialEditText etBankTransferAmount;
  private MaterialEditText etBankTransferBankName;
  private Button btnBankTransfer;

  private UserModel session = UserModel.getInstance();
  private LoadingDialog loadDlg;
  private View focusView = null;
  private boolean cancel;

  //Volley Tag
  private String tag_json_obj = "json_bank_transfer";
  private JSONObject jsonRequest;

  //Variables
  private String accHolderName, accIFSCCode, accAmount, accBankName;
  private String bankCode = "", accNo = "";


  private List<BankListModel> bankListModelList;
  private GetBank getBankTask = null;
  private ArrayList<String> ifscList;

  private TextView tvBankTransferInfo, etBankTransferConfromAccNo;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
    bankListModelList = Select.from(BankListModel.class).list();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_bank_transfer, container, false);
    etBankTransferAccHolderName = (MaterialEditText) rootView.findViewById(R.id.etBankTransferAccHolderName);
    etBankTransferAccNo = (MaterialEditText) rootView.findViewById(R.id.etBankTransferAccNo);
    etBankTransferIFSC = (MaterialEditText) rootView.findViewById(R.id.etBankTransferIFSC);
    etBankTransferAmount = (MaterialEditText) rootView.findViewById(R.id.etBankTransferAmount);
    etBankTransferBankName = (MaterialEditText) rootView.findViewById(R.id.etBankTransferBankName);
    tvBankTransferInfo = (TextView) rootView.findViewById(R.id.tvBankTransferInfo);
    etBankTransferConfromAccNo = (MaterialEditText) rootView.findViewById(R.id.etBankTransferConfromAccNo);
    btnBankTransfer = (Button) rootView.findViewById(R.id.btnBankTransfer);
    etBankTransferBankName.setFocusable(false);
    etBankTransferIFSC.setFocusable(false);


    etBankTransferAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          attemptPayment();
        }
        return false;
      }
    });

    if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
      tvBankTransferInfo.setVisibility(View.GONE);
      etBankTransferAccNo.setFocusable(true);
      etBankTransferAccHolderName.setFocusable(true);
      etBankTransferAmount.setFocusable(true);
      etBankTransferIFSC.setFocusable(true);
      etBankTransferConfromAccNo.setFocusable(true);
      btnBankTransfer.setClickable(true);
      btnBankTransfer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          attemptPayment();
        }
      });


    } else {
      tvBankTransferInfo.setText(getResources().getString(R.string.bank_transfer_upgrade));
      tvBankTransferInfo.setVisibility(View.VISIBLE);
      btnBankTransfer.setClickable(false);
      btnBankTransfer.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.light_text));
      etBankTransferAccNo.setFocusable(false);
      etBankTransferAccHolderName.setFocusable(false);
      etBankTransferAmount.setFocusable(false);
      etBankTransferConfromAccNo.setFocusable(false);
      etBankTransferIFSC.setFocusable(false);
    }

//        etBankTransferBankName.setText("Vijaya Bank");
//    getIFSCList(bankCode);
//
//    getBankTask = new GetBank();
//    getBankTask.execute();
    if (bankListModelList != null && bankListModelList.size() != 0) {

    } else {
      getBankList();
    }
//
    etBankTransferBankName.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (bankListModelList == null || bankListModelList.size() == 0) {
          CustomToast.showMessage(getActivity(), "Sorry error occured please try again");
        } else {
          showBankDialog();
        }

      }
    });

//    etBankTransferIFSC.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        if (!etBankTransferBankName.getText().toString().isEmpty()) {
//          if (ifscList == null || ifscList.size() == 0) {
//            CustomToast.showMessage(getActivity(), "Error fetching ifsc, please try again later");
//          } else {
//            showIFSCDialog();
//          }
//        } else {
//          CustomToast.showMessage(getActivity(), "Select Bank first");
//        }
//      }
//    });
//
//
    return rootView;
  }

  private void attemptPayment() {

    etBankTransferAccHolderName.setError(null);
    etBankTransferAccNo.setError(null);
    etBankTransferIFSC.setError(null);
    etBankTransferAmount.setError(null);
    etBankTransferBankName.setError(null);

    cancel = false;

    accHolderName = etBankTransferAccHolderName.getText().toString();
    accNo = etBankTransferAccNo.getText().toString();
    accIFSCCode = etBankTransferIFSC.getText().toString();
    accAmount = etBankTransferAmount.getText().toString();
    accBankName = etBankTransferBankName.getText().toString();

    checkPayAmount(accAmount);
    checkBankName(accBankName);
    checkHolderName(accHolderName);
    checkIFSCCode(accIFSCCode);
    checkConfromAccNo(etBankTransferConfromAccNo.getText().toString());
    checkAccNo(accNo);
    if (!accNo.equalsIgnoreCase("")) {
      if (!accNo.equalsIgnoreCase(etBankTransferConfromAccNo.getText().toString())) {
        cancel = true;
        etBankTransferConfromAccNo.setError("Account Number Mismatch");
        focusView = etBankTransferBankName;
      }
//      else {
//        cancel = false;
//      }
    }
    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();

    }
  }


  public void showCustomDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        if (session.isMPin() && Integer.valueOf(accAmount) >= getResources().getInteger(R.integer.transactionValue)) {
          showMPinDialog();
        } else {
          loadDlg.show();
          promoteBankTransfer();
        }
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showMPinDialog() {
    VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
  }

  public String generateMessage() {
    Double amount = Double.parseDouble(accAmount);
    Double totalAmount = amount + amount * 0.02;
    return "<b><font color=#000000>Acc Holder Name: </font></b>" + "<font color=#000000>" + accHolderName + "</font><br>" +
      "<b><font color=#000000> Receiver Acc No: </font></b>" + "<font>" + accNo + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + accAmount + "</font><br>" +
      "<b><font color=#000000> Convenience Charge(2%): </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount * 0.02 + "</font><br>" +
      "<b><font color=#000000> Total Payable Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + totalAmount + "</font><br>" +
      "<b><font color=#000000> Current Balance: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + session.getUserBalance() + "</font><br>" +
      "<b><font color=#000000> Note: </font></b><font> Bank transfer request initiated today will reflect back into beneficiary account on next working day.</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
  }

  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etBankTransferAmount.setError(getString(gasCheckLog.msg));
      focusView = etBankTransferAmount;
      cancel = true;
    } else if (Integer.valueOf(etBankTransferAmount.getText().toString()) < 500 || Integer.valueOf(etBankTransferAmount.getText().toString()) > 5000) {
      etBankTransferAmount.setError("Amount should be between 500 to 5000");
      focusView = etBankTransferAmount;
      cancel = true;
    }
  }

  private void checkBankName(String name) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
    if (!gasCheckLog.isValid) {
      etBankTransferBankName.setError(getString(gasCheckLog.msg));
      focusView = etBankTransferBankName;
      cancel = true;
    }
  }

  private void checkAccNo(String accNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkBankAccountNumber(accNo);
    if (!gasCheckLog.isValid) {
      etBankTransferAccNo.setError(getString(gasCheckLog.msg));
      focusView = etBankTransferAccNo;
      cancel = true;
    }
  }

  private void checkConfromAccNo(String accNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkBankAccountNumber(accNo);
    if (!gasCheckLog.isValid) {
      etBankTransferConfromAccNo.setError(getString(gasCheckLog.msg));
      focusView = etBankTransferConfromAccNo;
      cancel = true;
    }
  }


  private void checkIFSCCode(String ifscCode) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(ifscCode);
    if (!gasCheckLog.isValid) {
      etBankTransferIFSC.setError(getString(gasCheckLog.msg));
      focusView = etBankTransferIFSC;
      cancel = true;
    }
  }


  private void checkHolderName(String holderName) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(holderName);
    if (!gasCheckLog.isValid) {
      etBankTransferAccHolderName.setError(getString(gasCheckLog.msg));
      focusView = etBankTransferAccHolderName;
      cancel = true;
    }
  }


  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void verifiedCompleted() {
    promoteBankTransfer();
  }

  @Override
  public void sessionInvalid(String message) {
    showInvalidSessionDialog();
  }


  public void promoteBankTransfer() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("ifscCode", etBankTransferIFSC.getText().toString());
      jsonRequest.put("bankCode", bankCode);
      jsonRequest.put("accountNumber", etBankTransferAccNo.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("amount", etBankTransferAmount.getText().toString());
      jsonRequest.put("accountName", etBankTransferAccHolderName.getText().toString());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_BANK_TRANSFER)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .addHeaders(new HashMap<String, String>().put("hash", "1234"))
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject response) {
            Log.i("Send Money Bank", response.toString());
            try {
              String code = response.getString("code");
              if (code != null && code.equals("S00")) {
                etBankTransferIFSC.getText().clear();
                etBankTransferAccHolderName.getText().clear();
                etBankTransferBankName.getText().clear();
                etBankTransferAmount.getText().clear();
                etBankTransferAccNo.getText().clear();

                loadDlg.dismiss();
                showSuccessDialog(response.getString("message"));
//                            if (response.has("message") && response.getString("message") != null) {
//                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
//                            } else {
//                                CustomToast.showMessage(getActivity(), "Error message is null");
//                            }
              } else if (code != null && code.equals("F03")) {
                loadDlg.dismiss();
                showInvalidSessionDialog();
              } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
                loadDlg.dismiss();
                showBlockDialog();
              } else {
                loadDlg.dismiss();
                if (response.has("message") && response.getString("message") != null) {
                  String message = response.getString("message");
                  CustomToast.showMessage(getActivity(), message);
                } else {
                  CustomToast.showMessage(getActivity(), "Error message is null");
                }
              }

            } catch (JSONException e) {
              loadDlg.dismiss();
              CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError anError) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          }
        });

    }
  }


  private class GetBank extends AsyncTask<Void, Void, Void> {
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      loadDlg.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
      getBankList();
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      etBankTransferBankName.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (bankListModelList == null || bankListModelList.size() == 0) {
            CustomToast.showMessage(getActivity(), "Error fetching bank list, please try again later");
          } else {
            showBankDialog();
          }
        }
      });

      etBankTransferIFSC.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (!etBankTransferBankName.getText().toString().isEmpty()) {
          } else {
            CustomToast.showMessage(getActivity(), "Select Bank first");
          }
        }
      });
      loadDlg.dismiss();
    }
  }

  public void getBankList() {
    loadDlg.show();
    StringRequest jsonObjReq = new StringRequest(Request.Method.GET, ApiUrl.URL_LIST_BANK,
      new Response.Listener<String>() {
        @Override
        public void onResponse(String res) {
          loadDlg.dismiss();
          try {
            BankListModel.deleteAll(BankListModel.class);
            bankListModelList.clear();
            JSONObject response = new JSONObject(res);
            String bankList = response.getString("details");
            JSONArray bankArray = new JSONArray(bankList);
            for (int i = 0; i < bankArray.length(); i++) {
              JSONObject c = bankArray.getJSONObject(i);
              String bankCode = c.getString("code");
              String bankName = c.getString("name");
              BankListModel blModel = new BankListModel(bankCode, bankName);
              blModel.save();

              bankListModelList.add(blModel);
            }

          } catch (JSONException e) {
            e.printStackTrace();
          }

        }
      }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        loadDlg.dismiss();

      }
    }

    ) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("hash", "123");
        return map;
      }

    };
    PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
  }


  public void getIFSCList(String bankCode) {
    loadDlg.show();
    AndroidNetworking.get(ApiUrl.URL_LIST_IFSC + bankCode)
      .setTag("test")
      .setPriority(Priority.HIGH)
      .build().getAsString(new StringRequestListener() {
      @Override
      public void onResponse(String res) {
        try {
          JSONObject response = new JSONObject(res);
          ifscList = new ArrayList<>();
          String ifscListString = response.getString("details");

          JSONArray jsonArray = new JSONArray(ifscListString);
          String[] strArr = new String[jsonArray.length()];

          for (int i = 0; i < jsonArray.length(); i++) {
            strArr[i] = jsonArray.getString(i);
            ifscList.add(strArr[i]);
          }
          loadDlg.dismiss();
        } catch (JSONException e) {
          e.printStackTrace();
        }

      }

      @Override
      public void onError(ANError anError) {
        loadDlg.dismiss();
        CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception));

      }
    });

  }

  private boolean getString(String value) {
    for (String s : ifscList) {
      if (s.equalsIgnoreCase(value)) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }


  @Override
  public void onResume() {
    super.onResume();
    PayQwikApplication.getInstance().trackScreenView("Bank transfer");
  }

  private void showBankDialog() {
    final ArrayList<String> bankVal = new ArrayList<>();
    ArrayList<BankListModel> busCityModels = AppMetadata.getBankList();
    for (int i = 0; i < bankListModelList.size(); i++) {
      for (int j = 0; j < busCityModels.size(); j++) {
        if (bankListModelList.get(i).getBankName().equalsIgnoreCase(busCityModels.get(j).getBankName())) {
          Collections.swap(bankListModelList, j, i);
        }

      }
    }
    etBankTransferIFSC.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (!s.toString().startsWith(bankCode)) {
          etBankTransferIFSC.setText(bankCode);
          Selection.setSelection(etBankTransferIFSC.getText(), etBankTransferIFSC.getText().length());
        }

      }
    });
    CustomAlertDialogBankSearch customAlertDialogBankSearch = new CustomAlertDialogBankSearch(getActivity(), bankListModelList, "", new CitySelectedListener() {
      @Override
      public void citySelected(String type, long cityCode, String cityName) {

      }

      @Override
      public void cityFilter(BusCityModel type) {

      }

      @Override
      public void bankFilter(BankListModel type) {
        etBankTransferBankName.setText(type.getBankName());
        etBankTransferIFSC.getText().clear();
        bankCode = type.getBankCode();
        etBankTransferIFSC.setText(bankCode);
        etBankTransferIFSC.setFocusable(true);
        etBankTransferIFSC.setFocusableInTouchMode(true);
        Selection.setSelection(etBankTransferIFSC.getText(), etBankTransferIFSC.getText().length());
//        getIFSCList(bankCode);
      }

      @Override
      public void travelFilter(TravelCityModel type) {

      }
    });
    customAlertDialogBankSearch.show();
  }

  private void showIFSCDialog() {
    android.support.v7.app.AlertDialog.Builder ifscDialog =
      new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
    ifscDialog.setTitle("Select IFSC for the bank");

    ifscDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });

    ifscDialog.setItems(ifscList.toArray(new String[ifscList.size()]),
      new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int i) {
          etBankTransferIFSC.setText(ifscList.get(i));
        }
      });
    ifscDialog.show();
  }


  public void showSuccessDialog(String message) {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(message), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage(message));
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), message, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        getActivity().finish();
        sendRefresh();
      }
    });
    builder.show();
  }

  public String getSuccessMessage(String message) {
    String source =
      "<b><font color=#000000>Acc Holder Name: </font></b>" + "<font color=#000000>" + accHolderName + "</font><br>" +
        "<b><font color=#000000> Receiver Acc No: </font></b>" + "<font>" + accNo + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + accAmount + "</font><br>";
//        "<b><font color=#000000> message: </font></b>" + "<font>" + message + "</font><br>";
    return source;
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        getActivity().finish();
      }
    });
    builder.show();
  }
}
