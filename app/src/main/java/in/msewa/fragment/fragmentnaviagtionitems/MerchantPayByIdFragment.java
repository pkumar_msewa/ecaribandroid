package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.MerchantModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.util.TLSSocketFactory;

/**
 * Created by Ksf on 3/27/2016.
 */
public class MerchantPayByIdFragment extends Fragment {
  private View rootView;
  private MaterialEditText etMerchantId;
  private MaterialEditText etMerchantPayAmount;
  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();
  private RequestQueue rq;
  private boolean cancel;
  private View focusView = null;

  //Variables

  private List<MerchantModel> merchantList;
  private String amount;
  private long merchantId;
  private JSONObject jsonRequest;
  private String responseMessage;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    try {
      final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
      rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
    } catch (KeyManagementException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    loadDlg = new LoadingDialog(getActivity());
    responseMessage = "Please wait loading merchant list";

    merchantList = Select.from(MerchantModel.class).list();
    if (merchantList == null || merchantList.size() == 0) {
      loadDlg.show();
    }
    loadMerchant();
  }


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_merchant_pay_byid, container, false);
    Button btnMerchantPay = (Button) rootView.findViewById(R.id.btnMerchantPay);
    etMerchantPayAmount = (MaterialEditText) rootView.findViewById(R.id.etMerchantPayAmount);
    etMerchantId = (MaterialEditText) rootView.findViewById(R.id.etMerchantId);

    etMerchantId.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (merchantList == null || merchantList.size() == 0) {
          CustomToast.showMessage(getActivity(), responseMessage);
        } else {
          showMerchantDialog();
        }
      }
    });

    btnMerchantPay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });
    return rootView;
  }


  private void attemptPayment() {
    etMerchantPayAmount.setError(null);
    etMerchantId.setError(null);
    cancel = false;

    amount = etMerchantPayAmount.getText().toString();
    checkPayAmount(amount);
    checkMerchantId(etMerchantId.getText().toString());


    if (cancel) {
      focusView.requestFocus();
    } else {
      showCustomDialog();

    }
  }

  private void checkMerchantId(String merchantId) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(merchantId);
    if (!gasCheckLog.isValid) {
      etMerchantId.setError(getString(gasCheckLog.msg));
      focusView = etMerchantId;
      cancel = true;
    }
  }


  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    if (!gasCheckLog.isValid) {
      etMerchantPayAmount.setError(getString(gasCheckLog.msg));
      focusView = etMerchantPayAmount;
      cancel = true;
    } else if (Integer.valueOf(etMerchantPayAmount.getText().toString()) < 10) {
      etMerchantPayAmount.setError(getString(R.string.lessAmount));
      focusView = etMerchantPayAmount;
      cancel = true;
    }
  }

  public void showCustomDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(generateMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(generateMessage());
    }
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        loadDlg.show();
        promoteMerchantPay();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }


  public String generateMessage() {
    return "<b><font color=#000000> Merchant: </font></b>" + "<font color=#000000>" + etMerchantId.getText().toString() + "</font><br>" +
      "<b><font color=#000000> Amount: </font></b>" + "<font>" + amount + "</font><br><br>" +
      "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
  }


  public void promoteMerchantPay() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("id", merchantId);
      jsonRequest.put("netAmount", etMerchantPayAmount.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PAY_AT_STORE, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          Log.i("Send Money", response.toString());
          try {
            Log.i("Merchant Pay Response", response.toString());
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              loadDlg.dismiss();
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String successMessage = jsonObject.getString("details");
              CustomToast.showMessage(getActivity(), successMessage);
              sendRefresh();
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {
              loadDlg.dismiss();
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              String errorMessage = jsonObject.getString("details");
              CustomToast.showMessage(getActivity(), errorMessage);
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq,"test");
    }

  }

  private void showMerchantDialog() {
    final ArrayList<String> merchantVal = new ArrayList<>();
    for (MerchantModel countryModel : merchantList) {
      merchantVal.add(countryModel.getMerchantName());
    }

    android.support.v7.app.AlertDialog.Builder bankDialog =
      new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
    bankDialog.setTitle("Select a Merchant");

    bankDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });

    bankDialog.setItems(merchantVal.toArray(new String[merchantVal.size()]),
      new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int i) {
          etMerchantId.setText(merchantVal.get(i));
          merchantId = merchantList.get(i).getMerchantId();

        }
      });
    bankDialog.show();

  }


  public void loadMerchant() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("Api List Stores", ApiUrl.URL_LIST_STORES);
      Log.i("Json request", jsonRequest.toString());

      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LIST_STORES, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          Log.i("List Stores Response", response.toString());
          try {

            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              MerchantModel.deleteAll(MerchantModel.class);
              merchantList.clear();

              JSONArray merchantArray = response.getJSONArray("merchantList");
              for (int i = 0; i < merchantArray.length(); i++) {
                JSONObject c = merchantArray.getJSONObject(i);
                long merchantId = c.getLong("id");
                String merchantName = c.getString("name");
                String merchantContact = c.getString("contactNo");
                String merchantEmail = c.getString("email");
                String merchantImage = c.getString("image");

                MerchantModel merchantModel = new MerchantModel(merchantId, merchantName, merchantContact, merchantEmail, merchantImage);
                merchantModel.save();
                merchantList.add(merchantModel);
                loadDlg.dismiss();
              }
            } else {
              loadDlg.dismiss();
              if (response.has("message")) {
                responseMessage = response.getString("message");
              } else {
                responseMessage = "Error loading merchant list, please try again later";
              }
            }

          } catch (JSONException e) {
            e.printStackTrace();
            loadDlg.dismiss();
            responseMessage = "Exception occured while loading merchant list";
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          error.printStackTrace();
          loadDlg.dismiss();
          responseMessage = "Exception occured while loading merchant list";

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "123");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq,"test");
    }

  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }
}
