package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.CaptureActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.CheckLog;
import in.msewa.util.EncryptDecryptCouponUtility;
import in.msewa.util.PayingDetailsValidation;

/**
 * Created by Ksf on 4/10/2016.
 */
public class RedeemCouponFragment extends Fragment {
    private View rootView;
    private EditText etRedeemCoupons;
    private Button btnRedeemCoupons;
    private JSONObject jsonRequest;
    private LoadingDialog loadDlg;
    private UserModel session = UserModel.getInstance();

    private boolean cancel;
    private View focusView = null;

    //Volley Tag
    private String tag_json_obj = "json_redeem_coupons";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_redeem_coupon, container, false);
        btnRedeemCoupons = (Button) rootView.findViewById(R.id.btnRedeemCoupons);
        etRedeemCoupons = (EditText) rootView.findViewById(R.id.etRedeemCoupons);
        btnRedeemCoupons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRedeem();
            }
        });

        Button btnRedeemScan = (Button) rootView.findViewById(R.id.btnRedeemScan);
        btnRedeemScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator.forSupportFragment(RedeemCouponFragment.this)
                        .setOrientationLocked(false)
                        .setBeepEnabled(true)
                        .addExtra("PROMPT_MESSAGE", "Scan QR Code")
                        .setCaptureActivity(CaptureActivity.class)
                        .initiateScan(IntentIntegrator.QR_CODE_TYPES);
            }
        });
        return rootView;
    }


    private void attemptRedeem() {
        etRedeemCoupons.setError(null);
        cancel = false;
        checkBlankSpace(etRedeemCoupons.getText().toString());

        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteRedeemCoupons(etRedeemCoupons.getText().toString());
        }
    }

    private void checkBlankSpace(String orderId) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(orderId);
        if (!gasCheckLog.isValid) {
            etRedeemCoupons.setError(getString(gasCheckLog.msg));
            focusView = etRedeemCoupons;
            cancel = true;
        }
    }


    public void promoteRedeemCoupons(String couponsCode) {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("promoCode", couponsCode);
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SEND_REDEEM_CODE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        String message = "";

                        if (response.has("message") && response.getString("message") != null) {
                            message = response.getString("message");
                        }

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            etRedeemCoupons.getText().clear();
                            sendRefresh();
                            showCustomDialog(message);
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), message);
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

        }

    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                CustomToast.showMessage(getActivity(), "Cancelled");
            } else {
                try {
                    String[] splitResult = result.toString().split(":");
                    String encryptValue = splitResult[2].trim();
                    String finalResult = "";

                    if (encryptValue != null) {
                        encryptValue = encryptValue.replaceAll("\t", "");
                        encryptValue = encryptValue.replaceAll("\\n", "");
                        encryptValue = encryptValue.replaceAll("\r", "");
                        encryptValue = encryptValue.replace("Raw bytes", "");
                        try {
                            finalResult = EncryptDecryptCouponUtility.decrypt(encryptValue);
                            promoteRedeemCoupons(finalResult);
                        } catch (Exception e) {
                            CustomToast.showMessage(getActivity(), "Invalid QR");
                        }

                    } else {
                        CustomToast.showMessage(getActivity(), "Unsupported QR Format");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            CustomToast.showMessage(getActivity(), "Result is null");
        }
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showCustomDialog(String message) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(message, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(message);
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}
