package in.msewa.fragment.fragmentnaviagtionitems;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import in.msewa.adapter.HomeMenuAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomKycAlertDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.metadata.MenuMetadata;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.EditProfileActivity;
import in.msewa.ecarib.activity.HowToUpgradeActivity;
import in.msewa.ecarib.activity.MainMenuDetailActivity;


/**
 * Created by Ksf on 3/24/2016.
 */
public class HomeFragment extends Fragment {

  private View rootView;
  private RecyclerView rvHome;
  private UserModel session = UserModel.getInstance();
  private ImageButton play_button, ibClose;
  private ImageView images;
  private TextView text_shown;
  private MediaPlayer player;

  //Music Dailog
  SharedPreferences musicPreferences;
  boolean ShowMusicDailog;
  private boolean checked;
  private TextView tvUserMobile;
  private TextView tvBalance;
  private TextView tvUserKYCWallet;
  private SharedPreferences kycValues;
//  private TextView tvUserMobileWallet;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    musicPreferences = getActivity().getSharedPreferences("ShowMusicDialog", Context.MODE_PRIVATE);
    ShowMusicDailog = musicPreferences.getBoolean("ShowMusicDialog", false);
  }

  @SuppressLint("ClickableViewAccessibility")
  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_home_new, container, false);
    kycValues = getActivity().getSharedPreferences("kyc", Context.MODE_PRIVATE);
    rvHome = (RecyclerView) rootView.findViewById(R.id.rvHome);
    int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
    rvHome.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

    final GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);

    rvHome.setLayoutManager(manager);
    rvHome.setHasFixedSize(true);

    HomeMenuAdapter rcAdapter = new HomeMenuAdapter(getActivity(), MenuMetadata.getMenu(getActivity()));
    rvHome.setAdapter(rcAdapter);
//    tvUserMobile = (TextView) rootView.findViewById(R.id.tvUserMobile);
//    tvBalance = (TextView) rootView.findViewById(R.id.tvBalance);
//    tvUserMobileWallet = (TextView) rootView.findViewById(R.id.tvUserMobileWallet);
    TextView tvUserName = (TextView) rootView.findViewById(R.id.tvUserName);
    Button kyc = (Button) rootView.findViewById(R.id.kyc);
    Button tvUserBalance = (Button) rootView.findViewById(R.id.tvUserBalance);
    Button tvUserPoints = (Button) rootView.findViewById(R.id.tvUserPoints);
//    TextView tvPoints = (TextView) rootView.findViewById(R.id.tvPoints);
    Button tvUserLoadMoney = (Button) rootView.findViewById(R.id.tvUserLoadMoney);
    LinearLayout llBalanceDetails = (LinearLayout) rootView.findViewById(R.id.llBalanceDetails);
//    tvUserKYCWallet = (TextView) rootView.findViewById(R.id.tvUserKYCWallet);
    ImageView ivUser = (ImageView) rootView.findViewById(R.id.ivUser);

    final FloatingActionButton loading_image = (FloatingActionButton) rootView.findViewById(R.id.loading_image);
    final FloatingActionButton loading_image_back = (FloatingActionButton) rootView.findViewById(R.id.loading_image_back);
    loading_image.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          checked = true;
          rvHome.scrollToPosition(rvHome.getAdapter().getItemCount() - 1);
          loading_image.setVisibility(View.GONE);
          loading_image_back.setVisibility(View.VISIBLE);
        }
        return true;
      }
    });
    loading_image_back.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          rvHome.scrollToPosition(0);
          checked = false;
          loading_image.setVisibility(View.GONE);
          loading_image_back.setVisibility(View.VISIBLE);
        }
        return true;
      }
    });
//
//    tvUserKYCWallet.setText("Wallet:  " + session.getUserAcName());
    View view = getActivity().getCurrentFocus();
    if (view != null) {
      InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    kyc.setVisibility(View.GONE);
    if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {

      kyc.setVisibility(View.GONE);

    } else {
      if (!kycValues.getBoolean("KYC", false)) {
//      CustomKycAlertDialog builder = new CustomKycAlertDialog(getActivity(),R.string.dialog_title,"");
//      builder.show();
//      dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view1 = layoutInflater.inflate(R.layout.dialog_custom_kyc, null, false);
        dialog.setContentView(view1);
        final Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setGravity(Gravity.CENTER);
        TextView titleTextView = (TextView) view1.findViewById(R.id.cadTiltle);
        Button btnLater = (Button) view1.findViewById(R.id.btnLater);
        ImageView ivClose = (ImageView) view1.findViewById(R.id.ivClose);
        Button btnOk = (Button) view1.findViewById(R.id.btnOk);
//    titleTextView.setText(context.getText(title));
        btnLater.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
            SharedPreferences.Editor editor = kycValues.edit();
            editor.putBoolean("KYC", true);
            editor.apply();

          }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
            dialog.cancel();
          }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            dialog.dismiss();
            dialog.cancel();
            getActivity().startActivity(new Intent(getActivity(), HowToUpgradeActivity.class));
          }
        });

//      TextView messageTextView = (TextView) view.findViewById(R.id.cadMessage);
        try {
          dialog.show();
        } catch (Exception e) {
          dialog.dismiss();
        }
      }
    }
    kyc.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getActivity(), HowToUpgradeActivity.class));
      }
    });
    rvHome.addOnScrollListener(new RecyclerView.OnScrollListener() {

      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
          // Scrolling up
          if (checked) {
            loading_image.setVisibility(View.GONE);
            loading_image_back.setVisibility(View.VISIBLE);
          } else {
            checked = false;
            loading_image.setVisibility(View.GONE);
            loading_image_back.setVisibility(View.VISIBLE);
          }

        } else {
          // Scrolling down
          if (!checked) {
            loading_image.setVisibility(View.VISIBLE);
            loading_image_back.setVisibility(View.GONE);
          } else {
            checked = true;
            loading_image.setVisibility(View.GONE);
            loading_image_back.setVisibility(View.VISIBLE);
          }
        }
      }

      @Override
      public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
          // Do something
        } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
          // Do something
        } else {
          // Do something
        }
      }
    });
//
    AQuery aq = new AQuery(getActivity());
    if (session != null && session.getIsValid() == 1) {

      tvUserName.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          startActivity(new Intent(getActivity(), EditProfileActivity.class));
        }
      });

      ivUser.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          startActivity(new Intent(getActivity(), EditProfileActivity.class));
        }
      });

      tvUserName.setText(session.getUserFirstName() + " " + session.getUserLastName());
      tvUserBalance.setText("Balance \n" + "\u20B9 " + session.getUserBalance());
//      tvBalance.setText("\u20B9 " + session.getUserBalance());
      tvUserPoints.setText("Share Points" + "\n" + session.getUserPoints());
//      tvUserMobile.setText(session.getUserMobileNo());
//      tvUserMobileWallet.setText(session.getUserAcNo())
// ;
//      tvPoints.setText("Points : " + session.getUserPoints());
      Log.i("images", ApiUrl.URL_IMAGE_MAIN + session.getUserImage());
      if (session.getUserImage().contains("resources")) {
        aq.id(ivUser).background(R.drawable.ic_user_avatar).image(ApiUrl.URL_IMAGE_MAIN + session.getUserImage());
      } else {
        aq.id(ivUser).background(R.drawable.ic_user_avatar).image(decodeFromBase64ToBitmap(session.getUserImage())).getContext();
      }
    } else {

      tvUserName.setText(getResources().getString(R.string.user_welcome_guest));
      aq.id(ivUser).background(R.drawable.ic_user_load);
      getActivity().finish();
    }

    tvUserLoadMoney.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
        menuIntent.putExtra("AutoFill", "no");

        startActivity(menuIntent);

//                if (session.emailIsActive.equals("Active")) {
//                    Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
//                    menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
//                    startActivity(menuIntent);
//                } else {
//                    showCustomDialog();
//                }

      }
    });
    tvUserBalance.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Balance");
        startActivity(menuIntent);

      }
    });

    tvUserPoints.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Points");
        startActivity(menuIntent);
//                CustomToast.showMessage(getActivity(), "Coming shortly");

      }
    });
//      rvHome.smoothScrollToPosition(MenuMetadata.getMenu(getActivity()).size()-1);

//      final int speedScroll = 150;
//      final Handler handler = new Handler();
//      final Runnable runnable = new Runnable() {
//        int count = 0;
//        @Override
//        public void run() {
//          if(count <  MenuMetadata.getMenu(getActivity()).size()){
//            rvHome.scrollToPosition(++count);
//            handler.postDelayed(this,speedScroll);
//          }
//
//
//        }
//      };
//
//      handler.postDelayed(runnable,speedScroll);
//
//        if(ShowMusicDailog) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyleMusic);
//            LayoutInflater inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View viewDialog = inflater1.inflate(R.layout.dialog_song, null, false);
//            builder.setView(viewDialog);
//            final AlertDialog alertDialog = builder.create();
////        seek_bar = (SeekBar) viewDialog.findViewById(R.id.seek_bar);
//            play_button = (ImageButton) viewDialog.findViewById(R.id.play_button);
//            ibClose = (ImageButton) viewDialog.findViewById(R.id.ibClose);
//            text_shown = (TextView) viewDialog.findViewById(R.id.text_shown);
//            text_shown.setText("Enjoy the exclusive " + "\n" + "VPayQwik Rhyme!");
//            play_button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                text_shown.setText("Playing...");
//
//                    if (view.isSelected()) {
//                        view.setSelected(false);
//                        play_button.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
//                        player.pause();
//                    } else {
//                        view.setSelected(true);
//                        player.start();
//                        play_button.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
//                        //...Handled toggle on
//                    }
//                }
//            });
//            ibClose.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (player.isPlaying()) {
//                        player.stop();
//                    }
//                    alertDialog.dismiss();
//                    SharedPreferences.Editor editor = musicPreferences.edit();
//                    editor.clear();
//                    editor.putBoolean("ShowMusicDailog",false);
//                    editor.apply();
//                }
//            });
//
//            player = MediaPlayer.create(getActivity(), R.raw.song);
////        seek_bar.setMax(player.getDuration());
//
//
//            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                @Override
//                public void onCancel(DialogInterface dialogInterface) {
//                    if (player.isPlaying()) {
//                        player.stop();
//                    }
//                    SharedPreferences.Editor editor = musicPreferences.edit();
//                    editor.clear();
//                    editor.putBoolean("ShowMusicDailog",false);
//                    editor.apply();
//
//                }
//            });
//            alertDialog.show();
//        }

    return rootView;
  }


  public static DisplayMetrics getDeviceMetrics(Context context) {
    DisplayMetrics metrics = new DisplayMetrics();
    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    display.getMetrics(metrics);
    return metrics;
  }

  private Bitmap decodeFromBase64ToBitmap(String encodedImage)

  {
    byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);

    return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

  }

  public void showCustomDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateVerifyMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  public String generateVerifyMessage() {
    return "<b><font color=#000000> Please check your inbox and verify email, to proceed.</font></b>" +
      "<br><br><b><font color=#ff0000> You can check registered email address in edit profile section. </font></b><br></br>";
  }

  public void showCustomDialogNoService() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateVerifyMessageNoService()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  public String generateVerifyMessageNoService() {
    String source = "<b><font color=#000000> Dear User, You're using Beta Version of V-PayQwik. </font></b>" +
      "<br><br><b><font color=#ff0000>  Kindly wait for official release. </font></b><br></br>";
    return source;
  }

  @Override
  public void onResume() {
    super.onResume();
    PayQwikApplication.getInstance().trackScreenView("Home Screen");
  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }

}
