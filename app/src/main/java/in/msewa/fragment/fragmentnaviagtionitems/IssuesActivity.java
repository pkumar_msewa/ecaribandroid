package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import in.msewa.fragment.fragmentissues.AllissueFragment;
import in.msewa.fragment.fragmentissues.OpenissueFragment;
import in.msewa.ecarib.R;



/*
import in.payqwik.fragment.fragmentissues.AllissueFragment;
import in.payqwik.fragment.fragmentissues.CloseissueFragment;
import in.payqwik.fragment.fragmentissues.OpenissueFragment;
import in.payqwik.test.R;
*/

/**
 * Created by Ksf on 3/27/2016.
 */

public class IssuesActivity extends AppCompatActivity {


    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    CharSequence TitlesEnglish[] = {"All", "Open"};
    int NumbOfTabs = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);

        LocalBroadcastManager.getInstance(this).registerReceiver(mSignUpReceiver, new IntentFilter("sign-up"));

        Toolbar issuetoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(issuetoolbar);
        ImageButton issueIb = (ImageButton) findViewById(R.id.ivBackBtn);
        issueIb.setVisibility(View.VISIBLE);
        issueIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mainPager.setCurrentItem(1);

        }
    };

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                return new AllissueFragment();
            } else if (position == 1) {
                return new OpenissueFragment();
            } else {
                OpenissueFragment closeissueFragment = new OpenissueFragment();
                return closeissueFragment;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return TitlesEnglish.length;
        }
    }
}
