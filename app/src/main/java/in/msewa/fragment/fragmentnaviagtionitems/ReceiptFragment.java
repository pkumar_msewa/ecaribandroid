package in.msewa.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.ReceiptAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadMoreListView;
import in.msewa.custom.TextProgressBar;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.StatementModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.NetworkErrorHandler;

/**
 * Created by Ksf on 5/8/2016.
 */
public class ReceiptFragment extends Fragment {
  private View rootView;
  private ReceiptAdapter receiptAdapter;
  private LoadMoreListView lvReceipt;
  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;
  private ArrayList<StatementModel> receiptList;
  private ProgressBar pbReceipt;
  private LinearLayout llNoReceipt;
  private TextView tvNoReceipt;
  //Volley
  private String tag_json_obj = "json_receipt";
  private JsonObjectRequest postReq;

  //Pagination
  private int currentPage = 0;
  private int totalPage = 0;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    receiptList = new ArrayList<>();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_receipt, container, false);
    lvReceipt = (LoadMoreListView) rootView.findViewById(R.id.lvReceipt);
    pbReceipt = (ProgressBar) rootView.findViewById(R.id.pbReceipt);
    tvNoReceipt = (TextView) rootView.findViewById(R.id.tvNoReceipt);
    llNoReceipt = (LinearLayout) rootView.findViewById(R.id.llNoReceipt);
    llNoReceipt.setVisibility(View.GONE);
    loadUserStatement();
//    pbReceipt.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, android.graphics.PorterDuff.Mode.MULTIPLY);
//    pbReceipt.show();

    lvReceipt.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
      @Override
      public void onLoadMore() {
        if (currentPage < totalPage) {
          loadMoreUserStatement();
        } else {
          lvReceipt.onLoadMoreComplete();
        }
      }
    });

    return rootView;
  }

  public void loadUserStatement() {
    pbReceipt.setVisibility(View.VISIBLE);
    lvReceipt.setVisibility(View.GONE);
    jsonRequest = new JSONObject();
    try {

      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("page", currentPage);
      jsonRequest.put("size", 10);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
      AndroidNetworking.post(ApiUrl.URL_RECEIPT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject JsonObj) throws JSONException {
          try {

            Log.i("Receipts Response", JsonObj.toString());
            String message = JsonObj.getString("message");
            String code = JsonObj.getString("code");

            if (code != null && code.equals("S00")) {
              sendRefresh();
              String jsonString = JsonObj.getString("response");
              JSONObject response = new JSONObject(jsonString);
              JSONObject jsonDetails = response.getJSONObject("details");
              int totalElements = jsonDetails.getInt("totalElements");
              totalPage = jsonDetails.getInt("totalPages");
              if (totalElements == 0) {
                llNoReceipt.setVisibility(View.VISIBLE);
                pbReceipt.setVisibility(View.GONE);
                lvReceipt.setVisibility(View.GONE);

              } else {
                JSONArray operatorArray = jsonDetails.getJSONArray("content");

                for (int i = 0; i < operatorArray.length(); i++) {
                  JSONObject c = operatorArray.getJSONObject(i);

                  double currentBalanceD = c.getDouble("currentBalance");
                  NumberFormat nf = new DecimalFormat("##.##");
                  String currentBalance = String.valueOf(nf.format(currentBalanceD));

                  double amountPaidD = c.getDouble("amount");
                  String amountPaid = String.valueOf(amountPaidD);

                  long createdD = c.getLong("created");
                  String dateTime = String.valueOf(createdD);


                  JSONObject serviceObj = c.getJSONObject("service");
                  String serviceTypeStr = serviceObj.getString("serviceType");
                  JSONObject serviceTypeObj = new JSONObject(serviceTypeStr);
                  String servicesType = serviceTypeObj.getString("name");


                  String serviceStatus = c.getString("status");

                  String refNo = c.getString("transactionRefNo");
                  String description = c.getString("description");
                  boolean isDebited = c.getBoolean("debit");

                  String retrievRefNo = "";
                  String authNo = "";

                  if (c.has("retrivalReferenceNo")) {
                    if (!c.isNull("retrivalReferenceNo")) {
                      retrievRefNo = c.getString("retrivalReferenceNo");
                    }
                  }

                  if (c.has("authReferenceNo")) {
                    if (!c.isNull("authReferenceNo")) {
                      authNo = c.getString("authReferenceNo");
                    }
                  }


                  StatementModel statementModel = new StatementModel(currentBalance, amountPaid, dateTime, servicesType, serviceStatus, refNo, description, isDebited, authNo, retrievRefNo);
                  receiptList.add(statementModel);
                }
                if (receiptList != null && receiptList.size() != 0) {
                  receiptAdapter = new ReceiptAdapter(getActivity(), receiptList);
                  lvReceipt.setAdapter(receiptAdapter);
                  pbReceipt.setVisibility(View.GONE);
                  lvReceipt.setVisibility(View.VISIBLE);
                  llNoReceipt.setVisibility(View.GONE);
                }
              }


            } else if (code != null && code.equals("F03")) {
              pbReceipt.setVisibility(View.GONE);
              lvReceipt.setVisibility(View.GONE);
              showInvalidSessionDialog();
            } else {
              pbReceipt.setVisibility(View.GONE);
              lvReceipt.setVisibility(View.GONE);
              Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }

          } catch (JSONException e) {
            e.printStackTrace();
            pbReceipt.setVisibility(View.GONE);
            lvReceipt.setVisibility(View.GONE);
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
          }
        }

        @Override
        public void onError(ANError anError) {
          llNoReceipt.setVisibility(View.VISIBLE);
          pbReceipt.setVisibility(View.GONE);
          lvReceipt.setVisibility(View.GONE);
          try {
            tvNoReceipt.setText(getActivity().getResources().getString(R.string.server_exception));
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });
    }

  }

  public void loadMoreUserStatement() {
    currentPage = currentPage + 1;
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("page", currentPage);
      jsonRequest.put("size", 10);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      Log.i("Receipt Url", ApiUrl.URL_RECEIPT);

      AndroidNetworking.post(ApiUrl.URL_RECEIPT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject JsonObj) throws JSONException {
          try {

            Log.i("Receipts Response", JsonObj.toString());
            String message = JsonObj.getString("message");
            String code = JsonObj.getString("code");

            if (code != null && code.equals("S00")) {
              String jsonString = JsonObj.getString("response");
              JSONObject response = new JSONObject(jsonString);
              JSONObject jsonDetails = response.getJSONObject("details");
              JSONArray operatorArray = jsonDetails.getJSONArray("content");
              for (int i = 0; i < operatorArray.length(); i++) {
                JSONObject c = operatorArray.getJSONObject(i);
                double currentBalanceD = c.getDouble("currentBalance");
                String currentBalance = String.valueOf(currentBalanceD);

                double amountPaidD = c.getDouble("amount");
                String amountPaid = String.valueOf(amountPaidD);

                long createdD = c.getLong("created");
                String dateTime = String.valueOf(createdD);


                JSONObject serviceObj = c.getJSONObject("service");
                JSONObject serviceTypeObj = serviceObj.getJSONObject("serviceType");

                String servicesType = serviceTypeObj.getString("name");


                String serviceStatus = c.getString("status");

                String refNo = c.getString("transactionRefNo");
                String description = c.getString("description");
                boolean isDebited = c.getBoolean("debit");


                String retrievRefNo = "";
                String authNo = "";


                if (c.has("retrivalReferenceNo")) {
                  if (!c.isNull("retrivalReferenceNo")) {
                    retrievRefNo = c.getString("retrivalReferenceNo");
                  }
                }

                if (c.has("authReferenceNo")) {
                  if (!c.isNull("authReferenceNo")) {
                    authNo = c.getString("authReferenceNo");
                  }
                }

                StatementModel statementModel = new StatementModel(currentBalance, amountPaid, dateTime, servicesType, serviceStatus, refNo, description, isDebited, authNo, retrievRefNo);
                receiptList.add(statementModel);
              }
              receiptAdapter.notifyDataSetChanged();
            } else if (code != null && code.equals("F03")) {
              showInvalidSessionDialog();
            } else {
              Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
            lvReceipt.onLoadMoreComplete();

          } catch (JSONException e) {
            e.printStackTrace();
            lvReceipt.onLoadMoreComplete();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
          }
        }

        @Override
        public void onError(ANError anError) {
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          lvReceipt.onLoadMoreComplete();

        }
      });
    }

  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "8");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void onDetach() {
//    postReq.cancel();
    super.onDetach();
  }
}
