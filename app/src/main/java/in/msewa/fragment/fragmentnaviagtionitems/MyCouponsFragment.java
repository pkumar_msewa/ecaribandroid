package in.msewa.fragment.fragmentnaviagtionitems;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import in.msewa.adapter.MyCouponsAdapter;
import in.msewa.custom.LoadingDialog;
import in.msewa.model.MyCouponsModel;
import in.msewa.ecarib.R;

//import org.json.XML;

/**
 * Created by Ksf on 5/30/2016.
 */
public class MyCouponsFragment extends Fragment {

    private View rootView;
    private ListView lvMyCoupons;
    private MyCouponsAdapter myCouponsAdapter;
    private LoadingDialog loadingDialog;
//    private XMLParserDeal xmlParserDeal;

    private List<MyCouponsModel> myCouponsModels;
    private LinearLayout llNoCoupons;
    private GetRSSDataTask getCouponsTask;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_coupons, container, false);
        lvMyCoupons = (ListView) rootView.findViewById(R.id.lvMyCoupons);
        llNoCoupons = (LinearLayout) rootView.findViewById(R.id.llNoCoupons);

        getCouponsTask = new GetRSSDataTask();
        getCouponsTask.execute("");


        return rootView;
    }

    @Override
    public void onDestroyView() {
        if (getCouponsTask != null && !getCouponsTask.isCancelled()) {
            getCouponsTask.cancel(true);
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (getCouponsTask != null && !getCouponsTask.isCancelled()) {
            getCouponsTask.cancel(true);
        }
        super.onDestroy();
    }

    private class GetRSSDataTask extends AsyncTask<String, Void, List<MyCouponsModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog.show();
            myCouponsModels = Select.from(MyCouponsModel.class).list();
            if (myCouponsModels != null && myCouponsModels.size() != 0) {
                myCouponsAdapter = new MyCouponsAdapter(getActivity(), myCouponsModels);
                lvMyCoupons.setAdapter(myCouponsAdapter);
                lvMyCoupons.setVisibility(View.VISIBLE);
                llNoCoupons.setVisibility(View.GONE);
                loadingDialog.dismiss();
            }
        }

        @Override
        protected List<MyCouponsModel> doInBackground(String... urls) {
            List<MyCouponsModel> myCouponsInnerModels = new ArrayList<>();


//            try {
//                XMLParserUtils xmlParserUtils = new XMLParserUtils();
//                String xmlString = xmlParserUtils.doGetRequest("http://grouponindia1.go2feeds.org/feed.php?subdomain=grouponindia1&aff_id=4044&offer_id=9&file_id=248&feed=aHR0cHM6Ly9uZWFyYnV5LWltYWdlcy5zMy5hbWF6b25hd3MuY29tL3htbF9kYXRhLnhtbA&objects=ROWSET&object=ROW&url_tag=producturl&url_query=utm_source%3Dnap%26utm_medium%3Dcps%26utm_campaign%3D%7Baffiliate_id%7D%26utm_content%3Dxml");
//                JSONObject jsonObj = XML.toJSONObject(xmlString);
//
//                JSONObject jsonMain = jsonObj.getJSONObject("ROWSET");
//                JSONArray operatorArray = jsonMain.getJSONArray("ROW");
//                MyCouponsModel.deleteAll(MyCouponsModel.class);
//                for (int i = 0; i < 33; i++) {
//                    JSONObject c = operatorArray.getJSONObject(i);
//                    long dealIDL = c.getLong("id");
//                    String dealID = String.valueOf(dealIDL);
//                    String DealTitle = c.getString("title");
//                    String merchantName = c.getString("brand");
//                    String merchantAddress = c.getString("categoryid2");
//                    String dealURL = c.getString("producturl");
//                    String dealImageURL = c.getString("smallimage");
//
//                    MyCouponsModel myCouponsModel = new MyCouponsModel(DealTitle, dealImageURL, merchantAddress, merchantName, "", dealURL, dealID, "");
//                    myCouponsModel.save();
//                    myCouponsInnerModels.add(myCouponsModel);
//                }
//
//                return myCouponsInnerModels;
//            } catch (Exception e) {
//                Log.e("RssChannelActivity", e.getMessage());
//            }
            return null;
        }

        @Override
        protected void onPostExecute(List<MyCouponsModel> result) {
            loadingDialog.dismiss();
            if (result != null && result.size() != 0) {
                myCouponsAdapter = new MyCouponsAdapter(getActivity(), result);
                lvMyCoupons.setAdapter(myCouponsAdapter);
                llNoCoupons.setVisibility(View.GONE);
                lvMyCoupons.setVisibility(View.VISIBLE);
            } else if (result != null && result.size() == 0) {
                llNoCoupons.setVisibility(View.VISIBLE);
                lvMyCoupons.setVisibility(View.GONE);
            }
        }
    }


}
