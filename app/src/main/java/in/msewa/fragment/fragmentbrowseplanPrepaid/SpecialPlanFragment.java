package in.msewa.fragment.fragmentbrowseplanPrepaid;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import in.msewa.adapter.MobilePlanAdapter;
import in.msewa.ecarib.R;
import in.msewa.util.MobilePlansCheck;


/**
 * Created by Ksf on 3/16/2016.
 */
public class SpecialPlanFragment extends Fragment {

    MobilePlansCheck plans = MobilePlansCheck.getInstance();

    private View rootview;
    private ListView listOfDataPackPlans;
    private TextView tvNoPlans;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_full_talk_time, container,false);

        listOfDataPackPlans = (ListView) rootview.findViewById(R.id.lvMobileDataPackPlans);
        tvNoPlans = (TextView) rootview.findViewById(R.id.tvNoPlans);

        if (plans.getSpecialPlans().size() == 0) {
            listOfDataPackPlans.setVisibility(View.GONE);
            tvNoPlans.setVisibility(View.VISIBLE);
        } else {
            listOfDataPackPlans.setVisibility(View.VISIBLE);
            tvNoPlans.setVisibility(View.GONE);

            MobilePlanAdapter tabMobileList = new MobilePlanAdapter(getActivity(), plans.getSpecialPlans(), 0);
            listOfDataPackPlans.setAdapter(tabMobileList);
        }

        return rootview;
    }

}
