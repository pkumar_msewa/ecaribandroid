package in.msewa.fragment.fragmentbrowseplanPrepaid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import in.msewa.adapter.MobilePlanAdapter;
import in.msewa.ecarib.R;
import in.msewa.util.MobilePlansCheck;


/**
 * Created by Ksf on 3/16/2016.
 */
public class FullTalkTimeFragment extends Fragment {
    MobilePlansCheck plans = MobilePlansCheck.getInstance();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_full_talk_time,container,false);
        ListView listOfDataPackPlans = (ListView) rootView.findViewById(R.id.lvMobileDataPackPlans);
        TextView tvNoPlans = (TextView) rootView.findViewById(R.id.tvNoPlans);

        if (plans.getTalkTimePlans().size() == 0) {
            listOfDataPackPlans.setVisibility(View.GONE);
            tvNoPlans.setVisibility(View.VISIBLE);
        } else {
            listOfDataPackPlans.setVisibility(View.VISIBLE);
            tvNoPlans.setVisibility(View.GONE);

            MobilePlanAdapter tabMobileList = new MobilePlanAdapter(getActivity(), plans.getTalkTimePlans(), 0);
            listOfDataPackPlans.setAdapter(tabMobileList);
        }

        return rootView;
    }
}
