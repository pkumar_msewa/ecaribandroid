package in.msewa.fragment.fragmenthowtoupgrade;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import in.msewa.ecarib.R;

/**
 * Created by Ksf on 11/2/2016.
 */
public class OtherUserBankUpgradeFragment extends Fragment {
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_other_bank_upgrade, container, false);

        Button btnUpgradeWallet = (Button) rootView.findViewById(R.id.btnUpgradeWallet);
        btnUpgradeWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "care@vpayqwik.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Upgrade VPayQwik Wallet.");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });
        return rootView;
    }
}
