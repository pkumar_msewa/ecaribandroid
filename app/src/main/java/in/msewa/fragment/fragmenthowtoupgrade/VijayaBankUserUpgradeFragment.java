package in.msewa.fragment.fragmenthowtoupgrade;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.OtpVerificationKycAcitvity;

/**
 * Created by Ksf on 11/2/2016.
 */
public class VijayaBankUserUpgradeFragment extends Fragment {
  private MaterialEditText etVijayaUpgrageAccNo, etVijayaAccountName, etVijayaAccountMobile;
  private View focusView = null;
  private boolean cancel;
  private LoadingDialog loadingDialog;
  private JSONObject jsonRequest;
  //Volley Tag
  private String tag_json_obj = "json_kyc";
  private UserModel session = UserModel.getInstance();


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadingDialog = new LoadingDialog(getActivity());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_vijaya_bank_upgrade, container, false);
    etVijayaUpgrageAccNo = (MaterialEditText) rootView.findViewById(R.id.etVijayaUpgrageAccNo);
    etVijayaAccountName = (MaterialEditText) rootView.findViewById(R.id.etVijayaAccountName);
    etVijayaAccountMobile = (MaterialEditText) rootView.findViewById(R.id.etVijayaAccountMobile);

    Button btnVijayaUpgrade = (Button) rootView.findViewById(R.id.btnVijayaUpgrade);
    btnVijayaUpgrade.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptUpgrade();
      }
    });
    return rootView;
  }


  private void attemptUpgrade() {
    etVijayaAccountMobile.setError(null);
    etVijayaAccountName.setError(null);
    etVijayaUpgrageAccNo.setError(null);

    cancel = false;

    checkName(etVijayaAccountName.getText().toString());
    checkAccNo(etVijayaUpgrageAccNo.getText().toString());
    checkPhone(etVijayaAccountMobile.getText().toString());

    if (cancel) {
      focusView.requestFocus();
    } else {
      promoteUpgrade();
    }
  }

  private void checkName(String name) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
    if (!gasCheckLog.isValid) {
      etVijayaAccountName.setError(getString(gasCheckLog.msg));
      focusView = etVijayaAccountName;
      cancel = true;
    }
  }

  private void checkAccNo(String accNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkVijayaBankAc(accNo);
    if (!gasCheckLog.isValid) {
      etVijayaUpgrageAccNo.setError(getString(gasCheckLog.msg));
      focusView = etVijayaUpgrageAccNo;
      cancel = true;
    }
  }


  private void checkPhone(String phNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
    if (!gasCheckLog.isValid) {
      etVijayaAccountMobile.setError(getString(gasCheckLog.msg));
      focusView = etVijayaAccountMobile;
      cancel = true;
    }
  }


  private void promoteUpgrade() {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("accountNumber", etVijayaUpgrageAccNo.getText().toString());
      jsonRequest.put("mobileNumber", etVijayaAccountMobile.getText().toString());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SEND_KYC_DATA, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("KYC RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              }
              loadingDialog.dismiss();
              startActivity(new Intent(getActivity(), OtpVerificationKycAcitvity.class));
            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog(response.getString("message"));
            } else {
              loadingDialog.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
              }
            }

          } catch (JSONException e) {
            loadingDialog.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));


        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "123");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }


}
