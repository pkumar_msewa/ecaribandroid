package in.msewa.fragment.fragmentipl;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.FontsForXMLLight;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.IplTeamModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;


/**
 * Created by Kashif-PC on 3/27/2017.
 */
public class IplFragment extends Fragment {

  private static final String TAG = "timestakesss";
  private View rootView;
  private EditText etTeam;
  private TextInputLayout ilTeam;
  private TextView tvNote;
  private Button btnPredict;

  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();
  private String tcode;
  //Volley
  private String tag_json_obj = "json_events";
  private JsonObjectRequest postReq;


  public static final String inputFormat = "HH:mm";
  //
  private Date time, firstTime, secondTime;
  //
  SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);
  private IplTeamModel strtext;
  public TextView tvTeam1name, tvTeam2name, tvMatchInfo;
  CardView cvIplTeamList;
  TextView iplVs, ivTeam1name, ivTeam2name;
  LinearLayout llIplList;
  private String code;
  private String tittleName;
  private String teamName;
  private ImageView ivSecondTeam, ivFirstTeam;
  private TextView timeDate;
  private FrameLayout flFirstTeam, flSecondTeam;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    rootView = inflater.inflate(R.layout.fragment_ipl, container, false);
    strtext = (IplTeamModel) getArguments().getParcelable("IplDetails");
    loadDlg = new LoadingDialog(getActivity());
    Button btnTermCondition = (Button) rootView.findViewById(R.id.btnTermCondition);
    flFirstTeam = (FrameLayout) rootView.findViewById(R.id.flFirstTeam);
    flSecondTeam = (FrameLayout) rootView.findViewById(R.id.flSecondTeam);
    cvIplTeamList = (CardView) rootView.findViewById(R.id.cvIplTeamList);
    tvMatchInfo = (TextView) rootView.findViewById(R.id.tvMatchInfo);
    ivTeam1name = (TextView) rootView.findViewById(R.id.ivTeam1name);
    ivTeam2name = (TextView) rootView.findViewById(R.id.ivTeam2name);
    btnPredict = (Button) rootView.findViewById(R.id.btnPredict);
    ivSecondTeam = (ImageView) rootView.findViewById(R.id.ivSecondTeam);
    ivFirstTeam = (ImageView) rootView.findViewById(R.id.ivFirstTeam);
    TextView time2 = (TextView) rootView.findViewById(R.id.time);
    iplVs = (TextView) rootView.findViewById(R.id.iplVs);
    timeDate = (TextView) rootView.findViewById(R.id.timeDate);
    ivTeam1name.setText(strtext.getFirstTeamId());
    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "Myriad-Pro-Semibold.ttf");
    Typeface typeface1 = Typeface.create(typeface, Typeface.BOLD);
    ivTeam1name.setTypeface(typeface1);
    ivTeam2name.setTypeface(typeface1);
    iplVs.setTypeface(typeface1);
    code = strtext.getMatchCode();
    tittleName = strtext.getFirstTeamId();
    teamName = strtext.getFirstTeam();
    btnTermCondition.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("PREDICT AND WIN");
        builder.setMessage(Html.fromHtml("<p><b>TERMS &amp; CONDITIONS:</b></p> <p> &bull; Lucky winners get up to Rs.1000/- Cash Back per Match.(winner will get selected randomly through System.)</p>\n" +
          "<p>&bull; To increase the chances of winning, the user can do multiple transactions on VpayQwik.</p>\n" +
          "<p>&bull; Winners will be announced in weekends.</p>\n" +
          "<p>&bull; To participate in this Contest, Customer need to complete their KYC details .</p>\n" +
          "<p>&bull; VPAYQWIK reserves all the right to disqualify any transaction from participation in this contest.</p>\n" +
          "<p>&bull; VPAYQWIK employees and family members are not eligible to participate in this contest.</p>\n" +
          "<p>&bull; The Predictions for any match will be counted till before Time Mentioned On The Contest Page.</p>\n" +
          "<p>&bull; The Results Will be declared Only after Innings completion.</p>\n" +
          "<p>&bull; The Result will be declared on our official VpayQwik Social Media Page.</p>\n" +
          "<p>&bull; You Can Not Participate More Than One For A Single Contest. Your First prediction Is Final Its Not Changeable. So Be Careful While Making The Predictions.</p>\n" +
          "<p>&bull; If No One Makes An Exact Predictions Then There Will be No Winner For That Particular Contest</p>\n" +
          "<p>&bull; Any Fraudulent Activity Will Lead To Suspension From Contest.</p>\n" +
          "<p><b>Also, by participating in this Quiz, You:</b></p>\n" +
          "<p>1. Agree to the Terms and Conditions mentioned below and give Your consent for the same</p>\n" +
          "<p>2. You are acting voluntarily, using Your own time and resources to participate in this Quiz, solely at Your own risk  </p>"));
        builder.setCancelable(false);
        builder.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        });
        builder.show();
      }
    });


    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
    try {
      Date time = simpleDateFormat.parse(strtext.getMatchDate());
      SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
      SimpleDateFormat timeHour = new SimpleDateFormat("hh:mm aa");
      time2.setText("Start on : " + timeHour.format(time));
      timeDate.setText("Match Date : " + date.format(time));

    } catch (ParseException e) {
      e.printStackTrace();
    }
    flFirstTeam.setBackgroundResource(R.drawable.circle_broder_selected);
    ivTeam1name.setBackgroundResource(R.drawable.bg_button_ipl_pressed);
    ivTeam1name.setTextColor(getResources().getColor(R.color.colorPrimary));
    code = strtext.getMatchCode();
    tittleName = strtext.getFirstTeamId();
    iplVs.setText("VS");
    tvMatchInfo.setText(strtext.getVenue());
    ivTeam2name.setText(strtext.getSecondTeamId());
    ivTeam1name.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        ivTeam2name.setBackgroundResource(android.R.color.transparent);
        ivTeam2name.setTextColor(Color.BLACK);
        flFirstTeam.setBackgroundResource(R.drawable.circle_broder_selected);
        flSecondTeam.setBackgroundResource(android.R.color.transparent);
        ivTeam1name.setBackgroundResource(R.drawable.bg_button_ipl_pressed);
        ivTeam1name.setTextColor(getResources().getColor(R.color.colorPrimary));
        code = strtext.getMatchCode();
        tittleName = strtext.getFirstTeamId();
        teamName = strtext.getFirstTeam();
      }
    });

    ivTeam2name.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        ivTeam1name.setBackgroundResource(android.R.color.transparent);
        ivTeam1name.setTextColor(Color.BLACK);
        flSecondTeam.setBackgroundResource(R.drawable.circle_broder_selected);
        flFirstTeam.setBackgroundResource(android.R.color.transparent);
        ivTeam2name.setBackgroundResource(R.drawable.bg_button_ipl_pressed);
        ivTeam2name.setTextColor(getResources().getColor(R.color.colorPrimary));
        code = strtext.getMatchCode();
        tittleName = strtext.getSecondTeamId();
        teamName = strtext.getSecondTeam();
      }
    });

    ivFirstTeam.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        ivTeam2name.setBackgroundResource(android.R.color.transparent);
        ivTeam2name.setTextColor(Color.BLACK);
        flFirstTeam.setBackgroundResource(R.drawable.circle_broder_selected);
        flSecondTeam.setBackgroundResource(android.R.color.transparent);
        ivTeam1name.setBackgroundResource(R.drawable.bg_button_ipl_pressed);
        ivTeam1name.setTextColor(getResources().getColor(R.color.colorPrimary));
        code = strtext.getMatchCode();
        tittleName = strtext.getFirstTeamId();
        teamName = strtext.getFirstTeam();
      }
    });

    ivSecondTeam.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        ivTeam1name.setBackgroundResource(android.R.color.transparent);
        ivTeam1name.setTextColor(Color.BLACK);
        flSecondTeam.setBackgroundResource(R.drawable.circle_broder_selected);
        flFirstTeam.setBackgroundResource(android.R.color.transparent);
        ivTeam2name.setBackgroundResource(R.drawable.bg_button_ipl_pressed);
        ivTeam2name.setTextColor(getResources().getColor(R.color.colorPrimary));
        code = strtext.getMatchCode();
        tittleName = strtext.getSecondTeamId();
        teamName = strtext.getSecondTeam();
      }
    });
//    if (strtext.getPredictionEnable()) {
//      btnPredict.setClickable(true);
//      btnPredict.setActivated(true);
//    } else {
//      btnPredict.setClickable(false);
//      btnPredict.setActivated(false);
//    }

    if ((TextUtils.isEmpty(strtext.getFirstTeamLogo()))) {

      Picasso.with(getActivity()).load("new").placeholder(R.drawable.no_image_available).into(ivFirstTeam);
    } else {
      Picasso.with(getActivity()).load(strtext.getFirstTeamLogo()).placeholder(R.drawable.ic_loading_image).into(ivFirstTeam);
    }
    if ((TextUtils.isEmpty(strtext.getSecondTeamLogo()))) {
      Picasso.with(getActivity()).load("new").placeholder(R.drawable.ic_loading_image).into(ivSecondTeam);
    } else {
      Picasso.with(getActivity()).load(strtext.getSecondTeamLogo()).placeholder(R.drawable.ic_loading_image).into(ivSecondTeam);
    }

    btnPredict.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        if (strtext.getPredictionEnable()) {
          btnPredict.setClickable(true);
          btnPredict.setActivated(true);
          submitForm(tittleName, code);
        } else {
          CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, strtext.getAdditionalInfo());
          customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
              getActivity().finish();
//              startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
          });
          customAlertDialog.show();
        }

      }
    });

    return rootView;
  }


  @Override
  public void onResume() {
    super.onResume();

  }

  private void submitForm(final String fullName, final String code) {

    final AlertDialog.Builder customAlertDialogIPL = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_custom, null);
    customAlertDialogIPL.setView(view);
    customAlertDialogIPL.setCancelable(false);
    final AlertDialog alertDialog = customAlertDialogIPL.create();
    LinearLayout btnOk = (LinearLayout) view.findViewById(R.id.iplCheck);
    Button btnyes = (Button) view.findViewById(R.id.btnyes);
    Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
    FontsForXMLLight msg = (FontsForXMLLight) view.findViewById(R.id.cadMessage);
    btnOk.setVisibility(View.VISIBLE);
    msg.setGravity(Gravity.CENTER);
    msg.setText("Are you sure you want to Predict to" + " " + teamName + " " + "Team?");
    btnCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        alertDialog.dismiss();
        predict(code, fullName);
      }
    });
    btnyes.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        alertDialog.dismiss();
      }
    });
    alertDialog.show();


  }

  public void showSuccessDialog(String Title, String message) {

    final AlertDialog.Builder customAlertDialogIPL = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_custom_success, null);
    customAlertDialogIPL.setView(view);
    customAlertDialogIPL.setCancelable(false);
    final AlertDialog alertDialog = customAlertDialogIPL.create();
    Button btnOk = (Button) view.findViewById(R.id.btnOk);

    TextView titleTextView = (TextView) view.findViewById(R.id.cadTiltle);
    titleTextView.setText(Title);

    TextView messageTextView = (TextView) view.findViewById(R.id.cadMessage);
    messageTextView.setText(message);
    btnOk.setVisibility(View.VISIBLE);

    btnOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        alertDialog.dismiss();
        getActivity().finish();
        startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
      }
    });
    alertDialog.show();
  }

  public void predict(String code, String fullname) {
    loadDlg.show();

    final JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", session.getUserMobileNo());
      jsonRequest.put("matchId", code);
      jsonRequest.put("categoryType", "IPL");
      jsonRequest.put("teamId", fullname);

    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("Team Predict URL : ", ApiUrl.URL_TEAM_PREDICT);
      Log.i("Request : ", jsonRequest.toString());
      HashMap<String, String> map = new HashMap<>();
      map.put("hash", "12345");
      map.put("cllientKey", session.getMdexKey());
      map.put("clientToken", session.getMdexToken());
      AndroidNetworking.post(session.getIplPrediction())
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .addHeaders(map)
        .setPriority(Priority.HIGH)
        .build()
        .setAnalyticsListener(new AnalyticsListener() {
          @Override
          public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
            Log.d(TAG, " bytesSent : " + bytesSent);
            Log.d(TAG, " bytesReceived : " + bytesReceived);
            Log.d(TAG, " isFromCache : " + isFromCache);
          }
        }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject jsonObj) throws JSONException {

          try {
            loadDlg.dismiss();
            Log.i("EventsCatResponse", jsonObj.toString());
            String code = jsonObj.getString("code");
            String message = jsonObj.getString("message");

            if (code != null && code.equals("S00")) {
              showSuccessDialog("Prediction Sent", "Hurray, We received your prediction. We wish you all the best!");
            } else {
              CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, message);
              customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  dialog.dismiss();
                  getActivity().finish();
                  getActivity().startActivity(new Intent(getActivity(), HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
              });
              customAlertDialog.show();
              loadDlg.dismiss();
            }

          } catch (JSONException e) {
            e.printStackTrace();
            loadDlg.dismiss();
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
      }

      @Override
      public void onError(ANError anError) {
        CustomToast.showMessage(getActivity(),getActivity().getResources().getString(R.string.server_exception));
        loadDlg.dismiss();
      }
    });
    }
  }

  public void checkprediction(String abbreviation) {
    loadDlg.show();

    final JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("code", abbreviation);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("Prediction check URL : ", ApiUrl.URL_TEAM_CHECK_PREDICTION);
      Log.i("Request : ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TEAM_CHECK_PREDICTION, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            Log.i("EventsCatRespo;nse", jsonObj.toString());
            String code = jsonObj.getString("code");
            String message = jsonObj.getString("message");

            loadDlg.dismiss();
            if (code.equals("F03")) {
              showInvalidSessionDialog();
            } else {
              if (message.equals("Already Predicted Today")) {
                alredyPredictAlertDialog();
              }
//                            CustomToast.showMessage(getActivity(), message);
              loadDlg.dismiss();
              getActivity().getSupportFragmentManager().popBackStack();
            }

          } catch (JSONException e) {
            e.printStackTrace();
            loadDlg.dismiss();
            getActivity().getSupportFragmentManager().popBackStack();
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          error.printStackTrace();
          getActivity().getSupportFragmentManager().popBackStack();
          CustomToast.showMessage(getActivity(),getActivity().getResources().getString(R.string.server_exception));
          loadDlg.dismiss();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "12345");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void alredyPredictAlertDialog() {

    final AlertDialog.Builder customAlertDialogIPL = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_custom, null);
    customAlertDialogIPL.setView(view);
    customAlertDialogIPL.setCancelable(false);
    final AlertDialog alertDialog = customAlertDialogIPL.create();
    Button btnOk = (Button) view.findViewById(R.id.btnOk);
    FontsForXMLLight msg = (FontsForXMLLight) view.findViewById(R.id.cadMessage);
    btnOk.setVisibility(View.VISIBLE);
    msg.setGravity(Gravity.CENTER);
    msg.setText("You have already predicted for current game. Next Prediction will open shortly");
    btnOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        alertDialog.dismiss();
        getActivity().finish();
      }
    });
    alertDialog.show();


  }

  @Override
  public void onDetach() {
    PayQwikApplication.getInstance().cancelPendingRequests(tag_json_obj);
    super.onDetach();
  }

  private void compareTime(boolean b) throws ParseException {
    Calendar now = Calendar.getInstance();
    Calendar now1 = Calendar.getInstance();

    int hour = now.get(Calendar.HOUR);
    int minute = now.get(Calendar.MINUTE);

    time = parseTime(hour + ":" + minute);
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
    Date date = df.parse(strtext.getMatchDate());

    Time time = new Time(now.getTime().getTime());
    now1.set(Calendar.HOUR_OF_DAY, date.getHours()); // Your hour
    now1.set(Calendar.MINUTE, date.getMinutes()); // Your Mintue
    Time time2 = new Time(date.getTime());
    Log.i("time", String.valueOf(time.getHours()) + time2.getMinutes());
    if (time.before(time2)) {

      if (b) {
        submitForm(tittleName, strtext.getFirstTeamId());
      } else {
        checkprediction(strtext.getFirstTeamId());
        checkprediction(strtext.getSecondTeamId());
      }

    } else if (time.equals(time2) || time.after(time2)) {

      final AlertDialog.Builder customAlertDialogIPL = new AlertDialog.Builder(getActivity());
      LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View view = inflater.inflate(R.layout.dialog_custom, null);
      customAlertDialogIPL.setView(view);
      customAlertDialogIPL.setCancelable(false);
      final AlertDialog alertDialog = customAlertDialogIPL.create();
      Button btnOk = (Button) view.findViewById(R.id.btnOk);
      FontsForXMLLight msg = (FontsForXMLLight) view.findViewById(R.id.cadMessage);
      btnOk.setVisibility(View.VISIBLE);
      msg.setGravity(Gravity.CENTER);
      msg.setText("Sorry " + session.getUserFirstName() + "!" + "\n" + "You cannot predict the previous match.");
      btnOk.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          alertDialog.dismiss();
          getActivity().finish();
        }
      });
      alertDialog.show();
    }
  }


  private Date parseTime(String date) {

    try {
      return inputParser.parse(date);
    } catch (ParseException e) {
      return new Date(0);
    }
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

}
