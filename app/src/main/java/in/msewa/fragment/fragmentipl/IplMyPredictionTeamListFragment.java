package in.msewa.fragment.fragmentipl;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteException;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidquery.AQuery;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.adapter.IplMyPredicetTeamListAdapter;
import in.msewa.adapter.IplTeamListAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.IplMyPredicationTeamModel;
import in.msewa.model.IplTeamModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Kashif-PC on 3/31/2017.
 */
public class IplMyPredictionTeamListFragment extends Fragment {

  private static final String TAG = "lojkgds]";
  private RecyclerView rvIplTeamList;
  private ProgressBar pbIplTeamList;
  private UserModel session = UserModel.getInstance();

  //Volley
  private String tag_json_obj = "json_events", currentDate;
  private JsonObjectRequest postReq;

  private LinearLayout llNoReceipt;
  private TextView tvNoReceipt;
  private List<UserModel> userArray;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_ipl_team_list, container, false);
    rvIplTeamList = (RecyclerView) rootView.findViewById(R.id.rvIplTeamList);
    ImageView ivBackgroundImage = (ImageView) rootView.findViewById(R.id.ivBackgroundImage);
    pbIplTeamList = (ProgressBar) rootView.findViewById(R.id.pbIplTeamList);
    llNoReceipt = (LinearLayout) rootView.findViewById(R.id.llNoReceipt);
    int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
    try {
      userArray = Select.from(UserModel.class).list();
    }catch (SQLiteException e){
    userArray=new ArrayList<>();
    }
    rvIplTeamList.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    tvNoReceipt = (TextView) rootView.findViewById(R.id.tvNoReceipt);
    getCardCategory();
    return rootView;
  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }


  public void getCardCategory() {
    pbIplTeamList.setVisibility(View.VISIBLE);
    rvIplTeamList.setVisibility(View.GONE);
    final JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", session.getUserMobileNo());
    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {

      HashMap<String, String> map = new HashMap<>();
      map.put("hash", "12345");
      map.put("cllientKey", session.getMdexKey());
      map.put("clientToken", session.getMdexToken());
      AndroidNetworking.post(userArray.get(0).getIplMyPrediction())
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .addHeaders(map)
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject response) throws JSONException {
            //
            try {
//      JSONObject response = new JSONObject(loadJSONFromAsset());
              String code = response.getString("code");
              Log.i("IplTeamListResponse", response.toString());

              if (code.equals("S00")) {
                String details = response.getString("details");
                List<IplMyPredicationTeamModel> iplTeamListArray = new ArrayList<>();
                JSONArray scheduleArray = new JSONArray(details);
                for (int i = 0; i < scheduleArray.length(); i++) {
                  pbIplTeamList.setVisibility(View.VISIBLE);
                  JSONObject s = scheduleArray.getJSONObject(i);

                  String matchCode = s.getString("matchCode");
                  String firstTeam = s.getString("firstTeam");
                  String mobileNumber = s.getString("mobileNumber");
                  String secondTeam = s.getString("secondTeam");
                  String predictionDate = s.getString("predictionDate");
                  String predictedTeam = s.getString("predictedTeam");
                  String predictId = s.getString("predictId");
                  String matchDate = s.getString("matchDate");
                  Boolean winningResult = s.getBoolean("winningResult");
                  String additionalInfo = s.getString("additionalInfo");
                  String firstTeamLogo = s.getString("firstTeamLogo");
                  String secondTeamLogo = s.getString("secondTeamLogo");
                  iplTeamListArray.add(new IplMyPredicationTeamModel(matchCode, firstTeam, mobileNumber, secondTeam, predictionDate, predictedTeam, predictId, matchDate, winningResult, additionalInfo, firstTeamLogo, secondTeamLogo));
                }

                if (iplTeamListArray.size() != 0) {
                  GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
                  rvIplTeamList.setLayoutManager(manager);
                  rvIplTeamList.setHasFixedSize(true);

                  IplMyPredicetTeamListAdapter iplTeamListAdapter = new IplMyPredicetTeamListAdapter(getActivity(), iplTeamListArray);
                  rvIplTeamList.setAdapter(iplTeamListAdapter);
                  pbIplTeamList.setVisibility(View.GONE);
                  rvIplTeamList.setVisibility(View.VISIBLE);
                  llNoReceipt.setVisibility(View.GONE);
                }

              } else {
                String message = response.getString("message");
                pbIplTeamList.setVisibility(View.GONE);
                rvIplTeamList.setVisibility(View.VISIBLE);
                llNoReceipt.setVisibility(View.VISIBLE);
                tvNoReceipt.setText(message);
              }
//
            } catch (JSONException e) {
              e.printStackTrace();
              pbIplTeamList.setVisibility(View.GONE);
              Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
            }
          }

          @Override
          public void onError(ANError anError) {
            pbIplTeamList.setVisibility(View.GONE);
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          }
        });
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }


}
