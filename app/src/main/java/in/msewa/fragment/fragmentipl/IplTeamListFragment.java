package in.msewa.fragment.fragmentipl;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidquery.AQuery;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.adapter.IplTeamListAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.fragment.fragmentnaviagtionitems.HomeFragment;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.IplTeamModel;
import in.msewa.model.Team;
import in.msewa.model.UserModel;
import in.msewa.model.Venue;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Kashif-PC on 3/31/2017.
 */
public class IplTeamListFragment extends Fragment {

  private static final String TAG = "responsss";
  private RecyclerView rvIplTeamList;
  private ProgressBar pbIplTeamList;
  private UserModel session = UserModel.getInstance();

  //Volley
  private String tag_json_obj = "json_events", currentDate;
  private JsonObjectRequest postReq;
  private SearchView search_ipl_list;
  private List<UserModel> userArray;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_ipl_team_list, container, false);
    rvIplTeamList = (RecyclerView) rootView.findViewById(R.id.rvIplTeamList);
    ImageView ivBackgroundImage = (ImageView) rootView.findViewById(R.id.ivBackgroundImage);
    pbIplTeamList = (ProgressBar) rootView.findViewById(R.id.pbIplTeamList);
    search_ipl_list = (SearchView) rootView.findViewById(R.id.search_ipl_list);
    int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
    userArray = Select.from(UserModel.class).list();
    rvIplTeamList.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    getCardCategory();
    return rootView;
  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }

  public void getCardCategory() {
    pbIplTeamList.setVisibility(View.VISIBLE);
    rvIplTeamList.setVisibility(View.GONE);
    final JSONObject jsonRequest = new JSONObject();
    try {

      jsonRequest.put("mobileNumber", session.getUserMobileNo());
      jsonRequest.put("categoryType", "IPL");
    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {

      HashMap<String, String> map = new HashMap<>();
      map.put("hash", "12345");
      map.put("cllientKey", session.getMdexKey());
      map.put("clientToken", session.getMdexToken());
      AndroidNetworking.post(userArray.get(0).getIplSchedule())
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .addHeaders(map)
        .setPriority(Priority.HIGH)
        .build()
        .setAnalyticsListener(new AnalyticsListener() {
          @Override
          public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
            Log.d(TAG, " bytesSent : " + bytesSent);
            Log.d(TAG, " bytesReceived : " + bytesReceived);
            Log.d(TAG, " isFromCache : " + isFromCache);
          }
        }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) throws JSONException {


          try {
            String code = response.getString("code");
            Log.i("IplTeamListResponse", response.toString());

            if (code.equals("S00")) {
              String details = response.getString("details");
              List<IplTeamModel> iplTeamListArray = new ArrayList<>();
              JSONArray scheduleArray = new JSONArray(details);
              for (int i = 0; i < scheduleArray.length(); i++) {
                pbIplTeamList.setVisibility(View.VISIBLE);
                JSONObject s = scheduleArray.getJSONObject(i);
                String matchDate = s.getString("matchDate");
                String venue = s.getString("venue");
                String firstTeam = s.getString("firstTeam");
                String secondTeam = s.getString("secondTeam");
                String winningTeam = s.getString("winningTeam");
                String firstTeamId = s.getString("firstTeamId");
                String secondTeamId = s.getString("secondTeamId");
                String winningTeamId = s.getString("winningTeamId");
                Boolean predictionEnable = s.getBoolean("predictionEnable");
                String additionalInfo = s.getString("additionalInfo");
                String firstTeamLogo, secondTeamLogo;
                firstTeamLogo = s.getString("firstTeamLogo");
                secondTeamLogo = s.getString("secondTeamLogo");
                if (TextUtils.isEmpty(firstTeamLogo)) {
                  firstTeamLogo = "new";
                } else if (TextUtils.isEmpty(secondTeamLogo)) {
                  secondTeamLogo = "new";
                }
                String matchCode = s.getString("matchCode");
                Boolean active = s.getBoolean("active");
                iplTeamListArray.add(new IplTeamModel(matchDate, venue, firstTeam, secondTeam, winningTeam, firstTeamId, secondTeamId, winningTeamId, predictionEnable, additionalInfo, firstTeamLogo, secondTeamLogo, matchCode, active));
              }

              if (iplTeamListArray.size() != 0) {
                GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
                rvIplTeamList.setLayoutManager(manager);
                rvIplTeamList.setHasFixedSize(true);

                final IplTeamListAdapter iplTeamListAdapter = new IplTeamListAdapter(getActivity(), iplTeamListArray);
                rvIplTeamList.setAdapter(iplTeamListAdapter);
                pbIplTeamList.setVisibility(View.GONE);
                rvIplTeamList.setVisibility(View.VISIBLE);
                search_ipl_list.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                  @Override
                  public boolean onQueryTextSubmit(String query) {
                    // Toast like print

                    if (!search_ipl_list.isIconified()) {
                      search_ipl_list.setIconified(true);
                    }
                    iplTeamListAdapter.getFilter().filter(query);
                    iplTeamListAdapter.notifyDataSetChanged();
                    return false;
                  }

                  @Override
                  public boolean onQueryTextChange(String s) {
                    // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                    iplTeamListAdapter.getFilter().filter(s);
                    iplTeamListAdapter.notifyDataSetChanged();
                    return false;
                  }
                });

              }


            }
//
          } catch (JSONException e) {
            e.printStackTrace();
            pbIplTeamList.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }

        @Override
        public void onError(ANError anError) {
          CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception));
          pbIplTeamList.setVisibility(View.GONE);

        }
      });
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }


}
