package in.msewa.fragment.fragmentgiftcard;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import in.msewa.adapter.AmountAdapter;
import in.msewa.adapter.GiftCardItemAdapter;
import in.msewa.custom.AESCrypt;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.GiftCardCatModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveCartListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;


/**
 * Created by kashifimam on 21/02/17.
 */

public class PurchaseFragment extends Fragment implements View.OnClickListener {

  private View rootView;

  private EditText etFname, etLname, etEmail, etMobile, etStreet, etCity, etRegion, etDistrict, etPostalCode;
  private Button btnpayment;
  private TextView tvdenName;
  private TextInputLayout ilRegName, ilRegMobile, ilRegEmail, ilmsg;
  private String Fname, Lname, Email, Mobile, Street, City, Region, District, PostalCode;

  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private LoadingDialog loadDlg;
  private RecyclerView rvGiftCarditem;
  private LinearLayout llpbGiftCarditem;
  private String itemhash, itemImage, brandName;
  private UserModel session = UserModel.getInstance();
  private String tag_json_obj = "json_events";
  private JsonObjectRequest postReq;
  private JSONObject jsonRequests;
  private AQuery aq;


  private GiftCardItemAdapter itemAdp;
  private GiftCardCatModel giftCardCatModel;
  private RecyclerView llAmount;

  private final Pattern hasUppercase = Pattern.compile("[A-Z]");
  private final Pattern hasLowercase = Pattern.compile("[a-z]");
  private final Pattern hasNumber = Pattern.compile("\\d");
  private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");
  Calendar cl = Calendar.getInstance();

  private HashMap<String, String> hashMap;

  private String Ammount = "0";
  int postion;
  private TextView[] myTextViews;
  private Button[] button;
  private AmountAdapter mAdapter;
  private LinearLayoutManager mLayoutManager;
  private TextView tvExtraCharge;
  private JSONObject jsonRequest;
  private GiftCardCatModel model;
  private UserModel userModel = UserModel.getInstance();
  private EditText etAmount;
  private TextInputLayout tlLayout;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
  }


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


    rootView = inflater.inflate(R.layout.fragment_purchases, container, false);
    hashMap = new HashMap<>();
    etFname = (EditText) rootView.findViewById(R.id.etFName);
    etLname = (EditText) rootView.findViewById(R.id.etLName);
    etEmail = (EditText) rootView.findViewById(R.id.etEmail);
    etMobile = (EditText) rootView.findViewById(R.id.etMobile);
    etStreet = (EditText) rootView.findViewById(R.id.etStreet);
    etCity = (EditText) rootView.findViewById(R.id.etCity);
    etRegion = (EditText) rootView.findViewById(R.id.etRegion);
    etDistrict = (EditText) rootView.findViewById(R.id.etDist);
    etPostalCode = (EditText) rootView.findViewById(R.id.etPincode);
    tvExtraCharge = (TextView) rootView.findViewById(R.id.tvExtraCharge);
    etAmount = (EditText) rootView.findViewById(R.id.etAmount);
    tlLayout = (TextInputLayout) rootView.findViewById(R.id.tlLayout);

    //tvdenName = (TextView) rootView.findViewById(R.id.tvdenName);

    btnpayment = (Button) rootView.findViewById(R.id.btnRegPay);
    llAmount = (RecyclerView) rootView.findViewById(R.id.recycler_view);
    llAmount.setHasFixedSize(true);

    // The number of Columns
    mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
    llAmount.setLayoutManager(mLayoutManager);

    etFname.setText(session.getUserFirstName());
    etLname.setText(session.getUserLastName());
    etEmail.setText(session.getUserEmail());

    etMobile.setText(session.getUserMobileNo());
    rvGiftCarditem = (RecyclerView) rootView.findViewById(R.id.recycler_view);
    llpbGiftCarditem = (LinearLayout) rootView.findViewById(R.id.llpbGiftCarditem);
    Bundle bundle = getArguments();
    giftCardCatModel = bundle.getParcelable("model");
    tlLayout.setHint("Min Amount :" + giftCardCatModel.getMin_custom_price() + ",MaxAmount:" + giftCardCatModel.getMax_custom_price());
    assert giftCardCatModel != null;
    if (giftCardCatModel.getCustom_denominations() != null && !giftCardCatModel.getCustom_denominations().isEmpty() && !giftCardCatModel.getCustom_denominations().equalsIgnoreCase("null")) {
      etAmount.setVisibility(View.GONE);
      tlLayout.setVisibility(View.GONE);
      String[] amounts = giftCardCatModel.getCustom_denominations().split(",");
      mAdapter = new AmountAdapter(getActivity(), amounts, new AddRemoveCartListner() {
        @Override
        public void taskCompleted(String response) {
          Log.i("amoubhfsagf", response);
          Ammount = response;
        }
      });
      llAmount.setAdapter(mAdapter);
    } else {
      etAmount.setVisibility(View.VISIBLE);
      tlLayout.setVisibility(View.VISIBLE);
    }

    etAmount.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        Ammount = String.valueOf(s);
      }
    });
    getCardCategory();
//        Button btnRegPay = (Button) rootView.findViewById(R.id.btnRegPay);
    btnpayment.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        submitForm();
      }
    });
    return rootView;
  }

  private void submitForm() {

    if (!validateFName()) {
      return;
    }
//    if (!validateLName()) {
//      return;
//    }
    if (!validateEmail()) {
      return;
    }
    if (!validateMobile()) {
      return;
    }
//    if (!validateStreet()) {
//      return;
//    }
//    if (!validateRegion()) {
//      return;
//    }
//    if (!validateCity()) {
//      return;
//    }
//    if (!validateDistrict()) {
//      return;
//    }
//    if (!validatePostel()) {
//      return;
//    }
    if (Ammount.equalsIgnoreCase("0")) {
      if (TextUtils.isEmpty(etAmount.getText().toString())) {
        etAmount.setError("Amount required");
      } else {
        Ammount = etAmount.getText().toString();
      }
    }


//        loadDlg.show();
//
    Fname = etFname.getText().toString();
    Lname = etLname.getText().toString();
    Email = etEmail.getText().toString();
    Mobile = etMobile.getText().toString();
//    Street = etStreet.getText().toString();
//    City = etCity.getText().toString();
//    Region = etRegion.getText().toString();
//    District = etDistrict.getText().toString();
//    PostalCode = etPostalCode.getText().toString();


//        address = etRegMobile.getText().toString();
       /* city = etCity.getText().toString();
        state = etState.getText().toString();
        country = etCountry.getText().toString();*/


//        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//        date = df.format(cl.getTime());
//
//        Log.i("DATE", "DATE : " + date);
//        Log.i("HAsoMap", hashMap.toString());
//        if (Ammount != null && !Ammount.toString().isEmpty()) {
//            String amout = Ammount.replace("₹", "");
//            Log.i("amout", amout.trim());
//            try {
//                JSONObject jsonObject = new JSONObject(hashMap);
//                promotePay(jsonObject.get(amout.trim()).toString());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            String s = null;
//            for (String key : hashMap.keySet()) {
//                if (key.equalsIgnoreCase(amout)) {
//                    s = hashMap.get(key);

//
//                }


//            }
//            if(s!=null){
//                Log.i("amout1",s);
//                promotePay(s);
//            }
//            Log.i("gaspMap", hashMap.get(amout).toString());
//
//        } else {
//            CustomToast.showMessage(getActivity(), "please select Amount");
//        }
    Log.i("values", Ammount);
    if (Double.parseDouble(Ammount) >= Double.parseDouble(giftCardCatModel.getMin_custom_price()) && (Double.parseDouble(Ammount) <= Double.parseDouble(giftCardCatModel.getMax_custom_price()))) {
      promotePay();
    } else {
      CustomToast.showMessage(getActivity(), "Amount should be between:" + giftCardCatModel.getMin_custom_price() + "-" + giftCardCatModel.getMax_custom_price());
    }

  }


  private boolean validateFName() {
    if (etFname.getText().toString().trim().isEmpty()) {
      etFname.setError("Enter First Name");
      requestFocus(etFname);
      return false;
    } else {
      etFname.setError(null);
    }

    return true;
  }

  private boolean validateLName() {
    if (etLname.getText().toString().trim().isEmpty()) {
      etLname.setError("Enter Last Name");
      requestFocus(etLname);
      return false;
    } else {
      etLname.setError(null);
    }

    return true;
  }

  private boolean validateEmail() {
    String email = etEmail.getText().toString().trim();
    if (email.isEmpty() || !isValidEmail(email)) {
      etEmail.setError("Enter valid email");
      requestFocus(etEmail);
      return false;
    } else {
      etEmail.setError(null);
    }

    return true;
  }

  private boolean validateMobile() {
    if (etMobile.getText().toString().trim().isEmpty() || etMobile.getText().toString().trim().length() < 10) {
      etMobile.setError("Enter 10 digit mobile no");
      requestFocus(etMobile);
      return false;
    } else {
      etMobile.setError(null);
    }

    return true;
  }

  private boolean validateStreet() {
    if (etStreet.getText().toString().trim().isEmpty()) {
      etStreet.setError("Enter Street Name");
      requestFocus(etStreet);
      return false;
    } else {
      etStreet.setError(null);
    }

    return true;
  }

  private boolean validateCity() {
    if (etCity.getText().toString().trim().isEmpty()) {
      etCity.setError("Enter City Name");
      requestFocus(etCity);
      return false;
    } else {
      etCity.setError(null);
    }

    return true;
  }

  private boolean validateRegion() {
    if (etRegion.getText().toString().trim().isEmpty()) {
      etRegion.setError("Enter Region Name");
      requestFocus(etRegion);
      return false;
    } else {
      etRegion.setError(null);
    }

    return true;
  }


  private boolean validateDistrict() {
    if (etDistrict.getText().toString().trim().isEmpty()) {
      etDistrict.setError("Enter City Name");
      requestFocus(etDistrict);
      return false;
    } else {
      etDistrict.setError(null);
    }

    return true;
  }

  private boolean validatePostel() {
    if (etPostalCode.getText().toString().trim().isEmpty() || etPostalCode.getText().toString().trim().length() < 6) {
      etPostalCode.setError("Enter 10 digit mobile no");
      requestFocus(etPostalCode);
      return false;
    } else {
      etPostalCode.setError(null);
    }

    return true;
  }

  private boolean validateAmount() {
    if (etAmount.getText().toString().trim().isEmpty()) {
      etAmount.setError("Enter Amount");
      requestFocus(etAmount);
      return false;
    } else {
      etAmount.setError(null);
    }

    return true;
  }

  private static boolean isValidEmail(String email) {
    return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }

  public void requestFocus(View view) {
    if (view.requestFocus()) {
      getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }


  public void getCardCategory() {

    try {
      loadDlg.show();
      JSONArray jsonArray = new JSONArray();
      jsonArray.put(giftCardCatModel.getProduct_id());

      jsonRequest = new JSONObject();
      jsonRequest.put("sessionId", userModel.getUserSessionId());
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("productIds", jsonArray);
      jsonObject.put("brandCode", giftCardCatModel.getBrand_code());
      jsonRequest.put("data", AESCrypt.encrypt(jsonObject.toString().trim()));
      Log.i("JsonRequest", jsonObject.toString());
    } catch (JSONException e) {
      e.printStackTrace();
      loadDlg.dismiss();
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      final JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_WHOOHOO_PRICE_RECHECK, jsonRequest, new Response.Listener<JSONObject>() {

        @Override
        public void onResponse(JSONObject jsonObj) {
          loadDlg.dismiss();
          try {
            String success = jsonObj.getString("success");
            String message = jsonObj.getString("message");
            String code = jsonObj.getString("code");

            loadDlg.dismiss();
            if (success != null && success.equals("true")) {
              if (!jsonObj.isNull("handling_amount")) {
                tvExtraCharge.setText(getActivity().getResources().getString(R.string.rupease) + " " + jsonObj.getString("handling_amount"));
              }


            } else {
              loadDlg.dismiss();
              if (message.equalsIgnoreCase("Please login and try again.")) {
                showInvalidSessionDialog(message);

              } else if (code.equalsIgnoreCase("F03")) {
                showInvalidSessionDialog(message );
              } else {
                CustomToast.showMessage(getActivity(), message);
              }
            }


          } catch (
            JSONException e)

          {
            loadDlg.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }

      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          Log.i("Type", "ERro");
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
          loadDlg.dismiss();


        }
      })
//                {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "12345");
//                    return map;
//                }

//            }
        ;
      int socketTimeout = 60000 * 5;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  private void promotePay() {
    loadDlg.show();


    jsonRequests = new JSONObject();
    JSONObject jsonObject = new JSONObject();
    try {

//            String[] demo = s.split(",");

      jsonObject.put("firstname", Fname);
      jsonObject.put("lastname", "");
      jsonObject.put("email", Email);
      jsonObject.put("telephone", Mobile);
      jsonObject.put("line_1", "17,1st Main Rd");
      jsonObject.put("line_2", "17,1st Main Rd");
      jsonObject.put("district", "Bangaluru");
      jsonObject.put("city", "Bangalore");
      jsonObject.put("region", "Koramangala Industrial Layout");
      jsonObject.put("country_id", "In");
      jsonObject.put("postcode", "560034");
      jsonObject.put("product_id", giftCardCatModel.getProduct_id());
      jsonObject.put("price", Ammount);
      jsonObject.put("qty", "1");
      jsonObject.put("brandCode", giftCardCatModel.getBrand_code());

      jsonRequests.put("data", AESCrypt.encrypt(StringEscapeUtils.unescapeJava(jsonObject.toString()).replaceAll("\\n", "")).replaceAll("\\\\/", ""));
      jsonRequests.put("sessionId", userModel.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
//      jsonRequests = null;
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (jsonRequests != null) {
      Log.i("CardRequest", jsonRequests.toString());
      Log.i("Registration API", ApiUrl.URL_WHOOHOO_PROCESS_TRANSACTION);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_WHOOHOO_PROCESS_TRANSACTION, jsonRequests, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("Registration Response", response.toString());
            String messageAPI = response.getString("message");
            String code = response.getString("code");
            loadDlg.dismiss();
            if (code != null && code.equals("T01")) {
//              Intent mainMenuDetailActivity = new Intent(getActivity(), WebViewActivity.class);
//              mainMenuDetailActivity.putExtra("TYPE", "GIFTCARD");
//              mainMenuDetailActivity.putExtra("SUBTYPE", "");
//              mainMenuDetailActivity.putExtra("splitAmount", Ammount.replace("₹", "").trim());
//              mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_GIFT_CARD_ORDER);
//              mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequests.toString());
//              startActivity(mainMenuDetailActivity);

            } else if (code != null && code.equals("S00")) {

              showSuccessDialog("", messageAPI + " Your Transaction ID is " + response.getString("transactionRefNo"), response.getString("remainingBalance"));
            } else if (code.equalsIgnoreCase("f11")) {
              CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, messageAPI);
              customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  sendRefresh();
                  getActivity().finishAffinity();
                  startActivity(new Intent(getActivity(), HomeMainActivity.class));
                }
              });
              customAlertDialog.show();
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(messageAPI);
            } else {
              loadDlg.dismiss();
              if (messageAPI.equalsIgnoreCase("Please login and try again.")) {
                showInvalidSessionDialog(messageAPI);

              } else {
                CustomToast.showMessage(getActivity(), messageAPI);
              }
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            Log.i("Type", "jSON");
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          Log.i("Type", "ERro");
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
          loadDlg.dismiss();


        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "12345");
          return map;
        }

      };
      int socketTimeout = 60000 * 5;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  //
  public void showSuccessDialog(String message, String Title, final String balance) {
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), Title, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        session.setUserBalance(balance);
        session.save();
        sendRefresh();

      }
    });
    builder.show();
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void onClick(View v) {

  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    Intent mainActivityIntent = new Intent(getActivity(), HomeMainActivity.class);
    startActivity(mainActivityIntent);
    getActivity().finish();
  }


  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }

}
