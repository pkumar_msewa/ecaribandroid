package in.msewa.fragment.fragmentgiftcard;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.msewa.adapter.GiftCardCatAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.SpringRecyclerView;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.GiftCardCatModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;

/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardCatFragment extends AppCompatActivity {

  private SpringRecyclerView rvGiftCardCat;
  private ProgressBar pbGiftCardCat;
  private UserModel session = UserModel.getInstance();
  String cardTerms;

  //Volley
  private String tag_json_obj = "json_events";
  private JsonObjectRequest postReq;
  private int categoryId;
  private SearchView searchView;
  private android.support.v7.widget.Toolbar toolbar;
  private GiftCardCatAdapter eveCatAdp;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_gift_card);
    toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    rvGiftCardCat = (SpringRecyclerView) findViewById(R.id.rvGiftCardCat);
    pbGiftCardCat = (ProgressBar) findViewById(R.id.pbGiftCardCat);
    ImageView ivBackBtn = (ImageView) findViewById(R.id.ivBackBtn);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
//    categoryId = getArguments().getInt("values");
//    Log.i("valuesss", String.valueOf(categoryId));
    getCardCategory(1);
//    SearchManager searchManager = (SearchManager) GiftCardCatFragment.this.getSystemService(Context.SEARCH_SERVICE);
    searchView = (SearchView) findViewById(R.id.searchView);
//    searchView.setSearchableInfo(searchManager.getSearchableInfo(GiftCardCatFragment.this.getComponentName()));
    searchView.setMaxWidth(Integer.MAX_VALUE);
    searchView.setClickable(true);
    searchView.setVisibility(View.GONE);
//    searchView.setQueryHint("Search for Gift Card Brands");
//    EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//    searchEditText.setTextSize(12);
//    searchView.onActionViewExpanded();
//    searchEditText.setTextColor(getResources().getColor(R.color.white));
//    searchEditText.setHintTextColor(getResources().getColor(R.color.white));
//    searchEditText.setHint("Search for Gift Card Brands");

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.image_flight_filter, menu);

    MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
    searchView = (SearchView) myActionMenuItem.getActionView();
    searchView.setQueryHint("Search for Gift Card Brands");
    EditText editText = ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
    editText.setHintTextColor(getResources().getColor(android.R.color.black));
    editText.setTextColor(getResources().getColor(android.R.color.black));
    editText.setHint("Search for Gift Card Brands");
    editText.setTextSize(14);
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        // Toast like print

        if (!searchView.isIconified()) {
          searchView.setIconified(true);
        }
        eveCatAdp.getFilter().filter(query);
        eveCatAdp.notifyDataSetChanged();
        toolbar.collapseActionView();
        return false;
      }

      @Override
      public boolean onQueryTextChange(String s) {
        // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
        if (s.length() != 0) {
          eveCatAdp.getFilter().filter(s);
          eveCatAdp.notifyDataSetChanged();
        }
        return false;
      }
    });
    return true;
  }

  public void getCardCategory(int categoryId) {
    pbGiftCardCat.setVisibility(View.VISIBLE);
    rvGiftCardCat.setVisibility(View.GONE);
    final JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
    }
    AndroidNetworking.post(ApiUrl.URL_WHOOHOO_CATEGORIES)
      .setTag("test")
      .addJSONObjectBody(jsonRequest)
      .setPriority(Priority.HIGH)
      .build()
      .getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            Log.i("EventsCatResponse", jsonObj.toString());
            String code = jsonObj.getString("code");
            boolean success = jsonObj.getBoolean("success");
            String message = jsonObj.getString("message");
            if (success) {
              List<GiftCardCatModel> cardListCatArray = new ArrayList<>();
              if (!jsonObj.isNull("product")) {
                String jsonDetail = jsonObj.getString("product");
                JSONArray cardCatArray = new JSONArray(jsonDetail);
                for (int i = 0; i < cardCatArray.length(); i++) {
                  JSONObject c = cardCatArray.getJSONObject(i);
                  String product_id = c.getString("product_id");
                  String brand_id = c.getString("brand_id");
                  String brand_name = c.getString("brand_name");
                  String images = c.getString("imageUrl");
                  String base_image = c.getString("tempUrl");
                  String min_custom_price = c.getString("min_custom_price");
                  String max_custom_price = c.getString("max_custom_price");
                  String custom_denominations = c.getString("custom_denominations");
                  String brandCode = c.getString("brandCode");
                  String tnc_mobile = "null";
                  if (c.isNull("termsAndAcondition")) {
                    tnc_mobile = "null";
                  } else {
                    tnc_mobile = c.getString("termsAndAcondition");
                  }
                  if (!c.isNull("imageUrl") && images.equalsIgnoreCase("null")) {
                    GiftCardCatModel eveCatModel = new GiftCardCatModel(product_id, brand_id, brand_name, "https://woohooproducts.s3.amazonaws.com/" + product_id + ".jpeg", base_image, min_custom_price, max_custom_price, custom_denominations, tnc_mobile, brandCode);
                    cardListCatArray.add(eveCatModel);
                  } else {
                    GiftCardCatModel eveCatModel = new GiftCardCatModel(product_id, brand_id, brand_name, "https://woohooproducts.s3.amazonaws.com/" + product_id + ".jpeg", images, min_custom_price, max_custom_price, custom_denominations, tnc_mobile, brandCode);
                    cardListCatArray.add(eveCatModel);
                  }
                }
                if (cardListCatArray.size() != 0) {
                  int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                  rvGiftCardCat.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                  final LinearLayoutManager manager = new LinearLayoutManager(GiftCardCatFragment.this);

                  rvGiftCardCat.setLayoutManager(manager);
                  rvGiftCardCat.setHasFixedSize(true);


                  Collections.sort(cardListCatArray, new Comparator<GiftCardCatModel>() {
                    public int compare(GiftCardCatModel p1, GiftCardCatModel p2) {
                      return p1.getBrand_name().compareTo(p2.getBrand_name());
                    }
                  });


                  eveCatAdp = new GiftCardCatAdapter(GiftCardCatFragment.this, cardListCatArray);

                  rvGiftCardCat.setAdapter(eveCatAdp);
                  pbGiftCardCat.setVisibility(View.GONE);
                  rvGiftCardCat.setVisibility(View.VISIBLE);
//                  searchView.setVisibility(View.VISIBLE);
//                  searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//                    @Override
//                    public boolean onQueryTextSubmit(String query) {
//                      // filter recycler view when query submitted
//                      eveCatAdp.getFilter().filter(query);
//                      eveCatAdp.notifyDataSetChanged();
//                      return false;
//                    }
//
//                    @Override
//                    public boolean onQueryTextChange(String query) {
//                      // filter recycler view when text is changed
//                      eveCatAdp.getFilter().filter(query);
//                      eveCatAdp.notifyDataSetChanged();
//                      return false;
//                    }
//                  });

                } else {
                  pbGiftCardCat.setVisibility(View.GONE);
                }
              } else {
                CustomToast.showMessage(GiftCardCatFragment.this, message);
                GiftCardCatFragment.this.finish();
              }

            } else if (code.equalsIgnoreCase("F03")) {
              showInvalidSessionDialog(message);
            } else {
              if (message.equalsIgnoreCase("Please login and try again.")) {
                showInvalidSessionDialog(message);
                pbGiftCardCat.setVisibility(View.GONE);
              } else {
                CustomToast.showMessage(GiftCardCatFragment.this, message);
                pbGiftCardCat.setVisibility(View.GONE);
              }
            }

          } catch (JSONException e) {
            e.printStackTrace();
            pbGiftCardCat.setVisibility(View.GONE);
            Toast.makeText(GiftCardCatFragment.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }

        }

        @Override
        public void onError(ANError anError) {
          CustomToast.showMessage(GiftCardCatFragment.this, getResources().getString(R.string.server_exception));
          pbGiftCardCat.setVisibility(View.GONE);
        }
      });
  }


  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }

  }


  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(GiftCardCatFragment.this, R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(GiftCardCatFragment.this).sendBroadcast(intent);
  }
}


//package in.msewa.fragment.fragmentgiftcard;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Rect;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v4.view.ViewPager;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.text.Html;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.ProgressBar;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.StringRequest;
//import com.androidnetworking.AndroidNetworking;
//import com.androidnetworking.common.Priority;
//import com.androidnetworking.error.ANError;
//import com.androidnetworking.interfaces.StringRequestListener;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import in.msewa.adapter.GiftCardCatAdapter;
//import in.msewa.custom.CustomAlertDialog;
//import in.msewa.custom.CustomToast;
//import in.msewa.custom.LoadingDialog;
//import in.msewa.fragment.fragmentbrowseplanPrepaid.FullTalkTimeFragment;
//import in.msewa.fragment.fragmentbrowseplanPrepaid.SpecialPlanFragment;
//import in.msewa.fragment.fragmentbrowseplanPrepaid.ThreeGFragment;
//import in.msewa.fragment.fragmentbrowseplanPrepaid.TopUpFragment;
//import in.msewa.fragment.fragmentbrowseplanPrepaid.TwoGFragment;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.metadata.AppMetadata;
//import in.msewa.model.GiftCardCatModel;
//import in.msewa.model.MobilePlansModel;
//import in.msewa.model.UserModel;
//import in.msewa.util.MobilePlansCheck;
//import in.msewa.vpayqwiktest.PayQwikApplication;
//import in.msewa.vpayqwiktest.R;
//import in.msewa.vpayqwiktest.activity.BrowsePrepaidPlanActivity;
//
///**
// * Created by kashifimam on 21/02/17.
// */
//
//public class GiftCardCatFragment extends Fragment {
//
//  private RecyclerView rvGiftCardCat;
//  private ProgressBar pbGiftCardCat;
//  private UserModel session = UserModel.getInstance();
//  String cardTerms;
//
//  //Volley
//
//  private JsonObjectRequest postReq;
//  private ImageView ivBanner;
//
//  private TabLayout mSlidingTabLayout;
//  private ViewPager mainPager;
//  private Toolbar tbMobileTopUp;
//  private FragmentManager fragmentManager;
//  CharSequence TitlesEnglish[] = {"Full talktime", "Special", "2G", "3G", "Top up"};
//  int NumbOfTabs = 5;
//
//  private String operatorCode;
//  private String circleCode;
//
//  private ImageButton ivBackBtn;
//  private Toolbar toolbar;
//
//  private JSONObject jsonRequest;
//  private LoadingDialog loadingDialog;
//
//  //Volley Tag
//  private String tag_json_obj = "json_browse_plan";
//
//  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//    View rootView = inflater.inflate(R.layout.fragment_gift_card, container, false);
////    rvGiftCardCat = (RecyclerView) findViewById(R.id.rvGiftCardCat);
////    pbGiftCardCat = (ProgressBar) findViewById(R.id.pbGiftCardCat);
//    ivBanner = (ImageView) findViewById(R.id.ivBanner);
////    getCardCategory();
//
//    loadingDialog = new LoadingDialog(GiftCardCatFragment.this);
//
//    mainPager = (ViewPager) findViewById(R.id.vpPage);
//    mSlidingTabLayout = (TabLayout) findViewById(R.id.tabLayout);
//
//    mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//    mSlidingTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
//    browsePlan();
//    return rootView;
//
//
//  }
//
//  public class ViewPagerAdapter extends FragmentStatePagerAdapter {
//
//    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
//    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
//
//
//    // Build a Constructor and assign the passed Values to appropriate values in the class
//    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
//      super(fm);
//      this.Titles = mTitles;
//      this.NumbOfTabs = mNumbOfTabsumb;
//
//    }
//
//    @Override
//    public Fragment getItem(int position) {
//
//      if (position == 0) {
//        return new FullTalkTimeFragment();
//      } else if (position == 1) {
//        return new SpecialPlanFragment();
//      } else if (position == 2) {
//        return new TwoGFragment();
//      } else if (position == 3) {
//        return new ThreeGFragment();
//      } else {
//        return new TopUpFragment();
//      }
//    }
//
//    @Override
//    public CharSequence getPageTitle(int position) {
//      return Titles[position];
//    }
//
//
//    @Override
//    public int getCount() {
//      return NumbOfTabs;
//    }
//  }
//
//
//  private void browsePlan() {
//
//    loadingDialog.show();
////    http://66.207.206.54:8089//Api/v1/User/Android/en/Woohoo/categories
//    Log.i("BrowseURL", ApiUrl.URL_WHOOHOO_CATEGORIES);
//    AndroidNetworking.get(ApiUrl.URL_WHOOHOO_CATEGORIES)
//      .setTag("test")
//      .setPriority(Priority.HIGH)
//      .build().getAsString(new StringRequestListener() {
//      @Override
//      public void onResponse(String response) {
//        Log.i("Strng Response", response);
//      }
//
//      @Override
//      public void onError(ANError anError) {
//        Log.i("Strng Response", anError.getErrorBody());
//        loadingDialog.dismiss();
//        CustomToast.showMessage(GiftCardCatFragment.this, getResources().getString(R.string.server_exception));
//      }
//    });
//    StringRequest postReq = new StringRequest(Request.Method.GET, ApiUrl.URL_WHOOHOO_CATEGORIES, new Response.Listener<String>() {
//      @Override
//      public void onResponse(String jsonObj) {
//        loadingDialog.dismiss();
//        try {
//          JSONObject jsonObject = new JSONObject(jsonObj);
//          String code = jsonObject.getString("code");
//          if (code.equalsIgnoreCase("200")) {
//            JSONArray categoryList = jsonObject.getJSONArray("categoryList");
//            Log.i("API RESPOMSE", categoryList.toString());
//
//          }
//        } catch (JSONException e) {
//          e.printStackTrace();
//        }
//
//
//      }
//    }, new Response.ErrorListener() {
//      @Override
//      public void onErrorResponse(VolleyError error) {
//        loadingDialog.dismiss();
//        error.printStackTrace();
//        CustomToast.showMessage(GiftCardCatFragment.this, getResources().getString(R.string.server_exception));
//
//      }
//    }) {
//      @Override
//      public Map<String, String> getHeaders() throws AuthFailureError {
//        HashMap<String, String> map = new HashMap<>();
//        map.put("hash", "1234");
//        return map;
//      }
//
//    };
//    int socketTimeout = 60000;
//    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//    postReq.setRetryPolicy(policy);
//    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//
//  }
//
//
//  public static final int getColors(Context context, int id) {
//    final int version = Build.VERSION.SDK_INT;
//    if (version >= 23) {
//      return ContextCompat.getColor(context, id);
//    } else {
//      return context.getResources().getColor(id);
//    }
//  }
//
//
////  @Override
////  public void onCreate(@Nullable Bundle savedInstanceState) {
////    super.onCreate(savedInstanceState);
////    setHasOptionsMenu(true);
////
////  }
////
////  @Nullable
////  @Override
////  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
////    View rootView = inflater.inflate(R.layout.fragment_gift_card, container, false);
////    rvGiftCardCat = (RecyclerView) findViewById(R.id.rvGiftCardCat);
////    pbGiftCardCat = (ProgressBar) findViewById(R.id.pbGiftCardCat);
////    ivBanner = (ImageView) findViewById(R.id.ivBanner);
////    getCardCategory();
////    return rootView;
////  }
////
////
////  public void getCardCategory() {
////    pbGiftCardCat.setVisibility(View.VISIBLE);
////    rvGiftCardCat.setVisibility(View.GONE);
////    final JSONObject jsonRequest = new JSONObject();
////    try {
////      jsonRequest.put("sessionId", session.getUserSessionId());
////    } catch (JSONException e) {
////      e.printStackTrace();
////    }
////
////    if (jsonRequest != null) {
////      Log.i("CardCatUrl", ApiUrl.URL_GIFT_CARD_CAT);
////      Log.i("Request : ", jsonRequest.toString());
////      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GIFT_CARD_CAT, jsonRequest, new Response.Listener<JSONObject>() {
////        @Override
////        public void onResponse(JSONObject jsonObj) {
////          try {
////            Log.i("EventsCatResponse", jsonObj.toString());
////            String code = jsonObj.getString("success");
////            String message = jsonObj.getString("message");
////
////            if (code != null && code.equals("true")) {
////              List<GiftCardCatModel> cardListCatArray = new ArrayList<>();
////              String jsonDetail = jsonObj.getString("brandObject");
////
////              JSONArray cardCatArray = new JSONArray(jsonDetail);
////
////              for (int i = 0; i < cardCatArray.length(); i++) {
////                JSONObject c = cardCatArray.getJSONObject(i);
////                boolean cardSuccess = c.getBoolean("success");
////                String cardMessage = c.getString("message");
////                String cardHash = c.getString("hash");
////                String cardName = c.getString("name");
////                String cardDescription = c.getString("description");
////                String cardTerms = c.getString("terms");
////                String cardImage = c.getString("images");
////                String cardWebsite = c.getString("website");
////                boolean cardActive = c.getBoolean("active");
////                boolean cardOnline = c.getBoolean("online");
////                GiftCardCatModel eveCatModel = new GiftCardCatModel(cardHash, cardName, cardImage, cardDescription, cardTerms, cardMessage, cardWebsite, cardActive, cardOnline, cardSuccess);
////                cardListCatArray.add(eveCatModel);
////              }
////              if (cardListCatArray != null && cardListCatArray.size() != 0) {
////                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
////                rvGiftCardCat.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
////                GridLayoutManager manager = new GridLayoutManager(GiftCardCatFragment.this, 2);
////                rvGiftCardCat.setLayoutManager(manager);
////                rvGiftCardCat.setHasFixedSize(true);
////
////                GiftCardCatAdapter eveCatAdp = new GiftCardCatAdapter(GiftCardCatFragment.this, cardListCatArray);
////                rvGiftCardCat.setAdapter(eveCatAdp);
////                pbGiftCardCat.setVisibility(View.GONE);
////                rvGiftCardCat.setVisibility(View.VISIBLE);
////
////              } else {
////                Toast.makeText(GiftCardCatFragment.this, "Empty Array", Toast.LENGTH_SHORT).show();
////                pbGiftCardCat.setVisibility(View.GONE);
////              }
////            } else if (code.equalsIgnoreCase("F03")) {
////              showInvalidSessionDialog();
////            } else {
////              if (message.equalsIgnoreCase("Please login and try again.")) {
////                showInvalidSessionDialog();
////                pbGiftCardCat.setVisibility(View.GONE);
////              } else {
////                CustomToast.showMessage(GiftCardCatFragment.this, message);
////                pbGiftCardCat.setVisibility(View.GONE);
////              }
////            }
////
////          } catch (JSONException e) {
////            e.printStackTrace();
////            pbGiftCardCat.setVisibility(View.GONE);
////            Toast.makeText(GiftCardCatFragment.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
////          }
////        }
////      }, new Response.ErrorListener() {
////        @Override
////        public void onErrorResponse(VolleyError error) {
////          error.printStackTrace();
////          CustomToast.showMessage(GiftCardCatFragment.this, "Error connecting to server");
////          pbGiftCardCat.setVisibility(View.GONE);
////        }
////      }) {
////        @Override
////        public Map<String, String> getHeaders() throws AuthFailureError {
////          HashMap<String, String> map = new HashMap<>();
////          map.put("hash", "12345");
////          return map;
////        }
////
////      };
////      int socketTimeout = 60000;
////      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////      postReq.setRetryPolicy(policy);
////      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
////    }
////  }
////
////  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
////    private int space;
////
////    public SpacesItemDecoration(int space) {
////      this.space = space;
////    }
////
////    @Override
////    public void getItemOffsets(Rect outRect, View view,
////                               RecyclerView parent, RecyclerView.State state) {
////      outRect.left = space;
////      outRect.right = space;
////      outRect.bottom = space;
////      outRect.top = space;
////    }
////  }
////
////
////  @Override
////  public void onDetach() {
////    postReq.cancel();
////    super.onDetach();
////  }
////
////  public void showInvalidSessionDialog() {
////    CustomAlertDialog builder = new CustomAlertDialog(GiftCardCatFragment.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
////    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
////      public void onClick(DialogInterface dialog, int id) {
////        sendLogout();
////      }
////    });
////    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
////      public void onClick(DialogInterface dialog, int id) {
////        dialog.dismiss();
////
////      }
////    });
////    builder.show();
////  }
////
////  private void sendLogout() {
////    Intent intent = new Intent("setting-change");
////    intent.putExtra("updates", "4");
////    LocalBroadcastManager.getInstance(GiftCardCatFragment.this).sendBroadcast(intent);
////  }
//}
