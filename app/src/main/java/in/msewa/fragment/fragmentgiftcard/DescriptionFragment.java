package in.msewa.fragment.fragmentgiftcard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.msewa.model.GiftCardCatModel;
import in.msewa.ecarib.R;


//import in.payqwik.payqwik.activity.AdventureActivity.VideoPlayerActivity;

/**
 * Created by kashifimam on 21/02/17.
 */

public class DescriptionFragment extends Fragment {

  private View rootView;

  private TextView tvDescription;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


    rootView = inflater.inflate(R.layout.fragment_description, container, false);
    Bundle bundle = getArguments();
    GiftCardCatModel giftCardCatModel = bundle.getParcelable("model");
    tvDescription = (TextView) rootView.findViewById(R.id.tvDescription);
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      tvDescription.setText(Html.fromHtml(giftCardCatModel.getTnc_mobile(), Html.FROM_HTML_MODE_LEGACY));
    } else {
      tvDescription.setText(Html.fromHtml(giftCardCatModel.getTnc_mobile()));
    }


//        Bundle args = getArguments();
//        //String categoryId = args.getString("index");
//
//        String categoryDS = getArguments().getString("Model");
//        //String categoryId = getArguments().getString("category_id");
//        TextView textView = (TextView) rootView.findViewById(R.id.label);
//        tvDescription.setText(categoryDS);


    return rootView;
  }
}
