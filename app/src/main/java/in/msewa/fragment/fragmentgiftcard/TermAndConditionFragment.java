package in.msewa.fragment.fragmentgiftcard;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.msewa.model.GiftCardCatModel;
import in.msewa.ecarib.R;

//import in.payqwik.payqwik.activity.AdventureActivity.VideoPlayerActivity;

/**
 * Created by kashifimam on 21/02/17.
 */

public class TermAndConditionFragment extends Fragment {

    private View rootView;

    private TextView tvTerms;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_terms, container, false);
        Bundle bundle = getArguments();
        GiftCardCatModel giftCardCatModel = bundle.getParcelable("model");
        tvTerms = (TextView) rootView.findViewById(R.id.tvTerms);
//        tvTerms.setText(Html.fromHtml(giftCardCatModel.getCardTerms()));




        return rootView;
    }

}
