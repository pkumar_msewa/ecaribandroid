package in.msewa.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ebs.android.sdk.Config.Encryption;
import com.ebs.android.sdk.Config.Mode;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomDisclaimerDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HowToUpgradeActivity;
import in.msewa.ecarib.activity.LoadMoneyActivity;

//import in.msewa.upimerchant.MerchantDetails;
//import in.msewa.vpayqwik.activity.RazorpayPaymentActivity;

/**
 * Created by Ksf on 4/11/2016.
 */
public class LoadMoneyFragment extends Fragment {


  private View rootView;
  private MaterialEditText etLoadMoneyAmount;
  private Button btnLoadMoney;
  private RadioButton rbLoadMoneyVBank, rbLoadMoneyOther;
  private View focusView = null;
  private boolean cancel;
  private String amount = null;
  AlertDialog.Builder payDialog;
  private UserModel session = UserModel.getInstance();
  private LoadingDialog loadDlg;
  private RequestQueue rq;
  private boolean isVBank, isRozpay;
  private String inValidMessage = "";
  private String tag_json_obj = "load_money";
  private JSONObject jsonRequest;
  String autoFill;
  double loadAmount;
  private LinearLayout llsplitBalance;
  private TextView tvsplitBalance, tvTotalBalance;
  private TextView tvWalletBalance;
  private SharedPreferences loadMoneyPref;
  private String transactionId;
  private String Mer_Credentials;
  private RadioButton rbLoadMoneyUpi;
  private boolean isUPi;
  private String splitrequest;
  private String URL;
  private RadioButton rbLoadMoneyRozorpay;
  private View view_under_line;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    payDialog = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
    loadDlg = new LoadingDialog(getActivity());
    try {
      final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
      rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
    } catch (KeyManagementException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_ask_amount_to_load_money, container, false);
    btnLoadMoney = (Button) rootView.findViewById(R.id.btnLoadMoney);
//    rbLoadMoneyRozorpay=
    rbLoadMoneyRozorpay = (RadioButton) rootView.findViewById(R.id.rbLoadMoneyRozorpay);
    etLoadMoneyAmount = (MaterialEditText) rootView.findViewById(R.id.etLoadMoneyAmount);
    rbLoadMoneyVBank = (RadioButton) rootView.findViewById(R.id.rbLoadMoneyVBank);
    view_under_line = (View) rootView.findViewById(R.id.view_under_line);
    rbLoadMoneyOther = (RadioButton) rootView.findViewById(R.id.rbLoadMoneyOther);
    rbLoadMoneyUpi = (RadioButton) rootView.findViewById(R.id.rbLoadMoneyUpi);

    rbLoadMoneyUpi.setChecked(false);
    autoFill = getArguments().getString("AutoFill");
    llsplitBalance = (LinearLayout) rootView.findViewById(R.id.llsplitBalance);
//    if (session.ebsEnable && session.vnetEnable && session.razorPayEnable) {
//      rbLoadMoneyRozorpay.setVisibility(View.VISIBLE);
//      rbLoadMoneyUpi.setVisibility(View.VISIBLE);
//      rbLoadMoneyOther.setVisibility(View.VISIBLE);
//      rbLoadMoneyVBank.setVisibility(View.VISIBLE);
//    } else if (session.ebsEnable) {
//      view_under_line.setVisibility(View.GONE);
//      rbLoadMoneyRozorpay.setVisibility(View.GONE);
//      rbLoadMoneyUpi.setVisibility(View.VISIBLE);
//      rbLoadMoneyOther.setVisibility(View.VISIBLE);
//      rbLoadMoneyVBank.setVisibility(View.GONE);
//
//    } else if (session.vnetEnable) {
//      view_under_line.setVisibility(View.GONE);
//      rbLoadMoneyRozorpay.setVisibility(View.GONE);
//      rbLoadMoneyUpi.setVisibility(View.VISIBLE);
//      rbLoadMoneyOther.setVisibility(View.GONE);
//      rbLoadMoneyVBank.setVisibility(View.VISIBLE);
//    } else if (session.razorPayEnable) {
//      view_under_line.setVisibility(View.VISIBLE);
//      rbLoadMoneyRozorpay.setVisibility(View.VISIBLE);
//      rbLoadMoneyUpi.setVisibility(View.VISIBLE);
//      rbLoadMoneyOther.setVisibility(View.GONE);
//      rbLoadMoneyVBank.setVisibility(View.GONE);
//    }

    if (autoFill.equals("yes")) {
      String loadAmountString = getArguments().getString("splitAmount");
      splitrequest = getArguments().getString("payment");
      URL = getArguments().getString("URL");
      loadAmount = Math.ceil(Double.parseDouble(loadAmountString));
      DecimalFormat format = new DecimalFormat("0.#");

//      if (loadAmount <= 10) {
//        loadAmount = 10.0;
//      }
      llsplitBalance.setVisibility(View.VISIBLE);
      etLoadMoneyAmount.setText(String.valueOf(format.format(loadAmount)));
      etLoadMoneyAmount.setVisibility(View.GONE);
      tvTotalBalance = (TextView) rootView.findViewById(R.id.tvTotalBalance);
      tvWalletBalance = (TextView) rootView.findViewById(R.id.tvWalletBalance);
      tvsplitBalance = (TextView) rootView.findViewById(R.id.tvsplitBalance);
      Double totalBalances = Double.parseDouble(session.getUserBalance()) + loadAmount;
      tvTotalBalance.setText(getResources().getString(R.string.rupease) + String.valueOf(totalBalances));
      tvWalletBalance.setText(getResources().getString(R.string.rupease) + session.getUserBalance());
      tvsplitBalance.setText(getResources().getString(R.string.rupease) + loadAmount);
      btnLoadMoney.setText("Pay");
//            etLoadMoneyAmount.setEnabled(false);
    } else {
      loadMoneyPref = getActivity().getSharedPreferences("type", Context.MODE_PRIVATE);
      SharedPreferences.Editor sharedPreferences = loadMoneyPref.edit();
      sharedPreferences.putString("type", "normal");
      sharedPreferences.apply();
      llsplitBalance.setVisibility(View.GONE);
      etLoadMoneyAmount.setVisibility(View.VISIBLE);
    }

    btnLoadMoney.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (rbLoadMoneyVBank.isChecked()) {
          isVBank = true;
          isUPi = false;
          isRozpay = false;
        } else if (rbLoadMoneyUpi.isChecked()) {
          isUPi = true;
          isVBank = false;
          isRozpay = false;
        } else if (rbLoadMoneyOther.isChecked()) {
          isUPi = false;
          isVBank = false;
          isRozpay = false;
        } else if (rbLoadMoneyRozorpay.isChecked()) {
          isRozpay = true;
          isUPi = false;
          isVBank = false;
        }
        attemptLoad();

      }
    });


    //DONE CLICK ON VIRTUAL KEYPAD
    etLoadMoneyAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          if (rbLoadMoneyVBank.isChecked()) {
            isVBank = true;
            isUPi = false;
          } else if (rbLoadMoneyUpi.isChecked()) {
            isUPi = false;
            isVBank = false;
          } else if (rbLoadMoneyOther.isChecked()) {
            isUPi = false;
            isVBank = false;
          }
          attemptLoad();
        }
        return false;
      }
    });

    return rootView;
  }


  private void attemptLoad() {
    etLoadMoneyAmount.setError(null);
    cancel = false;
    amount = etLoadMoneyAmount.getText().toString();
    if (autoFill.equalsIgnoreCase("Yes")) {
      cancel = false;
    } else {
      checkPayAmount(amount);
    }
//        checkUserType();

    if (cancel) {
      focusView.requestFocus();
    } else {
      if (isVBank) {
        checkTrxTime();
      } else if (isUPi) {
        checkTrxTime();
      } else {
        showCustomDisclaimerDialog();
      }
    }
  }

  public void showCustomDisclaimerDialog() {
    CustomDisclaimerDialog builder = new CustomDisclaimerDialog(getActivity());
    builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        checkTrxTime();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showNonKYCDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateKYCMessage()));
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }


  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    try {
      if (!gasCheckLog.isValid) {
        etLoadMoneyAmount.setError(getString(gasCheckLog.msg));
        focusView = etLoadMoneyAmount;
        cancel = true;
      } else if (Integer.valueOf(amount) < 10) {
        etLoadMoneyAmount.setError("Amount cant be less than 10");
        focusView = etLoadMoneyAmount;
        cancel = true;
      } else if (Integer.valueOf(amount) >= 10000) {
        etLoadMoneyAmount.setError("Enter amount less than 10000");
        focusView = etLoadMoneyAmount;
        cancel = true;
      } else if (autoFill.equals("yes")) {
//        if (Integer.valueOf(amount) < loadAmount) {
//          DecimalFormat format = new DecimalFormat("0.#");
//          etLoadMoneyAmount.setError("Amount cant be less than " + format.format(loadAmount));
//          focusView = etLoadMoneyAmount;
        cancel = true;

      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  private boolean checkUserType() {
    if (amount != null && !amount.isEmpty()) {
      if (Integer.valueOf(amount) > 10000) {
        focusView = etLoadMoneyAmount;
        cancel = true;
        if (session.getUserAcName().equals("Non-KYC")) {
          showNonKYCDialog();
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }

  }

  public String generateMessage() {
    String source = "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
      "<br><b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    return source;
  }


  public String generateKYCMessage() {
    return "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
      "<br><b><font color=#ff0000> Sorry you cannot load more than 10,000 at a time.</font></b><br>" +
      "<br><b><font color=#ff0000> Please enter 10,000 or lesser amount to continue.</font></b><br>";
  }

  public void checkTrxTime() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRX_TIME, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          Log.i("jsonResponse", response.toString());
          try {
            String message = response.getString("message");
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              loadDlg.dismiss();
              boolean success = response.getBoolean("success");
              if (success) {
                verifyTransaction(amount);
              } else {
                CustomToast.showMessage(getActivity(), message);
              }
            } else if (code != null && code.equalsIgnoreCase("F02")) {
              CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, message);
              customAlertDialog.setPositiveButton("UpgradeKYC", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  loadDlg.dismiss();
                  dialog.dismiss();
                  getActivity().startActivity(new Intent(getActivity(), HowToUpgradeActivity.class));
                }
              });
              customAlertDialog.show();
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(message);
            } else {
              loadDlg.dismiss();
              CustomToast.showMessage(getActivity(), message);
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

    }

  }

  private void verifyTransaction(final String amount) {
    loadDlg.show();
    StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRANSACTION, new Response.Listener<String>() {
      @Override
      public void onResponse(String responseString) {
        try {

          JSONObject response = new JSONObject(responseString);

          String code = response.getString("code");
          inValidMessage = response.getString("message");
          if (code != null && code.equals("S00")) {
            loadDlg.dismiss();
            String details = response.getString("details");
            JSONObject jsonDetails = new JSONObject(details);
            boolean isValid = jsonDetails.getBoolean("valid");
            if (isValid) {
              if (isVBank) {
                Intent loadMoneyIntent = new Intent(getActivity(), LoadMoneyActivity.class);
                loadMoneyIntent.putExtra("amountToLoad", amount);
                loadMoneyIntent.putExtra("isVBank", isVBank);
                startActivity(loadMoneyIntent);
              } else {
                loadDlg.show();
                initialLoadMoneyEBS(amount);
              }
//              if (isUPi) {
//                initiateMerchantToken();
//              }
//              if (isRozpay) {
//                if (autoFill.equals("yes")) {
//                  Intent intent = new Intent(getActivity(), RazorpayPaymentActivity.class);
//                  intent.putExtra("splitpayment", true);
//                  intent.putExtra("URL", URL);
//                  intent.putExtra("bokkingrequest", splitrequest);
//                  intent.putExtra("amount", amount);
//                  startActivity(intent);
//                } else {
//                  Intent intent = new Intent(getActivity(), RazorpayPaymentActivity.class);
//                  intent.putExtra("splitpayment", false);
//                  intent.putExtra("amount", amount);
//                  startActivity(intent);
//                }
//              }
//              if (rbLoadMoneyOther.isChecked()) {
//                Initiate and call EBS KIT
//
//              }
            } else {
              loadDlg.dismiss();
              inValidMessage = jsonDetails.getString("message");
              showInvalidTranDialog();
            }
          } else if (code != null && code.equals("F03")) {
            loadDlg.dismiss();
            showInvalidSessionDialog(inValidMessage);
          } else {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), inValidMessage);
          }

        } catch (JSONException e) {
          loadDlg.dismiss();
          e.printStackTrace();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        loadDlg.dismiss();
        error.printStackTrace();
        CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

      }
    }) {
      @Override
      protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("sessionId", session.getUserSessionId());
        params.put("amount", amount);
        return params;
      }

    };
    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(postReq, "test");

  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void showInvalidTranDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, inValidMessage);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void initialLoadMoneyEBS(final String amount) {

    StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_INITIATE_LOAD_MONEY, new Response.Listener<String>() {
      @Override
      public void onResponse(String response) {
        Log.i("test", response.toString());
        try {
          JSONObject resObj = new JSONObject(response);
          boolean successMsg = resObj.getBoolean("success");
          if (successMsg) {
            String referenceNo = resObj.getString("referenceNo");
            String currency = resObj.getString("currency");
            String description = resObj.getString("description");
            String name = resObj.getString("name");
            String email = resObj.getString("email");
            String address = resObj.getString("address");
            String cityName = resObj.getString("city");
            String stateName = resObj.getString("state");
            String countryName = resObj.getString("country");
            String postalCode = resObj.getString("postalCode");
            String shipName = resObj.getString("shipName");
            String shipAddress = resObj.getString("shipAddress");
            String shipCity = resObj.getString("shipCity");
            String shipState = resObj.getString("shipState");
            String shipCountry = resObj.getString("shipCountry");
            String shipPostalCode = resObj.getString("shipPostalCode");
            String shipPhone = resObj.getString("shipPhone");

            int ACC_ID = 20696;
            String SECRET_KEY = "6496e4db9ebf824ffe2269afee259447";
            String HOST_NAME = getResources().getString(R.string.hostname);

            PaymentRequest.getInstance().setTransactionAmount(amount);
            PaymentRequest.getInstance().setAccountId(ACC_ID);
            PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

            PaymentRequest.getInstance().setReferenceNo(referenceNo);
            PaymentRequest.getInstance().setBillingEmail(email);
            PaymentRequest.getInstance().setFailureid("1");
            PaymentRequest.getInstance().setCurrency(currency);
            PaymentRequest.getInstance().setTransactionDescription(description);
            PaymentRequest.getInstance().setBillingName(name);
            PaymentRequest.getInstance().setBillingAddress(address);
            PaymentRequest.getInstance().setBillingCity(cityName);
            PaymentRequest.getInstance().setBillingPostalCode(postalCode);
            PaymentRequest.getInstance().setBillingState(stateName);
            PaymentRequest.getInstance().setBillingCountry(countryName);
            PaymentRequest.getInstance().setBillingPhone(session.getUserMobileNo());
            PaymentRequest.getInstance().setShippingName(shipName);
            PaymentRequest.getInstance().setShippingAddress(shipAddress);
            PaymentRequest.getInstance().setShippingCity(shipCity);
            PaymentRequest.getInstance().setShippingPostalCode(shipPostalCode);
            PaymentRequest.getInstance().setShippingState(shipState);
            PaymentRequest.getInstance().setShippingCountry(shipCountry);
            PaymentRequest.getInstance().setShippingEmail(email);
            PaymentRequest.getInstance().setShippingPhone(shipPhone);
            PaymentRequest.getInstance().setLogEnabled("1");

            PaymentRequest.getInstance().setHidePaymentOption(true);
            PaymentRequest.getInstance().setHideCashCardOption(true);
            PaymentRequest.getInstance().setHideCreditCardOption(false);
            PaymentRequest.getInstance().setHideDebitCardOption(false);
            PaymentRequest.getInstance().setHideNetBankingOption(false);
            PaymentRequest.getInstance().setHideStoredCardOption(true);

            ArrayList<HashMap<String, String>> custom_post_parameters = new ArrayList<>();

            HashMap<String, String> hashpostvalues = new HashMap<>();
            hashpostvalues.put("account_details", "saving");
            hashpostvalues.put("merchant_type", "vpayqwik");
            custom_post_parameters.add(hashpostvalues);

            PaymentRequest.getInstance().setCustomPostValues(custom_post_parameters);
            loadDlg.dismiss();
            EBSPayment.getInstance().init(getActivity(), ACC_ID, SECRET_KEY, Mode.ENV_LIVE, Encryption.ALGORITHM_MD5, HOST_NAME);

          } else {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), resObj.getString("description"));
          }
        } catch (JSONException e) {
          loadDlg.dismiss();
          e.printStackTrace();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        loadDlg.dismiss();
        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
      }
    }) {
      @Override
      protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("sessionId", session.getUserSessionId());
        params.put("amount", amount);
        params.put("name", session.getUserFirstName());
        params.put("email", session.getUserEmail());
        params.put("phone", session.getUserMobileNo());

        return params;
      }

      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("hash", "123");
        return map;
      }
    };

    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
  }

  @Override
  public void onResume() {
    super.onResume();
  }


//  public void initiateMerchantToken() {
//    loadDlg.show();
//    jsonRequest = new JSONObject();
//    try {
//      jsonRequest.put("sessionId", session.getUserSessionId());
//      jsonRequest.put("deviceId", SecurityUtil.getAndroidId(getActivity()));
//      jsonRequest.put("amount", amount);
//
//    } catch (JSONException e) {
//      e.printStackTrace();
//      jsonRequest = null;
//    }
//
//    if (jsonRequest != null) {
//      Log.i("MERTOKENURL", ApiUrl.URL_UPI_MER_GEN_TOKEN);
//      Log.i("MERTOKENREQ", jsonRequest.toString());
//      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_UPI_MER_GEN_TOKEN, jsonRequest, new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject response) {
//          Log.i("MERTOKENRES", response.toString());
//
//          try {
//            String message = response.getString("message");
//            String code = response.getString("code");
//            if (code != null && code.equals("S00")) {
//              loadDlg.dismiss();
//              transactionId = response.getString("transactionId");
//              String merchantVPA = response.getJSONObject("info").getString("merchantVPA");
//              String merTxnCurrency = response.getJSONObject("info").getString("merTxnCurrency");
//              String merTerminalId = response.getJSONObject("info").getString("merTerminalId");
//              String merDeviceInternet = response.getJSONObject("info").getString("merDeviceInternet");
//              String merTransPassword = response.getJSONObject("info").getString("merTransPassword");
//              String merchantId = response.getJSONObject("info").getString("merchantId");
//              String merPayeeCode = response.getJSONObject("info").getString("merPayeeCode");
//              String merPaymentType = response.getJSONObject("info").getString("merPaymentType");
//              String merSubMerchantId = response.getJSONObject("info").getString("merSubMerchantId");
//              String merKek = response.getJSONObject("info").getString("merKek");
//              String merDek = response.getJSONObject("info").getString("merDek");
//              loadDlg.show();
//              MerchantDetails merchantDetails = new MerchantDetails(getActivity());
//              MerchantDetails.Mer_MerchantId = merchantId;
//              MerchantDetails.Mer_TerminalId = merTerminalId;
//              MerchantDetails.Mer_SubMerchantID = merSubMerchantId;
//              MerchantDetails.Mer_Txn_Ref_Id = transactionId;
//              MerchantDetails.Mer_Txn_Currency = merTxnCurrency;
//              MerchantDetails.PayerCode = merPayeeCode;
//              MerchantDetails.merchant_vpa = merchantVPA;
//              MerchantDetails.Mer_TransPassword = merTransPassword;
//              MerchantDetails.Mer_Device_Internet_IP = merDeviceInternet;
//              Mer_Credentials = merchantDetails.callEncrypWork(merDek, merKek, transactionId, merTransPassword);
//
//              merchantDetails.call_Token_from_MSDK(Mer_Credentials, getActivity());
//              loadDlg.dismiss();
//
//            } else if (code != null && code.equals("F03")) {
//              loadDlg.dismiss();
//              showInvalidSessionDialog(message);
//            } else {
//              loadDlg.dismiss();
//              CustomToast.showMessage(getActivity(), message);
//            }
//
//          } catch (JSONException e) {
//            loadDlg.dismiss();
//            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//            e.printStackTrace();
//          } catch (Exception e) {
//            e.printStackTrace();
//          }
//        }
//      }, new Response.ErrorListener() {
//        @Override
//        public void onErrorResponse(VolleyError error) {
//          loadDlg.dismiss();
//          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
//          error.printStackTrace();
//
//        }
//      }) {
//        @Override
//        public Map<String, String> getHeaders() throws AuthFailureError {
//          HashMap<String, String> map = new HashMap<>();
//          map.put("hash", "1234");
//          return map;
//        }
//      };
//      int socketTimeout = 60000;
//      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//      postReq.setRetryPolicy(policy);
//      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//
//    }
//
//  }

}
