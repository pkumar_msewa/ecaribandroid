package in.msewa.fragment.fragmentqwikpayment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.orm.query.Select;

import java.util.List;

import in.msewa.adapter.FavQwikPaymentAdapter;
import in.msewa.model.QwikPayModel;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 3/27/2016.
 */
public class FavouriteFragment extends Fragment {

    private RecyclerView rvQuickPay;
    private List<QwikPayModel> payList;
    private ProgressBar pbQuickPayList;
    private LinearLayout llNoQuickPayList;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        payList = Select.from(QwikPayModel.class).list();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("toogle"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Views
        View rootView = inflater.inflate(R.layout.fragment_qwik_pay, container, false);
        rvQuickPay = (RecyclerView) rootView.findViewById(R.id.rvQuickPay);
        llNoQuickPayList = (LinearLayout) rootView.findViewById(R.id.llNoQuickPayList);
        pbQuickPayList = (ProgressBar) rootView.findViewById(R.id.pbQuickPayList);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
        rvQuickPay.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
        rvQuickPay.setLayoutManager(manager);

        if (payList.size() != 0) {
            FavQwikPaymentAdapter rcAdapter = new FavQwikPaymentAdapter(getActivity(), payList);
            rvQuickPay.setAdapter(rcAdapter);
            pbQuickPayList.setVisibility(View.GONE);
            rvQuickPay.setVisibility(View.VISIBLE);
            llNoQuickPayList.setVisibility(View.GONE);
        } else {
            llNoQuickPayList.setVisibility(View.VISIBLE);
            pbQuickPayList.setVisibility(View.GONE);
            rvQuickPay.setVisibility(View.GONE);
        }
        return rootView;
    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            payList.clear();
            payList = Select.from(QwikPayModel.class).list();
            if (payList.size() != 0) {
                FavQwikPaymentAdapter rcAdapter = new FavQwikPaymentAdapter(getActivity(), payList);
                rvQuickPay.setAdapter(rcAdapter);
                pbQuickPayList.setVisibility(View.GONE);
                rvQuickPay.setVisibility(View.VISIBLE);
                llNoQuickPayList.setVisibility(View.GONE);
            } else {
                llNoQuickPayList.setVisibility(View.VISIBLE);
                pbQuickPayList.setVisibility(View.GONE);
                rvQuickPay.setVisibility(View.GONE);
            }

        }
    };
}
