//package in.payqwik.fragment.fragmenttelebuy;
//
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.app.AlertDialog;
//import android.text.Html;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.DecimalFormat;
//import java.util.HashMap;
//import java.util.Map;
//
//import in.msewa.custom.CustomAlertDialog;
//import in.msewa.custom.CustomToast;
//import in.msewa.custom.LoadingDialog;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.metadata.AppMetadata;
//import in.msewa.model.PQCart;
//import in.msewa.model.PQTelebuyPaymentHolder;
//import in.msewa.model.UserModel;
//import in.msewa.test.R;
//import in.msewa.test.activity.MainMenuDetailActivity;
//import in.msewa.test.activity.PQChoosePaymentModeActivity;
//
///**
// * Created by Ksf on 4/7/2016.
// */
//public class TeleBuyPaymentFragmentOld extends Fragment  {
//
//    private UserModel session = UserModel.getInstance();
//    private PQCart cart = PQCart.getInstance();
//    PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();
//
//    private View rootView;
//    //	private SecurePayment mAsy = null;
//    private Button btnTelebuyTermsAndConditions;
//    private Button btnproceed;
//    private TextView tvtotalAmount, tvbalanceInPayQwik;
//    private Boolean cancel = false;
//    private View focusView = null;
//    private TextView tvDescriptions;
//    private TextView tvDetails;
//    private TextView tvTotalNetBalance;
//
//    private RadioGroup rgPaymentMethod;
//    private RadioButton rbtnPaymentMethod;
//
//    private String url;
//    private double netBalance;
//    private RequestQueue rq;
//    private LoadingDialog laodingDailog;
//    AlertDialog.Builder payDialog;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        laodingDailog = new LoadingDialog(getActivity());
//        payDialog = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
//        rq = Volley.newRequestQueue(getActivity());
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_telebuy_payment, container, false);
//        btnTelebuyTermsAndConditions = (Button) rootView.findViewById(R.id.btnTelebuyTermsAndConditions);
//        btnproceed = (Button) rootView.findViewById(R.id.btn_proceed_secure_payment);
//        tvtotalAmount = (TextView) rootView.findViewById(R.id.tvAmountSecurePayment);
//        tvbalanceInPayQwik = (TextView) rootView.findViewById(R.id.tvPayQwickBalanceSecurePayment);
//        tvTotalNetBalance = (TextView) rootView.findViewById(R.id.tvTotalNetBalance);
//
//        rgPaymentMethod = (RadioGroup) rootView.findViewById(R.id.rgPaymentMethod);
//        rbtnPaymentMethod = (RadioButton) rootView.findViewById(R.id.rbtnPayqwik);
//
//        btnproceed.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                doPayment();
//            }
//        });
//
//        double payQwikBalance = Double.parseDouble(session.getUserBalance().replaceAll("[^\\d.]", ""));
//        double cartTotal = Double.parseDouble(cart.getTotalCost().replaceAll("[^\\d.]", ""));
//        netBalance = payQwikBalance - cartTotal;
//
//        if (session.isValid==1) {
//            tvbalanceInPayQwik.setText("Rs. " + session.getUserBalance());
//        }
//
//        tvtotalAmount.setText("Rs. " + cart.getTotalCost());
//        payTelebuy.setAmount(cart.getTotalCostNumber());
//        if (netBalance < 0) {
//            tvTotalNetBalance.setTextColor(getActivity().getResources().getColor(R.color.mark_red));
//        } else {
//            tvTotalNetBalance.setTextColor(getActivity().getResources().getColor(android.R.color.white));
//        }
//        tvTotalNetBalance.setText("Rs. " + new DecimalFormat("##.##").format(netBalance));
//
//        btnTelebuyTermsAndConditions.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.tbuy.in/warranty-terms-and-conditions"));
//                startActivity(browserIntent);
//            }
//        });
//        return rootView;
//    }
//
//    protected void doPayment() {
//        int selectedId = rgPaymentMethod.getCheckedRadioButtonId();
//        rbtnPaymentMethod = (RadioButton) rootView.findViewById(selectedId);
//
//        cancel = false;
//
//        if (rbtnPaymentMethod.getText().equals("V-PayQwik")) {
//
//            if (netBalance < 0) {
//                url = ApiUrl.URL_TELEBUY_MAIN+"?API=CONVERTCARTTOORDER&PHONE="+session.getUserMobileNo()+"&DELVID="+payTelebuy.getContactAddressId()+"&PAYMODEID=59001"+"&PAYMODEDESC="+"PAYQWIK";
//                showLowBalanceDialog();
//                return;
//            }
//            else{
//                //Pay to merchant
//                url = ApiUrl.URL_TELEBUY_MAIN+"?API=CONVERTCARTTOORDER&PHONE="+session.getUserMobileNo()+"&DELVID="+payTelebuy.getContactAddressId()+"&PAYMODEID=59001"+"&PAYMODEDESC="+"PAYQWIK";
//                showPayQwikDirectPayDialog();
//            }
//
//        }
//        else{
//            url = ApiUrl.URL_TELEBUY_MAIN+"?API=CONVERTCARTTOORDER&PHONE="+session.getUserMobileNo()+"&DELVID="+payTelebuy.getContactAddressId()+"&PAYMODEID=2"+"&PAYMODEDESC="+"Credit%20card%20PrePaid";
//            payOtherMode();
//        }
//    }
//
//    public void showLowBalanceDialog() {
//        CustomAlertDialog payDialog = new CustomAlertDialog(getActivity(), R.string.dialog_low_balance, Html.fromHtml(generateLowBalanceMessage()));
//        payDialog.setPositiveButton("Load", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                if (session.emailIsActive.equals("Active")) {
//                    Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
//                    menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
//                    startActivity(menuIntent);
//                    getActivity().finish();
//                } else {
//                    showCustomDialog();
//                }
//
//
//            }
//        });
//        payDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//
//        payDialog.show();
//
//    }
//
//    public void showPayQwikDirectPayDialog() {
//        payDialog.setTitle("Use V-PayQwik Balance to Pay ?");
//        payDialog.setMessage(Html.fromHtml(generateMessageBeforePay()));
//
//        payDialog.setPositiveButton("Pay", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                laodingDailog.show();
//                CustomToast.showMessage(getActivity(), "Error occurred while connecting to server");
////                payPayqwik();
//            }
//        });
//        payDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//
//        payDialog.show();
//
//    }
//
//    public void showAfterPaymentDialog() {
//        payDialog.setTitle("Payment Done");
//        payDialog.setMessage(Html.fromHtml(generateMessageAfterPay()));
//
//        payDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                    getActivity().finish();
//            }
//        });
//
//
//        payDialog.show();
//
//    }
//
//
//
//    public void payOtherMode(){
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i("Card Payment Mode",response.toString());
//                        try {
//                            String responseCode = response.getString("response_code");
//                            if (responseCode.equals("0")) {
//                                String orderRef = response.getString("orderref");
//                                payTelebuy.setOrderRef(orderRef);
////                                payTelebuy.setAmount(cart.getTotalCostNumber());
//                                startActivity(new Intent(getActivity(), PQChoosePaymentModeActivity.class));
//                                getActivity().finish();
//                                cart.setCartUpdated(true);
//                            } else {
//                                CustomToast.showMessage(getActivity(),"Unable to process try again");
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//
//
//    public void payPayqwik(){
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i("PayQWik Pay",response.toString());
//                        try {
//                            String responseCode = response.getString("response_code");
//                            if (responseCode.equals("0")) {
//                                String orderRef = response.getString("orderref");
//                                payTelebuy.setOrderRef(orderRef);
//                                merchantPayFinal();
//                                cart.setCartUpdated(true);
//                            } else {
//                                CustomToast.showMessage(getActivity(),"Unable to process try again");
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//
//
//    }
//
//    public void loadPayPayqwik(){
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i("PAYQWIK LOAD AND PAY",response.toString());
//                        try {
//                            String responseCode = response.getString("response_code");
//                            if (responseCode.equals("0")) {
//                                String orderRef = response.getString("orderref");
//                                payTelebuy.setOrderRef(orderRef);
//                                Toast.makeText(getActivity(),"Load Money Coming soon",Toast.LENGTH_SHORT).show();
//
//                                cart.setCartUpdated(true);
//                            } else {
//                                CustomToast.showMessage(getActivity(),"Unable to process try again");
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//    public String generateMessageBeforePay(){
//
//        String source = "<b>TOTAL AMOUNT TO BE PAID</b><br>" + payTelebuy.getAmount() + "<br>"
//                + "<b>ORDER ID</b><br>" + payTelebuy.getOrderId() + "<br>"
//                + "<b>FULL NAME</b><br>" + payTelebuy.getName() + "<br>"
//                + "<b>ADDRESS 1</b><br>" + payTelebuy.getAddress() + "<br>"
//                + "<b>CITY/TOWN</b><br>" + payTelebuy.getCity() + "<br>"
//                + "<b>STATE</b><br>" + payTelebuy.getState() + "<br>"
//                + "<b>PIN CODE</b><br>" + payTelebuy.getZip() + "<br>"
//                + "<b>MOBILE NO.</b><br>" + payTelebuy.getTelephone();
//
//        return source;
//    }
//
//    public String generateLowBalanceMessage(){
//        String source = "<b><font color=#000000> Your balance is not sufficient.</font></b>"+
//                "<br><b><font color=#ff0000> Please load money to make payment.</font></b><br>";
//        return source;
//    }
//
//    public String generateMessageAfterPay(){
//
//        String source = "<b>TOTAL AMOUNT TO BE PAID</b><br>" + payTelebuy.getAmount() + "<br>"
//                + "<b>ORDER ID</b><br>" + payTelebuy.getOrderId() + "<br>"
//                + "<b>FULL NAME</b><br>" + payTelebuy.getName() + "<br>"
//                + "<b>ADDRESS 1</b><br>" + payTelebuy.getAddress() + "<br>"
//                + "<b>CITY/TOWN</b><br>" + payTelebuy.getCity() + "<br>"
//                + "<b>STATE</b><br>" + payTelebuy.getState() + "<br>"
//                + "<b>PIN CODE</b><br>" + payTelebuy.getZip() + "<br>"
//                + "<b>MOBILE NO.</b><br>" + payTelebuy.getTelephone()+ "<br>"
//                + "<b>THANK FOR USING PAYQWIK</b>";
//        return source;
//    }
//
//
//
//
//    private void merchantPayFinal() {
//        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_PAY_AT_STORE, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    Log.i("Merchant Pay Response", response.toString());
//                    JSONObject jsonObj = new JSONObject(response);
//                    String code = jsonObj.getString("code");
//                    String message = jsonObj.getString("details");
//
//
//                    if (code != null && code.equals("S00")) {
//                        laodingDailog.dismiss();
//                        showAfterPaymentDialog();
//                    } else {
//                        laodingDailog.dismiss();
//
//                    }
//
//                } catch (JSONException e) {
//                    laodingDailog.dismiss();
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                laodingDailog.dismiss();
//
//            }
//
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("serviceProvider", AppMetadata.TELEBUY_ID);
//                params.put("orderID",payTelebuy.getOrderRef() );
//                params.put("amount", payTelebuy.getAmount());
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<String, String>();
//                map.put("sessionId", session.getUserSessionId());
//                return map;
//            }
//        };
//
//        rq.add(postReq);
//    }
//
//    public void showCustomDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateVerifyMessage()));
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//
//            }
//        });
//        builder.show();
//    }
//
//
//    public String generateVerifyMessage() {
//        String source = "<b><font color=#000000> Please check your inbox and verify email, to proceed.</font></b>" +
//                "<br><br><b><font color=#ff0000> You can check registered email address in edit profile section. </font></b><br></br>";
//        return source;
//    }
//
//}
