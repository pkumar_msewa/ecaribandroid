package in.msewa.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.msewa.adapter.TicketAdapter;
import in.msewa.custom.ResultIPC;
import in.msewa.model.ImagicaTicketModel;
import in.msewa.model.TicketDetail;
import in.msewa.ecarib.R;


/**
 * Created by acer on 06-07-2017.
 */

public class TicketFragment extends Fragment {

    private View rootView;
    RecyclerView recyclerView;
    Context context;
    static double price = 0.0;
    ArrayList<ImagicaTicketModel> imagicaTicketModels;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_packages_list, container, false);
        int sys = getArguments().getInt("ticket", 0);
        String type = getArguments().getString("type");
        recyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        ResultIPC resultIPC = ResultIPC.get();
        imagicaTicketModels = resultIPC.getLargeData(sys);
        for (ImagicaTicketModel model : imagicaTicketModels) {
            for (TicketDetail ticketDetail : model.getTicketDetail()) {
                Log.i("Types", ticketDetail.getTdtype());
            }
        }

        TicketAdapter adapter = new TicketAdapter(getActivity(), imagicaTicketModels, type, recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemViewCacheSize(imagicaTicketModels.size());

        for (int i = 0; i < adapter.getItemCount(); i++) {
            View view = recyclerView.getLayoutManager().findViewByPosition(i);
            if (view instanceof TextView) {
                TextView textView = (TextView) view.findViewById(R.id.tvtotalcost);
                price += Double.parseDouble(textView.getText().toString());

            }
        }
        Log.i("total", String.valueOf(price));


        return rootView;
    }
}

