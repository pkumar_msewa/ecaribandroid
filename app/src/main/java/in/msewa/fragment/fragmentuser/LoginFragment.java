package in.msewa.fragment.fragmentuser;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.CircleModel;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.permission.PermissionHandler;
import in.msewa.permission.Permissions;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.SecurityUtil;
import in.msewa.util.VerifyLoginOTPListner;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.OtpVerificationMidLayerActivity;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LoginFragment extends Fragment implements VerifyLoginOTPListner {
  private static final String TAG = "loginresponse";
  private View rootView;
  private EditText etLoginMobile;
  private EditText etLoginPassword;
  private TextInputLayout ilLoginMobile;
  private TextInputLayout ilLoginPassword;

  private Button btnLogin;
  private Button btnForget;
  private Button btnRegsiter;
  private ImageButton iBtnShowLoginPwd;
  private LoadingDialog loadDlg;
  private JSONObject jsonRequest;
  private Button ok;

  SharedPreferences musicPreferences;

//    private JSONObject jsonRequestED;

  private UserModel session = UserModel.getInstance();

  public static final String PHONE = "phone";
  SharedPreferences phonePreferences;
  private String phoneNo;

  //FIREBASE
  public static final String PROPERTY_REG_ID = "registration_id";
  private String regId = "";

  //Volley Tag
  private String tag_json_obj = "json_user";

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    musicPreferences = getActivity().getSharedPreferences("ShowMusicDailog", Context.MODE_PRIVATE);
    phonePreferences = getActivity().getSharedPreferences(PHONE, Context.MODE_PRIVATE);
    phoneNo = phonePreferences.getString("phone", "");

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_login, container, false);
    etLoginMobile = (EditText) rootView.findViewById(R.id.etLoginMobile);
    etLoginPassword = (EditText) rootView.findViewById(R.id.etLoginPassword);
    btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
    btnForget = (Button) rootView.findViewById(R.id.btnForget);
    btnRegsiter = (Button) rootView.findViewById(R.id.btnRegsiter);
    iBtnShowLoginPwd = (ImageButton) rootView.findViewById(R.id.iBtnShowLoginPwd);
    ilLoginPassword = (TextInputLayout) rootView.findViewById(R.id.ilLoginPassword);
    ilLoginMobile = (TextInputLayout) rootView.findViewById(R.id.ilLoginMobile);

    etLoginMobile.addTextChangedListener(new MyTextWatcher(etLoginMobile));
    etLoginPassword.addTextChangedListener(new MyTextWatcher(etLoginPassword));
    loadDlg = new LoadingDialog(getActivity());


    etLoginMobile.setText(getStoredPhone());

    //DONE CLICK ON VIRTUAL KEYPAD
    etLoginPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          submitForm();
        }
        return false;
      }
    });

    btnLogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        submitForm();

      }
    });

    btnRegsiter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
//                promoteTest();
        Intent intent = new Intent("sign-up");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
      }
    });


    btnForget.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (!validateMobile()) {
          return;
        }

        loadDlg.show();
        promoteForgetPwd();
      }
    });


    iBtnShowLoginPwd.setOnTouchListener(new View.OnTouchListener() {

      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          etLoginPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
          etLoginPassword.setSelection(etLoginPassword.length());
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
          etLoginPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          etLoginPassword.setSelection(etLoginPassword.length());
        }

        return false;
      }
    });


    Button btnCallCustomer = (Button) rootView.findViewById(R.id.btnCallCustomer);
    btnCallCustomer.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String[] items = {"+918025011300"};
        android.support.v7.app.AlertDialog.Builder callDialog =
          new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        callDialog.setTitle("Select a no. to call");
        callDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
          }
        });
        callDialog.setItems(items, new DialogInterface.OnClickListener() {

          public void onClick(DialogInterface dialog, int position) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + items[position].toString().trim()));
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
              return;
            }
            startActivity(callIntent);
          }

        });

        callDialog.show();
      }
    });

    return rootView;
  }

  private String getStoredPhone() {
    if (phoneNo != null && !phoneNo.isEmpty()) {
      return phoneNo;
    } else {
      return "";
    }
  }


  private boolean validatePassword() {
    if (etLoginPassword.getText().toString().trim().isEmpty()) {
      ilLoginPassword.setError("Required password");
      requestFocus(etLoginPassword);
      return false;
    } else if (etLoginPassword.getText().toString().trim().length() < 6) {
      ilLoginPassword.setError(getResources().getString(R.string.password_validity));
      requestFocus(etLoginPassword);
      return false;
    } else {
      ilLoginPassword.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateMobile() {
    if (etLoginMobile.getText().toString().trim().isEmpty() || etLoginMobile.getText().toString().trim().length() < 10) {
      ilLoginMobile.setError("Enter 10 digit no");
      requestFocus(etLoginMobile);
      return false;
    } else {
      ilLoginMobile.setErrorEnabled(false);
    }

    return true;
  }


  private void submitForm() {
    if (!validateMobile()) {
      return;
    }

    if (!validatePassword()) {
      return;
    }
    if (Build.VERSION.SDK_INT >= 23) {
      Permissions.check(getActivity(), Manifest.permission.READ_PHONE_STATE, 0,
        new PermissionHandler() {
          @Override
          public void onGranted() {
            //do your task
            loadDlg.show();
            attemptLoginNew();
          }
        });
//
    } else {
      loadDlg.show();
      attemptLoginNew();
    }


  }

  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }


  private class MyTextWatcher implements TextWatcher {

    private View view;

    private MyTextWatcher(View view) {
      this.view = view;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {
      int i = view.getId();
      if (i == R.id.etLoginMobile) {
        validateMobile();
      } else if (i == R.id.etLoginPassword) {
        if (editable.length() < getActivity().getResources().getInteger(R.integer.passwordLength)) {
          validatePassword();
        } else {
          submitForm();
        }
      }
    }
  }

  public void attemptLoginNew() {
    jsonRequest = new JSONObject();
    String encryptValue = "";
    try {
      jsonRequest.put("username", etLoginMobile.getText().toString());
      jsonRequest.put("password", etLoginPassword.getText().toString());
      regId = getRegistrationId();
      if (!regId.isEmpty()) {
        jsonRequest.put("registrationId", regId);
      }
      if (SecurityUtil.getIMEI(getActivity()) != null) {
        jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getActivity()) + "-" + SecurityUtil.getIMEI(getActivity()));
      } else {
        jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getActivity()));
      }

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("values", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_LOGIN)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject response) {

            try {
              String code = response.getString("code");

              if (code != null && code.equals("S00")) {
                String jsonString = response.getString("response");
                JSONObject jsonObject = new JSONObject(jsonString);
//                logMultilineString(response.toString());
                //New Implementation
                JSONObject jsonDetail = jsonObject.getJSONObject("details");
                String userSessionId = jsonDetail.getString("sessionId");
                String nikiOffer = "nooffer";
                if (jsonDetail.has("nikiOffer")) {
                  nikiOffer = jsonDetail.getString("nikiOffer");
                }
                JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");

                double userBalanceD = accDetail.getDouble("balance");
                String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

                long userAcNoL = accDetail.getLong("accountNumber");
                String userAcNo = String.valueOf(userAcNoL);

                int userPointsI = accDetail.getInt("points");
                String userPoints = String.valueOf(userPointsI);

                JSONObject accType = accDetail.getJSONObject("accountType");
                String accName = accType.getString("name");
                String accCode = accType.getString("code");

                double accBalanceLimitD = accType.getDouble("balanceLimit");
                double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                double accDailyLimitD = accType.getDouble("dailyLimit");

                String accBalanceLimit = String.valueOf(accBalanceLimitD);
                String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                String accDailyLimit = String.valueOf(accDailyLimitD);

                JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");

                String userFName = jsonUserDetail.getString("firstName");
                String userLName = jsonUserDetail.getString("lastName");
                String userMobile = jsonUserDetail.getString("contactNo");
                String userEmail = jsonUserDetail.getString("email");
                String userEmailStatus = jsonUserDetail.getString("emailStatus");

                String userAddress = jsonUserDetail.getString("address");
                String userImage = jsonUserDetail.getString("image");
                String encodedImage = jsonUserDetail.getString("encodedImage");
                String images = "";
                if (!encodedImage.equals("")) {
                  images = encodedImage;
                } else {
                  images = userImage;
                }


                boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");
                String userDob = jsonUserDetail.getString("dateOfBirth");

                boolean hasRefer;
                if (response.has("hasRefer")) {
                  hasRefer = response.getBoolean("hasRefer");
                } else {
                  hasRefer = true;
                }
                String userGender = "";
                if (!jsonUserDetail.isNull("gender")) {
                  userGender = jsonUserDetail.getString("gender");
                }
                boolean iplEnable = false, ebsEnable = false, vnetEnable = false, razorPayEnable = false;
                String mdexToken = "", mdexKey = "", iplPrediction = "", iplSchedule = "", iplMyPrediction = "";
                if (response.has("iplEnable")) {
                  iplEnable = response.getBoolean("iplEnable");
                  mdexToken = response.getString("mdexToken");
                  mdexKey = response.getString("mdexKey");
                  iplPrediction = response.getString("iplPrediction");
                  iplSchedule = response.getString("iplSchedule");
                  iplMyPrediction = response.getString("iplMyPrediction");

                }
                if (response.has("ebsEnable") || response.has("vnetEnable") || response.has("razorPayEnable")) {
                  ebsEnable = response.getBoolean("ebsEnable");
                  vnetEnable = response.getBoolean("vnetEnable");
                  razorPayEnable = response.getBoolean("razorPayEnable");
                }


                try {
                  //Encrypting sessionId
                  UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, images, userDob, userGender, isMPin, userPoints, hasRefer, nikiOffer, iplEnable, iplMyPrediction, iplPrediction, iplSchedule, mdexKey, mdexToken, ebsEnable, vnetEnable, razorPayEnable);
                  if (Select.from(UserModel.class).list().size() > 0) {
                    userModel.update();
                  } else {
                    userModel.save();
                  }
                } catch (Exception e) {
                  e.printStackTrace();
                }

                session.setUserSessionId(userSessionId);
                session.setUserFirstName(userFName);
                session.setUserLastName(userLName);
                session.setUserMobileNo(userMobile);
                session.setUserEmail(userEmail);
                session.setEmailIsActive(userEmailStatus);
                session.setUserAcCode(accCode);
                session.setUserAcNo(userAcNo);
                session.setUserBalance(userBalance);
                session.setUserAcName(accName);
                session.setUserMonthlyLimit(accMonthlyLimit);
                session.setUserDailyLimit(accDailyLimit);
                session.setUserBalanceLimit(accBalanceLimit);
                session.setUserImage(images);
                session.setUserAddress(userAddress);
                session.setUserGender(userGender);
                session.setUserDob(userDob);
                session.setMPin(isMPin);
                session.setHasRefer(hasRefer);
                session.setIsValid(1);
                session.setRazorPayEnable(razorPayEnable);
                session.setEbsEnable(ebsEnable);
                session.setVnetEnable(vnetEnable);
                session.setNikiOffer(nikiOffer);
                session.setUserPoints(userPoints);
                session.setIplEnable(iplEnable);
                session.setIplMyPrediction(iplMyPrediction);
                session.setIplPrediction(iplPrediction);
                session.setIplSchedule(iplSchedule);
                session.setMdexKey(mdexKey);
                session.setMdexToken(mdexToken);
                try {
                  OperatorsModel.deleteAll(OperatorsModel.class);
                  CircleModel.deleteAll(CircleModel.class);
                } catch (SQLiteException e) {

                }


                SharedPreferences.Editor editor = phonePreferences.edit();
                editor.clear();
                editor.putString("phone", userMobile);
                editor.apply();

                SharedPreferences.Editor musiceditor = musicPreferences.edit();
                musiceditor.clear();
                musiceditor.putBoolean("ShowMusicDailog", true);
                musiceditor.apply();

                loadDlg.dismiss();

//                            startActivity((new Intent(getActivity(),ContactReadActivity.class)));
//                            getActivity().finish();
//                            Intent i = new Intent(getActivity(), ContactReadActivity.class);
//                            startActivity(i);
                startActivity(new Intent(getActivity(), HomeMainActivity.class));
                getActivity().finish();
              } else if (code != null && code.equals("L01")) {
                loadDlg.dismiss();

                Intent otpIntent = new Intent(getActivity(), OtpVerificationMidLayerActivity.class);
                otpIntent.putExtra("userMobileNo", etLoginMobile.getText().toString());
                otpIntent.putExtra("intentType", "Device");
                otpIntent.putExtra("devPassword", etLoginPassword.getText().toString());
                otpIntent.putExtra("devRegId", regId);
                startActivityForResult(otpIntent, 0);
                getActivity().finish();
//                            new VerifyLoginOTPDialog(getActivity(), LoginFragment.this, etLoginMobile.getText().toString(), etLoginPassword.getText().toString(), regId);
              } else {
                String message = response.getString("message");
                loadDlg.dismiss();
                CustomToast.showMessage(getActivity(), message);

              }
            } catch (
              JSONException e)

            {
              loadDlg.dismiss();
              CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError anError) {
            loadDlg.dismiss();
            Log.i("valus", anError.getErrorDetail());
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          }
        });

    }

  }

  private void promoteForgetPwd() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("username", etLoginMobile.getText().toString());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FORGET_PWD, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            String message = response.getString("message");
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
//                            Intent autoIntent = new Intent(getActivity(), ChangePwdActivity.class);
//                            autoIntent.putExtra("OtpCode","");
//                            autoIntent.putExtra("userMobileNo",etLoginMobile.getText().toString());


              Intent verifyIntent = new Intent(getActivity(), OtpVerificationMidLayerActivity.class);
              verifyIntent.putExtra("userMobileNo", etLoginMobile.getText().toString());
              verifyIntent.putExtra("intentType", "Forget");

              loadDlg.dismiss();
//                            startActivity(autoIntent);
              startActivity(verifyIntent);

            } else {
              loadDlg.dismiss();
              Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private String getRegistrationId() {
    final SharedPreferences prefs = getGCMPreferences();
    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
    if (registrationId.isEmpty()) {
      return "";
    }
    return registrationId;
  }

  private SharedPreferences getGCMPreferences() {
    return getActivity().getSharedPreferences("FIREBASE",
      Context.MODE_PRIVATE);
  }


  private void promoteTest() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("auth_facebook", "true");
      jsonRequest.put("access_token", "qwekqjsaldkaSDLAUREQWDAMSD");
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, "http://swasthyanews.com/api/oauth", jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          Log.i("Test Response", response.toString());

        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

        }
      }) {

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  @Override
  public void onVerifySuccess() {
    SharedPreferences.Editor editor = phonePreferences.edit();
    editor.clear();
    editor.putString("phone", etLoginMobile.getText().toString());
    editor.commit();
    startActivity(new Intent(getActivity(), HomeMainActivity.class));
    getActivity().finish();
  }

  @Override
  public void onVerifyError() {
//        CustomToast.showMessage(getActivity(),"Error occurred please try again");
  }

  void logMultilineString(String data) {
    for (String line : data.split("\n")) {
      logLargeString(line);
    }
  }

  void logLargeString(String data) {
    final int CHUNK_SIZE = 4076;  // Typical max logcat payload.
    int offset = 0;
    while (offset + CHUNK_SIZE <= data.length()) {
      Log.d(TAG, data.substring(offset, offset += CHUNK_SIZE));
    }
    if (offset < data.length()) {
      Log.d(TAG, data.substring(offset));
    }
  }

}
