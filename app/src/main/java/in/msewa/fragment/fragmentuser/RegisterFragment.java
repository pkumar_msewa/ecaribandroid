package in.msewa.fragment.fragmentuser;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.QuestionListModel;
import in.msewa.model.UserModel;
import in.msewa.permission.PermissionHandler;
import in.msewa.permission.Permissions;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.SecurityUtil;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.ecarib.activity.OtpVerificationMidLayerActivity;

/**
 * Created by Ksf on 3/27/2016.
 */
public class RegisterFragment extends Fragment {
  private EditText etRegisterPassword, etRegisterEmail, etRegisterMobile, etRegisterFirstName, etRegisterDob, etRegisterPinCode, etVBAcc, etSecQuest, etSecAns, etLastName;
  private RadioButton rbVbYes, rbVbNo;


  //    private RadioButton rbRegisterMale,rbRegisterFfragment_registereMale;
  private String firstName, lastName, newPassword, rePassword, email, mobileNo;

  private LoadingDialog loadDlg;

  private TextInputLayout ilRegisterMobile, ilRegisterPassword, ilRegisterEmail, ilRegisterDob, ilRegisterFName, ilRegisterPinCode, ilVBAcc, ilSecQuest, ilSecAns,ilLastName;

  private String gender;
  private View rootView;
  private JSONObject jsonRequest;
  private Button btnTermCondition;
  private String quesCode;
  private Boolean isQuesSet = false;

  //Strong password or weak
  private final Pattern hasUppercase = Pattern.compile("[A-Z]");
  private final Pattern hasLowercase = Pattern.compile("[a-z]");
  private final Pattern hasNumber = Pattern.compile("\\d");
  private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

  private List<QuestionListModel> QuestionListModelList;

  //Volley Tag
  private String tag_json_obj = "json_user";


  private static final String KEY_NAME = "santosh";
  private Cipher cipher;
  private KeyStore keyStore;
  private KeyGenerator keyGenerator;
  private TextView textView;
  private FingerprintManager.CryptoObject cryptoObject;
  private FingerprintManager fingerprintManager;
  private KeyguardManager keyguardManager;
  private byte[] fingerprintStatus;
  private boolean status;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
//    try {
//      QuestionListModelList = Select.from(QuestionListModel.class).list();
//    } catch (SQLiteException e) {
//
//    }

  }

  @SuppressLint("ClickableViewAccessibility")
  @RequiresApi(api = Build.VERSION_CODES.M)
  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_register, container, false);

    etRegisterFirstName = (EditText) rootView.findViewById(R.id.etRegisterFirstName);
    etLastName = (EditText) rootView.findViewById(R.id.etLastName);
    etRegisterPassword = (EditText) rootView.findViewById(R.id.etRegisterPassword);
    etRegisterEmail = (EditText) rootView.findViewById(R.id.etRegisterEmail);
    etRegisterMobile = (EditText) rootView.findViewById(R.id.etRegisterMobile);
    etRegisterDob = (EditText) rootView.findViewById(R.id.etRegisterDob);
    etRegisterPinCode = (EditText) rootView.findViewById(R.id.etRegisterPinCode);
    etVBAcc = (EditText) rootView.findViewById(R.id.etVBAcc);
    etSecAns = (EditText) rootView.findViewById(R.id.etSecAns);
    etSecQuest = (EditText) rootView.findViewById(R.id.etSecQuest);
    ilVBAcc = (TextInputLayout) rootView.findViewById(R.id.ilVBAcc);
    ilLastName = (TextInputLayout) rootView.findViewById(R.id.ilLastName);
    ImageButton ibtnShowRePwd = (ImageButton) rootView.findViewById(R.id.ibtnShowRePwd);

//    etSecQuest.setFocusable(false);
//    setAnsFocus(isQuesSet);

    rbVbNo = (RadioButton) rootView.findViewById(R.id.rbVbNo);
    rbVbYes = (RadioButton) rootView.findViewById(R.id.rbVbYes);
    RadioGroup rgVbRegister = (RadioGroup) rootView.findViewById(R.id.rgVbRegister);

    if (rbVbYes.isChecked()) {
      gender="M";
    } else {
      gender="F";
    }

    ImageButton ibtnShowPwd = (ImageButton) rootView.findViewById(R.id.ibtnShowPwd);

    btnTermCondition = (Button) rootView.findViewById(R.id.btnTermCondition);

    if (getEmail(getActivity()) != null && !getEmail(getActivity()).isEmpty()) {
      etRegisterEmail.setText(getEmail(getActivity()));
    }

    Button btnRegisterSubmit = (Button) rootView.findViewById(R.id.btnRegisterSubmit);

    ilRegisterFName = (TextInputLayout) rootView.findViewById(R.id.ilRegisterFName);
    ilRegisterMobile = (TextInputLayout) rootView.findViewById(R.id.ilRegisterMobile);
    ilRegisterPassword = (TextInputLayout) rootView.findViewById(R.id.ilRegisterPassword);
    ilRegisterEmail = (TextInputLayout) rootView.findViewById(R.id.ilRegisterEmail);
    ilRegisterDob = (TextInputLayout) rootView.findViewById(R.id.ilRegisterDob);
    ilRegisterPinCode = (TextInputLayout) rootView.findViewById(R.id.ilRegisterPinCode);
    ilSecAns = (TextInputLayout) rootView.findViewById(R.id.ilSecAns);
    ilSecQuest = (TextInputLayout) rootView.findViewById(R.id.ilSecQuest);
    etRegisterMobile.addTextChangedListener(new MyTextWatcher(etRegisterMobile));
    etRegisterEmail.addTextChangedListener(new MyTextWatcher(etRegisterEmail));
    etRegisterPassword.addTextChangedListener(new MyTextWatcher(etRegisterPassword));


    rgVbRegister.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId) {
          case R.id.rbVbYes:
            gender="M";
            break;
          case R.id.rbVbNo:
            gender="F";
            break;
        }
      }
    });

    ibtnShowPwd.setOnTouchListener(new View.OnTouchListener() {

      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          etRegisterPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
          etRegisterPassword.setSelection(etRegisterPassword.length());
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
          etRegisterPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          etRegisterPassword.setSelection(etRegisterPassword.length());
        }

        return false;
      }
    });

    btnTermCondition.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "TermsAndCondition");
        getActivity().startActivity(menuIntent);
      }
    });


    btnRegisterSubmit.setTextColor(Color.parseColor("#ffffff"));

    etRegisterDob.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String formattedDay, formattedMonth;
            if (dayOfMonth < 10) {
              formattedDay = "0" + dayOfMonth;
            } else {
              formattedDay = dayOfMonth + "";
            }

            if ((monthOfYear + 1) < 10) {
              formattedMonth = "0" + String.valueOf(monthOfYear + 1);
            } else {
              formattedMonth = String.valueOf(monthOfYear + 1) + "";
            }
            etRegisterDob.setText(String.valueOf(year) + "-"
              + String.valueOf(formattedMonth) + "-"
              + String.valueOf(formattedDay));
          }
        }, 1990, 01, 01);

        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

        {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
        dialog.show();
      }
    });

    btnRegisterSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        submitForm();
      }
    });

    //DONE CLICK ON VIRTUAL KEYPAD
    etRegisterEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          submitForm();
        }
        return false;
      }
    });

//    GetQuestion getQuesTask = new GetQuestion();
//    getQuesTask.execute();
//    etSecQuest.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        if (QuestionListModelList == null || QuestionListModelList.size() == 0) {
//          CustomToast.showMessage(getActivity(), "Sorry error occured please try again");
//        } else {
//          showQuesDialog();
//        }
//
//      }
//    });

    etSecAns.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (!etSecQuest.getText().toString().isEmpty()) {
          setAnsFocus(true);
        } else {
          setAnsFocus(false);
          CustomToast.showMessage(getActivity(), "Select Question first");
        }
      }
    });
//    getQuesList();
    return rootView;
  }

  public void setAnsFocus(Boolean isQuesSet) {
    if (isQuesSet) {
      etSecQuest.setNextFocusDownId(R.id.etSecAns);
      etSecAns.setFocusable(true);
      etSecAns.setFocusableInTouchMode(true);
      etSecAns.requestFocus();
    } else
      etSecAns.setFocusable(false);
  }

  private void showQuesDialog() {
    final ArrayList<String> quesVal = new ArrayList<>();
    for (QuestionListModel questionListModel : QuestionListModelList) {
      quesVal.add(questionListModel.getQuestion());
    }

    android.support.v7.app.AlertDialog.Builder bankDialog =
      new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
    bankDialog.setTitle("Select Your Security Question");

    bankDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });

    bankDialog.setItems(quesVal.toArray(new String[quesVal.size()]),
      new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int i) {
          etSecQuest.setText(quesVal.get(i));
          etSecAns.getText().clear();
          quesCode = QuestionListModelList.get(i).getQuesCode();
        }
      });
    bankDialog.show();

  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  private void submitForm() {

    if (!validateMobile()) {
      return;
    }
    if (!validatePassword()) {
      return;
    }
    if (!validateFName()) {
      return;
    }
    if (!validateLName()) {
      return;
    }
    if (!validateDob()) {
      return;
    }
//    if (!validatePin()) {
//      return;
//    }
    if (!validateEmail()) {
      return;
    }

    firstName = etRegisterFirstName.getText().toString();
    newPassword = etRegisterPassword.getText().toString();
    email = etRegisterEmail.getText().toString();
    mobileNo = etRegisterMobile.getText().toString();
    if (Build.VERSION.SDK_INT >= 23) {
      Permissions.check(getActivity(), Manifest.permission.READ_PHONE_STATE, 0,
        new PermissionHandler() {
          @Override
          public void onGranted() {
            //do your task
            promoteRegister();
          }
        });
//
    } else {

      promoteRegister();
    }


  }


  private boolean validateFName() {
    if (etRegisterFirstName.getText().toString().trim().isEmpty()) {
      ilRegisterFName.setError("Enter first name");
      requestFocus(etRegisterFirstName);
      return false;
    } else {
      ilRegisterFName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateLName() {
    if (etLastName.getText().toString().trim().isEmpty()) {
      ilLastName.setError("Enter Last name");
      requestFocus(etLastName);
      return false;
    } else {
      ilLastName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateEmail() {
    String email = etRegisterEmail.getText().toString().trim();
    if (email.isEmpty() || !isValidEmail(email)) {
      ilRegisterEmail.setError("Enter valid email");
      requestFocus(ilRegisterEmail);
      return false;
    } else {
      ilRegisterEmail.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateMobile() {
    if (etRegisterMobile.getText().toString().trim().isEmpty() || etRegisterMobile.getText().toString().trim().length() < 10) {
      ilRegisterMobile.setError("Enter 10 digit no");
      requestFocus(etRegisterMobile);
      return false;
    } else {
      ilRegisterMobile.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateVBAcc() {
    String acc = etVBAcc.getText().toString().trim();
    if (!acc.isEmpty()) {
      if (acc.length() != 15) {
        ilVBAcc.setError(getString(R.string.error_vijaya_invalid1));
        requestFocus(etVBAcc);
        return false;
      } else if (acc.charAt(4) != '0') {
        ilVBAcc.setError(getString(R.string.error_vijaya_invalid1));
        requestFocus(etVBAcc);
        return false;
      } else {
        return true;
      }
    } else {
      ilRegisterEmail.setErrorEnabled(false);
      return true;
    }
  }

  private boolean validatePasswordListner() {
    if (etRegisterPassword.getText().toString().trim().isEmpty() || etRegisterPassword.getText().toString().trim().length() < 6) {
      ilRegisterPassword.setError(getResources().getString(R.string.password_validity));
      requestFocus(etRegisterPassword);
      return false;
    } else {
      ilRegisterPassword.setErrorEnabled(false);
      checkPwdStr();
    }

    return true;
  }


  private String checkPwdStr() {
    if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Strong Password");
    } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Strong Password");
    } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Strong Password");
    } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Strong Password");
    } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Good Password");
    } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Good Password");
    } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Good Password");
    } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Good Password");
    } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
      ilRegisterPassword.setError("Good Password");
    } else {
      ilRegisterPassword.setError("Weak Password");
    }


    return null;
  }


  private boolean validatePassword() {
    if (etRegisterPassword.getText().toString().isEmpty()) {
      ilRegisterPassword.setError("Check the password");
      requestFocus(etRegisterPassword);
      return false;
    } else if (etRegisterPassword.getText().toString().length() < 6) {
      ilRegisterPassword.setError(getResources().getString(R.string.password_validity));
      requestFocus(etRegisterPassword);
      return false;
    } else {
      ilRegisterPassword.setErrorEnabled(false);
    }
    return true;
  }

  private boolean validateDob() {
    if (etRegisterDob.getText().toString().trim().isEmpty()) {
      ilRegisterDob.setError("Enter Date of birth");
      requestFocus(etRegisterDob);
      return false;
    } else {
      ilRegisterDob.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validatePin() {
    if (etRegisterPinCode.getText().toString().trim().isEmpty()) {
      ilRegisterPinCode.setError("Enter Postal Code");
      requestFocus(etRegisterPinCode);
      return false;
    } else {
      ilRegisterPinCode.setErrorEnabled(false);
    }

    return true;
  }

  private static boolean isValidEmail(String email) {
    return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }

  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }


  private class MyTextWatcher implements TextWatcher {

    private View view;

    private MyTextWatcher(View view) {
      this.view = view;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {
      int i = view.getId();
      if (i == R.id.etRegisterEmail) {
        validateEmail();
      } else if (i == R.id.etRegisterMobile) {
        validateMobile();
      } else if (i == R.id.etRegisterPassword) {
        validatePasswordListner();
      }

    }
  }

  private void promoteRegister() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("firstName", etRegisterFirstName.getText().toString());
      jsonRequest.put("lastName", etLastName.getText().toString());
      jsonRequest.put("password", etRegisterPassword.getText().toString());
      jsonRequest.put("email", etRegisterEmail.getText().toString());
      jsonRequest.put("contactNo", etRegisterMobile.getText().toString());
      jsonRequest.put("dateOfBirth", etRegisterDob.getText().toString());
      jsonRequest.put("gender", gender);
//      if (etSecAns.getText() != null && !etSecAns.getText().toString().isEmpty()) {
//        jsonRequest.put("secQuestionCode", quesCode);
//        jsonRequest.put("secAnswer", etSecAns.getText().toString());
//      }
//      if (rbVbYes.isChecked()) {
//        jsonRequest.put("accountNumber", etVBAcc.getText().toString());
//        jsonRequest.put("vbankCustomer", "true");
//      } else {
//        jsonRequest.put("accountNumber", "");
//        jsonRequest.put("vbankCustomer", "false");
//        Log.i("jsonRequest", jsonRequest.toString());
//      }
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {

      AndroidNetworking.post(ApiUrl.URL_REGISTRATION)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) throws JSONException {
          loadDlg.dismiss();
          try {
            Log.i("Registration Response", response.toString());
            String messageAPI = response.getString("message");
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {

              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);
              loadDlg.dismiss();
              Intent otpIntent = new Intent(getActivity(), OtpVerificationMidLayerActivity.class);
              otpIntent.putExtra("userMobileNo", etRegisterMobile.getText().toString());
              otpIntent.putExtra("intentType", "Register");
              startActivityForResult(otpIntent, 0);
              getActivity().finish();
            } else {
              if (response.has("details")) {
                if (!response.isNull("details") && response.getString("details") != null) {
                  String messageDetail = response.getString("message");
                  CustomToast.showMessage(getActivity(), messageDetail);
                } else {
                  CustomToast.showMessage(getActivity(), messageAPI);
                }
              }
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
          }

        }

        @Override
        public void onError(ANError anError) {
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          loadDlg.dismiss();
        }
      });

    }
  }

  private class GetQuestion extends AsyncTask<Void, Void, Void> {
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      loadDlg.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
      getQuesList();
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      etSecQuest.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (QuestionListModelList == null || QuestionListModelList.size() == 0) {
            CustomToast.showMessage(getActivity(), "Error fetching questions list, please try again later");
          } else {
            showQuesDialog();
          }
        }
      });

      etSecAns.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (!etSecQuest.getText().toString().isEmpty()) {
            setAnsFocus(true);
          } else {
            setAnsFocus(false);
            CustomToast.showMessage(getActivity(), "Select Security Question first");
          }
        }
      });
      loadDlg.dismiss();
    }
  }

  public void getQuesList() {
    AndroidNetworking.get(ApiUrl.URL_LIST_QUES)
      .addHeaders(new HashMap<String, String>().put("hash", "1234"))
      .setPriority(Priority.HIGH)
      .build()
      .getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) throws JSONException {
          try {
            String code = response.getString("code");
            String mes = response.getString("msg");
            if (code != null && code.equals("S00")) {
              try {
                QuestionListModel.deleteAll(QuestionListModel.class);
                QuestionListModelList.clear();
              } catch (SQLiteException e) {
              }


              String resp = response.getString("response");
              JSONObject respObj = new JSONObject(resp);

              String quesList = respObj.getString("questions");
              JSONArray questionArray = new JSONArray(quesList);
              for (int i = 0; i < questionArray.length(); i++) {
                JSONObject q = questionArray.getJSONObject(i);
                String quesCode = q.getString("code");
                String question = q.getString("question");
                QuestionListModel qlModel = new QuestionListModel(quesCode, question);
                qlModel.save();
                QuestionListModelList.add(qlModel);
              }

            }
          } catch (JSONException e) {
          }

        }

        @Override
        public void onError(ANError anError) {

        }
      });
  }

  static String getEmail(Context context) {
    AccountManager accountManager = AccountManager.get(context);
    Account account = getAccount(accountManager);

    if (account == null) {
      return null;
    } else {
      return account.name;
    }
  }

  private static Account getAccount(AccountManager accountManager) {
    Account[] accounts = accountManager.getAccountsByType("com.google");
    Account account;
    if (accounts.length > 0) {
      account = accounts[0];
    } else {
      account = null;
    }
    return account;
  }

}
