package in.msewa.fragment.treatcard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.Inflater;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.ecarib.activity.TravelHealthActivity;

public class TreatcardFragment extends Fragment {
  private View rootView;
  private UserModel session = UserModel.getInstance();

  private JSONObject jsonRequest;
  private LoadingDialog loadDlg;

  //Volley Tag
  private String tag_json_obj = "json_invite_freinds";

  private LinearLayout llTreatCard;
  private LinearLayout llTcValidity;
  private ProgressBar pbTreatCard;
  private TextView tvTreatCard0;
  private TextView tvTreatCard1;
  private TextView tvTreatCard2;
  private TextView tvTreatCard3;
  private TextView tvTreatCard4;
  private TextView tvTreatCardS1;
  //  private RadioGroup rgTreatCard;
  //  private RadioButton rbTreatCard1;
//  private RadioButton rbTreatCard2;
//  private RadioButton rbTreatCard3;
  private Button btnTreatCardPay;
  private Button btnTreatCardBack;

  private String amt;
  private HashMap<String, String> planslist;
  private TextView textlink;
  private LinearLayout llPlans;
  private String plansValue = "";
  private int currentHours = 0;
  ArrayList<RadioButton> radioButtons = new ArrayList<>();

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_treatcard_new, container, false);

    pbTreatCard = (ProgressBar) rootView.findViewById(R.id.pbTreatCard);
    llTreatCard = (LinearLayout) rootView.findViewById(R.id.llTreatCard);
    llTcValidity = (LinearLayout) rootView.findViewById(R.id.llTcValidity);
    tvTreatCard0 = (TextView) rootView.findViewById(R.id.tvTreatCard0);
    tvTreatCard1 = (TextView) rootView.findViewById(R.id.tvTreatCard1);
    textlink = (TextView) rootView.findViewById(R.id.textlink);
    tvTreatCard2 = (TextView) rootView.findViewById(R.id.tvTreatCard2);
    tvTreatCard3 = (TextView) rootView.findViewById(R.id.tvTreatCard3);
    tvTreatCard4 = (TextView) rootView.findViewById(R.id.tvTreatCard4);
    tvTreatCardS1 = (TextView) rootView.findViewById(R.id.tvTreatCardS1);
//    rgTreatCard = (RadioGroup) rootView.findViewById(R.id.rgTreatCard);
//    rbTreatCard1 = (RadioButton) rootView.findViewById(R.id.rbTreatCard1);
//    rbTreatCard2 = (RadioButton) rootView.findViewById(R.id.rbTreatCard2);
//    rbTreatCard3 = (RadioButton) rootView.findViewById(R.id.rbTreatCard3);
    btnTreatCardPay = (Button) rootView.findViewById(R.id.btnTreatCardPay);
    btnTreatCardBack = (Button) rootView.findViewById(R.id.btnTreatCardBack);
    llPlans = (LinearLayout) rootView.findViewById(R.id.llPlans);
    llPlans.setVisibility(View.GONE);


    btnTreatCardBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getActivity().finish();
      }
    });

    pbTreatCard.setVisibility(View.GONE);
    llTreatCard.setVisibility(View.GONE);

    btnTreatCardPay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (plansValue != null && !plansValue.equalsIgnoreCase("")) {

          getTreatCardPay(plansValue);
        } else {
          CustomToast.showMessage(getActivity(), "Please Select Plans");
        }
      }
    });
    textlink.setPaintFlags(textlink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    textlink.setText(R.string.teardcardlink);
    textlink.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent menuIntent = new Intent(getActivity(), TravelHealthActivity.class);
        getActivity().startActivity(menuIntent);
      }
    });

    getTreatCardDetails();

    return rootView;
  }

  private void getTreatCardDetails() {
    pbTreatCard.setVisibility(View.VISIBLE);
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("TreatCard Request", jsonRequest.toString());
      Log.i("TreatCard URL", ApiUrl.URL_TREAT_CARD);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TREAT_CARD, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          pbTreatCard.setVisibility(View.GONE);
          llTreatCard.setVisibility(View.VISIBLE);
          tvTreatCardS1.setText(String.valueOf(session.getUserFirstName()));
          try {
            Log.i("TreatCard Response", response.toString());
            String message = response.getString("message");
            String code = response.getString("code");
            boolean success = response.getBoolean("success");

            if (code != null && !code.equals("F04")) {
              pbTreatCard.setVisibility(View.VISIBLE);
              getTreatCardPlans();
              String res = response.getString("response");
              JSONObject resObj = new JSONObject(res);
              String cod = resObj.getString("code");
              String membershipCode = resObj.getString("membershipCode");
//                            String membershipCode = "0";
//                            String value = membershipCode.substring(0, 4) + membershipCode.substring(4, 8) + " " + membershipCode.substring(8, 12) + " " + membershipCode.substring(12, 16);
              String value = "";
              try {
                value = membershipCode;
              } catch (StringIndexOutOfBoundsException e) {

              }
              String memberShipCodeExpire = resObj.getString("memberShipCodeExpire");
              if (!membershipCode.equals("0")) {
                long memcodeexp = Long.parseLong(memberShipCodeExpire);
                String formated = getDateCurrentTimeZone(memcodeexp);
                String[] ddmm = formated.split("-");
                tvTreatCard4.setText("Hurray!\n\nHere is your Treatcard\nShow this card to get your Treat!");
                tvTreatCard0.setText("Extend validity for your Treats!");
                tvTreatCard3.setText("MEMBER");
//                                btnTreatCardBack.setVisibility(View.VISIBLE);
                DecimalFormat formatter = new DecimalFormat("####,####,####,####");
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(' ');
                formatter.setDecimalFormatSymbols(symbols);
                value = membershipCode.replaceAll("\\D", "").replaceAll("(\\d{4}(?!$))", "$1 ");
                tvTreatCard1.setText(String.valueOf(value));
                tvTreatCard2.setText(ddmm[2] + "/" + ddmm[1] + "/" + ddmm[0]);
              }
//              else {
//
////                                rbTreatCard1.setText(getActivity().getResources().getString(R.string.rupease) + "120/m");
////                                rbTreatCard2.setText(getContext().getResources().getString(R.string.rupease) + "720/6m");
////                                rbTreatCard3.setText(getContext().getResources().getString(R.string.rupease) + "1440/yr");
//              }
            } else if (code.equalsIgnoreCase("F03")) {
              showInvalidSessionDialog();
            } else if (code.equalsIgnoreCase("F04")) {
              pbTreatCard.setVisibility(View.VISIBLE);
              getTreatCardPlans();
              String res = response.getString("response");
              JSONObject resObj = new JSONObject(res);
              String membershipCode = resObj.getString("membershipCode");
              tvTreatCard3.setText("EXPIRED");
              tvTreatCard4.setVisibility(View.VISIBLE);
              tvTreatCard4.setText("Purchase your membership now to enjoy a whole year of Treats!");
              tvTreatCard0.setVisibility(View.VISIBLE);
              tvTreatCard0.setText("Your TreatCard Membership is expired, \nTo renew please choose the package.");
//                            String membershipCode = "0";
//                            String value = membershipCode.substring(0, 4) + membershipCode.substring(4, 8) + " " + membershipCode.substring(8, 12) + " " + membershipCode.substring(12, 16);
              String value = "";
              try {
                value = membershipCode.replaceAll("\\D", "").replaceAll("(\\d{4}(?!$))", "$1 ");
              } catch (StringIndexOutOfBoundsException e) {

              }
              tvTreatCard1.setText(String.valueOf(value));
            }
          } catch (JSONException e) {
            CustomToast.showMessage(getContext(), getContext().getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          CustomToast.showMessage(getContext(), NetworkErrorHandler.getMessage(error, getContext()));
          pbTreatCard.setVisibility(View.GONE);
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void getTreatCardPlans() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("TreatCard Request", jsonRequest.toString());
      Log.i("TreatCard URL", ApiUrl.URL_TREAT_CARD);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TREAT_CARD_PLANS, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("TreatCard Response", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              planslist = new HashMap<>();
              String res = response.getString("response");
              JSONObject resObj = new JSONObject(res);

              String strDetails = resObj.getString("details");
              JSONArray arrDetails = new JSONArray(strDetails);

              pbTreatCard.setVisibility(View.GONE);
              LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
              assert inflater != null;
              View passengerDetailView = inflater.inflate(R.layout.list_treat_card_plans, null, false);
              RadioGroup tvBusBookSeatNo = (RadioGroup) passengerDetailView.findViewById(R.id.viewPlansList);
              for (int i = 0; i < arrDetails.length(); i++) {
                JSONObject arrObj = arrDetails.getJSONObject(i);
                String days = arrObj.getString("days");
                String amount = arrObj.getString("amount");
                String status = arrObj.getString("status");
                String id = arrObj.getString("id");

                RadioButton radioButtonView = new RadioButton(getActivity());
                radioButtonView.setText(getActivity().getResources().getString(R.string.rupease) + amount + "/" + days + "d");
                radioButtonView.setId(i);
                radioButtonView.setChecked(i == currentHours);
                if (i == currentHours) {
                  plansValue = radioButtonView.getText().toString();
                }
                tvBusBookSeatNo.addView(radioButtonView);
                llPlans.setVisibility(View.VISIBLE);
              }
              llPlans.addView(passengerDetailView);
              tvBusBookSeatNo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                  RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                  try {
                    plansValue = radioButton.getText().toString();
                  } catch (Exception e1) {
                    e1.printStackTrace();
                  }
                }
              });
            } else if (code.equals("F03")) {
              pbTreatCard.setVisibility(View.GONE);
              showInvalidSessionDialog();
            } else {
              pbTreatCard.setVisibility(View.GONE);
              CustomToast.showMessage(getActivity(), response.getString("message"));
              getActivity().finish();
            }
          } catch (JSONException e) {
            pbTreatCard.setVisibility(View.GONE);
            CustomToast.showMessage(getActivity(), getContext().getResources().getString(R.string.server_exception2));
          } catch (NullPointerException e) {

          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          pbTreatCard.setVisibility(View.GONE);
          CustomToast.showMessage(getContext(), NetworkErrorHandler.getMessage(error, getContext()));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }

  }

  public String getDateCurrentTimeZone(long timestamp) {
    try {
      Calendar calendar = Calendar.getInstance();
      TimeZone tz = TimeZone.getDefault();
      calendar.setTimeInMillis(timestamp);
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      Date currenTimeZone = (Date) calendar.getTime();
      return sdf.format(currenTimeZone);
    } catch (Exception e) {
    }
    return "";
  }

  private View.OnClickListener mThisButtonListener = new View.OnClickListener() {
    public void onClick(View v) {
      try {

        plansValue = ((RadioButton) v).getText().toString();
      } catch (Exception e1) {
        e1.printStackTrace();
      }

    }
  };

  private void sendLogout() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void getTreatCardPay(String plansValue) {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

      String[] value = plansValue.split("/");
      jsonRequest.put("days", value[1].replace("d", ""));
      jsonRequest.put("amount", value[0].replace("₹", ""));

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("TreatCard Request", jsonRequest.toString());
      Log.i("TreatCard URL", ApiUrl.URL_TREAT_CARD);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TREAT_CARD_UPDATE, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("TreatCard Response", response.toString());
            String code = response.getString("code");
            loadDlg.dismiss();
            if (code != null && code.equals("S00")) {
              CustomSuccessDialog customSuccessDialog = new CustomSuccessDialog(getActivity(), "Payment Successful", "");
              customSuccessDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  sendRefresh();
                }
              });
              customSuccessDialog.show();
            } else if (code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {
              loadDlg.dismiss();
              CustomToast.showMessage(getActivity(), response.getString("message"));
              loadDlg.dismiss();

            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getContext(), getContext().getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getContext(), NetworkErrorHandler.getMessage(error, getContext()));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void sendRefresh() {
    getActivity().finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }


}
