package in.msewa.fragment.paymentlimit;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.msewa.custom.ExceptionHandler;
import in.msewa.metadata.AppMetadata;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;


public class PaymentLimitFragment extends Fragment {


  private View rootView;
  private TextView tvPayMerchant, tvSendMoneyUsers, tvBankTransfer;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getActivity()));
    rootView = inflater.inflate(R.layout.fragment_payment_limit, container, false);
    tvBankTransfer = rootView.findViewById(R.id.tvBankTransfer);
    tvPayMerchant = rootView.findViewById(R.id.tvPayMerchant);
    tvSendMoneyUsers = rootView.findViewById(R.id.tvSendMoneyUsers);
    tvBankTransfer.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        getCallFragment("Banktransfer");
      }
    });
    tvPayMerchant.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        getCallFragment("Merchant");
      }
    });
    tvSendMoneyUsers.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        getCallFragment("User");
      }
    });


    return rootView;
  }

  public void getCallFragment(String value) {

    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "PaymentLimitFragment").putExtra("paymentlimittype", value));
  }


}
