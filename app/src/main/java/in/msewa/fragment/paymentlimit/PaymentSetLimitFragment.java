package in.msewa.fragment.paymentlimit;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.ExceptionHandler;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveCartListner;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.ecarib.activity.OtpDeviceVerificationActivity;


public class PaymentSetLimitFragment extends Fragment implements TextWatcher {


  private MaterialEditText etTranscationOneDay, etTranscationOneMonth, etAmountOneDay, etAmountOneMonth;
  private TextView tvMonthTranscation, tvOneDayTranscation;
  private Boolean valid = false, apivalue;
  private Button btnSubmit;
  private TextView tvOneDayAmount, tvMonthAmount;
  private String paymentlimittype, onedayTrnaction = "0", monthlyTranscation = "0", oneydayAmount = "0", monthlyAmount = "0";
  private UserModel userModel = UserModel.getInstance();
  private LinearLayout llLimit;
  private boolean cancel = false;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getActivity()));
    View rootView = inflater.inflate(R.layout.fragment_payment_set_limit, container, false);
    paymentlimittype = getArguments().getString("paymentlimittype", "");
    llLimit = (LinearLayout) rootView.findViewById(R.id.llLimit);
    etAmountOneDay = rootView.findViewById(R.id.etAmountOneDay);
    etAmountOneMonth = rootView.findViewById(R.id.etAmountOneMonth);
    etTranscationOneDay = rootView.findViewById(R.id.etTranscationOneDay);
    tvMonthTranscation = rootView.findViewById(R.id.tvMonthTranscation);
    tvOneDayTranscation = rootView.findViewById(R.id.tvOneDayTranscation);
    etTranscationOneMonth = rootView.findViewById(R.id.etTranscationOneMonth);
    tvOneDayAmount = rootView.findViewById(R.id.tvOneDayAmount);
    tvMonthAmount = rootView.findViewById(R.id.tvMonthAmount);
    btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);
    btnSubmit.setVisibility(View.GONE);

    getTraniscationLimit();
    etAmountOneDay.addTextChangedListener(this);
    etAmountOneMonth.addTextChangedListener(this);
    etTranscationOneDay.addTextChangedListener(this);
    etTranscationOneMonth.addTextChangedListener(this);

    MainMenuDetailActivity.bindListener(new AddRemoveCartListner() {
      @Override
      public void taskCompleted(String response) {
        if (response.equalsIgnoreCase("true")) {

          btnSubmit.setVisibility(View.VISIBLE);
          getEanbleView(etAmountOneDay, tvOneDayAmount,"amount");
          getEanbleView(etAmountOneMonth, tvMonthAmount,"amount");
          getEanbleView(etTranscationOneMonth, tvMonthTranscation,"transcation");
          getEanbleView(etTranscationOneDay, tvOneDayTranscation,"transcation");
        }
      }
    });
    btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (!TextUtils.isEmpty(etTranscationOneDay.getText().toString()) && !TextUtils.isEmpty(etTranscationOneMonth.getText().toString())) {
          onedayTrnaction = etTranscationOneDay.getText().toString();
          monthlyTranscation = etTranscationOneMonth.getText().toString();
          if (Double.parseDouble(onedayTrnaction) > Double.parseDouble(monthlyTranscation)) {
            cancel = true;
            showCustomDialog("transactions");
          }
        }
        if (!TextUtils.isEmpty(etAmountOneDay.getText().toString()) && !TextUtils.isEmpty(etAmountOneMonth.getText().toString())) {
          oneydayAmount = etAmountOneDay.getText().toString();
          monthlyAmount = etAmountOneMonth.getText().toString();
          if (Double.parseDouble(oneydayAmount) > Double.parseDouble(monthlyAmount)) {
            cancel = true;
            showCustomDialog("transactions amount");
          }
        }
        if (!cancel) {
          saveTraniscationLimit();
        }

      }
    });
    return rootView;
  }

  private void showCustomDialog(String amount) {
    CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title, "Daily " + amount + " limit can not be larger than monthly " + amount + " limit");
    customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        cancel = false;
      }
    });
    customAlertDialog.show();
  }

  public void getEanbleView(MaterialEditText editText, TextView textView,String value) {
    textView.setVisibility(View.VISIBLE);
    textView.setText("Set ".concat(textView.getText().toString().replaceAll("Set", "")));
    editText.setHideUnderline(false);
    if(!value.equalsIgnoreCase("transcation")) {
      editText.setIconLeft(R.drawable.et_rupees);
    }
    editText.setFocusable(true);
    editText.setCursorVisible(true);
    editText.setFocusableInTouchMode(true);
  }

  public void getDisEnableView(MaterialEditText editText, TextView textView, String value) {
    textView.setVisibility(View.VISIBLE);
    textView.setText(textView.getText().toString().replaceAll("Set", ""));
    Double aDouble = Double.parseDouble(value);
    editText.setText(String.valueOf(aDouble.intValue()));
    editText.setHideUnderline(true);
    editText.setIconLeft((Bitmap) null);
    editText.setFocusable(false);
    editText.setUnderlineColor(getResources().getColor(R.color.white_text));
    editText.setBackgroundColor(getResources().getColor(R.color.white_text));
    editText.setCursorVisible(false);
    editText.setFocusableInTouchMode(false);
  }

  private void saveTraniscationLimit() {
    final LoadingDialog loadingDialog = new LoadingDialog(getActivity());
    loadingDialog.show();
    final JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put("dailyNoOfTxn", etTranscationOneDay.getText().toString());
      jsonObject.put("monthlyNoOfTxn", etTranscationOneMonth.getText().toString());
      jsonObject.put("dailyAmountOfTxn", etAmountOneDay.getText().toString());
      jsonObject.put("monthlyAmountOfTxn", etAmountOneMonth.getText().toString());
      jsonObject.put("transactionType", paymentlimittype);
      jsonObject.put("mobileToken", "");
      jsonObject.put("sessionId", userModel.getUserSessionId());
      Log.i("loginpage", jsonObject.toString());
      AndroidNetworking.post(ApiUrl.URL_UPDATE_TRANSATION_LIMIT)
        .setPriority(Priority.HIGH)
        .setTag("test")
        .addJSONObjectBody(jsonObject)
        .build().getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) throws JSONException {
          loadingDialog.dismiss();
          if (response.getString("code").equalsIgnoreCase("S00")) {
            btnSubmit.setVisibility(View.GONE);
//            CustomToast.showMessage(getActivity(), response.getString("message"));
            getDisEnableView(etAmountOneDay, tvOneDayAmount, etAmountOneDay.getText().toString());
            getDisEnableView(etAmountOneMonth, tvMonthAmount, etAmountOneMonth.getText().toString());
            getDisEnableView(etTranscationOneMonth, tvMonthTranscation, etTranscationOneMonth.getText().toString());
            getDisEnableView(etTranscationOneDay, tvOneDayTranscation, etTranscationOneDay.getText().toString());
            getActivity().finish();
            startActivity(new Intent(getActivity(), OtpDeviceVerificationActivity.class).putExtra("type", "paymentlimit").putExtra("jsonRequest", jsonObject.toString()));
          } else if (response.getString("code").equalsIgnoreCase("F03")) {
            showInvalidSessionDialog();
          } else {
            CustomToast.showMessage(getActivity(), response.getString("message"));
          }
        }

        @Override
        public void onError(ANError anError) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

        }
      });
    } catch (JSONException e) {
      loadingDialog.dismiss();
      CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

    }


  }


  private void getTraniscationLimit() {
    final LoadingDialog loadingDialog = new LoadingDialog(getActivity());
    loadingDialog.show();
    llLimit.setVisibility(View.GONE);
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put("transactionType", paymentlimittype);
      jsonObject.put("sessionId", userModel.getUserSessionId());
      AndroidNetworking.post(ApiUrl.URL_GET_TRANSATION_LIMIT)
        .setPriority(Priority.HIGH)
        .setTag("test")
        .addJSONObjectBody(jsonObject)
        .build().getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) throws JSONException {
          loadingDialog.dismiss();
          llLimit.setVisibility(View.VISIBLE);
          if (response.getString("code").equalsIgnoreCase("S00")) {
            btnSubmit.setVisibility(View.GONE);
            JSONObject jsonObject1 = new JSONObject(response.getString("details"));

            getDisEnableView(etAmountOneDay, tvOneDayAmount, jsonObject1.getString("dailyAmountOfTxn"));
            getDisEnableView(etAmountOneMonth, tvMonthAmount, jsonObject1.getString("monthlyAmountOfTxn"));
            getDisEnableView(etTranscationOneMonth, tvMonthTranscation, jsonObject1.getString("monthlyNoOfTxn"));
            getDisEnableView(etTranscationOneDay, tvOneDayTranscation, jsonObject1.getString("dailyNoOfTxn"));
            valid = false;
          } else if (response.getString("code").equalsIgnoreCase("F03")) {
            showInvalidSessionDialog();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadingDialog.dismiss();
          llLimit.setVisibility(View.GONE);
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

        }
      });
    } catch (JSONException e) {
      loadingDialog.dismiss();
      llLimit.setVisibility(View.GONE);
      CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

    }


  }


  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

  }

  @Override
  public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

  }

  @Override
  public void afterTextChanged(Editable editable) {
//    if (!editable.toString().contains("No Limit")) {
//      Double aDouble = Double.parseDouble(editable.toString());
//      if (editable.length() > 0 && aDouble.intValue() > 0) {
//        btnSubmit.setEnabled(true);
//      } else {
//        btnSubmit.setEnabled(false);
//        CustomToast.showMessage(getActivity(), "Amount greater than 0");
//      }
//    }

  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    getActivity().finish();
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }
}
