package in.msewa.fragment.fragmenttravel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import com.rengwuxian.materialedittext.MaterialEditText;

import in.msewa.custom.CustomToast;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 9/26/2016.
 */
public class CabTravelFragment extends Fragment {

    private RadioButton rbCabOneWay,rbCabRoundTrip;
    private MaterialEditText etCabFrom,etCabTo,etCabPickDate,etCabPickTime,etCabDays;
    private Button btnSearchCab;

    private View focusView = null;
    private boolean cancel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_travel_cab, container, false);
        etCabFrom = (MaterialEditText) rootView.findViewById(R.id.etCabFrom);
        etCabTo = (MaterialEditText) rootView.findViewById(R.id.etCabTo);
        etCabPickDate = (MaterialEditText) rootView.findViewById(R.id.etCabPickDate);
        etCabPickTime = (MaterialEditText) rootView.findViewById(R.id.etCabPickTime);
        etCabDays = (MaterialEditText) rootView.findViewById(R.id.etCabDays);

        rbCabOneWay = (RadioButton) rootView.findViewById(R.id.rbCabOneWay);
        rbCabRoundTrip = (RadioButton) rootView.findViewById(R.id.rbCabRoundTrip);

        Button btnSearchCab = (Button) rootView.findViewById(R.id.btnSearchCab);
        btnSearchCab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidity();
            }
        });
        return rootView;
    }

    private void checkValidity() {
        etCabFrom.setError(null);
        etCabTo.setError(null);
        etCabPickDate.setError(null);
        etCabPickTime.setError(null);
        etCabDays.setError(null);
        cancel = false;

        checkFrom(etCabFrom.getText().toString());
        checkTo(etCabTo.getText().toString());
        checkDate(etCabPickDate.getText().toString());
        checkTime(etCabPickTime.getText().toString());
        checkDays(etCabDays.getText().toString());

        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteSearch();
        }
    }

    private void checkFrom(String cabFrom) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabFrom);
        if (!gasCheckLog.isValid) {
            etCabFrom.setError(getString(gasCheckLog.msg));
            focusView = etCabFrom;
            cancel = true;
        }
    }


    private void checkTo(String cabTo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabTo);
        if (!gasCheckLog.isValid) {
            etCabTo.setError(getString(gasCheckLog.msg));
            focusView = etCabTo;
            cancel = true;
        }
    }

    private void checkDate(String cabDate) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabDate);
        if (!gasCheckLog.isValid) {
            etCabPickDate.setError(getString(gasCheckLog.msg));
            focusView = etCabPickDate;
            cancel = true;
        }
    }

    private void checkDays(String cabDays) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabDays);
        if (!gasCheckLog.isValid) {
            etCabDays.setError(getString(gasCheckLog.msg));
            focusView = etCabDays;
            cancel = true;
        }
    }

    private void checkTime(String cabTime) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabTime);
        if (!gasCheckLog.isValid) {
            etCabPickTime.setError(getString(gasCheckLog.msg));
            focusView = etCabPickTime;
            cancel = true;
        }
    }

    private void promoteSearch(){
        CustomToast.showMessage(getActivity(),"Ready to search cab");
    }

}
