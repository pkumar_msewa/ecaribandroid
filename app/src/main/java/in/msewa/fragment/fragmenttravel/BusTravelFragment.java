package in.msewa.fragment.fragmenttravel;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import in.msewa.adapter.BusBookListAdapter;
import in.msewa.adapter.HomeSliderAdapter;
import in.msewa.custom.CirclePageIndicator;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomAlertDialogSearch;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BankListModel;
import in.msewa.model.BusBookedTicketModel;
import in.msewa.model.BusCityModel;
import in.msewa.model.BusPassengerModel;
import in.msewa.model.BusSaveModel;
import in.msewa.model.TravelCityModel;
import in.msewa.model.UserModel;
import in.msewa.util.CitySelectedListener;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.businneractivity.BusListActivity;
import in.msewa.ecarib.activity.businneractivity.BusNewCitySearchActivity;


/**
 * Created by Ksf on 9/26/2016.
 */
public class BusTravelFragment extends Fragment implements ViewPager.OnPageChangeListener {
  private View rootView;

  //Views
//    private RadioButton rbBusOneWay, rbBusRoundTrip;
  private MaterialEditText etBusFrom, etBusTo, etBusDepartDate;
  //    private MaterialEditText etBusReturnDate;
  private Button btnSearchBus;
//    private RadioGroup rgBusTripType;

  private View focusView = null;
  private boolean cancel;
  private MaterialEditText spBusFrom, spBusTo;


  //Variables
  private long selectedFromCityCode, selectedToCityCode;
  private String selectedFromCityName, selectedToCityName;
  private ArrayList<String> cities = new ArrayList<>();

  //search city
  private JSONObject jsonRequest;
  private LoadingDialog loadingDialog;
  private UserModel session = UserModel.getInstance();
  private ArrayList<BusCityModel> busCityModelList;
  private String tag_json_obj = "json_bus_city_search";
  private ArrayAdapter cityAdapter;
  private boolean lock = false;
  //image slider
  private static int currentPage = 0;
  private static int NUM_PAGES = 0;
  private int[] IMAGES;
  public ViewPager mPager;
  public CirclePageIndicator indicator;

  //Previous book tickets
  private RecyclerView rvPreviousTickets;
  private LinearLayout llpbPreviousTickets;
  private ArrayList<BusPassengerModel> busPassengerArrayList;
  private TextView tvHeaderBookedTickets;
  private LinearLayout llFlightSearch;
  private ProgressBar pbBar;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_travel_bus, container, false);
    btnSearchBus = (Button) rootView.findViewById(R.id.btnSearchBus);
    etBusFrom = (MaterialEditText) rootView.findViewById(R.id.etBusFrom);
    etBusTo = (MaterialEditText) rootView.findViewById(R.id.etBusTo);
    etBusDepartDate = (MaterialEditText) rootView.findViewById(R.id.etBusDepartDate);
    spBusFrom = (MaterialEditText) rootView.findViewById(R.id.spBusFrom);
    spBusTo = (MaterialEditText) rootView.findViewById(R.id.spBusTo);
    mPager = (ViewPager) rootView.findViewById(R.id.pagerHome);
    indicator = (CirclePageIndicator) rootView.findViewById(R.id.productHeaderImageSlider);
    tvHeaderBookedTickets = (TextView) rootView.findViewById(R.id.tvHeaderBookedTickets);
    loadingDialog = new LoadingDialog(getActivity());
    rvPreviousTickets = (RecyclerView) rootView.findViewById(R.id.rvPreviousTickets);
    llpbPreviousTickets = (LinearLayout) rootView.findViewById(R.id.llpbPreviousTickets);
    rvPreviousTickets.setNestedScrollingEnabled(false);
    llFlightSearch = (LinearLayout) rootView.findViewById(R.id.llFlightSearch);
    pbBar = (ProgressBar) rootView.findViewById(R.id.pbBar);
    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    String formattedDate = df.format(c.getTime());
    etBusDepartDate.setText(formattedDate);
    etBusFrom.setText("");
    etBusTo.setText("");
    busCityModelList = new ArrayList<>();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(bMessageReceiver, new IntentFilter("booking-done"));
    getImageSlider();
    List<BusSaveModel> saveModels = Select.from(BusSaveModel.class).list();

    if (saveModels != null && saveModels.size() != 0) {
      JSONArray citydata = null;
      try {
        citydata = new JSONArray(saveModels.get(0).getCityname());
        for (int i = 0; i < citydata.length(); i++) {
          JSONObject cityValues = citydata.getJSONObject(i);
          long cityId = cityValues.getLong("cityId");
          String cityName = cityValues.getString("cityName");
          BusCityModel cModel = new BusCityModel(cityId, cityName);
          busCityModelList.add(cModel);
        }
        if (busCityModelList.size() != 0) {
          ArrayList<BusCityModel> busCityModels = AppMetadata.getBusCities();
          for (int i = 0; i < busCityModelList.size(); i++) {
            for (int j = 0; j < busCityModels.size(); j++) {
              if (busCityModelList.get(i).getCityname().equalsIgnoreCase(busCityModels.get(j).getCityname())) {
                Collections.swap(busCityModelList, j, i);
              }

            }
          }
        }
      } catch (JSONException e) {
        e.printStackTrace();
      }

    } else {
      getCity();
    }
    getBookedTicketsList();
//        spBusFrom.setTitle("Select Source City");
    spBusFrom.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final CustomAlertDialogSearch search = new CustomAlertDialogSearch(getActivity(), busCityModelList, "from", new CitySelectedListener() {
          @Override
          public void citySelected(String type, long cityCode, String cityName) {

          }

          @Override
          public void cityFilter(BusCityModel type) {

            spBusFrom.setText(type.getCityname());
            selectedFromCityCode = type.getCityId();
            selectedFromCityName = type.getCityname();
            if (selectedToCityCode != 0) {
              if (selectedToCityCode == selectedFromCityCode) {
                spBusFrom.setText("");

                CustomToast.showMessage(getActivity(), "source city could not be same as destination city.");
              }
            }

          }

          @Override
          public void bankFilter(BankListModel type) {

          }

          @Override
          public void travelFilter(TravelCityModel type) {

          }
        });
        search.show();
      }
    });
//        spBusFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedFromCityCode = busCityModelList.get(position).getCityId();
//                selectedFromCityName = busCityModelList.get(position).getCityname();
//                if(selectedToCityCode!=0){
//                    lock= selectedToCityCode == selectedFromCityCode;
//                    if(lock) {
//                        Toast.makeText(getActivity(), "source city could not be same as destination city.", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        spBusTo.setTitle("Select Destination City");
//        spBusTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedToCityCode = busCityModelList.get(position).getCityId();
//                selectedToCityName = busCityModelList.get(position).getCityname();
//                if (selectedFromCityCode != 0) {
//                    lock = selectedToCityCode == selectedFromCityCode;
//                    if (lock) {
//                        Toast.makeText(getActivity(), "destination city could not be same as source city.", Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
    spBusTo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CustomAlertDialogSearch search = new CustomAlertDialogSearch(getActivity(), busCityModelList, "To", new CitySelectedListener() {
          @Override
          public void citySelected(String type, long cityCode, String cityName) {

          }

          @Override
          public void cityFilter(BusCityModel type) {
            spBusTo.setText(type.getCityname());
            selectedToCityCode = type.getCityId();
            selectedToCityName = type.getCityname();
            if (selectedFromCityCode != 0) {
              if (selectedToCityCode == selectedFromCityCode) {
                spBusTo.setText("");
                CustomToast.showMessage(getActivity(), "source city could not be same as destination city.");
              }
            }

          }

          @Override
          public void bankFilter(BankListModel type) {

          }

          @Override
          public void travelFilter(TravelCityModel type) {

          }

        });
        search.show();
      }
    });
//
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("search-complete"));

    etBusDepartDate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        if (etBusDepartDate.getText().toString() != null && !etBusDepartDate.getText().toString().isEmpty()) {
          String[] dateSplit = etBusDepartDate.getText().toString().split("-");

          int mYear = Integer.valueOf(dateSplit[2]);
          int mMonth = Integer.valueOf(dateSplit[1]) - 1;
          int mDay = Integer.valueOf(dateSplit[0]);
          final Calendar c = Calendar.getInstance();
          int year = c.get(Calendar.YEAR);
          int month = c.get(Calendar.MONTH);
          int day = c.get(Calendar.DAY_OF_MONTH);

          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0".concat(String.valueOf(dayOfMonth));
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0".concat(String.valueOf(monthOfYear + 1));
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etBusDepartDate.setText(String.valueOf(formattedDay) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(year));
            }
          }, mYear, mMonth, mDay);
          dialog.getDatePicker().setMinDate(c.getTimeInMillis());
          dialog.setCustomTitle(null);
          dialog.setTitle("");
          dialog.show();
        } else {
          final Calendar c = Calendar.getInstance();
          int mYear = c.get(Calendar.YEAR);
          int mMonth = c.get(Calendar.MONTH);
          int mDay = c.get(Calendar.DAY_OF_MONTH);

          DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              String formattedDay, formattedMonth;
              if (dayOfMonth < 10) {
                formattedDay = "0" + dayOfMonth;
              } else {
                formattedDay = dayOfMonth + "";
              }

              if ((monthOfYear + 1) < 10) {
                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
              } else {
                formattedMonth = String.valueOf(monthOfYear + 1) + "";
              }
              etBusDepartDate.setText(String.valueOf(formattedDay) + "-"
                + String.valueOf(formattedMonth) + "-"
                + String.valueOf(year));
            }
          }, mYear, mMonth, mDay);

          dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
          dialog.setTitle("");
          dialog.setCustomTitle(null);
          dialog.show();
        }

      }
    });
    etBusFrom.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent searchCityIntent = new Intent(getActivity(), BusNewCitySearchActivity.class);
        searchCityIntent.putExtra("searchType", "From");
        startActivity(searchCityIntent);

      }
    });

    etBusTo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent searchCityIntent = new Intent(getActivity(), BusNewCitySearchActivity.class);
        searchCityIntent.putExtra("searchType", "To");
        startActivity(searchCityIntent);

      }
    });

    btnSearchBus.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptSearch();
      }
    });

    return rootView;
  }

  private void attemptSearch() {

    etBusDepartDate.setError(null);
    if (!spBusFrom.getText().toString().equals("")) {
      if (!spBusTo.getText().toString().equals("") && !spBusTo.getText().toString().equals("")) {
        if (etBusDepartDate.getText().toString().isEmpty()) {
          etBusDepartDate.setError("Please select departure date");
          focusView = etBusDepartDate;
          focusView.requestFocus();
        } else {
          if (lock) {
            CustomToast.showMessage(getActivity(), "Please select desitination city other then source city");
          } else {
            promoteSearch();
          }
        }
      } else {
        CustomToast.showMessage(getActivity(), "Select Destination City");
      }
    } else {
      CustomToast.showMessage(getActivity(), "Select Source City");
    }
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("searchType");

      if (action.equals("To")) {
        selectedToCityCode = intent.getLongExtra("selectedCityCode", 0);
        selectedToCityName = intent.getStringExtra("selectedCityName");
        if (selectedToCityCode == selectedFromCityCode) {
          CustomToast.showMessage(getActivity(), "Please destination city other than source city.");
        } else {
          etBusTo.setText(selectedToCityName);
        }

      } else {
        selectedFromCityCode = intent.getLongExtra("selectedCityCode", 0);
        selectedFromCityName = intent.getStringExtra("selectedCityName");
        etBusFrom.setText(selectedFromCityName);
      }
    }
  };

  private void getImageSlider() {
    IMAGES = new int[]{R.drawable.bus_img_slider_1, R.drawable.bus_img_slider_2, R.drawable.bus_img_slider_3};
    mPager.setAdapter(new HomeSliderAdapter(getActivity(), IMAGES));
    indicator.setViewPager(mPager);
    final float density = getActivity().getResources().getDisplayMetrics().density;
    indicator.setRadius(5 * density);

    NUM_PAGES = IMAGES.length;

    // Auto start of viewpager
    final Handler handler = new Handler();
    final Runnable Update = new Runnable() {
      public void run() {
        if (currentPage == NUM_PAGES) {
          currentPage = 0;
        }
        mPager.setCurrentItem(currentPage++, true);
      }
    };
    Timer swipeTimer = new Timer();
    swipeTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        handler.post(Update);
      }
    }, 3000, 3000);

    // Pager listener over indicator
    indicator.setOnPageChangeListener(this);
//        {
//
//            @Override
//            public void onPageSelected(int position) {
//                currentPage = position;
//
//            }
//
//            @Override
//            public void onPageScrolled(int pos, float arg1, int arg2) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int pos) {
//
//            }
//        });
//    }
  }

  @Override
  public void onDestroyView() {
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(bMessageReceiver);
    super.onDestroyView();
  }

  private void promoteSearch() {
    Intent getBusIntent = new Intent(getActivity(), BusListActivity.class);
    getBusIntent.putExtra("date", etBusDepartDate.getText().toString());
    getBusIntent.putExtra("destination", selectedToCityCode);
    getBusIntent.putExtra("source", selectedFromCityCode);
    getBusIntent.putExtra("sourceName", selectedFromCityName);
    getBusIntent.putExtra("destinationName", selectedToCityName);
    startActivity(getBusIntent);
  }

  private void getCity() {
    llFlightSearch.setVisibility(View.GONE);
    pbBar.setVisibility(View.VISIBLE);

    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    AndroidNetworking.post(ApiUrl.URL_BUS_GET_CITY)
      .addJSONObjectBody(jsonRequest) // posting json
      .setTag("test")
      .setPriority(Priority.HIGH)
      .build()
      .getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          Log.d("values", response.toString());
          try {
            llFlightSearch.setVisibility(View.VISIBLE);
            pbBar.setVisibility(View.GONE);
//          } catch (NullPointerException e) {
//
//          }
            if (response.has("code")) {
              String code = response.getString("code");
              if (code != null && code.equals("S00")) {
                JSONArray citydata = response.getJSONArray("details");
                BusSaveModel busSaveModel = new BusSaveModel(citydata.toString());
                busSaveModel.save();
                for (int i = 0; i < citydata.length(); i++) {
                  JSONObject c = citydata.getJSONObject(i);
                  long cityId = c.getLong("cityId");
                  String cityName = c.getString("cityName");
                  BusCityModel cModel = new BusCityModel(cityId, cityName);
                  busCityModelList.add(cModel);
                }
                if (busCityModelList.size() != 0) {
                  ArrayList<BusCityModel> busCityModels = AppMetadata.getBusCities();
                  for (int i = 0; i < busCityModelList.size(); i++) {
                    for (int j = 0; j < busCityModels.size(); j++) {
                      if (busCityModelList.get(i).getCityname().equalsIgnoreCase(busCityModels.get(j).getCityname())) {
                        Collections.swap(busCityModelList, j, i);
                      }

                    }
                  }
                  getBookedTicketsList();
//                                        cityAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, cities);
//                                        spBusFrom.setAdapter(cityAdapter);
//                                        spBusTo.setAdapter(cityAdapter);
                }

              } else if (code != null && code.equals("F03")) {
                llFlightSearch.setVisibility(View.GONE);
                pbBar.setVisibility(View.VISIBLE);
                showInvalidSessionDialog(response.getString("message"));

              } else {
                if (response.has("message") && response.getString("message") != null) {
                  llFlightSearch.setVisibility(View.VISIBLE);
                  pbBar.setVisibility(View.GONE);
                  String message = response.getString("message");
                  CustomToast.showMessage(getActivity(), message);
                }
              }
            }
          } catch (
            JSONException e)

          {
            llFlightSearch.setVisibility(View.VISIBLE);
            pbBar.setVisibility(View.GONE);
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
            try {
              loadingDialog.dismiss();
            } catch (NullPointerException e1) {

            }

          }
        }

        @Override
        public void onError(ANError error) {
          try {
            llFlightSearch.setVisibility(View.VISIBLE);
            pbBar.setVisibility(View.GONE);
          } catch (NullPointerException e) {

          }
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
        }
      });
  }

  private BroadcastReceiver bMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("detail");
      if (action.equals("1")) {
        getBookedTicketsList();
      }

    }
  };

  public void getBookedTicketsList() {
    llpbPreviousTickets.setVisibility(View.VISIBLE);
    rvPreviousTickets.setVisibility(View.GONE);
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
    }


    AndroidNetworking.post(ApiUrl.URL_BUS_GET_BOOKED_TICKETS)
      .addJSONObjectBody(jsonRequest) // posting json
      .setTag("test")
      .setPriority(Priority.IMMEDIATE)
      .build()
      .getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject jsonObj) {

          try {
            Log.i("BUSBOOKEDRES", jsonObj.toString());
            String code = jsonObj.getString("code");
            String message = jsonObj.getString("message");

            if (code != null && code.equals("S00")) {
              ArrayList<BusBookedTicketModel> busBookedTicketArrayList = new ArrayList<>();
              String details = jsonObj.getString("details");
              JSONArray busBookedTicketarray = new JSONArray(details);

              for (int i = 0; i < busBookedTicketarray.length(); i++) {
                JSONObject c = busBookedTicketarray.getJSONObject(i);
                String busTicketString = c.getString("busTicket");
                JSONObject busTicket = new JSONObject(busTicketString);
                String userMobile = busTicket.getString("userMobile");
                String userEmail = busTicket.getString("userEmail");
                String ticketPnr = busTicket.getString("ticketPnr");
                String operatorPnr = busTicket.getString("operatorPnr");
                String emtTxnId = busTicket.getString("emtTxnId");
                String busId = busTicket.getString("busId");
                double totalFare = busTicket.getDouble("totalFare");
                String journeyDate = busTicket.getString("journeyDate");
                String source = busTicket.getString("source");
                String destination = busTicket.getString("destination");
                String boardingId = busTicket.getString("boardingId");
                String boardingAddress = busTicket.getString("boardingAddress");
                String busOperator = busTicket.getString("busOperator");
                String arrTime = busTicket.getString("arrTime");
                String deptTime = busTicket.getString("deptTime");
                String travellerDetailsSTring = c.getString("travellerDetails");
                JSONArray travellerDetails = new JSONArray(travellerDetailsSTring);
                if (travellerDetails.length() != 0) {
                  busPassengerArrayList = new ArrayList<>();
                  for (int j = 0; j < travellerDetails.length(); j++) {
                    JSONObject p = travellerDetails.getJSONObject(j);
                    String fName = p.getString("fName");
                    String lName = p.getString("lName");
                    String age = p.getString("age");
                    String gender = p.getString("gender");
                    String seatNo = p.getString("seatNo");
                    String seatType = p.getString("seatType");
                    String fare = p.getString("fare");
                    BusPassengerModel busPassengerModel = new BusPassengerModel(fName, lName, age, gender, seatNo, seatType, fare);
                    busPassengerArrayList.add(busPassengerModel);
                  }
                }
                BusBookedTicketModel busBookedTicketModel = new BusBookedTicketModel(userMobile, userEmail, ticketPnr, operatorPnr, emtTxnId, busId, totalFare, journeyDate, source, destination, boardingId, boardingAddress, busOperator, arrTime, deptTime, busPassengerArrayList, "", "");
                busBookedTicketArrayList.add(busBookedTicketModel);

              }
              if (busBookedTicketArrayList != null && busBookedTicketArrayList.size() != 0) {
//                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
                rvPreviousTickets.addItemDecoration(new SpacesItemDecoration(4));
                GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
                rvPreviousTickets.setLayoutManager(manager);
                rvPreviousTickets.setHasFixedSize(true);

                BusBookListAdapter itemAdp = new BusBookListAdapter(getActivity(), busBookedTicketArrayList);
                rvPreviousTickets.setAdapter(itemAdp);
                tvHeaderBookedTickets.setVisibility(View.VISIBLE);
                llpbPreviousTickets.setVisibility(View.GONE);
                rvPreviousTickets.setVisibility(View.VISIBLE);

              } else {
                llpbPreviousTickets.setVisibility(View.GONE);
              }
            } else if (code != null && code.equals("F03")) {
              llpbPreviousTickets.setVisibility(View.GONE);
//                            if(isAdded()){
              showInvalidSessionDialog(message);
//                            }
            } else {
              llpbPreviousTickets.setVisibility(View.GONE);
              CustomToast.showMessage(getActivity(), message);

            }

          } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Oops, something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
            llpbPreviousTickets.setVisibility(View.GONE);
          }
        }

        @Override
        public void onError(ANError error) {
          try {
            loadingDialog.dismiss();
            llpbPreviousTickets.setVisibility(View.GONE);
          } catch (NullPointerException e) {

          }
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

        }
      });


  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }

  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {
    currentPage = position;
  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void onStop() {
    super.onStop();
    if (loadingDialog != null) {
      loadingDialog.dismiss();
      loadingDialog = null;
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (loadingDialog != null) {
      loadingDialog.dismiss();
      loadingDialog = null;
    }
  }
}

