package in.msewa.fragment.fragmenttravel;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ebs.android.sdk.Config.Encryption;
import com.ebs.android.sdk.Config.Mode;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomDisclaimerDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.flightinneracitivty.FlightVnetWebViewActivity;


public class FlightPaymentActivity extends AppCompatActivity {

  private View rootView;
  private MaterialEditText etLoadMoneyAmount;
  private Button btnLoadMoney;
  private RadioButton rbLoadMoneyVBank, rbLoadMoneyOther;
  private View focusView = null;
  private boolean cancel;
  private String amount = null;
  AlertDialog.Builder payDialog;
  private UserModel session = UserModel.getInstance();
  private LoadingDialog loadDlg;
  private RequestQueue rq;
  private boolean isVBank = true;
  private String inValidMessage = "";
  private String tag_json_obj = "load_money";
  private JSONObject jsonRequest;
  String autoFill;
  double loadAmount;
  private String jsonBokking;
  private SharedPreferences loadMoneyPref;
  private RadioButton rbLoadMoneyWallet;
  private boolean roundTrip;
  private boolean contingFlight;
  private Toolbar toolbar;
  private ImageButton ivBackBtn;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_flght_load_money);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    payDialog = new AlertDialog.Builder(FlightPaymentActivity.this, R.style.AppCompatAlertDialogStyle);
    loadDlg = new LoadingDialog(FlightPaymentActivity.this);
    rbLoadMoneyWallet = (RadioButton) findViewById(R.id.rbLoadMoneyWallet);
    try {
      loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
    } catch (NullPointerException e) {

    }
    try {
      final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(FlightPaymentActivity.this);
      rq = Volley.newRequestQueue(FlightPaymentActivity.this, new HurlStack(null, sslSocketFactory));
    } catch (KeyManagementException | NoSuchAlgorithmException e) {
      e.printStackTrace();
    }

    btnLoadMoney = (Button) findViewById(R.id.btnLoadMoney);
    etLoadMoneyAmount = (MaterialEditText) findViewById(R.id.etLoadMoneyAmount);
    rbLoadMoneyVBank = (RadioButton) findViewById(R.id.rbLoadMoneyVBank);
    rbLoadMoneyOther = (RadioButton) findViewById(R.id.rbLoadMoneyOther);


    String loadAmountString = String.valueOf(getIntent().getDoubleExtra("Amount", 0));
    jsonBokking = getIntent().getStringExtra("flightbooking");
    roundTrip = getIntent().getBooleanExtra("roundTrip", false);
    contingFlight = getIntent().getBooleanExtra("conting", false);
    Log.i("value", String.valueOf(contingFlight));
    Log.i("jsonPayment", jsonBokking.toString());
    loadAmount = Math.ceil(Double.parseDouble(loadAmountString));
    DecimalFormat format = new DecimalFormat("0.#");
    amount = loadAmountString;
    etLoadMoneyAmount.setText(String.valueOf(format.format(loadAmount)));
//
    btnLoadMoney.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (rbLoadMoneyVBank.isChecked()) {
          isVBank = true;
        } else if (rbLoadMoneyOther.isChecked()) {
          isVBank = false;
        } else if (rbLoadMoneyWallet.isChecked()) {
          isVBank = false;
        } else {
          isVBank = true;
        }
        attemptLoad();

      }
    });


    //DONE CLICK ON VIRTUAL KEYPAD
    etLoadMoneyAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          if (rbLoadMoneyVBank.isChecked()) {
            isVBank = true;
          } else if (rbLoadMoneyOther.isChecked()) {
            isVBank = false;
          } else {
            isVBank = true;
          }
          attemptLoad();
        }
        return false;
      }
    });

  }


  private void attemptLoad() {
    etLoadMoneyAmount.setError(null);
    cancel = false;
    amount = etLoadMoneyAmount.getText().toString();
    checkPayAmount(amount);
//        checkUserType();

    if (cancel) {
      focusView.requestFocus();
    } else {
      if (isVBank) {
        checkTrxTime();
      } else if (rbLoadMoneyWallet.isChecked()) {
        JSONObject jsonRequest = null;
        try {
          jsonRequest = new JSONObject(jsonBokking);
          jsonRequest.put("paymentMethod", "Wallet");
          longLog(jsonRequest.toString());
        } catch (JSONException e) {
          e.printStackTrace();
          jsonRequest = null;
        }

        String URL = "";

        loadDlg.show();
        AndroidNetworking.post(ApiUrl.URL_FLIGHT_BOOKING)
          .addJSONObjectBody(jsonRequest) // posting json
          .setTag("mpin")
          .setPriority(Priority.HIGH)
          .addHeaders("hash", "1234")
          .build()
          .getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
              try {
                Log.i("CheckOut Resonse", response.toString());
                String code = response.getString("code");
                loadDlg.dismiss();
                if (code != null && code.equals("S00")) {

                  loadDlg.dismiss();
                  showSuccessDialog();

                } else if (code != null && code.equals("F03")) {
                  loadDlg.dismiss();
                  showInvalidSessionDialog(response.getString("message"));
                } else {
                  String message = response.getString("message");
                  loadDlg.dismiss();
                  CustomToast.showMessage(FlightPaymentActivity.this, message);

                }
              } catch (JSONException e) {
                loadDlg.dismiss();
                CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception2));
                e.printStackTrace();
              }
            }

            @Override
            public void onError(ANError error) {
              loadDlg.dismiss();
              CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception));
              error.printStackTrace();
            }
          });
      } else {
        showCustomDisclaimerDialog();
      }
    }
  }

  public void showCustomDisclaimerDialog() {
    CustomDisclaimerDialog builder = new CustomDisclaimerDialog(FlightPaymentActivity.this);
    builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        checkTrxTime();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showNonKYCDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(FlightPaymentActivity.this, R.string.dialog_title2, Html.fromHtml(generateKYCMessage()));
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }


  private void checkPayAmount(String amount) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
    try {
      if (!gasCheckLog.isValid) {
        etLoadMoneyAmount.setError(getString(gasCheckLog.msg));
        focusView = etLoadMoneyAmount;
        cancel = true;
      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  private boolean checkUserType() {
    if (amount != null && !amount.isEmpty()) {
      if (Integer.valueOf(amount) > 10000) {
        focusView = etLoadMoneyAmount;
        cancel = true;
        if (session.getUserAcName().equals("Non-KYC")) {
          showNonKYCDialog();
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }

  }

  public String generateMessage() {
    String source = "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
      "<br><b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    return source;
  }


  public String generateKYCMessage() {
    return "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
      "<br><b><font color=#ff0000> Sorry you cannot load more than 10,000 at a time.</font></b><br>" +
      "<br><b><font color=#ff0000> Please enter 10,000 or lesser amount to continue.</font></b><br>";
  }

  public void checkTrxTime() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("TRXTIMEURL", ApiUrl.URL_VALIDATE_TRX_TIME);
      Log.i("TRXTIMEREQ", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRX_TIME, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          Log.i("TRXTIMERES", response.toString());

          try {
            String message = response.getString("message");
            String code = response.getString("code");
            String msg = response.getString("message");
            if (code != null && code.equals("S00")) {
              loadDlg.dismiss();
              String success = response.getString("success");
              if (success.equalsIgnoreCase("true")) {
                verifyTransaction(amount);
              } else {
                CustomToast.showMessage(FlightPaymentActivity.this, message);
              }
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog(message);
            } else {
              loadDlg.dismiss();
              CustomToast.showMessage(FlightPaymentActivity.this, message);
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception));

          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

    }

  }

  private void verifyTransaction(final String amount) {
    if (isVBank) {
      Intent loadMoneyIntent = new Intent(FlightPaymentActivity.this, FlightVnetWebViewActivity.class);
      loadMoneyIntent.putExtra("amountToLoad", amount);
      loadMoneyIntent.putExtra("isVBank", isVBank);
      loadMoneyIntent.putExtra("jsonBokking", jsonBokking);
      loadMoneyIntent.putExtra("roundTrip", roundTrip);
      loadMoneyIntent.putExtra("conting", contingFlight);
      startActivity(loadMoneyIntent);
    } else {
      //Initiate and call EBS KIT
      loadDlg.show();
      initialLoadMoneyEBS(amount);
    }
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(FlightPaymentActivity.this, R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(FlightPaymentActivity.this).sendBroadcast(intent);
  }

  private void showInvalidTranDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(FlightPaymentActivity.this, R.string.dialog_title2, inValidMessage);
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void initialLoadMoneyEBS(final String amount) {
    SharedPreferences.Editor editor = loadMoneyPref.edit();
    editor.clear();
    editor.putString("type", "flight");
    editor.putString("jsonBooking", jsonBokking);
    editor.putBoolean("roundTrip", roundTrip);
    editor.putBoolean("conting", contingFlight);
    editor.apply();
    StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_INITIATE_LOAD_MONEY, new Response.Listener<String>() {
      @Override
      public void onResponse(String response) {
        Log.d("values", response);
        try {
          loadDlg.dismiss();
          JSONObject resObj = new JSONObject(response);
          boolean successMsg = resObj.getBoolean("success");

          String msg = resObj.getString("message");
          if (successMsg) {
            String referenceNo = resObj.getString("referenceNo");
            String currency = resObj.getString("currency");
            String description = resObj.getString("description");
            String name = resObj.getString("name");
            String email = resObj.getString("email");
            String address = resObj.getString("address");
            String cityName = resObj.getString("city");
            String stateName = resObj.getString("state");
            String countryName = resObj.getString("country");
            String postalCode = resObj.getString("postalCode");
            String shipName = resObj.getString("shipName");
            String shipAddress = resObj.getString("shipAddress");
            String shipCity = resObj.getString("shipCity");
            String shipState = resObj.getString("shipState");
            String shipCountry = resObj.getString("shipCountry");
            String shipPostalCode = resObj.getString("shipPostalCode");
            String shipPhone = resObj.getString("shipPhone");

            int ACC_ID = 20696;
            String SECRET_KEY = "6496e4db9ebf824ffe2269afee259447";
            String HOST_NAME = getResources().getString(R.string.hostname);

            PaymentRequest.getInstance().setTransactionAmount(amount);
            PaymentRequest.getInstance().setAccountId(ACC_ID);
            PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

            PaymentRequest.getInstance().setReferenceNo(referenceNo);
            PaymentRequest.getInstance().setBillingEmail(email);
            PaymentRequest.getInstance().setFailureid("1");
            PaymentRequest.getInstance().setCurrency(currency);
            PaymentRequest.getInstance().setTransactionDescription(description);
            PaymentRequest.getInstance().setBillingName(name);
            PaymentRequest.getInstance().setBillingAddress(address);
            PaymentRequest.getInstance().setBillingCity(cityName);
            PaymentRequest.getInstance().setBillingPostalCode(postalCode);
            PaymentRequest.getInstance().setBillingState(stateName);
            PaymentRequest.getInstance().setBillingCountry(countryName);
            PaymentRequest.getInstance().setBillingPhone(session.getUserMobileNo());
            PaymentRequest.getInstance().setShippingName(shipName);
            PaymentRequest.getInstance().setShippingAddress(shipAddress);
            PaymentRequest.getInstance().setShippingCity(shipCity);
            PaymentRequest.getInstance().setShippingPostalCode(shipPostalCode);
            PaymentRequest.getInstance().setShippingState(shipState);
            PaymentRequest.getInstance().setShippingCountry(shipCountry);
            PaymentRequest.getInstance().setShippingEmail(email);
            PaymentRequest.getInstance().setShippingPhone(shipPhone);
            PaymentRequest.getInstance().setLogEnabled("1");

            PaymentRequest.getInstance().setHidePaymentOption(true);
            PaymentRequest.getInstance().setHideCashCardOption(true);
            PaymentRequest.getInstance().setHideCreditCardOption(false);
            PaymentRequest.getInstance().setHideDebitCardOption(false);
            PaymentRequest.getInstance().setHideNetBankingOption(false);
            PaymentRequest.getInstance().setHideStoredCardOption(true);

            ArrayList<HashMap<String, String>> custom_post_parameters = new ArrayList<>();

            HashMap<String, String> hashpostvalues = new HashMap<>();
            hashpostvalues.put("account_details", "saving");
            hashpostvalues.put("merchant_type", "vpayqwik");
            custom_post_parameters.add(hashpostvalues);

            PaymentRequest.getInstance().setCustomPostValues(custom_post_parameters);
            loadDlg.dismiss();
            EBSPayment.getInstance().init(FlightPaymentActivity.this, ACC_ID, SECRET_KEY, Mode.ENV_LIVE, Encryption.ALGORITHM_MD5, HOST_NAME);

          } else {
            loadDlg.dismiss();
            CustomToast.showMessage(FlightPaymentActivity.this, msg);
          }
        } catch (JSONException e) {
          e.printStackTrace();
          loadDlg.dismiss();
          CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception2));

        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        loadDlg.dismiss();
        CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception));
      }
    }) {
      @Override
      protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("sessionId", session.getUserSessionId());
        try {
          if (roundTrip) {
            String flightOneAmount = new JSONObject(jsonBokking).getJSONArray("bookSegments").getJSONObject(0).getJSONObject("fares").getString("basicFare");
            String flightTwoAmount = new JSONObject(jsonBokking).getJSONArray("bookSegments").getJSONObject(1).getJSONObject("fares").getString("basicFare");
            params.put("baseFare", String.valueOf(Double.parseDouble(flightOneAmount) + Double.parseDouble(flightTwoAmount)));
          } else {
            params.put("baseFare", String.valueOf(new JSONObject(jsonBokking).getJSONArray("bookSegments").getJSONObject(0).getJSONObject("fares").getString("basicFare")));
          }

        } catch (JSONException e) {
          e.printStackTrace();
        }
        params.put("amount", amount);
        params.put("name", session.getUserFirstName());
        params.put("email", session.getUserEmail());
        params.put("phone", session.getUserMobileNo());

        Log.d("LoadMoneyParam", "LoadParams: " + params);
        return params;
      }

      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("hash", "123");
        return map;
      }
    };

    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(FlightPaymentActivity.this).sendBroadcast(intent);
  }


  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(FlightPaymentActivity.this, "Booked Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
        Intent i = new Intent(FlightPaymentActivity.this, HomeMainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(i);
      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    return "<b><font color=#000000> </font></b>" + "<font color=#000000>" + "Flight Ticket Booked Successfully" + "</font><br>" +
      "<b><font color=#000000> Please check your email for details </font></b>" + "<font>";
  }

  public static void longLog(String str) {
    if (str.length() > 4000) {
      Log.d("bokking", str.substring(0, 4000));
      longLog(str.substring(4000));
    } else
      Log.d("bokking", str);
  }
}
