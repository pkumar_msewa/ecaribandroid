package in.msewa.fragment.fragmenttravel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;

import in.msewa.custom.CustomToast;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 9/26/2016.
 */
public class HotelTravelFragment extends Fragment {

    private MaterialEditText etHotelLocation, etHotelCheckIn, etHotelCheckOut, etHotelRooms, etHotelGuestNo;
    private View focusView = null;
    private boolean cancel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_travel_hotel, container, false);
        etHotelLocation = (MaterialEditText) rootView.findViewById(R.id.etHotelLocation);
        etHotelCheckIn = (MaterialEditText) rootView.findViewById(R.id.etHotelCheckIn);
        etHotelCheckOut = (MaterialEditText) rootView.findViewById(R.id.etHotelCheckOut);
        etHotelRooms = (MaterialEditText) rootView.findViewById(R.id.etHotelRooms);
        etHotelGuestNo = (MaterialEditText) rootView.findViewById(R.id.etHotelGuestNo);

        Button btnSearchHotel = (Button) rootView.findViewById(R.id.btnSearchHotel);
        btnSearchHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidity();
            }
        });


        return rootView;
    }

    private void checkValidity() {
        etHotelLocation.setError(null);
        etHotelCheckIn.setError(null);
        etHotelCheckOut.setError(null);
        etHotelRooms.setError(null);
        etHotelGuestNo.setError(null);
        cancel = false;

        checkLocation(etHotelLocation.getText().toString());
        checkCheckIn(etHotelCheckIn.getText().toString());
        checkCheckOut(etHotelCheckOut.getText().toString());
        checkRooms(etHotelRooms.getText().toString());
        checkGuestNo(etHotelGuestNo.getText().toString());

        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteSearch();
        }
    }

    private void checkLocation(String hotelLocation) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelLocation);
        if (!gasCheckLog.isValid) {
            etHotelLocation.setError(getString(gasCheckLog.msg));
            focusView = etHotelLocation;
            cancel = true;
        }
    }

    private void checkCheckIn(String hotelCheckIn) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelCheckIn);
        if (!gasCheckLog.isValid) {
            etHotelCheckIn.setError(getString(gasCheckLog.msg));
            focusView = etHotelCheckIn;
            cancel = true;
        }
    }

    private void checkCheckOut(String hotelCheckOut) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelCheckOut);
        if (!gasCheckLog.isValid) {
            etHotelCheckOut.setError(getString(gasCheckLog.msg));
            focusView = etHotelCheckOut;
            cancel = true;
        }
    }

    private void checkRooms(String hotelRooms) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelRooms);
        if (!gasCheckLog.isValid) {
            etHotelRooms.setError(getString(gasCheckLog.msg));
            focusView = etHotelRooms;
            cancel = true;
        }
    }

    private void checkGuestNo(String hotelGuest) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelGuest);
        if (!gasCheckLog.isValid) {
            etHotelGuestNo.setError(getString(gasCheckLog.msg));
            focusView = etHotelGuestNo;
            cancel = true;
        }
    }


    private void promoteSearch(){
        CustomToast.showMessage(getActivity(),"Ready to search");
    }
}
