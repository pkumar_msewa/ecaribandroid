package in.msewa.fragment.fragmenttravel;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import in.msewa.adapter.FlightBookListAdapter;
import in.msewa.adapter.HomeSliderAdapter;
import in.msewa.custom.CirclePageIndicator;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.CustomTravellerPickerDialog;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.ResultIPC;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.BusBookedTicketModel;
import in.msewa.model.BusPassengerModel;
import in.msewa.model.DomesticFlightModel;
import in.msewa.model.FlightSaveModel;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.util.TravellerSelectedListner;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.flightinneracitivty.DomesticFlightSearchActivity;
import in.msewa.ecarib.activity.flightinneracitivty.FlightListOnWayActivity;
import in.msewa.ecarib.activity.flightinneracitivty.FlightListTwoWayActivity;

/**
 * Created by Ksf on 9/26/2016.
 */
public class FlightTravelFragment extends Fragment implements TravellerSelectedListner, ViewPager.OnPageChangeListener {
  private View rootView;
  private MaterialEditText etFlightFrom, etFlightTo, etFlightDepartDate, etFlightReturnDate, etFlightTraveller, etFlightClass;
  private Button btnSearchFlight;
  private ArrayList<DomesticFlightModel> flightCityModelList;
  private RadioButton rbFlightRoundTrip, rbFlightOneWay;
  private RadioGroup rgFlightTripType;
  private UserModel session = UserModel.getInstance();

  private RadioButton rbFlightDomestic, rbFlightInternational;
  private RadioGroup rgFlightType;
  private RecyclerView rvPreviousTickets;

//    private Switch switchFLightType;

  //imageslider

  //image slider
  private static int currentPage = 0;
  private static int NUM_PAGES = 0;
  private int[] IMAGES;
  public ViewPager mPager;
  public CirclePageIndicator indicator;

  private View focusView = null;
  private boolean cancel;

  private int adultNo, childNo, infantNo;
  private String desCode, sourceCode;
  private String flightClass;
  private int flightType;
  private LoadingDialog loadingDialog;
  private JSONObject jsonRequest;
  private String tag_json_obj = "json_flight_city_search";
  private int sys;
  private LinearLayout llpbPreviousTickets;
  private ArrayList<BusPassengerModel> busPassengerArrayList;
  private TextView tvHeaderBookedTickets;
  private Thread thread;
  private ProgressBar pbBar;
  private LinearLayout llFlightSearch;


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_travel_flight, container, false);
    loadingDialog = new LoadingDialog(getActivity());
    btnSearchFlight = (Button) rootView.findViewById(R.id.btnSearchFlight);
    etFlightFrom = (MaterialEditText) rootView.findViewById(R.id.etFlightFrom);
    etFlightTo = (MaterialEditText) rootView.findViewById(R.id.etFlightTo);
    etFlightDepartDate = (MaterialEditText) rootView.findViewById(R.id.etFlightDepartDate);
    etFlightReturnDate = (MaterialEditText) rootView.findViewById(R.id.etFlightReturnDate);
    etFlightTraveller = (MaterialEditText) rootView.findViewById(R.id.etFlightTraveller);
    mPager = (ViewPager) rootView.findViewById(R.id.pagerHome);
    indicator = (CirclePageIndicator) rootView.findViewById(R.id.productHeaderImageSlider);
    etFlightClass = (MaterialEditText) rootView.findViewById(R.id.etFlightClass);
    rgFlightTripType = (RadioGroup) rootView.findViewById(R.id.rgFlightTripType);
    rbFlightRoundTrip = (RadioButton) rootView.findViewById(R.id.rbFlightRoundTrip);
    rbFlightOneWay = (RadioButton) rootView.findViewById(R.id.rbFlightOneWay);
    tvHeaderBookedTickets = (TextView) rootView.findViewById(R.id.tvHeaderBookedTickets);
    rvPreviousTickets = (RecyclerView) rootView.findViewById(R.id.rvPreviousTickets);
    llpbPreviousTickets = (LinearLayout) rootView.findViewById(R.id.llpbPreviousTickets);
    llFlightSearch = (LinearLayout) rootView.findViewById(R.id.llFlightSearch);
    pbBar = (ProgressBar) rootView.findViewById(R.id.pbBar);
    flightCityModelList = new ArrayList<>();
    rgFlightType = (RadioGroup) rootView.findViewById(R.id.rgFlightType);
    rbFlightDomestic = (RadioButton) rootView.findViewById(R.id.rbFlightDomestic);
    rbFlightInternational = (RadioButton) rootView.findViewById(R.id.rbFlightInternational);
    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    String formattedDate = df.format(c.getTime());
    etFlightDepartDate.setText(formattedDate);

    rvPreviousTickets.setNestedScrollingEnabled(false);
    c.add(Calendar.DATE, 1);
    String convertedDate = df.format(c.getTime());
    etFlightReturnDate.setText(convertedDate);
    getImageSlider();
    List<FlightSaveModel> flightSaveModels = Select.from(FlightSaveModel.class).list();

    if (flightSaveModels != null && flightSaveModels.size() != 0) {
      llFlightSearch.setVisibility(View.VISIBLE);
      pbBar.setVisibility(View.GONE);

      Log.i("flightSaveModels", String.valueOf(flightSaveModels.size()));

      try {
        JSONArray citydata = new JSONArray(flightSaveModels.get(0).getCityname());
        for (int i = 0; i < citydata.length(); i++) {
          JSONObject cityList = citydata.getJSONObject(i);
          String airportCode = cityList.getString("cityCode");
          String cityName = cityList.getString("cityName");
          String airportDesc = cityList.getString("airportName");

          DomesticFlightModel busModel = new DomesticFlightModel(airportCode, cityName, "", airportDesc);
          flightCityModelList.add(busModel);
        }
        if (flightCityModelList.size() != 0) {
          etFlightFrom.setText(flightCityModelList.get(0).getAirportCode());
          sourceCode = flightCityModelList.get(0).getAirportCode();
          etFlightTo.setText(flightCityModelList.get(1).getAirportCode());
          desCode = flightCityModelList.get(1).getAirportCode();
//          getBookedTicketsList();
          ResultIPC resultIPC = ResultIPC.get();
          sys = resultIPC.setaddOnFlight(flightCityModelList);
        }
      } catch (JSONException e) {

      }
//      getBookedTicketsList();
    } else {
      getCity();
    }

    getBookedTicketsList();

    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("flight-search-complete"));
    etFlightTraveller.setText("1 Adult");
    adultNo = 1;
    rgFlightTripType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        View radioButton = rgFlightTripType.findViewById(checkedId);
        int index = rgFlightTripType.indexOfChild(radioButton);

        switch (index) {
          case 0:
            etFlightReturnDate.setEnabled(false);
            break;
          case 1:
            etFlightReturnDate.setEnabled(true);
            break;
        }
      }
    });

    etFlightDepartDate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String formattedDay, formattedMonth;
            if (dayOfMonth < 10) {
              formattedDay = "0" + dayOfMonth;
            } else {
              formattedDay = dayOfMonth + "";
            }

            if ((monthOfYear + 1) < 10) {
              formattedMonth = "0" + String.valueOf(monthOfYear + 1);
            } else {
              formattedMonth = String.valueOf(monthOfYear + 1) + "";
            }
            etFlightDepartDate.setText(
              String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));

          }
        }, mYear, mMonth, mDay);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.DAY_OF_MONTH, mDay);
        maxDate.set(Calendar.MONTH, mMonth + 6);
        maxDate.set(Calendar.YEAR, mYear);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", datePickerDialog);
        datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

        {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
        datePickerDialog.show();


      }
    });


    etFlightReturnDate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {

        Calendar c = Calendar.getInstance();
        int mYear, mMonth, mDay;
        if (!etFlightDepartDate.getText().toString().isEmpty()) {
          String[] date = etFlightDepartDate.getText().toString().split("-");
          mYear = Integer.parseInt(date[0]);
          mMonth = Integer.parseInt(date[1]) - 1;
          mDay = Integer.parseInt(date[2]);
          c.set(mYear, mMonth, mDay);

        } else {
          mYear = c.get(Calendar.YEAR);
          mMonth = c.get(Calendar.MONTH);
          mDay = c.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String formattedDay, formattedMonth;
            if (dayOfMonth < 10) {
              formattedDay = "0" + dayOfMonth;
            } else {
              formattedDay = dayOfMonth + "";
            }

            if ((monthOfYear + 1) < 10) {
              formattedMonth = "0" + String.valueOf(monthOfYear + 1);
            } else {
              formattedMonth = String.valueOf(monthOfYear + 1) + "";
            }
            etFlightReturnDate.setText(
              String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));
          }
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.DAY_OF_MONTH, maxDate.get(Calendar.DATE));
        maxDate.set(Calendar.MONTH, maxDate.get(Calendar.MONTH) + 6);
        maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR));
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", datePickerDialog);
        datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

        {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
        datePickerDialog.show();

      }
    });
    etFlightClass.setText("Economy");
    flightClass = "Economy";

    etFlightClass.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String[] items = {"Economy", "Business", "Premium Economy"};
        android.support.v7.app.AlertDialog.Builder callDialog = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        callDialog.setTitle("Select your flight class");
        callDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
          }
        });
        callDialog.setItems(items, new DialogInterface.OnClickListener() {

          public void onClick(DialogInterface dialog, int position) {
            if (position == 0) {
              flightClass = "Economy";
            } else if (position == 1) {
              flightClass = "Business";
            } else {
              flightClass = "Business";
            }
            etFlightClass.setText(items[position].toString());
          }

        });

        callDialog.show();
      }
    });

    etFlightTraveller.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showTravellerDialog();
      }
    });

    btnSearchFlight.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (rbFlightInternational.isChecked()) {
          flightType = 2;
        } else {
          flightType = 1;
        }
        attemptSearch();
      }
    });

    etFlightFrom.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (flightCityModelList.size() != 0) {
          Intent searchCityIntent = new Intent(getActivity(), DomesticFlightSearchActivity.class);
          searchCityIntent.putExtra("searchType", "From");
          searchCityIntent.putExtra("city", sys);
          startActivity(searchCityIntent);
        }

      }
    });
    etFlightTo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (flightCityModelList.size() != 0) {
          Intent searchCityIntent = new Intent(getActivity(), DomesticFlightSearchActivity.class);
          searchCityIntent.putExtra("searchType", "To");
          searchCityIntent.putParcelableArrayListExtra("city", flightCityModelList);
          searchCityIntent.putExtra("city", sys);
          startActivity(searchCityIntent);
        }

      }
    });

    return rootView;
  }

  private void getImageSlider() {
    IMAGES = new int[]{R.drawable.flight_banner, R.drawable.flight_banner_two};
    mPager.setAdapter(new HomeSliderAdapter(getActivity(), IMAGES));
    indicator.setViewPager(mPager);
    final float density = getActivity().getResources().getDisplayMetrics().density;
    indicator.setRadius(5 * density);

    NUM_PAGES = IMAGES.length;
    final Handler handler = new Handler();
    final Runnable Update = new Runnable() {
      public void run() {
        if (currentPage == NUM_PAGES) {
          currentPage = 0;
        }
        mPager.setCurrentItem(currentPage++, true);
      }
    };
    Timer swipeTimer = new Timer();
    swipeTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        handler.post(Update);
      }
    }, 3000, 3000);

    indicator.setOnPageChangeListener(this);
  }

  public void showTravellerDialog() {
    CustomTravellerPickerDialog builder = new CustomTravellerPickerDialog(getActivity(), this);
    builder.setCancelable(true);
  }

  @Override
  public void onSelected(int adultCount, int childCount, int infantCount) {
    adultNo = adultCount;
    childNo = childCount;
    infantNo = infantCount;
    //ALL THREE
    if (adultCount != 0 && childCount != 0 && infantCount != 0) {
      etFlightTraveller.setText(adultCount + " Adults, " + childCount + " Children, " + infantCount + " Infant");
    }
    //ADULT AND CHILD
    else if (adultCount != 0 && childCount != 0 && infantCount == 0) {
      etFlightTraveller.setText(adultCount + " Adults, " + childCount + " Children");
    }
    //ADULT AND INFANT
    else if (adultCount != 0 && infantCount != 0 && childCount == 0) {
      etFlightTraveller.setText(adultCount + " Adults, " + infantCount + " Infant");
    }
    //CHILD AND INFANT
    else if (adultCount == 0 && childCount != 0 && infantCount != 0) {
      etFlightTraveller.setText(childCount + " Children, " + infantCount + " Infant");
    }

    //ONLY ADULT
    else if (adultCount != 0 && childCount == 0 && infantCount == 0) {
      if (adultCount > 1) {
        etFlightTraveller.setText(adultCount + " Adults, ");
      } else {
        etFlightTraveller.setText(adultCount + " Adult, ");
      }
    }
    //ONLY INFANT
    else if (adultCount == 0 && childCount == 0 && infantCount != 0) {
      etFlightTraveller.setText(adultCount + " Adults, " + infantCount + " Infant");
    }
    //ONLY CHILD
    else if (adultCount == 0 && infantCount == 0 && childCount != 0) {
      if (childCount > 1) {
        etFlightTraveller.setText(childCount + " Children");
      } else {
        etFlightTraveller.setText(childCount + " Child");
      }
    } else {
      etFlightTraveller.setText("");
    }

  }

  private void attemptSearch() {
    etFlightFrom.setError(null);
    etFlightTo.setError(null);
    etFlightDepartDate.setError(null);
    etFlightReturnDate.setError(null);
    etFlightTraveller.setError(null);
    etFlightClass.setError(null);

    cancel = false;


    checkBusFrom(etFlightFrom.getText().toString());
    checkBusTo(etFlightTo.getText().toString());
    checkDepDate(etFlightDepartDate.getText().toString());
    checkTraveller(etFlightTraveller.getText().toString());
    checkFlightClass(etFlightClass.getText().toString());

    if (rbFlightRoundTrip.isChecked()) {
      checkReturnDate(etFlightReturnDate.getText().toString());
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      if (rbFlightRoundTrip.isChecked()) {
        Intent flightSearchIntent = new Intent(getActivity(), FlightListTwoWayActivity.class);
        flightSearchIntent.putExtra("Adult", adultNo);
        flightSearchIntent.putExtra("Child", childNo);
        flightSearchIntent.putExtra("Infant", infantNo);
        flightSearchIntent.putExtra("Class", flightClass);
        flightSearchIntent.putExtra("destination", desCode);
        flightSearchIntent.putExtra("source", sourceCode);
        flightSearchIntent.putExtra("citylist", sys);
        flightSearchIntent.putExtra("date", etFlightDepartDate.getText().toString());
        flightSearchIntent.putExtra("flightType", flightType);
        flightSearchIntent.putExtra("dateOfReturn", etFlightReturnDate.getText().toString());
        startActivity(flightSearchIntent);
      } else {
        Intent flightSearchIntent = new Intent(getActivity(), FlightListOnWayActivity.class);
        flightSearchIntent.putExtra("Adult", adultNo);
        flightSearchIntent.putExtra("Child", childNo);
        flightSearchIntent.putExtra("Infant", infantNo);
        flightSearchIntent.putExtra("Class", flightClass);
        flightSearchIntent.putExtra("destination", desCode);
        flightSearchIntent.putExtra("citylist", sys);
        flightSearchIntent.putExtra("source", sourceCode);
        flightSearchIntent.putExtra("date", etFlightDepartDate.getText().toString());
        flightSearchIntent.putExtra("flightType", flightType);

        startActivity(flightSearchIntent);
      }
    }
  }


  private void checkBusFrom(String busFrom) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busFrom);
    if (!gasCheckLog.isValid) {
      etFlightFrom.setError(getString(gasCheckLog.msg));
      focusView = etFlightFrom;
      cancel = true;
    }
  }

  private void checkBusTo(String busTo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busTo);
    if (!gasCheckLog.isValid) {
      etFlightTo.setError(getString(gasCheckLog.msg));
      focusView = etFlightTo;
      cancel = true;
    }
  }

  private void checkFlightClass(String flightClass) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(flightClass);
    if (!gasCheckLog.isValid) {
      etFlightClass.setError(getString(gasCheckLog.msg));
      focusView = etFlightClass;
      cancel = true;
    }
  }

  private void checkDepDate(String busDepDate) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busDepDate);
    if (!gasCheckLog.isValid) {
      etFlightDepartDate.setError(getString(gasCheckLog.msg));
      focusView = etFlightDepartDate;
      cancel = true;
    }
  }

  private void checkReturnDate(String busReDate) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busReDate);
    if (!gasCheckLog.isValid) {
      etFlightReturnDate.setError(getString(gasCheckLog.msg));
      focusView = etFlightReturnDate;
      cancel = true;
    }
  }

  private void checkTraveller(String travellerNo) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(travellerNo);
    if (!gasCheckLog.isValid) {
      etFlightTraveller.setError(getString(gasCheckLog.msg));
      focusView = etFlightTraveller;
      cancel = true;
    }
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("searchType");
      if (action.equals("To")) {
        DomesticFlightModel domesticFlightModel = intent.getParcelableExtra("selectedCityModel");
        desCode = domesticFlightModel.getAirportCode();
        if (etFlightTo.getText().toString().equals("")) {
          etFlightTo.setText(domesticFlightModel.getAirportCode());
        } else if (etFlightTo.getText().toString().equalsIgnoreCase(etFlightFrom.getText().toString())) {
          CustomToast.showMessage(getActivity(), "Source and destination ");
          etFlightTo.setText("");
        } else {
          etFlightTo.setText(domesticFlightModel.getAirportCode());
        }
      } else {
        DomesticFlightModel domesticFlightModel = intent.getParcelableExtra("selectedCityModel");
        sourceCode = domesticFlightModel.getAirportCode();
        if (etFlightFrom.getText().toString().equals("")) {
          etFlightFrom.setText(domesticFlightModel.getAirportCode());
        } else if (etFlightFrom.getText().toString().equalsIgnoreCase(etFlightTo.getText().toString())) {
          CustomToast.showMessage(getActivity(), "Source and destination ");
          etFlightFrom.setText("");
        } else {
          etFlightFrom.setText(domesticFlightModel.getAirportCode());
        }

      }
    }
  };

  @Override
  public void onDestroyView() {
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    super.onDestroyView();
  }


  private void getCity() {
    llFlightSearch.setVisibility(View.GONE);
    pbBar.setVisibility(View.VISIBLE);

//    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    AndroidNetworking.post(ApiUrl.URL_GET_LIST_FLIGHT_SOURCE_DETAILS)
      .addJSONObjectBody(jsonRequest) // posting json
      .setTag("test")
      .setPriority(Priority.IMMEDIATE)
      .build()
      .getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          llFlightSearch.setVisibility(View.VISIBLE);
          pbBar.setVisibility(View.GONE);

          try {
            String code = response.getString("code");
//            loadingDialog.dismiss();
            llFlightSearch.setVisibility(View.VISIBLE);
            pbBar.setVisibility(View.GONE);

            if (code != null && code.equals("S00")) {

              JSONArray citydata = response.getJSONArray("details");
              FlightSaveModel flightSaveModel = new FlightSaveModel(citydata.toString());
              flightSaveModel.save();
              for (int i = 0; i < citydata.length(); i++) {
                JSONObject c = citydata.getJSONObject(i);
                String airportCode = c.getString("cityCode");
                String cityName = c.getString("cityName");
                String airportDesc = c.getString("airportName");

                DomesticFlightModel busModel = new DomesticFlightModel(airportCode, cityName, "", airportDesc);
                flightCityModelList.add(busModel);
              }
              if (flightCityModelList.size() != 0) {
                etFlightFrom.setText(flightCityModelList.get(0).getAirportCode());
                sourceCode = flightCityModelList.get(0).getAirportCode();
                etFlightTo.setText(flightCityModelList.get(1).getAirportCode());
                desCode = flightCityModelList.get(1).getAirportCode();
//                  getBookedTicketsList();
                ResultIPC resultIPC = ResultIPC.get();
                sys = resultIPC.setaddOnFlight(flightCityModelList);
              }
              loadingDialog.dismiss();

            } else if (code != null && code.equals("F03")) {
              llFlightSearch.setVisibility(View.VISIBLE);
              pbBar.setVisibility(View.GONE);
              showInvalidSessionDialog(response.getString("message"));
            } else {
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(getActivity(), message);
                llFlightSearch.setVisibility(View.VISIBLE);
                pbBar.setVisibility(View.GONE);

              }
            }

          } catch (JSONException e) {
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
            llFlightSearch.setVisibility(View.VISIBLE);
            pbBar.setVisibility(View.GONE);

          }
        }

        @Override
        public void onError(ANError error) {
          try {
            llFlightSearch.setVisibility(View.VISIBLE);
            pbBar.setVisibility(View.GONE);
            llpbPreviousTickets.setVisibility(View.GONE);
          } catch (NullPointerException e) {

          }
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

        }
      });

  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {
    currentPage = position;

  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }


  public void getBookedTicketsList() {
    llpbPreviousTickets.setVisibility(View.VISIBLE);
    rvPreviousTickets.setVisibility(View.GONE);

    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
    }

    AndroidNetworking.post(ApiUrl.URL_FLIGHT_GET_BOOKED_TICKETS)
      .addJSONObjectBody(jsonRequest) // posting json
      .setTag("test")
      .setPriority(Priority.MEDIUM)
      .build().getAsJSONObject(new JSONObjectRequestListener() {
      @Override
      public void onResponse(JSONObject response) throws JSONException {
        try {
          Log.i("BUSBOOKEDRES", response.toString());
          String code = response.getString("code");
          final String message = response.getString("message");

          if (code != null && code.equals("S00")) {
            final ArrayList<BusBookedTicketModel> busBookedTicketArrayList = new ArrayList<>();
            String details = response.getString("details");
            JSONArray busBookedTicketarray = new JSONArray(details);

            for (int i = 0; i < busBookedTicketarray.length(); i++) {
              JSONObject c = busBookedTicketarray.getJSONObject(i);
//                                String busTicketString = c.getString("flightTicket");
//                                JSONObject busTicket = new JSONObject(busTicketString);
              String userMobile = session.getUserMobileNo();
              String userEmail = session.getUserEmail();
              String ticketPnr = c.getString("bookingRefId");
              String operatorPnr = "";
              String emtTxnId = c.getString("mdexTxnRefNo");
              String busId = "";
              double totalFare = c.getDouble("totalFare");
              String journeyDate = c.getString("journeyDate");
              String source = c.getString("source");
              String destination = c.getString("destination");
              String boardingId = "";
              String boardingAddress = "";
              String busOperator = "";
              JSONArray oneway = c.getJSONArray("oneway");
              JSONArray roundway = c.getJSONArray("roundway");
              String arrTime = c.getString("arrTime");
              String deptTime = c.getString("deptTime");

              String travellerDetailsSTring = c.getString("travellerDetails");
              JSONArray travellerDetails = new JSONArray(travellerDetailsSTring);
              if (travellerDetails.length() != 0) {
                busPassengerArrayList = new ArrayList<>();
                for (int j = 0; j < travellerDetails.length(); j++) {
                  JSONObject p = travellerDetails.getJSONObject(j);
                  String fName = p.getString("fName");
                  String lName = p.getString("lName");
                  String age = p.getString("age");
                  String gender = p.getString("gender");
                  String seatNo = p.getString("ticketNo");
                  String seatType = p.getString("travellerType");
                  String fare = p.getString("fare");
                  if (seatNo.equalsIgnoreCase("null")) {
                    seatNo = "failed";
                  }

                  BusPassengerModel busPassengerModel = new BusPassengerModel(fName, lName, age, gender, seatNo, seatType, fare);
                  busPassengerArrayList.add(busPassengerModel);
                }
              }
              if (ticketPnr.equalsIgnoreCase("null")) {
                ticketPnr = "Failed";
              }
              BusBookedTicketModel busBookedTicketModel = new BusBookedTicketModel(userMobile, userEmail, ticketPnr, operatorPnr, emtTxnId, busId, totalFare, journeyDate, source, destination, boardingId, boardingAddress, busOperator, arrTime, deptTime, busPassengerArrayList, oneway.toString(), roundway.toString());
              busBookedTicketArrayList.add(busBookedTicketModel);

            }
            if (busBookedTicketArrayList != null && busBookedTicketArrayList.size() != 0) {
//                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
              rvPreviousTickets.addItemDecoration(new FlightTravelFragment.SpacesItemDecoration(4));
              GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
              rvPreviousTickets.setLayoutManager(manager);
              rvPreviousTickets.setHasFixedSize(true);

              FlightBookListAdapter itemAdp = new FlightBookListAdapter(getActivity(), busBookedTicketArrayList, sys);
              rvPreviousTickets.setAdapter(itemAdp);
              tvHeaderBookedTickets.setVisibility(View.VISIBLE);
              llpbPreviousTickets.setVisibility(View.GONE);
              rvPreviousTickets.setVisibility(View.VISIBLE);
            } else {

              llpbPreviousTickets.setVisibility(View.GONE);
            }
          } else if (code != null && code.equals("F03")) {
            llpbPreviousTickets.setVisibility(View.GONE);
            showInvalidSessionDialog(message);
          } else {
            llpbPreviousTickets.setVisibility(View.GONE);
            CustomToast.showMessage(getActivity(), message);

          }

        } catch (JSONException e) {
          e.printStackTrace();
          llpbPreviousTickets.setVisibility(View.GONE);
        }
      }

      @Override
      public void onError(ANError anError) {
        llpbPreviousTickets.setVisibility(View.GONE);
        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
      }
    });
  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }

  public void refresh() {

    onResume();
  }

  @Override
  public void onResume() {
    super.onResume();
    FlightTravelFragment.MyReceiver r = new FlightTravelFragment.MyReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r, new IntentFilter("TAG_REFRESH"));
  }

  public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      FlightTravelFragment.this.refresh();
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    AndroidNetworking.cancelAll();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }
}
