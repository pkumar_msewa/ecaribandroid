package in.msewa.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.msewa.adapter.ImagicaAdapter;
import in.msewa.model.ImagicaPackageModel;
import in.msewa.ecarib.R;


/**
 * Created by acer on 06-07-2017.
 */

public class packageFragment extends Fragment {

    private View rootView;
    RecyclerView recyclerView;
    Context context;
    ArrayList<ImagicaPackageModel> imagicaPackageModels;
    private ImagicaAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_packages_list, container, false);
        imagicaPackageModels = getArguments().getParcelableArrayList("package");
        recyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
//        getAllValues();
        adapter = new ImagicaAdapter(getActivity(), imagicaPackageModels);
        recyclerView.setAdapter(adapter);


        return rootView;
    }

//    public void getAllValues() {
//        View parentView = null;
//        int temp = 0;
//        int getTotal = 0;
//        int totQuan = 0;
//        int quan = 0;
//        for (int i = 0; i < recyclerView.getAdapter().getItemCount(); i++) {
//            parentView = recyclerView.getChildAt(i);
//
//            String getString = ((TextView) parentView
//                    .findViewById(R.id.tvtotalcost)).getText().toString();
//            Log.i("value", getString);
//
//        }
//
//    }




}

