package in.msewa.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.msewa.adapter.BusFilterAdapter;
import in.msewa.adapter.ElectryFilterAdapter;
import in.msewa.model.BillPaymentOperatorsModel;
import in.msewa.model.BusCityModel;
import in.msewa.util.CitySelectedListener;
import in.msewa.util.ElectrySelectedListener;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 4/12/2016.
 */
public class CustomAlertDialogElectrictySearch extends Dialog {

  public CustomAlertDialogElectrictySearch(final Context context, List<BillPaymentOperatorsModel> flightCityModelList, String type, final ElectrySelectedListener citySelectedListener) {
    super(context);

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    final View viewDialog = inflater.inflate(R.layout.dialog_custom_search, null, false);
    android.widget.SearchView searchView = (android.widget.SearchView) viewDialog.findViewById(R.id.cadMessage);
    ListView lvSearch = (ListView) viewDialog.findViewById(R.id.lvSearchBus);
    final TextView tv_popularCity = (TextView) viewDialog.findViewById(R.id.tv_popularCity);
    ImageView back = (ImageView) viewDialog.findViewById(R.id.back);
//        flightCityModelList = new ArrayList<>();


    searchView.setQueryHint("Enter Operators");

    final ElectryFilterAdapter domesticCityListAdapter = new ElectryFilterAdapter(context, flightCityModelList);
    lvSearch.setAdapter(domesticCityListAdapter);
    searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        return false;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        domesticCityListAdapter.getFilter().filter(newText);

        return false;
      }
    });
    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dismiss();
      }
    });
    lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        dismiss();
        BillPaymentOperatorsModel domesticFlightModel = (BillPaymentOperatorsModel) adapterView.getItemAtPosition(i);
        citySelectedListener.Selected(domesticFlightModel,i);

      }
    });
    this.setCancelable(true);

    this.setContentView(viewDialog);

    this.getWindow().setSoftInputMode(
      WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

  }

}
