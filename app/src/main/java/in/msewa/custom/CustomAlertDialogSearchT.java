package in.msewa.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.msewa.adapter.BusFilterAdapter;
import in.msewa.adapter.BusFilterAdapter;
import in.msewa.adapter.BusFilterAdapterT;
import in.msewa.model.BusCityModel;
import in.msewa.model.TravelCityModel;
import in.msewa.util.CitySelectedListener;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 4/12/2016.
 */
public class CustomAlertDialogSearchT extends Dialog {

    public CustomAlertDialogSearchT(final Context context, ArrayList<TravelCityModel> flightCityModelList, final CitySelectedListener citySelectedListener) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewDialog = inflater.inflate(R.layout.dialog_custom_search_t, null, false);
        android.widget.SearchView searchView = (android.widget.SearchView) viewDialog.findViewById(R.id.cadMessage);
        ListView lvSearch = (ListView) viewDialog.findViewById(R.id.lvSearchBus);
        final TextView tv_popularCity=(TextView)viewDialog.findViewById(R.id.tv_popularCity);
        ImageView ivClose = (ImageView) viewDialog.findViewById(R.id.ivClose);

//        flightCityModelList = new ArrayList<>();

        final BusFilterAdapterT domesticCityListAdapter = new BusFilterAdapterT(context, flightCityModelList);
        lvSearch.setAdapter(domesticCityListAdapter);
        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length() !=0) {
                    tv_popularCity.setText("Suggested Cities");
                }else {
                    tv_popularCity.setText("All Cities");
                }
                domesticCityListAdapter.getFilter().filter(newText);

                return false;
            }
        });
        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dismiss();
                TravelCityModel domesticFlightModel = (TravelCityModel) adapterView.getItemAtPosition(i);
                citySelectedListener.travelFilter(domesticFlightModel);

            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        this.setCancelable(false);

        this.setContentView(viewDialog);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

}
