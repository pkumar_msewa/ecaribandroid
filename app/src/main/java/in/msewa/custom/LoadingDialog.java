package in.msewa.custom;

import android.app.ProgressDialog;
import android.content.Context;

import in.msewa.ecarib.PayQwikApplication;

/**
 * Created by Ksf on 3/13/2016.
 */
public class LoadingDialog extends ProgressDialog {

  public LoadingDialog(Context context) {
    super(context);
    setMessage("Please wait...");
    setCancelable(false);
  }
}
