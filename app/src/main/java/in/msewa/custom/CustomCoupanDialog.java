package in.msewa.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import in.msewa.ecarib.R;

/**
 * Created by Ksf on 4/12/2016.
 */
public class CustomCoupanDialog extends AlertDialog.Builder {

    private EditText etCoupan;
    private Button btnSkip, btnApply;


    public CustomCoupanDialog(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewDialog = inflater.inflate(R.layout.dialog_coupan, null, false);
        btnApply = (Button) viewDialog.findViewById(R.id.btnApply);
        btnSkip = (Button) viewDialog.findViewById(R.id.btnSkip);

        this.setCancelable(false);
        this.setView(viewDialog);
        this.create();


    }
}
