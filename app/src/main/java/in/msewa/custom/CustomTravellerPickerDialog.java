package in.msewa.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;

import in.msewa.ecarib.R;
import in.msewa.util.TravellerSelectedListner;

/**
 * Created by Ksf on 9/26/2016.
 */
public class CustomTravellerPickerDialog extends AlertDialog.Builder {

    private TravellerSelectedListner travellerSelectedListner;
    private AlertDialog closeDialog;

    public CustomTravellerPickerDialog(final Context context, final TravellerSelectedListner travellerSelectedListner) {
        super(context);
        this.travellerSelectedListner = travellerSelectedListner;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_travller_picker, null, false);
        Button btnTravellerDismiss = (Button) viewDialog.findViewById(R.id.btnTravellerDismiss);
        Button btnTravellerOk = (Button) viewDialog.findViewById(R.id.btnTravellerOk);


        final NumberPicker npAdult = (NumberPicker) viewDialog.findViewById(R.id.npAdult);
        final NumberPicker npChild = (NumberPicker) viewDialog.findViewById(R.id.npChild);
        final NumberPicker npInfant = (NumberPicker) viewDialog.findViewById(R.id.npInfant);
        npInfant.setMaxValue(9);
        npInfant.setMinValue(0);
        npAdult.setMaxValue(9);
        npAdult.setMinValue(1);
        npChild.setMaxValue(9);
        npChild.setMinValue(0);

        btnTravellerDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog.dismiss();
            }
        });

        btnTravellerOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int total = npAdult.getValue() + npChild.getValue() + npInfant.getValue();
                Log.i("total", String.valueOf(total));
                if (npAdult.getValue() >= npInfant.getValue()) {
                    if (total <= 9) {
                        travellerSelectedListner.onSelected(npAdult.getValue(), npChild.getValue(), npInfant.getValue());
                        closeDialog.dismiss();
                    } else {
                        CustomToast.showMessage(context, "Your reached maximum passenger limit");
                    }
                } else {
                    CustomToast.showMessage(context, "Number of infants cannot be exceed number of adult");
                }

            }
        });

        this.setView(viewDialog);
        this.setCancelable(true);
        closeDialog = this.create();
        closeDialog.show();
        closeDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }


}