package in.msewa.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.VerifyLoginOTPListner;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 1/21/2017.
 */
public class VerifyLoginOTPDialog extends AlertDialog.Builder {

  private final TextView tvshowpassword;
  private TextView tvDialogOtpTitle;
  private MaterialEditText cadMessage;
  private Button btnDialogVerifyDismiss;
  private Button btnDialogVerifyOk;

  private AlertDialog closeDialog;
  private VerifyLoginOTPListner verifyListner;
  private Context context;

  private View focusView = null;
  private boolean cancel;

  private LoadingDialog loadDlg;
  private JSONObject jsonRequest;
  //Volley Tag
  private String tag_json_obj = "json_fund_pay";
  private UserModel session = UserModel.getInstance();
  private String mobileNo;
  private String password;
  private String regId;

  //To Be Used Later
  private JSONObject jsonRequestED;
  private boolean passwordenable;

  public VerifyLoginOTPDialog(Context context, final VerifyLoginOTPListner verifyListner) {
    super(context);
    this.context = context;
    this.verifyListner = verifyListner;
    loadDlg = new LoadingDialog(context);

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View viewDialog = inflater.inflate(R.layout.dialog_verify_login_otp, null, false);

    cadMessage = (MaterialEditText) viewDialog.findViewById(R.id.cadMessage);

    btnDialogVerifyOk = (Button) viewDialog.findViewById(R.id.btnyes);
    tvshowpassword = (TextView) viewDialog.findViewById(R.id.tvshowpassword);
    btnDialogVerifyDismiss = (Button) viewDialog.findViewById(R.id.btncancel);
//    tvDialogOtpTitle.setText("Verify the OTP sent on " + mobileNo);

    cadMessage.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
//        if (editable.length() == 6) {
//        attemptLoginNew();
//        }
      }
    });

    btnDialogVerifyOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (!TextUtils.isEmpty(cadMessage.getText().toString()) && cadMessage.getText().length() == 6) {
          attemptLoginNew();
        } else {
          cadMessage.setError("Please enter valid password");
        }
      }
    });
    tvshowpassword.setOnTouchListener(new View.OnTouchListener() {

      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (passwordenable) {
          passwordenable = false;
          cadMessage.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          cadMessage.setSelection(cadMessage.length());
          tvshowpassword.setText("Show Password");
        } else {
          cadMessage.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
          cadMessage.setSelection(cadMessage.length());
          tvshowpassword.setText("Hide Password");
          passwordenable = true;
        }
        return false;
      }
    });

    btnDialogVerifyDismiss.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        closeDialog.dismiss();

      }
    });


    this.setView(viewDialog);
    closeDialog = this.create();
    closeDialog.show();
  }

  public void attemptLoginNew() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("password", cadMessage.getText().toString());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("valies", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VERIFY_PASSWORD_LIMIT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          loadDlg.dismiss();
          try {
            String code = response.getString("code");
            loadDlg.dismiss();
            if (code != null && code.equals("S00")) {
              closeDialog.dismiss();
              verifyListner.onVerifySuccess();
            } else {
              String message = response.getString("message");
              loadDlg.dismiss();
              cadMessage.setError(message);
              verifyListner.onVerifyError();
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };

      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }
}
