package in.msewa.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.msewa.adapter.BankFilterAdapter;
import in.msewa.adapter.BusFilterAdapter;
import in.msewa.model.BankListModel;
import in.msewa.model.BusCityModel;
import in.msewa.util.CitySelectedListener;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 4/12/2016.
 */
public class CustomAlertDialogBankSearch extends Dialog {

  public CustomAlertDialogBankSearch(final Context context, List<BankListModel> flightCityModelList, String type, final CitySelectedListener citySelectedListener) {
    super(context);

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    final View viewDialog = inflater.inflate(R.layout.dialog_custom_search, null, false);
    android.widget.SearchView searchView = (android.widget.SearchView) viewDialog.findViewById(R.id.cadMessage);
    ListView lvSearch = (ListView) viewDialog.findViewById(R.id.lvSearchBus);
    final TextView tv_popularCity = (TextView) viewDialog.findViewById(R.id.tv_popularCity);
    ImageView back = (ImageView) viewDialog.findViewById(R.id.back);
//        flightCityModelList = new ArrayList<>();

    tv_popularCity.setText("Popular Banks");
    if (type.equalsIgnoreCase("To")) {
      searchView.setQueryHint("Enter Bank Name");
    } else {
      searchView.setQueryHint("Enter Bank Name");
    }

    final BankFilterAdapter domesticCityListAdapter = new BankFilterAdapter(context, flightCityModelList);
    lvSearch.setAdapter(domesticCityListAdapter);
    searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        return false;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        if (newText.length() != 0) {
          tv_popularCity.setText("Suggested Banks");
        } else {
          tv_popularCity.setText("Popular Banks");
        }
        domesticCityListAdapter.getFilter().filter(newText);

        return false;
      }
    });

    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dismiss();
      }
    });
    lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        dismiss();
        BankListModel domesticFlightModel = (BankListModel) adapterView.getItemAtPosition(i);
        citySelectedListener.bankFilter(domesticFlightModel);

      }
    });
    this.setCancelable(true);

    this.setContentView(viewDialog);

    this.getWindow().setSoftInputMode(
      WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

  }

}
