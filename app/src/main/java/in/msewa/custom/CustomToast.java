package in.msewa.custom;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import in.msewa.ecarib.R;


public class CustomToast {

	private static final int CUSTOM_LAYOUT_MESSAGE = R.layout.custom_toast_message;
	private static final int CUSTOM_LAYOUT_DEBUG = R.layout.custom_toast_message;
	private static Toast toast;

	public static void showMessage(Context context, final String message) {
		LayoutInflater inflater = LayoutInflater.from(context);
		final View layout = inflater.inflate(CUSTOM_LAYOUT_MESSAGE, null);

		final TextView text = (TextView) layout.findViewById(R.id.tvToastMessage);
		text.setText(message);
		if (toast == null) {
			toast = new Toast(context);
		}
		toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.BOTTOM, 0, 50);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		if (toast.getView().isShown()) {
			toast.cancel();
		}
		toast.show();
	}

	public static void cancelToast() {
		if (toast != null)
			toast = null;
	}

}
