//package in.msewa.custom;
//
//
//import android.content.Context;
//import android.content.pm.PackageManager;
//import android.hardware.fingerprint.FingerprintManager;
//import android.Manifest;
//import android.os.Build;
//import android.os.CancellationSignal;
//import android.support.annotation.RequiresApi;
//import android.support.v4.app.ActivityCompat;
//
//
//@RequiresApi(api = Build.VERSION_CODES.M)
//public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
//
//    private CancellationSignal cancellationSignal;
//    private Context context;
//    private FingerprintHelperListener listener;
//
//    byte[] password;
//
//    public FingerprintHandler(Context mContext, FingerprintHelperListener listener) {
//        context = mContext;
//        this.listener = listener;
//
//    }
//
//    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
//        cancellationSignal = new CancellationSignal();
//
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
//        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
//    }
//
//    @Override
//    public void onAuthenticationError(int errMsgId,
//                                      CharSequence errString) {
//
//        listener.authenticationFailed(errString.toString());
//    }
//
//    @Override
//    public void onAuthenticationFailed() {
//
//        listener.authenticationFailed("Authentication failed");
//    }
//
//    @Override
//    public void onAuthenticationHelp(int helpMsgId,
//                                     CharSequence helpString) {
//
//    }
//
//    public interface FingerprintHelperListener {
//        public void authenticationFailed(String error);
//
//        public void authenticationSucceeded(FingerprintManager.AuthenticationResult result);
//    }
//
//    @Override
//    public void onAuthenticationSucceeded(
//            FingerprintManager.AuthenticationResult result) {
//        listener.authenticationSucceeded(result);
////        SharedPreferences prefs = context.getSharedPreferences("san", MODE_PRIVATE);
////        String restoredText = prefs.getString("san", null);
////        if (restoredText != null) {
////            if (restoredText.equals(result.getCryptoObject().getCipher().getProvider().getName())) {
////               Toast.makeText(context,"sucess",Toast.LENGTH_LONG).show();
////            }//"No name defined" is the default value.
////        } else {
////            SharedPreferences.Editor editor = context.getSharedPreferences("san", MODE_PRIVATE).edit();
////            editor.putString("san", result.getCryptoObject().getCipher().getProvider().getName());
////            editor.commit();
////        }
//
//
////        listener.authenticationSucceeded(result);
//    }
//
//
//}
