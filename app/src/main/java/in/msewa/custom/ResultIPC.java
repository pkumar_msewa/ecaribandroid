package in.msewa.custom;

import java.util.ArrayList;

import in.msewa.model.AddOnsModel;
import in.msewa.model.DomesticFlightModel;
import in.msewa.model.ImagicaTicketModel;

public class ResultIPC {
    private static ResultIPC instance;

    public synchronized static ResultIPC get() {
        if (instance == null) {
            instance = new ResultIPC();
        }
        return instance;
    }

    private int sync = 0,sync1 = 0,sync2 = 0,sync3=0;
    private ArrayList<ImagicaTicketModel> largeData;
    private ArrayList<AddOnsModel> addOnsModels;
    private ArrayList<AddOnsModel.CarAdons> addOnsModels1;
    private ArrayList<DomesticFlightModel> domesticFlightModels;
    private String totalAmount;

    public int setLargeData(ArrayList<ImagicaTicketModel> largeData) {
        this.largeData = largeData;
        return ++sync;
    }

    public ArrayList<ImagicaTicketModel> getLargeData(int request) {
        return (request == sync) ? largeData : null;
    }


    public int setaddOnsModels(ArrayList<AddOnsModel> addOnsModels) {
        this.addOnsModels = addOnsModels;
        return ++sync1;
    }

    public ArrayList<AddOnsModel> getaddOnsModels(int request) {
        return (request == sync1) ? addOnsModels : null;
    }

    public int setaddOnsModels1(ArrayList<AddOnsModel.CarAdons> addOnsModels1) {
        this.addOnsModels1 = addOnsModels1;
        return ++sync2;
    }

    public ArrayList<AddOnsModel.CarAdons> getaddOnsModels1(int request) {
        return (request == sync2) ? addOnsModels1 : null;
    }

    public int setaddOnFlight(ArrayList<DomesticFlightModel> addOnsModels1) {
        this.domesticFlightModels = addOnsModels1;
        return ++sync3;
    }

    public ArrayList<DomesticFlightModel> getaddOnsFlight(int request) {
        return (request == sync3) ? domesticFlightModels : null;
    }

    public int setTotalAmount(String addOnsModels1) {
        this.totalAmount = addOnsModels1;
        return ++sync2;
    }

    public String getTotalAmount(int request) {
        return (request == sync2) ? totalAmount : null;
    }
}