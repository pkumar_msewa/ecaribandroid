package in.msewa.custom;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.msewa.model.UserModel;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 4/12/2016.
 */
public class CustomAlertDialog extends AlertDialog.Builder {
  private final SharedPreferences kycValues;
  UserModel userModel = UserModel.getInstance();

  public CustomAlertDialog(Context context, int title, CharSequence message) {
    super(context);


    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View viewDialog = inflater.inflate(R.layout.dialog_custom, null, false);

    TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
    titleTextView.setText(context.getText(title));

    TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
    messageTextView.setText(message);
    kycValues = context.getSharedPreferences("kyc", Context.MODE_PRIVATE);
    if (!kycValues.getBoolean("KYC", false)) {
      if (userModel.getUserAcName() != null && !userModel.getUserAcName().trim().equalsIgnoreCase("KYC")) {
        TextView textView = (TextView) viewDialog.findViewById(R.id.cadTiltleKYC);
        textView.setText("As per RBI guidelines, it is mandatory to update your KYC details.\\n\\nTo ensure continued usage of your wallet, kindly provide the below details.");
      }
    }

    this.setCancelable(false);

    this.setView(viewDialog);
    this.create();

  }
}
