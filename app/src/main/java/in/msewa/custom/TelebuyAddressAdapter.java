package in.msewa.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

import in.msewa.model.AddressEntityModel;
import in.msewa.ecarib.R;

public class TelebuyAddressAdapter extends ArrayAdapter<AddressEntityModel> {
	
	private Context context;
	
	ArrayList<AddressEntityModel> data = new ArrayList<AddressEntityModel>();

	public TelebuyAddressAdapter(Context context, int resource, ArrayList<AddressEntityModel> data) {
		super(context, resource, data);
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			row = LayoutInflater.from(context).inflate(R.layout.telebuy_address_spinner, null);
		}
		AddressEntityModel addressEntity = data.get(position);
		if (addressEntity != null) { 
			TextView addressName = (TextView) row.findViewById(R.id.item_value);
			addressName.setText(addressEntity.getDesc());
		}
		return row;
	}
}