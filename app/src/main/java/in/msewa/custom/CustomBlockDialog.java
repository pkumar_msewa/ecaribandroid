package in.msewa.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 1/21/2017.
 */
public class CustomBlockDialog extends AlertDialog.Builder {

    public CustomBlockDialog(Context context, String title, CharSequence message) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_success, null, false);

        TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
        titleTextView.setText(title);
        titleTextView.setVisibility(View.GONE);

        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
        messageTextView.setText(message);

        ImageView ivsuccessLogo = (ImageView) viewDialog.findViewById(R.id.ivsuccessLogo);
        ivsuccessLogo.setImageResource(R.drawable.ic_user_block);
        this.setCancelable(false);
        this.setView(viewDialog);


    }
}

