package in.msewa.ecarib;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.TextDrawable;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveCartListner;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.activity.HomeMainActivity;

public class ContactSelector extends AppCompatActivity {

  private static final int PERMISSION_REQUEST_CONTACT = 12;
  //EditText - Filter
  EditText inputSearch;

  ImageView iv;
  private static final int PLUS_SIGN_POS = 0;
  private static final int MOBILE_DIGITS = 10;
  ArrayList<String> mycontactsNameList = new ArrayList<String>();
  ArrayList<String> fullContactList = new ArrayList<String>();
  ArrayList<String> fullContactNumberList = new ArrayList<String>();
  ArrayList<String> mycontactsNumberList = new ArrayList<String>();
  ArrayList<String> mycontactsImageList = new ArrayList<String>();
  ArrayList<String> checked = new ArrayList<String>();

  //List Adapter
  ArrayAdapter<mItems> listAdapter;

  private ListView mainListView;
  private mItems[] itemss;

  //Here these are String of Array to save checked items
  private String[] nameschecked = new String[0];
  private String[] numberschecked = new String[0];
  private CheckBox btnCheckAll;
  private LoadingDialog loadDlg;
  private JSONObject jsonRequest;
  private UserModel userModel = UserModel.getInstance();
  private ImageView count;

  /**
   * Called when the activity is first created.
   */
  @SuppressWarnings("deprecation")
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.contact_phone);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    mainListView = (ListView) findViewById(R.id.list);
    inputSearch = (EditText) findViewById(R.id.search);
    count = (ImageView) findViewById(R.id.count);
    loadDlg = new LoadingDialog(ContactSelector.this);
    //HERE IS THE LOGIC TO ADD CHECKED ITEMS TO STRING OF ARRAY - nameschecked, numberschecked
    mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View item,
                              int position, long id) {
        mItems planet = listAdapter.getItem(position);
        planet.toggleChecked();

        SelectViewHolder viewHolder = (SelectViewHolder) item.getTag();
        viewHolder.getCheckBox().setChecked(planet.isChecked());
        if (nameschecked[position] == "" || nameschecked[position] == null) {
        } else {
        }
        if (planet.isChecked()) {
          nameschecked[position] = viewHolder.getCheckBox().getText().toString();
        } else {
          nameschecked[position] = "";

          if (nameschecked[position] == "") {
          }
        }

      }
    });

    itemss = (mItems[]) getLastNonConfigurationInstance();

    ArrayList<mItems> listArray = new ArrayList<mItems>();

    btnCheckAll = (CheckBox) findViewById(R.id.check);
    btnCheckAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        if (btnCheckAll.isChecked()) {
          for (int j = 0; j < mainListView.getAdapter().getCount(); j++) {
            mItems planet = listAdapter.getItem(j);
            planet.setChecked(true);
            listAdapter.notifyDataSetChanged();
          }
          TextDrawable plus = new TextDrawable(ContactSelector.this, (mainListView.getAdapter().getCount()) + "/50", ColorStateList.valueOf(getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
          count.setImageDrawable(plus);
        } else {

          for (int j = 0; j < mainListView.getAdapter().getCount(); j++) {
            mItems planet = listAdapter.getItem(j);
            planet.setChecked(false);
            listAdapter.notifyDataSetChanged();

          }
          TextDrawable plus = new TextDrawable(ContactSelector.this, (0) + "/50", ColorStateList.valueOf(getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
          count.setImageDrawable(plus);


        }


      }
    });

    /**
     * Enabling Search Filter
     * */

    //SEARCH FILTER
    inputSearch.addTextChangedListener(new TextWatcher() {

      public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                int arg3) {
        ContactSelector.this.listAdapter.getFilter().filter(cs);
      }

      public void beforeTextChanged(CharSequence arg0, int arg1,
                                    int arg2, int arg3) {

      }

      public void afterTextChanged(Editable arg0) {

      }
    });

//        try {
//            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//            String[] projection = new String[]{
//                    ContactsContract.CommonDataKinds.Phone._ID,
//                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
//                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
//                    ContactsContract.CommonDataKinds.Phone.NUMBER,
//                    ContactsContract.CommonDataKinds.Phone.STARRED,
//                    ContactsContract.CommonDataKinds.Phone.TYPE,
//                    ContactsContract.CommonDataKinds.Phone.PHOTO_URI,
//                    ContactsContract.CommonDataKinds.Phone.PHOTO_ID,
//                    ContactsContract.CommonDataKinds.Phone.PHOTO_FILE_ID,
//                    ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
//                    ContactsContract.CommonDataKinds.Phone.TIMES_CONTACTED};
//            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
//                    + " COLLATE LOCALIZED ASC";
//
//            Cursor cursor = PayQwikApplication.getInstance().getContentResolver()
//                    .query(uri, projection, null, null, sortOrder);
//
//            cursor.moveToFirst();
//
//            //SAVING CONTACTS DATA TO TEMP ARRAY
//            while (!cursor.isLast()) {
//
//                try {
//                    System.out
//                            .println("*******contact***2*****diplay name*******"
//                                    + cursor.getString(2));
//                    mycontactsNameList.add(cursor.getString(2).toString());
//                } catch (Exception e) {
//                    mycontactsNameList.add("");
//                    System.out.println("error 2");
//                    e.printStackTrace();
//                }
//
//                try {
//                    System.out.println("*******contact***3****number********"
//                            + cursor.getString(3));
//                    mycontactsNumberList.add(cursor.getString(3).toString());
//                } catch (Exception e) {
//                    mycontactsNumberList.add("");
//                    System.out.println("error 3");
//                    e.printStackTrace();
//                }
//
//                try {
//                    System.out.println("*******contact***4****image********"
//                            + cursor.getString(1));
//                    mycontactsImageList.add(cursor.getString(1).toString());
//                } catch (Exception e) {
//                    mycontactsImageList.add("");
//                    System.out.println("error 4");
//                }
//
//                cursor.moveToNext();
//            }
//        } catch (Exception e) {
//            System.out.println("err for read cont:::");
//            e.printStackTrace();
//        }

    //CLEAR ACTUAL ARRAY TO AVOID DUPLICATES AND REFRESH LIST
    askForContactPermission();
    fullContactList.clear();
    fullContactNumberList.clear();


    //ADD ALL TEMP ARRAY TO ACTUAL ARRAY
    fullContactList.addAll(mycontactsNameList);
    fullContactNumberList.addAll(mycontactsNumberList);

    //SET SIZE OF nameschecked and numberschecked SO THAT WE CAN ADD CHECKED DATA TO SPECIFIC POSITION
    nameschecked = new String[fullContactList.size()];
    numberschecked = new String[fullContactList.size()];

    //ADD ACTUAL ARRAY TO ARRAYLIST
    for (int i = 0; i < fullContactList.size(); i++) {
      listArray.add(new mItems(fullContactList.get(i),
        fullContactNumberList.get(i)));
    }


    //ADD ARRAY LIST TO ADAPTER AND SET ADAPTER TO LISTVIEW
    listAdapter = new SelectArrayAdapter(ContactSelector.this, listArray, count, new AddRemoveCartListner() {
      @Override
      public void taskCompleted(String response) {
        ArrayList<String> strings = new ArrayList<>();
        for (int j = 0; j < mainListView.getAdapter().getCount(); j++) {
          mItems planet = listAdapter.getItem(j);

          if (planet.isChecked()) {
            strings.add(planet.getName());
          }
        }
        if (strings.size() != 0) {
          TextDrawable plus = new TextDrawable(ContactSelector.this, (strings.size()) + "/50", ColorStateList.valueOf(getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
          count.setImageDrawable(plus);
        } else {
          TextDrawable plus = new TextDrawable(ContactSelector.this, (0) + "/50", ColorStateList.valueOf(getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
          count.setImageDrawable(plus);
        }
      }
    });
    mainListView.setAdapter(listAdapter);


    for (int j = 0; j < mainListView.getAdapter().getCount(); j++) {
      mItems planet = listAdapter.getItem(j);
      planet.setChecked(true);
      listAdapter.notifyDataSetChanged();
    }
    btnCheckAll.setChecked(true);
    listAdapter.notifyDataSetChanged();
    Button proceed = (Button) findViewById(R.id.btnProceed);
    proceed.setOnClickListener(new View.OnClickListener()

    {
      @Override
      public void onClick(View v) {
        ArrayList<String> strings = new ArrayList<String>();
        HashMap<String, String> stringHashMap = new HashMap<String, String>();
        for (int j = 0; j < mainListView.getAdapter().getCount(); j++) {
          mItems planet = listAdapter.getItem(j);
          if (planet.isChecked()) {
            strings.add(planet.getName());
            stringHashMap.put(planet.getNumber(), planet.getName());
          }
        }
        if (strings.size() != 0 && strings.size() >= 50) {
          Log.i("response", stringHashMap.toString());
          referearn(stringHashMap);
        } else {
          AlertDialog alert = new AlertDialog.Builder(ContactSelector.this).create();
          alert.setTitle("");
          alert.setMessage(" Choose atleast 50 contacts!!");
          alert.setButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
          });
          alert.show();
        }
      }
    });
    ArrayList<String> strings1 = new ArrayList<>();
    for (int j = 0; j < mainListView.getAdapter().getCount(); j++) {
      mItems planet = listAdapter.getItem(j);
      if (planet.isChecked()) {
        strings1.add(planet.getName());
      }
    }
    listAdapter.notifyDataSetChanged();
    Log.i("value", String.valueOf(strings1.size()));
    TextDrawable plus = new TextDrawable(ContactSelector.this, (strings1.size()) + "/50", ColorStateList.valueOf(getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
    count.setImageDrawable(plus);

  }

  /**
   * Holds data. - GETTER AND SETTER
   */
  private static class mItems {
    private String name = "";
    private boolean checked = false;
    private String number = "";


    public mItems(String name, String number) {
      this.name = name;
      this.number = number;

    }

    public String getName() {
      return name;
    }

    public String getNumber() {
      return number;
    }


    public boolean isChecked() {
      return checked;
    }

    public void setChecked(boolean checked) {
      this.checked = checked;
    }

    public String toString() {
      return name;
    }

    public void toggleChecked() {
      checked = !checked;
    }
  }

  private static class SelectViewHolder {
    private CheckBox checkBox;
    private TextView textView, number;
    private ImageView image;

    public SelectViewHolder(
      CheckBox checkBox) {
      this.checkBox = checkBox;
    }

    public CheckBox getCheckBox() {
      return checkBox;
    }


  }

  /**
   * Custom adapter for displaying an array of Planet objects.
   */
  private static class SelectArrayAdapter extends ArrayAdapter<mItems> {
    private LayoutInflater inflater;
    private ImageView count;
    private Context context;
    private AddRemoveCartListner listner;

    public SelectArrayAdapter(Context context, List<mItems> planetList, ImageView count, AddRemoveCartListner addRemoveCartListner) {
      super(context, R.layout.row, planetList);
      // Cache the LayoutInflate to avoid asking for a new one each time.
      inflater = LayoutInflater.from(context);
      this.count = count;
      this.context = context;
      this.listner = addRemoveCartListner;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // Planet to display
      mItems planet = (mItems) this.getItem(position);

      // The child views in each row.
      CheckBox checkBox;
      TextView name, number;
      ImageView iv1;

      // Create a new row view
      if (convertView == null) {
        convertView = inflater.inflate(R.layout.row, null);
        checkBox = (CheckBox) convertView.findViewById(R.id.checkbox1);
        convertView.setTag(new SelectViewHolder(checkBox));
        final ArrayList<String> strings = new ArrayList<>();
        checkBox.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            CheckBox cb = (CheckBox) v;
            mItems planet = (mItems) cb.getTag();
            planet.setChecked(cb.isChecked());
            strings.add(planet.getName());
            listner.taskCompleted(String.valueOf(strings.size()));

          }
        });
        if (strings.size() != 0) {
          Log.i("value", String.valueOf(strings.size()));
          TextDrawable plus = new TextDrawable(context, (strings.size()) + "/50", ColorStateList.valueOf(context.getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
          count.setImageDrawable(plus);
        }

      }
      // Reuse existing row view
      else {
        SelectViewHolder viewHolder = (SelectViewHolder) convertView
          .getTag();
        checkBox = viewHolder.getCheckBox();
      }
      checkBox.setTag(planet);
      checkBox.setChecked(planet.isChecked());
      checkBox.setText(planet.getName());

      return convertView;
    }
  }

//    public Object onRetainNonConfigurationInstance() {
//        return itemss;
//    }


  private void referearn(HashMap<String, String> stringStringHashMap) {
    loadDlg.show();
    try {
      jsonRequest = new JSONObject();
      jsonRequest.put("sessionId", userModel.getUserSessionId());
      jsonRequest.put("mobileList", getJsonFromMap(stringStringHashMap));
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("REFER_AND_EARN", jsonRequest.toString());
      Log.i("REFER API", ApiUrl.URL_REFER_AND_EARN);
      userModel.setHasRefer(true);
      userModel.save();
      startService(new Intent(this, MyService.class).putExtra("test", jsonRequest.toString()));
      AlertDialog.Builder builder = new AlertDialog.Builder(ContactSelector.this, R.style.AppCompatAlertDialogStyleMusic);
      LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View viewDialog = inflater1.inflate(R.layout.dialog_earntopay, null, false);
      builder.setView(viewDialog);
      builder.setCancelable(false);
      final AlertDialog alertDialog = builder.create();

      Button ok = (Button) viewDialog.findViewById(R.id.btnOK);

      ok.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Intent intent = new Intent(ContactSelector.this, HomeMainActivity.class);
          intent.putExtra("status", "true");
          startActivity(intent);
          finish();
          finishAffinity();
          alertDialog.dismiss();
        }
      });


      alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {

          dialogInterface.dismiss();
        }
      });
      alertDialog.show();
      loadDlg.dismiss();

    }
  }

  public void showSuccessDialog(String message, String Title) {
    CustomSuccessDialog builder = new CustomSuccessDialog(ContactSelector.this, Title, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        Intent mainActivityIntent = new Intent(ContactSelector.this, HomeMainActivity.class);
        sendRefresh();
        startActivity(mainActivityIntent);
        ContactSelector.this.finish();
      }
    });
    builder.show();
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(ContactSelector.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(ContactSelector.this).sendBroadcast(intent);
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(ContactSelector.this).sendBroadcast(intent);
  }

  private JSONObject getJsonFromMap(Map<String, String> map) throws JSONException {
    JSONObject jsonData = new JSONObject();
    for (String key : map.keySet()) {
      Object value = map.get(key);
      if (value instanceof Map<?, ?>) {
        value = getJsonFromMap((Map<String, String>) value);
      }
      jsonData.put(key, value);
    }
    return jsonData;
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.shopping_menu, menu);
    return true;

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();
    if (id == R.id.menuCart) {
      Intent intent = new Intent(getApplicationContext(), HomeMainActivity.class);
      intent.putExtra("status", "true");
      startActivity(intent);
      finishAffinity();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void setMargins(View view, int left, int top, int right, int bottom) {
    if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
      ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
      p.setMargins(left, top, right, bottom);
      view.requestLayout();
    }
  }


  private void doSomethingForEachUniquePhoneNumber(Context context) {
    String[] projection = new String[]{
      ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
      ContactsContract.CommonDataKinds.Phone.NUMBER,
      ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
      //plus any other properties you wish to query
    };

    Cursor cursor = null;
    try {
      cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, null);
    } catch (SecurityException e) {
      //SecurityException can be thrown if we don't have the right permissions
    }

    if (cursor != null) {
      try {
        HashSet<String> normalizedNumbersAlreadyFound = new HashSet<>();
        int indexOfNormalizedNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
        int indexOfDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexOfDisplayNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        while (cursor.moveToNext()) {
          String normalizedNumber = cursor.getString(indexOfNormalizedNumber);
          if (normalizedNumbersAlreadyFound.add(normalizedNumber)) {
            String name = cursor.getString(indexOfDisplayName);
            String phoneNumber = cursor.getString(indexOfDisplayNumber);
            //haven't seen this number yet: do something with this contact!
            if (removeCountryCode(phoneNumber.replaceAll("[^0-9+]", "")) != null && !removeCountryCode(phoneNumber.replaceAll("[^0-9+]", "")).isEmpty()) {

              if (name != null && !name.isEmpty()) {
                mycontactsNameList.add(name);
                mycontactsNumberList.add(removeCountryCode(phoneNumber.replaceAll("[^0-9+]", "")));
              } else {
                mycontactsNameList.add("User");
                mycontactsNumberList.add(removeCountryCode(phoneNumber.replaceAll("[^0-9+]", "")));
              }
            }
          } else {
            //don't do anything with this contact because we've already found this number
          }
        }
      } finally {
        cursor.close();
      }
    }
  }

  private String removeCountryCode(String number) {
    if (hasCountryCode(number)) {
      try {
        int country_digits = number.length() - MOBILE_DIGITS;
        number = number.substring(country_digits);

        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(number.trim().replace(" ", "").replace("-", ""));
        if (!gasCheckLog.isValid) {
          return null;
        } else {
          return number;
        }
      } catch (StringIndexOutOfBoundsException e) {
        return null;
      }
    } else if (hasZero(number))

    {
      if (number.length() >= 10) {
        int country_digits = number.length() - MOBILE_DIGITS;
        number = number.substring(country_digits);
        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(number.trim());
        if (!gasCheckLog.isValid) {
          return null;
        } else {
          return number;
        }
      }
    } else

    {
      return null;
    }
    return null;
  }

  private boolean hasCountryCode(String number) {
    try {
      return number.charAt(PLUS_SIGN_POS) == '+';
    } catch (StringIndexOutOfBoundsException e) {
      return false;
    }
  }

  private boolean hasZero(String number) {
    try {
      return number.charAt(PLUS_SIGN_POS) == '0';
    } catch (StringIndexOutOfBoundsException e) {
      return false;
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
//        askForContactPermission();

  }

  public void askForContactPermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
          android.Manifest.permission.READ_CONTACTS)) {
          AlertDialog.Builder builder = new AlertDialog.Builder(this);
          builder.setTitle("Contacts access needed");
          builder.setPositiveButton(android.R.string.ok, null);
          builder.setMessage("please confirm Contacts access");//TODO put real question
          builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onDismiss(DialogInterface dialog) {
              requestPermissions(
                new String[]
                  {android.Manifest.permission.READ_CONTACTS}
                , PERMISSION_REQUEST_CONTACT);
            }
          });
          builder.show();
          // Show an expanation to the user *asynchronously* -- don't block
          // this thread waiting for the user's response! After the user
          // sees the explanation, try again to request the permission.

        } else {

          // No explanation needed, we can request the permission.

          ActivityCompat.requestPermissions(this,
            new String[]{android.Manifest.permission.READ_CONTACTS},
            PERMISSION_REQUEST_CONTACT);

          // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
          // app-defined int constant. The callback method gets the
          // result of the request.
        }
      } else {
        doSomethingForEachUniquePhoneNumber(ContactSelector.this);
      }
    } else {
      doSomethingForEachUniquePhoneNumber(ContactSelector.this);
    }
  }


}
