package in.msewa.ecarib.broadcast;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import in.msewa.ecarib.NotificationMessage;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.LoginRegActivity;
import in.msewa.ecarib.firebase.MyFirebaseMessagingService;

import static android.support.v4.content.WakefulBroadcastReceiver.startWakefulService;

/**
 * Created by Ksf on 6/30/2016.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver  {
  private static final int NOTIFICATION_ID = 1;
  private static final String NOTIFICATION_CHANNEL_ID = "my_notification_channel";
  private static final String TAG = "values";


  @Override
  public void onReceive(Context context, Intent intent) {

    ComponentName comp = new ComponentName(context.getPackageName(), MyFirebaseMessagingService.class.getName());
    startWakefulService(context, (intent.setComponent(comp)));
    setResultCode(Activity.RESULT_OK);
//    PowerManager mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//    PowerManager.WakeLock mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
//    mWakeLock.acquire();
////    Bundle bundle = intent.getExtras();
////    if (bundle != null) {
////      Object message = intent.getExtras().get("message");
////      Object title = intent.getExtras().get("title");
////      Object image = intent.getExtras().get("image");
////      if (image == null) {
////        image = "null";
////      }
////      sendNotification(message.toString(), title.toString(), image, context);
////    }
//
////    ComponentName comp = new ComponentName(context.getPackageName(),
////      MyFirebaseMessagingService.class.getName());
////    startWakefulService(context, (intent.setComponent(comp)));
////    setResultCode(Activity.RESULT_OK);
//    ComponentName comp = new ComponentName(context.getPackageName(), MyFirebaseMessagingService.class.getName());
//    try {
//      startWakefulService(context, (intent.setComponent(comp)));
//      context.startActivity(new Intent(context,LoginRegActivity.class));
//    }catch (IllegalStateException e) {
//
//    }
//    if (intent.getExtras() != null) {
//      for (String key : intent.getExtras().keySet()) {
//        Object message = intent.getExtras().get("message");
//        Object title = intent.getExtras().get("title");
//        Object image = intent.getExtras().get("image");
//        if (image == null) {
//          image = "null";
//        }
//        sendNotification(message.toString(), title.toString(), image, context);
//
//      }
//    }
  }

  private void sendNotification(String msg, String title, Object image, Context context) {
    Intent myIntent = new Intent(context, NotificationMessage.class);
    myIntent.putExtra("Tittle", title);
    myIntent.putExtra("message", msg);
    myIntent.putExtra("image", image.toString());

    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent,
      PendingIntent.FLAG_UPDATE_CURRENT);

    NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);

      // Configure the notification channel.
      notificationChannel.setDescription("Channel description");
      notificationChannel.enableLights(true);
      notificationChannel.setLightColor(Color.RED);
      notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
      notificationChannel.enableVibration(true);
      notificationManager.createNotificationChannel(notificationChannel);
    }

    NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
      .setVibrate(new long[]{0, 100, 100, 100, 100, 100})
      .setSmallIcon(R.mipmap.ic_launcher)
      .setContentTitle(title)
      .setContentText(msg)
      .setAutoCancel(true)
      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
      .setContentIntent(pendingIntent);
    assert notificationManager != null;
    notificationManager.notify(NOTIFICATION_ID, builder.build());
//    GcmBroadcastReceiver.completeWakefulIntent(myIntent);
  }

}
