package in.msewa.ecarib;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.msewa.custom.AESCrypt;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomKycSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.ecarib.activity.HowToUpgradeActivity;
import me.philio.pinentry.PinEntryView;

public class LinkAdaharFragment extends android.support.v4.app.Fragment {

  private TextView tvOtherKYC;
  private UserModel session = UserModel.getInstance();
  private TextView tvnoAadhar;
  private ImageButton ivBackBtn;
  private String uniquiID;
  private LoadingDialog loadingDialog;
  private View rootView;
  private LinearLayout llAadhar;
  private LinearLayout llOTP;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.activity_link_adahar, container, false);
    tvOtherKYC = (TextView) rootView.findViewById(R.id.tvOtherKYC);
    tvnoAadhar = (TextView) rootView.findViewById(R.id.tvnoAadhar);
    ivBackBtn = (ImageButton) rootView.findViewById(R.id.ivBackBtn);
    final Button submit = (Button) rootView.findViewById(R.id.submit);
    llAadhar = (LinearLayout) rootView.findViewById(R.id.llAadhar);
    final TextView tvOtp = (TextView) rootView.findViewById(R.id.tvOtp);
    final Button otp = (Button) rootView.findViewById(R.id.otp);
    final CheckBox btnTerms = (CheckBox) rootView.findViewById(R.id.btnTerms);
    final EditText ed_Aadhar_Card = (EditText) rootView.findViewById(R.id.ed_Aadhar_Card);
    final PinEntryView et_otp = (PinEntryView) rootView.findViewById(R.id.et_otp);
    llOTP = (LinearLayout) rootView.findViewById(R.id.llOTP);
    ivBackBtn.setVisibility(View.VISIBLE);
    loadingDialog = new LoadingDialog(getActivity());
    ed_Aadhar_Card.setFocusable(true);
    ed_Aadhar_Card.setFocusableInTouchMode(true);
    TextView termsConditons = (TextView) rootView.findViewById(R.id.termsConditons);
    termsConditons.setText(Html.fromHtml("I Agree  <font fgcolor=\"#FF33B5E5\">Terms & Conditions</font>"));
    Spannable word = new SpannableString("I Agree");

    word.setSpan(new ForegroundColorSpan(Color.BLACK), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    termsConditons.setText(word);
    Spannable wordTwo = new SpannableString(" Terms & Conditions");

    wordTwo.setSpan(new ForegroundColorSpan(Color.parseColor("#08bbc7")), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    termsConditons.append(wordTwo);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        getActivity().finish();
      }
    });
    et_otp.setVisibility(View.GONE);
    otp.setVisibility(View.GONE);
    termsConditons.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Aadhaar  Terms & Conditions");
        builder.setMessage(Html.fromHtml("<p style=\"text-align: justify;\">I hereby understand/authorize VPayQwik&nbsp; to</p>\n" +
          "<p style=\"text-align: justify;\">1. Use my Aadhaar details for VPayqwik wallet and authenticate my identity through the Aadhaar Authentication system (Aadhaar based e-KYC services of UIDAI) in accordance with the provisions of the Aadhaar (Targeted Delivery of Financial and other Subsidies, Benefits and Services) Act 2016 and the allied rules and regulations notified thereunder.</p>\n" +
          "<p style=\"text-align: justify;\">2. Use Aadhaar number and OTP for authenticating my identity through the Aadhaar Authentication system for obtaining my e-KYC through Aadhaar based e-KYC services of UIDAI.</p>\n" +
          "<p style=\"text-align: justify;\">3. I understand that the Aadhaar details (physical and / or digital, as the case maybe) submitted for availing services under wallet will be maintained in wallet till the time the account is not inactive in wallet or the timeframe decided by RBI, the regulator of wallet, whichever is later.</p>\n" +
          "<pre><span style=\"color: #ff0000;\">Please confirm the declaration by clicking on the Agree to proceed</span></pre>\n" +
          "<p>&nbsp;</p>"));
        builder.setCancelable(false);
        builder.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            btnTerms.setChecked(true);
          }
        });
        AlertDialog dialog = builder.show();
        TextView messageView = (TextView) dialog.findViewById(android.R.id.message);
      }
    });
    et_otp.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
//        "^\\d{4}\\s\\d{4}\\s\\d{4}$"


      }
    });
    if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
      tvOtherKYC.setVisibility(View.GONE);
      tvnoAadhar.setVisibility(View.GONE);
    } else {
      tvOtherKYC.setVisibility(View.GONE);
      tvnoAadhar.setVisibility(View.GONE);
    }
    otp.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        loadingDialog.show();
        et_otp.setVisibility(View.VISIBLE);
        otp.setVisibility(View.VISIBLE);
        llAadhar.setVisibility(View.GONE);
        ed_Aadhar_Card.setVisibility(View.GONE);
        submit.setVisibility(View.GONE);

        JSONObject jsonObject = new JSONObject();
        try {
          jsonObject.put("sessionId", session.getUserSessionId());
          jsonObject.put("aadharNumber", ed_Aadhar_Card.getText().toString());
          jsonObject.put("otpreqId", uniquiID);
          jsonObject.put("key", et_otp.getText().toString());
          Log.i("values", jsonObject.toString());
        } catch (JSONException e) {
          e.printStackTrace();
        }

        try {
          AndroidNetworking.post(ApiUrl.URL_KYC_ADDAHAR_OTP)
            .addJSONObjectBody(new JSONObject().put("encryptData", AESCrypt.encrypt(jsonObject.toString()))) // posting json
            .setTag("test")
            .setPriority(Priority.IMMEDIATE)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
              @Override
              public void onResponse(JSONObject response) {

                loadingDialog.dismiss();
                String code = null;
                try {
                  code = response.getString("code");
                  String message = response.getString("message");
                  if (code.equalsIgnoreCase("S00")) {
                    String customerName = response.getString("customerName");
                    String customerAddress = response.getString("customerAddress");
                    CustomKycSuccessDialog customSuccessDialog = new CustomKycSuccessDialog(getActivity(), "", message,customerName,customerAddress);
                    customSuccessDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        sendRefresh();
                      }
                    });
                    customSuccessDialog.show();
                  } else if (code.equalsIgnoreCase("F03")) {
                    showInvalidSessionDialog(message);
                  } else {
                    CustomToast.showMessage(getActivity(), message);
                  }
                } catch (JSONException e) {
                  e.printStackTrace();
                }
              }
              @Override
              public void onError(ANError error) {
                loadingDialog.dismiss();
                CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception));
              }
            });

        } catch (JSONException e) {
          loadingDialog.dismiss();
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
    submit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (ed_Aadhar_Card.getText() != null && !ed_Aadhar_Card.getText().toString().isEmpty() && ed_Aadhar_Card.getText().length() == 12) {
          if (btnTerms.isChecked()) {
            loadingDialog.show();
            JSONObject jsonObject = new JSONObject();
            try {
              jsonObject.put("sessionId", session.getUserSessionId());
              jsonObject.put("aadharNumber", ed_Aadhar_Card.getText().toString());
            } catch (JSONException e) {
              e.printStackTrace();
            }


            try {
              AndroidNetworking.post(ApiUrl.URL_KYC_ADHAAR)
                .addJSONObjectBody(new JSONObject().put("encryptData", AESCrypt.encrypt(jsonObject.toString()))) // posting json
                .setTag("test")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                  @Override
                  public void onResponse(JSONObject response) throws JSONException {

                    loadingDialog.dismiss();
                    Log.i("responses", response.toString());
                    String code = response.getString("code");
                    String message = response.getString("message");
                    if (code.equalsIgnoreCase("S00")) {
                      CustomToast.showMessage(getActivity(), message);
                      et_otp.setVisibility(View.VISIBLE);
                      llOTP.setVisibility(View.VISIBLE);
                      llAadhar.setVisibility(View.GONE);
                      otp.setVisibility(View.VISIBLE);
                      tvOtp.setText(message);
                      ed_Aadhar_Card.setVisibility(View.INVISIBLE);
                      submit.setVisibility(View.INVISIBLE);
                      uniquiID = response.getString("otpRequestId");
                    } else if (code.equalsIgnoreCase("F03")) {
                      showInvalidSessionDialog(message);
                    } else {
                      CustomToast.showMessage(getActivity(), message);
                      et_otp.setVisibility(View.GONE);
                      otp.setVisibility(View.GONE);
                      ed_Aadhar_Card.setVisibility(View.VISIBLE);
                      submit.setVisibility(View.VISIBLE);
                    }

                  }

                  @Override
                  public void onError(ANError error) {
                    loadingDialog.dismiss();
                    et_otp.setVisibility(View.GONE);
                    otp.setVisibility(View.GONE);
                    ed_Aadhar_Card.setVisibility(View.VISIBLE);
                    submit.setVisibility(View.VISIBLE);
                    CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception));

                  }
                });
            } catch (Exception e) {
              e.printStackTrace();
              et_otp.setVisibility(View.GONE);
              otp.setVisibility(View.GONE);
              ed_Aadhar_Card.setVisibility(View.VISIBLE);
              submit.setVisibility(View.VISIBLE);
              loadingDialog.dismiss();
            }
          } else {
            CustomToast.showMessage(getActivity(), "Please accept terms & conditions to proceed");
          }
        } else {
          ed_Aadhar_Card.setError("Please enter 12 digits Aadharcard Number");

        }
      }
    });

    tvOtherKYC.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getActivity(), HowToUpgradeActivity.class));
      }
    });

    return rootView;
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }


  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void sendRefresh() {
    getActivity().finish();
    List<UserModel> currentUserList = Select.from(UserModel.class).list();
    UserModel currentUser = currentUserList.get(0);
    currentUser.setUserAcName("KYC");
    session.setUserAcName("KYC");
    currentUser.save();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }
}
