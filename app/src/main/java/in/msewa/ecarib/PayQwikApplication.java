package in.msewa.ecarib;

import android.app.Application;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ConnectionQuality;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ConnectionQualityChangeListener;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.orm.SugarContext;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import in.msewa.custom.ExceptionHandler;
import in.msewa.custom.Tls12SocketFactory;
import in.msewa.metadata.AppMetadata;
import in.msewa.util.AnalyticsTrackers;
import in.msewa.ecarib.activity.SplashActivity;
import io.appsfly.core.models.AppsFlyClientConfig;
import io.appsfly.core.providers.AppsFlyProvider;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;


/**
 * Created by Ksf on 6/24/2016.
 */
public class PayQwikApplication extends Application {

  public static final String TAG = PayQwikApplication.class.getSimpleName();

  private static PayQwikApplication mInstance;
  private static Context appContext;
  private RequestQueue mRequestQueue;
  public static SplashActivity this_MainActivity;

  @Override
  public void onCreate() {
    super.onCreate();
    Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
    SugarContext.init(this);
    appContext = getApplicationContext();

    mInstance = this;

    AnalyticsTrackers.initialize(PayQwikApplication.this);
    AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
    HttpLoggingInterceptor.Level logLevel = HttpLoggingInterceptor.Level.BODY;
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(logLevel);
    OkHttpClient okHttpClient = getUnsafeOkHttpClient().newBuilder()
      .retryOnConnectionFailure(false)
      .connectTimeout(4, TimeUnit.MINUTES)
      .readTimeout(4, TimeUnit.MINUTES)
      .writeTimeout(4, TimeUnit.MINUTES)
      .build();


    AndroidNetworking.initialize(this, okHttpClient);
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPurgeable = true;
    AndroidNetworking.setBitmapDecodeOptions(options);
    AndroidNetworking.setConnectionQualityChangeListener(new ConnectionQualityChangeListener() {
      @Override
      public void onChange(ConnectionQuality currentConnectionQuality, int currentBandwidth) {
        Log.d(TAG, "onChange: currentConnectionQuality : " + currentConnectionQuality + " currentBandwidth : " + currentBandwidth);
      }
    });


    //housejoy sdk

    ArrayList<AppsFlyClientConfig> appsFlyClientConfigs = new ArrayList<AppsFlyClientConfig>();
    AppsFlyClientConfig appsflyConfig = new AppsFlyClientConfig(getApplicationContext(), AppMetadata.hj_microapp_id, AppMetadata.hj_execution_url);
    appsFlyClientConfigs.add(appsflyConfig);
    AppsFlyProvider.getInstance().initialize(appsFlyClientConfigs, PayQwikApplication.this);

  }


  public static synchronized PayQwikApplication getInstance() {
    return mInstance;
  }

  public RequestQueue getRequestQueue() {
    if (mRequestQueue == null) {
      try {
//        final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(PayQwikApplication.this);

        final TrustManager[] trustAllCerts = new TrustManager[]{
          new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
              return new java.security.cert.X509Certificate[]{};
            }
          }
        };

        // Install the all-trusting trust manager
        final SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        // Create an ssl socket factory with our all-trusting manager
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack(null, sslSocketFactory));
      } catch (KeyManagementException e) {
        e.printStackTrace();
      } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
    }

    return mRequestQueue;
  }

  public static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
    if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
      try {
        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(null, null, null);
        client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

        ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
          .tlsVersions(TlsVersion.TLS_1_2)
          .tlsVersions(TlsVersion.TLS_1_3)
          .tlsVersions(TlsVersion.TLS_1_0)
          .tlsVersions(TlsVersion.TLS_1_1)
          .build();

        List<ConnectionSpec> specs = new ArrayList<>();
        specs.add(cs);
        specs.add(ConnectionSpec.COMPATIBLE_TLS);
        specs.add(ConnectionSpec.CLEARTEXT);

        client.connectionSpecs(specs);
      } catch (Exception exc) {
        Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
      }
    }

    return client;
  }

  private OkHttpClient getNewHttpClient() {
    OkHttpClient.Builder client = new OkHttpClient.Builder()
      .followRedirects(true)
      .followSslRedirects(true)
      .retryOnConnectionFailure(false)
      .cache(null)
      .connectTimeout(4, TimeUnit.MINUTES)
      .readTimeout(4, TimeUnit.MINUTES)
      .writeTimeout(4, TimeUnit.MINUTES);

    return enableTls12OnPreLollipop(client).build();
  }

  public <T> void addToRequestQueue(Request<T> req, String tag) {
    // set the default tag if tag is empty
    req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
    getRequestQueue().add(req);
  }

  public <T> void addToRequestQueue(Request<T> req) {
    req.setTag(TAG);
    getRequestQueue().add(req);
  }

  public void cancelPendingRequests(Object tag) {
    if (mRequestQueue != null) {
      mRequestQueue.cancelAll(tag);
    }
  }

  public synchronized Tracker getGoogleAnalyticsTracker() {
    AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
    return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
  }

  /***
   * Tracking screen view
   *
   * @param screenName screen name to be displayed on GA dashboard
   */
  public void trackScreenView(String screenName) {
    Tracker t = getGoogleAnalyticsTracker();

    // Set screen name.
    t.setScreenName(screenName);

    // Send a screen view.
    t.send(new HitBuilders.ScreenViewBuilder().build());

    GoogleAnalytics.getInstance(this).dispatchLocalHits();
  }

  /***
   * Tracking exception
   *
   * @param e exception to be tracked
   */
  public void trackException(Exception e) {
    if (e != null) {
      Tracker t = getGoogleAnalyticsTracker();

      t.send(new HitBuilders.ExceptionBuilder()
        .setDescription(
          new StandardExceptionParser(this, null)
            .getDescription(Thread.currentThread().getName(), e))
        .setFatal(false)
        .build()
      );
    }
  }

  /***
   * Tracking event
   *
   * @param category event category
   * @param action   action of the event
   * @param label    label
   */
  public void trackEvent(String category, String action, String label) {
    Tracker t = getGoogleAnalyticsTracker();

    // Build and send an Event.
    t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
  }

  @Override
  public void onTerminate() {
    super.onTerminate();
    SugarContext.terminate();
  }

  public static Context getAppContext() {
    return appContext;
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  private static OkHttpClient getUnsafeOkHttpClient() {
    try {
      // Create a trust manager that does not validate certificate chains
      final TrustManager[] trustAllCerts = new TrustManager[]{
        new X509TrustManager() {
          @Override
          public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
          }

          @Override
          public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
          }

          @Override
          public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[]{};
          }
        }
      };

      // Install the all-trusting trust manager
      final SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
      // Create an ssl socket factory with our all-trusting manager
      final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

      OkHttpClient.Builder builder = new OkHttpClient.Builder();
      builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
      builder.hostnameVerifier(new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      });

      OkHttpClient okHttpClient = builder.build();
      return okHttpClient;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


}
