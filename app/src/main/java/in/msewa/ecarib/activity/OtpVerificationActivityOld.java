package in.msewa.ecarib.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.metadata.ApiUrl;


/**
 * Created by Ksf on 3/11/2016.
 */
public class OtpVerificationActivityOld extends AppCompatActivity {

  private EditText etVerifyOTP;
  private Button btnVerify;
  private Button btnVerifyResend;

  private LoadingDialog loadDlg;
  private String userMobileNo;
  private String otpCode = null;

  private JSONObject jsonRequest;
  //Volley Tag
  private String tag_json_obj = "json_user";
  private int count = 0;
  private TextInputLayout ilVerifyOtp;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_verify_mobile);
    loadDlg = new LoadingDialog(OtpVerificationActivityOld.this);
    userMobileNo = getIntent().getStringExtra("userMobileNo");
    otpCode = getIntent().getStringExtra("OtpCode");

    btnVerifyResend = (Button) findViewById(R.id.btnVerifyResend);
    btnVerify = (Button) findViewById(R.id.btnVerify);
    etVerifyOTP = (EditText) findViewById(R.id.etVerifyOTP);
    ilVerifyOtp = (TextInputLayout) findViewById(R.id.ilVerifyOtp);

    if (otpCode != null && !otpCode.isEmpty()) {
      etVerifyOTP.setText(otpCode);
      verifyOTP();
    }

    btnVerifyResend.setTextColor(Color.parseColor("#ffffff"));
    btnVerify.setTextColor(Color.parseColor("#ffffff"));


    btnVerifyResend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        count++;
        if (count != 3) {
          loadDlg.show();
          resendOTP();
        } else {
          btnVerifyResend.setClickable(false);
          btnVerifyResend.setBackgroundResource(R.drawable.bg_button_gray_pressed);
        }

      }
    });


    btnVerify.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (!validateMobile()) {
          return;
        }
        verifyOTP();
      }
    });

  }

  private boolean validateMobile() {
    if (etVerifyOTP.getText().toString().trim().isEmpty() || etVerifyOTP.getText().toString().trim().length() < 6) {
      ilVerifyOtp.setError("Enter 10 digit no");
      etVerifyOTP.requestFocus();
      return false;
    } else {
      ilVerifyOtp.setErrorEnabled(false);
    }

    return true;
  }

  private void resendOTP() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", userMobileNo);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RESEND_OTP, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          Log.i("load", jsonObj.toString());
          try {
            String code = jsonObj.getString("code");
            String detail = jsonObj.getString("details");
            if (code != null & code.equals("S00")) {
              loadDlg.dismiss();
              CustomToast.showMessage(OtpVerificationActivityOld.this, detail);
              if (!etVerifyOTP.getText().toString().isEmpty()) {
                etVerifyOTP.setText("");
              }
            } else {
              Toast.makeText(OtpVerificationActivityOld.this, detail, Toast.LENGTH_SHORT).show();
              loadDlg.dismiss();


            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));

          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  private void verifyOTP() {
    final LoadingDialog loadDlg = new LoadingDialog(OtpVerificationActivityOld.this);
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", userMobileNo);
      jsonRequest.put("key", etVerifyOTP.getText().toString());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VERIFY_PHONE, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            String level = response.getString("status");
            String code = response.getString("code");

            if (code != null & code.equals("S00")) {
              loadDlg.dismiss();
              showLoginDialog();

            } else {
              loadDlg.dismiss();
              Toast.makeText(getApplicationContext(), "Failed matching Otp key", Toast.LENGTH_SHORT).show();


            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  public void showLoginDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(OtpVerificationActivityOld.this, R.string.dialog_title2, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        Intent otpIntent = new Intent(getApplicationContext(), LoginRegActivity.class);
        startActivity(otpIntent);
        finish();

      }
    });

    builder.show();
  }

  public String generateMessage() {
    String source = "<b><font color=#000000> Registration Successful.</font></b>" +
      "<br><br><b><font color=#ff0000> Please Login to use Ecarib. </font></b><br></br>";
    return source;
  }


}
