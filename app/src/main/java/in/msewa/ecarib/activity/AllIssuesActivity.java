package in.msewa.ecarib.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.AllIssuesListAdapter;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.ListIssueModel;
import in.msewa.model.ListcomponentModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;



/*import in.payqwik.adapter.AllIssuesListAdapter;
import in.payqwik.custom.LoadingDialog;
import in.payqwik.metadata.ApiUrl;
import in.payqwik.model.ListIssueModel;
import in.payqwik.model.ListcomponentModel;
import in.payqwik.model.UserModel;
import in.payqwik.test.PayQwikApplication;
import in.payqwik.test.R;

import static in.payqwik.test.R.id.spnComponent;*/

public class AllIssuesActivity extends AppCompatActivity {
    private ListView lvCustomer;
    private Spinner spnComponent;
    private LoadingDialog loadingDialog;
    private JSONObject jsonRequest;
    private String tag_json_obj = "json_user";
    private UserModel userModel=UserModel.getInstance();
    private ArrayList<ListcomponentModel> listcomponentModels;
    private ArrayList<ListIssueModel> listIssueModelArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_issues);
        spnComponent = (Spinner) findViewById(R.id.spnComponent);
        lvCustomer = (ListView) findViewById(R.id.lvCustomer);
        loadingDialog = new LoadingDialog(AllIssuesActivity.this);
        attemptRegister();

        Toolbar issuentoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(issuentoolbar);
        ImageButton issueIb = (ImageButton) findViewById(R.id.ivBackBtn);
        issueIb.setVisibility(View.VISIBLE);
        issueIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        AllIsusesAdapter allIsusesAdapter=new AllIsusesAdapter(AllIssuesActivity.this,)
    }

    public void attemptRegister() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("merchantCode", "LETS10031");
            jsonRequest.put("projectCode", "LETSPRO03202");
            jsonRequest.put("email", userModel.getUserEmail());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_TICKET_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("GET USER PARAMS", response.toString());
                    try {
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadingDialog.dismiss();
//                            String jsonString = response.getString("response");
                            //JSONObject jsonObject = new JSONObject(jsonString);
                            //String jsonString = response.getString("details");
                            JSONArray jsonArray = response.getJSONArray("details");
                            listIssueModelArrayList=new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                long Id = jsonArray.getJSONObject(i).getLong("id");
                                String TicketNo = jsonArray.getJSONObject(i).getString("ticketNo");
                                String Description = jsonArray.getJSONObject(i).getString("description");
                                String Status = jsonArray.getJSONObject(i).getString("status");
                                boolean New = jsonArray.getJSONObject(i).getBoolean("new");
                                String component = jsonArray.getJSONObject(i).getString("component");
                                long date = jsonArray.getJSONObject(i).getLong("date");
                                ListIssueModel listIssueModel = new ListIssueModel(Id,TicketNo,Description,Status,New,component,date);
                                listIssueModelArrayList.add(listIssueModel);

                            }
                        }
                        if (listIssueModelArrayList != null) {
                            AllIssuesListAdapter compountSpinnerAdapter = new AllIssuesListAdapter(AllIssuesActivity.this, listIssueModelArrayList);
                            loadingDialog.dismiss();
                            lvCustomer.setAdapter(compountSpinnerAdapter);
                        }

                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

}
