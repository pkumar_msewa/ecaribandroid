package in.msewa.ecarib.activity.useractivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.dialogsearch.SearchListItem;
import in.msewa.custom.dialogsearch.SearchableDialog;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;
import in.msewa.ecarib.activity.OtpVerificationMidLayerActivity;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.CircleModel;
import in.msewa.model.Country;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.SecurityUtil;
import in.msewa.util.VerifyLoginOTPListner;


//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LoginActivity extends AppCompatActivity implements VerifyLoginOTPListner {
    private static final String TAG = "loginresponse";
    public static final String PHONE = "phone";
    //FIREBASE
    public static final String PROPERTY_REG_ID = "registration_id";
    SharedPreferences musicPreferences;
    SharedPreferences phonePreferences;
    private EditText etLoginMobile, etPassword;
    private TextView tvForgotPassword;
    private FloatingActionButton fab, fab1;
    private CardView cvRegister, cvMob, cvBack;
    private ImageView ivBtn,ivLogo;
    private Button btnregister, btnBack;
    private boolean isBack = true;

    //    private JSONObject jsonRequestED;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();
    private String phoneNo;
    private String regId = "";

    //Volley Tag
    private String tag_json_obj = "json_user";
    private String countryCode;
    private ArrayList<Country> countries;
    private ImageView imageView;
    private List<SearchListItem> searchListItems = new ArrayList<>();
    private SearchableDialog searchableDialog;
    private LinearLayout llPass;
    private TextView tvCcode;
    private ImageButton ibShowPass;
    private String country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        musicPreferences = getSharedPreferences("ShowMusicDailog", Context.MODE_PRIVATE);
        phonePreferences = getSharedPreferences(PHONE, Context.MODE_PRIVATE);
        phoneNo = phonePreferences.getString("phone", "");

        cvMob = (CardView) findViewById(R.id.cvMob);
        llPass = (LinearLayout) findViewById(R.id.llPass);
        ivLogo=(ImageView)findViewById(R.id.ivLogo);
        etLoginMobile = (EditText) findViewById(R.id.etLoginMobile);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvCcode = (TextView) findViewById(R.id.tvCcode);
        fab = findViewById(R.id.fab);
        fab1 = findViewById(R.id.fab1);
        btnBack = (Button) findViewById(R.id.btnBack);
        cvRegister = (CardView) findViewById(R.id.cvRegister);
        cvBack = (CardView) findViewById(R.id.cvBack);
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        btnregister = (Button) findViewById(R.id.btnregister);
        ibShowPass = (ImageButton) findViewById(R.id.ibShowPass);
        etLoginMobile.addTextChangedListener(new MyTextWatcher(etLoginMobile));
        etPassword.addTextChangedListener(new MyTextWatcher(etPassword));
        loadDlg = new LoadingDialog(LoginActivity.this);
        Glide.with(this)
                .load(R.drawable.ic_top_banner)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivLogo);
//        countries = loadJSONFromAsset();
//        for (Country country : countries){
//            SearchListItem searchListItem = new SearchListItem(Integer.parseInt(country.getCountryCode()),country.getCountry());
//            searchListItems.add(searchListItem);
//        }
        if(!phoneNo.isEmpty() || phoneNo!=null || phoneNo.equalsIgnoreCase("null")){
            etLoginMobile.setText(phoneNo);
        }
        searchableDialog= new SearchableDialog(LoginActivity.this, searchListItems, "Select Country");
        //DONE CLICK ON VIRTUAL KEYPAD
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitForm();
                }
                return false;
            }
        });

        tvCcode.setText("+91 |");
//        Picasso.with(LoginActivity.this).load(ApiUrl.URL_COUNTRY_IMAGE + countries.get(0).getCountryImageCode().toLowerCase() + ".png").placeholder(R.drawable.ic_loading_image).into(imageView);
        countryCode = "91";
        country = "India";
//        tvCcode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (countries != null && countries.size() != 0) {
//                    searchableDialog.show();
//                    searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
//                        @Override
//                        public void onClick(int position, SearchListItem searchListItem) {
//                            tvCcode.setText("+" + searchListItem.getId()+" |");
//                            countryCode = String.valueOf(searchListItem.getId());
//                            country = searchListItem.getTitle();
//                        }
//                    });
//                }
//            }
//        });

        ibShowPass.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etPassword.setSelection(etPassword.length());
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etPassword.setSelection(etPassword.length());
                }

                return false;
            }
        });
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateMobile()) {
                    return;
                }
                Intent regIntent = new Intent(LoginActivity.this,RegisterActivity.class);
                regIntent.putExtra("MOB",etLoginMobile.getText().toString());
                regIntent.putExtra("COUNTRY",country);
                regIntent.putExtra("COUNTRYCODE",countryCode);
                startActivity(regIntent);
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateMobile()) {
                    return;
                }

                loadDlg.show();
                promoteForgetPwd();
            }
        });


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvBack.setVisibility(View.GONE);
                cvMob.setVisibility(View.VISIBLE);
                llPass.setVisibility(View.GONE);
                requestFocus(etLoginMobile);
                isBack=true;
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateMobile()) {
                    return;
                }
                cvMob.setVisibility(View.GONE);
                llPass.setVisibility(View.VISIBLE);
                cvBack.setVisibility(View.VISIBLE);
                requestFocus(etPassword);
                isBack=false;
            }
        });

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validatePassword()) {
                    return;
                }
                submitForm();
            }
        });

//        Button btnCallCustomer = (Button) findViewById(R.id.btnCallCustomer);
//        btnCallCustomer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                final String[] items = {"+919632494645"};
//                android.support.v7.app.AlertDialog.Builder callDialog =
//                        new android.support.v7.app.AlertDialog.Builder(LoginActivity.this, R.style.AppCompatAlertDialogStyle);
//                callDialog.setTitle("Select a no. to call");
//                callDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
//                callDialog.setItems(items, new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int position) {
//                        Intent callIntent = new Intent(Intent.ACTION_CALL);
//                        callIntent.setData(Uri.parse("tel:" + items[position].toString().trim()));
//                        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                            return;
//                        }
//                        startActivity(callIntent);
//                    }
//
//                });
//
//                callDialog.show();
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        if(isBack){
            super.onBackPressed();
        }else {
            cvBack.setVisibility(View.GONE);
            cvMob.setVisibility(View.VISIBLE);
            llPass.setVisibility(View.GONE);
            requestFocus(etLoginMobile);
            isBack=true;
        }
    }

    private String getStoredPhone() {
        if (phoneNo != null && !phoneNo.isEmpty()) {
            for (Country country : countries) {
                if (phoneNo.contains("+" + country.getCountryCode())) {
//                    Picasso.with(LoginActivity.this).load(ApiUrl.URL_COUNTRY_IMAGE + country.getCountryImageCode().toLowerCase() + ".png").placeholder(R.drawable.ic_loading_image).into(imageView);
                    return phoneNo.replace("+" + country.getCountryCode(), "");
                }
            }
            return "";
        } else {
            return "";
        }
    }

    public ArrayList<Country> loadJSONFromAsset() {
        ArrayList<Country> locList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getAssets().open("country.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONArray obj = new JSONArray(json);

            for (int i = 0; i < obj.length(); i++) {
                JSONObject jo_inside = obj.getJSONObject(i);
                Country location = new Country(jo_inside.getString("name"), jo_inside.getString("callingCode"), jo_inside.getString("code"));
                locList.add(location);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locList;
    }

    private boolean validatePassword() {
        if (etPassword.getText().toString().trim().isEmpty()) {
            etPassword.setError("Enter Password");
            requestFocus(etPassword);
            fab1.setImageResource(R.drawable.ic_clear_black_24dp);
            return false;
        }
//        else if (etLoginPassword.getText().toString().trim().length() < 6) {
//            ilLoginPassword.setError(getResources().getString(R.string.password_validity));
//            requestFocus(etLoginPassword);
//            return false;
//        }
        else {
            etPassword.setError(null);
            fab1.setImageResource(R.drawable.ic_arrow_forward);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etLoginMobile.getText().toString().trim().isEmpty() || etLoginMobile.getText().length()<10) {
            etLoginMobile.setError("Enter valid mobile number");
            requestFocus(etLoginMobile);
            fab.setImageResource(R.drawable.ic_clear_black_24dp);
            return false;
        } else {
            fab.setImageResource(R.drawable.ic_arrow_forward);
            etPassword.setError(null);
        }

        return true;
    }

    private void submitForm() {
//        if (!validateMobile()) {
//            return;
//        }
//
//        if (!validatePassword()) {
//            return;
//        }
        loadDlg.show();
        attemptLoginNew();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void attemptLoginNew() {
        jsonRequest = new JSONObject();
        String encryptValue = "";
        try {
            jsonRequest.put("username", etLoginMobile.getText().toString());
            jsonRequest.put("password", etPassword.getText().toString());
            regId = getRegistrationId();
            if (!regId.isEmpty()) {
                jsonRequest.put("registrationId", regId);
            }
            if (SecurityUtil.getIMEI(LoginActivity.this) != null) {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(LoginActivity.this) + "-" + SecurityUtil.getIMEI(LoginActivity.this));
            } else {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(LoginActivity.this));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("values", jsonRequest.toString());
            AndroidNetworking.post(ApiUrl.URL_LOGIN)
                    .addJSONObjectBody(jsonRequest) // posting json
                    .setTag("test")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                String code = response.getString("code");

                                if (code != null && code.equals("S00")) {
                                    String jsonString = response.getString("response");
                                    JSONObject jsonObject = new JSONObject(jsonString);
//                logMultilineString(response.toString());
                                    //New Implementation
                                    JSONObject jsonDetail = jsonObject.getJSONObject("details");
                                    String userSessionId = jsonDetail.getString("sessionId");
                                    String nikiOffer = "nooffer";
                                    if (jsonDetail.has("nikiOffer")) {
                                        nikiOffer = jsonDetail.getString("nikiOffer");
                                    }
                                    JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");

                                    double userBalanceD = accDetail.getDouble("balance");
                                    String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

                                    long userAcNoL = accDetail.getLong("accountNumber");
                                    String userAcNo = String.valueOf(userAcNoL);

                                    int userPointsI = accDetail.getInt("points");
                                    String userPoints = String.valueOf(userPointsI);

                                    JSONObject accType = accDetail.getJSONObject("accountType");
                                    String accName = accType.getString("name");
                                    String accCode = accType.getString("code");

                                    double accBalanceLimitD = accType.getDouble("balanceLimit");
                                    double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                                    double accDailyLimitD = accType.getDouble("dailyLimit");

                                    String accBalanceLimit = String.valueOf(accBalanceLimitD);
                                    String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                                    String accDailyLimit = String.valueOf(accDailyLimitD);

                                    JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");

                                    String userFName = jsonUserDetail.getString("firstName");
                                    String userLName = jsonUserDetail.getString("lastName");
                                    String userMobile = jsonUserDetail.getString("contactNo");
                                    String userEmail = jsonUserDetail.getString("email");
                                    String userEmailStatus = jsonUserDetail.getString("emailStatus");

                                    String userAddress = jsonUserDetail.getString("address");
                                    String userImage = jsonUserDetail.getString("image");
                                    String encodedImage = jsonUserDetail.getString("encodedImage");
                                    String images = "";
                                    if (!encodedImage.equals("")) {
                                        images = encodedImage;
                                    } else {
                                        images = userImage;
                                    }


                                    boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");
                                    String userDob = jsonUserDetail.getString("dateOfBirth");

                                    boolean hasRefer;
                                    if (response.has("hasRefer")) {
                                        hasRefer = response.getBoolean("hasRefer");
                                    } else {
                                        hasRefer = true;
                                    }
                                    String userGender = "";
                                    if (!jsonUserDetail.isNull("gender")) {
                                        userGender = jsonUserDetail.getString("gender");
                                    }
                                    boolean iplEnable = false, ebsEnable = false, vnetEnable = false, razorPayEnable = false;
                                    String mdexToken = "", mdexKey = "", iplPrediction = "", iplSchedule = "", iplMyPrediction = "";
                                    if (response.has("iplEnable")) {
                                        iplEnable = response.getBoolean("iplEnable");
                                        mdexToken = response.getString("mdexToken");
                                        mdexKey = response.getString("mdexKey");
                                        iplPrediction = response.getString("iplPrediction");
                                        iplSchedule = response.getString("iplSchedule");
                                        iplMyPrediction = response.getString("iplMyPrediction");

                                    }
                                    if (response.has("ebsEnable") || response.has("vnetEnable") || response.has("razorPayEnable")) {
                                        ebsEnable = response.getBoolean("ebsEnable");
                                        vnetEnable = response.getBoolean("vnetEnable");
                                        razorPayEnable = response.getBoolean("razorPayEnable");
                                    }


                                    try {
                                        //Encrypting sessionId
                                        UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, images, userDob, userGender, isMPin, userPoints, hasRefer, nikiOffer, iplEnable, iplMyPrediction, iplPrediction, iplSchedule, mdexKey, mdexToken, ebsEnable, vnetEnable, razorPayEnable);
                                        if (Select.from(UserModel.class).list().size() > 0) {
                                            userModel.update();
                                        } else {
                                            userModel.save();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    session.setUserSessionId(userSessionId);
                                    session.setUserFirstName(userFName);
                                    session.setUserLastName(userLName);
                                    session.setUserMobileNo(userMobile);
                                    session.setUserEmail(userEmail);
                                    session.setEmailIsActive(userEmailStatus);
                                    session.setUserAcCode(accCode);
                                    session.setUserAcNo(userAcNo);
                                    session.setUserBalance(userBalance);
                                    session.setUserAcName(accName);
                                    session.setUserMonthlyLimit(accMonthlyLimit);
                                    session.setUserDailyLimit(accDailyLimit);
                                    session.setUserBalanceLimit(accBalanceLimit);
                                    session.setUserImage(images);
                                    session.setUserAddress(userAddress);
                                    session.setUserGender(userGender);
                                    session.setUserDob(userDob);
                                    session.setMPin(isMPin);
                                    session.setHasRefer(hasRefer);
                                    session.setIsValid(1);
                                    session.setRazorPayEnable(razorPayEnable);
                                    session.setEbsEnable(ebsEnable);
                                    session.setVnetEnable(vnetEnable);
                                    session.setNikiOffer(nikiOffer);
                                    session.setUserPoints(userPoints);
                                    session.setIplEnable(iplEnable);
                                    session.setIplMyPrediction(iplMyPrediction);
                                    session.setIplPrediction(iplPrediction);
                                    session.setIplSchedule(iplSchedule);
                                    session.setMdexKey(mdexKey);
                                    session.setMdexToken(mdexToken);
                                    try {
                                        OperatorsModel.deleteAll(OperatorsModel.class);
                                        CircleModel.deleteAll(CircleModel.class);
                                    } catch (SQLiteException e) {

                                    }


                                    SharedPreferences.Editor editor = phonePreferences.edit();
                                    editor.clear();
                                    editor.putString("phone", userMobile);
                                    editor.apply();

//                                    SharedPreferences.Editor musiceditor = musicPreferences.edit();
//                                    musiceditor.clear();
//                                    musiceditor.putBoolean("ShowMusicDailog", true);
//                                    musiceditor.apply();

                                    loadDlg.dismiss();

//                            startActivity((new Intent(LoginActivity.this,ContactReadActivity.class)));
//                            LoginActivity.this.finish();
//                            Intent i = new Intent(LoginActivity.this, ContactReadActivity.class);
//                            startActivity(i);
                                    startActivity(new Intent(LoginActivity.this, HomeMainActivity.class));
                                    LoginActivity.this.finish();
                                } else if (code != null && code.equals("L01")) {
                                    loadDlg.dismiss();

                                    Intent otpIntent = new Intent(LoginActivity.this, OtpVerificationMidLayerActivity.class);
                                    otpIntent.putExtra("userMobileNo", etLoginMobile.getText().toString());
                                    otpIntent.putExtra("intentType", "Device");
                                    otpIntent.putExtra("devPassword", etPassword.getText().toString());
                                    otpIntent.putExtra("devRegId", regId);
                                    startActivityForResult(otpIntent, 0);
                                    LoginActivity.this.finish();
//                            new VerifyLoginOTPDialog(LoginActivity.this, LoginFragment.this, etLoginMobile.getText().toString(), etLoginPassword.getText().toString(), regId);
                                } else {
                                    String message = response.getString("message");
                                    loadDlg.dismiss();
                                    CustomToast.showMessage(LoginActivity.this, message);

                                }
                            } catch (
                                    JSONException e)

                            {
                                loadDlg.dismiss();
                                CustomToast.showMessage(LoginActivity.this, getResources().getString(R.string.server_exception2));
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            loadDlg.dismiss();
                            Log.i("valus", anError.getErrorDetail());
                            CustomToast.showMessage(LoginActivity.this, getResources().getString(R.string.server_exception));
                        }
                    });

        }

    }

    private void promoteForgetPwd() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", etLoginMobile.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FORGET_PWD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String message = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
//                            Intent autoIntent = new Intent(LoginActivity.this, ChangePwdActivity.class);
//                            autoIntent.putExtra("OtpCode","");
//                            autoIntent.putExtra("userMobileNo",etLoginMobile.getText().toString());


                            Intent verifyIntent = new Intent(LoginActivity.this, OtpVerificationMidLayerActivity.class);
                            verifyIntent.putExtra("userMobileNo", etLoginMobile.getText().toString());
                            verifyIntent.putExtra("intentType", "Forget");

                            loadDlg.dismiss();
//                            startActivity(autoIntent);
                            startActivity(verifyIntent);

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoginActivity.this, getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(LoginActivity.this, NetworkErrorHandler.getMessage(error, LoginActivity.this));

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }

    private void promoteTest() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("auth_facebook", "true");
            jsonRequest.put("access_token", "qwekqjsaldkaSDLAUREQWDAMSD");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, "http://swasthyanews.com/api/oauth", jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Test Response", response.toString());

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(LoginActivity.this, NetworkErrorHandler.getMessage(error, LoginActivity.this));

                }
            }) {

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    public void onVerifySuccess() {
        SharedPreferences.Editor editor = phonePreferences.edit();
        editor.clear();
        editor.putString("phone", countryCode + etLoginMobile.getText().toString());
        editor.commit();
        startActivity(new Intent(LoginActivity.this, HomeMainActivity.class));
        finish();
    }

    @Override
    public void onVerifyError() {
//        CustomToast.showMessage(LoginActivity.this,"Error occurred please try again");
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable s) {
            int i = view.getId();
            if (i == R.id.etLoginMobile) {
                validateMobile();
            } else if (i == R.id.etPassword) {
                validatePassword();
            }
        }
    }

    void logLargeString(String data) {
        final int CHUNK_SIZE = 4076;  // Typical max logcat payload.
        int offset = 0;
        while (offset + CHUNK_SIZE <= data.length()) {
            Log.d(TAG, data.substring(offset, offset += CHUNK_SIZE));
        }
        if (offset < data.length()) {
            Log.d(TAG, data.substring(offset));
        }
    }
}
