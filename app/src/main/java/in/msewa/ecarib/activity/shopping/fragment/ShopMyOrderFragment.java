package in.msewa.ecarib.activity.shopping.fragment;


import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.adapter.ShopMyOrderAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.ShoppingMyOrderModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


public class ShopMyOrderFragment extends Fragment {

    String cardTerms;
    private RecyclerView rvGiftCardCat;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog dialog;
    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shop_my_order, container, false);
        rvGiftCardCat = (RecyclerView) rootView.findViewById(R.id.rvGiftCardCat);
        dialog = new LoadingDialog(getActivity());
        getOrderList();
        return rootView;
    }


    public void getOrderList() {
        dialog.show();
        rvGiftCardCat.setVisibility(View.GONE);
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNo", session.getUserMobileNo());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("OrderlistURL", ApiUrl.URL_SHOPPING_MY_ORDER);
            Log.i("OrderREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPING_MY_ORDER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("ORDERREQ", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        String message = jsonObj.getString("message");

                        if (code != null && code.equals("S00")) {
                            List<ShoppingMyOrderModel> shoppingMyOrderModelArray = new ArrayList<>();
                            JSONArray responseArray = jsonObj.getJSONArray("response");
                            for (int i = 0; i < responseArray.length(); i++) {
                                JSONObject r = responseArray.getJSONObject(i);
                                String date = r.getString("date");
                                String trxId = r.getString("transactionRefNo");
                                String total = r.getString("totalAmount");
                                String orderID = r.getString("id");
                                JSONArray prodArray = r.getJSONArray("products");
                                ShoppingMyOrderModel shoppingMyOrderModel;
                                for (int j = 0; j < prodArray.length(); j++) {
                                    JSONObject p = prodArray.getJSONObject(j);
                                    String pName = p.getString("productName");
                                    String pPrice = String.valueOf(p.getLong("unitPrice"));
                                    String pImage =  p.getString("productImage");
                                    shoppingMyOrderModel = new ShoppingMyOrderModel(pName, pPrice, pImage, total, "0");
                                    shoppingMyOrderModel.setDate(date);
                                    shoppingMyOrderModel.setTotAmount(total);
                                    shoppingMyOrderModel.setTranxNo(trxId);
                                    shoppingMyOrderModel.setOrderId(orderID);
//                                    productsArray.add(product);
                                    shoppingMyOrderModelArray.add(shoppingMyOrderModel);
                                }
                            }
                            if (shoppingMyOrderModelArray != null && shoppingMyOrderModelArray.size() != 0) {
                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                                rvGiftCardCat.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                                GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
                                rvGiftCardCat.setLayoutManager(manager);
                                rvGiftCardCat.setHasFixedSize(true);
                                    ShopMyOrderAdapter shopMyOrderAdapter = new ShopMyOrderAdapter(getActivity(), shoppingMyOrderModelArray);
                                    rvGiftCardCat.setAdapter(shopMyOrderAdapter);
                                    dialog.dismiss();
                                    rvGiftCardCat.setVisibility(View.VISIBLE);
//                                }
                            } else {
                                Toast.makeText(getActivity(), "You haven't purchase any product yet.", Toast.LENGTH_SHORT).show();
                                getActivity().onBackPressed();
                                dialog.dismiss();
                            }
                        } else {

                            CustomToast.showMessage(getActivity(), message);
                            dialog.dismiss();
                            getActivity().finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), "Error connecting to server");
                    dialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 6000000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    public void onDetach() {
        postReq.cancel();
        super.onDetach();
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }
}
