package in.msewa.ecarib.activity.imagicaActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.ThemeParkTicketModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by kashifimam on 21/02/17.
 */

public class
ImagicaThemeParkList extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;
    private String sessid, session_name, token, aDate;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;
    private AQuery aq;

    private TextView tvRegularTitle, tvRegularDescrption, tvQtyRegularAdult, tvAmtRegularAdult, tvQtyRegularChild,
            tvAmtRegularChild, tvQtyRegularSenior, tvAmtRegularSenior, tvQtyRegularCollege, tvAmtRegularCollege,
            tvExpressTitle, tvExpressDescrption, tvQtyExpressAdult, tvAmtExpressAdult, tvQtyExpressChild, tvAmtExpressChild,
            tvQtyExpressCollege, tvAmtExpressCollege;
    private ImageView ivRegularTicket, ivExpressTicket;
    private String regularTicketDescr, regularTitle, regularImageUrl, expressTicketDescr, expressTitle, expressImageUrl;
    private ThemeParkTicketModel regularSenior, regularCollege, regularAdult, regularChild, expressChild, expressCollege,
            expressAdult;
    private CardView llRegularChild, llRegularAdult, llRegularCollege, llRegularSenior, llExpressChild, llExpressCollege, llExpressAdult;
    private float regAdultAmount, regChildAmount, regSeniorAmount, regCollegeAmount, expAdultAmount, expChildAmount, expCollegeAmount;
    private Button btnRegularAdultBook, btnRegularChildBook, btnSeniorBook, btnCollegeBook, btnExpressAdultBook, btnExpressChildBook, btnECollegeBook;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_park_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("imagica-booking-done"));
        loadDlg = new LoadingDialog(ImagicaThemeParkList.this);
        setSupportActionBar(toolbar);
        sessid = getIntent().getStringExtra("sessid");
        session_name = getIntent().getStringExtra("session_name");
        token = getIntent().getStringExtra("token");
        aDate = getIntent().getStringExtra("aDate");
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        llRegularChild = (CardView) findViewById(R.id.llRegularChild);
        llRegularAdult = (CardView) findViewById(R.id.llRegularAdult);
        llRegularCollege = (CardView) findViewById(R.id.llRegularCollege);
        llRegularSenior = (CardView) findViewById(R.id.llRegularSenior);

        llExpressChild = (CardView) findViewById(R.id.llExpressChild);
        llExpressCollege = (CardView) findViewById(R.id.llExpressCollege);
        llExpressAdult = (CardView) findViewById(R.id.llExpressAdult);

        tvAmtRegularAdult = (TextView) findViewById(R.id.tvAmtRegularAdult);
        tvRegularTitle = (TextView) findViewById(R.id.tvRegularTitle);
        tvRegularDescrption = (TextView) findViewById(R.id.tvRegularDescrption);
        tvQtyRegularAdult = (TextView) findViewById(R.id.tvQtyRegularAdult);

        tvQtyRegularChild = (TextView) findViewById(R.id.tvQtyRegularChild);
        tvAmtRegularChild = (TextView) findViewById(R.id.tvAmtRegularChild);

        tvQtyRegularSenior = (TextView) findViewById(R.id.tvQtyRegularSenior);
        tvAmtRegularSenior = (TextView) findViewById(R.id.tvAmtRegularSenior);

        tvQtyRegularCollege = (TextView) findViewById(R.id.tvQtyRegularCollege);
        tvAmtRegularCollege = (TextView) findViewById(R.id.tvAmtRegularCollege);

        tvExpressTitle = (TextView) findViewById(R.id.tvExpressTitle);
        tvExpressDescrption = (TextView) findViewById(R.id.tvExpressDescrption);
        tvQtyExpressAdult = (TextView) findViewById(R.id.tvQtyExpressAdult);
        tvAmtExpressAdult = (TextView) findViewById(R.id.tvAmtExpressAdult);
        tvQtyExpressChild = (TextView) findViewById(R.id.tvQtyExpressChild);
        tvAmtExpressChild = (TextView) findViewById(R.id.tvAmtExpressChild);
        tvQtyExpressCollege = (TextView) findViewById(R.id.tvQtyExpressCollege);
        tvAmtExpressCollege = (TextView) findViewById(R.id.tvAmtExpressCollege);

        ivRegularTicket = (ImageView) findViewById(R.id.ivRegularTicket);
        ivExpressTicket = (ImageView) findViewById(R.id.ivExpressTicket);

        btnRegularAdultBook = (Button) findViewById(R.id.btnRegularAdultBook);
        btnRegularChildBook = (Button) findViewById(R.id.btnRegularChildBook);
        btnSeniorBook = (Button) findViewById(R.id.btnSeniorBook);
        btnCollegeBook = (Button) findViewById(R.id.btnCollegeBook);
        btnExpressAdultBook = (Button) findViewById(R.id.btnExpressAdultBook);
        btnExpressChildBook = (Button) findViewById(R.id.btnExpressChildBook);
        btnECollegeBook = (Button) findViewById(R.id.btnECollegeBook);

        tvQtyRegularAdult.setOnClickListener(this);
        tvQtyExpressAdult.setOnClickListener(this);
        tvQtyExpressChild.setOnClickListener(this);
        tvQtyExpressCollege.setOnClickListener(this);
        tvQtyRegularChild.setOnClickListener(this);
        tvQtyRegularCollege.setOnClickListener(this);
        tvQtyRegularSenior.setOnClickListener(this);

        btnRegularAdultBook.setOnClickListener(this);
        btnRegularChildBook.setOnClickListener(this);
        btnSeniorBook.setOnClickListener(this);
        btnCollegeBook.setOnClickListener(this);
        btnExpressAdultBook.setOnClickListener(this);
        btnExpressChildBook.setOnClickListener(this);
        btnECollegeBook.setOnClickListener(this);

        getCardCategory();
    }

   private void setViews(){
       tvRegularTitle.setText(regularTitle);
       AQuery aq = new AQuery(ImagicaThemeParkList.this);
//       if (regularImageUrl != null && !regularImageUrl.isEmpty()) {
//           aq.id(ivRegularTicket).image(regularImageUrl, true, true);
//       } else {
//           aq.id(ivRegularTicket).background(R.drawable.image_imagica_theme);
//       }
       if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
           tvRegularDescrption.setText(Html.fromHtml(regularTicketDescr, Html.FROM_HTML_MODE_LEGACY));
       } else {
           tvRegularDescrption.setText(Html.fromHtml(regularTicketDescr));
       }
//        Log.i("AMOUNTVAL", "Cost : "+regularAdult.getCost_per()+" KKC : " + regularAdult.getKkc_tax()+" Service tax : " + regularAdult.getService_tax()+" swach tax : "+ regularAdult.getSwachh_tax());


       if(regularAdult!=null){
           regAdultAmount = Float.parseFloat(regularAdult.getService_tax())+ Float.parseFloat(regularAdult.getCost_per())+Float.parseFloat(regularAdult.getSwachh_tax())+Float.parseFloat(regularAdult.getKkc_tax());
           tvAmtRegularAdult.setText(String.format("%.2f", regAdultAmount));
        }else{
           llRegularAdult.setVisibility(View.GONE);
    }

       if(regularSenior!=null) {
           regSeniorAmount = Float.parseFloat(regularSenior.getService_tax()) + Float.parseFloat(regularSenior.getCost_per()) + Float.parseFloat(regularSenior.getSwachh_tax()) + Float.parseFloat(regularSenior.getKkc_tax());
           tvAmtRegularSenior.setText(String.format("%.2f", regSeniorAmount));
       }else{
           llRegularSenior.setVisibility(View.GONE);
       }

       if(regularChild!=null){
           regChildAmount = Float.parseFloat(regularChild.getService_tax())+ Float.parseFloat(regularChild.getCost_per())+Float.parseFloat(regularChild.getSwachh_tax())+Float.parseFloat(regularChild.getKkc_tax());
           tvAmtRegularChild.setText(String.format("%.2f",regChildAmount));
       }else{
           llRegularChild.setVisibility(View.GONE);
       }

       if(regularCollege!=null) {
           regCollegeAmount = Float.parseFloat(regularCollege.getService_tax()) + Float.parseFloat(regularCollege.getCost_per()) + Float.parseFloat(regularCollege.getSwachh_tax()) + Float.parseFloat(regularCollege.getKkc_tax());
           tvAmtRegularCollege.setText(String.format("%.2f", regCollegeAmount));
       } else{
           llRegularCollege.setVisibility(View.GONE);
       }

       tvExpressTitle.setText(expressTitle);
//       if (expressImageUrl != null && !expressImageUrl.isEmpty()) {
//           aq.id(ivExpressTicket).image(expressImageUrl, true, true);
//       } else {
//           aq.id(ivExpressTicket).background(R.drawable.image_imagica_theme);
//       }
       if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
           tvExpressDescrption.setText(Html.fromHtml(expressTicketDescr, Html.FROM_HTML_MODE_LEGACY));
       } else {
           tvExpressDescrption.setText(Html.fromHtml(expressTicketDescr));
       }

       if(expressAdult!=null){
           expAdultAmount = Float.parseFloat(expressAdult.getService_tax())+ Float.parseFloat(expressAdult.getCost_per())+Float.parseFloat(expressAdult.getSwachh_tax())+Float.parseFloat(expressAdult.getKkc_tax());
           tvAmtExpressAdult.setText(String.format("%.2f",expAdultAmount));
       }else {
           llExpressAdult.setVisibility(View.GONE);
       }


       if(expressChild!=null) {
           expChildAmount = Float.parseFloat(expressChild.getService_tax()) + Float.parseFloat(expressChild.getCost_per()) + Float.parseFloat(expressChild.getSwachh_tax()) + Float.parseFloat(expressChild.getKkc_tax());
           tvAmtExpressChild.setText(String.format("%.2f", expChildAmount));
       }else {
           llExpressChild.setVisibility(View.GONE);
       }

      if(expressCollege!=null){
          expCollegeAmount = Float.parseFloat(expressCollege.getService_tax())+ Float.parseFloat(expressCollege.getCost_per())+Float.parseFloat(expressCollege.getSwachh_tax())+Float.parseFloat(expressCollege.getKkc_tax());
          tvAmtExpressCollege.setText(String.format("%.2f",expCollegeAmount));
      }else {
          llExpressChild.setVisibility(View.GONE);
      }

    }

    private void showQty(final TextView qtyView, final float amount, final TextView amtView) {
        final CharSequence[] qty = {"1", "2", "3", "4"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ImagicaThemeParkList.this);
        builder.setTitle("Select Quantity");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setItems(qty, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                qtyView.setText(qty[i]);
             float totamt = Float.parseFloat(String.format("%.2f", amount)) * Float.parseFloat(String.valueOf(qty[i]));
                amtView.setText(String.valueOf(totamt));
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
    public void getCardCategory() {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("adult", "1");
            jsonRequest.put("child", "0");
            jsonRequest.put("senior", "0");
            jsonRequest.put("college", "0");
            jsonRequest.put("total_day", "1");
            jsonRequest.put("dateVisit", aDate);
            jsonRequest.put("dateDeparture", aDate);
            jsonRequest.put("destination", "Theme Park");
            //jsonRequest.put("session_name", session_name);
            jsonRequest.put("sessionId", sessid);
            jsonRequest.put("token", token);
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("THEMEPARKTICKETLIST", ApiUrl.URL_IMAGICA_THEME_PARK_LIST);
            Log.i("THEMETICKETREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_IMAGICA_THEME_PARK_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("THEMEPARKRESPONSE", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            JSONObject packagesObj = jsonObj.getJSONObject("packages");
                            String messageStr = packagesObj.getString("message");
                            JSONObject messageObj  = new JSONObject(messageStr);
                            regularImageUrl = packagesObj.getString("image_url");
                            //tvRegularDescrption = packagesObj.getString("description");
                            regularTitle = packagesObj.getString("title");



                            JSONObject ticket = messageObj.getJSONObject("ticket");

                            JSONObject regularTicket = ticket.getJSONObject("Regular Ticket");
                            regularTicketDescr = regularTicket.getString("ticket_des");
                            regularTitle = regularTicket.getString("title");


                            if(regularTicket.has("Senior Citizen")) {
                                JSONObject seniorCitizen = regularTicket.getJSONObject("Senior Citizen");
                                String type1 = seniorCitizen.getString("type");
                                String cost1 = seniorCitizen.getString("cost_per");
                                String productId1 = seniorCitizen.getString("product_id");
                                JSONObject taxDetails1 = seniorCitizen.getJSONObject("tax_details");
                                JSONObject tagServiceTax1 = taxDetails1.getJSONObject("service_tax");
                                String serviceTax1 = tagServiceTax1.getString("price_per_quantity");
                                JSONObject tagSwachhTax1 = taxDetails1.getJSONObject("swachh_tax");
                                String swachhTax1 = tagSwachhTax1.getString("price_per_quantity");
                                JSONObject tagKkcTax1 = taxDetails1.getJSONObject("kkc_tax");
                                String kkcTax1 = tagKkcTax1.getString("price_per_quantity");
                                regularSenior = new ThemeParkTicketModel(type1, cost1, productId1, serviceTax1, swachhTax1, kkcTax1);
                            }

                            if(regularTicket.has("College")) {
                                JSONObject college = regularTicket.getJSONObject("College");
                                String type2 = college.getString("type");
                                String cost2 = college.getString("cost_per");
                                String productId2 = college.getString("product_id");
                                JSONObject taxDetails2 = college.getJSONObject("tax_details");
                                JSONObject tagServiceTax2 = taxDetails2.getJSONObject("service_tax");
                                String serviceTax2 = tagServiceTax2.getString("price_per_quantity");
                                JSONObject tagSwachhTax2 = taxDetails2.getJSONObject("swachh_tax");
                                String swachhTax2 = tagSwachhTax2.getString("price_per_quantity");
                                JSONObject tagKkcTax2 = taxDetails2.getJSONObject("kkc_tax");
                                String kkcTax2 = tagKkcTax2.getString("price_per_quantity");
                                regularCollege = new ThemeParkTicketModel(type2, cost2, productId2, serviceTax2, swachhTax2, kkcTax2);
                            }

                            if(regularTicket.has("Adult")) {
                                JSONObject adult = regularTicket.getJSONObject("Adult");
                                String type3 = adult.getString("type");
                                String cost3 = adult.getString("cost_per");
                                String productId3 = adult.getString("product_id");
                                JSONObject taxDetails3 = adult.getJSONObject("tax_details");
                                JSONObject tagServiceTax3 = taxDetails3.getJSONObject("service_tax");
                                String serviceTax3 = tagServiceTax3.getString("price_per_quantity");
                                JSONObject tagSwachhTax3 = taxDetails3.getJSONObject("swachh_tax");
                                String swachhTax3 = tagSwachhTax3.getString("price_per_quantity");
                                JSONObject tagKkcTax3 = taxDetails3.getJSONObject("kkc_tax");
                                String kkcTax3 = tagKkcTax3.getString("price_per_quantity");
                                regularAdult = new ThemeParkTicketModel(type3, cost3, productId3, serviceTax3, swachhTax3, kkcTax3);
                            }

                            if(regularTicket.has("Child")){
                                JSONObject child = regularTicket.getJSONObject("Child");
                                String type7 = child.getString("type");
                                String cost7 = child.getString("cost_per");
                                String productId7 = child.getString("product_id");
                                JSONObject taxDetails7 = child.getJSONObject("tax_details");
                                JSONObject tagServiceTax7 = taxDetails7.getJSONObject("service_tax");
                                String serviceTax7 = tagServiceTax7.getString("price_per_quantity");
                                JSONObject tagSwachhTax7 = taxDetails7.getJSONObject("swachh_tax");
                                String swachhTax7 = tagSwachhTax7.getString("price_per_quantity");
                                JSONObject tagKkcTax7 = taxDetails7.getJSONObject("kkc_tax");
                                String kkcTax7 = tagKkcTax7.getString("price_per_quantity");
                                regularChild = new ThemeParkTicketModel(type7,cost7,productId7,serviceTax7,swachhTax7,kkcTax7);
                            }

                            JSONObject expressTicket = ticket.getJSONObject("Imagica Express");
                            expressTicketDescr = expressTicket.getString("ticket_des");
                            expressTitle = expressTicket.getString("title");
                            expressImageUrl = expressTicket.getString("image_url");

                            if(expressTicket.has("Child")) {
                                JSONObject eChild = expressTicket.getJSONObject("Child");
                                String type4 = eChild.getString("type");
                                String cost4 = eChild.getString("cost_per");
                                String productId4 = eChild.getString("product_id");
                                JSONObject taxDetails4 = eChild.getJSONObject("tax_details");
                                JSONObject tagServiceTax4 = taxDetails4.getJSONObject("service_tax");
                                String serviceTax4 = tagServiceTax4.getString("price_per_quantity");
                                JSONObject tagSwachhTax4 = taxDetails4.getJSONObject("swachh_tax");
                                String swachhTax4 = tagSwachhTax4.getString("price_per_quantity");
                                JSONObject tagKkcTax4 = taxDetails4.getJSONObject("kkc_tax");
                                String kkcTax4 = tagKkcTax4.getString("price_per_quantity");
                                expressChild = new ThemeParkTicketModel(type4, cost4, productId4, serviceTax4, swachhTax4, kkcTax4);
                            }

                            if(expressTicket.has("College")) {
                                JSONObject eCollege = expressTicket.getJSONObject("College");
                                String type5 = eCollege.getString("type");
                                String cost5 = eCollege.getString("cost_per");
                                String productId5 = eCollege.getString("product_id");
                                JSONObject taxDetails5 = eCollege.getJSONObject("tax_details");
                                JSONObject tagServiceTax5 = taxDetails5.getJSONObject("service_tax");
                                String serviceTax5 = tagServiceTax5.getString("price_per_quantity");
                                JSONObject tagSwachhTax5 = taxDetails5.getJSONObject("swachh_tax");
                                String swachhTax5 = tagSwachhTax5.getString("price_per_quantity");
                                JSONObject tagKkcTax5 = taxDetails5.getJSONObject("kkc_tax");
                                String kkcTax5 = tagKkcTax5.getString("price_per_quantity");
                                expressCollege = new ThemeParkTicketModel(type5, cost5, productId5, serviceTax5, swachhTax5, kkcTax5);
                            }

                            if(expressTicket.has("Adult")) {
                                JSONObject eAdult = expressTicket.getJSONObject("Adult");
                                String type6 = eAdult.getString("type");
                                String cost6 = eAdult.getString("cost_per");
                                String productId6 = eAdult.getString("product_id");
                                JSONObject taxDetails6 = eAdult.getJSONObject("tax_details");
                                JSONObject tagServiceTax6 = taxDetails6.getJSONObject("service_tax");
                                String serviceTax6 = tagServiceTax6.getString("price_per_quantity");
                                JSONObject tagSwachhTax6 = taxDetails6.getJSONObject("swachh_tax");
                                String swachhTax6 = tagSwachhTax6.getString("price_per_quantity");
                                JSONObject tagKkcTax6 = taxDetails6.getJSONObject("kkc_tax");
                                String kkcTax6 = tagKkcTax6.getString("price_per_quantity");
                                expressAdult = new ThemeParkTicketModel(type6, cost6, productId6, serviceTax6, swachhTax6, kkcTax6);
                            }

                            loadDlg.dismiss();
                            setViews();


                        }else{
                            loadDlg.dismiss();
                            String message = jsonObj.getString("message");
                            CustomToast.showMessage(ImagicaThemeParkList.this,message);
                            finish();

                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(ImagicaThemeParkList.this, NetworkErrorHandler.getMessage(error, ImagicaThemeParkList.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void createOrder(String pid, final String qty, final String amount) {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("product_id", pid);
            jsonRequest.put("quantity", qty);
            jsonRequest.put("total_day", "1");
            jsonRequest.put("field_visit_date", aDate);
            jsonRequest.put("field_departure_date", aDate);
            jsonRequest.put("field_evt_res_business_unit", "Theme Park");
            jsonRequest.put("amount", amount);
            jsonRequest.put("session_name", session_name);
            jsonRequest.put("sessid", sessid);
            jsonRequest.put("token", token);
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("ORDERCREATEURL", ApiUrl.URL_IMAGICA_CREATE_ORDER);
            Log.i("ORDERCREATEREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_IMAGICA_CREATE_ORDER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("ORDERCREATERESP", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            JSONObject detailsObj = jsonObj.getJSONObject("details");
                            String messageStr = detailsObj.getString("message");
                            JSONObject messageObj  = new JSONObject(messageStr);
                            String uid = messageObj.getString("uid");
                            String order_id = messageObj.getString("order_id");
                            Intent imagicaOrderIntent = new Intent(getApplicationContext(), ImagicaOrderActivity.class);
                            imagicaOrderIntent.putExtra("AMOUNT", amount);
                            imagicaOrderIntent.putExtra("QTY", qty);
                            imagicaOrderIntent.putExtra("UID", uid);
                            imagicaOrderIntent.putExtra("ORDER_ID", order_id);
                            imagicaOrderIntent.putExtra("session_name", session_name);
                            imagicaOrderIntent.putExtra("sessid", sessid);
                            imagicaOrderIntent.putExtra("token", token);
                            startActivity(imagicaOrderIntent);
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(ImagicaThemeParkList.this, NetworkErrorHandler.getMessage(error, ImagicaThemeParkList.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvQtyRegularAdult:
                showQty(tvQtyRegularAdult, regAdultAmount, tvAmtRegularAdult);
                break;
            case R.id.tvQtyRegularChild:
                showQty(tvQtyRegularChild, regChildAmount, tvAmtRegularChild);
                break;
            case R.id.tvQtyRegularSenior:
                showQty(tvQtyRegularSenior, regSeniorAmount, tvAmtRegularSenior);
                break;
            case R.id.tvQtyRegularCollege:
                showQty(tvQtyRegularCollege, regCollegeAmount, tvAmtRegularCollege);
                break;
            case R.id.tvQtyExpressAdult:
                showQty(tvQtyExpressAdult, expAdultAmount, tvAmtExpressAdult);
                break;
            case R.id.tvQtyExpressChild:
                showQty(tvQtyExpressChild, expChildAmount, tvAmtExpressChild);
                break;
            case R.id.btnRegularAdultBook:
                createOrder(regularAdult.getProduct_id(), tvQtyRegularAdult.getText().toString(), tvAmtRegularAdult.getText().toString());
                break;
            case R.id.btnRegularChildBook:
                createOrder(regularChild.getProduct_id(), tvQtyRegularChild.getText().toString(), tvAmtRegularChild.getText().toString());
                break;
            case R.id.btnSeniorBook:
                createOrder(regularSenior.getProduct_id(), tvQtyRegularSenior.getText().toString(), tvAmtRegularSenior.getText().toString());
                break;
            case R.id.btnCollegeBook:
                createOrder(regularCollege.getProduct_id(), tvQtyRegularCollege.getText().toString(), tvAmtRegularCollege.getText().toString());
                break;
            case R.id.btnECollegeBook:
                createOrder(expressCollege.getProduct_id(), tvQtyExpressCollege.getText().toString(), tvAmtExpressCollege.getText().toString());
                break;
            case R.id.btnExpressAdultBook:
                createOrder(expressAdult.getProduct_id(), tvQtyExpressAdult.getText().toString(), tvAmtExpressAdult.getText().toString());
                break;
            case R.id.btnExpressChildBook:
                createOrder(expressChild.getProduct_id(), tvQtyExpressChild.getText().toString(), tvAmtExpressChild.getText().toString());
                break;
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

}
