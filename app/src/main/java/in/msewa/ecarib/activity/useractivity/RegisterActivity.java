package in.msewa.ecarib.activity.useractivity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DatePickerDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import in.msewa.adapter.OperatorSpinnerAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.MainMenuDetailActivity;
import in.msewa.ecarib.activity.OtpVerificationActivity;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.metadata.MenuMetadata;
import in.msewa.model.Country;
import in.msewa.model.OperatorsModel;
import in.msewa.util.NetworkErrorHandler;

/**
 * Created by Ksf on 3/27/2016.
 */
public class RegisterActivity extends AppCompatActivity {
    private static final String KEY_NAME = "santosh";
    //Strong password or weak
    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");
    private  EditText etRegisterPassword, etRegisterEmail,  etRegisterFirstName, etLasttNameAns, etRegisterDob, etGender;
    //    private RadioButton rbRegisterMale,rbRegisterFeMale;
    private String firstName, lastName, newPassword, rePassword, email, mobileNo;
    private LoadingDialog loadDlg;
    private String gender = "M";
    private View rootView;
    private JSONObject jsonRequest;
    private String quesCode;
    private Boolean isQuesSet = false;
    //Volley Tag
    private String tag_json_obj = "json_user";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private TextView textView;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private byte[] fingerprintStatus;
    private boolean status;
    private TextView  tvTermCondition;
    private String countryCode;
    private String cntry;

    //Govt. ID proof
    private Spinner spinner_govtid_provider;
    private OperatorSpinnerAdapter operatorSpinnerAdapter;
    private EditText etGovtIdNo;
    private CheckBox cbAcceptTerms;
    private Toolbar toolbar;

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        loadDlg = new LoadingDialog(RegisterActivity.this);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        etRegisterFirstName = (EditText) findViewById(R.id.etRegisterFirstName);
        etRegisterPassword = (EditText) findViewById(R.id.etRegisterPassword);
        etGender = (EditText) findViewById(R.id.etGender);
        etRegisterEmail = (EditText) findViewById(R.id.etRegisterEmail);
        etRegisterDob = (EditText) findViewById(R.id.etRegisterDob);
        etLasttNameAns = (EditText) findViewById(R.id.etLasttNameAns);
        ImageView ivBackBtn = (ImageView) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        spinner_govtid_provider = (Spinner) findViewById(R.id.spinner_govtid_provider);
//        operatorSpinnerAdapter = new OperatorSpinnerAdapter(RegisterActivity.this, R.layout.spinners, MenuMetadata.getgovtId());
//        spinner_govtid_provider.setAdapter(operatorSpinnerAdapter);

        etGovtIdNo = (EditText) findViewById(R.id.etGovtIdNo);

        mobileNo = getIntent().getStringExtra("MOB");
        countryCode = getIntent().getStringExtra("COUNTRYCODE");
        cntry = getIntent().getStringExtra("COUNTRY");

        ImageButton ibShowPass = (ImageButton) findViewById(R.id.ibShowPass);

        ibShowPass.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    etRegisterPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etRegisterPassword.setSelection(etRegisterPassword.length());
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    etRegisterPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etRegisterPassword.setSelection(etRegisterPassword.length());
                }

                return false;
            }
        });

        tvTermCondition = (TextView) findViewById(R.id.tvTermCondition);

        if (getEmail(RegisterActivity.this) != null && !getEmail(RegisterActivity.this).isEmpty()) {
            etRegisterEmail.setText(getEmail(RegisterActivity.this));
        }

        final FloatingActionButton fabRegisterSubmit = (FloatingActionButton) findViewById(R.id.fabRegisterSubmit);

        cbAcceptTerms = (CheckBox) findViewById(R.id.cbAcceptTerms);
        cbAcceptTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    fabRegisterSubmit.setVisibility(View.VISIBLE);
                } else {
                    fabRegisterSubmit.setVisibility(View.INVISIBLE);
                }
            }
        });

        etRegisterEmail.addTextChangedListener(new MyTextWatcher(etRegisterEmail));
        etRegisterPassword.addTextChangedListener(new MyTextWatcher(etRegisterPassword));
        etLasttNameAns.addTextChangedListener(new MyTextWatcher(etLasttNameAns));
        etGovtIdNo.addTextChangedListener(new MyTextWatcher(etGovtIdNo));
//        if (isSensorAvialable()) {
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                try {
//
//                    keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//                    fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
//                    status = true;
//                    if (!fingerprintManager.isHardwareDetected()) {
//
//
//                    } else if (ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//                        CustomToast.showMessage(RegisterActivity.this, "Please enable the fingerprint permission");
//
//                    }
//
//                    if (!fingerprintManager.hasEnrolledFingerprints()) {
//                        CustomToast.showMessage(RegisterActivity.this, "No fingerprint configured. Please register at least one fingerprint in your device's Settings");
//
//                    }
//
//                    if (!keyguardManager.isKeyguardSecure()) {
//                        CustomToast.showMessage(RegisterActivity.this, "Please enable lockscreen security in your device's Settings");
//                    } else {
//                        try {
//
//                            generateKey();
//                        } catch (FingerprintException e) {
//                            e.printStackTrace();
//                        }
////
//                        initCipher();
//
//
//                    }
//                } catch (NoClassDefFoundError error) {
//                    error.printStackTrace();
//                    status = false;
//                }
//
//            }
//        }

        tvTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menuIntent = new Intent(RegisterActivity.this, MainMenuDetailActivity.class);
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "TermsAndCondition");
                startActivity(menuIntent);
            }
        });

        etGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGender();
            }
        });

        etRegisterDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatePickerDialog dialogDate = new DatePickerDialog(RegisterActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar userAge = new GregorianCalendar(year, (monthOfYear + 1), dayOfMonth);
                        Calendar minAdultAge = new GregorianCalendar();
                        minAdultAge.add(Calendar.YEAR, -18);
                        String formattedDay, formattedMonth;
                        if (minAdultAge.before(userAge)) {
                            etRegisterDob.setError("You must be at least 18 years old to Register");
                            return;
                        } else {
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }

                            etRegisterDob.setText(String.valueOf(year) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(formattedDay));

                            etRegisterDob.setError(null);
                        }
                    }
                }, 1990, 00, 01);
                dialogDate.setButton(DatePickerDialog.BUTTON_POSITIVE, getResources().getString(R.string.zxing_button_ok), dialogDate);
                dialogDate.setButton(DatePickerDialog.BUTTON_NEGATIVE, getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dialogDate.getDatePicker().setMaxDate(new Date().getTime());
                dialogDate.show();
            }
        });

        fabRegisterSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        //DONE CLICK ON VIRTUAL KEYPAD
        etRegisterEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitForm();
                }
                return false;
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {

        if (!validateFName()) {
            return;
        }
        if (!validateLName()) {
            return;
        }
        if (!validateGender()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }
        if (!validateDob()) {
            return;
        }
//        if (!validateIdno()) {
//            return;
//        }
//
//        if (!validateValues(spinner_govtid_provider.getSelectedItemPosition())) {
//            return;
//        }


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        firstName = etRegisterFirstName.getText().toString();
        newPassword = etRegisterPassword.getText().toString();
        email = etRegisterEmail.getText().toString();
//    if (isSensorAvialable()) {
//      if (status) {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View viewDialog = inflater.inflate(R.layout.dialog_finger_print, null, false);
//        builder.setCancelable(false);
//        builder.setTitle("Touch ID for Centrum PAy ");
//        builder.setView(viewDialog);
//        final AlertDialog alertDialog = builder.create();
//        final TextView fingerprint_description = (TextView) viewDialog.findViewById(R.id.fingerprint_status);
//        Button button = (Button) viewDialog.findViewById(R.id.cancel);
//        button.setOnClickListener(new View.OnClickListener() {
//          @Override
//          public void onClick(View view) {
//            alertDialog.dismiss();
//          }
//        });
//
//        cryptoObject = new FingerprintManager.CryptoObject(cipher);
//
//        FingerprintHandler helper = new FingerprintHandler(RegisterActivity.this, new FingerprintHandler.FingerprintHelperListener() {
//          @Override
//          public void authenticationFailed(String error) {
//            fingerprint_description.setText(error);
//            fingerprint_description.setTextColor(Color.parseColor("#FF1732"));
//
//          }
//
//          @Override
//          public void authenticationSucceeded(FingerprintManager.AuthenticationResult result) {
//
//            fingerprint_description.setText("Fingerprint Verified");
//            fingerprintStatus = result.getCryptoObject().getCipher().getIV();
//            loadDlg.show();
//            promoteRegister(fingerprintStatus);
//            alertDialog.dismiss();
//
//          }
//        });
//
//        helper.startAuth(fingerprintManager, cryptoObject);
//
//        alertDialog.show();
//      }
//    } else {
        loadDlg.show();
        promoteRegister(null);
//    }

    }

    private void showGender() {
        final CharSequence[] year = {"Male", "Female"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("Select Gender");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setItems(year, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                etGender.setText(year[i]);
                etGender.setError(null);

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private boolean validateDob() {
        if (etRegisterDob.getText().toString().trim().isEmpty()) {
            CustomToast.showMessage(getApplicationContext(),"Enter Date of birth");
            requestFocus(etRegisterDob);
            return false;
        } else {
            etRegisterDob.setError(null);
        }

        return true;
    }

//    private boolean validateIdno() {
//        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 11) {
//            etGovtIdNo.setError("Enter valid Id number");
//            requestFocus(etGovtIdNo);
//            return false;
//        } else {
//            etGovtIdNo.setError(null);
//        }
//
//        return true;
//    }

    private boolean validateKTP() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length()!=16) {
            etGovtIdNo.setError("Enter 16 digit KTP number");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

    private boolean validatedl() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 10) {
            etGovtIdNo.setError("Enter valid Driving License");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

    private boolean validatepancard() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() != 10) {
            etGovtIdNo.setError("Enter valid Pan Card number");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

    private boolean validatepassport() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 8) {
            etGovtIdNo.setError("Enter valid passport number");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

    private boolean validateFName() {
        if (etRegisterFirstName.getText().toString().trim().isEmpty()) {
            etRegisterFirstName.setError("Enter first name");
            requestFocus(etRegisterFirstName);
            return false;
        } else {
            etRegisterFirstName.setError(null);
        }

        return true;
    }

    private boolean validateLName() {
        if (etLasttNameAns.getText().toString().trim().isEmpty()) {
            etLasttNameAns.setError("Enter Last name");
            requestFocus(etLasttNameAns);
            return false;
        } else {
            etLasttNameAns.setError(null);
        }

        return true;
    }

    private boolean validateGender() {
        if (etGender.getText().toString().trim().isEmpty()) {
            CustomToast.showMessage(getApplicationContext(),"Enter Gender");
            requestFocus(etGender);
            return false;
        } else {
            etGender.setError(null);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = etRegisterEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etRegisterEmail.setError("Enter valid email");
            requestFocus(etRegisterEmail);
            return false;
        } else {
            etRegisterEmail.setError(null);
        }
        return true;
    }

    private boolean validatePasswordListner() {
        if (etRegisterPassword.getText().toString().trim().isEmpty() || etRegisterPassword.getText().toString().trim().length() < 6) {
            etRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
            etRegisterPassword.setError(null);
            checkPwdStr();
        }

        return true;
    }

    private String checkPwdStr() {
        if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else {
            etRegisterPassword.setError("Weak Password");
        }
        return null;
    }

    private boolean validateValues(int postion) {
        if (postion == 0) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(16);
            etGovtIdNo.setFilters(FilterArray);
            return validateKTP();
        } else if (postion == 2) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(12);
            etGovtIdNo.setFilters(FilterArray);
            return validateKTP();
        } else if (postion == 3) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(10);
            etGovtIdNo.setFilters(FilterArray);
            return validatepancard();
        } else if (postion == 4) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(16);
            etGovtIdNo.setFilters(FilterArray);
            return validatepassport();
        }
        return true;
    }

    private boolean validatePassword() {
        if (etRegisterPassword.getText().toString().isEmpty()) {
            etRegisterPassword.setError("Check the password");
            requestFocus(etRegisterPassword);
            return false;
        } else if (etRegisterPassword.getText().toString().length() < 6) {
            etRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
            etRegisterPassword.setError(null);
        }
        return true;
    }

    private boolean validateIdno() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 8 || etGovtIdNo.getText().toString().trim().length() > 16) {
            etGovtIdNo.setError("Enter valid ID no.");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private String getGender(){
        String gender;
        if(etGender.getText().toString().equalsIgnoreCase("male")){
            gender="M";
        }else {
            gender="F";
        }
        return gender;
    }

    private void promoteRegister(byte[] fingerprintStatus) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("firstName", etRegisterFirstName.getText().toString());
            jsonRequest.put("lastName", etLasttNameAns.getText().toString());
            jsonRequest.put("gender", getGender());
            jsonRequest.put("contactNo", mobileNo);
            jsonRequest.put("password", etRegisterPassword.getText().toString());
            jsonRequest.put("email", etRegisterEmail.getText().toString());
//            jsonRequest.put("gender", "NA");
            jsonRequest.put("dateOfBirth", etRegisterDob.getText().toString());
//            jsonRequest.put("country", cntry);
//            jsonRequest.put("operatingSystem", "Android");

//            if (!etGovtIdNo.getText().toString().trim().isEmpty()) {
//                jsonRequest.put("idType", ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).getName());
//                jsonRequest.put("idNumber", etGovtIdNo.getText().toString());
//            } else {
//                jsonRequest.put("idType", "");
//                jsonRequest.put("idNumber", "");
//            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Registration API", ApiUrl.URL_REGISTRATION);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REGISTRATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {

                            Toast.makeText(RegisterActivity.this, messageAPI, Toast.LENGTH_SHORT).show();
                            loadDlg.dismiss();
                            Intent otpIntent = new Intent(RegisterActivity.this, OtpVerificationActivity.class);
                            otpIntent.putExtra("userMobileNo",  mobileNo);
                            otpIntent.putExtra("intentType", "Register");
                            startActivityForResult(otpIntent, 0);
                            finish();

                        } else {
                            Log.i("ERROR MESSAGE", messageAPI);

//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
//                            if(jsonObject.has("details")) {
//                                JSONObject jsonErrorDetail = jsonObject.getJSONObject("details");
//                                String errorFName = null, errorLName = null, errorMobile = null, errorPassword = null, errorRePassword = null, errorEmail = null;
//                                if (jsonErrorDetail.has("firstName")&&jsonErrorDetail.getString("firstName")!=null) {
//                                    errorFName = jsonErrorDetail.getString("firstName");
//                                }
//                                if (jsonErrorDetail.has("lastName")&&jsonErrorDetail.getString("lastName")!=null) {
//                                    errorLName = jsonErrorDetail.getString("lastName");
//                                }
//                                if (jsonErrorDetail.has("contactNo")&&jsonErrorDetail.getString("contactNo")!=null) {
//                                    errorMobile = jsonErrorDetail.getString("contactNo");
//                                }
//                                if (jsonErrorDetail.has("password")&&jsonErrorDetail.getString("password")!=null) {
//                                    errorPassword = jsonErrorDetail.getString("password");
//                                }
//                                if (jsonErrorDetail.has("confirmPassword")&&jsonErrorDetail.getString("confirmPassword")!=null) {
//                                    errorRePassword = jsonErrorDetail.getString("confirmPassword");
//                                }
//                                if (jsonErrorDetail.has("email")&&jsonErrorDetail.getString("email")!=null) {
//                                    errorEmail = jsonErrorDetail.getString("email");
//                                }
//
//                                if (errorFName != null) {
//                                    Toast.makeText(RegisterActivity.this, errorFName, Toast.LENGTH_SHORT).show();
//
//                                } else if (errorLName != null) {
//                                    Toast.makeText(RegisterActivity.this, errorLName, Toast.LENGTH_SHORT).show();
//
//                                } else if (errorMobile != null) {
//                                    Toast.makeText(RegisterActivity.this, errorMobile, Toast.LENGTH_SHORT).show();
//
//                                } else if (errorPassword != null) {
//                                    Toast.makeText(RegisterActivity.this, errorPassword, Toast.LENGTH_SHORT).show();
//
//                                } else if (errorRePassword != null) {
//                                    Toast.makeText(RegisterActivity.this, errorRePassword, Toast.LENGTH_SHORT).show();
//
//                                } else if (errorEmail != null) {
//                                    Toast.makeText(RegisterActivity.this, errorEmail, Toast.LENGTH_SHORT).show();
//
//                                }
//                            }
//                            else{

                            if (response.has("details")) {
                                if (!response.isNull("details")) {
                                    String messageDetail = response.getString("details");
                                    CustomToast.showMessage(RegisterActivity.this, messageDetail);
                                } else {
                                    CustomToast.showMessage(RegisterActivity.this, messageAPI);
                                }
                            }

                            loadDlg.dismiss();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(RegisterActivity.this, getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(RegisterActivity.this, NetworkErrorHandler.getMessage(error, RegisterActivity.this));
                    loadDlg.dismiss();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() throws FingerprintException {
        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");


            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
//            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | UnrecoverableKeyException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private boolean isSensorAvialable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED &&
                    getSystemService(FingerprintManager.class).isHardwareDetected();
        } else {
            return FingerprintManagerCompat.from(RegisterActivity.this).isHardwareDetected();
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etRegisterEmail) {
                validateEmail();
            } else if (i == R.id.etRegisterPassword) {
                validatePasswordListner();
            } else if (i == R.id.etLasttNameAns) {
                validateLName();
            } else if (i==R.id.etGovtIdNo){
                if (!validateValues(spinner_govtid_provider.getSelectedItemPosition())) {
                    return;
                }
            }

        }
    }

    private class FingerprintException extends Exception {

        public FingerprintException(Exception e) {
            super(e);
        }

    }

    public ArrayList<Country> loadJSONFromAsset() {
        ArrayList<Country> locList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getAssets().open("country.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONArray obj = new JSONArray(json);

            for (int i = 0; i < obj.length(); i++) {
                JSONObject jo_inside = obj.getJSONObject(i);
                Country location = new Country(jo_inside.getString("name"), jo_inside.getString("callingCode"), jo_inside.getString("code"));
                locList.add(location);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locList;
    }
}
