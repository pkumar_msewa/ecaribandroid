package in.msewa.ecarib.activity.businneractivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.msewa.adapter.BusListAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BusBoardingPointModel;
import in.msewa.model.BusListModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class BusListActivity extends AppCompatActivity {

  private ListView lvListBus;
  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private LoadingDialog loadingDialog;
  private long destinationCode, sourceCode;
  private String dateOfDep;

  private String sourceName = "", destinationName = "";

  //Volley
  private String tag_json_obj = "json_travel";
  private LinearLayout llNoBus;
  private List<BusListModel> busItem;
  private ArrayList<BusBoardingPointModel> boardingPointList;
  private ArrayList<BusBoardingPointModel> droppingPointList;
  private ArrayList<BusBoardingPointModel> totboardingPointList;
  private ArrayList<BusBoardingPointModel> totdroppingPointList;
  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;

  //Sort
  private TextView itemOne, itemTwo, itemThree, itemFive, itemFour, tvArrival, tvDeparture, tvPrice;
  private Button submit;
  AlertDialog alertDialog;
  private static String selectValue;
  private BusListAdapter busTyeAdapter;
  private LinearLayout llSort, LLSortFilter, llArrival, llDeparture, llPrice, llFilter;
  private ImageView ivArrSort, ivDepSort, ivPriceSort;
  private boolean arrcheck = true;
  private boolean depcheck = true;
  private boolean pricecheck = true;

  //filter
  private SearchableSpinner spBoarding, spDropping;
  //    private MaterialEditText spBoarding;
  private RadioGroup rgSeatType, rgAC;
  private RadioButton rbSeater, rbSleeper, rbBoth, rbAC, rbNonAC;
  Button btnFilter, btnResetFilter;
  private ArrayList<String> boardingPoints;
  private ArrayList<String> droppingPoints;
  private ArrayAdapter boardingAdapter, droppingAdapter;
  private String selectedBoardingCode, selectedDroppingCode;
  private BusBoardingPointModel busboardingmodel, destionBoardingModel;
  String seatType;

  boolean ac, seater = false, sleaper = false;
  List<BusListModel> filterlist = new ArrayList<BusListModel>();
  private ArrayList<Object> objectArrayList;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_bus_list);
    loadingDialog = new LoadingDialog(BusListActivity.this);
    llNoBus = (LinearLayout) findViewById(R.id.llNoBus);
    llSort = (LinearLayout) findViewById(R.id.llSort);
    LLSortFilter = (LinearLayout) findViewById(R.id.LLSortFilter);
    //Sort
    llFilter = (LinearLayout) findViewById(R.id.llFilter);
    llArrival = (LinearLayout) findViewById(R.id.llArrival);
    tvArrival = (TextView) findViewById(R.id.tvArrival);
    ivArrSort = (ImageView) findViewById(R.id.ivArrSort);
    llDeparture = (LinearLayout) findViewById(R.id.llDeparture);
    tvDeparture = (TextView) findViewById(R.id.tvDeparture);
    ivDepSort = (ImageView) findViewById(R.id.ivDepSort);
    llPrice = (LinearLayout) findViewById(R.id.llPrice);
    tvPrice = (TextView) findViewById(R.id.tvPrice);
    ivPriceSort = (ImageView) findViewById(R.id.ivPriceSort);
    //Sort End
    destinationCode = getIntent().getLongExtra("destination", 0);
    sourceCode = getIntent().getLongExtra("source", 0);
    dateOfDep = getIntent().getStringExtra("date");
    sourceName = getIntent().getStringExtra("sourceName");
    destinationName = getIntent().getStringExtra("destinationName");

    lvListBus = (ListView) findViewById(R.id.lvListBus);
    //press back button in toolbar
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    getBusLists();
    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("booking-done"));
//        busItem = Select.from(BusListModel.class).list();
    busItem = new ArrayList<>();
    llFilter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showFilterDialog();
      }
    });

    llArrival.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (arrcheck) {
          tvArrival.setTextColor(getResources().getColor(R.color.colorPrimary));
          ivArrSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrival_active));
          tvDeparture.setTextColor(getResources().getColor(R.color.white_text));
          ivDepSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_departure));
          tvPrice.setTextColor(getResources().getColor(R.color.white_text));
          ivPriceSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
          if (filterlist.isEmpty()) {
            getLowToHighArr(busItem);
          } else {
            getLowToHighArr(filterlist);
          }

          depcheck = true;
          pricecheck = true;
        } else {
          tvArrival.setTextColor(getResources().getColor(R.color.white_text));
          ivArrSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrival));
          if (filterlist.isEmpty()) {
            getHighToLowHighArr(busItem);
          } else {
            getHighToLowHighArr(filterlist);
          }
        }

      }
    });

    llDeparture.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (depcheck) {
          tvDeparture.setTextColor(getResources().getColor(R.color.colorPrimary));
          ivDepSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_departure_active));
          tvArrival.setTextColor(getResources().getColor(R.color.white_text));
          ivArrSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrival));
          tvPrice.setTextColor(getResources().getColor(R.color.white_text));
          ivPriceSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
          if (filterlist.isEmpty()) {
            getLowToHighDep(busItem);
          } else {
            getLowToHighDep(filterlist);
          }
          arrcheck = true;
          pricecheck = true;
        } else {
          tvDeparture.setTextColor(getResources().getColor(R.color.white_text));
          ivDepSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_departure));
          if (filterlist.isEmpty()) {
            getHighToLowHighDep(busItem);
          } else {
            getHighToLowHighDep(filterlist);
          }
        }

      }
    });
    llPrice.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (pricecheck) {
          tvPrice.setTextColor(getResources().getColor(R.color.colorPrimary));
          ivPriceSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_price_active));
          tvArrival.setTextColor(getResources().getColor(R.color.white_text));
          ivArrSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrival));
          tvDeparture.setTextColor(getResources().getColor(R.color.white_text));
          ivDepSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_departure));
          if (filterlist.isEmpty()) {
            getLowToHigh(busItem);
          } else {
            getLowToHigh(filterlist);
          }
          arrcheck = true;
          depcheck = true;
        } else {
          tvPrice.setTextColor(getResources().getColor(R.color.white_text));
          ivPriceSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
          if (filterlist.isEmpty()) {
            getHighToLow(busItem);
          } else {
            getHighToLow(filterlist);
          }
        }

      }
    });
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("ticket");
      if (action.equals("1")) {
        finish();
      }

    }
  };


  public void getBusLists() {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("date", dateOfDep);
      jsonRequest.put("sourceId", sourceCode);
      jsonRequest.put("destinationId", destinationCode);
//            jsonRequest.put("returnDate", "");


    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest Bus", jsonRequest.toString());
      Log.i("URL Bus", ApiUrl.URL_GET_LIST_BUS_NEW);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_LIST_BUS_NEW, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("BUs Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {

              JSONObject detailsObj = response.getJSONObject("details");
              JSONArray jsonArray = detailsObj.getJSONArray("availableTripsDTOs");
              objectArrayList = new ArrayList<>();
              if (jsonArray.length() != 0) {
                busItem.clear();

//                                JSONArray totBdPoints = detailsObj.getJSONArray("bdPoints");
//                                if (totBdPoints.length() != 0) {
//                                    boardingPoints = new ArrayList<>();
//                                    totboardingPointList = new ArrayList<>();
//                                    BusBoardingPointModel all = new BusBoardingPointModel("ALL", "", "", "");
//                                    totboardingPointList.add(all);
//                                    boardingPoints.add("All");
//                                    for (int j = 0; j < totBdPoints.length(); j++) {
//                                        JSONObject d = totBdPoints.getJSONObject(j);
//                                        String boardingName = d.getString("bdLongName");
//                                        String boardingTime = d.getString("time");
//                                        String boardingId = d.getString("bdid");
//                                        String boardingTimePrime = d.getString("prime");
//                                        boardingPoints.add(boardingName);
//                                        BusBoardingPointModel busBoardingPointModel = new BusBoardingPointModel(boardingName, boardingTime, boardingId, boardingTimePrime);
//                                        totboardingPointList.add(busBoardingPointModel);
//                                    }
//                                }
//                                JSONArray totDPoints = detailsObj.getJSONArray("droppingPoints");
//                                if (totDPoints.length() != 0) {
//                                    droppingPoints = new ArrayList<>();
//                                    totdroppingPointList = new ArrayList<>();
//                                    BusBoardingPointModel all = new BusBoardingPointModel("ALL", "", "", "");
//                                    totdroppingPointList.add(all);
//                                    droppingPoints.add("All");
//                                    for (int j = 0; j < totDPoints.length(); j++) {
//                                        JSONObject d = totDPoints.getJSONObject(j);
//                                        String boardingName = d.getString("dpName");
//                                        String boardingTime = d.getString("dpTime");
//                                        String boardingId = d.getString("dpId");
//                                        String boardingTimePrime = d.getString("prime");
//                                        droppingPoints.add(boardingName);
//                                        BusBoardingPointModel busBoardingPointModel = new BusBoardingPointModel(boardingName, boardingTime, boardingId, boardingTimePrime);
//                                        totdroppingPointList.add(busBoardingPointModel);
//                                    }
//                                }

                for (int i = 0; i < jsonArray.length(); i++) {
                  JSONObject c = jsonArray.getJSONObject(i);
//                                    if (c.getInt("engineId") != 3) {
                  String busTravelName = c.getString("travels");
                  String busAvailableSeat = c.getString("availableSeats");
                  String busArrivalTime = c.getString("arrivalTime");
                  String depTime = c.getString("departureDate");
//                                    String[] depTimeArray = depTime.trim().split("T");
                  String busDepartureTime = c.getString("departureTime");
                  String busType = c.getString("busType");
                  String busDuration = c.getString("duration");
                  String busFare = c.getString("price");
                  String tripId = c.getString("id");
                  String busProvider = "";
                  String busTravel = c.getString("travels");
                  String busConvenienceFee = "";
                  String busPartialCancellation = "";
                  String cancelPolicyListDTO = "";
                  if (!c.isNull("cancelPolicyListDTO")) {
                    cancelPolicyListDTO = String.valueOf(c.getJSONArray("cancelPolicyListDTO"));
                  } else {
                    cancelPolicyListDTO = "";
                  }
                  String jnrDate = c.getString("doj");
                  int engineId = c.getInt("engineId");
                  boolean seater = c.getBoolean("seater");
                  boolean sleeper = c.getBoolean("sleeper");
                  String routeId = c.getString("routeId");
                  double discount = c.getDouble("discount");
                  double commission = c.getDouble("commission");
                  double markup = c.getDouble("markup");
                  boolean ac = c.getBoolean("ac");
                  boolean nonAC = c.getBoolean("nonAC");
                  String bpId = "", dpId = "";
                  if (c.isNull("bpId")) {
                    bpId = c.getString("bpId");
                  }
                  if (c.isNull("dpId")) {
                    dpId = c.getString("dpId");
                  }
                  Boolean bpDpLayout = c.getBoolean("bpDpLayout");

                  boardingPoints = new ArrayList<>();
                  droppingPoints = new ArrayList<>();
                  boardingPointList = new ArrayList<>();
                  JSONArray jsonBoarding = c.getJSONArray("bdPointDTO");
                  if (jsonBoarding.length() != 0) {
                    for (int j = 0; j < jsonBoarding.length(); j++) {
                      JSONObject d = jsonBoarding.getJSONObject(j);
                      String boardingName = d.getString("bdLongName");
                      String boardingTime = d.getString("time");
                      String boardingId = d.getString("bdid");
                      String boardingTimePrime = d.getString("prime");
                      boardingPoints.add(boardingName);
                      BusBoardingPointModel busBoardingPointModel = new BusBoardingPointModel(boardingName, boardingTime, boardingId, boardingTimePrime, tripId, seater, sleeper, ac, "boardingPoint");
                      objectArrayList.add(busBoardingPointModel);
                      boardingPointList.add(busBoardingPointModel);
                    }
                  }
                  filterlist.clear();
                  droppingPointList = new ArrayList<>();
                  JSONArray jsonDropping = c.getJSONArray("dpPointsDTO");
                  if (jsonDropping.length() != 0) {
                    for (int k = 0; k < jsonDropping.length(); k++) {
                      JSONObject e = jsonDropping.getJSONObject(k);
                      String boardingName = e.getString("dpName");
                      String boardingTime = e.getString("dpTime");
                      String boardingId = e.getString("dpId");
                      String boardingTimePrime = e.getString("prime");
                      droppingPoints.add(boardingName);
                      BusBoardingPointModel busBoardingPointModel = new BusBoardingPointModel(boardingName, boardingTime, boardingId, boardingTimePrime, tripId, seater, sleeper, ac, "droppingPoint");
                      objectArrayList.add(busBoardingPointModel);
                      droppingPointList.add(busBoardingPointModel);
                    }
                  }

                  BusListModel busModel = new BusListModel(busTravelName, busDepartureTime, busArrivalTime, busType, busFare, busAvailableSeat, busDuration, busProvider, tripId, busTravel, boardingPointList, droppingPointList, sourceName, destinationName, busConvenienceFee, cancelPolicyListDTO, busPartialCancellation, jnrDate, engineId, seater, sleeper, routeId, discount, commission, markup, ac, nonAC, bpId, dpId, bpDpLayout);
                  busItem.add(busModel);

//                                    }
                }
                if (!busItem.isEmpty() && busItem.size() != 0) {
                  LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
                  View dateView = layoutInflater.inflate(R.layout.header_bus_list, null);
                  TextView tvHeaderBusListDate = (TextView) dateView.findViewById(R.id.tvHeaderBusListDate);
                  TextView tvSourceCity = (TextView) dateView.findViewById(R.id.tvSourceCity);
                  TextView tvDestCity = (TextView) dateView.findViewById(R.id.tvDestCity);
                  tvSourceCity.setText(sourceName);
                  tvDestCity.setText(destinationName);
                  String[] splitDate = dateOfDep.split("-");

                  tvHeaderBusListDate.setText(splitDate[0] + " " + getMonth(Integer.valueOf(splitDate[1])) + " " + splitDate[2]);
                  lvListBus.addHeaderView(dateView);

                  busTyeAdapter = new BusListAdapter(BusListActivity.this, busItem, destinationCode, sourceCode, dateOfDep);
                  lvListBus.setAdapter(busTyeAdapter);
//                                    fbSort.setVisibility(View.VISIBLE);
                  loadingDialog.dismiss();
                } else {
                  loadingDialog.dismiss();
                  llNoBus.setVisibility(View.VISIBLE);
                  lvListBus.setVisibility(View.GONE);
                  LLSortFilter.setVisibility(View.GONE);
                }
              } else {
                loadingDialog.dismiss();
                llNoBus.setVisibility(View.VISIBLE);
                lvListBus.setVisibility(View.GONE);
                LLSortFilter.setVisibility(View.GONE);
              }
            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog(message);
            } else {
              loadingDialog.dismiss();
              CustomToast.showMessage(BusListActivity.this, message);

            }
          } catch (JSONException e) {
            e.printStackTrace();
            CustomToast.showMessage(getApplicationContext(), "Oops, something went wrong. Please after sometime");
            finish();
            loadingDialog.dismiss();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          error.printStackTrace();
          finish();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };

      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public String getMonth(int month) {
    return new DateFormatSymbols().getMonths()[month - 1];
  }


  public String getDay(int day) {
    return new DateFormatSymbols().getWeekdays()[day + 1];
  }

  public void getLowToHigh(List<BusListModel> busArray) {
    if (busArray != null && busArray.size() != 0) {
      Collections.sort(busArray, new Comparator<BusListModel>() {
        @Override
        public int compare(BusListModel lhs, BusListModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
          return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
        }
      });
      try {
        busTyeAdapter.notifyDataSetChanged();
        pricecheck = false;
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }

  public void getHighToLow(List<BusListModel> busArray) {
    if (busArray != null && busArray.size() != 0) {
      Collections.sort(busArray, new Comparator<BusListModel>() {
        @Override
        public int compare(BusListModel lhs, BusListModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
          return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? -1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? 1 : 0;
        }
      });
      try {
        busTyeAdapter.notifyDataSetChanged();
        pricecheck = true;
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }

  public void getLowToHighArr(List<BusListModel> busArray) {
    if (busArray != null && busArray.size() != 0) {
      Collections.sort(busArray, new Comparator<BusListModel>() {
        @Override
        public int compare(BusListModel lhs, BusListModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
          return parseDate(lhs.getBusArrivalTime()).after(parseDate(rhs.getBusArrivalTime())) ? 1 : (parseDate(lhs.getBusArrivalTime()).before(parseDate(rhs.getBusArrivalTime()))) ? -1 : 0;
        }
      });
      try {
        busTyeAdapter.notifyDataSetChanged();
        arrcheck = false;
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }

  public void getHighToLowHighArr(List<BusListModel> busArray) {
    if (busArray != null && busArray.size() != 0) {
      Collections.sort(busArray, new Comparator<BusListModel>() {
        @Override
        public int compare(BusListModel lhs, BusListModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
          return parseDate(lhs.getBusArrivalTime()).after(parseDate(rhs.getBusArrivalTime())) ? -1 : (parseDate(lhs.getBusArrivalTime()).before(parseDate(rhs.getBusArrivalTime()))) ? 1 : 0;
        }
      });
      try {
        busTyeAdapter.notifyDataSetChanged();
        arrcheck = true;
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }

  public void getLowToHighDep(List<BusListModel> busArray) {
    if (busArray != null && busArray.size() != 0) {
      Collections.sort(busArray, new Comparator<BusListModel>() {
        @Override
        public int compare(BusListModel lhs, BusListModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
          return parseDate(lhs.getBusDepTime()).after(parseDate(rhs.getBusDepTime())) ? 1 : (parseDate(lhs.getBusDepTime()).before(parseDate(rhs.getBusDepTime()))) ? -1 : 0;
        }
      });
      try {
        busTyeAdapter.notifyDataSetChanged();
        depcheck = false;
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }

  public void getHighToLowHighDep(List<BusListModel> busArray) {
    if (busArray != null && busArray.size() != 0) {
      Collections.sort(busArray, new Comparator<BusListModel>() {
        @Override
        public int compare(BusListModel lhs, BusListModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
          return parseDate(lhs.getBusDepTime()).after(parseDate(rhs.getBusDepTime())) ? -1 : (parseDate(lhs.getBusDepTime()).before(parseDate(rhs.getBusDepTime()))) ? 1 : 0;
        }
      });
      try {
        busTyeAdapter.notifyDataSetChanged();
        depcheck = true;
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }


  public void showFilterDialog() {

    LayoutInflater myLayout = LayoutInflater.from(BusListActivity.this);
    View dialogView = myLayout.inflate(R.layout.dailog_bus_filter, null);

    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
      BusListActivity.this);
    alertDialogBuilder.setView(dialogView);

    // create alert dialog
    alertDialog = alertDialogBuilder.create();
    spBoarding = (SearchableSpinner) dialogView.findViewById(R.id.spBoarding);
    spDropping = (SearchableSpinner) dialogView.findViewById(R.id.spDropping);
    rgSeatType = (RadioGroup) dialogView.findViewById(R.id.rgSeatType);
    rgAC = (RadioGroup) dialogView.findViewById(R.id.rgAC);
    rbSeater = (RadioButton) dialogView.findViewById(R.id.rbSeater);
    rbSleeper = (RadioButton) dialogView.findViewById(R.id.rbSleeper);
    rbBoth = (RadioButton) dialogView.findViewById(R.id.rbBoth);
    rbAC = (RadioButton) dialogView.findViewById(R.id.rbAC);
    rbNonAC = (RadioButton) dialogView.findViewById(R.id.rbNonAC);
    btnFilter = (Button) dialogView.findViewById(R.id.btnFilter);
    btnResetFilter = (Button) dialogView.findViewById(R.id.btnResetFilter);
    spBoarding.setTitle("Select Boarding Location");
    spDropping.setTitle("Select Dropping Location");
    ImageView ivFilterClose = (ImageView) dialogView.findViewById(R.id.ivFilterClose);

    ivFilterClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        alertDialog.dismiss();
      }
    });


    boardingAdapter = new ArrayAdapter(BusListActivity.this, R.layout.support_simple_spinner_dropdown_item, boardingPoints);
    spBoarding.setAdapter(boardingAdapter);
    spBoarding.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedBoardingCode = boardingPointList.get(position).getBoardingId();
      }


      @Override
      public void onNothingSelected(AdapterView<?> parent) {
        selectedBoardingCode = "";
      }
    });
    droppingAdapter = new ArrayAdapter(BusListActivity.this, R.layout.support_simple_spinner_dropdown_item, droppingPoints);
    spDropping.setAdapter(droppingAdapter);
    spDropping.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedDroppingCode = droppingPointList.get(position).getBoardingId();

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {
        selectedDroppingCode = "";
      }
    });

    btnResetFilter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        filterlist.clear();
        busTyeAdapter = new BusListAdapter(BusListActivity.this, busItem, destinationCode, sourceCode, dateOfDep);
        lvListBus.setAdapter(busTyeAdapter);
        alertDialog.dismiss();
      }
    });

    btnFilter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        filterlist.clear();
        int checkedRadioButtonId = rgSeatType.getCheckedRadioButtonId();
        switch (checkedRadioButtonId) {
          case R.id.rbSleeper:
            seatType = "sleeper";
            sleaper = true;
            break;
          case R.id.rbSeater:
            seatType = "seater";
            seater = true;
            break;
          case R.id.rbBoth:
            seatType = "both";
            sleaper = true;
            seater = true;
            break;
        }
        int checkedRadioButtonId1 = rgAC.getCheckedRadioButtonId();
        switch (checkedRadioButtonId1) {
          case R.id.rbNonAC:
            ac = false;
            break;
          case R.id.rbAC:
            ac = true;
            break;
        }
        filter(selectedBoardingCode, selectedDroppingCode, seatType, ac);
        alertDialog.dismiss();

      }
    });

    // show it
    alertDialog.show();
    alertDialog.setCancelable(true);
  }

//    private BusListModel filter2(String seatType, BusListModel bus, boolean ac) {
//        BusListModel busListModel = null;
//        try {
//            if (seatType.equalsIgnoreCase("both")) {
//                if (ac) {
//                    if (bus.isSleeper() && bus.isSeater() && bus.isAc()) {
//                        busListModel = bus;
//                    }
//                } else if (!ac) {
//                    if (bus.isSleeper() && bus.isSeater() && !bus.isAc()) {
//                        busListModel = bus;
//                    }
//                }
//            }
//            if (seatType.equalsIgnoreCase("sleeper")) {
//                if (ac) {
//                    if (bus.isSleeper() && bus.isAc()) {
//                        Log.i("slepperAC", String.valueOf(bus.isAc()));
//                        busListModel = bus;
//                    }
//
//                } else if (!ac) {
//                    if (bus.isSleeper() && !bus.isAc()) {
//                        Log.i("slepperNON", String.valueOf(ac));
//                        busListModel = bus;
//                    }
//                }
//            }
//            if (seatType.equalsIgnoreCase("Seater")) {
//                if (ac) {
//                    if (bus.isSeater() && bus.isAc()) {
//                        Log.i("seaterAC", String.valueOf(ac));
//                        busListModel = bus;
//                    }
//                }
//                if (!ac) {
//                    if (bus.isSeater() && bus.isNonAC()) {
//                        Log.i("seaterNON-AC", String.valueOf(ac));
//                        busListModel = bus;
//                    }
//                }
//            }
//            return busListModel;
//        } catch (IndexOutOfBoundsException e) {
//            return busListModel;
//            e.printStackTrace();
//        }
//        return busListModel;
//    }


  private void filter(String boarding, String dropping, String seatType, boolean ac) {


    if (boarding.equals("") && dropping.equals("") && seatType.equals("seater") && ac) {
      filterlist.clear();
      for (int i = 0; i < busItem.size(); i++) {
        if (busItem.get(i).isSeater() && !busItem.get(i).isSleeper() && busItem.get(i).isAc()) {
          filterlist.add(busItem.get(i));
        }
      }
    } else if (boarding.equals("") && dropping.equals("") && seatType.equals("sleeper") && ac) {
      filterlist.clear();
      for (int i = 0; i < busItem.size(); i++) {
        if (busItem.get(i).isSleeper() && !busItem.get(i).isSeater() && busItem.get(i).isAc()) {
          filterlist.add(busItem.get(i));

        }
      }
    } else if (boarding.equals("") && dropping.equals("") && seatType.equals("both") && ac) {
      filterlist.clear();
      for (int i = 0; i < busItem.size(); i++) {
        if (busItem.get(i).isSleeper() && busItem.get(i).isSeater() && busItem.get(i).isAc()) {
          filterlist.add(busItem.get(i));
        }
      }
    }
    if (boarding.equals("") && dropping.equals("") && seatType.equals("seater") && !ac) {
      filterlist.clear();
      for (int i = 0; i < busItem.size(); i++) {
        if (busItem.get(i).isSeater() && !busItem.get(i).isSleeper() && busItem.get(i).isNonAC()) {
          filterlist.add(busItem.get(i));
        }
      }
    } else if (boarding.equals("") && dropping.equals("") && seatType.equals("sleeper") && !ac) {
      filterlist.clear();
      for (int i = 0; i < busItem.size(); i++) {
        if (busItem.get(i).isSleeper() && !busItem.get(i).isSeater() && busItem.get(i).isNonAC()) {
          filterlist.add(busItem.get(i));
        }
      }
    } else if (boarding.equals("") && dropping.equals("") && seatType.equals("both") && !ac) {
      filterlist.clear();
      for (int i = 0; i < busItem.size(); i++) {
        if (busItem.get(i).isSleeper() && busItem.get(i).isSeater() && busItem.get(i).isNonAC()) {
          filterlist.add(busItem.get(i));
        }
      }
    } else if (!boarding.equals("") && dropping.equals("") && seatType.equals("seater") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            if (busListModel.isSeater() && !busListModel.isSleeper() && busListModel.isAc()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (!boarding.equals("") && dropping.equals("") && seatType.equals("sleeper") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            if (!busListModel.isSeater() && busListModel.isSleeper() && busListModel.isAc()) {
              filterlist.add(busListModel);
            }
          }
        }
      }

    } else if (!boarding.equals("") && dropping.equals("") && seatType.equals("both") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            if (busListModel.isSeater() && busListModel.isSleeper() && busListModel.isAc()) {
              filterlist.add(busListModel);
            }
          }
        }
      }

    }
    if (!boarding.equals("") && dropping.equals("") && seatType.equals("seater") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            if (busListModel.isSeater() && !busListModel.isSleeper() && busListModel.isNonAC()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (!boarding.equals("") && dropping.equals("") && seatType.equals("sleeper") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            if (!busListModel.isSeater() && busListModel.isSleeper() && busListModel.isNonAC()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (!boarding.equals("") && dropping.equals("") && seatType.equals("both") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            if (busListModel.isSeater() && busListModel.isSleeper() && busListModel.isNonAC()) {
              filterlist.add(busListModel);
            }
          }
        }
      }

    }


    if (boarding.equals("") && !dropping.equals("") && seatType.equals("seater") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusDroppingPointModels()) {
          if (listModel.getBoardingId().equals(dropping)) {
            if (busListModel.isSeater() && !busListModel.isSleeper() && busListModel.isAc()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (boarding.equals("") && !dropping.equals("") && seatType.equals("sleeper") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusDroppingPointModels()) {
          if (listModel.getBoardingId().equals(dropping)) {
            if (!busListModel.isSeater() && busListModel.isSleeper() && busListModel.isAc()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (boarding.equals("") && !dropping.equals("") && seatType.equals("both") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusDroppingPointModels()) {
          if (listModel.getBoardingId().equals(dropping)) {
            if (busListModel.isSeater() && busListModel.isSleeper() && busListModel.isAc()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    }
    if (boarding.equals("") && !dropping.equals("") && seatType.equals("seater") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusDroppingPointModels()) {
          if (listModel.getBoardingId().equals(dropping)) {
            if (busListModel.isSeater() && !busListModel.isSleeper() && busListModel.isNonAC()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (boarding.equals("") && !dropping.equals("") && seatType.equals("sleeper") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusDroppingPointModels()) {
          if (listModel.getBoardingId().equals(dropping)) {
            if (!busListModel.isSeater() && busListModel.isSleeper() && busListModel.isNonAC()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (boarding.equals("") && !dropping.equals("") && seatType.equals("both") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusDroppingPointModels()) {
          if (listModel.getBoardingId().equals(dropping)) {
            if (busListModel.isSeater() && busListModel.isSleeper() && busListModel.isAc()) {
              filterlist.add(busListModel);
            }
          }
        }
      }
    } else if (!boarding.equals("") && !dropping.equals("") && seatType.equals("seater") && ac) {
      filterlist.clear();
//            for (int i = 0; i < busItem.size(); i++) {
//                for (int j = 0; j < busItem.get(i).getBusBoardingPointModels().size(); j++) {
////                  j  Log.i("LogBOARD", boarding + ": " + busItem.get(i).getBusBoardingPointModels().get(j).getBoardingId());
//                    if (busItem.get(i).getBusBoardingPointModels().get(j).getBoardingTypeID().equals(boarding)) {
//                        for (int k = 0; k < busItem.get(i).getBusDroppingPointModels().size(); k++) {
////                         k   Log.i("LogDROP", dropping + ": " + busItem.get(i).getBusBoardingPointModels().get(k).getBoardingId());
//                            if (busItem.get(i).getBusDroppingPointModels().get(k).getBoardingTypeID().equals(dropping)) {
//                                if (busItem.get(i).isSeater() && !busItem.get(i).isSleeper() && busItem.get(i).isAc()) {
//                                    filterlist.add(busItem.get(i));
//                                }
//                            }
//                        }
//
//                    }
//                }
//            }
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            for (BusBoardingPointModel busBoardingPointModel : busListModel.getBusDroppingPointModels()) {
              if (busBoardingPointModel.equals(dropping)) {
                if (busListModel.isSeater() && !busListModel.isSleeper() && busListModel.isAc()) {
                  filterlist.add(busListModel);
                }
              }
            }
          }
        }
      }
    } else if (!boarding.equals("") && !dropping.equals("") && seatType.equals("sleeper") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            for (BusBoardingPointModel busBoardingPointModel : busListModel.getBusDroppingPointModels()) {
              if (busBoardingPointModel.equals(dropping)) {
                if (!busListModel.isSeater() && busListModel.isSleeper() && busListModel.isAc()) {
                  filterlist.add(busListModel);
                }
              }
            }
          }
        }
      }
    } else if (!boarding.equals("") && !dropping.equals("") && seatType.equals("both") && ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            for (BusBoardingPointModel busBoardingPointModel : busListModel.getBusDroppingPointModels()) {
              if (busBoardingPointModel.equals(dropping)) {
                if (busListModel.isSeater() && busListModel.isSleeper() && busListModel.isAc()) {
                  filterlist.add(busListModel);
                }
              }
            }
          }
        }
      }
    }
    if (!boarding.equals("") && !dropping.equals("") && seatType.equals("seater") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            for (BusBoardingPointModel busBoardingPointModel : busListModel.getBusDroppingPointModels()) {
              if (busBoardingPointModel.equals(dropping)) {
                if (busListModel.isSeater() && !busListModel.isSleeper() && busListModel.isNonAC()) {
                  filterlist.add(busListModel);
                }
              }
            }
          }
        }
      }
    } else if (!boarding.equals("") && !dropping.equals("") && seatType.equals("sleeper") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            for (BusBoardingPointModel busBoardingPointModel : busListModel.getBusDroppingPointModels()) {
              if (busBoardingPointModel.equals(dropping)) {
                if (!busListModel.isSeater() && busListModel.isSleeper() && busListModel.isNonAC()) {
                  filterlist.add(busListModel);
                }
              }
            }
          }
        }
      }
    } else if (!boarding.equals("") && !dropping.equals("") && seatType.equals("both") && !ac) {
      filterlist.clear();
      for (BusListModel busListModel : busItem) {
        for (BusBoardingPointModel listModel : busListModel.getBusBoardingPointModels()) {
          if (listModel.getBoardingId().equals(boarding)) {
            for (BusBoardingPointModel busBoardingPointModel : busListModel.getBusDroppingPointModels()) {
              if (busBoardingPointModel.equals(dropping)) {
                if (busListModel.isSeater() && busListModel.isSleeper() && busListModel.isNonAC()) {
                  filterlist.add(busListModel);
                }
              }
            }
          }
        }
      }
    }
    busTyeAdapter = new BusListAdapter(BusListActivity.this, filterlist, destinationCode, sourceCode, dateOfDep);
    lvListBus.setAdapter(busTyeAdapter);
    if (filterlist.size() > 1) {
      CustomToast.showMessage(this, filterlist.size() + " buses found");
    } else if (filterlist.size() == 1) {
    } else {
      busTyeAdapter = new BusListAdapter(BusListActivity.this, busItem, destinationCode, sourceCode, dateOfDep);
      lvListBus.setAdapter(busTyeAdapter);
    }

  }


  @Override
  protected void onResume() {
    super.onResume();

  }

  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    super.onDestroy();
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(BusListActivity.this, R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(BusListActivity.this).sendBroadcast(intent);
  }


  public class compPopulation implements Comparator<BusListModel> {

    public int compare(BusListModel a, BusListModel b) {
      Log.i("MAx", "value");
      if (Double.parseDouble(a.getBusFare()) > Double.parseDouble(b.getBusFare()))
        return -1; // highest value first
      if (Double.parseDouble(a.getBusFare()) == Double.parseDouble(b.getBusFare()))
        return 0;
      return 1;
    }
  }

  private Date parseDate(String date) {
    Date arrTime = null;
    SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    try {
      arrTime = arrinput.parse(date);
//                depTime = depinput.parse(busArray.get(position).getBusDepTime());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return arrTime;
  }

}
