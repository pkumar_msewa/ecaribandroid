package in.msewa.ecarib.activity.shopping.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import in.msewa.adapter.InCartAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.model.PQCart;
import in.msewa.util.InCartListner;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 4/6/2016.
 */
public class TeleBuyInCartFragment extends Fragment implements InCartListner {
    private PQCart cart = PQCart.getInstance();
    private ListView lvInCartItem;
    private ImageView imBuyNext;
    private TextView tvTotalRupees;
    private View rootView;
    private double totalCost = 0.0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_incart_products, container, false);
        tvTotalRupees = (TextView) rootView.findViewById(R.id.tvBuyTotalRupess);
        imBuyNext = (ImageView) rootView.findViewById(R.id.imBuyNext);
        lvInCartItem = (ListView) rootView.findViewById(R.id.hlvInCart);
        lvInCartItem.setAdapter(new InCartAdapter(getActivity(),this));
        updateView();

        imBuyNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (cart.getProductsInCart().size() == 0) {
                    CustomToast.showMessage(getActivity(), "You have no products in cart.");
                }
                else{
                    TeleBuyDeleveryFragment deliveryFragment = new TeleBuyDeleveryFragment();
                    Bundle args = new Bundle();
                    args.putDouble("total_amount", Double.parseDouble(tvTotalRupees.getText().toString().replace(",","")));
                    deliveryFragment.setArguments(args);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(getTag());
                    fragmentTransaction.replace(R.id.frameInCart, deliveryFragment);

                    fragmentTransaction.commit();
                }

            }
        });
        return rootView;
    }

    private void updateView() {
        tvTotalRupees.setText(cart.getTotalCost());
    }

    private void clearCartBadge() {
        Intent intent = new Intent("cart-clear");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void taskCompleted() {
        updateView();
    }

    @Override
    public void selectAddress(String s) {

    }

    @Override
    public void closeCart() {

        clearCartBadge();
        getActivity().finish();
    }

    @Override
    public void deleteAddress(String addID, int pos) {

    }

    @Override
    public void editAddress(String addId, int pos) {

    }

}
