package in.msewa.ecarib.activity.flightinneracitivty;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.fragment.fragmenttravel.FlightPaymentActivity;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.FlightListModel;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.R;


/**
 * Created by kashifimam on 05/08/17.
 */

public class FlightBookingFormReturnActivity extends AppCompatActivity {


  private MaterialEditText etBookFlightMobile, etBookFlightDob, etBookFlightCitizen, etBookFlightEmail;
  private RadioButton rbBookFlightMale, rbBookFlightFeMale;
  private LinearLayout llFlightAdult, llFlightChild, llFlightInfant;

  private Button btnBookFlightSubmit;


  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private LoadingDialog loadDlg;

  private String gender = "MALE";

  private String sourceCode, destinationCode, dateOfJourney, dateOfReturn;
  private int adultNo, childNo, infantNo;
  private String flightClass;


  private FlightListModel flightListModelUp, flightListModelDown;
  private UserModel session = UserModel.getInstance();

  private JSONObject segmentObject;
  private JSONObject segmentObjectDown;

  private JSONObject jsonRequest;
  private String tag_json_obj = "json_flight";

  private String engineId;


  private boolean cancel;
  private View focusView;
  private String flightjsonUp, flightjsondown;
  private JSONObject contingFlight;
  private String totalAmount;
  private String totalAmountUp, totalAmountDown;
  private String latesttravel;
  private String flightSegment;
  private String flightsecond;
  private String oneWayDetails, twoWayDetails;
  private ArrayList<EditText> materialEditAdultFirstName, materialEditAdultLastName, materialEditChildFirstName, materialEditChildLastName;
  private ArrayList<MaterialEditText> materialEditinfantfirstName, materialEditinfantLastName, materialEditAgeFistName, materialEditAgeLAst;
  private ArrayList<MaterialEditText> materialEditAdultAge, materialEditChildAge, materialEditinfantAge;
  private ArrayList<EditText> materialEditAdultDOB, materialEditAdultPassport, materialEditAdultExpriDate, materialEditChildDOB, materialEditChildPassport, materialEditChildExpriDate, materialEditinfantDOB, materialEditinfantPassport, materialEditinfantExpriDate;
  private TextView tvadult, tvChild, tvinfant;


  public static void longLog(String str) {
    if (str.length() > 4000) {
      Log.d("travel", str.substring(0, 4000));
      longLog(str.substring(4000));
    } else
      Log.d("travel", str);
  }

  public static JSONObject function(JSONObject obj, String keyMain, String newValue) throws Exception {
    Log.i("values", newValue);
    // We need to know keys of Jsonobject
    JSONObject json = new JSONObject();
    Iterator iterator = obj.keys();
    String key = null;
    while (iterator.hasNext()) {
      key = (String) iterator.next();
      // if object is just string we change value in key
      if ((obj.optJSONArray(key) == null) && (obj.optJSONObject(key) == null)) {
        if ((key.equalsIgnoreCase(keyMain))) {
          // put new value
          obj.put(key, newValue);
          Log.i("json", obj.toString());
          return obj;
        }
      }

      // if it's jsonobject
      if (obj.optJSONObject(key) != null) {
        function(obj.getJSONObject(key), keyMain, newValue);
      }

      // if it's jsonarray
      if (obj.optJSONArray(key) != null) {
        JSONArray jArray = obj.getJSONArray(key);
        for (int i = 0; i < jArray.length(); i++) {
          function(jArray.getJSONObject(i), keyMain, newValue);
        }
      }
    }
    return obj;
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_flight_booking_form);
    loadDlg = new LoadingDialog(FlightBookingFormReturnActivity.this);

    //Intent Data
    flightListModelUp = getIntent().getParcelableExtra("FlightModelUp");
    flightListModelDown = getIntent().getParcelableExtra("FlightModelDown");
    totalAmount = getIntent().getStringExtra("totalAmount");
    latesttravel = getIntent().getStringExtra("latesttravel");
    flightSegment = getIntent().getStringExtra("flightSegment");
    flightsecond = getIntent().getStringExtra("flightsecond");
    oneWayDetails = getIntent().getStringExtra("oneWayDetails");
    tvadult = (TextView) findViewById(R.id.tvadult);
    tvChild = (TextView) findViewById(R.id.tvChild);
    tvinfant = (TextView) findViewById(R.id.tvinfant);
    twoWayDetails = getIntent().getStringExtra("twoWayDetails");

    try {
      segmentObject = new JSONObject(flightSegment);
      segmentObjectDown = new JSONObject(flightsecond);

      engineId = segmentObject.getString("engineID");
    } catch (JSONException e) {
      e.printStackTrace();
    }
    sourceCode = getIntent().getStringExtra("sourceCode");
    destinationCode = getIntent().getStringExtra("destinationCode");
    dateOfJourney = getIntent().getStringExtra("dateOfJourney");
    dateOfReturn = getIntent().getStringExtra("dateOfReturn");
    flightClass = getIntent().getStringExtra("flightClass");
    flightjsonUp = getIntent().getStringExtra("flightjsonUp");
    flightjsondown = getIntent().getStringExtra("flightdown");

    totalAmountUp = getIntent().getStringExtra("totalUp");
    totalAmountDown = getIntent().getStringExtra("totalDown");
    contingFlight = new JSONObject();
    if (!flightjsonUp.equalsIgnoreCase("{}") && !flightjsondown.equalsIgnoreCase("{}")) {
      try {
        JSONObject flighTList = new JSONObject(flightjsonUp);
        JSONObject flighTwo = new JSONObject(flightjsondown);
        try {
          JSONObject seconArray = function(flighTwo, "ItineraryKey", flighTList.getString("itineraryKey"));
          Log.i("values", flighTList.toString());
          Log.i("values_install", flighTwo.toString());
          JSONArray jsonArray = new JSONArray();
          jsonArray.put(flighTList);
          jsonArray.put(seconArray);
          contingFlight.put("bookSegments", jsonArray);
          Log.i("currectJson", contingFlight.toString());
        } catch (Exception e) {
          e.printStackTrace();
        }

      } catch (JSONException e) {
        e.printStackTrace();
      }
      Log.i("value", contingFlight.toString());
    } else if (!flightjsonUp.equalsIgnoreCase("{}")) {
      try {
        JSONObject flighTList = new JSONObject(flightjsonUp);
        JSONObject oneWayDetailsjson = new JSONObject(oneWayDetails);
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(flighTList);
        contingFlight.put("bookSegments", jsonArray);
        Log.i("currectJson", contingFlight.toString());
      } catch (JSONException e) {
        e.printStackTrace();
      }
      Log.i("value", contingFlight.toString());
    } else if (!flightjsondown.equalsIgnoreCase("{}")) {
      try {
        JSONObject flighTwo = new JSONObject(flightjsondown);
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(flighTwo);
        contingFlight.put("bookSegments", jsonArray);
        Log.i("currectJson", contingFlight.toString());
      } catch (JSONException e) {
        e.printStackTrace();
      }
      Log.i("value", contingFlight.toString());
    }
    adultNo = getIntent().getIntExtra("adultNo", 0);
    childNo = getIntent().getIntExtra("childNo", 0);
    infantNo = getIntent().getIntExtra("infantNo", 0);


    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });


    etBookFlightMobile = (MaterialEditText) findViewById(R.id.etBookFlightMobile);
    etBookFlightDob = (MaterialEditText) findViewById(R.id.etBookFlightDob);
    etBookFlightCitizen = (MaterialEditText) findViewById(R.id.etBookFlightCitizen);
    etBookFlightEmail = (MaterialEditText) findViewById(R.id.etBookFlightEmail);


    rbBookFlightMale = (RadioButton) findViewById(R.id.rbBookFlightMale);
    rbBookFlightFeMale = (RadioButton) findViewById(R.id.rbBookFlightFeMale);

    btnBookFlightSubmit = (Button) findViewById(R.id.btnBookFlightSubmit);

    llFlightAdult = (LinearLayout) findViewById(R.id.llFlightAdult);
    llFlightChild = (LinearLayout) findViewById(R.id.llFlightChild);
    llFlightInfant = (LinearLayout) findViewById(R.id.llFlightInfant);

    if (childNo == 0) {
      llFlightChild.setVisibility(View.GONE);
    }

    if (adultNo == 0) {
      llFlightAdult.setVisibility(View.GONE);
    }

    if (infantNo == 0) {
      llFlightInfant.setVisibility(View.GONE);
    }


    etBookFlightDob.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        new DatePickerDialog(FlightBookingFormReturnActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String formattedDay, formattedMonth;
            if (dayOfMonth < 10) {
              formattedDay = "0" + dayOfMonth;
            } else {
              formattedDay = dayOfMonth + "";
            }

            if ((monthOfYear + 1) < 10) {
              formattedMonth = "0" + String.valueOf(monthOfYear + 1);
            } else {
              formattedMonth = String.valueOf(monthOfYear + 1) + "";
            }
            etBookFlightDob.setText(String.valueOf(year) + "-"
              + String.valueOf(formattedMonth) + "-"
              + String.valueOf(formattedDay));
          }
        }, 1990, 01, 01).show();
      }
    });


    materialEditAdultFirstName = new ArrayList<>();
    materialEditAdultLastName = new ArrayList<>();
    materialEditAdultDOB = new ArrayList<>();
    materialEditAdultPassport = new ArrayList<>();
    materialEditAdultExpriDate = new ArrayList<>();
    materialEditAdultAge = new ArrayList<>();
    materialEditChildFirstName = new ArrayList<>();
    materialEditChildLastName = new ArrayList<>();
    materialEditChildDOB = new ArrayList<>();
    materialEditChildPassport = new ArrayList<>();
    materialEditChildExpriDate = new ArrayList<>();
    materialEditChildAge = new ArrayList<>();
    materialEditinfantfirstName = new ArrayList<>();
    materialEditinfantLastName = new ArrayList<>();
    materialEditinfantAge = new ArrayList<>();
    materialEditinfantDOB = new ArrayList<>();
    materialEditinfantPassport = new ArrayList<>();
    materialEditinfantExpriDate = new ArrayList<>();
    InputFilter filter = new InputFilter() {
      public CharSequence filter(CharSequence source, int start, int end,
                                 Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; i++) {
          if (!Character.isLetterOrDigit(source.charAt(i))) {
            return "";
          }
        }
        return null;
      }
    };

    for (int i = 0; i < adultNo; i++) {
      tvadult.setVisibility(View.VISIBLE);
      ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
        , ViewGroup.LayoutParams.WRAP_CONTENT);

      final MaterialEditText etFName = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etFName.setHint("Enter adult first name");
      etFName.setIconLeft(R.drawable.et_user);
      etFName.setFilters(new InputFilter[]{filter});
      final MaterialEditText etLName = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etLName.setHint("Enter adult  last name");
      etLName.setIconLeft(R.drawable.et_user);
      etLName.setFilters(new InputFilter[]{filter});
      final MaterialEditText etAge = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etAge.setHint("Enter Gender");
      etAge.setIconLeft(R.drawable.et_gender);

      final MaterialEditText etDOb = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etDOb.setHint("Enter Date of Birth");
      etDOb.setIconLeft(R.drawable.et_date);
      etDOb.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          getDob(etDOb, "adult");
        }
      });
      final MaterialEditText etPassport = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etPassport.setHint("Enter Passport Number");
      final MaterialEditText etPassportExpiry = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etPassportExpiry.setHint("Enter Passport Expiry Date");
      etPassportExpiry.setIconLeft(R.drawable.et_date);
      etPassportExpiry.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          getexiper(etPassportExpiry);
        }
      });
      etPassport.setIconLeft(R.drawable.et_date);
      etDOb.setLayoutParams(lp);
      etDOb.setFocusable(false);
      etAge.setIconLeft(R.drawable.et_gender);
      etAge.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          getAdult(etAge);
        }
      });
      etLName.setIconLeft(R.drawable.et_user);
      etFName.setLayoutParams(lp);
      etLName.setLayoutParams(lp);
      etAge.setLayoutParams(lp);
      etAge.setFocusable(false);
      etPassportExpiry.setFocusable(false);
      etPassportExpiry.setFocusableInTouchMode(false);
      etAge.setFocusableInTouchMode(false);
      llFlightAdult.addView(etFName);
      llFlightAdult.addView(etLName);
      llFlightAdult.addView(etAge);
      materialEditAdultFirstName.add(etFName);
      materialEditAdultLastName.add(etLName);
      materialEditAdultAge.add(etAge);
      llFlightAdult.addView(etDOb);
//      llFlightAdult.addView(etPassport);
//      llFlightAdult.addView(etPassportExpiry);
      materialEditAdultDOB.add(etDOb);
//      materialEditAdultPassport.add(etPassport);
//      materialEditAdultExpriDate.add(etPassportExpiry);
    }
    for (int i = 0; i < childNo; i++) {
      tvChild.setVisibility(View.VISIBLE);
      ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
        , ViewGroup.LayoutParams.WRAP_CONTENT);

      final MaterialEditText etFName = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etFName.setIconLeft(R.drawable.et_user);
      etFName.setHint("Enter child first name");
      final MaterialEditText etLName = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etLName.setHint("Enter child last name");
      etLName.setIconLeft(R.drawable.et_user);

      final MaterialEditText etAge = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etAge.setHint("Enter Gender");
      etAge.setIconLeft(R.drawable.et_gender);
      final MaterialEditText etDOb = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etDOb.setHint("Enter Date of Birth");
      etDOb.setIconLeft(R.drawable.et_date);
      etDOb.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          getDob(etDOb, "child");
        }
      });
      final MaterialEditText etPassport = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etPassport.setHint("Enter Passport Number");
      final MaterialEditText etPassportExpiry = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etPassportExpiry.setHint("Enter Passport Expiry Date");
      etPassportExpiry.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          getexiper(etPassportExpiry);
        }
      });
      etPassport.setIconLeft(R.drawable.et_date);
      etPassportExpiry.setIconLeft(R.drawable.et_date);
      etDOb.setLayoutParams(lp);
      etDOb.setFocusable(false);

      etAge.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          getAdult(etAge);
        }
      });
      etLName.setIconLeft(R.drawable.et_user);
      etFName.setLayoutParams(lp);
      etLName.setLayoutParams(lp);
      etAge.setLayoutParams(lp);
      etAge.setFocusable(false);
      etAge.setFocusableInTouchMode(false);
      etPassportExpiry.setFocusable(false);
      etPassportExpiry.setFocusableInTouchMode(false);
      etFName.setLayoutParams(lp);
      etLName.setLayoutParams(lp);
      etAge.setLayoutParams(lp);
      llFlightChild.addView(etFName);
      llFlightChild.addView(etLName);
      llFlightChild.addView(etAge);
      llFlightChild.addView(etDOb);
//      llFlightChild.addView(etPassport);
//      llFlightChild.addView(etPassportExpiry);
      materialEditChildDOB.add(etDOb);
//      materialEditChildExpriDate.add(etPassport);
//      materialEditChildPassport.add(etPassportExpiry);
      materialEditChildFirstName.add(etFName);
      materialEditChildLastName.add(etLName);
      materialEditChildAge.add(etAge);
    }

    for (int i = 0; i < infantNo; i++) {
      tvinfant.setVisibility(View.VISIBLE);
      ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
        , ViewGroup.LayoutParams.WRAP_CONTENT);

      final MaterialEditText etFName = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etFName.setHint("Enter infant first name");
      etFName.setIconLeft(R.drawable.et_user);
      final MaterialEditText etLName = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etLName.setHint("Enter infant last name");

      etLName.setIconLeft(R.drawable.et_user);
      final MaterialEditText etAge = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etAge.setHint("Enter Gender");
      etAge.setIconLeft(R.drawable.et_gender);

      etAge.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          getAdult(etAge);
        }
      });
      etLName.setIconLeft(R.drawable.et_user);
      etFName.setLayoutParams(lp);
      etLName.setLayoutParams(lp);
      etAge.setLayoutParams(lp);
      etAge.setFocusable(false);
      etAge.setFocusableInTouchMode(false);
      etFName.setLayoutParams(lp);
      etLName.setLayoutParams(lp);
      etAge.setLayoutParams(lp);
      final MaterialEditText etDOb = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etDOb.setHint("Enter Date of Birth");
      etDOb.setIconLeft(R.drawable.et_date);
      etDOb.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          getDob(etDOb, "infantNo");
        }
      });
      final MaterialEditText etPassport = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etPassport.setHint("Enter Passport Number");
      final MaterialEditText etPassportExpiry = new MaterialEditText(FlightBookingFormReturnActivity.this);
      etPassportExpiry.setHint("Enter Passport Expiry Date");
      etPassportExpiry.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          getexiper(etPassportExpiry);
        }
      });

      etPassport.setLayoutParams(lp);
      etPassportExpiry.setLayoutParams(lp);
      etDOb.setLayoutParams(lp);
      etPassport.setIconLeft(R.drawable.et_date);
      etPassportExpiry.setIconLeft(R.drawable.et_date);
      etDOb.setFocusable(false);
      etPassport.setFocusableInTouchMode(false);
      etPassportExpiry.setFocusable(false);
      etPassportExpiry.setFocusableInTouchMode(false);
      etPassport.setIconLeft(R.drawable.et_date);
//      materialEditinfantPassport.add(etPassport);
//      materialEditinfantExpriDate.add(etPassportExpiry);
      llFlightInfant.addView(etFName);
      llFlightInfant.addView(etLName);
      llFlightInfant.addView(etAge);
      llFlightInfant.addView(etDOb);
//      llFlightInfant.addView(etPassport);
//      llFlightInfant.addView(etPassportExpiry);
      materialEditinfantDOB.add(etDOb);
      materialEditinfantfirstName.add(etFName);
      materialEditinfantLastName.add(etLName);
      materialEditinfantAge.add(etAge);
    }

    etBookFlightEmail.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

      }
    });

    btnBookFlightSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        submitForm();
      }
    });


  }

  public void getAdult(final MaterialEditText materialEditText) {
    final String[] chars = {"MALE", "FEMALE"};
    AlertDialog.Builder alertDialog = new AlertDialog.Builder(FlightBookingFormReturnActivity.this);
    alertDialog.setItems(chars, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        materialEditText.setText(chars[i]);
        dialogInterface.dismiss();
      }
    });
    alertDialog.show();

  }

  private void submitForm() {
    validateDynamicAdultField();
    validateDynamicChildField();
    validateDynamicInfantField();
    etBookFlightCitizen.setError(null);
    etBookFlightDob.setError(null);
    etBookFlightMobile.setError(null);
    etBookFlightEmail.setError(null);

    cancel = false;

    checkDynamicAdultField();
    checkDynamicChildField();
    checkDynamicInfantField();
//        checkDob(etBookFlightDob.getText().toString());
//        checkCitizenship(etBookFlightCitizen.getText().toString());
    checkEmail(etBookFlightEmail.getText().toString());
    checkMobile(etBookFlightMobile.getText().toString());


//        if (rbBookFlightMale.isChecked()) {
//            gender = "MALE";
//        } else {
//            gender = "FEMALE";
//        }

    if (cancel) {
      focusView.requestFocus();
    } else {
      try {
        attemptCheckOut();
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }


  }

  private void validateDynamicAdultField() {
    int count = llFlightAdult.getChildCount();
    for (int i = 0; i < count; i++) {
      View view = llFlightAdult.getChildAt(i);
      EditText etDynamic = (EditText) view;
      etDynamic.setError(null);
    }


  }

  private void checkDynamicAdultField() {
    int count = llFlightAdult.getChildCount();
    for (int i = 0; i < count; i++) {
      View view = llFlightAdult.getChildAt(i);
      EditText etDynamic = (EditText) view;

      if (etDynamic.getText().toString().trim().isEmpty()) {
        etDynamic.setError("This field is required");
        focusView = etDynamic;
        cancel = true;
      }
    }

  }

  private void validateDynamicChildField() {
    int count = llFlightChild.getChildCount();
    for (int i = 0; i < count; i++) {
      View view = llFlightChild.getChildAt(i);
      EditText etDynamic = (EditText) view;
      etDynamic.setError(null);
    }


  }

  private void checkDynamicChildField() {
    int count = llFlightChild.getChildCount();
    for (int i = 0; i < count; i++) {
      View view = llFlightChild.getChildAt(i);
      EditText etDynamic = (EditText) view;

      if (etDynamic.getText().toString().trim().isEmpty()) {
        etDynamic.setError("This field is required");
        focusView = etDynamic;
        cancel = true;
      }
    }

  }

  private void validateDynamicInfantField() {
    int count = llFlightInfant.getChildCount();
    for (int i = 0; i < count; i++) {
      View view = llFlightInfant.getChildAt(i);
      EditText etDynamic = (EditText) view;
      etDynamic.setError(null);
    }


  }

  private void checkDynamicInfantField() {
    int count = llFlightInfant.getChildCount();
    for (int i = 0; i < count; i++) {
      View view = llFlightInfant.getChildAt(i);
      EditText etDynamic = (EditText) view;

      if (etDynamic.getText().toString().trim().isEmpty()) {
        etDynamic.setError("This field is required");
        focusView = etDynamic;
        cancel = true;
      }
    }

  }

  private void checkEmail(String inviteName) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkEmail(inviteName);
    if (!inviteNameCheckLog.isValid) {
      etBookFlightEmail.setError(getString(inviteNameCheckLog.msg));
      focusView = etBookFlightEmail;
      cancel = true;
    }

  }

  private void checkMobile(String inviteName) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkMobileTenDigit(inviteName);
    if (!inviteNameCheckLog.isValid) {
      etBookFlightMobile.setError(getString(inviteNameCheckLog.msg));
      focusView = etBookFlightMobile;
      cancel = true;
    }

  }

  private void checkCitizenship(String inviteName) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
    if (!inviteNameCheckLog.isValid) {
      etBookFlightCitizen.setError(getString(inviteNameCheckLog.msg));
      focusView = etBookFlightCitizen;
      cancel = true;
    }

  }

  private void checkDob(String inviteName) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
    if (!inviteNameCheckLog.isValid) {
      etBookFlightDob.setError(getString(inviteNameCheckLog.msg));
      focusView = etBookFlightDob;
      cancel = true;
    }

  }

  public void attemptCheckOut() throws JSONException {
    jsonRequest = new JSONObject();

    try {

      //Adult
      String adultFName = "";
      String adultLName = "";

      for (int i = 0; i < materialEditAdultFirstName.size(); i++) {
        adultFName = adultFName + "~" + materialEditAdultFirstName.get(i).getText().toString();
        adultLName = adultLName + "@" + materialEditAdultLastName.get(i).getText().toString();
      }


      //Child
      String childFName = "";
      String childLName = "";
      for (int i = 0; i < materialEditChildFirstName.size(); i++) {
        childFName = childFName + "~" + materialEditChildFirstName.get(i).getText().toString();
        childLName = childLName + "@" + materialEditChildLastName.get(i).getText().toString();

      }

      //Infant
      String infantFName = "";
      String infantLName = "";

      for (int i = 0; i < materialEditinfantfirstName.size(); i++) {
        infantFName = infantFName + "~" + materialEditinfantfirstName.get(i).getText().toString();
        infantLName = infantLName + "@" + materialEditinfantLastName.get(i).getText().toString();

      }

      if (childNo != 0) {
        jsonRequest.put("firstNamechild", childFName);
        jsonRequest.put("lastNamechild", childLName);
      }

      if (adultNo != 0) {
        jsonRequest.put("firstName", adultFName);
        jsonRequest.put("lastName", adultLName);

      }

      if (infantNo != 0) {
        jsonRequest.put("firstNameinfant", infantFName);
        jsonRequest.put("lastNameinfant", infantLName);

      }


      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("grandtotal", segmentObject.getJSONArray("fares").getJSONObject(0).getDouble("totalFareWithOutMarkUp") + segmentObjectDown.getJSONArray("fares").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
      jsonRequest.put("boundTypes", "");
      jsonRequest.put("itineraryKey", segmentObject.getString("itineraryKey"));
      jsonRequest.put("baggageFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("baggageFare"));
      jsonRequest.put("ssrFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("ssrFare"));
      jsonRequest.put("journeyTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
      jsonRequest.put("addOnDetail", "");
      jsonRequest.put("infants", infantNo);
      jsonRequest.put("aircraftCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftCode"));
      jsonRequest.put("aircraftType", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftType"));
      jsonRequest.put("airlineName", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("airlineName"));
      jsonRequest.put("amount", totalAmountUp);
      jsonRequest.put("arrivalDate", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalDate"));
      jsonRequest.put("arrivalTerminal", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTerminal"));
      jsonRequest.put("arrivalTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTime"));
      jsonRequest.put("availableSeat", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("availableSeat"));
      jsonRequest.put("baggageUnit", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageUnit"));
      jsonRequest.put("baggageWeight", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageWeight"));
      jsonRequest.put("boundTypes", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundTypes"));
      jsonRequest.put("cabin", flightClass);
      jsonRequest.put("capacity", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("capacity"));
      jsonRequest.put("carrierCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("carrierCode"));
      jsonRequest.put("currencyCode", "INR");
      jsonRequest.put("departureDate", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureDate"));
      jsonRequest.put("departureTerminal", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTerminal"));
      jsonRequest.put("departureTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTime"));
      jsonRequest.put("destination", destinationCode);
      jsonRequest.put("duration", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
      jsonRequest.put("fareBasisCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareBasisCode"));
      jsonRequest.put("fareClassOfService", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareClassOfService"));
      jsonRequest.put("flightDesignator", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDesignator"));

      jsonRequest.put("flightDetailRefKey", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDetailRefKey"));
      jsonRequest.put("flightName", segmentObject.getString("engineID"));
      jsonRequest.put("flightNumber", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightNumber"));
      jsonRequest.put("group", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("group"));
      jsonRequest.put("connecting", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getBoolean("connecting"));
      jsonRequest.put("numberOfStops", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("numberOfStops"));
      jsonRequest.put("origin", sourceCode);
      jsonRequest.put("providerCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("providerCode"));
      jsonRequest.put("remarks", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("remarks"));
      jsonRequest.put("sold", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getInt("sold"));
      jsonRequest.put("status", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("status"));
      jsonRequest.put("deeplink", segmentObject.getString("deeplink"));
      jsonRequest.put("fareIndicator", 0);
      jsonRequest.put("fareRule", segmentObject.getString("fareRule"));
      jsonRequest.put("cache", segmentObject.getBoolean("cache"));
      jsonRequest.put("holdBooking", segmentObject.getBoolean("holdBooking"));
      jsonRequest.put("international", segmentObject.getBoolean("international"));
      jsonRequest.put("roundTrip", segmentObject.getBoolean("roundTrip"));
      jsonRequest.put("special", segmentObject.getBoolean("special"));
      jsonRequest.put("specialId", segmentObject.getBoolean("specialId"));
      jsonRequest.put("journeyIndex", segmentObject.getInt("journeyIndex"));
      jsonRequest.put("memoryCreationTime", "/Date(1499158249483+0530)/");
      jsonRequest.put("nearByAirport", segmentObject.getBoolean("nearByAirport"));
      jsonRequest.put("remark", segmentObject.getString("remark"));
      jsonRequest.put("searchId", segmentObject.getString("searchId"));
      jsonRequest.put("engineID", segmentObject.getString("engineID"));

      jsonRequest.put("basicFare", segmentObject.getJSONArray("fares").getJSONObject(0).getDouble("basicFare"));
      jsonRequest.put("exchangeRate", segmentObject.getJSONArray("fares").getJSONObject(0).getDouble("exchangeRate"));
      jsonRequest.put("totalFareWithOutMarkUp", segmentObject.getJSONArray("fares").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
      jsonRequest.put("totalTaxWithOutMarkUp", segmentObject.getJSONArray("fares").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
      jsonRequest.put("baseTransactionAmount", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
      jsonRequest.put("cancelPenalty", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
      jsonRequest.put("changePenalty", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
      jsonRequest.put("equivCurrencyCode", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("equivCurrencyCode"));
      jsonRequest.put("fareInfoKey", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoKey"));
      jsonRequest.put("fareInfoValue", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoValue"));

      jsonRequest.put("markUP", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
      jsonRequest.put("paxType", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
      jsonRequest.put("refundable", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));
      jsonRequest.put("totalFare", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
      jsonRequest.put("totalTax", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
      jsonRequest.put("transactionAmount", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));
      jsonRequest.put("beginDate", dateOfJourney);
      jsonRequest.put("endDate", dateOfJourney);

//            jsonRequest.put("bookingCurrencyCode", "INR");
      jsonRequest.put("address1", "BTM");
      jsonRequest.put("address2", "BTM");
      jsonRequest.put("city", "Banglore");
      jsonRequest.put("countryCode", "IN");
      jsonRequest.put("cultureCode", "String content");

      jsonRequest.put("emailAddress", etBookFlightEmail.getText().toString());
      jsonRequest.put("middleName", "");
      jsonRequest.put("homePhone", "");

      jsonRequest.put("frequentFlierNumber", "");
      jsonRequest.put("mobileNumber", etBookFlightMobile.getText().toString());
      jsonRequest.put("nationality", "India");
      jsonRequest.put("passportExpiryDate", "/Date(928129800000+0530)/");
      jsonRequest.put("passportNo", "");
      jsonRequest.put("provisionState", "Karnataka");
      jsonRequest.put("residentCountry", "India");

      String genders = materialEditAdultAge.get(0).getText().toString();

      if (genders.equalsIgnoreCase("MALE")) {
        jsonRequest.put("title", "Mr");
        jsonRequest.put("gender", genders);
      } else {
        jsonRequest.put("title", "Miss");
        jsonRequest.put("gender", genders);
      }

      jsonRequest.put("visatype", "Employee Visa");
      jsonRequest.put("traceId", "AYTM00011111111110001");
      jsonRequest.put("domestic", true);
      jsonRequest.put("bookFares", segmentObject.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares"));

      //Date Fomat
//            jsonRequest.put("bookingAmount", totalAmount);
//            jsonRequest.put("dateofBirth", etBookFlightDob.getText().toString());
      jsonRequest.put("meal", "");


      //Reutrn
      jsonRequest.put("boundTypereturn", "");
      jsonRequest.put("itineraryKeyreturn", segmentObjectDown.getString("itineraryKey"));
      jsonRequest.put("baggageFarereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getBoolean("baggageFare"));
      jsonRequest.put("ssrFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("ssrFare"));
      jsonRequest.put("journeyTimereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
      jsonRequest.put("addOnDetailreturn", "");
      jsonRequest.put("aircraftCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftCode"));
      jsonRequest.put("aircraftTypereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftType"));
      jsonRequest.put("airlineNamereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("airlineName"));
      jsonRequest.put("amountreturn", totalAmountDown);
      jsonRequest.put("arrivalDatereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalDate"));
      Log.i("arrivaldate", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalDate"));
      jsonRequest.put("nearByAirportreturn", segmentObjectDown.getBoolean("nearByAirport"));
      jsonRequest.put("arrivalTerminalreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTerminal"));
      jsonRequest.put("arrivalTimereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTime"));
      jsonRequest.put("availableSeatreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("availableSeat"));
      jsonRequest.put("baggageUnitreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageUnit"));
      jsonRequest.put("baggageWeightreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageWeight"));
      jsonRequest.put("boundTypesreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundTypes"));
      jsonRequest.put("cabinreturn", flightClass);
      jsonRequest.put("capacityreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("capacity"));
      jsonRequest.put("carrierCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("carrierCode"));
      jsonRequest.put("currencyCodereturn", "INR");
      jsonRequest.put("departureDatereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureDate"));
      jsonRequest.put("departureTerminalreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTerminal"));
      jsonRequest.put("departureTimereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTime"));
      jsonRequest.put("destinationreturn", sourceCode);
      jsonRequest.put("durationreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
      jsonRequest.put("fareBasisCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareBasisCode"));
      jsonRequest.put("fareClassOfServicereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareClassOfService"));
      jsonRequest.put("flightDesignatorreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDesignator"));
      jsonRequest.put("specialIdreturn", segmentObjectDown.getBoolean("specialId"));
      jsonRequest.put("flightDetailRefKeyreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDetailRefKey"));
      jsonRequest.put("flightNamereturn", segmentObjectDown.getString("engineID"));
      jsonRequest.put("flightNumberreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightNumber"));
      jsonRequest.put("groupreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("group"));
      jsonRequest.put("connectingreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getBoolean("connecting"));
      jsonRequest.put("numberOfStopsreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("numberOfStops"));
      jsonRequest.put("originreturn", destinationCode);
      jsonRequest.put("providerCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("providerCode"));
      jsonRequest.put("remarkreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("remarks"));
      jsonRequest.put("soldreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getInt("sold"));
      jsonRequest.put("statusreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("status"));
      jsonRequest.put("deeplinkreturn", segmentObjectDown.getString("deeplink"));
      jsonRequest.put("engineIDreturn", segmentObjectDown.getString("engineID"));
      jsonRequest.put("ssrFarereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getBoolean("ssrFare"));
      jsonRequest.put("cachereturn", segmentObjectDown.getBoolean("cache"));
      jsonRequest.put("fareIndicatorreturn", 0);
      jsonRequest.put("basicFarereturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getDouble("basicFare"));
      jsonRequest.put("exchangeRatereturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getDouble("exchangeRate"));
      jsonRequest.put("baseTransactionAmountreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
      jsonRequest.put("bookFaresreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares"));
      jsonRequest.put("cancelPenaltyreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
      jsonRequest.put("changePenaltyreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
      jsonRequest.put("equivCurrencyCodereturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("equivCurrencyCode"));
      jsonRequest.put("fareInfoKeyreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoKey"));
      jsonRequest.put("fareInfoValuereturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoValue"));
      jsonRequest.put("totalFareWithOutMarkUpreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
      jsonRequest.put("totalTaxWithOutMarkUpreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
      jsonRequest.put("fareRulereturn", segmentObjectDown.getString("fareRule"));
      jsonRequest.put("holdBookingreturn", segmentObjectDown.getBoolean("holdBooking"));
      jsonRequest.put("internationalreturn", segmentObjectDown.getBoolean("international"));
      jsonRequest.put("roundTripreturn", segmentObjectDown.getBoolean("roundTrip"));
      jsonRequest.put("specialreturn", segmentObjectDown.getBoolean("special"));
      jsonRequest.put("searchIdreturn", segmentObjectDown.getBoolean("specialId"));
      jsonRequest.put("journeyIndexreturn", segmentObjectDown.getInt("journeyIndex"));
      jsonRequest.put("memoryCreationTimereturn", "/Date(1499158249483+0530)/");

      jsonRequest.put("markUPreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
      jsonRequest.put("paxTypereturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
      jsonRequest.put("refundablereturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));
      jsonRequest.put("totalFarereturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
      jsonRequest.put("totalTaxreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
      jsonRequest.put("transactionAmountreturn", segmentObjectDown.getJSONArray("fares").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));
      jsonRequest.put("adults", adultNo);
      jsonRequest.put("childs", childNo);
      jsonRequest.put("firstNamechild", childFName);
      jsonRequest.put("lastNamechild", childLName);
      jsonRequest.put("firstNameinfant", infantFName);
      jsonRequest.put("lastNameinfant", infantLName);
      JSONObject jsonObject = new JSONObject(latesttravel);

      JSONObject jsonObject1 = new JSONObject();
      jsonObject1.put("Tickets", latesttravel);
      longLog(latesttravel);
      String details1 = jsonObject1.toString();
      jsonRequest.put("ticketDetails", new JSONObject(details1).toString());


      longLog(jsonRequest.toString());

      if (!flightjsonUp.equalsIgnoreCase("{}") || !flightjsondown.equalsIgnoreCase("{}")) {
        JSONArray jsonArray = new JSONArray();
        contingFlight.put("emailAddress", etBookFlightEmail.getText().toString());
        contingFlight.put("mobileNumber", etBookFlightMobile.getText().toString());
        contingFlight.put("visatype", "Employee Visa");
        contingFlight.put("traceId", "AYTM00011111111110001");
        contingFlight.put("androidBooking", true);
        contingFlight.put("domestic", true);
        contingFlight.put("engineID", segmentObject.getString("engineID"));
        contingFlight.put("engineIDList", jsonArray.put(segmentObject.getString("engineID")));
        JSONObject paymentDetails = new JSONObject();
        paymentDetails.put("bookingAmount", totalAmount);
        paymentDetails.put("bookingCurrencyCode", "INR");
        contingFlight.put("paymentDetails", paymentDetails);
        JSONObject flightSearchDetails = new JSONObject();
        flightSearchDetails.put("beginDate", dateOfJourney);
        flightSearchDetails.put("destination", destinationCode);
        flightSearchDetails.put("origin", sourceCode);
        JSONObject flightSearchSecond = new JSONObject();
        flightSearchSecond.put("beginDate", dateOfReturn);
        flightSearchSecond.put("destination", sourceCode);
        flightSearchSecond.put("origin", destinationCode);
        longLog(flightSearchSecond.toString());
        JSONArray jsonArray1 = new JSONArray();
        jsonArray1.put(flightSearchDetails);
        jsonArray1.put(flightSearchSecond);
        longLog(jsonArray1.toString());
        contingFlight.put("flightSearchDetails", new JSONArray().put(flightSearchDetails).put(flightSearchSecond));
        JSONArray flighDetailsArray = new JSONArray();
        for (int i = 0; i < materialEditAdultFirstName.size(); i++) {
          JSONObject travellerAdultDetails = new JSONObject();
          travellerAdultDetails.put("fName", materialEditAdultFirstName.get(i).getText().toString());
          travellerAdultDetails.put("lName", materialEditAdultLastName.get(i).getText().toString());
          String gender = materialEditAdultAge.get(i).getText().toString();
          travellerAdultDetails.put("dob", materialEditAdultDOB.get(i).getText().toString());
          if (gender.equalsIgnoreCase("MALE")) {
            travellerAdultDetails.put("title", "Mr");
            travellerAdultDetails.put("gender", gender);

          } else {
            travellerAdultDetails.put("title", "Miss");
            travellerAdultDetails.put("gender", gender);
          }
          if (travellerAdultDetails.has("type")) {
            travellerAdultDetails.remove("type");
            travellerAdultDetails.put("type", "Adult");
          } else {
            travellerAdultDetails.put("type", "Adult");
          }
          travellerAdultDetails.put("dob", materialEditAdultDOB.get(i).getText().toString());
//                if (intentional) {
//          travellerAdultDetails.put("passNo", materialEditAdultPassport.get(i).getText().toString());
//          travellerAdultDetails.put("passExpDate", materialEditAdultExpriDate.get(i).getText().toString());
//                }
          flighDetailsArray.put(travellerAdultDetails);

        }

        for (int i = 0; i < materialEditChildFirstName.size(); i++) {
          JSONObject travellerAdultDetails = new JSONObject();
          travellerAdultDetails.put("fName", materialEditChildFirstName.get(i).getText().toString());
          travellerAdultDetails.put("lName", materialEditChildLastName.get(i).getText().toString());
          travellerAdultDetails.put("dob", materialEditAdultDOB.get(i).getText().toString());
          String gender = materialEditChildAge.get(i).getText().toString();
          if (gender.equalsIgnoreCase("MALE")) {
            travellerAdultDetails.put("title", "Mr");
            travellerAdultDetails.put("gender", gender);

          } else {
            travellerAdultDetails.put("title", "Miss");
            travellerAdultDetails.put("gender", gender);
          }
          if (travellerAdultDetails.has("type")) {
            travellerAdultDetails.remove("type");
            travellerAdultDetails.put("type", "Child");
          } else {
            travellerAdultDetails.put("type", "Child");
          }
          travellerAdultDetails.put("dob", materialEditChildDOB.get(i).getText().toString());
//                if (intentional) {
//          travellerAdultDetails.put("passNo", materialEditChildPassport.get(i).getText().toString());
//          travellerAdultDetails.put("passExpDate", materialEditChildExpriDate.get(i).getText().toString());
//                }
          flighDetailsArray.put(travellerAdultDetails);

        }

//                JSONObject travelChildDetails = new JSONObject();
//                //child
//                for (int i = 0; i < llFlightChild.getChildCount(); i++) {
//                    View view = llFlightChild.getChildAt(i);
//                    MaterialEditText etDynamic = (MaterialEditText) view;
//                    if ((i % 2) == 0) {
//                        travelChildDetails.put("fName", etDynamic.getText().toString());
//                    } else {
//                        travelChildDetails.put("lName", etDynamic.getText().toString());
//                    }
//
//                    travelChildDetails.put("type", "Child");
//                    if (gender.equals("MALE")) {
//                        travelChildDetails.put("title", "Mr");
//                        travelChildDetails.put("gender", gender);
//
//                    } else {
//                        travelChildDetails.put("title", "Miss");
//                        travelChildDetails.put("gender", gender);
//                    }
//
//                }
//                //infats

        for (int i = 0; i < materialEditinfantfirstName.size(); i++) {
          JSONObject travellerAdultDetails = new JSONObject();
          travellerAdultDetails.put("fName", materialEditinfantfirstName.get(i).getText().toString());
          travellerAdultDetails.put("lName", materialEditinfantLastName.get(i).getText().toString());
          String gender = materialEditinfantAge.get(i).getText().toString();
          if (gender.equalsIgnoreCase("MALE")) {
            travellerAdultDetails.put("title", "Mr");
            travellerAdultDetails.put("gender", gender);

          } else {
            travellerAdultDetails.put("title", "Miss");
            travellerAdultDetails.put("gender", gender);
          }
          if (travellerAdultDetails.has("type")) {
            travellerAdultDetails.remove("type");
            travellerAdultDetails.put("type", "Infant");
          } else {
            travellerAdultDetails.put("type", "Infant");
          }
          travellerAdultDetails.put("dob", materialEditinfantDOB.get(i).getText().toString());
//          travellerAdultDetails.put("passNo", materialEditinfantPassport.get(i).getText().toString());
//          travellerAdultDetails.put("passExpDate", materialEditinfantExpriDate.get(i).getText().toString());
          flighDetailsArray.put(travellerAdultDetails);

        }
//
//                JSONObject infants = new JSONObject();
//                for (int i = 0; i < llFlightInfant.getChildCount(); i++) {
//                    View view = llFlightInfant.getChildAt(i);
//                    MaterialEditText etDynamic = (MaterialEditText) view;
//                    if ((i % 2) == 0) {
//                        infants.put("fName", etDynamic.getText().toString());
//                    } else {
//                        infants.put("lName", etDynamic.getText().toString());
//                    }
//
//                    infants.put("type", "Child");
//                    if (gender.equals("MALE")) {
//                        infants.put("title", "Mr");
//                        infants.put("gender", gender);
//
//                    } else {
//                        infants.put("title", "Miss");
//                        infants.put("gender", gender);
//                    }
//                }
//

        contingFlight.put("travellerDetails", flighDetailsArray);
        contingFlight.put("sessionId", session.getUserSessionId());
        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("Tickets", latesttravel);
        String details2 = jsonObject2.toString();
        contingFlight.put("ticketDetails", new JSONObject(details2).toString());
        Log.i("valueJSON", flighDetailsArray.toString());
      }

    } catch (JSONException e)

    {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null)

    {
      if (!flightjsonUp.equalsIgnoreCase("{}") && !flightjsondown.equalsIgnoreCase("{}")) {
        Intent intent = new Intent(FlightBookingFormReturnActivity.this, FlightPaymentActivity.class);
        intent.putExtra("Amount", Double.parseDouble(totalAmount));
        intent.putExtra("flightbooking", contingFlight.toString());
        longLog(contingFlight.toString());
        intent.putExtra("conting", true);
        intent.putExtra("roundTrip", true);
        startActivity(intent);
      } else {
        Intent intent = new Intent(FlightBookingFormReturnActivity.this, FlightPaymentActivity.class);
        intent.putExtra("Amount", Double.parseDouble(totalAmount));
        intent.putExtra("flightbooking", jsonRequest.toString());
        intent.putExtra("conting", false);
        intent.putExtra("roundTrip", true);
        startActivity(intent);

      }

//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_CHECKOUT_ROUND, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        Log.i("CheckOut Resonse", response.toString());
//                        String code = response.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            sendRefresh();
//                            loadDlg.dismiss();
//                            showSuccessDialog();
//                            Toast.makeText(getApplicationContext(),"S00",Toast.LENGTH_SHORT).show();
//
//                        }else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        }
//
//                        else {
//                            String message = response.getString("message");
//                            loadDlg.dismiss();
//                            CustomToast.showMessage(FlightBookingFormReturnActivity.this, message);
//
//                        }
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(FlightBookingFormReturnActivity.this, getResources().getString(R.string.server_exception2));
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(FlightBookingFormReturnActivity.this, NetworkErrorHandler.getMessage(error, FlightBookingFormReturnActivity.this));
//                    error.printStackTrace();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    return map;
//                }
//
//            };
//
//            int socketTimeout = 6000000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }

  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(FlightBookingFormReturnActivity.this).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(FlightBookingFormReturnActivity.this).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(FlightBookingFormReturnActivity.this, R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(FlightBookingFormReturnActivity.this, "Booked Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        finish();

      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source =
      "<b><font color=#000000> Airline Name </font></b>" + "<font color=#000000>" + engineId + "</font><br>" +
        "<b><font color=#000000> Date: </font></b>" + "<font>" + dateOfJourney + "</font><br>" +
        "<b><font color=#000000> Please check provided email for detail. </font></b>" + "<font>";
    return source;
  }

  private void getexiper(final MaterialEditText etPassportExpiry) {
    final Calendar c = Calendar.getInstance();
    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH);
    int day = c.get(Calendar.DAY_OF_MONTH);
    DatePickerDialog datePickerDialog = new DatePickerDialog(FlightBookingFormReturnActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
      @Override
      public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String formattedDay, formattedMonth;
        if (dayOfMonth < 10) {
          formattedDay = "0" + dayOfMonth;
        } else {
          formattedDay = dayOfMonth + "";
        }

        if ((monthOfYear + 1) < 10) {
          formattedMonth = "0" + String.valueOf(monthOfYear + 1);
        } else {
          formattedMonth = String.valueOf(monthOfYear + 1) + "";
        }
        etPassportExpiry.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));
      }
    }, 1990, 01, 01);
    datePickerDialog.show();
    datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
  }

  public void getDob(final MaterialEditText materialEditText, final String adult) {
    DatePickerDialog pickerDialog = new DatePickerDialog(FlightBookingFormReturnActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
      @Override
      public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String formattedDay, formattedMonth;
        if (dayOfMonth < 10) {
          formattedDay = "0" + dayOfMonth;
        } else {
          formattedDay = dayOfMonth + "";
        }

        if ((monthOfYear + 1) < 10) {
          formattedMonth = "0" + String.valueOf(monthOfYear + 1);
        } else {
          formattedMonth = String.valueOf(monthOfYear + 1) + "";
        }
        int Age = getAge(year, Integer.parseInt(formattedMonth), Integer.parseInt(formattedDay));
        if (adult.equalsIgnoreCase("adult")) {
          if (Age >= 12) {
            materialEditText.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));

          } else {
            materialEditText.setText("");
            CustomToast.showMessage(FlightBookingFormReturnActivity.this, "Adult Age should be greater than or equal to 12");
          }
        } else if (adult.equalsIgnoreCase("child")) {
          if (Age >= 2 && Age <= 12) {
            materialEditText.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));

          } else {
            materialEditText.setText("");
            CustomToast.showMessage(FlightBookingFormReturnActivity.this, "Child Age should be minmum 2years maximum 12 years");
          }

        } else if (adult.equalsIgnoreCase("infantNo")) {
          if (Age < 2) {
            materialEditText.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));

          } else {
            materialEditText.setText("");
            CustomToast.showMessage(FlightBookingFormReturnActivity.this, "Adult Age should be greater than or equal to 12");
          }

        }
      }
    }, 1990, 01, 01);
    pickerDialog.show();

  }

  private int getAge(int year, int month, int day) {
    Calendar dob = Calendar.getInstance();
    Calendar today = Calendar.getInstance();

    dob.set(year, month, day);

    int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

    if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
      age--;
    }


    return age;
  }
}
