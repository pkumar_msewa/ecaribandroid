package in.msewa.ecarib.activity.adventureActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.regex.Pattern;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.ecarib.R;

public class SnowCityOrder extends AppCompatActivity {
    private EditText etSnowName, etSnowEmail, etSnowType, etSnowMobile, etSnowBookingDate;
    private String Name, email;
    private TextView tvAmount, tvSnowTickerQty;
    private TextInputLayout ilSnowName, ilSnowEmail, ilSnowType, ilSnowMobile, ilSnowBookingDate;
    private Button btnSnowSubmit, btnAddItem, btnRemoveItem;
    private LoadingDialog loadDlg;
    int ticketQty = 1;
    double totalPrice = 0;

    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snow_city_order);

        loadDlg = new LoadingDialog(SnowCityOrder.this);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvAmount = (TextView) findViewById(R.id.tvAmount);
        tvSnowTickerQty = (TextView) findViewById(R.id.tvSnowTickerQty);

        etSnowName = (EditText) findViewById(R.id.etSnowName);
        etSnowEmail = (EditText) findViewById(R.id.etSnowEmail);
        etSnowType = (EditText) findViewById(R.id.etSnowType);
        etSnowMobile = (EditText) findViewById(R.id.etSnowMobile);
        etSnowBookingDate = (EditText) findViewById(R.id.etSnowBookingDate);

        ilSnowName = (TextInputLayout) findViewById(R.id.ilSnowName);
        ilSnowEmail = (TextInputLayout) findViewById(R.id.ilSnowEmail);
        ilSnowType = (TextInputLayout) findViewById(R.id.ilSnowType);
        ilSnowMobile = (TextInputLayout) findViewById(R.id.ilSnowMobile);
        ilSnowBookingDate = (TextInputLayout) findViewById(R.id.ilSnowBookingDate);

        tvAmount.setText("\u20B9 " +" "+ getIntent().getIntExtra("PRICE",0));

        btnSnowSubmit = (Button) findViewById(R.id.btnSnowSubmit);
        btnAddItem = (Button) findViewById(R.id.btnAddItem);
        btnRemoveItem = (Button) findViewById(R.id.btnRemoveItem);

        etSnowMobile.addTextChangedListener(new MyTextWatcher(etSnowMobile));
        etSnowEmail.addTextChangedListener(new MyTextWatcher(etSnowEmail));

        String Type = getIntent().getStringExtra("TYPE");
                etSnowType.setText(Type);

        etSnowBookingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (etSnowBookingDate.getText().toString() != null && !etSnowBookingDate.getText().toString().isEmpty()) {
                    String[] dateSplit = etSnowBookingDate.getText().toString().split("-");

                    int mYear = Integer.valueOf(dateSplit[2]);
                    int mMonth = Integer.valueOf(dateSplit[1]) - 1;
                    int mDay = Integer.valueOf(dateSplit[0]);

                    DatePickerDialog dialog = new DatePickerDialog(SnowCityOrder.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etSnowBookingDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));

                        }
                    }, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setCustomTitle(null);
                    dialog.setTitle("Select Booking Date");
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(SnowCityOrder.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etSnowBookingDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setTitle("Select Booking Date");
                    dialog.setCustomTitle(null);
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                }

            }
        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(tvSnowTickerQty.getText().toString());
                ticketQty = qty;
                Log.i("NUMBER", "qty : " + qty);
                qty++;
                int price = getIntent().getIntExtra("PRICE",0);
                price = price * qty;
                tvSnowTickerQty.setText(Integer.toString(qty));
                tvAmount.setText("\u20B9 " +" "+ Integer.toString(price));
                totalPrice = (double) price;
                ticketQty = qty;

            }
        });

        btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(tvSnowTickerQty.getText().toString());
                Log.i("NUMBER", "qty : " + qty);
                if (qty > 1) {
                    qty--;
                    int price = getIntent().getIntExtra("PRICE",0);
                    price = price * qty;
                    tvSnowTickerQty.setText(Integer.toString(qty));
                    tvAmount.setText("\u20B9 "+" "+ Integer.toString(price));
                    totalPrice = (double) price;
                    ticketQty = qty;
                }


            }
        });

        btnSnowSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });


    }

    private void submitForm() {

        if (!validateName()) {
            return;
        }if (!validateEmail()) {
            return;
        }if(!validateTicketType()){
            return;
        }if (!validateMobile()) {
            return;
        }if (!validateBookingDate()) {
            return;
        }
        if(totalPrice==0){
            int price = getIntent().getIntExtra("PRICE",0);
            totalPrice = price;
        }

        String name, email, type, mobile, date;
        name = etSnowName.getText().toString();
        email = etSnowEmail.getText().toString();
        type = etSnowType.getText().toString();
        mobile = etSnowMobile.getText().toString();
        date = etSnowBookingDate.getText().toString();

        Log.i("DETAILS", "name : "+name+" email : "+email+" type : "+type+" mobile : "+mobile+" date : "+date+" Quantity : "+ticketQty+" Amount : "+totalPrice);
        CustomToast.showMessage(getApplicationContext(), "name : "+name+" email : "+email+" type : "+type+" mobile : "+mobile+" date : "+date+" Quantity : "+ticketQty+" Amount : "+totalPrice);
    }

    private boolean validateName() {
        if (etSnowName.getText().toString().trim().isEmpty()) {
            ilSnowName.setError("Enter Name");
            requestFocus(etSnowName);
            return false;
        } else {
            ilSnowName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = etSnowEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            ilSnowEmail.setError("Enter valid email");
            requestFocus(etSnowEmail);
            return false;
        } else {
            ilSnowEmail.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showTicketType() {
        final CharSequence[] year = {"Snow City", "9-D Cinema"};

        AlertDialog.Builder builder = new AlertDialog.Builder(SnowCityOrder.this);
        builder.setTitle("Select Ticket Type");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setItems(year, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                etSnowType.setText(year[i]);

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private boolean validateTicketType() {
        if (etSnowType.getText().toString().trim().isEmpty()) {
            ilSnowType.setError("Select Ticket Type");
            requestFocus(etSnowType);
            return false;
        } else {
            ilSnowType.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etSnowMobile.getText().toString().trim().isEmpty() || etSnowMobile.getText().toString().trim().length() < 10) {
            ilSnowMobile.setError("Enter 10 digit no");
            requestFocus(etSnowMobile);
            return false;
        } else {
            ilSnowMobile.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateBookingDate() {
        if (etSnowBookingDate.getText().toString().trim().isEmpty()) {
            ilSnowBookingDate.setError("Enter Booking Date");
            requestFocus(etSnowBookingDate);
            return false;
        } else {
            ilSnowBookingDate.setErrorEnabled(false);
        }

        return true;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etSnowMobile) {
                validateMobile();
            } else if(i == R.id.etSnowEmail){
                validateEmail();
            }

        }
    }
}
