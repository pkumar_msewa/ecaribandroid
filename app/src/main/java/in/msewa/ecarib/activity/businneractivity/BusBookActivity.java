package in.msewa.ecarib.activity.businneractivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomSuccessDialogBus;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.BusBoardingPointModel;
import in.msewa.model.BusListModel;
import in.msewa.model.BusSeatModel;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.util.SecurityUtil;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class BusBookActivity extends AppCompatActivity {

  private LinearLayout llBusBookPassengers;
  private MaterialEditText etBusBookEmail, etBusBookMobile, etBusBookAddress, etBusBookPostal;
  private MaterialEditText etBusBookState, etBusBookCity;

  private MaterialEditText etBusBookIDProofType, etBusBookIDProofCardNo, etBusBookIDProofIssuedBy;
  private MaterialEditText etBusBookBoardingLocation, etBusBookDroppingLocation;

  private BusBoardingPointModel boardingPointModels;
  private BusBoardingPointModel droppingPointModels;

  private long desId, sourceId;

  private HashMap<String, BusSeatModel> seatSelectedMap;

//  private String boardingPointId, droppingPointId, boardingTimePrime, droppingTimePrime, boardingTime, droppingTime;


  private boolean cancel;
  private View focusView;

  private ArrayList<MaterialEditText> listEtPassengerFName = new ArrayList<>();
  private ArrayList<MaterialEditText> listEtPassengerLName = new ArrayList<>();
  private ArrayList<MaterialEditText> listEtPassengerAge = new ArrayList<>();
  private ArrayList<MaterialEditText> listEtPassengerGender = new ArrayList<>();
  private ArrayList<MaterialEditText> listEtPassengerTitle = new ArrayList<>();
  private ArrayList<String> listSeatNo = new ArrayList<>();
  private ArrayList<String> listSeatType = new ArrayList<>();
  private ArrayList<String> listFare = new ArrayList<>();
  private ArrayList<String> listSeatId = new ArrayList<>();

  private LoadingDialog loadingDialog;
  private JSONObject jsonRequest;
  private String tag_json_obj = "json_travel";

  private String genderParam = "";
  private String ageParam = "";
  private String nameParam = "";

  private String titleParam = "";
  private String seatNoParam = "";
  private String seatCodeParam = "";
  private String seatDeatils = "";
  private String totalAmount = "";


  private String fareParam = "";
  private String serviceChargeParam = "";
  private String serviceTaxParam = "";

  private BusListModel busListModel;

  private String refNo = "12345";

  private UserModel session = UserModel.getInstance();
  private String GetTxnIdUpdated;

  private static boolean isValidEmail(String email) {
    return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);


    loadingDialog = new LoadingDialog(BusBookActivity.this);
    setContentView(R.layout.activity_bus_booking);

    //press back button in toolbar

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

    llBusBookPassengers = (LinearLayout) findViewById(R.id.llBusBookPassengers);
    etBusBookEmail = (MaterialEditText) findViewById(R.id.etBusBookEmail);
    etBusBookMobile = (MaterialEditText) findViewById(R.id.etBusBookMobile);
    etBusBookAddress = (MaterialEditText) findViewById(R.id.etBusBookAddress);
    etBusBookPostal = (MaterialEditText) findViewById(R.id.etBusBookPostal);
    etBusBookCity = (MaterialEditText) findViewById(R.id.etBusBookCity);
    etBusBookState = (MaterialEditText) findViewById(R.id.etBusBookState);

    if (session.getUserEmail() != null) {
      etBusBookEmail.setText(session.getUserEmail());
    }
    if (session.getUserMobileNo() != null) {
      etBusBookMobile.setText(session.getUserMobileNo());
    }
    if (session.getUserAddress() != null) {
      etBusBookAddress.setText(session.getUserAddress());
    }


    etBusBookIDProofType = (MaterialEditText) findViewById(R.id.etBusBookIDProofType);
    etBusBookIDProofCardNo = (MaterialEditText) findViewById(R.id.etBusBookIDProofCardNo);
    etBusBookIDProofIssuedBy = (MaterialEditText) findViewById(R.id.etBusBookIDProofIssuedBy);

    etBusBookBoardingLocation = (MaterialEditText) findViewById(R.id.etBusBookBoardingLocation);
    etBusBookDroppingLocation = (MaterialEditText) findViewById(R.id.etBusBookDroppingLocation);

    boardingPointModels = getIntent().getParcelableExtra("busBoardingPointModels");
    droppingPointModels = getIntent().getParcelableExtra("busDroppingPointModels");
    GetTxnIdUpdated = getIntent().getStringExtra("GetTxnIdUpdated");
    Button btnBusBook = (Button) findViewById(R.id.btnBusBook);

    desId = getIntent().getLongExtra("desId", 0);
    sourceId = getIntent().getLongExtra("sourceId", 0);
    seatSelectedMap = (HashMap<String, BusSeatModel>) getIntent().getSerializableExtra("SeatHashValue");
    busListModel = getIntent().getParcelableExtra("busValues");
    etBusBookBoardingLocation.setText(boardingPointModels.getBoardingLocation());
    etBusBookDroppingLocation.setText(droppingPointModels.getBoardingLocation());
//    etBusBookDroppingLocation.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        showDroppingDialog();
//      }
//    });

//    etBusBookBoardingLocation.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        showBoardingDialog();
//      }
//    });
//
    etBusBookIDProofType.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String[] items = {"Pan Card", "Licence", "Adhar Card", "Passport", "Other"};
        android.support.v7.app.AlertDialog.Builder idDialog =
          new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
        idDialog.setTitle("Select your Id type");
        idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
          }
        });
        idDialog.setItems(items, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int position) {
            etBusBookIDProofType.setText(items[position].toString().trim());
          }

        });

        idDialog.show();
      }
    });

    etBusBookIDProofIssuedBy.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String[] items = {"Government", "Other"};
        android.support.v7.app.AlertDialog.Builder idDialog =
          new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
        idDialog.setTitle("Card Issued by");
        idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
          }
        });
        idDialog.setItems(items, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int position) {
            etBusBookIDProofIssuedBy.setText(items[position].toString().trim());
          }

        });
        idDialog.show();
      }
    });

    btnBusBook.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        genderParam = "";
        ageParam = "";
        nameParam = "";
        titleParam = "";
        seatNoParam = "";
        fareParam = "";
        serviceChargeParam = "";
        serviceTaxParam = "";
//                promoteBooking();
        validateFields();
      }
    });

    generateViewsForSeat();
  }

  private void validateFields() {
    etBusBookEmail.setError(null);
    etBusBookMobile.setError(null);
    etBusBookAddress.setError(null);
    etBusBookPostal.setError(null);
    etBusBookIDProofType.setError(null);
    etBusBookIDProofCardNo.setError(null);
    etBusBookIDProofIssuedBy.setError(null);
    etBusBookBoardingLocation.setError(null);
    etBusBookDroppingLocation.setError(null);
    cancel = false;

    checkEmail(etBusBookEmail.getText().toString());
    checkMobileNumber(etBusBookMobile.getText().toString());
    checkAddress(etBusBookAddress.getText().toString());
//        checkPostal(etBusBookPostal.getText().toString());
//        checkCity(etBusBookCity.getText().toString());
//        checkState(etBusBookState.getText().toString());


//        checkIdType(etBusBookIDProofType.getText().toString());
//        checkIdNo(etBusBookIDProofCardNo.getText().toString());
//        checkIdIssuedBy(etBusBookIDProofIssuedBy.getText().toString());

    checkBoardingPoint(etBusBookBoardingLocation.getText().toString());

    checkPassengerInfo();

    if (cancel) {
      focusView.requestFocus();
    } else {
      getTransactionId();

    }
  }

  private void checkMobileNumber(String mNumber) {
    CheckLog inviteNumberCheckLog = PayingDetailsValidation.checkMobileTenDigit(mNumber);
    if (!inviteNumberCheckLog.isValid) {
      etBusBookMobile.setError(getString(inviteNumberCheckLog.msg));
      focusView = etBusBookMobile;
      cancel = true;
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
  private void checkEmail(String email) {
//
    if (email.isEmpty() || !isValidEmail(email)) {
      etBusBookEmail.setError("Enter valid Email");
      focusView = etBusBookEmail;
      cancel = true;
    } else if (!email.substring(0, 1).matches("^[a-z|A-Z|]+[a-z|A-Z|0-9|\\s]*")) {
      etBusBookEmail.setError("Enter valid Email");
      focusView = etBusBookEmail;
      cancel = true;
    }
  }

  private void checkAddress(String address) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(address);
    if (!inviteNameCheckLog.isValid) {
      etBusBookAddress.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookAddress;
      cancel = true;
    }

  }

  private void checkPostal(String postal) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
    if (!inviteNameCheckLog.isValid) {
      etBusBookPostal.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookPostal;
      cancel = true;
    }

  }


  private void checkCity(String city) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(city);
    if (!inviteNameCheckLog.isValid) {
      etBusBookCity.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookCity;
      cancel = true;
    }

  }

  private void checkState(String state) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(state);
    if (!inviteNameCheckLog.isValid) {
      etBusBookState.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookState;
      cancel = true;
    }

  }

  private void checkIdType(String postal) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
    if (!inviteNameCheckLog.isValid) {
      etBusBookIDProofType.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookIDProofType;
      cancel = true;
    }

  }

  private void checkIdNo(String postal) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
    if (!inviteNameCheckLog.isValid) {
      etBusBookIDProofCardNo.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookIDProofCardNo;
      cancel = true;
    }

  }

  private void checkIdIssuedBy(String postal) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
    if (!inviteNameCheckLog.isValid) {
      etBusBookIDProofIssuedBy.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookIDProofIssuedBy;
      cancel = true;
    }

  }

  private void checkBoardingPoint(String boarding) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(boarding);
    if (!inviteNameCheckLog.isValid) {
      etBusBookBoardingLocation.setError(getString(inviteNameCheckLog.msg));
      focusView = etBusBookBoardingLocation;
      cancel = true;
    }

  }

  private void generateViewsForSeat() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
      View passengerDetailView = layoutInflater.inflate(R.layout.view_bus_passanger_detail, null);
      TextView tvBusBookSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvBusBookSeatNo);
      tvBusBookSeatNo.setText(String.valueOf("Seat no. " + entry.getValue().getSeatNo()));
      listFare.add(entry.getValue().getSeatFare());
      listSeatId.add(entry.getValue().getSeatCode());
      listSeatNo.add(entry.getValue().getSeatNo());
      listSeatType.add(entry.getValue().getSeatType());

      llBusBookPassengers.addView(passengerDetailView);

      MaterialEditText etBusBookPassengerTitle = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerTitle);
      listEtPassengerTitle.add(etBusBookPassengerTitle);
      if (entry.getValue().getIsLadiesSeat().equals("true")) {
        etBusBookPassengerTitle.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(final View view) {
            final String[] items = {"Ms.", "Mrs."};
            android.support.v7.app.AlertDialog.Builder idDialog =
              new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
            idDialog.setTitle("Select your Title");
            idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
              }
            });
            idDialog.setItems(items, new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int position) {
                ((MaterialEditText) view).setText(items[position].trim());
              }

            });

            idDialog.show();
          }
        });
      } else {
        etBusBookPassengerTitle.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(final View view) {
            final String[] items = {"Mr.", "Ms.", "Mrs."};
            android.support.v7.app.AlertDialog.Builder idDialog =
              new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
            idDialog.setTitle("Select your Title");
            idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
              }
            });
            idDialog.setItems(items, new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int position) {
                ((MaterialEditText) view).setText(items[position].trim());
              }

            });

            idDialog.show();
          }
        });
      }
      InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
          for (int i = start; i < end; i++) {
            if (!Character.isLetterOrDigit(source.charAt(i))) {
              return "";
            }
          }
          return null;
        }
      };


      MaterialEditText etBusBookPassengerFName = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerFName);
      etBusBookPassengerFName.setClickable(true);
      etBusBookPassengerFName.setFilters(new InputFilter[]{filter});
      listEtPassengerFName.add(etBusBookPassengerFName);

      MaterialEditText etBusBookPassengerLName = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerLName);
      etBusBookPassengerLName.setClickable(true);
      etBusBookPassengerLName.setFilters(new InputFilter[]{filter});
      listEtPassengerLName.add(etBusBookPassengerLName);

      MaterialEditText etBusBookPassengerAge = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerAge);
      etBusBookPassengerAge.setClickable(true);
      listEtPassengerAge.add(etBusBookPassengerAge);

      MaterialEditText etBusBookPassengerGender = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerGender);
      listEtPassengerGender.add(etBusBookPassengerGender);
      if (entry.getValue().getIsLadiesSeat().equals("true")) {
        etBusBookPassengerGender.setText("Female");
      } else {
        etBusBookPassengerGender.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(final View view) {
            final String[] items = {"Male", "Female"};
            android.support.v7.app.AlertDialog.Builder idDialog =
              new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
            idDialog.setTitle("Select your Gender");
            idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
              }
            });
            idDialog.setItems(items, new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int position) {
                ((MaterialEditText) view).setText(items[position].trim());
              }

            });

            idDialog.show();
          }
        });
      }


    }

  }

  private void checkPassengerInfo() {
    for (MaterialEditText et : listEtPassengerTitle) {
      validateField(et.getText().toString(), et);
    }
    for (MaterialEditText et : listEtPassengerFName) {
      validateField(et.getText().toString(), et);
    }
    for (MaterialEditText et : listEtPassengerLName) {
      validateField(et.getText().toString(), et);
    }
    for (MaterialEditText et : listEtPassengerAge) {
      validateAge(et.getText().toString(), et);
    }
    for (MaterialEditText et : listEtPassengerGender) {
      validateField(et.getText().toString(), et);
    }
  }

  private void validateField(String data, MaterialEditText mEt) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(data);
    if (!inviteNameCheckLog.isValid) {
      mEt.setError(getString(inviteNameCheckLog.msg));
      focusView = mEt;
      cancel = true;
    }

  }

  private void validateAge(String data, MaterialEditText mEt) {
    CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(data);
    if (!inviteNameCheckLog.isValid) {
      mEt.setError(getString(inviteNameCheckLog.msg));
      focusView = mEt;
      cancel = true;
    } else if (Integer.valueOf(data) <= 0) {
      mEt.setError("Please enter valid age");
      focusView = mEt;
      cancel = true;
    }

  }

  private String getServiceTaxParam() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      if (serviceTaxParam.equals("")) {
        serviceTaxParam = serviceTaxParam + entry.getValue().getSeatServiceTax();
      } else {
        serviceTaxParam = serviceTaxParam + "~" + entry.getValue().getSeatServiceTax();

      }
    }
    return serviceTaxParam;
  }

  private String getServiceChargeParam() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      if (serviceChargeParam.equals("")) {
        serviceChargeParam = serviceChargeParam + entry.getValue().getSeatOptServiceCharge();
      } else {
        serviceChargeParam = serviceChargeParam + "~" + entry.getValue().getSeatOptServiceCharge();
      }
    }
    return serviceChargeParam;
  }

  private String getFareParam() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      if (fareParam.equals("")) {
        fareParam = fareParam + entry.getValue().getNetFare();
      } else {
        fareParam = fareParam + "~" + entry.getValue().getNetFare();
      }
    }
    return fareParam;
  }

  private String getNameParam() {
    for (int e = 0; e < listEtPassengerFName.size(); e++) {
      if (nameParam.equals("")) {
        nameParam = nameParam + listEtPassengerFName.get(e).getText().toString();
      } else {
        nameParam = nameParam + "~" + listEtPassengerFName.get(e).getText().toString();

      }
    }
    return nameParam;
  }

  private String getTitleParam() {
    for (int e = 0; e < listEtPassengerGender.size(); e++) {
      if (titleParam.equals("")) {
        if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
          titleParam = titleParam + "Mr";
        } else {
          titleParam = titleParam + "Miss";
        }
      } else {
        if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
          titleParam = titleParam + "~" + "Mr";
        } else {
          titleParam = titleParam + "~" + "Miss";
        }
      }
    }
    return titleParam;
  }

  private String getSeatNoParam() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      if (seatNoParam.equals("")) {
        seatNoParam = seatNoParam + entry.getValue().getSeatNo();
      } else {
        seatNoParam = seatNoParam + "," + entry.getValue().getSeatNo();

      }
    }
    return seatNoParam;
  }

  private String getTotalAmount() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      if (totalAmount.equals("")) {
        totalAmount = totalAmount + entry.getValue().getSeatFare();
      } else {
        totalAmount = String.valueOf(Double.parseDouble(totalAmount) + Double.parseDouble(entry.getValue().getSeatFare()));
      }
    }
    return totalAmount;
  }

  private String getSeatDeatils() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      if (seatDeatils.equals("")) {
        seatDeatils = seatDeatils + entry.getValue().getSeatNo() + "@" + entry.getValue().getSeatFare();
      } else {
        seatDeatils = seatDeatils + "#" + entry.getValue().getSeatNo() + "@" + entry.getValue().getSeatFare();
      }
    }
    return seatDeatils;
  }


  private String getSeatCodeParam() {
    for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
      if (seatCodeParam != null && seatCodeParam.equals("")) {
        seatCodeParam = seatCodeParam + entry.getValue().getSeatCode();
      } else if (seatCodeParam != null) {
        seatCodeParam = seatCodeParam + "~" + entry.getValue().getSeatCode();
      } else {
        seatCodeParam = null;
      }
    }
    return seatCodeParam;
  }

  private String getAgeParam() {
    for (int e = 0; e < listEtPassengerAge.size(); e++) {
      if (ageParam.equals("")) {
        ageParam = ageParam + listEtPassengerAge.get(e).getText().toString();
      } else {
        ageParam = ageParam + "~" + listEtPassengerAge.get(e).getText().toString();
      }
    }
    return ageParam;
  }

  private String getGenderParam() {
    for (int e = 0; e < listEtPassengerGender.size(); e++) {
      if (genderParam.equals("")) {
        if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
          genderParam = genderParam + "M";
        } else {
          genderParam = genderParam + "F";
        }
      } else {
        if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
          genderParam = genderParam + "~" + "M";
        } else {
          genderParam = genderParam + "~" + "F";
        }

      }
    }
    return genderParam;
  }

  private JSONObject getGSTDetails() {
    JSONObject gstdetails = new JSONObject();
    try {
      gstdetails.put("phone", "");
      gstdetails.put("email", "");
      gstdetails.put("companyName", "");
      gstdetails.put("address", "");
      gstdetails.put("gstNumber", "");

    } catch (JSONException e) {
      e.printStackTrace();
    }
    return gstdetails;
  }

  private JSONArray getTravellersDetails() {
    JSONArray travellersArray = new JSONArray();
    for (int i = 0; i < seatSelectedMap.size(); i++) {
      JSONObject t = new JSONObject();
      try {
        t.put("title", listEtPassengerTitle.get(i).getText().toString());
        t.put("fName", listEtPassengerFName.get(i).getText().toString());
        t.put("mName", "");
        t.put("lName", listEtPassengerLName.get(i).getText().toString());
        t.put("age", listEtPassengerAge.get(i).getText().toString());
        t.put("gender", listEtPassengerGender.get(i).getText().toString());
        t.put("seatNo", listSeatNo.get(i));
        t.put("seatType", listSeatType.get(i));
        t.put("fare", listFare.get(i));
        t.put("seatId", listSeatId.get(i));
        travellersArray.put(t);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
    return travellersArray;
  }

  private void getTransactionId() {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("mobileNo", etBusBookMobile.getText().toString());
      jsonRequest.put("emailId", etBusBookEmail.getText().toString());
      jsonRequest.put("email", etBusBookEmail.getText().toString());
      jsonRequest.put("idProofId", "");
      jsonRequest.put("idProofNo", "");
      jsonRequest.put("address", etBusBookAddress.getText().toString());
      jsonRequest.put("engineId", busListModel.getEngineId());
      jsonRequest.put("busid", busListModel.getTripId());
      jsonRequest.put("busType", busListModel.getBusType());
      jsonRequest.put("boardId", boardingPointModels.getBoardingId());
      jsonRequest.put("boardLocation", boardingPointModels.getBoardingLocation());
      jsonRequest.put("boardName", etBusBookBoardingLocation.getText().toString());
      jsonRequest.put("arrTime", busListModel.getBusArrivalTime());
      jsonRequest.put("depTime", busListModel.getBusDepTime());
      jsonRequest.put("travelName", busListModel.getBusName());
      jsonRequest.put("source", busListModel.getBusSourceName());
      jsonRequest.put("destination", busListModel.getBusDesName());
      jsonRequest.put("sourceid", sourceId);
      jsonRequest.put("destinationId", desId);
      jsonRequest.put("journeyDate", formatDate(busListModel.getJnrDate()));
      jsonRequest.put("boardpoint", etBusBookBoardingLocation.getText().toString());
      jsonRequest.put("boardprime", boardingPointModels.getBoardingTimePrime());
      jsonRequest.put("boardTime", boardingPointModels.getBoardingTime());
      jsonRequest.put("boardlandmark", "");
      jsonRequest.put("boardContactNo", "");
      jsonRequest.put("dropId", droppingPointModels.getBoardingId());
      jsonRequest.put("dropName", etBusBookDroppingLocation.getText().toString());
      jsonRequest.put("droplocatoin", droppingPointModels.getBoardingLocation());
      jsonRequest.put("dropprime", droppingPointModels.getBoardingTimePrime());
      jsonRequest.put("dropTime", droppingPointModels.getBoardingTime());
      jsonRequest.put("routeId", busListModel.getRouteId());
      jsonRequest.put("seatDetail", getSeatDeatils());
      jsonRequest.put("couponCode", "");
      jsonRequest.put("discount", String.valueOf(busListModel.getDiscount()));
      jsonRequest.put("commission", String.valueOf(busListModel.getCommission()));
      jsonRequest.put("markup", String.valueOf(busListModel.getMarkup()));
      jsonRequest.put("wLCode", "Bus");
      jsonRequest.put("totalFare", Double.parseDouble(getTotalAmount()));
      jsonRequest.put("version", "1.0");
      jsonRequest.put("agentCode", "");
      jsonRequest.put("cancelPolicyList", new JSONArray(busListModel.getBusCancellationPolicy()));
      jsonRequest.put("gstDetails", getGSTDetails());
      jsonRequest.put("travellers", getTravellersDetails());
      jsonRequest.put("tripId", GetTxnIdUpdated);
      if (SecurityUtil.getIMEI(BusBookActivity.this) != null) {
        jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(BusBookActivity.this) + "-" + SecurityUtil.getIMEI(BusBookActivity.this));
      } else {
        jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(BusBookActivity.this));
      }

      try {
//        jsonObject1.put("data", AESCrypt.encrypt(StringEscapeUtils.unescapeJson(jsonRequest.toString()).replaceAll("\\n", "")).replaceAll("\\\\/", ""));
      } catch (Exception e) {
        e.printStackTrace();
      }


    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JSONREQBus", jsonRequest.toString());
      Log.i("URLBUSSEATRANX", ApiUrl.URL_BUS_SEAT_TRANXID);
      String url;
//      if (!ApiUrl.URL_BUS_SEAT_TRANXID.contains("66.207.206.54:8089")) {
//        url = ApiUrl.URL_BUS_SEAT_TRANXID;
//      } else {
      url = ApiUrl.URL_BUS_SEAT_TRANXID_NEW;
//      }
      JsonObjectRequest postReq = null;
      try {
        postReq = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
          @Override
          public void onResponse(JSONObject response) {
            try {
              Log.i("JSONRESBus", response.toString());
              if (response.has("code")) {
                String code = response.getString("code");
                if (code != null && code.equals("S00")) {
                  JSONObject details = response.getJSONObject("details");
                  loadingDialog.dismiss();
                  String transactionid = details.getString("transactionid");
                  String vpQTxnId = details.getString("vpQTxnId");
                  String mpQTxnId = details.getString("mpQTxnId");
                  if (details.has("seatHoldId") && details.has("priceRecheckAmt")) {
                    String seatHoldId = details.getString("seatHoldId");
                    String priceRecheckAmt = details.getString("priceRecheckAmt");
                    showSuccessDialog(transactionid, vpQTxnId, mpQTxnId, seatHoldId, priceRecheckAmt, true);
                  } else {
                    showSuccessDialog(transactionid, vpQTxnId, mpQTxnId, "", getTotalAmount(), false);
                  }
                } else if (code != null && code.equals("ng   mesaggegeF03")) {
                  loadingDialog.dismiss();
                  showInvalidSessionDialog(response.getString("message"));
                } else {
                  loadingDialog.dismiss();
                  if (response.has("message") && response.getString("message") != null) {
                    String message = response.getString("message");
                    CustomToast.showMessage(getApplicationContext(), message);
                  } else {
                    CustomToast.showMessage(getApplicationContext(), "Error message is null");
                  }

                }
              }
            } catch (JSONException e) {
              loadingDialog.dismiss();
              CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }

          }
        }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            loadingDialog.dismiss();
            CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
            error.printStackTrace();
          }
        }) {
        };
      } catch (Exception e) {
        e.printStackTrace();
      }

      int socketTimeout = 1800000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void promoteBooking(String trxId, String pqTrxId, String mdexTxnId, String seatHold, String rechckAmount, boolean recheckamounts) {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    JSONObject jsonObject1 = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("transactionId", trxId);
      jsonRequest.put("vpqTxnId", pqTrxId);
      jsonRequest.put("mdexTxnId", mdexTxnId);
      if (recheckamounts) {
        jsonRequest.put("seatHoldId", seatHold);
        jsonRequest.put("amount", rechckAmount);
      }
      try {
//        jsonObject1.put("data", AESCrypt.encrypt(StringEscapeUtils.unescapeJson(jsonRequest.toString()).replaceAll("\\n", "")).replaceAll("\\\\/", ""));
      } catch (Exception e) {
        e.printStackTrace();
      }

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("BOOKINGREQ", jsonRequest.toString());
      Log.i("BOOKINGURL", ApiUrl.URL_BUS_SEAT_BOOKING);
      String url;
//      if (recheckamounts) {
      url = ApiUrl.URL_BUS_SEAT_BOOKING_NEW;
//      } else {
//        url = ApiUrl.URL_BUS_SEAT_BOOKING;
//      }
      JsonObjectRequest postReq = null;
      try {
        postReq = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
          @Override
          public void onResponse(JSONObject response) {
            try {
              if (response.has("code")) {
                String code = response.getString("code");
                String msg = response.getString("message");
                if (code != null && code.equals("S00")) {
                  loadingDialog.dismiss();
                  showPaymentSuccessDialog("Congratulations, Your ticket(s) have been booked. We wish you a happy Journey ", "Payment Successful");

                } else if (code != null && code.equals("F03")) {
                  loadingDialog.dismiss();
                  showInvalidSessionDialog(response.getString("message"));
                } else {
                  loadingDialog.dismiss();
                  if (response.has("message") && response.getString("message") != null) {
                    String message = response.getString("message");
                    CustomToast.showMessage(getApplicationContext(), message);
                  } else {
                    CustomToast.showMessage(getApplicationContext(), "Error message is null");
                  }

                }
              }
            } catch (JSONException e) {
              loadingDialog.dismiss();
              CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }

          }
        }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            loadingDialog.dismiss();
            CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
            error.printStackTrace();
          }
        }) {
        };
      } catch (Exception e) {


      }

      int socketTimeout = 600000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  private void cancelTranx(String pqTrxId) {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("vpqTxnId", pqTrxId);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("BOOKINGREQ", jsonRequest.toString());
      Log.i("BOOKINGURL", ApiUrl.URL_BUS_CANCEL_INITIATION);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_CANCEL_INITIATION, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("BOOKINGRES", response.toString());
            if (response.has("code")) {
              String code = response.getString("code");
              if (code != null && code.equals("S00")) {
                loadingDialog.dismiss();

              } else if (code != null && code.equals("F03")) {
                loadingDialog.dismiss();
                showInvalidSessionDialog(response.getString("message"));
              } else {
                loadingDialog.dismiss();
                if (response.has("message") && response.getString("message") != null) {
                  String message = response.getString("message");
                  CustomToast.showMessage(getApplicationContext(), message);
                } else {
                  CustomToast.showMessage(getApplicationContext(), "Error message is null");
                }

              }
            }
          } catch (JSONException e) {
            loadingDialog.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }

        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          error.printStackTrace();
        }
      }) {
      };

      int socketTimeout = 600000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private String formatDate(String dateString) {
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    Date date = null;
    try {
      date = fmt.parse(dateString);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
    return (fmtOut.format(date)).toString();
  }


//  private void showDroppingDialog() {
//    final ArrayList<String> placeVal = new ArrayList<>();
//
//    for (BusBoardingPointModel placeModel : droppingPointModels) {
//      placeVal.add(placeModel.getBoardingLocation());
//    }
//
//    android.support.v7.app.AlertDialog.Builder placeDialog =
//      new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
//    placeDialog.setTitle("Select Dropping point");
//
//    placeDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//      public void onClick(DialogInterface dialog, int id) {
//        dialog.dismiss();
//      }
//    });
//
//    placeDialog.setItems(placeVal.toArray(new String[placeVal.size()]),
//      new DialogInterface.OnClickListener() {
//        @Override
//        public void onClick(DialogInterface arg0, int i) {
////          etBusBookDroppingLocation.setText(droppingPointModels.get(i).getBoardingLocation());
////          droppingPointId = droppingPointModels.get(i).getBoardingId();
////          droppingTimePrime = droppingPointModels.get(i).getBoardingTimePrime();
////          droppingTime = droppingPointModels.get(i).getBoardingTime();
//        }
//      });
//
//    placeDialog.show();
//  }

//  private void showBoardingDialog() {
//    final ArrayList<String> placeVal = new ArrayList<>();
//
//    for (BusBoardingPointModel placeModel : boardingPointModels) {
//      placeVal.add(placeModel.getBoardingLocation() + "-" + placeModel.getBoardingTime());
//      Log.i("Place Value", placeModel.getBoardingLocation());
//    }
//
//    android.support.v7.app.AlertDialog.Builder placeDialog =
//      new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
//    placeDialog.setTitle("Select Boarding point");
//
//    placeDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//      public void onClick(DialogInterface dialog, int id) {
//        dialog.dismiss();
//      }
//    });
//
//    placeDialog.setItems(placeVal.toArray(new String[placeVal.size()]),
//      new DialogInterface.OnClickListener() {
//        @Override
//        public void onClick(DialogInterface arg0, int i) {
//          etBusBookBoardingLocation.setText(boardingPointModels.get(i).getBoardingLocation());
//          boardingPointId = boardingPointModels.get(i).getBoardingId();
//          boardingTimePrime = boardingPointModels.get(i).getBoardingTimePrime();
//          boardingTime = boardingPointModels.get(i).getBoardingTime();
//        }
//      });
//
//    placeDialog.show();
////  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(BusBookActivity.this, R.string.dialog_title2, Html.fromHtml(message));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendFinish();
        sendLogout();
      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(BusBookActivity.this).sendBroadcast(intent);
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(BusBookActivity.this).sendBroadcast(intent);
  }

  private void sendFinish() {
    Intent intent = new Intent("booking-done");
    intent.putExtra("ticket", "1");
    intent.putExtra("detail", "1");
    LocalBroadcastManager.getInstance(BusBookActivity.this).sendBroadcast(intent);
  }

  public void showSuccessDialog(final String trnxID, final String PQTrnxID, final String mpQTxnId, final String seatHoldID, final String recheckAmount, final boolean recheckAmounts) {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(recheckAmount), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage(recheckAmount));
    }
    CustomSuccessDialogBus builder = new CustomSuccessDialogBus(BusBookActivity.this, "Please, Confirm Payment", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

        promoteBooking(trnxID, PQTrnxID, mpQTxnId, seatHoldID, recheckAmount, recheckAmounts);

      }
    });
    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        cancelTranx(PQTrnxID);
      }
    });
    builder.show();
  }

  public String getSuccessMessage(String amount) {
    return "<b><font color=#000000> Please confirm your booking details:  </font></b><br>" +
      "<b><font color=#000000> From: </font></b>" + "<font>" + busListModel.getBusSourceName() + "</font><br>" +
      "<b><font color=#000000> To: </font></b>" + "<font>" + busListModel.getBusDesName() + "</font><br>" +
      "<b><font color=#000000> Date: </font></b>" + "<font>" + busListModel.getJnrDate() + "</font><br>" +
      "<b><font color=#000000> Travels: </font></b>" + "<font>" + busListModel.getBusTravel() + "</font><br>" +
      "<b><font color=#000000> Seats: </font></b>" + "<font>" + getSeatNoParam() + "</font><br>" +
      "<b><font color=#000000> Total Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
  }

  public void showPaymentSuccessDialog(String message, String Title) {
    CustomSuccessDialog builder = new CustomSuccessDialog(BusBookActivity.this, Title, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
        sendFinish();
        finish();
      }
    });
    builder.show();
  }

}
