//package in.msewa.vpayqwik.activity;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AppCompatActivity;
//import android.text.Html;
//import android.text.Spanned;
//import android.util.Log;
//
//import com.androidnetworking.AndroidNetworking;
//import com.androidnetworking.common.Priority;
//import com.androidnetworking.error.ANError;
//import com.androidnetworking.interfaces.AnalyticsListener;
//import com.androidnetworking.interfaces.JSONObjectRequestListener;
//import com.niki.config.NikiConfig;
//import com.razorpay.Checkout;
//import com.razorpay.PaymentResultListener;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import in.msewa.custom.CustomAlertDialog;
//import in.msewa.custom.CustomBlockDialog;
//import in.msewa.custom.CustomSuccessDialog;
//import in.msewa.custom.CustomToast;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.metadata.AppMetadata;
//import in.msewa.model.BusSaveModel;
//import in.msewa.model.FlightSaveModel;
//import in.msewa.model.UserModel;
//import in.msewa.vpayqwik.R;
//
//public class RazorpayPaymentActivity extends AppCompatActivity implements PaymentResultListener {
//
//  private static final String TAG = "Rozpay";
//  private static String booking_url;
//  private static String bookingrequest;
//  private String ampount;
//  private UserModel userModel = UserModel.getInstance();
//  private static String transcationID;
//
//  @Override
//  protected void onCreate(Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    setContentView(R.layout.activity_razorpay_payment);
//    ampount = getIntent().getStringExtra("amount");
//
//    if (getIntent().getBooleanExtra("splitpayment", false)) {
//      booking_url = getIntent().getStringExtra("URL");
//      bookingrequest = getIntent().getStringExtra("bokkingrequest");
//      try {
//        getInitiated();
//      } catch (JSONException e) {
//        e.printStackTrace();
//      }
//    } else {
//      try {
//        getInitiated();
//      } catch (JSONException e) {
//        e.printStackTrace();
//      }
//    }
//  }
//
//  @Override
//  public void onBackPressed() {
//    if (!shouldAllowBack()) {
//    } else {
//      super.onBackPressed();
//    }
//  }
//
//  private boolean shouldAllowBack() {
//    return false;
//  }
//
//  public void startPayment(String referenceNo) {
//    /**
//     * Instantiate Checkout
//     */
//    Checkout checkout = new Checkout();
//    transcationID = referenceNo;
//
//    /**
//     * Set your logo here
//     */
//    checkout.setImage(R.drawable.toolbar_back);
//    checkout.setFullScreenDisable(true);
//
//    /**
//     * Reference to current activity
//     */
//    final Activity activity = this;
//
//    /**
//     * Pass your payment options to the Razorpay Checkout as a JSONObject
//     */
//    try {
//      JSONObject options = new JSONObject();
//
//      /**
//       * Merchant Name
//       * eg: Rentomojo || HasGeek etc.
//       */
//      options.put("name", "Vpayqwik");
//
//
//      /**
//       * Description can be anything
//       * eg: Order #123123
//       *     Invoice Payment
//       *     etc.
//       */
//      options.put("description", "Order #".concat(referenceNo));
//
//      options.put("currency", "INR");
//
//      /**
//       * Amount is always passed in PAISE
//       * Eg: "500" = Rs 5.00
//       */
//      Double aDouble = Double.parseDouble(getIntent().getStringExtra("amount")) * (100.0);
//      options.put("amount", aDouble.toString());
//
//      checkout.open(activity, options);
//    } catch (Exception e) {
//
//      Log.e(TAG, "Error in starting Razorpay Checkout", e);
//    }
//  }
//
//  @Override
//  public void onPaymentSuccess(String s) {
//
//    JSONObject jsonObject = new JSONObject();
//    try {
//      jsonObject.put("sessionId", userModel.getUserSessionId());
//      jsonObject.put("amount", ampount);
//      jsonObject.put("transactionId", transcationID);
//      jsonObject.put("razorpayPaymentId", s);
//
//    } catch (JSONException e) {
//      e.printStackTrace();
//    }
//    Log.i("error", jsonObject.toString());
//    AndroidNetworking.post(ApiUrl.URL_LOAD_MONEY_REDIRCET)
//      .addJSONObjectBody(jsonObject)
//      .setPriority(Priority.IMMEDIATE)
//      .setTag("vpayqwik")
//      .build().getAsJSONObject(new JSONObjectRequestListener() {
//      @Override
//      public void onResponse(JSONObject response) throws JSONException {
//        String code = response.getString("code");
//        if (code != null && code.equalsIgnoreCase("S00")) {
//          if (getIntent().getBooleanExtra("splitpayment", false)) {
//            promoteRecharge();
//          } else {
//            CustomSuccessDialog customAlertDialog = new CustomSuccessDialog(RazorpayPaymentActivity.this, "Payment Successful", response.getString("Payment Successful"));
//            customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//              @Override
//              public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//                userModel.setUserBalance(ampount);
//                userModel.update();
//                sendRefresh();
//
//              }
//            });
//            customAlertDialog.show();
//          }
//        } else {
//          CustomAlertDialog customAlertDialog = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, response.getString("message"));
//          customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//              dialogInterface.dismiss();
//              sendRefresh();
//
//            }
//          });
//          customAlertDialog.show();
//        }
//
//      }
//
//      @Override
//      public void onError(ANError anError) {
//        CustomAlertDialog customAlertDialog = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, getResources().getString(R.string.server_exception));
//        customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//          @Override
//          public void onClick(DialogInterface dialogInterface, int i) {
//            dialogInterface.dismiss();
//            sendRefresh();
//
//          }
//        });
//        customAlertDialog.show();
//
//      }
//    });
//
//
//  }
//
//  @Override
//  public void onPaymentError(int i, String s) {
//    CustomAlertDialog customAlertDialog = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, s);
//    customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//      @Override
//      public void onClick(DialogInterface dialogInterface, int i) {
//        dialogInterface.dismiss();
//        finish();
//        finishAffinity();
//        startActivity(new Intent(RazorpayPaymentActivity.this, HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
//      }
//    });
//    customAlertDialog.show();
//
//  }
//
//  public void getInitiated() throws JSONException {
//    JSONObject jsonObject = new JSONObject();
//    jsonObject.put("sessionId", userModel.getUserSessionId());
//    jsonObject.put("amount", ampount);
//    AndroidNetworking.post(ApiUrl.URL_LOAD_MONEY_ROZORPAY)
//      .addJSONObjectBody(jsonObject)
//      .setPriority(Priority.IMMEDIATE)
//      .setTag("vpayqwik")
//      .build().getAsJSONObject(new JSONObjectRequestListener() {
//      @Override
//      public void onResponse(JSONObject response) throws JSONException {
//        String code = response.getString("code");
//        String message = response.getString("message");
//        if (code != null && code.equalsIgnoreCase("S00")) {
//          startPayment(response.getString("referenceNo"));
//        } else if (code != null && code.equalsIgnoreCase("F03")) {
//          CustomAlertDialog customAlertDialog = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, message);
//          customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//              dialogInterface.dismiss();
//              promoteLogout();
//            }
//          });
//          customAlertDialog.show();
//        } else {
//          CustomAlertDialog customAlertDialog = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, message);
//          customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//              dialogInterface.dismiss();
//              finish();
//              finishAffinity();
//              startActivity(new Intent(RazorpayPaymentActivity.this, HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
//            }
//          });
//          customAlertDialog.show();
//        }
//      }
//
//      @Override
//      public void onError(ANError anError) {
//        CustomAlertDialog customAlertDialog = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, getResources().getString(R.string.server_exception));
//        customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//          @Override
//          public void onClick(DialogInterface dialogInterface, int i) {
//            dialogInterface.dismiss();
//            finish();
//            finishAffinity();
//            startActivity(new Intent(RazorpayPaymentActivity.this, HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
//          }
//        });
//        customAlertDialog.show();
//      }
//    });
//  }
//
//
//  private void promoteLogout() {
//    JSONObject jsonRequest = new JSONObject();
//    try {
//      jsonRequest.put("sessionId", userModel.getUserSessionId());
//    } catch (JSONException e) {
////            e.printStackTrace();
//      jsonRequest = null;
//    }
//
//    AndroidNetworking.post(ApiUrl.URL_LOGOUT)
//      .addJSONObjectBody(jsonRequest) // posting json
//      .setTag("test")
//      .setPriority(Priority.HIGH)
//      .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
//      .build().setAnalyticsListener(new AnalyticsListener() {
//      @Override
//      public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
//        Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
//      }
//    }).getAsJSONObject(new JSONObjectRequestListener() {
//      @Override
//      public void onResponse(JSONObject response) {
//        try {
//          String code = response.getString("code");
//          if (code != null && code.equals("S00")) {
//            if (NikiConfig.isNikiInitialized()) {
//              NikiConfig.getInstance().logout(RazorpayPaymentActivity.this);
//            }
//            Checkout.clearUserData(RazorpayPaymentActivity.this);
//            SharedPreferences kycValues = getSharedPreferences("kyc", Context.MODE_PRIVATE);
//            kycValues.edit().clear().remove("KYC").apply();
//            UserModel.deleteAll(UserModel.class);
//            BusSaveModel.deleteAll(BusSaveModel.class);
//            FlightSaveModel.deleteAll(FlightSaveModel.class);
//            Intent i = new Intent(getApplicationContext(), LoginRegActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            finish();
//            finishAffinity();
//            startActivity(i);
//          } else {
//            Checkout.clearUserData(RazorpayPaymentActivity.this);
//            if (NikiConfig.isNikiInitialized()) {
//              NikiConfig.getInstance().logout(RazorpayPaymentActivity.this);
//            }
//            SharedPreferences kycValues = getSharedPreferences("kyc", Context.MODE_PRIVATE);
//            kycValues.edit().clear().remove("KYC").apply();
//            UserModel.deleteAll(UserModel.class);
//            BusSaveModel.deleteAll(BusSaveModel.class);
//            FlightSaveModel.deleteAll(FlightSaveModel.class);
//
//            Intent i = new Intent(RazorpayPaymentActivity.this, LoginRegActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            finish();
//            finishAffinity();
//            startActivity(i);
//          }
//        } catch (JSONException e) {
//          CustomToast.showMessage(RazorpayPaymentActivity.this, getResources().getString(R.string.server_exception));
//          e.printStackTrace();
//        }
//      }
//
//      @Override
//      public void onError(ANError anError) {
//        CustomToast.showMessage(RazorpayPaymentActivity.this, getResources().getString(R.string.server_exception));
//      }
//    });
//  }
//
//
//  public void promoteRecharge() throws JSONException {
//
//    AndroidNetworking.post(booking_url)
//      .addJSONObjectBody(new JSONObject(bookingrequest)) // posting json
//      .setTag("test")
//      .setPriority(Priority.HIGH)
//      .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
//      .build().setAnalyticsListener(new AnalyticsListener() {
//      @Override
//      public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
//        Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
//      }
//    }).getAsJSONObject(new JSONObjectRequestListener() {
//      @Override
//      public void onResponse(JSONObject response) throws JSONException {
//        try {
//          String code = response.getString("code");
//          String message = response.getString("message");
//          if (code != null && code.equals("S00")) {
//            String jsonString = response.getString("response");
//            try {
//              if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
//
//                showSuccessDialog(new JSONObject(jsonString), message);
//              } else {
//
//                showSuccessDialog(new JSONObject(jsonString), message);
//              }
//            } catch (NullPointerException e) {
//              e.printStackTrace();
//            }
//          } else if (code != null && code.equals("F03")) {
//
//            CustomAlertDialog customAlertDialog = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, message);
//            customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//              @Override
//              public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//                promoteLogout();
//              }
//            });
//            customAlertDialog.show();
//          } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
//
//            showBlockDialog();
//          } else {
//
//            String values;
//            if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
//              values = "Recharge Failed";
//            } else {
//              values = "Payment Failed";
//            }
//            CustomAlertDialog builder = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, values);
//            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//              @Override
//              public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//              }
//            });
//            builder.show();
//          }
//
//        } catch (JSONException e) {
//          CustomAlertDialog builder = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, getResources().getString(R.string.server_exception2));
//          builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//              dialogInterface.dismiss();
//            }
//          });
//          builder.show();
//          e.printStackTrace();
//        }
//      }
//
//      @Override
//      public void onError(ANError anError) {
//        CustomAlertDialog builder = new CustomAlertDialog(RazorpayPaymentActivity.this, R.string.dialog_title, getResources().getString(R.string.server_exception2));
//        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//          @Override
//          public void onClick(DialogInterface dialogInterface, int i) {
//            dialogInterface.dismiss();
//          }
//        });
//        builder.show();
//      }
//    });
//  }
//
//
//  public void showSuccessDialog(final JSONObject jsonString, String result) {
//
//
//    try {
//      if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
//        CustomSuccessDialog builder = new CustomSuccessDialog(RazorpayPaymentActivity.this, "Recharge Successful", result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//          public void onClick(DialogInterface dialog, int id) {
//
//            String sucessMessage = null;
//            try {
//              sucessMessage = jsonString.getString("balance");
//              userModel.setUserBalance(sucessMessage);
//              userModel.save();
//            } catch (JSONException e) {
//              e.printStackTrace();
//            }
//            sendRefresh();
//            dialog.dismiss();
//            finish();
//          }
//        });
//        builder.show();
//      } else {
//        CustomSuccessDialog builder = new CustomSuccessDialog(RazorpayPaymentActivity.this, "Payment Successful", result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//          public void onClick(DialogInterface dialog, int id) {
//            sendRefresh();
//            dialog.dismiss();
//            finish();
//          }
//        });
//        builder.show();
//      }
//
//    } catch (NullPointerException e) {
//      e.printStackTrace();
//    }
//
//
//  }
//
//
//  public void showBlockDialog() {
//    Spanned result;
//    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
//    } else {
//      result = Html.fromHtml(AppMetadata.getBlockSession());
//    }
//    CustomBlockDialog builder = new CustomBlockDialog(RazorpayPaymentActivity.this, "Please contact customer care.", result);
//    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//      public void onClick(DialogInterface dialog, int id) {
//        dialog.dismiss();
//        promoteLogout();
//      }
//    });
//    builder.show();
//  }
//
//  private void sendRefresh() {
//    finish();
//    Intent intent = new Intent("setting-change");
//    intent.putExtra("updates", "1");
//    LocalBroadcastManager.getInstance(RazorpayPaymentActivity.this).sendBroadcast(intent);
//    startActivity(new Intent(RazorpayPaymentActivity.this, HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
//  }
//}
//
//
