package in.msewa.ecarib.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.ecarib.R;
import in.msewa.util.ConvertImage;

/**
 * Created by Ksf on 8/12/2016.
 */
public class CouponsDetailsActivity extends AppCompatActivity {

    private WebView wvCouponStatus;
    private TextView tvCouponCode;
    private WebView wvCouponDetails;
    private TextView tvCouponDetailDate;

    private String webViewMessage, couponStatus, couponCode, couponDate;
    private LinearLayout llCouponsDetailMain;
    private Toolbar tbCouponDetails;
    private LoadingDialog loadingDialog;

    private Button btnSaveCouponsDetail, btnEmailCouponsDetail;
    private SaveImageTask saveImageTask;
    private ShareImageTask shareImageTask;

    private ImageButton ivBackBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons_details);
        loadingDialog = new LoadingDialog(CouponsDetailsActivity.this);
        tbCouponDetails = (Toolbar) findViewById(R.id.tbCouponDetails);
        ivBackBtn = (ImageButton)findViewById(R.id.ivBackBtn);
        setSupportActionBar(tbCouponDetails);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvCouponCode = (TextView) findViewById(R.id.tvCouponCode);
        wvCouponStatus = (WebView) findViewById(R.id.wvCouponStatus);
        wvCouponDetails = (WebView) findViewById(R.id.wvCouponDetails);
        tvCouponDetailDate = (TextView) findViewById(R.id.tvCouponDetailDate);
        llCouponsDetailMain = (LinearLayout) findViewById(R.id.llCouponsDetailMain);
        btnEmailCouponsDetail = (Button) findViewById(R.id.btnEmailCouponsDetail);
        btnSaveCouponsDetail = (Button) findViewById(R.id.btnSaveCouponsDetail);

        webViewMessage = getIntent().getStringExtra("webViewMessage");
        couponStatus = getIntent().getStringExtra("couponStatus");
        couponCode = getIntent().getStringExtra("couponCode");
        couponDate = getIntent().getStringExtra("couponDate");

        wvCouponDetails.loadData(webViewMessage, "text/html; charset=utf-8", "utf-8");
        tvCouponCode.setText(couponCode);

        wvCouponStatus.loadData(couponStatus, "text/html; charset=utf-8", "utf-8");

        String[] dateSplit = couponDate.split("\\s+");
        String[] finalDate = dateSplit[0].split("-");
        tvCouponDetailDate.setText("Valid till: " + finalDate[2] + " " + getMonthForInt(Integer.parseInt(finalDate[1]) - 1) + ", " + finalDate[0]);

        btnSaveCouponsDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveImageTask = new SaveImageTask();
                saveImageTask.execute();
            }
        });

        btnEmailCouponsDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareImageTask = new ShareImageTask();
                shareImageTask.execute();
            }
        });
    }

    public int save(Bitmap bb) {
        FileOutputStream fileOutputStream = null;
        try {
            if (bb != null) {
                File file = new File(Environment.getExternalStorageDirectory() + "/VPayQwik_Coupons");
                if (!file.isDirectory()) {
                    file.mkdir();
                }

                Calendar c = Calendar.getInstance();
                SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                String filename = "VPayQwik_Coupons" + f.format(c.getTime());

                file = new File(Environment.getExternalStorageDirectory() + "/VPayQwik_Coupons", filename + ".png");
                fileOutputStream = new FileOutputStream(file);

                try {
                    BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
                    bb.compress(Bitmap.CompressFormat.PNG, 50, bos);
                    bos.flush();
                    bos.close();
//                    CustomToast.showMessage(getApplicationContext(),"Image saved to gallery");

                    MediaScannerConnection.scanFile(getApplicationContext(),
                            new String[]{file.toString()}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> uri=" + uri);
                                }
                            });

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (fileOutputStream != null) {
                            fileOutputStream.close();
                            return 1;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        } catch (Exception e) {
            Log.e("save()", e.getMessage());
        }
        return 0;
    }

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }


    private class SaveImageTask extends AsyncTask<String, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog.show();
        }

        @Override
        protected Integer doInBackground(String... urls) {
            ConvertImage convert = new ConvertImage();
            Bitmap bmp = convert.getBitmapFromView(llCouponsDetailMain);
            if (bmp != null) {
                return save(bmp);
            } else {
                return 0;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result == 1) {
                CustomToast.showMessage(getApplicationContext(), "Saved to Image gallery");
            } else {
                CustomToast.showMessage(getApplicationContext(), "Error saving image");
            }
            loadingDialog.dismiss();
        }
    }


    private class ShareImageTask extends AsyncTask<String, Uri, Uri> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog.show();
        }

        @Override
        protected Uri doInBackground(String... urls) {
            Uri imageUri = null;
            ConvertImage convert = new ConvertImage();
            Bitmap bmp = convert.getBitmapFromView(llCouponsDetailMain);
            if (bmp != null) {
                OutputStream output;
                File filepath = Environment.getExternalStorageDirectory();

                File dir = new File(filepath.getAbsolutePath() + "/VPayQwik_Coupons/");
                dir.mkdirs();

                File file = new File(dir, "VPayQwik_Coupons.png");

                try {
                    output = new FileOutputStream(file);
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
                    output.flush();
                    output.close();
                    imageUri = Uri.fromFile(file);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return imageUri;
        }

            @Override
            protected void onPostExecute(Uri result){
                loadingDialog.dismiss();
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("*/*");
                share.putExtra(Intent.EXTRA_STREAM, result);
                share.putExtra(Intent.EXTRA_TEXT, "Sent via: " + "https://play.google.com/store/apps/details?id=" + getPackageName());
                startActivity(Intent.createChooser(share, "Select an option"));

            }


    }

}


