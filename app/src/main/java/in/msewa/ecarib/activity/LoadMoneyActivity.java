package in.msewa.ecarib.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.util.EncodingUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.LoadMoneyParams;
import in.msewa.model.UserModel;
import in.msewa.util.ServiceUtility;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 5/14/2016.
 */
public class LoadMoneyActivity extends AppCompatActivity {
  private static final String TAG = "test";
  private String amountToLoad;
  private boolean isVBank;
  private String orderId;
  private UserModel session = UserModel.getInstance();
  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private WebView webview;
  private LinearLayout llLoadMoneyWebViewLoading;
  boolean success = false;

  //WebWiew Error
  private LinearLayout llWebError;
  private TextView tvWebError;
  private String jsonBokking;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_ebs_web);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    webview = (WebView) findViewById(R.id.webview);
    llLoadMoneyWebViewLoading = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);
    llWebError = (LinearLayout) findViewById(R.id.llWebError);
    tvWebError = (TextView) findViewById(R.id.tvWebError);

    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

    amountToLoad = getIntent().getStringExtra("amountToLoad");
    isVBank = getIntent().getBooleanExtra("isVBank", false);

    orderId = String.valueOf(System.currentTimeMillis());
    renderView();
  }

  /**
   * To get Json By Making HTTP Call
   */

  public void renderView() {
    @SuppressWarnings("unused")
    class MyJavaScriptInterface {
      @JavascriptInterface
      public void processHTML(String html) {
        try {
          String status = null;
          String details = null;
          JSONObject jsonDetails = null;
          String transactionId = "";

          String result = Html.fromHtml(html).toString();
          Log.i("Transaction Response", result);

          JSONObject jsonResponse = new JSONObject(result);
          success = jsonResponse.getBoolean("success");

          if (success) {
            status = "Congratulations!\nTransaction Successful!\n\nAmount Loaded: " + amountToLoad + "\nV-PayQwik Id: " + session.getUserMobileNo() + "\nEmail: " + session.getUserEmail();
          } else {
            status = "Transaction Failed!\nPlease try again";
          }

          if (status != null) {
            showCustomDialog(status);

          }

        } catch (JSONException e) {
          e.printStackTrace();
        }

      }
    }

    webview.getSettings().setJavaScriptEnabled(true);

    webview.setWebChromeClient(new WebChromeClient() {

      public void onProgressChanged(WebView view, int progress) {
        if (progress == 100) {
          webview.setVisibility(View.VISIBLE);
          llLoadMoneyWebViewLoading.setVisibility(View.GONE);
        } else {
          webview.setVisibility(View.GONE);
          llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
        }
      }
    });

    webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
    webview.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(webview, url);
        final WebView v = view;
        if (url.indexOf("/LoadMoney/Redirect") != -1 || url.indexOf("/LoadMoney/VRedirect") != -1) {
          llLoadMoneyWebViewLoading.setVisibility(View.GONE);
          webview.setVisibility(View.INVISIBLE);
          webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
        } else {
          llLoadMoneyWebViewLoading.setVisibility(View.GONE);
          webview.setVisibility(View.VISIBLE);
        }
      }

      @Override
      public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
        try {
          view.loadData("", "text/html", "UTF-8");
          llWebError.setVisibility(View.VISIBLE);
          tvWebError.setText(description);

        } catch (Exception e) {
          e.printStackTrace();
        }

      }

      @TargetApi(android.os.Build.VERSION_CODES.M)
      @Override
      public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
        try {
          view.loadData("", "text/html", "UTF-8");
          llWebError.setVisibility(View.VISIBLE);
          tvWebError.setText(rerr.getDescription().toString());

        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      @Override
      public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//        SslCertificate sslCertificateServer = error.getCertificate();
//        Certificate pinnedCert = getCertificateForRawResource(R.raw.www_vpayqwik_com, LoadMoneyActivity.this);
//        Certificate serverCert = convertSSLCertificateToCertificate(sslCertificateServer);
//
//        if (pinnedCert.equals(serverCert)) {
        handler.proceed();
//        } else {
        super.onReceivedSslError(view, handler, error);
//        }
      }


      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (url.equals("https://mpi.onlinesbi.com/electraSECURE/vbv/MPIEntry.jsp")) {
          llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
        }
        Log.i("Page Started", url);
      }
    });


    StringBuffer params = new StringBuffer();

    params.append(ServiceUtility.addToPostParams("sessionId", session.getUserSessionId()));
    params.append(ServiceUtility.addToPostParams("channel", LoadMoneyParams.CHANNEL));
    params.append(ServiceUtility.addToPostParams("accountId", LoadMoneyParams.ACC_ID));
    params.append(ServiceUtility.addToPostParams("referenceNo", LoadMoneyParams.REF_NO));
    params.append(ServiceUtility.addToPostParams("amount", amountToLoad));
    params.append(ServiceUtility.addToPostParams("mode", LoadMoneyParams.MODE));
    params.append(ServiceUtility.addToPostParams("currency", LoadMoneyParams.CURRENCY));
    params.append(ServiceUtility.addToPostParams("description", LoadMoneyParams.DESCRIPTION));
    if (isVBank) {
      Log.i("return URL", LoadMoneyParams.RETURN_URL_VBNET);
      params.append(ServiceUtility.addToPostParams("returnUrl", LoadMoneyParams.RETURN_URL_VBNET));
    } else {
      params.append(ServiceUtility.addToPostParams("returnUrl", LoadMoneyParams.RETURN_URL));
    }

    params.append(ServiceUtility.addToPostParams("name", session.getUserFirstName() + " " + session.getUserLastName()));
    params.append(ServiceUtility.addToPostParams("address", LoadMoneyParams.ADDRESS));
    params.append(ServiceUtility.addToPostParams("city", LoadMoneyParams.CITY));
    params.append(ServiceUtility.addToPostParams("state", LoadMoneyParams.STATE));
    params.append(ServiceUtility.addToPostParams("country", LoadMoneyParams.COUNTRY));
    params.append(ServiceUtility.addToPostParams("postalCode", LoadMoneyParams.POSTAL));
    params.append(ServiceUtility.addToPostParams("phone", session.getUserMobileNo()));
    params.append(ServiceUtility.addToPostParams("email", session.getUserEmail()));

    params.append(ServiceUtility.addToPostParams("shipName", LoadMoneyParams.SHIP_NAME));
    params.append(ServiceUtility.addToPostParams("shipAddress", LoadMoneyParams.SHIP_ADDRESS));
    params.append(ServiceUtility.addToPostParams("shipCity", LoadMoneyParams.SHIP_CITY));
    params.append(ServiceUtility.addToPostParams("shipState", LoadMoneyParams.SHIP_STATE));
    params.append(ServiceUtility.addToPostParams("shipCountry", LoadMoneyParams.SHIP_COUNTRY));
    params.append(ServiceUtility.addToPostParams("shipPostalCode", LoadMoneyParams.SHIP_POSTAL_CODE));
    params.append(ServiceUtility.addToPostParams("shipPhone", session.getUserMobileNo()));
    params.append(ServiceUtility.addToPostParams("useVnet", String.valueOf(isVBank)));
    params.append(ServiceUtility.addToPostParams("useVnet", String.valueOf(isVBank)));

    String vPostParams = params.substring(0, params.length() - 1);

    webview.postUrl(ApiUrl.URL_LOAD_MONEY, EncodingUtils.getBytes(vPostParams, "UTF-8"));

  }

  public void showCustomDialog(String msg) {
    CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyActivity.this, R.string.dialog_message_eb, msg);
    String kyc;
    if (session.getUserAcName() != null && !session.getUserAcName().trim().equalsIgnoreCase("KYC")) {
      kyc = "Update KYC";
    } else {
      kyc = "GoBack";
    }
    builder.setNegativeButton(kyc, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        if (success) {
          if (session.getUserAcName() != null && !session.getUserAcName().trim().equalsIgnoreCase("KYC")) {
            startActivity(new Intent(LoadMoneyActivity.this, HowToUpgradeActivity.class).putExtra("kyc", true));

            finishAffinity();
          } else {
            sendFinish();
          }

        } else {
          Log.i("VBANLFailure", "Load money failure");
          finish();
        }
      }
    });
    builder.show();
  }

  public static Certificate getCertificateForRawResource(int resourceId, Context context) {
    CertificateFactory cf = null;
    Certificate ca = null;
    Resources resources = context.getResources();
    InputStream caInput = resources.openRawResource(resourceId);

    try {
      cf = CertificateFactory.getInstance("X.509");
      ca = cf.generateCertificate(caInput);
    } catch (CertificateException e) {
      Log.e(TAG, "exception", e);
    } finally {
      try {
        caInput.close();
      } catch (IOException e) {
        Log.e(TAG, "exception", e);
      }
    }

    return ca;
  }

  public static Certificate convertSSLCertificateToCertificate(SslCertificate sslCertificate) {
    CertificateFactory cf = null;
    Certificate certificate = null;
    Bundle bundle = sslCertificate.saveState(sslCertificate);
    byte[] bytes = bundle.getByteArray("x509-certificate");

    if (bytes != null) {
      try {
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        Certificate cert = certFactory.generateCertificate(new ByteArrayInputStream(bytes));
        certificate = cert;
      } catch (CertificateException e) {
        Log.e(TAG, "exception", e);
      }
    }

    return certificate;
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  @Override
  public void onBackPressed() {
    sendRefresh();
    super.onBackPressed();
  }

  private void sendFinish() {
    finish();
    Log.i("Success", "Send Finish");
    Intent intent = new Intent("loadMoney-done");
    intent.putExtra("result", "1");
    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
  }
}
