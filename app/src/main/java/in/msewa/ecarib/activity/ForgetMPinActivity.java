package in.msewa.ecarib.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 8/11/2016.
 */
public class ForgetMPinActivity extends AppCompatActivity {

  private TextView tvForgetMPinMessage;
  private TextInputLayout ilForgetMPinDob, ilForgetMPinCurrentPwd;
  private EditText etForgetMPinDob, etForgetMPinPwd;
  private ImageButton iBtnForgetMPinShowPwd;
  private Button btnResetMPin;

  private LoadingDialog loadingDialog;
  private JSONObject jsonRequest;

  private UserModel session = UserModel.getInstance();

  //Volley Tag
  private String tag_json_obj = "json_user";

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadingDialog = new LoadingDialog(ForgetMPinActivity.this);
    setContentView(R.layout.activity_forget_mpin);
    tvForgetMPinMessage = (TextView) findViewById(R.id.tvForgetMPinMessage);
    ilForgetMPinDob = (TextInputLayout) findViewById(R.id.ilForgetMPinDob);
    ilForgetMPinCurrentPwd = (TextInputLayout) findViewById(R.id.ilForgetMPinCurrentPwd);
    etForgetMPinDob = (EditText) findViewById(R.id.etForgetMPinDob);
    etForgetMPinPwd = (EditText) findViewById(R.id.etForgetMPinPwd);
    iBtnForgetMPinShowPwd = (ImageButton) findViewById(R.id.iBtnForgetMPinShowPwd);
    btnResetMPin = (Button) findViewById(R.id.btnResetMPin);
    tvForgetMPinMessage.setText("Enter your current password and date of birth to reset your MPin");

    etForgetMPinDob.setFocusable(false);
    etForgetMPinDob.setLongClickable(false);


    btnResetMPin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        submitForm();
      }
    });

    iBtnForgetMPinShowPwd.setOnTouchListener(new View.OnTouchListener() {

      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          etForgetMPinPwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
          etForgetMPinPwd.setSelection(etForgetMPinPwd.length());
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
          etForgetMPinPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          etForgetMPinPwd.setSelection(etForgetMPinPwd.length());
        }

        return false;
      }
    });

    etForgetMPinDob.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        new DatePickerDialog(ForgetMPinActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String formattedDay, formattedMonth;
            if (dayOfMonth < 10) {
              formattedDay = "0" + dayOfMonth;
            } else {
              formattedDay = dayOfMonth + "";
            }

            if ((monthOfYear + 1) < 10) {
              formattedMonth = "0" + String.valueOf(monthOfYear + 1);
            } else {
              formattedMonth = String.valueOf(monthOfYear + 1) + "";
            }
            etForgetMPinDob.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));
          }
        }, 1990, 01, 01).show();
      }
    });


  }

  private void submitForm() {
    if (!validatePassword()) {
      return;
    }
    if (!validateDob()) {
      return;
    }
    promoteReset();
  }

  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }


  private boolean validatePassword() {
    if (etForgetMPinPwd.getText().toString().isEmpty()) {
      ilForgetMPinCurrentPwd.setError("Check the password");
      requestFocus(etForgetMPinPwd);
      return false;
    } else if (etForgetMPinPwd.getText().toString().length() < 6) {
      ilForgetMPinCurrentPwd.setError(getResources().getString(R.string.password_validity));
      requestFocus(etForgetMPinPwd);
      return false;
    } else {
      ilForgetMPinCurrentPwd.setErrorEnabled(false);
    }
    return true;
  }

  private boolean validateDob() {
    if (etForgetMPinDob.getText().toString().trim().isEmpty()) {
      ilForgetMPinDob.setError("Enter Date of birth");
      requestFocus(etForgetMPinDob);
      return false;
    } else {
      ilForgetMPinDob.setErrorEnabled(false);
    }

    return true;
  }

  private void promoteReset() {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("password", etForgetMPinPwd.getText().toString());
      jsonRequest.put("dateOfBirth", etForgetMPinDob.getText().toString());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FORGET_MPIN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          loadingDialog.dismiss();
          Log.i("MPin Reset Response", response.toString());
          try {
            String message = response.getString("message");
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              loadingDialog.dismiss();
//                            Intent intent = new Intent("Logout");
//                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
              //IF SUCCESS
              showSuccessDialog();
            } else if (code != null && code.equals("F04")) {
              loadingDialog.dismiss();
              if (response.has("details")) {
                String details = response.getString("details");
                if (details == null || details.equals("null")) {
                  details = response.getString("response");
                  CustomToast.showMessage(getApplicationContext(), details);
                } else {
                  CustomToast.showMessage(getApplicationContext(), details);
                }

              } else {
                CustomToast.showMessage(getApplicationContext(), message);
              }

            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog();
            } else {
              loadingDialog.dismiss();
              CustomToast.showMessage(getApplicationContext(), message);
            }
          } catch (JSONException e) {
            loadingDialog.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(ForgetMPinActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        promoteLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void showSuccessDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(ForgetMPinActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getSuccessMPinReset()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        startActivity(new Intent(getApplicationContext(), CreatMPinActivity.class).putExtra("IntentType", "Login"));
        finish();
      }
    });

    builder.show();
  }

  private void promoteLogout() {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGOUT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            String code = jsonObj.getString("code");
            if (code != null && code.equals("S00")) {
              UserModel.deleteAll(UserModel.class);
              loadingDialog.dismiss();

              Intent intent = new Intent("Logout");
              LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

              startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
              finish();
            } else {
              loadingDialog.dismiss();
              UserModel.deleteAll(UserModel.class);
              loadingDialog.dismiss();
              startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
              finish();
            }
          } catch (JSONException e) {
            loadingDialog.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "123");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }
}
