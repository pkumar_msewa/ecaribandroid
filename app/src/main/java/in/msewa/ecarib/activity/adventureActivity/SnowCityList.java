package in.msewa.ecarib.activity.adventureActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import in.msewa.ecarib.R;

public class SnowCityList extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private CardView llSnowCity, llCinema9D;
    private Button btnSnowCity, btnCinema9D;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snow_city_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        llSnowCity = (CardView) findViewById(R.id.llSnowCity);
        btnSnowCity = (Button) findViewById(R.id.btnSnowCity);
        llCinema9D = (CardView) findViewById(R.id.llCinema9D);
        btnCinema9D = (Button) findViewById(R.id.btnCinema9D);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnSnowCity.setOnClickListener(this);
        btnCinema9D.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSnowCity:
                Intent OrderIntent = new Intent(getApplicationContext(), SnowCityOrder.class);
                OrderIntent.putExtra("TYPE", "Snow City");
                OrderIntent.putExtra("PRICE", 600);
                startActivity(OrderIntent);
                break;
            case R.id.btnCinema9D:
                Intent OrderIntent1 = new Intent(getApplicationContext(), SnowCityOrder.class);
                OrderIntent1.putExtra("TYPE", "9-D Cinema");
                OrderIntent1.putExtra("PRICE", 100);
                startActivity(OrderIntent1);
                break;
        }
    }
}
