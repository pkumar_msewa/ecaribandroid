package in.msewa.ecarib.activity.imagicaActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.HomeMainActivity;


public class ImagicaOrderActivity extends AppCompatActivity {

  private EditText etFName, etLName, etSolution, etEmail, etdob, etMobile, etlocationCode, etAddress, etCity, etState, etVersion;


  private Button btnPayment;
  private double price;

  private String first_name, last_name, email_Id, mobile_No, location_Code, FName, LName, salutation, email, dob, mobile, locationCode, address, city, state, version, uid, order_id, quantity, token, session_name, sessid, transaction_id;

  private TextView tvAmount, tvuid, tvorderId, tvtransactionId;


  private LoadingDialog loadDlg;

  private TextInputLayout ilFName, ilEmail, ilAddress, ilCity, ilLName, ilMobile, ilSolution, ildob, illocationCode, ilState, ilVersion;
  private JSONObject jsonRequest;

  private final Pattern hasUppercase = Pattern.compile("[A-Z]");
  private final Pattern hasLowercase = Pattern.compile("[a-z]");
  private final Pattern hasNumber = Pattern.compile("\\d");
  private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

  private UserModel session = UserModel.getInstance();

  //Volley Tag
  private String tag_json_obj = "json_user";
  private String product;
  private String productQuantityMap, totaldays, departureDate, visitDate, busOrder, snowMagicaOrder, carExit;
  private String destination;
  private LinearLayout llCarDetails;
  private EditText etcarFName, etcarLName, etcarEmail, etcardob, etcarMobile, etcarlocationCode, etcarAddress, etcarState, etcarCity;
  private TextView tvTime;
  private TextInputLayout ilcarFName, ilcarLName, ilcarEmail, ilcardob, ilcarMobile, ilcarlocationCode, ilcarAddress, ilcarCity, ilcarState;
  private Spinner spHours, spMints;
  private CheckBox addreack;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_imagica_order);
    loadDlg = new LoadingDialog(ImagicaOrderActivity.this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    llCarDetails = (LinearLayout) findViewById(R.id.llCarDetails);
    ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    productQuantityMap = getIntent().getStringExtra("productQuantityMap");
    totaldays = String.valueOf(getIntent().getIntExtra("totaldays", 0));
    departureDate = getIntent().getStringExtra("departureDate");
    visitDate = getIntent().getStringExtra("visitDate");
    busOrder = getIntent().getStringExtra("busOrder");
    destination = getIntent().getStringExtra("destination");
    snowMagicaOrder = getIntent().getStringExtra("snowMagicaOrder");
    carExit = getIntent().getStringExtra("carExit");
    Log.i("JSONVALUE", busOrder + carExit);
    if (carExit.equalsIgnoreCase("True")) {
      llCarDetails.setVisibility(View.VISIBLE);
    }
//
//        product = getIntent().getStringExtra("ticketProduct");
//        uid = getIntent().getStringExtra("uid");
//        price = getIntent().getDoubleExtra("ammount", 0);
//        order_id = getIntent().getStringExtra("orderId");
//        transaction_id = getIntent().getStringExtra("transactionId");
//        Log.i("Amountsss", String.valueOf(price));
    etFName = (EditText) findViewById(R.id.etFName);
    etLName = (EditText) findViewById(R.id.etLName);
    etSolution = (EditText) findViewById(R.id.etSolution);
    etEmail = (EditText) findViewById(R.id.etEmail);
    etdob = (EditText) findViewById(R.id.etdob);
    etMobile = (EditText) findViewById(R.id.etMobile);
    etlocationCode = (EditText) findViewById(R.id.etlocationCode);
    etAddress = (EditText) findViewById(R.id.etAddress);
    etCity = (EditText) findViewById(R.id.etCity);
    etState = (EditText) findViewById(R.id.etState);
    etVersion = (EditText) findViewById(R.id.etVersion);
    etdob.setFocusable(false);
//CarDetails

    etcarFName = (EditText) findViewById(R.id.etcarFName);
    etcarLName = (EditText) findViewById(R.id.etcarLName);
    etcarEmail = (EditText) findViewById(R.id.etcarEmail);
    etcardob = (EditText) findViewById(R.id.etcardob);
    etcarMobile = (EditText) findViewById(R.id.etcarMobile);
    etcarlocationCode = (EditText) findViewById(R.id.etcarlocationCode);
    etcarAddress = (EditText) findViewById(R.id.etcarAddress);
    etcarState = (EditText) findViewById(R.id.etcarState);
    etcarCity = (EditText) findViewById(R.id.etcarCity);
    tvTime = (TextView) findViewById(R.id.tvTime);

    etcardob.setFocusable(false);

    ilcarFName = (TextInputLayout) findViewById(R.id.ilcarFName);
    ilcarLName = (TextInputLayout) findViewById(R.id.ilcarLName);
    ilcarEmail = (TextInputLayout) findViewById(R.id.ilcarEmail);
    ilcardob = (TextInputLayout) findViewById(R.id.ilcardob);
    ilcarMobile = (TextInputLayout) findViewById(R.id.ilcarMobile);
    ilcarlocationCode = (TextInputLayout) findViewById(R.id.ilcarlocationCode);
    ilcarAddress = (TextInputLayout) findViewById(R.id.ilcarAddress);
    ilcarCity = (TextInputLayout) findViewById(R.id.ilcarCity);
    ilcarState = (TextInputLayout) findViewById(R.id.ilcarState);
    spHours = (Spinner) findViewById(R.id.spHours);
    spMints = (Spinner) findViewById(R.id.spMints);
    etMobile.addTextChangedListener(new MyTextWatcher(etMobile));
    etEmail.addTextChangedListener(new MyTextWatcher(etEmail));


    btnPayment = (Button) findViewById(R.id.btnPayment);

    ilLName = (TextInputLayout) findViewById(R.id.ilImgLName);
    ilFName = (TextInputLayout) findViewById(R.id.ilImgFName);
    ilMobile = (TextInputLayout) findViewById(R.id.ilImgMobile);
    ilEmail = (TextInputLayout) findViewById(R.id.ilImgEmail);
    ilAddress = (TextInputLayout) findViewById(R.id.ilImgAddress);
    ilCity = (TextInputLayout) findViewById(R.id.ilImgCity);
    ilSolution = (TextInputLayout) findViewById(R.id.ilImgSolution);
    ildob = (TextInputLayout) findViewById(R.id.ilImgdob);
    illocationCode = (TextInputLayout) findViewById(R.id.ilImglocationCode);
    ilState = (TextInputLayout) findViewById(R.id.ilImgState);
    ilVersion = (TextInputLayout) findViewById(R.id.ilImgVersion);
    etMobile.addTextChangedListener(new MyTextWatcher(etMobile));
    etEmail.addTextChangedListener(new MyTextWatcher(etEmail));
//        String date = new SimpleDateFormat("dd-MM-yyyy").format(departureDate);
    tvTime.setText(departureDate);
    btnPayment.setTextColor(Color.parseColor("#ffffff"));
//        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));

    String[] hours = {"07", "08", "09", "10", "11", "12", "13", "14", "15"};
    String[] mints = {"00", "15", "30", "45"};
    ArrayAdapter hourAdapter = new ArrayAdapter(ImagicaOrderActivity.this, android.R.layout.simple_spinner_dropdown_item, hours);
    ArrayAdapter mintsAdapter = new ArrayAdapter(ImagicaOrderActivity.this, android.R.layout.simple_spinner_dropdown_item, mints);
    spHours.setAdapter(hourAdapter);
    spMints.setAdapter(mintsAdapter);
    addreack = (CheckBox) findViewById(R.id.addreack);
    addreack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (addreack.isChecked()) {
          etcarFName.setText(etFName.getText().toString());
          etcarLName.setText(etLName.getText().toString());
          etcarMobile.setText(etMobile.getText().toString());
          etcarEmail.setText(etEmail.getText().toString());
          etcarAddress.setText(etAddress.getText().toString());
          etcarCity.setText(etCity.getText().toString());
          etcarState.setText(etState.getText().toString());
          etcardob.setText(etdob.getText().toString());
          etcarlocationCode.setText(etlocationCode.getText().toString());

        } else {
          etcarFName.setText("");
          etcarLName.setText("");
          etcarMobile.setText("");
          etcarEmail.setText("");
          etcarAddress.setText("");
          etcarCity.setText("");
          etcarState.setText("");
          etcardob.setText("");
          etcarlocationCode.setText("");
        }
      }
    });
    btnPayment.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        submitForm();
      }
    });
    etdob.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        DatePickerDialog dialog = new DatePickerDialog(ImagicaOrderActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String formattedDay, formattedMonth;
            if (dayOfMonth < 10) {
              formattedDay = "0" + dayOfMonth;
            } else {
              formattedDay = dayOfMonth + "";
            }

            if ((monthOfYear + 1) < 10) {
              formattedMonth = "0" + String.valueOf(monthOfYear + 1);
            } else {
              formattedMonth = String.valueOf(monthOfYear + 1) + "";
            }
            etdob.setText(String.valueOf(formattedDay) + "-"
              + String.valueOf(formattedMonth) + "-"
              + String.valueOf(year));
          }
        }, 1990, 01, 01);

        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

        {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
        dialog.show();
      }
    });
    //DONE CLICK ON VIRTUAL KEYPAD
    etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          submitForm();
        }
        return false;
      }
    });

    etcardob.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        DatePickerDialog dialog = new DatePickerDialog(ImagicaOrderActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String formattedDay, formattedMonth;
            if (dayOfMonth < 10) {
              formattedDay = "0" + dayOfMonth;
            } else {
              formattedDay = dayOfMonth + "";
            }

            if ((monthOfYear + 1) < 10) {
              formattedMonth = "0" + String.valueOf(monthOfYear + 1);
            } else {
              formattedMonth = String.valueOf(monthOfYear + 1) + "";
            }
            etcardob.setText(String.valueOf(formattedDay) + "-"
              + String.valueOf(formattedMonth) + "-"
              + String.valueOf(year));
          }
        }, 1990, 01, 01);

        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

        {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
        dialog.show();
      }
    });

    fillViews();

  }

  private class MyTextWatcher implements TextWatcher {

    private View view;

    private MyTextWatcher(View view) {
      this.view = view;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {
      int i = view.getId();
      if (i == R.id.etMobile) {
        validateMobile(etMobile, ilMobile);
      } else if (i == R.id.etEmail) {
        validateEmail(etEmail, ilEmail);
      }
    }
  }

  private void fillViews() {
    etFName.setText(session.getUserFirstName());
    etLName.setText(session.getUserLastName());
    etEmail.setText(session.getUserEmail());
    etMobile.setText(session.getUserMobileNo());
    etAddress.setText(session.getUserAddress());
//        SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");
//        try {
//            Date dt = sm.parse(session.getUserDob());
//            etdob.setText(sm.format(dt));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

  }


  private void submitForm() {

    if (!validateFName(etFName, ilFName)) {
      return;
    }
    if (!validateLName(etLName, ilLName)) {
      return;
    }
//        if (!validateSolution()) {
//            return;
//        }
    if (!validateMobile(etMobile, ilMobile)) {
      return;
    }
    if (!validateEmail(etEmail, ilEmail)) {
      return;
    }
    if (!validateAddress(etAddress, ilAddress)) {
      return;
    }
    if (!validateCIty(etCity, ilCity)) {
      return;
    }
    if (!validatedob(etdob, ildob)) {
      return;
    }
    if (!validatestate(etState, ilState)) {
      return;
    }
//        if (!validateversion()) {
//            return;
//        }
    if (!validatlocationcode(etlocationCode, illocationCode)) {
      return;
    }

    if (carExit.equalsIgnoreCase("true")) {
      if (!validateFName(etcarFName, ilcarFName)) {
        return;
      }
      if (!validateLName(etcarLName, ilcarLName)) {
        return;
      }
//        if (!validateSolution()) {
//            return;
//        }
      if (!validateMobile(etcarMobile, ilcarMobile)) {
        return;
      }
      if (!validateEmail(etcarEmail, ilcarEmail)) {
        return;
      }
      if (!validateAddress(etcarAddress, ilcarAddress)) {
        return;
      }
      if (!validateCIty(etcarCity, ilcarCity)) {
        return;
      }
      if (!validatedob(etcardob, ilcardob)) {
        return;
      }
      if (!validatestate(etcarState, ilcarState)) {
        return;
      }
//        if (!validateversion()) {
//            return;
//        }
      if (!validatlocationcode(etcarlocationCode, ilcarlocationCode)) {
        return;
      }
    }
    loadDlg.show();

    FName = etFName.getText().toString();
    LName = etLName.getText().toString();
    mobile = etMobile.getText().toString();
    email = etEmail.getText().toString();
    address = etAddress.getText().toString();
    city = etCity.getText().toString();

    try {
      getApiCallPlaceLoader(new JSONObject(productQuantityMap));
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  private boolean validatedob(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty()) {
      iiName.setError("Enter Solution");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validatestate(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty()) {
      iiName.setError("Enter Solution");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validatlocationcode(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty()) {
      iiName.setError("Enter Solution");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateSolution() {
    if (etSolution.getText().toString().trim().isEmpty()) {
      ilSolution.setError("Enter Solution");
      requestFocus(etSolution);
      return false;
    } else {
      ilSolution.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateLName(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty()) {
      iiName.setError("Enter Name");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateFName(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty()) {
      iiName.setError("Enter Name");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateMobile(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty() || text.getText().toString().trim().length() < 10) {
      iiName.setError("Enter 10 digit no");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateAddress(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty()) {
      iiName.setError("Enter Address");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateCIty(EditText text, TextInputLayout iiName) {
    if (text.getText().toString().trim().isEmpty()) {
      iiName.setError("Enter your City");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateEmail(EditText text, TextInputLayout iiName) {
    String email = text.getText().toString().trim();
    if (email.isEmpty() || !isValidEmail(email)) {
      iiName.setError("Enter valid email");
      requestFocus(text);
      return false;
    } else {
      iiName.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateversion() {
    if (etdob.getText().toString().trim().isEmpty()) {
      ildob.setError("Enter Solution");
      requestFocus(etdob);
      return false;
    } else {
      ildob.setErrorEnabled(false);
    }

    return true;
  }

  private static boolean isValidEmail(String email) {
    return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }

  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }


  private void promotePay(String uid, String orderId, String transactionId, double ammount) {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("uid", uid);
      jsonRequest.put("orderId", orderId);
      jsonRequest.put("transactionId", transactionId);
      jsonRequest.put("amount", String.valueOf(ammount));
      jsonRequest.put("firstName", FName);
      jsonRequest.put("lastName", LName);
      jsonRequest.put("salutation", "MR");
      jsonRequest.put("emailId", etEmail.getText().toString());
      jsonRequest.put("dob", etdob.getText().toString());
      jsonRequest.put("mobileNo", etMobile.getText().toString());
      jsonRequest.put("locationCode", etlocationCode.getText().toString());
      jsonRequest.put("address", address);
      jsonRequest.put("city", city);
      jsonRequest.put("state", etState.getText().toString());
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("PAYMENTREQ", jsonRequest.toString());
      Log.i("PAYMENTAPI", ApiUrl.URL_PLACEPAYMENT);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PLACEPAYMENT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("Registration Response", response.toString());
            String messageAPI = response.getString("success");
            String code = response.getString("message");
            String details = response.getString("details");
            String errorMsgs = response.getString("errorMsgs");
            if (messageAPI != null && messageAPI.equals("true")) {
              loadDlg.dismiss();

              showSuccessDialog("We have sent you the ticket details at your email address. Please check your email account for purchased ticket(s) details.", "Congratulations \n Payment Successful");

            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {
              CustomToast.showMessage(getApplicationContext(), code);
              loadDlg.dismiss();

            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            Log.i("Type", "jSON");
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          Log.i("Type", "ERro");
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          loadDlg.dismiss();


        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "12345");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void showSuccessDialog(String message, String Title) {
    CustomSuccessDialog builder = new CustomSuccessDialog(ImagicaOrderActivity.this, Title, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
        clearbackactivity();
        Intent i = new Intent(ImagicaOrderActivity.this, HomeMainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finishAffinity();
        startActivity(i);
      }
    });
    builder.show();
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(ImagicaOrderActivity.this).sendBroadcast(intent);
  }

  private void clearbackactivity() {
    Intent intent = new Intent("imagica-booking-done");
    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(ImagicaOrderActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
  }

  private void getApiCallPlaceLoader(JSONObject jsonObject) {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("visitDate", visitDate);
      jsonRequest.put("departureDate", departureDate);
      jsonRequest.put("totaldays", 1);
      jsonRequest.put("destination", destination);
      jsonRequest.put("productQuantityMap", jsonObject);
      if (busOrder != null && !busOrder.equalsIgnoreCase("null")) {
        jsonRequest.put("busOrder", new JSONObject(busOrder));
      }
      if (snowMagicaOrder != null && !snowMagicaOrder.equalsIgnoreCase("null")) {
        jsonRequest.put("snowMagicaOrder", new JSONObject(snowMagicaOrder));
      }
      if (carExit.equalsIgnoreCase("true")) {
        JSONObject jsonObject1 = new JSONObject();
        try {
          jsonObject1.put("title", "MR");
          jsonObject1.put("firstName", etcarFName.getText().toString());
          jsonObject1.put("lastName", etcarLName.getText().toString());
          jsonObject1.put("locationCode", etcarlocationCode.getText().toString());
          jsonObject1.put("city", etcarCity.getText().toString());
          jsonObject1.put("state", etcarState.getText().toString());
          jsonObject1.put("address", etcarAddress.getText().toString());
          jsonObject1.put("contactNo", etcarMobile.getText().toString());
          jsonObject1.put("bookingTime", tvTime.getText().toString() + " " + spHours.getSelectedItem() + ":" + spMints.getSelectedItem() + ":" + "00");
          jsonRequest.put("carDetails", jsonObject1);
          JSONObject carPassengerList = new JSONObject();
          carPassengerList.put("title", "MR");
          carPassengerList.put("firstName", etcarFName.getText().toString());
          carPassengerList.put("lastName", etcarLName.getText().toString());
          carPassengerList.put("dob", etcardob.getText().toString());
          carPassengerList.put("email", etcarEmail.getText().toString());
          carPassengerList.put("contactNo", etcarMobile.getText().toString());

        } catch (JSONException e) {
        }


      }

    } catch (
      JSONException e)

    {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null)

    {
      Log.i("THEMEPARKTICKETLIST", ApiUrl.URL_PLACEORDER);
      Log.i("THEMETICKETREQ", jsonRequest.toString());
      JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PLACEORDER, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("ORDER_PLACE", response.toString());

            boolean success = response.getBoolean("success");
            String errorMsgs = response.getString("errorMsgs");
            String Msgs = response.getString("message");
            loadDlg.dismiss();
            if (success) {
              String message = response.getString("message");
              String uid = response.getString("uid");
              String orderId = response.getString("orderId");
              String transactionId = response.getString("transactionId");
              double ammount = response.getDouble("amount");
              promotePay(uid, orderId, transactionId, ammount);

            } else {

              if (!errorMsgs.equals("null")) {
                CustomToast.showMessage(ImagicaOrderActivity.this, errorMsgs.replace("[", "").replace("]", ""));
              } else {
                CustomToast.showMessage(ImagicaOrderActivity.this, Msgs.replace("[", "").replace("]", ""));
              }
            }

          } catch (JSONException e) {
            e.printStackTrace();
          }

        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(ImagicaOrderActivity.this, NetworkErrorHandler.getMessage(error, ImagicaOrderActivity.this));

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> hashMap = new HashMap<>();
          hashMap.put("hash", "1234");
          return hashMap;
        }
      };
      int socketTimeout = 600000;
//
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      jsonObjectRequest.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);

    }
  }


}
