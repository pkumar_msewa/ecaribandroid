package in.msewa.ecarib.activity.travelKhanaAvtivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.msewa.adapter.StationListAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.TravelKhanaStationListModel;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class StationListActivity extends AppCompatActivity {

    private ListView lvListBus;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadingDialog;
    private StationListAdapter stationListAdapter;
    private TextView tvSrcCityCode, tvDestCityCode, tvSrcCity, tvSrcTimeDate, tvDestCity, tvDestTimeDate, tvChangeTrip, tvTrainNameNum;
    private String sCode, sName, sDateTime,dCode, dName, dDateTime,tnameno;

    //Volley
    private String tag_json_obj = "json_travel";
    private LinearLayout llNoBus;
    private ArrayList<TravelKhanaStationListModel> stationList;
    private LinearLayout listValues;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_list_travelkhana);
        loadingDialog = new LoadingDialog(StationListActivity.this);
        llNoBus = (LinearLayout) findViewById(R.id.llNoBus);
        tvSrcCityCode = (TextView) findViewById(R.id.tvSrcCityCode);
        tvDestCityCode = (TextView) findViewById(R.id.tvDestCityCode);
        tvSrcCity = (TextView) findViewById(R.id.tvSrcCity);
        tvDestCity = (TextView) findViewById(R.id.tvDestCity);
        tvSrcTimeDate = (TextView) findViewById(R.id.tvSrcTimeDate);
        tvDestTimeDate = (TextView) findViewById(R.id.tvDestTimeDate);
        tvChangeTrip = (TextView) findViewById(R.id.tvChangeTrip);
        tvTrainNameNum = (TextView) findViewById(R.id.tvTrainNameNum);
        stationList = getIntent().getParcelableArrayListExtra("STATIONSLIST");
        sCode = getIntent().getStringExtra("SCODE");
        sName = getIntent().getStringExtra("SNAME");
        sDateTime = getIntent().getStringExtra("SDATETIME");
        dCode = getIntent().getStringExtra("DCODE");
        dName = getIntent().getStringExtra("DNAME");
        dDateTime = getIntent().getStringExtra("DDATETIME");
        tnameno = getIntent().getStringExtra("TNAMENO");
        tvSrcCityCode.setText(sCode);
        tvSrcCity.setText(sName);
        tvSrcTimeDate.setText(sDateTime);
        tvDestCity.setText(dName);
        tvDestCityCode.setText(dCode);
        tvDestTimeDate.setText(dDateTime);
        tvTrainNameNum.setText(tnameno);
        Log.i("ARRAYSIZE",String.valueOf(stationList.size()));
        lvListBus = (ListView) findViewById(R.id.lvListBus);
        listValues=(LinearLayout)findViewById(R.id.listValues);
        lvListBus.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        //press back button in toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getStationList();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("booking-done"));
        LocalBroadcastManager.getInstance(this).registerReceiver(cMessageReceiver, new IntentFilter("food-book-done"));
        tvChangeTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePreviousActivity();
            }
        });

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("ticket");
            if (action.equals("1")) {
                finish();
            }
        }
    };

    private BroadcastReceiver cMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           finish();
        }
    };


    public void getStationList() {
        if (!stationList.isEmpty() && stationList.size() != 0) {
            stationListAdapter = new StationListAdapter(StationListActivity.this, stationList);
            lvListBus.setAdapter(stationListAdapter);
            loadingDialog.dismiss();
        } else {
            loadingDialog.dismiss();
            llNoBus.setVisibility(View.VISIBLE);
            lvListBus.setVisibility(View.GONE);
            listValues.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(cMessageReceiver);
        super.onDestroy();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(StationListActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(StationListActivity.this).sendBroadcast(intent);
    }

    private void closePreviousActivity() {
        Intent intent = new Intent("change-trip");
        LocalBroadcastManager.getInstance(StationListActivity.this).sendBroadcast(intent);
        finish();
    }

}
