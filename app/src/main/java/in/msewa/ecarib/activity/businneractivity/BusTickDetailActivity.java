package in.msewa.ecarib.activity.businneractivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

import in.msewa.model.BusBookedTicketModel;
import in.msewa.model.BusSeatModel;
import in.msewa.ecarib.R;


/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class BusTickDetailActivity extends AppCompatActivity {

    private LinearLayout llBusBookPassengers;

    private long desId, sourceId;

    private HashMap<String, BusSeatModel> seatSelectedMap;
    private TextView tvTicketPnrNo, tvTravelsName, tvFromTo, tvJourneyDate, tvArrTime, tvDepTime, tvMobile, tvEmail, tvBoardingAddress;
    private BusBookedTicketModel busBookedTicket;

    private String refNo = "12345";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bus_booked_tickets);

        tvTicketPnrNo = (TextView) findViewById(R.id.tvTicketPnrNo);
        tvTravelsName = (TextView) findViewById(R.id.tvTravelsName);
        tvFromTo = (TextView) findViewById(R.id.tvFromTo);
        tvJourneyDate = (TextView) findViewById(R.id.tvJourneyDate);
        tvArrTime = (TextView) findViewById(R.id.tvArrTime);
        tvDepTime = (TextView) findViewById(R.id.tvDepTime);
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvBoardingAddress = (TextView) findViewById(R.id.tvBoardingAddress);


        //press back button in toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llBusBookPassengers = (LinearLayout) findViewById(R.id.llBusBookPassengers);
        busBookedTicket = getIntent().getParcelableExtra("TicketValues");

        tvTicketPnrNo.setText(": "+busBookedTicket.getTicketPnr());
        tvTravelsName.setText(": "+busBookedTicket.getBusOperator());
        tvFromTo.setText(": "+busBookedTicket.getSource()+" - "+busBookedTicket.getDestination());
        tvJourneyDate.setText(": "+busBookedTicket.getJourneyDate());
        tvArrTime.setText(": "+busBookedTicket.getArrTime());
        tvDepTime.setText(": "+busBookedTicket.getDeptTime());
        tvEmail.setText(": "+busBookedTicket.getUserEmail());
        tvMobile.setText(": "+busBookedTicket.getUserMobile());
        tvBoardingAddress.setText(": "+busBookedTicket.getBoardingAddress());
        generateViewsForSeat();
    }

    private void generateViewsForSeat() {

        for (int i=0; i<busBookedTicket.getBusPassengerList().size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View passengerDetailView = layoutInflater.inflate(R.layout.view_book_passanger_detail, null);
            TextView tvBusBookSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvBusBookSeatNo);
            llBusBookPassengers.addView(passengerDetailView);
            int j = i+1;
            tvBusBookSeatNo.setText(String.valueOf("Passenger " + j));
            TextView tvPassName = (TextView) passengerDetailView.findViewById(R.id.tvPassName);
            tvPassName.setText(": "+busBookedTicket.getBusPassengerList().get(i).getfName()+" "+busBookedTicket.getBusPassengerList().get(i).getlName());
            Log.i("NAME",busBookedTicket.getBusPassengerList().get(i).getfName()+" "+busBookedTicket.getBusPassengerList().get(i).getlName());
            TextView tvPassAge = (TextView) passengerDetailView.findViewById(R.id.tvPassAge);
            tvPassAge.setText(": "+busBookedTicket.getBusPassengerList().get(i).getAge());
            Log.i("AGE","Age: "+busBookedTicket.getBusPassengerList().get(i).getAge());
            TextView tvPassGender = (TextView) passengerDetailView.findViewById(R.id.tvPassGender);
            tvPassGender.setText(": "+busBookedTicket.getBusPassengerList().get(i).getGender());
            Log.i("GENDER","Gender: "+busBookedTicket.getBusPassengerList().get(i).getGender());
            TextView tvPassSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatNo);
            tvPassSeatNo.setText(": "+busBookedTicket.getBusPassengerList().get(i).getSeatNo());
            Log.i("SEATNO","Seat No: "+busBookedTicket.getBusPassengerList().get(i).getSeatNo());
            TextView tvPassSeatType = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatType);
            tvPassSeatType.setText(": "+busBookedTicket.getBusPassengerList().get(i).getSeatType());
            Log.i("SEATTYPE","seat Type: "+busBookedTicket.getBusPassengerList().get(i).getSeatType());
            TextView tvPassSeatFare = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatFare);
            tvPassSeatFare.setText(": "+"\u20B9 " +busBookedTicket.getBusPassengerList().get(i).getFare()+"/-");
            Log.i("FARE","Fare: "+ "\u20B9 " +busBookedTicket.getBusPassengerList().get(i).getFare()+"/-");

        }

    }
}
