package in.msewa.ecarib.activity.shopping.filter;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.BrandListModel;
import in.msewa.model.CategoryListModel;
import in.msewa.model.SubCategoryListModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by DELL on 26-07-2016.
 */
public class FiltterActivity extends AppCompatActivity {
    String catCode, subCatCode, brandCode;
    private MaterialEditText etCategory, etSubCategory, etBrand;
    private Toolbar toolbar;
    private Button btnCancel, btnSubmit;
    private List<CategoryListModel> categoryModelList;
    private List<SubCategoryListModel> subCategoryModelList;
    private List<BrandListModel> brandModelList;
    private GetCategory GetCategoryTask = null;
    private LoadingDialog loadDlg;
    private UserModel session = UserModel.getInstance();
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_events";
    private JSONObject jsonRequest;
    private ArrayList<String> subCatVal;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_filter_activity);
        categoryModelList = Select.from(CategoryListModel.class).list();
        subCategoryModelList = Select.from(SubCategoryListModel.class).list();
        brandModelList = Select.from(BrandListModel.class).list();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        subCatVal = new ArrayList<>();
        loadDlg = new LoadingDialog(FiltterActivity.this);
        etCategory = (MaterialEditText) findViewById(R.id.etCategory);
        etSubCategory = (MaterialEditText) findViewById(R.id.etSubCategory);
        etBrand = (MaterialEditText) findViewById(R.id.etBrand);
        etSubCategory.setFocusable(false);
        etCategory.setFocusable(false);
        etBrand.setFocusable(false);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etCategory.getText().toString().isEmpty()) {
                    if (!etSubCategory.getText().toString().isEmpty()) {
                        if (!etBrand.getText().toString().isEmpty()) {
//                            Toast.makeText(getApplicationContext(), "catergory, sub category and brand", Toast.LENGTH_SHORT).show();
                            sendFilterBroadcast("brand");
                            finish();
                        } else {
//                            Toast.makeText(getApplicationContext(), "catergory and sub category", Toast.LENGTH_SHORT).show();
                            sendFilterBroadcast("subCat");
                            finish();
                        }
                    } else {
                        etSubCategory.setError("please select sub category");
                        etSubCategory.requestFocus();
                    }
                } else {
                    etCategory.setError("please select category");
                    etCategory.requestFocus();
                }

            }
        });

//        getSubCategoryList();
        GetCategoryTask = new GetCategory();
        GetCategoryTask.execute();

        etCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (categoryModelList == null || categoryModelList.size() == 0) {
                    CustomToast.showMessage(FiltterActivity.this, "Sorry error occured please try again");
                } else {
                    showCatDialog();
                }

            }
        });

        etSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etCategory.getText().toString().isEmpty()) {
                    if (subCategoryModelList == null || subCategoryModelList.size() == 0) {
                        CustomToast.showMessage(FiltterActivity.this, "Error fetching Sub Category, please try again later");
                    } else {
                        subCatVal.clear();
                        showsubCatDialog();
                    }
                } else {
                    CustomToast.showMessage(FiltterActivity.this, "Select Category First first");
                }
            }
        });

        etBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etSubCategory.getText().toString().isEmpty()) {
                    if (brandModelList == null || brandModelList.size() == 0) {
                        CustomToast.showMessage(FiltterActivity.this, "Error fetching brand list, please try again later");
                    } else {
                        showBrandDialog();
                    }
                } else {
                    CustomToast.showMessage(FiltterActivity.this, "Select Sub Category First");
                }
            }
        });


    }

    public void getBrandList() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("subCategoryId", subCatCode);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("BRANDLISTURL", ApiUrl.URL_SHOPPING_BRAND_LIST);
            Log.i("BRANDLISTREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPING_BRAND_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("BRANDLISTRES", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            BrandListModel.deleteAll(BrandListModel.class);
                            JSONArray catArray = response.getJSONArray("response");
                            brandModelList.clear();
                            for (int i = 0; i < catArray.length(); i++) {
                                JSONObject c = catArray.getJSONObject(i);
                                long brandCode = c.getLong("id");
                                String brandName = c.getString("brandName");
                                BrandListModel brandListModel = new BrandListModel(String.valueOf(brandCode), brandName);
                                brandListModel.save();
                                brandModelList.add(brandListModel);
                            }

                        } else {
                            CustomToast.showMessage(FiltterActivity.this, "can't fetch brands");
                        }

                        loadDlg.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FiltterActivity.this, NetworkErrorHandler.getMessage(error, FiltterActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void getCategoryList() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("countryName", "india");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("CATLISTURL", ApiUrl.URL_SHOPPING_CATEGORY_LIST);
            Log.i("CATLISTREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPING_CATEGORY_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("CATRES", response.toString());
                    try {
                        categoryModelList.clear();
                        CategoryListModel appendAllmodel = new CategoryListModel("0", "All","");
                        categoryModelList.add(appendAllmodel);
                        CategoryListModel.deleteAll(CategoryListModel.class);
                        JSONArray catArray = response.getJSONArray("response");
                        for (int i = 0; i < catArray.length(); i++) {
                            JSONObject c = catArray.getJSONObject(i);
                            long catCode = c.getLong("id");
                            String catName = c.getString("categoryName");
                            if (!catName.equalsIgnoreCase("Home Appliances")) {
                                CategoryListModel categoryListModel = new CategoryListModel(String.valueOf(catCode), catName,"");
                                categoryListModel.save();
                                categoryModelList.add(categoryListModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FiltterActivity.this, NetworkErrorHandler.getMessage(error, FiltterActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void getSubCategoryList() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("categoryId", catCode);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("SUBCATLISTURL", ApiUrl.URL_SHOPPING_SUB_CATEGORY_LIST);
            Log.i("SUBCATLISTREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPING_SUB_CATEGORY_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("SUBCATRES", response.toString());
                    try {
                        subCategoryModelList.clear();
                        String code = response.getString("code");
                        if (code.equals("S00")) {
                            SubCategoryListModel.deleteAll(SubCategoryListModel.class);
                            JSONArray catArray = response.getJSONArray("response");
                            for (int i = 0; i < catArray.length(); i++) {
                                JSONObject c = catArray.getJSONObject(i);
                                long id = c.getLong("id");
                                String catName = c.getString("subCategoryName");
                                SubCategoryListModel subCategoryListModel = new SubCategoryListModel(String.valueOf(id), catName);
                                subCategoryListModel.save();
                                subCategoryModelList.add(subCategoryListModel);
                            }
                        } else {
                            String msg = response.getString("message");
                            CustomToast.showMessage(FiltterActivity.this, msg);
                        }
                        loadDlg.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FiltterActivity.this, NetworkErrorHandler.getMessage(error, FiltterActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Filter Activity");
    }

    private void showCatDialog() {
        final ArrayList<String> catVal = new ArrayList<>();
        catVal.clear();
        for (CategoryListModel categoryListModel : categoryModelList) {
            catVal.add(categoryListModel.getcategoryName());
        }

        android.support.v7.app.AlertDialog.Builder catDialog =
                new android.support.v7.app.AlertDialog.Builder(FiltterActivity.this, R.style.AppCompatAlertDialogStyle);
        catDialog.setTitle("Select Category");

        catDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        catDialog.setItems(catVal.toArray(new String[catVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        if (categoryModelList.get(i).getcategoryCode().equals("0")) {
                            sendFilterBroadcast("all");
                            finish();
                        } else {
                            etCategory.setText(catVal.get(i));
                            etSubCategory.getText().clear();
                            catCode = categoryModelList.get(i).getcategoryCode();
                            getSubCategoryList();
                        }

                    }
                });
        catDialog.show();

    }

    private void showsubCatDialog() {
        for (SubCategoryListModel subCategoryListModel : subCategoryModelList) {
            subCatVal.add(subCategoryListModel.getsubCategoryName());
        }
        android.support.v7.app.AlertDialog.Builder subCatDialog =
                new android.support.v7.app.AlertDialog.Builder(FiltterActivity.this, R.style.AppCompatAlertDialogStyle);
        subCatDialog.setTitle("Select Sub Category");

        subCatDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        subCatDialog.setItems(subCatVal.toArray(new String[subCatVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etSubCategory.setText(subCatVal.get(i));
                        etBrand.getText().clear();
                        subCatCode = subCategoryModelList.get(i).getsubCategoryCode();
                        getBrandList();
                    }
                });
        subCatDialog.show();
    }

    private void showBrandDialog() {
        final ArrayList<String> brandVal = new ArrayList<>();
        brandVal.clear();
        for (BrandListModel brandListModel : brandModelList) {
            brandVal.add(brandListModel.getbrandName());
        }
        android.support.v7.app.AlertDialog.Builder brandDialog =
                new android.support.v7.app.AlertDialog.Builder(FiltterActivity.this, R.style.AppCompatAlertDialogStyle);
        brandDialog.setTitle("Select your favourite brand");

        brandDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        brandDialog.setItems(brandVal.toArray(new String[brandVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBrand.setText(brandVal.get(i));
                        brandCode = brandModelList.get(i).getbrandCode();
                        getBrandList();
                    }
                });
        brandDialog.show();
    }

    private void sendFilterBroadcast(String type) {
        Intent intent = new Intent("filter-activated");
        if (type.equals("subCat")) {
            intent.putExtra("TYPE", "SUBCAT");
            intent.putExtra("CATID", catCode);
            intent.putExtra("SUBCATID", subCatCode);
            Log.i("TAGSFILTER", catCode+" "+subCatCode);
        } else if (type.equals("all")) {
            intent.putExtra("TYPE", "all");
        } else {
            intent.putExtra("TYPE", "BRAND");
            intent.putExtra("BRANDID", brandCode);
        }
        LocalBroadcastManager.getInstance(FiltterActivity.this).sendBroadcast(intent);
    }

    private class GetCategory extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadDlg.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getCategoryList();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            etCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (categoryModelList == null || categoryModelList.size() == 0) {
                        CustomToast.showMessage(FiltterActivity.this, "Error fetching category list, please try again later");
                    } else {
                        showCatDialog();
                    }
                }
            });

            etSubCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!etCategory.getText().toString().isEmpty()) {
                        if (subCategoryModelList == null || subCategoryModelList.size() == 0) {
                            CustomToast.showMessage(FiltterActivity.this, "Error fetching sub category list, please try again later");
                        } else {
                            subCatVal.clear();
                            showsubCatDialog();
                        }
                    } else {
                        CustomToast.showMessage(FiltterActivity.this, "Select Category first");
                    }
                }
            });

            etBrand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!etSubCategory.getText().toString().isEmpty()) {
                        if (brandModelList == null || brandModelList.size() == 0) {
                            CustomToast.showMessage(FiltterActivity.this, "Error fetching brand list, please try again later");
                        } else {
                            showBrandDialog();
                        }
                    } else {
                        CustomToast.showMessage(FiltterActivity.this, "Select Sub Category First");
                    }
                }
            });
            loadDlg.dismiss();
        }
    }
}
