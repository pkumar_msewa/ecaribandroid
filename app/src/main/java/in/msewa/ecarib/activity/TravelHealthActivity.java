package in.msewa.ecarib.activity;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import in.msewa.ecarib.R;

/**
 * Created by Ksf on 3/27/2016.
 */
public class TravelHealthActivity extends AppCompatActivity {
  private View rootView;
  private WebView webview;
  private String url = "";
  private ProgressBar pbWebView;
  private LinearLayout llWebError;
  private TextView tvWebError;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.treatcradwebview);
    ImageView ivBackBtn=(ImageView)findViewById(R.id.ivBackBtn);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (webview.canGoBack()) {
          webview.goBack();
        } else {
          finish();
        }
      }
    });
    url = "http://treatcard.in/restaurants/";
      webview = (WebView) findViewById(R.id.wvMain);
      pbWebView = (ProgressBar) findViewById(R.id.pbWebView);
      llWebError = (LinearLayout) findViewById(R.id.llWebError);
      tvWebError = (TextView) findViewById(R.id.tvWebError);

      webview.setVisibility(View.GONE);
      webview.getSettings().setJavaScriptEnabled(true);
      webview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
      webview.getSettings().setDomStorageEnabled(true);
      webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

      webview.setWebChromeClient(new WebChromeClient() {

        public void onProgressChanged(WebView view, int progress) {
          if (progress == 100) {
            webview.setVisibility(View.VISIBLE);
            pbWebView.setVisibility(View.GONE);
          } else {
            webview.setVisibility(View.GONE);
            pbWebView.setVisibility(View.VISIBLE);
          }
        }
      });

      webview.setWebViewClient(new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.loadUrl(request.getUrl().toString());
          } else {
            view.loadUrl(url);
          }
          return true;
        }

        public void onPageFinished(WebView view, String url) {
          webview.loadUrl("javascript:(function() { " + "var head = document.getElementById('header-one-line').style.display='none'; " + "})()");
          webview.loadUrl("javascript:(function() { " +
            "document.getElementsByClassName('navbar-mobile-wrap')[0].style.display='none'; })()");
          webview.setVisibility(View.VISIBLE);
          pbWebView.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                 Handle the error
          view.loadData("", "text/html", "UTF-8");
          pbWebView.setVisibility(View.GONE);
          llWebError.setVisibility(View.VISIBLE);
          tvWebError.setText(description);
        }

        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
          // Redirect to deprecated method, so you can use it in all SDK versions
          view.loadData("", "text/html", "UTF-8");
          pbWebView.setVisibility(View.GONE);
          llWebError.setVisibility(View.VISIBLE);
          tvWebError.setText(rerr.getDescription());
          onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
        }


      });
      webview.loadUrl(url);

    }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
  }
}
