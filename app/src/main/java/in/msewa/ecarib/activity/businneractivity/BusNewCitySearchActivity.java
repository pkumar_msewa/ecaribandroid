package in.msewa.ecarib.activity.businneractivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.adapter.BusCityListAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.BusCityModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class BusNewCitySearchActivity extends AppCompatActivity {
    private ListView lvSearchBusCity;
    private MaterialEditText etSearchBusCity;

    private FetchCityTask fetchCityTask;

    private JSONObject jsonRequest;
    private JSONArray allCityList;
    private LoadingDialog loadingDialog;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_savaari_city";
    private String URL;

    private List<BusCityModel> busCityModelList;

    private BusCityListAdapter busCityListAdapter;
    private String searchType, subTripType;
    private String cityID;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private TextView tvSearchResult;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_city_search);
        searchType = getIntent().getStringExtra("searchType");
        subTripType = getIntent().getStringExtra("SubTripType");
        URL = ApiUrl.URL_BUS_GET_CITY;

        //press back button in toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        loadingDialog = new LoadingDialog(BusNewCitySearchActivity.this);

        etSearchBusCity = (MaterialEditText) findViewById(R.id.etSearchBusCity);
        lvSearchBusCity = (ListView) findViewById(R.id.lvSearchBusCity);
        tvSearchResult = (TextView) findViewById(R.id.tvSearchResult);

        busCityModelList = new ArrayList<>();

        getCity();
        tvSearchResult.setText("All Cities");
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("booking-done"));

        lvSearchBusCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent("search-complete");
                intent.putExtra("selectedCityName", busCityModelList.get(i).getCityname());
                intent.putExtra("selectedCityCode", busCityModelList.get(i).getCityId());
                intent.putExtra("searchType", searchType);
                LocalBroadcastManager.getInstance(BusNewCitySearchActivity.this).sendBroadcast(intent);
                loadingDialog.dismiss();
                finish();
            }
        });

        etSearchBusCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (cs.length() > 2) {
                    searchBus(String.valueOf(cs));
                } else {
                    try {
                        busCityModelList.clear();
                        for (int i = 0; i < allCityList.length(); i++) {
                            JSONObject c = allCityList.getJSONObject(i);
                            long cityId = c.getLong("cityId");
                            String cityName = c.getString("cityName");
                            BusCityModel cModel = new BusCityModel(cityId, cityName);
                            busCityModelList.add(cModel);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    tvSearchResult.setText("All Cities");
                    busCityListAdapter = new BusCityListAdapter(getApplicationContext(), busCityModelList);
                    lvSearchBusCity.setAdapter(busCityListAdapter);
                    busCityListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("ticket");
            if (action.equals("1")) {
                finish();
            }

        }
    };

    private void searchBus(String cityValue) {
        fetchCityTask = new FetchCityTask();
        fetchCityTask.execute(cityValue);
    }


    private class FetchCityTask extends AsyncTask<String, Void, List<BusCityModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            busCityModelList.clear();
            loadingDialog.show();
        }

        @Override
        protected List<BusCityModel> doInBackground(String... selectedCityString) {

//            String mainJson = loadJSONFromAsset();
            try {
//                JSONArray response = new JSONArray(mainJson);
                for (int i = 0; i < allCityList.length(); i++) {
                    JSONObject c = allCityList.getJSONObject(i);
                    long cityId = c.getLong("cityId");
                    String cityName = c.getString("cityName");
                    Log.i("Selected", selectedCityString[0]);

                    if ((cityName.toUpperCase()).contains(selectedCityString[0].toUpperCase())) {
                        BusCityModel cModel = new BusCityModel(cityId, cityName);
                        busCityModelList.add(cModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return busCityModelList;
        }

        @Override
        protected void onPostExecute(List<BusCityModel> result) {
            if (result != null && result.size() != 0) {
                tvSearchResult.setText("Obtained Result");
                busCityListAdapter = new BusCityListAdapter(getApplicationContext(), result);
                lvSearchBusCity.setAdapter(busCityListAdapter);
                loadingDialog.dismiss();
                lvSearchBusCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent("search-complete");
                        intent.putExtra("selectedCityName", busCityModelList.get(i).getCityname());
                        intent.putExtra("selectedCityCode", busCityModelList.get(i).getCityId());
                        intent.putExtra("searchType", searchType);
                        LocalBroadcastManager.getInstance(BusNewCitySearchActivity.this).sendBroadcast(intent);
                        loadingDialog.dismiss();
                        finish();
                    }
                });
            } else {
                loadingDialog.dismiss();
                tvSearchResult.setText("Obtained Result");
                busCityModelList.clear();
                busCityListAdapter.notifyDataSetChanged();
            }

        }
    }

    private void getCity() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

//            if(subTripType.equals("roundtrip")){
//                jsonRequest.put("subTripType", "roundtrip");
//            }else{
//                jsonRequest.put("subTripType", "oneway");
//            }
//
//            if(searchType.equals("To")){
//                jsonRequest.put("sourceCity",cityID);
//            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("CityRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, URL, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("CITYRESPONSE", response.toString());
                        String code = response.getString("code");
                        loadingDialog.dismiss();
                        if (code != null && code.equals("S00")) {
                            JSONArray citydata = response.getJSONArray("details");
                            allCityList = citydata;
                            for (int i = 0; i < citydata.length(); i++) {
                                JSONObject c = citydata.getJSONObject(i);
                                long cityId = c.getLong("cityId");
                                String cityName = c.getString("cityName");
                                BusCityModel cModel = new BusCityModel(cityId, cityName);
                                busCityModelList.add(cModel);

                            }
                            if (busCityModelList.size() != 0) {
                                busCityListAdapter = new BusCityListAdapter(getApplicationContext(), busCityModelList);
                                lvSearchBusCity.setAdapter(busCityListAdapter);
                            }
                            loadingDialog.dismiss();

                        } else {
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getApplicationContext(), message);
                                loadingDialog.dismiss();
                                finish();
                            }
                        }

                    } catch (JSONException e) {
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                        loadingDialog.dismiss();
                        finish();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    loadingDialog.dismiss();
                    finish();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


}
