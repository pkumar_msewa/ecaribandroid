package in.msewa.ecarib.activity.shopping.fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.readystatesoftware.viewbadger.BadgeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.CategoryListModel;
import in.msewa.model.PQCart;
import in.msewa.model.ShoppingModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.CatAdapter;

//import in.msewa.adapter.ShoppingAdapter;
//import in.msewa.custom.CustomToast;

/**
 * Created by Ksf on 3/27/2016.
 */
public class ShopCatListFragment extends Fragment {

  private static String selectValue;
  private View rootView;
  private GridView gvShopping;
  private TextView tvcart_point;
  private List<CategoryListModel> categoryArray;
  private List<ShoppingModel> shoppingArray;
  private RequestQueue rq;
  private String cartValue = "0";
  private CatAdapter catAdapter;
  private PQCart pqCart = PQCart.getInstance();
  //    private ShoppingAdapter shoppingAdapter;
  private UserModel session = UserModel.getInstance();
  private Button btnGoCart;
  private RecyclerView rvShopping;
  private LoadingDialog loadDlg;
  private JsonObjectRequest postReq;
  private String tag_json_obj = "json_events";
  private JSONObject jsonRequest;
  private String action;
  private ImageView ivFab;
  private BadgeView badge;
  private ProgressBar pbLoading;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    try {
      final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
      rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
    } catch (KeyManagementException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    loadDlg = new LoadingDialog(getActivity());
//        Select specificProductQueryGt = Select.from(ShoppingModel.class);
//        shoppingArray = specificProductQueryGt.list();
//        cartValue =  String.valueOf(pqCart.getProductsInCartArray().size());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_category_list, container, false);
    gvShopping = (GridView) rootView.findViewById(R.id.gvShopping);
    btnGoCart = (Button) rootView.findViewById(R.id.btnGoCart);
    ivFab = (ImageView) rootView.findViewById(R.id.ivFab);
    badge = new BadgeView(getActivity(), ivFab);
    pbLoading = (ProgressBar) rootView.findViewById(R.id.pbLoading);
//        tvcart_point = (TextView) rootView.findViewById(R.id.tvshopping_cart_value);
//        tvcart_point.setText(cartValue);

    rvShopping = (RecyclerView) rootView.findViewById(R.id.rvShoppingCat);
    rvShopping.addItemDecoration(new SpacesItemDecoration(8));
    final GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
    rvShopping.setLayoutManager(manager);
    rvShopping.setHasFixedSize(true);
    manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
      @Override
      public int getSpanSize(int position) {
        return catAdapter.isPositionHeader(position) ? manager.getSpanCount() : 1;
      }
    });

//        rvShopping.setAdapter(new ShopAdapter());
//        btnGoCart = (Button) findViewById(R.id.btnGoCart);
    categoryArray = new ArrayList<>();
//        tvcart_point = (TextView) findViewById(R.id.tvshopping_cart_value);


//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("cart-clear"));
//        LocalBroadcastManager.getInstance(this).registerReceiver(fMessageReceiver, new IntentFilter("filter-activated"));
//        if (shoppingArray != null && shoppingArray.size() != 0) {
//            Collections.sort(shoppingArray, new compPopulation());
//            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
//            rvShopping.setAdapter(shoppingAdapter);
//        } else {
//            getShoppingItems("all", "", "", "");
//
//        }
    getCatList();

    return rootView;
  }


  @Override
  public void onResume() {
    super.onResume();
    PayQwikApplication.getInstance().trackScreenView("Shopping");
    cartValue = String.valueOf(pqCart.getProductsInCart().size());
    Log.i("CARTONRESUME", cartValue);
//        tvcart_point.setText(cartValue);
    if (Integer.parseInt(cartValue) > 0) {
      badge.setText(cartValue);
      badge.show();
    }
  }

  public FragmentManager getHostFragmentManager() {
    FragmentManager fm = getFragmentManager();
    if (fm == null && isAdded() && getActivity() != null) {
      fm = (getActivity()).getSupportFragmentManager();
    }
    return fm;
  }

  public void getCatList() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobile", session.getUserMobileNo());
      jsonRequest.put("countryName", "india");


    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("CATLIST", ApiUrl.URL_SHOPPING_CATEGORY_LIST);
      Log.i("CATREQ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPING_CATEGORY_LIST, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          loadDlg.dismiss();
          try {
            Log.i("CATRES", jsonObj.toString());
            String code = jsonObj.getString("code");

            if (code != null && code.equals("S00")) {
              try {
                if (!jsonObj.isNull("response")) {
                  categoryArray.clear();
                  CategoryListModel.deleteAll(CategoryListModel.class);

                  JSONArray productJArray = jsonObj.getJSONArray("response");
                  if (productJArray.length() != 0) {

                    for (int i = 0; i < productJArray.length(); i++) {
                      JSONObject c = productJArray.getJSONObject(i);
                      JSONObject catId = c.getJSONObject("country");
                      long cId = c.getLong("id");
                      String cImage = c.getString("categoryImage");
                      String cName = c.getString("categoryName");
                      CategoryListModel categoryListModel = new CategoryListModel(String.valueOf(cId), cName, cImage);
                      categoryArray.add((categoryListModel));

                    }
                    if (categoryArray.size() != 0) {
                      pbLoading.setVisibility(View.GONE);
                      try {
                        catAdapter = new CatAdapter(getActivity(), categoryArray, getHostFragmentManager());
                        rvShopping.setAdapter(catAdapter);
                      } catch (NullPointerException e) {
                        e.printStackTrace();
                      }
                    }
                  } else {
//                                        if (shoppingArray.size() != 0) {
//                                            Collections.sort(shoppingArray, new compPopulation());
//                                            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
//                                            rvShopping.setAdapter(shoppingAdapter);
//                                        }
                    CustomToast.showMessage(getActivity(), "No category Found");
                    getActivity().finish();
                  }
                }
              } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
              }
//                            getHighToLow(shoppingArray);
              loadDlg.dismiss();

            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
          getActivity().finish();
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "6");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }

  public class compPopulation implements Comparator<ShoppingModel> {

    public int compare(ShoppingModel a, ShoppingModel b) {
      Log.i("MAx", "value");
      if (a.getpPrice() > b.getpPrice())
        return -1; // highest value first
      if (a.getpPrice() == b.getpPrice())
        return 0;
      return 1;
    }
  }

}
