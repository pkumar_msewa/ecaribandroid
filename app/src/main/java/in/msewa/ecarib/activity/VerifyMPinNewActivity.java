package in.msewa.ecarib.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BusSaveModel;
import in.msewa.model.FlightSaveModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import me.philio.pinentry.PinEntryView;

/**
 * Created by Ksf on 7/26/2016.
 */
public class VerifyMPinNewActivity extends AppCompatActivity {
  private PinEntryView etVerifyPin;

  private Button btnForgetPin;
  private JSONObject jsonRequest;
  private UserModel session = UserModel.getInstance();

  private LoadingDialog loadDlg;
  private TextWatcher textWatcher;
  private boolean isRunning = false;

  private String detailMessage;

  //Volley Tag
  private String tag_json_obj = "json_user";

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("Logout"));


    setContentView(R.layout.activity_verify_m_pin_new);
    loadDlg = new LoadingDialog(VerifyMPinNewActivity.this);

    etVerifyPin = (PinEntryView) findViewById(R.id.etVerifyPin);
    btnForgetPin = (Button) findViewById(R.id.btnForgetPin);
    etVerifyPin.requestFocus();


    btnForgetPin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(VerifyMPinNewActivity.this, ForgetMPinActivity.class));
      }
    });


    textWatcher = new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
        if (editable.length() == 4) {
          verifyMPin();
        }

      }
    };

    etVerifyPin.addTextChangedListener(textWatcher);
  }

  private void verifyMPin() {
    isRunning = true;
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("newMpin", etVerifyPin.getText().toString());
      jsonRequest.put("username", session.getUserMobileNo());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VERIFY_M_PIN, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
      AndroidNetworking.post(ApiUrl.URL_VERIFY_M_PIN)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("mpin")
        .setPriority(Priority.HIGH)
        .addHeaders("hash", "1234")
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject response) {
            try {
              Log.i("Verify PIN RESPONSE", response.toString());
              String code = response.getString("code");
              String jsonString = response.getString("response");

              JSONObject jsonObject = new JSONObject(jsonString);
              loadDlg.dismiss();
              if (code != null && code.equals("S00")) {
                //Verified
                Intent mainIntent = new Intent(VerifyMPinNewActivity.this, HomeMainActivity.class);
                loadDlg.dismiss();
                boolean hasRefer;
                if (response.has("hasRefer")) {
                  hasRefer = response.getBoolean("hasRefer");

                } else {
                  hasRefer = true;
                }
                mainIntent.putExtra("status", String.valueOf(hasRefer));
                startActivity(mainIntent);
                finish();
              } else if (code != null && code.equals("F03")) {
                loadDlg.dismiss();
                showInvalidSessionDialog(detailMessage);
                etVerifyPin.getText().clear();
              } else if (code != null && code.equals("F00")) {
                etVerifyPin.getText().clear();
                detailMessage = response.getString("message");
                loadDlg.dismiss();
                CustomToast.showMessage(getApplicationContext(), detailMessage);
              } else if (code != null && code.equals("F05")) {
                loadDlg.dismiss();
                etVerifyPin.getText().clear();
                detailMessage = response.getString("message");
                CustomToast.showMessage(VerifyMPinNewActivity.this, detailMessage);
                promoteLogout();
              } else if (code != null && code.equalsIgnoreCase("F04")) {
                loadDlg.dismiss();
                etVerifyPin.getText().clear();
                detailMessage = response.getString("message");
                CustomToast.showMessage(VerifyMPinNewActivity.this, detailMessage);

              } else {
                etVerifyPin.getText().clear();
                loadDlg.dismiss();
                detailMessage = response.getString("message");
                CustomToast.showMessage(getApplicationContext(), detailMessage);
              }


            } catch (JSONException e) {
              loadDlg.dismiss();
              e.printStackTrace();
              CustomToast.showMessage(VerifyMPinNewActivity.this, getResources().getString(R.string.server_exception2));

            }
          }

          @Override
          public void onError(ANError error) {
            loadDlg.dismiss();
            error.printStackTrace();
            CustomToast.showMessage(VerifyMPinNewActivity.this, getResources().getString(R.string.server_error_auth_error));
          }
        });
    }
  }

  public void showInvalidSessionDialog(String message) {
    CustomAlertDialog builder = new CustomAlertDialog(VerifyMPinNewActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        promoteLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void promoteLogout() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_LOGOUT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("mpin")
        .setPriority(Priority.HIGH)
        .addHeaders("hash", "1234")
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject jsonObj) {
            try {
              String code = jsonObj.getString("code");
              if (code != null && code.equals("S00")) {
                SharedPreferences kycValues = getSharedPreferences("kyc", Context.MODE_PRIVATE);
                kycValues.edit().clear().remove("KYC").commit();
                UserModel.deleteAll(UserModel.class);
                BusSaveModel.deleteAll(BusSaveModel.class);
                FlightSaveModel.deleteAll(FlightSaveModel.class);
                loadDlg.dismiss();
                finishAffinity();
                startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
              } else {
                loadDlg.dismiss();
                SharedPreferences kycValues = getSharedPreferences("kyc", Context.MODE_PRIVATE);
                kycValues.edit().clear().remove("KYC").commit();
                UserModel.deleteAll(UserModel.class);
                BusSaveModel.deleteAll(BusSaveModel.class);
                FlightSaveModel.deleteAll(FlightSaveModel.class);
                loadDlg.dismiss();
                finishAffinity();
                startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));

              }
            } catch (JSONException e) {
              loadDlg.dismiss();
              CustomToast.showMessage(VerifyMPinNewActivity.this, getResources().getString(R.string.server_error_auth_error));
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError error) {
            loadDlg.dismiss();
            error.printStackTrace();
            CustomToast.showMessage(VerifyMPinNewActivity.this, getResources().getString(R.string.server_error_auth_error));

          }
        });
    }
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      finish();
    }
  };

  @Override
  protected void onDestroy() {
    super.onDestroy();
    AndroidNetworking.cancel("mpin");
  }
}
