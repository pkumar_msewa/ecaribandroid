package in.msewa.ecarib.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import in.msewa.permission.PermissionHandler;
import in.msewa.permission.Permissions;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 6/22/2016.
 */
public class OtpVerificationMidLayerActivity extends AppCompatActivity {

  private String userMobileNo;
  private String intentType;


  private Button btnApplyManual;
  private boolean value = false;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_opt_verification_mid_layer);
    btnApplyManual = (Button) findViewById(R.id.btnApplyManual);
    if (Build.VERSION.SDK_INT >= 23) {
      Permissions.check(OtpVerificationMidLayerActivity.this, new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}, 0, null,
        new PermissionHandler() {
          @Override
          public void onGranted() {
            //do your task
            value = true;
            LocalBroadcastManager.getInstance(OtpVerificationMidLayerActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));
          }
        });
//
    } else {
      value = true;
      LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));

    }

    userMobileNo = getIntent().getStringExtra("userMobileNo");
    intentType = getIntent().getStringExtra("intentType");

    btnApplyManual.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (value) {
          LocalBroadcastManager.getInstance(OtpVerificationMidLayerActivity.this).unregisterReceiver(broadcastReceiver);
        }
        if (intentType.equals("Register"))

        {
          Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpVerificationActivity.class);
          autoIntent.putExtra("OtpCode", "");
          autoIntent.putExtra("userMobileNo", userMobileNo);
          startActivity(autoIntent);
          finish();
        } else if (intentType.equals("Device"))

        {
          String password = getIntent().getStringExtra("devPassword");
          String regId = getIntent().getStringExtra("devRegId");
          Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpDeviceVerificationActivity.class);
          autoIntent.putExtra("devPassword", password);
          autoIntent.putExtra("devRegId", regId);
          autoIntent.putExtra("OtpCode", "");
          autoIntent.putExtra("userMobileNo", userMobileNo);
          startActivity(autoIntent);
          finish();
        } else

        {
          Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, ChangePwdActivity.class);
          autoIntent.putExtra("OtpCode", "");
          autoIntent.putExtra("userMobileNo", userMobileNo);
          startActivity(autoIntent);
          finish();
        }
      }
    });
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    if (value) {
      LocalBroadcastManager.getInstance(OtpVerificationMidLayerActivity.this).unregisterReceiver(broadcastReceiver);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (value) {
      LocalBroadcastManager.getInstance(OtpVerificationMidLayerActivity.this).unregisterReceiver(broadcastReceiver);
    }
  }

  //  @Override
//  protected void onDestroy() {
//    super.onDestroy();
//    unregisterReceiver(broadcastReceiver);
//  }

  BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      Bundle b = intent.getExtras();
      if (b != null) {
        String message = b.getString("message");
        Log.i("BroadCast", message);
        message = message.replaceAll("\\.", "");
        Log.i("BroadCast", message);


        if (intentType.equals("Register")) {
          Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpVerificationActivity.class);
          autoIntent.putExtra("OtpCode", message);
          autoIntent.putExtra("userMobileNo", userMobileNo);
          startActivity(autoIntent);
          finish();
        } else if (intentType.equals("Device")) {
          String password = getIntent().getStringExtra("devPassword");
          String regId = getIntent().getStringExtra("devRegId");
          Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpDeviceVerificationActivity.class);
          autoIntent.putExtra("devPassword", password);
          autoIntent.putExtra("devRegId", regId);
          autoIntent.putExtra("OtpCode", message);
          autoIntent.putExtra("userMobileNo", userMobileNo);
          startActivity(autoIntent);
          finish();
        } else if (intentType.equals("Forget")) {
          Log.i("Change", "Password");
          Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, ChangePwdActivity.class);
          autoIntent.putExtra("OtpCode", message);
          autoIntent.putExtra("userMobileNo", userMobileNo);
          startActivity(autoIntent);
          finish();
        }

        LocalBroadcastManager.getInstance(OtpVerificationMidLayerActivity.this).unregisterReceiver(broadcastReceiver);
      }

    }
  };


}
