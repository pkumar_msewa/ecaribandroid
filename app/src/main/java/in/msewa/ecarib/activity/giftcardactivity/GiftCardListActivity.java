package in.msewa.ecarib.activity.giftcardactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.fragment.fragmentgiftcard.DescriptionFragment;
import in.msewa.fragment.fragmentgiftcard.PurchaseFragment;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.GiftCardCatModel;
import in.msewa.ecarib.R;

/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardListActivity extends AppCompatActivity {


  private TabLayout mSlidingTabLayout;
  private ViewPager mainPager;
  private FragmentManager fragmentManager;
  ImageView ivGiftCardItem;
  CharSequence TitlesEnglish[] = {"Terms & conditions", "Purchase"};
  int NumbOfTabs = 3;
  private GiftCardCatModel model;
  private LoadingDialog loading;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    fragmentManager = getSupportFragmentManager();
    setContentView(R.layout.activity_tabs_layout);
    mainPager = (ViewPager) findViewById(R.id.mainPager);
    model = getIntent().getParcelableExtra("Model");
    mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
    mSlidingTabLayout.setupWithViewPager(mainPager);
    ivGiftCardItem = (ImageView) findViewById(R.id.ivGiftCardItem);
    supportPostponeEnterTransition();
    if (!model.getImages().equalsIgnoreCase("") && model.getImages() != null) {
      Picasso.with(GiftCardListActivity.this).load(model.getImages()).error(R.drawable.no_image_available)
        .placeholder(R.drawable.ic_loading_image).into(ivGiftCardItem,new  Callback() {
        @Override
        public void onSuccess () {
          supportStartPostponedEnterTransition();
        }

        @Override
        public void onError () {
          supportStartPostponedEnterTransition();
        }
      });
    }
    android.support.v7.widget.Toolbar issuentoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(issuentoolbar);
    ImageButton issueIb = (ImageButton) findViewById(R.id.ivBackBtn);
    issueIb.setVisibility(View.VISIBLE);
    issueIb.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
    loading = new LoadingDialog(GiftCardListActivity.this);
    if (model.getTnc_mobile().equalsIgnoreCase("null")) {
      TitlesEnglish = new CharSequence[]{"Purchase"};
      mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), TitlesEnglish, TitlesEnglish.length, model));
    } else {
      mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), TitlesEnglish, TitlesEnglish.length, model));
    }
    mSlidingTabLayout.setupWithViewPager(mainPager);
  }


  private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      mainPager.setCurrentItem(1);

    }
  };

  public static class ViewPagerAdapter extends FragmentPagerAdapter {
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

    GiftCardCatModel giftModel;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, GiftCardCatModel giftModel) {
      super(fm);
      this.giftModel = giftModel;
      this.Titles = mTitles;
      this.NumbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {
      if (NumbOfTabs == 1) {
        if (position == 0) {
          Bundle bundle = new Bundle();
          bundle.putParcelable("model", giftModel);
          PurchaseFragment tab1 = new PurchaseFragment();
          tab1.setArguments(bundle);
          return tab1;
        }
      } else {
        if (position == 1) {
          Bundle bundle = new Bundle();
          bundle.putParcelable("model", giftModel);
          PurchaseFragment tab1 = new PurchaseFragment();
          tab1.setArguments(bundle);
          return tab1;
        } else if (position == 0) {
          Bundle bundle = new Bundle();
          bundle.putParcelable("model", giftModel);
          DescriptionFragment tab2 = new DescriptionFragment();
          tab2.setArguments(bundle);
          return tab2;
        } else {
          Bundle bundle = new Bundle();
          bundle.putParcelable("model", giftModel);
          DescriptionFragment tab2 = new DescriptionFragment();
          tab2.setArguments(bundle);
          return tab2;
        }
      }
      return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return Titles[position];
    }

    @Override
    public int getCount() {
      return NumbOfTabs;
    }
  }


  public void getCardCategory() {
//    pbGiftCardCat.setVisibility(View.VISIBLE);
//    rvGiftCardCat.setVisibility(View.GONE);
//    loading.show();
//    AndroidNetworking.get(ApiUrl.URL_WHOOHOO_DESCRIPTION + model.getProduct_id())
//      .setTag("test")
//      .setPriority(Priority.HIGH)
//      .build()
//      .getAsJSONObject(new JSONObjectRequestListener() {
//        @Override
//        public void onResponse(JSONObject jsonObj) {
//          try {
//            Log.i("EventsCatResponse", jsonObj.toString());
//            String code = jsonObj.getString("success");
//            String message = jsonObj.getString("message");
//            loading.dismiss();
//            if (code != null && code.equals("true")) {
////              Picasso.with(GiftCardListActivity.this).load(jsonObj.getString("categoryImage")).centerInside().fit().into(ivBanner);
//              if (!jsonObj.isNull("product_id")) {
//                String product_id = jsonObj.getString("product_id");
//                String brand_id = model.getBrand_id();
//                String brand_name = model.getBrand_name();
//                String images = model.getImages();
//                String baseImage = model.getBaseImage();
//                String min_custom_price = jsonObj.getString("min_custom_price");
//                String max_custom_price = jsonObj.getString("max_custom_price");
//                String sku = jsonObj.getString("sku");
//                String product_type = jsonObj.getString("product_type");
//                String paywithamazon_disable = jsonObj.getString("paywithamazon_disable");
//                String custom_denominations = jsonObj.getString("custom_denominations");
//                String tnc_mobile = jsonObj.getString("tnc_mobile");
//                String order_handling_charge = jsonObj.getString("order_handling_charge");
//                String themes = jsonObj.getString("themes");
//                GiftCardCatModel eveCatModel = new GiftCardCatModel(product_id, brand_id, brand_name, "https://woohooproducts.s3.amazonaws.com/" + product_id + ".jpeg", baseImage, min_custom_price, max_custom_price, custom_denominations, sku, product_type, paywithamazon_disable, tnc_mobile, order_handling_charge, themes);
//                if (!jsonObj.isNull("tnc_mobile") && !jsonObj.getString("tnc_mobile").equalsIgnoreCase("null")) {


//                } else {
//                  TitlesEnglish = new CharSequence[]{"Purchase"};
//                  mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), TitlesEnglish, TitlesEnglish.length, eveCatModel));
//                  mSlidingTabLayout.setupWithViewPager(mainPager);
//                }
//              } else {
//                CustomToast.showMessage(GiftCardListActivity.this, message);
//                finish();
//              }
//
//            } else if (code.equalsIgnoreCase("F03")) {
////              showInvalidSessionDialog();
//              loading.dismiss();
//            } else {
//              if (message.equalsIgnoreCase("Please login and try again.")) {
////                showInvalidSessionDialog();
////                pbGiftCardCat.setVisibility(View.GONE);
//                loading.dismiss();
//              } else {
//                CustomToast.showMessage(GiftCardListActivity.this, message);
////                pbGiftCardCat.setVisibility(View.GONE);
//                loading.dismiss();
//              }
//            }
//
//          } catch (JSONException e) {
//            e.printStackTrace();
////            pbGiftCardCat.setVisibility(View.GONE);
//            loading.dismiss();
//            Toast.makeText(GiftCardListActivity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//          }
//
//        }
//
//        @Override
//        public void onError(ANError anError) {
//          loading.dismiss();
//          CustomToast.showMessage(GiftCardListActivity.this, getResources().getString(R.string.server_exception));
//        }
//      });
  }
}



