package in.msewa.ecarib.activity;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

public class RealService extends Service {

    private static final String TAG = "RealService";
    private boolean isRunning = false;
    private IBinder mBinder = new MyBinder();
    UserModel userModel = UserModel.getInstance();
    private boolean intenetAccess = false;
    /* Change here */
    private RequestQueue reQueue;
    private final String url = "http://www.google.com";
    private String tag_json_obj = "RealService";

    public boolean SendRequest(JSONObject stringStringHashMap) {
/* Change here */
//               reQueue = Volley.newRequestQueue(this);
//        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST,
//                url,
//                new Response.Listener<String>() {
//
//                    @Override
//                    public void onResponse(
//                            String response) {
//
//                        intenetAccess=true;
//                    }
//                },
//
//                new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(
//                            VolleyError error) {
//
//                        intenetAccess=false;
//
//                    }
//                });
//
//        try{
//            reQueue.add(request);
//        }
//        catch(Exception e){}
        JSONObject jsonRequest;
        try {
            jsonRequest = new JSONObject();
            jsonRequest.put("sessionId", userModel.getUserSessionId());
            jsonRequest.put("mobileList", stringStringHashMap);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("REFER_AND_EARN", jsonRequest.toString());
            Log.i("REFER API", ApiUrl.URL_REFER_AND_EARN);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REFER_AND_EARN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responses) {
                    try {
                        Log.i("REFER_EARN_Response", responses.toString());
                        String messageAPI = responses.getString("message");
                        String code = responses.getString("code");
                        if (code != null && code.equals("S00")) {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(RealService.this, R.style.AppCompatAlertDialogStyleMusic);
//                            LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                            View viewDialog = inflater1.inflate(R.layout.dialog_earntopay, null, false);
//                            builder.setView(viewDialog);
//                            builder.setCancelable(false);
//                            final AlertDialog alertDialog = builder.create();
//
//                            Button ok = (Button) viewDialog.findViewById(R.id.btnOK);
//
//                            ok.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    startActivity(new Intent(RealService.this, MainActivity.class));
//                                }
//                            });
//
//
//                            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                                @Override
//                                public void onCancel(DialogInterface dialogInterface) {
//
//
//                                }
//                            });
//                            alertDialog.show();
                            intenetAccess = true;

                        } else if (code != null && code.equals("F03")) {
                            intenetAccess = false;
                        } else {
                            CustomToast.showMessage(getApplicationContext(), messageAPI);


                        }
                    } catch (JSONException e) {
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Type", "ERro");
                    intenetAccess = false;
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
        return intenetAccess;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service onCreate");

        isRunning = true;

    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "Service onBind");
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.i(TAG, "Service onRebind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Service onUnBind");
        return true;
    }

    @Override
    public void onDestroy() {

        isRunning = false;

        Log.i(TAG, "Service onDestroy");
        super.onDestroy();
    }


    public class MyBinder extends Binder {
        RealService getService() {
            return RealService.this;
        }
    }
} 