package in.msewa.ecarib.activity.shopping.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.adapter.showCartPayemnt;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.PQCart;
import in.msewa.model.UserModel;
import in.msewa.util.InCartListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.PQTelebuyPaymentHolder;
import in.msewa.ecarib.activity.shopping.ShoppingActivity;


/**
 * Created by Ksf on 4/7/2016.
 */
public class TeleBuyPaymentFragment extends Fragment implements InCartListner {

  PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();
  AlertDialog.Builder payDialog;
  private UserModel session = UserModel.getInstance();
  private PQCart cart = PQCart.getInstance();
  //	private SecurePayment mAsy = null;
  private Button btnTelebuyTermsAndConditions;
  private Button btnproceed, btnPay;
  private TextView tvtotalAmount, tvbalanceInPayQwik;
  private TextView tvTotalNetBalance;
  private String url;
  private double netBalance;
  private RequestQueue rq;
  private JsonObjectRequest postReq;
  private String tag_json_obj = "json_events";
  private JSONObject jsonRequest;
  private LoadingDialog loadDlg;
  private String trxRefNo;
  private ListView lvInCartItem;
  private ImageView imBuyNext;
  private TextView tvTotalRupees;
  private View rootView;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadDlg = new LoadingDialog(getActivity());
    payDialog = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
    try {
      final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
      rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
    } catch (KeyManagementException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_incart_products, container, false);
    tvTotalRupees = (TextView) rootView.findViewById(R.id.tvBuyTotalRupess);
    imBuyNext = (ImageView) rootView.findViewById(R.id.imBuyNext);
    lvInCartItem = (ListView) rootView.findViewById(R.id.hlvInCart);
    imBuyNext.setVisibility(View.GONE);
    btnPay = (Button) rootView.findViewById(R.id.btnPay);
    btnPay.setVisibility(View.VISIBLE);
    btnPay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        doPayment();
      }
    });
    lvInCartItem.setAdapter(new showCartPayemnt(getActivity(), this));
    updateView();
    Bundle extras = getArguments();
    trxRefNo = extras.getString("TRXREF");


    double payQwikBalance = Double.parseDouble(session.getUserBalance().replaceAll("[^\\d.]", ""));
    double cartTotal = Double.parseDouble(cart.getTotalCost().replaceAll("[^\\d.]", ""));
    netBalance = payQwikBalance - cartTotal;
    imBuyNext.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        doPayment();

      }
    });

//        if (session.isValid==1) {
//            tvbalanceInPayQwik.setText("Rs. " + session.getUserBalance());
//        }

//        tvtotalAmount.setText("Rs. " + cart.getTotalCost());
//        payTelebuy.setAmount(cart.getTotalCostNumber());
//        if (netBalance < 0) {
//            netBalance = netBalance * (-1);
////            tvTotalNetBalance.setTextColor(getActivity().getResources().getColor(R.color.mark_red));
//            tvTotalNetBalance.setTextColor(getActivity().getResources().getColor(android.R.color.white));
//        } else {
//            tvTotalNetBalance.setTextColor(getActivity().getResources().getColor(android.R.color.white));
//        }
////        tvTotalNetBalance.setText("Rs. " + new DecimalFormat("##.##").format(netBalance));
//        tvTotalNetBalance.setText("Rs. " + cart.getTotalCost());
//
//        btnTelebuyTermsAndConditions.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vpayqwik.com/Terms&Conditions"));
//                startActivity(browserIntent);
//            }
//        });


    return rootView;
  }


  public void showLowBalanceDialog() {
    payDialog.setTitle("Low PayQwik Balance Load and Pay ?");
    payDialog.setMessage(Html.fromHtml(generateMessageBeforePay()));

    payDialog.setPositiveButton("Load & Pay", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
//                loadDlg.show();
//                loadPayPayqwik();
        Toast.makeText(getActivity(), "Load Money Coming soon", Toast.LENGTH_SHORT).show();
      }
    });
    payDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });

    payDialog.show();

  }

//

  public void showAfterPaymentDialog() {
    payDialog.setTitle("Payment Done");
    payDialog.setMessage(Html.fromHtml(generateMessageAfterPay()));

    payDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        getActivity().finish();
      }
    });


    payDialog.show();

  }


  public void doPayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobile", session.getUserMobileNo());
      jsonRequest.put("amount", String.valueOf(new DecimalFormat("##.##").format(Double.parseDouble(cart.getTotalCost().replace(",", "")))));
      jsonRequest.put("qWikrTxnNo", trxRefNo);

    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("PAYMENTURL", ApiUrl.URL_GET_ALL_SHOPPING_PAYMENT);
      Log.i("PAYMENTREQ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_ALL_SHOPPING_PAYMENT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          loadDlg.dismiss();
          String message = "Sorry Payment is not done, try again later";
          try {
            Log.i("PAYMENTRES", jsonObj.toString());
            String code = jsonObj.getString("code");
            message = jsonObj.getString("message");

            if (code != null && code.equals("S00")) {
              loadDlg.dismiss();
              clearCart();
              showSuccessDialog("your Order ID is " + trxRefNo, "Payment Successful");
            } else {
              loadDlg.dismiss();
              CustomToast.showMessage(getActivity(), message);
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 120000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  public void showSuccessDialog(String message, String Title) {
    CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), Title, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        getActivity().finish();
        clearbackactivity();
        Intent shoppingIntent = new Intent(getActivity(), ShoppingActivity.class);
        startActivity(shoppingIntent);
      }
    });
    builder.show();
  }


  public String generateMessageBeforePay() {

    String source = "<b>TOTAL AMOUNT TO BE PAID</b><br>" + payTelebuy.getAmount() + "<br>"
      + "<b>ORDER ID</b><br>" + payTelebuy.getOrderId() + "<br>"
      + "<b>FULL NAME</b><br>" + payTelebuy.getName() + "<br>"
      + "<b>ADDRESS 1</b><br>" + payTelebuy.getAddress() + "<br>"
      + "<b>CITY/TOWN</b><br>" + payTelebuy.getCity() + "<br>"
      + "<b>STATE</b><br>" + payTelebuy.getState() + "<br>"
      + "<b>PIN CODE</b><br>" + payTelebuy.getZip() + "<br>"
      + "<b>MOBILE NO.</b><br>" + payTelebuy.getTelephone();

    return source;
  }

  public String generateMessageAfterPay() {

    String source = "<b>TOTAL AMOUNT TO BE PAID</b><br>" + payTelebuy.getAmount() + "<br>"
      + "<b>ORDER ID</b><br>" + payTelebuy.getOrderId() + "<br>"
      + "<b>FULL NAME</b><br>" + payTelebuy.getName() + "<br>"
      + "<b>ADDRESS 1</b><br>" + payTelebuy.getAddress() + "<br>"
      + "<b>CITY/TOWN</b><br>" + payTelebuy.getCity() + "<br>"
      + "<b>STATE</b><br>" + payTelebuy.getState() + "<br>"
      + "<b>PIN CODE</b><br>" + payTelebuy.getZip() + "<br>"
      + "<b>MOBILE NO.</b><br>" + payTelebuy.getTelephone() + "<br>"
      + "<b>THANK FOR USING PAYQWIK</b>";
    return source;
  }

  private void clearbackactivity() {
    Intent intent = new Intent("shopping-order-done");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void clearCart() {
    Intent intent = new Intent("cart-clear");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  private void updateView() {
    tvTotalRupees.setText(cart.getTotalCost());
  }

  private void clearCartBadge() {
    Intent intent = new Intent("cart-clear");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void taskCompleted() {
    updateView();
  }

  @Override
  public void selectAddress(String s) {

  }

  @Override
  public void closeCart() {

    clearCartBadge();
    getActivity().finish();
  }

  @Override
  public void deleteAddress(String addID, int pos) {

  }

  @Override
  public void editAddress(String addId, int pos) {

  }

}
