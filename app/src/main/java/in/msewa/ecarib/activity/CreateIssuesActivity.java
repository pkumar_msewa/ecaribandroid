package in.msewa.ecarib.activity;

import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.CompountSpinnerAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.ListcomponentModel;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


public class CreateIssuesActivity extends AppCompatActivity {

    private Spinner spnComponent;
    private EditText etdescription;
    private Button btnSubmt;
    private LoadingDialog loadingDialog;
    private String tag_json_obj = "json_user";
    private JSONObject jsonRequest;
    private ArrayList<ListcomponentModel> listcomponentModels;
    private UserModel userModel = UserModel.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_issues);
        spnComponent = (Spinner) findViewById(R.id.spnComponent);
        etdescription = (EditText) findViewById(R.id.etdescription);
        btnSubmt = (Button) findViewById(R.id.btnSubmt);
        listcomponentModels = new ArrayList<>();
        loadingDialog = new LoadingDialog(CreateIssuesActivity.this);
        getListCustomerCompondsBalance();

        Toolbar issuetoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(issuetoolbar);
        ImageButton issueIb = (ImageButton) findViewById(R.id.ivBackBtn);
        issueIb.setVisibility(View.VISIBLE);
        issueIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSubmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegister();
            }
        });
    }

    public void getListCustomerCompondsBalance() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("merchantCode", "LETS10031");
            jsonRequest.put("projectCode", "LETSPRO03202");

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_LIST_COMPONTENTS_CUSTOMRER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        listcomponentModels.clear();
                        try {
                            ListcomponentModel.deleteAll(ListcomponentModel.class);
                        } catch (SQLiteException e) {
                            e.printStackTrace();
                        }
                        if (code != null && code.equals("S00")) {
                            String jsonString = response.getString("details");
                            JSONArray jsonArray = new JSONArray(jsonString);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                long Id = jsonArray.getJSONObject(i).getLong("id");
                                String Name = jsonArray.getJSONObject(i).getString("name");
                                boolean New = jsonArray.getJSONObject(i).getBoolean("new");
                                ListcomponentModel listcomponentModel = new ListcomponentModel(Id, Name, New);
                                listcomponentModels.add(listcomponentModel);
                            }
                        }
                        if (listcomponentModels != null) {
                            CompountSpinnerAdapter compountSpinnerAdapter = new CompountSpinnerAdapter(CreateIssuesActivity.this, android.R.layout.simple_dropdown_item_1line, listcomponentModels);
                            loadingDialog.dismiss();
                            spnComponent.setAdapter(compountSpinnerAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void attemptRegister() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("merchantCode", "LETS10031");
            jsonRequest.put("projectCode", "LETSPRO03202");
            jsonRequest.put("email", userModel.getUserEmail());
            jsonRequest.put("mobileNo", userModel.getUserMobileNo());
            jsonRequest.put("requester", userModel.getUserFirstName());
            jsonRequest.put("componentName", ((ListcomponentModel) spnComponent.getSelectedItem()).getName());
            jsonRequest.put("description", etdescription.getText().toString());
            jsonRequest.put("transactionId", System.currentTimeMillis());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CREAT_TICKET, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        String msg = response.getString("code");
                        listcomponentModels.clear();
                        ListcomponentModel.deleteAll(ListcomponentModel.class);

                        if (code != null && code.equals("S00")) {
                            CustomToast.showMessage(CreateIssuesActivity.this, "Issue's Created Successfully");
                            finish();
//                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }
}
