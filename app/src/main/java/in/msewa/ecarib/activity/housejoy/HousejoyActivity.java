package in.msewa.ecarib.activity.housejoy;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.permission.PermissionHandler;
import in.msewa.permission.Permissions;
import in.msewa.util.GPSTracker;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import io.appsfly.microapp.MicroAppLauncher;

public class HousejoyActivity extends AppCompatActivity implements LocationListener {

  private UserModel session = UserModel.getInstance();
  private GPSTracker gps;
  private String stringLatitude;
  private String stringLongitude;

  //Volley Tag
  private String tag_json_obj = "json_invite_freinds";
  private JSONObject jsonRequest;

  private String merchantUserName = "houseJoy@msewa.com";

  //HJ onActivityResult
  private String amount;
  private String serviceName;

  private String hJtransactionRefNo;
  private String JobId;
  private String date;
  private String time;
  private String customerName;
  private String mobile;
  private String address;
  private boolean payment_offline;
  boolean isGPSEnable = false;
  boolean isNetworkEnable = false;
  double latitude, longitude;
  LocationManager locationManager;
  Location location;

  private String transactionId;

  private LoadingDialog loadDlg;
  private String playLoad;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    loadDlg = new LoadingDialog(HousejoyActivity.this);
    if (Build.VERSION.SDK_INT >= 23) {
      Permissions.check(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
        "Location  permissions are required because...", new Permissions.Options()
          .setSettingsDialogTitle("Warning!").setRationaleDialogTitle("Info"),
        new PermissionHandler() {
          @Override
          public void onGranted() {
            //do your task
            getLocation();
          }
        });
//
    } else {
      getLocation();
    }


//    if (checkGPS(1)) {
//
//    }
  }

  private boolean checkGPS(int type) {

    if (type == 1) {
      gps = new GPSTracker(this);

      // check if GPS enabled
      if (gps.canGetLocation()) {
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        stringLatitude = String.valueOf(latitude);
        stringLongitude = String.valueOf(longitude);
        Log.i("HJGPS", stringLatitude.toString());
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        return true;
      } else {
        // can't get location
        // GPS or Network is not enabled
        // Ask user to enable GPS/network in settings
        gps.showSettingsAlert(HousejoyActivity.this);
        return false;
      }
    } else {
      gps = new GPSTracker(HousejoyActivity.this);

      // check if GPS enabled
      if (gps.canGetLocation()) {
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        stringLatitude = String.valueOf(latitude);
        stringLongitude = String.valueOf(longitude);
        Log.i("HJGPS", stringLatitude.toString());
        return true;
      } else {
        // can't get location
        // GPS or Network is not enabled
        // Ask user to enable GPS/network in settings
        gps.showSettingsAlert(HousejoyActivity.this);
        return false;
      }
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    JSONObject resultData;

    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == RESULT_OK && requestCode == 9200) {
      String dataStr = data.getStringExtra("postData");
      Log.i("HJDATA", dataStr);
//            CustomToast.showMessage(HousejoyActivity.this, dataStr);
//            CustomToast.showMessage(HousejoyActivity.this, String.valueOf(resultCode));

      try {
        resultData = new JSONObject(dataStr);
        if (resultData.has("payment_offline")) {
          hJtransactionRefNo = resultData.getString("transactionRefNo");
          JobId = resultData.getString("JobId");
          date = resultData.getString("date");
          time = resultData.getString("time");
          customerName = resultData.getString("customerName");
          mobile = resultData.getString("mobile");
          address = resultData.getString("address");
          payment_offline = resultData.getBoolean("payment_offline");
          serviceName = resultData.getString("serviceName");
          successHjPayment();
        } else {
          amount = resultData.getString("amount");
          serviceName = resultData.getString("serviceName");
          JSONObject jsonObject = new JSONObject(data.getStringExtra("postData")).getJSONObject("payload");

          initiateHjPayment(amount, serviceName, jsonObject);
        }
      } catch (JSONException e) {
        e.printStackTrace();
      }
      //Get values from resultData with keys specified by the MicroApp Service Provider .
    } else if (resultCode == RESULT_CANCELED && requestCode == 9200) {
      Log.i("HJRCCANCEL", "" + resultCode);
//            cancelHjPayment();
      if (data != null) {
        String dataStr = data.getStringExtra("result");
        Log.i("HJDATACANCEL", dataStr);
      }
      finish();
    }
  }

  private void initiateHjPayment(String amt, String sn, final JSONObject jsonObject) {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("amount", amt);
      jsonRequest.put("serviceName", sn);
      jsonRequest.put("merchantUserName", merchantUserName);
      jsonRequest.put("mobileNo", session.getUserMobileNo());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("HJI Request", jsonRequest.toString());
      Log.i("HJI URL", ApiUrl.URL_HJ_INITIATE_TXN);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_HJ_INITIATE_TXN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          loadDlg.dismiss();
          try {
            Log.i("HJI Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {
              transactionId = response.getString("transactionId");
              JSONObject userContextData = new JSONObject();
              try {
                userContextData.put("payment", "success");
                userContextData.put("amount", amount);
                userContextData.put("publisher", "VpayQwik");
                userContextData.put("payload", jsonObject);
                userContextData.put("transactionRefNo", transactionId);
              } catch (JSONException e) {
                e.printStackTrace();
              }
              Intent i = new Intent(HousejoyActivity.this, HJPaymentActivity.class);
              i.putExtra("parameters", userContextData.toString());
              i.putExtra("amount", amount);
              i.putExtra("transactionRefNo", transactionId);
              startActivity(i);
              finish();
            } else if (code.equals("F03")) {
              showInvalidSessionDialog();
            } else {
              CustomToast.showMessage(HousejoyActivity.this, message);
              finish();
            }
          } catch (JSONException e) {
            CustomToast.showMessage(HousejoyActivity.this, getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(HousejoyActivity.this, NetworkErrorHandler.getMessage(error, HousejoyActivity.this));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void successHjPayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("transactionRefNo", "");
      jsonRequest.put("jobId", JobId);
      jsonRequest.put("date", date);
      jsonRequest.put("time", time);
      jsonRequest.put("customerName", customerName);
      jsonRequest.put("mobile", mobile);
      jsonRequest.put("mobileNo", session.getUserMobileNo());
      jsonRequest.put("address", address);
      jsonRequest.put("paymentOffline", payment_offline);
      jsonRequest.put("merchantUserName", merchantUserName);
      jsonRequest.put("serviceName", serviceName);
      jsonRequest.put("amount", "");
      jsonRequest.put("hjtxnRefNo", hJtransactionRefNo);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("HJS Request", jsonRequest.toString());
      Log.i("HJS URL", ApiUrl.URL_HJ_SUCCESS_TXN);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_HJ_SUCCESS_TXN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          loadDlg.dismiss();
          try {
            Log.i("HJS Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");

            if (code != null && code.equals("S00")) {

              CustomSuccessDialog builder = new CustomSuccessDialog(HousejoyActivity.this, "Booking Successful", "You will get your booking details in SMS & Email shortly.");
              builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  dialog.dismiss();
                  finish();
                }
              });
              builder.show();

            } else if (code.equals("F03")) {
              showInvalidSessionDialog();
            } else {
              CustomToast.showMessage(HousejoyActivity.this, message);
              finish();
            }
          } catch (JSONException e) {
            CustomToast.showMessage(HousejoyActivity.this, getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(HousejoyActivity.this, getResources().getString(R.string.server_exception));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void cancelHjPayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("transactionRefNo", transactionId);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("HJS Request", jsonRequest.toString());
      Log.i("HJS URL", ApiUrl.URL_HJ_SUCCESS_TXN);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_HJ_SUCCESS_TXN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          loadDlg.dismiss();
          try {
            Log.i("HJS Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");

            if (code != null && code.equals("S00")) {

              CustomSuccessDialog builder = new CustomSuccessDialog(HousejoyActivity.this, "Payment failed.", "Your booking is not completed.");
              builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  dialog.dismiss();
                  finish();
                }
              });
              builder.show();

            } else if (code.equals("F03")) {
              showInvalidSessionDialog();
            } else {
              CustomToast.showMessage(HousejoyActivity.this, message);
              finish();
            }
          } catch (JSONException e) {
            CustomToast.showMessage(HousejoyActivity.this, getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(HousejoyActivity.this, NetworkErrorHandler.getMessage(error, HousejoyActivity.this));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void sendLogout() {
    finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
  }

  private void getLocation() {

    locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
    isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

    if (!isGPSEnable && !isNetworkEnable) {
      buildAlertMessageNoGps();
    } else {
      if (isNetworkEnable) {
        location = null;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 0, HousejoyActivity.this);
        if (locationManager != null) {
          location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
          if (location != null) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();
            JSONObject userContextData = new JSONObject();
            try {
              userContextData.put("categories", "All");
//        userContextData.put("offline_payment", "true");
              userContextData.put("lat", latitude);
              userContextData.put("long", longitude);
              JSONObject contactDetails = new JSONObject();
              contactDetails.put("phone", session.getUserMobileNo());
              contactDetails.put("email", session.getUserEmail());
              contactDetails.put("name", session.getUserFirstName());
              userContextData.put("contactDetails", contactDetails);

              Log.i("HJGPS", userContextData.toString());
            } catch (JSONException e) {
              e.printStackTrace();
            }
            MicroAppLauncher.pushApp(AppMetadata.hj_microapp_id, AppMetadata.hj_intent_string, userContextData, HousejoyActivity.this);
          }
        }
      } else if (isGPSEnable) {
        location = null;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, HousejoyActivity.this);
        if (locationManager != null) {
          location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
          if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            JSONObject userContextData = new JSONObject();
            try {
              userContextData.put("categories", "All");
//        userContextData.put("offline_payment", "true");
              userContextData.put("lat", latitude);
              userContextData.put("long", longitude);
              JSONObject contactDetails = new JSONObject();
              contactDetails.put("phone", session.getUserMobileNo());
              contactDetails.put("email", session.getUserEmail());
              contactDetails.put("name", session.getUserFirstName());
              userContextData.put("contactDetails", contactDetails);

              Log.i("HJGPS", userContextData.toString());
            } catch (JSONException e) {
              e.printStackTrace();
            }
            MicroAppLauncher.pushApp(AppMetadata.hj_microapp_id, AppMetadata.hj_intent_string, userContextData, HousejoyActivity.this);
          }
        }
      }

    }
  }

  private void buildAlertMessageNoGps() {
    final AlertDialog.Builder builder = new AlertDialog.Builder(HousejoyActivity.this);
    builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
      .setCancelable(false)
      .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
        public void onClick(final DialogInterface dialog, final int id) {
          startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
      })
      .setNegativeButton("No", new DialogInterface.OnClickListener() {
        public void onClick(final DialogInterface dialog, final int id) {
          dialog.cancel();
        }
      });
    final AlertDialog alert = builder.create();
    alert.show();
  }


  private void sendNotification(String msg, String image, String details) {

  }

  public Bitmap getBitmapFromUrl(String imageUrl) {
    try {
      URL url = new URL(imageUrl);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setDoInput(true);
      connection.connect();
      StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
      StrictMode.setThreadPolicy(policy);
      InputStream input = connection.getInputStream();
      Bitmap bitmap = BitmapFactory.decodeStream(input);
      return bitmap;

    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;

    }
  }

  @Override
  public void onLocationChanged(Location location) {

  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {

  }

  @Override
  public void onProviderEnabled(String provider) {

  }

  @Override
  public void onProviderDisabled(String provider) {

  }
}
