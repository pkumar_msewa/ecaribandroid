package in.msewa.ecarib.activity.travelKhanaAvtivity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomCoupanDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomSuccessDialogBus;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.businneractivity.BusBookActivity;


public class OrderFoodActivity extends AppCompatActivity {

  private TextView tvAmount, tvDate;
  private EditText etMobile, etName, etEmail, etPnr, etCoachNo, etSeatNo, etExtraComment, etCoupan;
  private Button btnOrderPay;
  private String mobileNo, name, email, pnr, coach, seat, extracomment, itemarray, outletid, amount, trainno, date, arrivaltime, station;

  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();

  private JSONObject jsonRequest;

  //Volley Tag
  private String tag_json_obj = "json_user";


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_order_food);


    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    tvAmount = (TextView) findViewById(R.id.tvAmount);
    tvDate = (TextView) findViewById(R.id.tvDate);
    etMobile = (EditText) findViewById(R.id.etMobile);
    etName = (EditText) findViewById(R.id.etName);
    etEmail = (EditText) findViewById(R.id.etEmail);
    etPnr = (EditText) findViewById(R.id.etPnr);
    etCoachNo = (EditText) findViewById(R.id.etCoachNo);
    etSeatNo = (EditText) findViewById(R.id.etSeatNo);
    etExtraComment = (EditText) findViewById(R.id.etExtraComment);
    etCoupan = (EditText) findViewById(R.id.etCoupan);

    loadDlg = new LoadingDialog(OrderFoodActivity.this);

    if (getEmail(this) != null && !getEmail(this).isEmpty()) {
      etEmail.setText(getEmail(this));
    }

    etMobile.addTextChangedListener(new MyTextWatcher(etMobile));
    etEmail.addTextChangedListener(new MyTextWatcher(etEmail));
    etPnr.addTextChangedListener(new MyTextWatcher(etPnr));

    itemarray = getIntent().getStringExtra("ITEMARRAY");
    outletid = getIntent().getStringExtra("OUTLETID");
    amount = getIntent().getStringExtra("AMOUNT");
    trainno = getIntent().getStringExtra("TRAINNO");
    date = getIntent().getStringExtra("DATE");
    arrivaltime = getIntent().getStringExtra("ARRTIME");
    station = getIntent().getStringExtra("STATION");

    tvAmount.setText("Total Amount: "+amount);
    tvDate.setText("Delivery on: "+date);

    btnOrderPay = (Button) findViewById(R.id.btnOrderPay);
    btnOrderPay.setTextColor(Color.parseColor("#ffffff"));
    btnOrderPay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        submitForm();
      }
    });

    //DONE CLICK ON VIRTUAL KEYPAD
    etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          submitForm();
        }
        return false;
      }
    });

  }

  private void submitForm() {

    if (!validateMobile()) {
      return;
    }
    if (!validateName()) {
      return;
    }
    if (!validateEmail()) {
      return;
    }
    if (!validatePnrNo()) {
      return;
    }
    if (!validateCoachNo()) {
      return;
    }
    if (!validateSeatNo()) {
      return;
    }
    mobileNo = etMobile.getText().toString();
    name = etName.getText().toString();
    email = etEmail.getText().toString();
    pnr = etPnr.getText().toString();
    coach = etCoachNo.getText().toString();
    seat = etSeatNo.getText().toString();
    extracomment = etExtraComment.getText().toString();
    if(etCoupan.getText().toString().isEmpty()){
      promotePayment(Double.parseDouble(amount));
    }else {
      promoteApplyCoupan();
    }
  }


  private boolean validateName() {
    if (etName.getText().toString().trim().isEmpty()) {
      etName.setError("Enter name");
      requestFocus(etName);
      return false;
    } else {
      etName.setError(null);
    }

    return true;
  }

  private boolean validateCoachNo() {
    if (etCoachNo.getText().toString().trim().isEmpty()) {
      etCoachNo.setError("Enter Coach No");
      requestFocus(etCoachNo);
      return false;
    } else {
      etCoachNo.setError(null);
    }

    return true;
  }

  private boolean validateSeatNo() {
    if (etSeatNo.getText().toString().trim().isEmpty()) {
      etSeatNo.setError("Enter Seat No");
      requestFocus(etSeatNo);
      return false;
    } else {
      etSeatNo.setError(null);
    }

    return true;
  }

  private boolean validateEmail() {
    String email = etEmail.getText().toString().trim();
    if (email.isEmpty() || !isValidEmail(email)) {
      etEmail.setError("Enter valid email");
      requestFocus(etEmail);
      return false;
    } else {
      etEmail.setError(null);
    }

    return true;
  }

  private boolean validateMobile() {
    if (etMobile.getText().toString().trim().isEmpty() || etMobile.getText().toString().trim().length() < 10) {
      etMobile.setError("Enter 10 digit no");
      requestFocus(etMobile);
      return false;
    } else {
      etMobile.setError(null);
    }

    return true;
  }

  private static boolean isValidEmail(String email) {
    return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }

  private boolean validatePnrNo() {
    if (etPnr.getText().toString().trim().isEmpty() || etPnr.getText().toString().trim().length() < 10) {
      etPnr.setError("Enter 10 digit PNR no");
      requestFocus(etPnr);
      return false;
    } else {
      etPnr.setError(null);
    }

    return true;
  }

  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }

  private class MyTextWatcher implements TextWatcher {

    private View view;

    private MyTextWatcher(View view) {
      this.view = view;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {
      int i = view.getId();
      if (i == R.id.etRegisterEmail) {
        validateEmail();
      } else if (i == R.id.etRegisterMobile) {
        validateMobile();
      } else if (i == R.id.etPnr) {
        validatePnrNo();
      }

    }
  }

  private void promotePayment(double payAmount) {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("items", new JSONArray(itemarray));
      jsonRequest.put("cod", "0");
      jsonRequest.put("order_outlet_id", outletid);
      jsonRequest.put("pnr", pnr);
      jsonRequest.put("coach", coach);
      jsonRequest.put("seat", seat);
      jsonRequest.put("customer_comment", extracomment);
      jsonRequest.put("totalCustomerPayable", payAmount);
      jsonRequest.put("name", name);
      jsonRequest.put("contact_no", mobileNo);
      jsonRequest.put("mail_id", email);
      jsonRequest.put("train_number", trainno);
      jsonRequest.put("station_code", station);
      jsonRequest.put("date", date);
      jsonRequest.put("eta", arrivaltime);


    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("ORDERREQ", jsonRequest.toString());
      Log.i("ORDERURL", ApiUrl.URL_TRAVELKHANA_ORDER);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_ORDER, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("Registration Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {
              showPaymentSuccessDialog(message,"Payment Confirm");
              loadDlg.dismiss();
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            }  else {
              loadDlg.dismiss();
              CustomToast.showMessage(OrderFoodActivity.this, message);
              finish();
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          loadDlg.dismiss();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "12345");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void promoteApplyCoupan() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("cartDetails", getCoupanArray());
      jsonRequest.put("mobileNumber", mobileNo);
      jsonRequest.put("mailId", email);


    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("COUPANREQ", jsonRequest.toString());
      Log.i("COUPANURL", ApiUrl.URL_TRAVELKHANA_APPLY_COUPAN);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_APPLY_COUPAN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("Registration Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {
              JSONArray cartDetails = response.getJSONArray("cartDetails");
              double discount = cartDetails.getJSONObject(0).getDouble("discount");
              showAppliedCoupanDialog(discount);
              loadDlg.dismiss();
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            }  else {
              loadDlg.dismiss();
              CustomToast.showMessage(OrderFoodActivity.this, message);
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          loadDlg.dismiss();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "12345");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  static String getEmail(Context context) {
    AccountManager accountManager = AccountManager.get(context);
    Account account = getAccount(accountManager);

    if (account == null) {
      return null;
    } else {
      return account.name;
    }
  }

  private static Account getAccount(AccountManager accountManager) {
    Account[] accounts = accountManager.getAccountsByType("com.google");
    Account account;
    if (accounts.length > 0) {
      account = accounts[0];
    } else {
      account = null;
    }
    return account;
  }
  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(OrderFoodActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });

    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(OrderFoodActivity.this).sendBroadcast(intent);
  }

  @Override
  protected void onResume() {
    super.onResume();

  }

  public void showPaymentSuccessDialog(String message, String Title) {
    CustomSuccessDialog builder = new CustomSuccessDialog(OrderFoodActivity.this, Title, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
        sendFinish();
        finish();
      }
    });
    builder.show();
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(OrderFoodActivity.this).sendBroadcast(intent);
  }

  private void sendFinish() {
    Intent intent = new Intent("food-book-done");
    LocalBroadcastManager.getInstance(OrderFoodActivity.this).sendBroadcast(intent);
  }

  public void showCoupanDialog() {
    LayoutInflater factory = LayoutInflater.from(this);
    final View coupanDialogView = factory.inflate(R.layout.dialog_coupan, null);
    CustomCoupanDialog builder = new CustomCoupanDialog(OrderFoodActivity.this);
    builder.setView(coupanDialogView);
    etCoupan = (EditText) coupanDialogView.findViewById(R.id.etCoupan);
    builder.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        if(!etCoupan.getText().toString().isEmpty()){
          promoteApplyCoupan();
          dialog.dismiss();
        }else {
          CustomToast.showMessage(OrderFoodActivity.this,"Enter Coupan Code");
        }
      }
    });
    builder.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        promotePayment(Double.parseDouble(amount));
        dialog.dismiss();
      }
    });
    builder.show();
  }

  public void showAppliedCoupanDialog(final double discount) {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(discount), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage(discount));
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(OrderFoodActivity.this, "Coupan Applied Successfully", result);
    builder.setPositiveButton("CONTINUE PAYMENT", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        double totatAmount = Double.parseDouble(amount)-discount;
        promotePayment(totatAmount);
      }
    });
    builder.show();
  }

  public String getSuccessMessage(double discount) {
    double totatAmount = Double.parseDouble(amount)-discount;
    return "<b><font color=#000000> Total Amount: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
      "<b><font color=#000000> Discount : </font></b>" + "<font>" + String.valueOf(discount) + "</font><br>" +
      "<b><font color=#000000> Payable Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + String.valueOf(totatAmount) + "</font><br><br>";
  }

  public JSONArray getCoupanArray(){
    JSONArray coupanArray = new JSONArray();
    JSONObject object = new JSONObject();
    try {
      object.put("couponCode", etCoupan.getText().toString());
      object.put("totalSP", amount);
      object.put("outletId", outletid);
      coupanArray.put(object);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return coupanArray;
  }
}
