package in.msewa.ecarib.activity.shopping;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.fragment.TeleBuyInCartFragment;


/**
 * Created by Ksf on 4/6/2016.
 */
public class TeleBuyPaymentActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telebuy_pay);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRefresh();
                onBackPressed();
            }
        });
        TeleBuyInCartFragment inCartFragment = new TeleBuyInCartFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameInCart, inCartFragment).addToBackStack("");
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
      if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
        sendRefresh();
        getSupportFragmentManager().popBackStack();
      } else {
        sendRefresh();
        super.onBackPressed();
      }
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "6");
        LocalBroadcastManager.getInstance(TeleBuyPaymentActivity.this).sendBroadcast(intent);
    }




}
