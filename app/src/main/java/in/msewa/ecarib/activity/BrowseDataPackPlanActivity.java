package in.msewa.ecarib.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.fragment.fragmentbrowseplandatapack.ThreeGDataFragment;
import in.msewa.fragment.fragmentbrowseplandatapack.TwoGDataFragment;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.MobilePlansModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.MobilePlansCheck;


/**
 * Created by Ksf on 3/23/2016.
 */
public class BrowseDataPackPlanActivity extends AppCompatActivity {
    /*
     * Sliding tabs Setup
	 */
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    CharSequence TitlesEnglish[] = {"2G", "3G"};
    int NumbOfTabs = 2;

    private String operatorCode;
    private String circleCode;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    private JSONObject jsonRequest;
    private LoadingDialog loadingDialog;

    //Volley Tag
    private String tag_json_obj = "json_browse_plan";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(BrowseDataPackPlanActivity.this);
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        circleCode = getIntent().getStringExtra("circleCode");
        operatorCode = getIntent().getStringExtra("operatorCode");
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        browsePlan();
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                TwoGDataFragment tab1 = new TwoGDataFragment();
                return tab1;
            } else {
                ThreeGDataFragment tab3 = new ThreeGDataFragment();
                return tab3;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }


        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }


    private void browsePlan() {
        jsonRequest = new JSONObject();
        loadingDialog.show();
        try {
            jsonRequest.put("circleCode", circleCode);
            jsonRequest.put("operatorCode", operatorCode);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            AndroidNetworking.post(ApiUrl.URL_BROWSE_PLAN)
                    .addJSONObjectBody(jsonRequest) // posting json
                    .setTag("test")
                    .setPriority(Priority.HIGH)
                    .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
                    .build().setAnalyticsListener(new AnalyticsListener() {
                @Override
                public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                    Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
                }
            }).getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    Log.i("API RESPOMSE", jsonObj.toString());
                    ArrayList<MobilePlansModel> talkTimePlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> topUpPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> threeGPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> twoGPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> specialPlans = new ArrayList<>();


                    try {
                        String responseString = jsonObj.getString("response");
                        JSONObject response = new JSONObject(responseString);

                        JSONArray planArray = response.getJSONArray("plans");
                        for (int i = 0; i < planArray.length(); i++) {
                            JSONObject c = planArray.getJSONObject(i);
                            String plan_name = c.getString("planName");
                            String plan_desc = c.getString("description");
                            String plan_amount = c.getString("amount");
                            String plan_validity = c.getString("validity");
                            String plan_operator_code = c.getString("smsDaakCode");

                            if (plan_name.equals("Full Talktime")) {
                                talkTimePlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("Top up")) {
                                topUpPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("3G Data") || plan_name.equals("3G Plans")) {
                                threeGPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("2G Data") || plan_name.equals("Data Plans")) {
                                twoGPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else {
                                specialPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            }


                        }

                        MobilePlansCheck plans = MobilePlansCheck.getInstance();
                        plans.setTalkTimePlans(talkTimePlans);
                        plans.setTopUpPlans(topUpPlans);
                        plans.setThreeGPlans(threeGPlans);
                        plans.setTwoGPlans(twoGPlans);
                        plans.setSpecialPlans(specialPlans);

                        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
                        mSlidingTabLayout.setupWithViewPager(mainPager);
                        loadingDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Oops!! Exception Caused", Toast.LENGTH_SHORT).show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    loadingDialog.dismiss();
                    finish();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
                }
            });
        }
    }
//    public static final int getColors(Context context, int id) {
//        final int version = Build.VERSION.SDK_INT;
//        if (version >= 23) {
//            return ContextCompat.getColor(context, id);
//        } else {
//            return context.getResources().getColor(id);
//        }
//    }

}
