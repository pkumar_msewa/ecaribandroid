package in.msewa.ecarib.activity.flightinneracitivty;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import in.msewa.adapter.FlightDetailAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.NonScrollListView;
import in.msewa.custom.ResultIPC;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.DomesticFlightModel;
import in.msewa.model.FlightListModel;
import in.msewa.model.FlightModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 11/28/2016.
 */
public class FlightPriceTwoWayActivity extends AppCompatActivity {

  //    private JSONArray jsonUp, jsonDown;
  public static String jsonUpString;
  public static String jsonDownString;
  private FlightListModel flightModelUp, flightModelDown;
  private JSONObject segmentObject;
  private JSONObject segmentObject2;
  private LoadingDialog loadingDialog;
  //    private JSONObject parms = null;
  private int postUp = 0, postDown = 0;
  //Volley
  private String tag_json_obj = "json_travel";

  private double flightUpPrice = 0;
  private double flightDownPrice = 0;


  //Views
  private String sourceCode = "", destinationCode = "", dateOfJourney = "", dateOfReturn = "";
  private int adultNo = 0, childNo = 0, infantNo = 0;
  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private Button btnBookFlight;

  private NonScrollListView lvFlightDetailUp;
  private NonScrollListView lvFlightDetailRound;


  private TextView tvflightDetailsTotalFare, tvflightDetailsTaxFees, tvflightDetailsBaseFare, tvflightDetailsPassengers, tvFlightFareRuleDown, tvFlightFareRuleUp;
  private TextView tvFlightTotalTimeUp, tvFlightTotalTimeDown;


  private JSONObject jsonRequest;


  private String flightClass = "";


  private JSONObject jsonReceived;
  private JSONObject jsonFareReceived;
  private UserModel session = UserModel.getInstance();


  private String jsonToSendUp = "";
  private String jsonFareToSendUp = "";

  private String jsonToSendDown = "";
  private String jsonFareToSendDown = "";
  private JSONObject travelJson;
  private boolean value = false, valuedown = false;
  private JSONObject oneWayTRIP, twoWayTRIP;
  private double basefirstfare, totalfirstPrice, totalfirstTax, basesecondfare, totalsecondPrice, totalsecondTax;
  private int citylist;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_flight_price_twoway);
    travelJson = new JSONObject();
    loadingDialog = new LoadingDialog(FlightPriceTwoWayActivity.this);


    fillingViews();

//        parseUpperPrice();
  }


  private void fillingViews() {
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

    lvFlightDetailUp = (NonScrollListView) findViewById(R.id.lvFlightDetailUp);
    lvFlightDetailRound = (NonScrollListView) findViewById(R.id.lvFlightDetailRound);
    btnBookFlight = (Button) findViewById(R.id.btnBookFlight);

    tvflightDetailsTotalFare = (TextView) findViewById(R.id.tvflightDetailsTotalFare);
    tvflightDetailsTaxFees = (TextView) findViewById(R.id.tvflightDetailsTaxFees);
    tvflightDetailsBaseFare = (TextView) findViewById(R.id.tvflightDetailsBaseFare);
    tvflightDetailsPassengers = (TextView) findViewById(R.id.tvflightDetailsPassengers);
    tvFlightFareRuleUp = (TextView) findViewById(R.id.tvFlightFareRuleUp);
    tvFlightFareRuleDown = (TextView) findViewById(R.id.tvFlightFareRuleDown);

    tvFlightTotalTimeUp = (TextView) findViewById(R.id.tvFlightTotalTimeUp);
    tvFlightTotalTimeDown = (TextView) findViewById(R.id.tvFlightTotalTimeDown);

    flightModelUp = getIntent().getParcelableExtra("flightUp");
    flightModelDown = getIntent().getParcelableExtra("flightDown");

    try {
      segmentObject = new JSONObject(flightModelUp.getFlightJsonString());
      segmentObject2 = new JSONObject(flightModelDown.getFlightJsonString());
    } catch (JSONException e) {
      e.printStackTrace();
    }

    postUp = getIntent().getIntExtra("postUp", 0);
    postDown = getIntent().getIntExtra("postDown", 0);

    dateOfJourney = getIntent().getStringExtra("dateOfDep");
    dateOfReturn = getIntent().getStringExtra("dateOfReturn");

    sourceCode = getIntent().getStringExtra("sourceCode");
    destinationCode = getIntent().getStringExtra("destinationCode");

    adultNo = getIntent().getIntExtra("adultNo", 0);
    childNo = getIntent().getIntExtra("childNo", 0);
    infantNo = getIntent().getIntExtra("infantNo", 0);
    flightClass = getIntent().getStringExtra("flightClass");


    jsonToSendUp = getIntent().getStringExtra("jsonToSendUp");
    jsonFareToSendUp = getIntent().getStringExtra("jsonFareToSendUp");

    jsonToSendDown = getIntent().getStringExtra("jsonToSendDown");
    jsonFareToSendDown = getIntent().getStringExtra("jsonFareToSendDown");

    //Upper
    LayoutInflater layoutInflaterUp = LayoutInflater.from(getApplicationContext());
    View dateViewUp = layoutInflaterUp.inflate(R.layout.header_flight_detail, null);
    TextView tvHeaderBusListDateUp = (TextView) dateViewUp.findViewById(R.id.tvHeaderBusListDate);
    TextView tvHeaderFlightNameUp = (TextView) dateViewUp.findViewById(R.id.tvHeaderFlightName);
    ImageView ivHeaderListDate = (ImageView) dateViewUp.findViewById(R.id.ivHeaderListDate);

    ImageView ivHeaderFlightUp = (ImageView) dateViewUp.findViewById(R.id.ivHeaderFlight);
    ivHeaderFlightUp.setImageResource(AppMetadata.getFLightImage(flightModelUp.getFlightListArray().get(0).getFlightCode(), tvHeaderFlightNameUp));

    ivHeaderListDate.setImageResource(R.drawable.ic_no_flight);
//        String[] splitDateUp = dateOfJourney.split("-");
    tvHeaderBusListDateUp.setText(destinationCode + " " + getResources().getString(R.string.arrow) + " " + sourceCode + "\n" + dateOfJourney);
    tvHeaderFlightNameUp.setText(flightModelUp.getFlightListArray().get(0).getFlightName());
    lvFlightDetailUp.addHeaderView(dateViewUp);

    //Down
    LayoutInflater layoutInflaterDown = LayoutInflater.from(getApplicationContext());
    View dateViewDown = layoutInflaterDown.inflate(R.layout.header_flight_detail, null);
    TextView tvHeaderBusListDateDown = (TextView) dateViewDown.findViewById(R.id.tvHeaderBusListDate);
    TextView tvHeaderFlightNameDown = (TextView) dateViewDown.findViewById(R.id.tvHeaderFlightName);
    ImageView ivHeaderListDateDown = (ImageView) dateViewDown.findViewById(R.id.ivHeaderListDate);
    ivHeaderListDateDown.setImageResource(R.drawable.ic_no_flight);

    ImageView ivHeaderFlightDown = (ImageView) dateViewDown.findViewById(R.id.ivHeaderFlight);
    ivHeaderFlightDown.setImageResource(AppMetadata.getFLightImage(flightModelDown.getFlightListArray().get(0).getFlightCode(), tvHeaderFlightNameDown));

//        String[] splitDateDown = dateOfReturn.split("-");
    tvHeaderBusListDateDown.setText(destinationCode + " " + getResources().getString(R.string.arrow) + " " + sourceCode + "\n" + dateOfReturn);
    tvHeaderFlightNameDown.setText(flightModelDown.getFlightListArray().get(0).getFlightName());
    lvFlightDetailRound.addHeaderView(dateViewDown);
    citylist = getIntent().getIntExtra("citylist", 0);
    ArrayList<DomesticFlightModel> resultIPC = ResultIPC.get().getaddOnsFlight(citylist);
    FlightDetailAdapter flightDetailAdapterUp = new FlightDetailAdapter(getApplicationContext(), flightModelUp.getFlightListArray(), resultIPC);
    lvFlightDetailUp.setAdapter(flightDetailAdapterUp);

    FlightDetailAdapter flightDetailAdapterDown = new FlightDetailAdapter(getApplicationContext(), flightModelDown.getFlightListArray(), resultIPC);
    lvFlightDetailRound.setAdapter(flightDetailAdapterDown);


    tvflightDetailsTotalFare.setText(calculateFares(flightModelUp.getFlightNetFare(), flightModelDown.getFlightNetFare()));
//        tvflightDetailsTaxFees.setText(calculateFares(flightModelUp.getFlightTaxFare(), flightModelDown.getFlightTaxFare()));
//        tvflightDetailsBaseFare.setText(calculateFares(flightModelUp.getFlightActualBaseFare(), flightModelDown.getFlightActualBaseFare()));


    tvFlightFareRuleUp.setText(flightModelUp.getFlightListArray().get(0).getFlightRule());
    tvFlightFareRuleDown.setText(flightModelDown.getFlightListArray().get(0).getFlightRule());


    tvFlightTotalTimeDown.setText("");
    tvFlightTotalTimeUp.setText("");

    //ALL THREE
    if (adultNo != 0 && childNo != 0 && infantNo != 0) {
      tvflightDetailsPassengers.setText(adultNo + " Adults, " + childNo + " Children, " + infantNo + " Infant");
    }
    //ADULT AND CHILD
    else if (adultNo != 0 && childNo != 0 && infantNo == 0) {
      tvflightDetailsPassengers.setText(adultNo + " Adults, " + childNo + " Children");
    }
    //ADULT AND INFANT
    else if (adultNo != 0 && infantNo != 0 && childNo == 0) {
      tvflightDetailsPassengers.setText(adultNo + " Adults, " + infantNo + " Infant");
    }
    //CHILD AND INFANT
    else if (adultNo == 0 && childNo != 0 && infantNo != 0) {
      tvflightDetailsPassengers.setText(adultNo + " Children, " + infantNo + " Infant");
    }

    //ONLY ADULT
    else if (adultNo != 0 && childNo == 0 && infantNo == 0) {
      tvflightDetailsPassengers.setText(adultNo + " Adults ");
    }
    //ONLY INFANT
    else if (adultNo == 0 && childNo == 0 && infantNo != 0) {
      tvflightDetailsPassengers.setText(adultNo + " Adults, " + infantNo + " Infant");
    }
    //ONLY CHILD
    else if (adultNo == 0 && infantNo == 0 && childNo != 0) {
      tvflightDetailsPassengers.setText(childNo + " Children");
    } else {
      tvflightDetailsPassengers.setText("");
    }


    btnBookFlight.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        attemptBook();
      }
    });
  }


  private String calculateFares(double firstPrice, double secondPrice) {
    double totalPrice = 0;
    totalPrice = firstPrice + secondPrice;
    return getResources().getString(R.string.rupease) + " " + totalPrice;
  }

  private String calculateFares1(double firstPrice, double secondPrice) {
    double totalPrice = 0;
    totalPrice = firstPrice + secondPrice;
    return String.valueOf(totalPrice);
  }

  public void attemptBook() {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("origin", sourceCode);
      jsonRequest.put("destination", destinationCode);
      jsonRequest.put("beginDate", dateOfJourney);
      jsonRequest.put("tripType", "OneWay");
      jsonRequest.put("cabin", flightClass);
      jsonRequest.put("adults", adultNo);
      jsonRequest.put("childs", childNo);
      jsonRequest.put("infants", infantNo);
      jsonRequest.put("traceId", "AYTM00011111111110002");
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

//        if (jsonRequest != null) {
//
//
//            Log.i("URL LOGIN", ApiUrl.URL_LOGIN);
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_BOOK_ONEWAY, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        Log.i("LOgin Resonse", response.toString());
//                        String code = response.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            loadingDialog.dismiss();
    try {
      fetchLatestPriceUp();
    } catch (JSONException e) {
      e.printStackTrace();
    }

//                        } else if (code != null && code.equals("F03")) {
//                            loadingDialog.dismiss();
//                            showInvalidSessionDialog();
//                        } else {
//                            String message = response.getString("message");
//                            loadingDialog.dismiss();
//                            CustomToast.showMessage(getApplicationContext(), message);
//
//                        }
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    CustomToast.showMessage(FlightPriceTwoWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceTwoWayActivity.this));
//                    error.printStackTrace();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
//                    Log.i("Header", map.get("hash"));
//                    return map;
//                }
//
//            };
//
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
  }


  public void fetchLatestPriceUp() throws JSONException {
    loadingDialog.show();


//        try {
//            jsonReceived = new JSONObject(flightModelUp.getFlightListArray().get(0).getJsrequestsegment());
//            jsonFareReceived = new JSONObject(flightModelUp.getFlightListArray().get(0).getJsonFareString());
//            jsonReceived.remove("cabinClasses");
//            jsonReceived.remove("ssrDetails");
//            jsonReceived.remove("aircraftCode");
//            jsonReceived.remove("aircraftType");
//            jsonReceived.remove("availableSeat");
//            jsonReceived.remove("fareBasisCode");
//            jsonReceived.remove("flightDesignator");
//            jsonReceived.remove("flightDetailRefKey");
//            jsonReceived.remove("group");
//            jsonReceived.remove("remarks");
//            jsonReceived.remove("providerCode");
//            jsonReceived.remove("numberOfStops");
//
//            jsonReceived.put("sessionId", session.getUserSessionId());
//            jsonReceived.put("engineID", segmentObject.getString("engineID"));
//            jsonReceived.put("basicFare", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("basicFare"));
//            jsonReceived.put("exchangeRate", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("exchangeRate"));
//            jsonReceived.put("baseTransactionAmount", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
//            jsonReceived.put("cancelPenalty", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
//            jsonReceived.put("changePenalty", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
//            jsonReceived.put("chargeCode", "");
//            jsonReceived.put("chargeType", "FarePrice");
//            jsonReceived.put("markUP", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
//            jsonReceived.put("paxType", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
//            jsonReceived.put("refundable", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));
//
//            jsonReceived.put("totalFare", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
//            jsonReceived.put("totalTax", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
//            jsonReceived.put("totalFareWithOutMarkUp", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
//            jsonReceived.put("totalTaxWithOutMarkUp", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
//            jsonReceived.put("isCache", segmentObject.getBoolean("isCache"));
//
//            jsonReceived.put("isHoldBooking", segmentObject.getBoolean("isHoldBooking"));
//            jsonReceived.put("isInternational", segmentObject.getBoolean("isInternational"));
//            jsonReceived.put("isRoundTrip", segmentObject.getBoolean("isRoundTrip"));
//
//            jsonReceived.put("isSpecial", segmentObject.getBoolean("isSpecial"));
//            jsonReceived.put("isSpecialId", segmentObject.getBoolean("isSpecialId"));
//            jsonReceived.put("journeyIndex", segmentObject.getInt("journeyIndex"));
//            jsonReceived.put("nearByAirport", segmentObject.getBoolean("nearByAirport"));
//            jsonReceived.put("searchId", segmentObject.getString("searchId"));
//
//            jsonReceived.put("traceId", "AYTM00011111111110002");
//            jsonReceived.put("tripType", "OneWay");
//            jsonReceived.put("transactionAmount", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));
//
//            jsonReceived.put("childs", childNo);
//            jsonReceived.put("infants", infantNo);
//            jsonReceived.put("adults", adultNo);
//
//
//            jsonReceived.put("beginDate", dateOfJourney);
//            jsonReceived.put("bondType", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundType"));
//            jsonReceived.put("fareRule", segmentObject.getString("fareRule"));
//
//            jsonReceived.put("isBaggageFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isBaggageFare"));
//
//            jsonReceived.put("isSSR", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isSSR"));
//
//            jsonReceived.put("itineraryKey", segmentObject.getString("itineraryKey"));
//            jsonReceived.put("flightName", segmentObject.getString("engineID"));
//
//            jsonReceived.put("journeyTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
//
//            jsonReceived.put("amount", flightModelUp.getTotalFare());
//            jsonRequest.put("isConnecting", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getBoolean("isConnecting"));
//            jsonReceived.put("journeyTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
//
//            String response = jsonReceived.toString().replace("\"boundTypes\":", "\"boundType\":");
//            jsonReceived = new JSONObject(response);
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonReceived = null;
//        }
//
//        if (jsonReceived != null) {


    Log.i("URL", ApiUrl.URL_FETCH_RECHECK_PRICE);
//        Log.i("jsonReceived", jsonReceived.toString());
    String url;

//            if (!flightModelUp.getContingFlight().equalsIgnoreCase("{}")) {
    jsonReceived = new JSONObject(flightModelUp.getContingFlight());
    jsonReceived.put("sessionId", session.getUserSessionId());
    url = ApiUrl.URL_FETCH_RECHECK_PRICE;
    Log.i("URLS", url);
    value = false;

//            } else {
//                url = ApiUrl.URL_FETCH_ONE_WAY_PRICE;
//                value = true;
//            }
//
    Log.i("firstflightRequest", jsonReceived.toString());
    JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, url, jsonReceived, new Response.Listener<JSONObject>() {
      @Override
      public void onResponse(JSONObject response) {
        try {
          Log.i("Price Resonse first", response.toString());
          String code = response.getString("code");

          if (code != null && code.equals("S00")) {
            JSONObject jsonDetails = response.getJSONObject("details");
            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");
            JSONObject jsonJrnyObj = jsonJourney.getJSONObject(0);
            JSONArray jsonSegmentArray = jsonJrnyObj.getJSONArray("segments");
            JSONObject contingFlight = new JSONObject();

            String jsonSegmentString1 = jsonSegmentArray.getJSONObject(0).toString();
            for (int k = 0; k < jsonSegmentArray.length(); k++) {

              String jsonSegmentString = jsonSegmentArray.getJSONObject(k).toString();
              ArrayList<FlightModel> flightArray = new ArrayList<>();
              JSONObject segmentObj = jsonSegmentArray.getJSONObject(k);
              JSONArray bondArray = segmentObj.getJSONArray("bonds");

              JSONObject bonObj = bondArray.getJSONObject(0);
              JSONArray legArray = bonObj.getJSONArray("legs");
              JSONArray legArrays = bonObj.getJSONArray("legs");
              JSONObject segmentConting = jsonSegmentArray.getJSONObject(k);

              JSONArray bounds = new JSONArray();
              JSONObject bondary = bondArray.getJSONObject(0);
              bondary.remove("specialServices");
              bondary.put("baggageFare", bondary.getString("baggageFare"));
              bondary.put("ssrFare", bondary.getString("ssrFare"));
              bondary.remove("cabin");
              JSONArray jsonArray = new JSONArray();
              JSONArray travelDetails = new JSONArray();
              for (int j = 0; j < legArray.length(); j++) {
                JSONObject d = legArray.getJSONObject(j);
                d.remove("cabinClasses");
                d.remove("ssrDetails");
                d.remove("amount");
                d.remove("cabin");
                d.put("cabin", flightClass);
//                                d.remove("group");
                d.remove("isConnecting");
                d.remove("numberOfStops");
//                                d.remove("providerCode");
                d.remove("remarks");
                d.remove("journeyTime");
                jsonArray.put(d);
              }
              for (int i = 0; i > jsonArray.length(); i++) {
                jsonArray.getJSONObject(i).remove("journeyTime");
              }
              bondary.put("legs", jsonArray);
              bounds.put(bondary);
              contingFlight.put("bonds", bounds);

              Log.i("valuesss", contingFlight.toString());
              for (int j = 0; j < legArrays.length(); j++) {
                JSONObject d1 = legArrays.getJSONObject(j);
                d1.remove("cabinClasses");
                d1.remove("aircraftCod");
                d1.remove("ssrDetails");
                d1.remove("amount");
                d1.remove("aircraftType");
                d1.remove("availableSeat");
                d1.remove("journeyTime");
                d1.remove("boundType");
                d1.remove("amount");
                d1.remove("group");
//                                    d1.remove("sold");
                d1.remove("arrivalTerminal");
                d1.remove("currencyCode");
                d1.remove("availableSeat");
                d1.remove("isConnecting");
                d1.remove("numberOfStops");
                d1.remove("providerCode");
                d1.remove("remarks");
                d1.remove("aircraftCode");
                d1.remove("ircraftType");
                d1.remove("status");
                d1.remove("carrierCode");
                d1.remove("departureTerminal");
                d1.remove("fareBasisCode");
                d1.remove("fareClassOfService");
                d1.remove("flightDesignator");
                d1.remove("flightDetailRefKey");
                travelDetails.put(d1);
              }


              HashMap<String, Object> pairs = new HashMap<String, Object>();
              for (int i = 0; i < travelDetails.length(); i++) {
                JSONObject j = travelDetails.optJSONObject(i);
                Iterator it = j.keys();
                while (it.hasNext()) {
                  String n = (String) it.next();
                  pairs.put(n, j.getString(n));
                }
              }
              pairs.remove("sold");
              pairs.remove("capacity");
              Log.i("HASHMAP", String.valueOf(getJsonFromMap(pairs).toString()));
              travelJson.put("Oneway", new JSONArray().put(getJsonFromMap(pairs)));
              String s = segmentObj.getJSONArray("fares").toString();
              s = s.replace("\"fares\":", "\"bookFares\":");
              JSONObject jsonArray1 = new JSONObject(s.substring(1, s.length() - 1));
              contingFlight.put("fares", jsonArray1);
              segmentConting.remove("bondType");
              contingFlight.put("baggageFare", segmentConting.getString("baggageFare"));
              contingFlight.put("cache", segmentConting.getString("cache"));
              contingFlight.put("deeplink", segmentConting.getString("deeplink"));
              contingFlight.remove("destination");
              contingFlight.put("holdBooking", segmentConting.getString("holdBooking"));
              contingFlight.put("international", segmentConting.getString("international"));
              contingFlight.put("roundTrip", segmentConting.getString("roundTrip"));
              contingFlight.put("special", segmentConting.getString("special"));
              contingFlight.put("specialId", segmentConting.getString("specialId"));
              contingFlight.put("engineID", segmentConting.getString("engineID"));
              contingFlight.put("fareRule", segmentConting.getString("fareRule"));
              contingFlight.put("itineraryKey", segmentConting.getString("itineraryKey"));
              contingFlight.put("journeyIndex", segmentConting.getString("journeyIndex"));
              contingFlight.put("nearByAirport", segmentConting.getString("nearByAirport"));
              contingFlight.put("searchId", segmentConting.getString("searchId"));
//                            contingFlight.put("remark", segmentConting.getString("remark"));
              if (value) {
                oneWayTRIP = contingFlight;
              }

              Log.i("contingflightBooking", String.valueOf(contingFlight));

            }

            JSONObject jsonSegment = jsonSegmentArray.getJSONObject(0);
            JSONArray fareArray = jsonSegment.getJSONArray("fares");
            JSONObject fareObj = fareArray.getJSONObject(0);
            basefirstfare = fareObj.getDouble("basicFare");
            totalfirstPrice = fareObj.getDouble("totalFareWithOutMarkUp");
            totalfirstTax = fareObj.getDouble("totalTaxWithOutMarkUp");


            fetchLatestPriceDown(totalfirstPrice, totalfirstPrice, basefirstfare, contingFlight.toString(), jsonSegmentString1);


          } else if (code != null && code.equals("F03")) {
            loadingDialog.dismiss();
            showInvalidSessionDialog();
          } else {
            loadingDialog.dismiss();
            if (response.has("message")) {
              String message = response.getString("message");
              CustomToast.showMessage(FlightPriceTwoWayActivity.this, message);
            } else {
              CustomToast.showMessage(FlightPriceTwoWayActivity.this, "Error occurred");
            }


          }
        } catch (JSONException e) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
          e.printStackTrace();
        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        loadingDialog.dismiss();
        CustomToast.showMessage(FlightPriceTwoWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceTwoWayActivity.this));
        error.printStackTrace();
      }
    }) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
        Log.i("Header", map.get("hash"));
        return map;
      }

    };

    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
  }

  public void fetchLatestPriceDown(final double upPrice, final double upTotax, final double upbaseFare, final String flightUpJson, final String flightSegment) throws JSONException {
    loadingDialog.show();


//        try {
//            jsonReceived = new JSONObject(flightModelDown.getFlightListArray().get(0).getJsrequestsegment());
//            jsonFareReceived = new JSONObject(flightModelDown.getFlightListArray().get(0).getJsonFareString());
//            jsonReceived.remove("cabinClasses");
//            jsonReceived.remove("ssrDetails");
//            jsonReceived.remove("aircraftCode");
//            jsonReceived.remove("aircraftType");
//            jsonReceived.remove("availableSeat");
//            jsonReceived.remove("fareBasisCode");
//            jsonReceived.remove("flightDesignator");
//            jsonReceived.remove("flightDetailRefKey");
//            jsonReceived.remove("group");
//            jsonReceived.remove("remarks");
//            jsonReceived.remove("providerCode");
//            jsonReceived.remove("numberOfStops");
//
//            jsonReceived.put("sessionId", session.getUserSessionId());
//
//            jsonReceived.put("engineID", segmentObject2.getString("engineID"));
//            jsonReceived.put("basicFare", segmentObject2.getJSONArray("fare").getJSONObject(0).getDouble("basicFare"));
//            jsonReceived.put("exchangeRate", segmentObject2.getJSONArray("fare").getJSONObject(0).getDouble("exchangeRate"));
//            jsonReceived.put("baseTransactionAmount", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
//            jsonReceived.put("cancelPenalty", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
//            jsonReceived.put("changePenalty", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
//            jsonReceived.put("chargeCode", "");
//            jsonReceived.put("chargeType", "FarePrice");
//            jsonReceived.put("markUP", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
//            jsonReceived.put("paxType", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
//            jsonReceived.put("refundable", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));
//
//            jsonReceived.put("totalFare", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
//            jsonReceived.put("totalTax", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
//            jsonReceived.put("totalFareWithOutMarkUp", segmentObject2.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
//            jsonReceived.put("totalTaxWithOutMarkUp", segmentObject2.getJSONArray("fare").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
//            jsonReceived.put("isCache", segmentObject2.getBoolean("isCache"));
//
//            jsonReceived.put("isHoldBooking", segmentObject2.getBoolean("isHoldBooking"));
//            jsonReceived.put("isInternational", segmentObject2.getBoolean("isInternational"));
//            jsonReceived.put("isRoundTrip", segmentObject2.getBoolean("isRoundTrip"));
//
//            jsonReceived.put("isSpecial", segmentObject2.getBoolean("isSpecial"));
//            jsonReceived.put("isSpecialId", segmentObject2.getBoolean("isSpecialId"));
//            jsonReceived.put("journeyIndex", segmentObject2.getInt("journeyIndex"));
//            jsonReceived.put("nearByAirport", segmentObject2.getBoolean("nearByAirport"));
//            jsonReceived.put("searchId", segmentObject2.getString("searchId"));
//
//            jsonReceived.put("traceId", "AYTM00011111111110002");
//            jsonReceived.put("tripType", "OneWay");
//            jsonReceived.put("transactionAmount", segmentObject2.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));
//
//            jsonReceived.put("childs", childNo);
//            jsonReceived.put("infants", infantNo);
//            jsonReceived.put("adults", adultNo);
//
//
//            jsonReceived.put("beginDate", dateOfReturn);
//            jsonReceived.put("bondType", segmentObject2.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundType"));
//            jsonReceived.put("fareRule", segmentObject2.getString("fareRule"));
//
//            jsonReceived.put("isBaggageFare", segmentObject2.getJSONArray("bonds").getJSONObject(0).getBoolean("isBaggageFare"));
//
//            jsonReceived.put("isSSR", segmentObject2.getJSONArray("bonds").getJSONObject(0).getBoolean("isSSR"));
//
//            jsonReceived.put("itineraryKey", segmentObject2.getString("itineraryKey"));
//            jsonReceived.put("flightName", segmentObject2.getString("engineID"));
//
//            jsonReceived.put("journeyTime", segmentObject2.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
//            jsonReceived.put("amount", flightModelDown.getTotalFare());
//            jsonRequest.put("isConnecting", segmentObject2.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getBoolean("isConnecting"));
//
//            String response = jsonReceived.toString().replace("\"boundTypes\":", "\"boundType\":");
//            jsonReceived = new JSONObject(response);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonReceived = null;
//        }
//
//        if (jsonReceived != null) {


    String url;
//            if (!flightModelDown.getContingFlight().equalsIgnoreCase("{}")) {
    jsonReceived = new JSONObject(flightModelDown.getContingFlight());
    jsonReceived.put("sessionId", session.getUserSessionId());
    url = ApiUrl.URL_FETCH_RECHECK_PRICE;
    Log.i("secondFligthtRequest", jsonReceived.toString()) ;
    valuedown = true;
//            } else {
//                url = ApiUrl.URL_FETCH_ONE_WAY_PRICE;
//                valuedown = false;
//            }
    JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, url, jsonReceived, new Response.Listener<JSONObject>() {
      @Override
      public void onResponse(JSONObject response) {
        try {
          Log.i("Price Resonse second", response.toString());
          String code = response.getString("code");

          if (code != null && code.equals("S00")) {


            JSONObject jsonDetails = response.getJSONObject("details");

            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");
            JSONObject jsonJrnyObj = jsonJourney.getJSONObject(0);
            JSONArray jsonSegmentArray = jsonJrnyObj.getJSONArray("segments");
            JSONObject contingFlight = new JSONObject();
            String flightsecond = jsonSegmentArray.getJSONObject(0).toString();

            for (int k = 0; k < jsonSegmentArray.length(); k++) {


              ArrayList<FlightModel> flightArray = new ArrayList<>();
              JSONObject segmentObj = jsonSegmentArray.getJSONObject(k);
              JSONArray bondArray = segmentObj.getJSONArray("bonds");

              JSONObject bonObj = bondArray.getJSONObject(0);
              JSONArray legArray = bonObj.getJSONArray("legs");
              JSONArray legArrays = bonObj.getJSONArray("legs");
              JSONObject segmentConting = jsonSegmentArray.getJSONObject(k);

              JSONArray bounds = new JSONArray();
              JSONObject bondary = bondArray.getJSONObject(0);
              bondary.remove("specialServices");
              bondary.put("baggageFare", bondary.getString("baggageFare"));
              bondary.put("ssrFare", bondary.getString("ssrFare"));
              bondary.remove("cabin");
              JSONArray jsonArray = new JSONArray();
              JSONArray travelDetails = new JSONArray();
              for (int j = 0; j < legArray.length(); j++) {
                JSONObject connect = legArray.getJSONObject(j);
                connect.remove("cabinClasses");
                connect.remove("journeyTime");
                connect.remove("ssrDetails");
                connect.remove("boundType");
                connect.remove("amount");
                connect.remove("cabin");
                connect.put("cabin", flightClass);
                connect.remove("isConnecting");
                connect.remove("numberOfStops");
                connect.remove("journeyTime");
                jsonArray.put(connect);

              }
              Log.i("travels", jsonArray.toString());
              bondary.put("legs", jsonArray);
              bounds.put(bondary);
              contingFlight.put("bonds", bounds);

              for (int j = 0; j < legArrays.length(); j++) {
                JSONObject d1 = legArrays.getJSONObject(j);
                d1.remove("cabinClasses");
                d1.remove("aircraftCod");
                d1.remove("ssrDetails");
                d1.remove("amount");
                d1.remove("aircraftType");
                d1.remove("availableSeat");
                d1.remove("boundType");
                d1.remove("amount");
//                                d1.remove("group");
//                                    d1.remove("sold");
                d1.remove("arrivalTerminal");
                d1.remove("currencyCode");
                d1.remove("availableSeat");
                d1.remove("isConnecting");
                d1.remove("numberOfStops");
                d1.remove("providerCode");
                d1.remove("remarks");
                d1.remove("aircraftCode");
                d1.remove("ircraftType");
                d1.remove("status");

                d1.remove("carrierCode");
                d1.remove("departureTerminal");
                d1.remove("fareBasisCode");
                d1.remove("fareClassOfService");
                d1.remove("flightDesignator");
                d1.remove("flightDetailRefKey");
                travelDetails.put(d1);
              }

              HashMap<String, Object> pairs = new HashMap<String, Object>();
              for (int i = 0; i < travelDetails.length(); i++) {
                JSONObject j = travelDetails.optJSONObject(i);
                Iterator it = j.keys();
                while (it.hasNext()) {
                  String n = (String) it.next();
                  pairs.put(n, j.getString(n));
                }
              }
              pairs.remove("sold");
              pairs.remove("capacity");

              travelJson.put("Roundway", new JSONArray().put(getJsonFromMap(pairs)));

              String s = segmentObj.getJSONArray("fares").toString();
              s = s.replace("\"fares\":", "\"bookFares\":");
              JSONObject jsonArray1 = new JSONObject(s.substring(1, s.length() - 1));
              contingFlight.put("fares", jsonArray1);
              segmentConting.remove("bondType");

              contingFlight.put("baggageFare", segmentConting.getString("baggageFare"));
              contingFlight.put("cache", segmentConting.getString("cache"));
              contingFlight.put("deeplink", segmentConting.getString("deeplink"));
              contingFlight.remove("destination");
              contingFlight.put("holdBooking", segmentConting.getString("holdBooking"));
              contingFlight.put("international", segmentConting.getString("international"));
              contingFlight.put("roundTrip", segmentConting.getString("roundTrip"));
              contingFlight.put("special", segmentConting.getString("special"));
              contingFlight.put("specialId", segmentConting.getString("specialId"));
              contingFlight.put("engineID", segmentConting.getString("engineID"));
              contingFlight.put("fareRule", segmentConting.getString("fareRule"));
              contingFlight.put("itineraryKey", segmentConting.getString("itineraryKey"));
              contingFlight.put("journeyIndex", segmentConting.getString("journeyIndex"));
              contingFlight.put("nearByAirport", segmentConting.getString("nearByAirport"));
              contingFlight.put("searchId", segmentConting.getString("searchId"));
//                            contingFlight.put("remark", segmentConting.getString("remark"));
              if (valuedown) {
                twoWayTRIP = contingFlight;
              }

              Log.i("contingflightBooking", String.valueOf(contingFlight));

            }

            JSONObject jsonSegment = jsonSegmentArray.getJSONObject(0);
            JSONArray fareArray = jsonSegment.getJSONArray("fares");
            JSONObject fareObj = fareArray.getJSONObject(0);

            double totalPriceDown = fareObj.getDouble("totalFareWithOutMarkUp");
            basesecondfare = fareObj.getDouble("basicFare");
            totalsecondPrice = fareObj.getDouble("totalFareWithOutMarkUp");
            totalsecondTax = fareObj.getDouble("totalTaxWithOutMarkUp");

            double totalPrice = totalPriceDown + upPrice;
            loadingDialog.dismiss();


            showPriceDialog(totalPrice + "", flightUpJson, contingFlight.toString(), String.valueOf(upPrice), String.valueOf(totalPriceDown), flightSegment, flightsecond);

          } else if (code != null && code.equals("F03")) {
            loadingDialog.dismiss();
            showInvalidSessionDialog();
          } else {
            loadingDialog.dismiss();
            if (response.has("message")) {
              String message = response.getString("message");
              CustomToast.showMessage(FlightPriceTwoWayActivity.this, message);
            } else {
              CustomToast.showMessage(FlightPriceTwoWayActivity.this, "Error occurred");
            }

          }
        } catch (JSONException e) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
          e.printStackTrace();
        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        loadingDialog.dismiss();
        CustomToast.showMessage(FlightPriceTwoWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceTwoWayActivity.this));
        error.printStackTrace();
      }
    }) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
        Log.i("Header", map.get("hash"));
        return map;
      }

    };

    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(FlightPriceTwoWayActivity.this, R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(FlightPriceTwoWayActivity.this).sendBroadcast(intent);
  }


  private void showPriceDialog(final String price, final String flightjsonUp, final String flightjsondown, final String totalUp, final String totalDown, final String flightSegment, final String flightsecond) {

    String totalValues;
    Double values = Double.parseDouble(calculateFares1(flightModelUp.getFlightNetFare(), flightModelDown.getFlightNetFare())) + Double.parseDouble("200");
    if ((Double.parseDouble(price) + Double.parseDouble("200")) > values) {
      totalValues = "Sorry! the price for your selected flight has changed from " + values + " to " + price;
    } else if ((Double.parseDouble(price) + Double.parseDouble("200")) < values) {
      totalValues = "The price for your selected flight has changed from " + values + " to " + price;
    } else {
      totalValues = "";
    }
    CustomOnTwoWayFlightPriceDialog builder = new CustomOnTwoWayFlightPriceDialog(FlightPriceTwoWayActivity.this, price, basefirstfare, totalfirstTax, totalfirstPrice, basesecondfare, totalsecondTax, totalsecondPrice, totalValues);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        Intent bookFormIntent = new Intent(getApplicationContext(), FlightBookingFormReturnActivity.class);
        bookFormIntent.putExtra("FlightModelUp", flightModelUp);
        bookFormIntent.putExtra("FlightModelDown", flightModelDown);
        bookFormIntent.putExtra("sourceCode", sourceCode);
        bookFormIntent.putExtra("destinationCode", destinationCode);
        bookFormIntent.putExtra("dateOfJourney", dateOfJourney);
        bookFormIntent.putExtra("dateOfReturn", dateOfReturn);
        bookFormIntent.putExtra("adultNo", adultNo);
        bookFormIntent.putExtra("childNo", childNo);
        bookFormIntent.putExtra("infantNo", infantNo);
        bookFormIntent.putExtra("flightClass", flightClass);
        bookFormIntent.putExtra("flightjsonUp", flightjsonUp);
        bookFormIntent.putExtra("flightdown", flightjsondown);
        String priceNew = String.valueOf(Double.parseDouble(price) + Double.parseDouble("200"));
        bookFormIntent.putExtra("totalAmount", priceNew);
        bookFormIntent.putExtra("totalUp", totalUp);
        bookFormIntent.putExtra("latesttravel", travelJson.toString());
        bookFormIntent.putExtra("totalDown", totalDown);
        bookFormIntent.putExtra("flightSegment", flightSegment);
        bookFormIntent.putExtra("flightsecond", flightsecond);
        if (valuedown && value) {
          bookFormIntent.putExtra("twoWayDetails", "{}");
          bookFormIntent.putExtra("oneWayDetails", "{}");
        } else if (valuedown) {
          bookFormIntent.putExtra("twoWayDetails", twoWayTRIP.toString());
        } else if (value) {
          bookFormIntent.putExtra("oneWayDetails", oneWayTRIP.toString());
        }

        startActivity(bookFormIntent);
      }
    });

    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });

    builder.show();

  }

  private JSONObject getJsonFromMap(Map<String, Object> map) throws JSONException {
    JSONObject jsonData = new JSONObject();
    for (String key : map.keySet()) {
      Object value = map.get(key);
      if (value instanceof Map<?, ?>) {
        value = getJsonFromMap((Map<String, Object>) value);
      }
      jsonData.put(key, value);
    }
    return jsonData;
  }
}
