package in.msewa.ecarib.activity.imagicaActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import in.msewa.adapter.AddonsAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.ResultIPC;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.AddOnsModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by acer on 14-07-2017.
 */

public class AddonsActivity extends AppCompatActivity {
    private static String ticketProduct;
    private static double totalTicketAmount = 0.0, totalpackageAmount = 0.0;
    private static String carAddon, bunchAddon, snowAddon, snowmap;
    private static String busAddon;
    RecyclerView rvCarAddons;
    AddonsAdapter addonsAdapter;
    ArrayList<AddOnsModel.CarAdons> addOnsModels1;
    private View rootView;
    private RecyclerView.Adapter carAdapter;
    private RecyclerView.LayoutManager rlCarAddons;
    private int bunchLength;
    private int busLength;
    private int carLength;
    private TextView tvcarttotalamount;
    private Button btnaddons;
    private UserModel userModel = UserModel.getInstance();
    private JSONObject jsonRequest;
    private LoadingDialog loadDlg;
    private String dept_date;
    private int tot_date;
    private String visit_Date;
    private String destination;
    private String tag_json_obj = "lshdf";
    private String product, productPackage;
    private JSONObject busDetails;
    private JSONObject snowObject;
    private String CarExit = "false";
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("updates");
            if (action.equals("1")) {
                try {
                    String type = intent.getStringExtra("type");
                    totalTicketAmount = Double.parseDouble(intent.getStringExtra("ticketAmount"));
                    carAddon = intent.getStringExtra("carAddon");
                    busAddon = intent.getStringExtra("busAddon");
                    snowAddon = intent.getStringExtra("snowAddon");
                    snowmap = intent.getStringExtra("snowmap");
                    String BusDetails = intent.getStringExtra("busDetails");
                    String snowdetails = intent.getStringExtra("snowdetails");
                    CarExit = intent.getStringExtra("carExit");
                    try {
                        Log.i("valuessa", CarExit);
                    } catch (NullPointerException e) {

                    }
                    if (BusDetails != null && !BusDetails.isEmpty()) {
                        try {
                            busDetails = new JSONObject(BusDetails);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (snowdetails != null && !snowdetails.isEmpty()) {
                        try {
                            snowObject = new JSONObject(snowdetails.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    tvcarttotalamount.setText(String.valueOf(Double.parseDouble(amount) + totalTicketAmount));
                }catch (NullPointerException e){

                }
            }
        }
    };
    private String amount;

    public static Map<String, Integer> jsonString2Map(String jsonString) throws JSONException {
        Map<String, Integer> keys = new HashMap<String, Integer>();

        org.json.JSONObject jsonObject = new org.json.JSONObject(jsonString); // HashMap
        Iterator<?> keyset = jsonObject.keys(); // HM

        while (keyset.hasNext()) {
            String key = (String) keyset.next();
            Object value = jsonObject.get(key);
            keys.put(key, Integer.parseInt(value.toString()));
        }
        return keys;
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.addons_activity);
        int sys = getIntent().getIntExtra("addon", 0);
        loadDlg = new LoadingDialog(AddonsActivity.this);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        amount = getIntent().getStringExtra("totalAmount");
        ticketProduct = getIntent().getStringExtra("ticketProduct");
        dept_date = getIntent().getStringExtra("departureDate");
        tot_date = getIntent().getIntExtra("totaldays", 1);
        visit_Date = getIntent().getStringExtra("visitDate");
        destination = getIntent().getStringExtra("destination");
        Log.i("product", amount + " " + ticketProduct);
        rvCarAddons = (RecyclerView) findViewById(R.id.my_recycler_view);
        rvCarAddons.setHasFixedSize(true);
        tvcarttotalamount = (TextView) findViewById(R.id.tvcarttotalamount);
        tvcarttotalamount.setText(amount);
        rlCarAddons = new LinearLayoutManager(this);
        rvCarAddons.setLayoutManager(rlCarAddons);
        btnaddons = (Button) findViewById(R.id.btnaddons);
        rvCarAddons.setAdapter(carAdapter);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("Packages"));
        ResultIPC resultIPC = ResultIPC.get();
        addOnsModels1 = resultIPC.getaddOnsModels1(sys);

        ArrayList<AddOnsModel> addOnsModels = resultIPC.getaddOnsModels(sys);
        ArrayList<Object> objectArrayList = new ArrayList<>();
        for (int i = 0; i < addOnsModels.size(); i++) {

            bunchLength = addOnsModels.get(i).getBrunAdons().size();
            busLength = addOnsModels.get(i).getBusAdons().size();
            carLength = addOnsModels.get(i).getCarAdons().size();
            for (AddOnsModel.CarAdons carAdons : addOnsModels.get(i).getCarAdons()) {
                objectArrayList.add(carAdons);
            }
            for (AddOnsModel.BusAdons carAdons : addOnsModels.get(i).getBusAdons()) {
                objectArrayList.add(carAdons);

            }
//            for (AddOnsModel.BrunchAdons carAdons : addOnsModels.get(i).getBrunAdons()) {
//                objectArrayList.add(carAdons);
//
//            }
//            for (AddOnsModel.snowMagicaAddOn carAdons : addOnsModels.get(i).getSnowMagicaAddOn()) {
//            objectArrayList.add(addOnsModels.get(i).getSnowMagicaAddOn());
//            }
        }
        if (objectArrayList.size() != 0) {
            addonsAdapter = new AddonsAdapter(AddonsActivity.this, objectArrayList, carLength, busLength, bunchLength);
            rvCarAddons.setAdapter(addonsAdapter);
        }

        btnaddons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    Map<String, Object> stringObjectHashMap = new HashMap<>();

                    if (ticketProduct != null) {
                        stringObjectHashMap.putAll(jsonString2Map(ticketProduct));
                    } else if (carAddon != null) {
                        stringObjectHashMap.putAll(jsonString2Map(carAddon));
                    } else if (snowAddon != null) {
                        stringObjectHashMap.putAll(jsonString2Map(snowAddon));
                    }
                    if (busAddon != null) {
                        stringObjectHashMap.putAll(jsonString2Map(busAddon));
                    }

                    JSONObject jsonObject = new JSONObject(stringObjectHashMap);
                    Log.i("TotalJSON", jsonObject.toString());
                    Intent intent = new Intent(AddonsActivity.this, ImagicaOrderActivity.class);
                    intent.putExtra("productQuantityMap", jsonObject.toString());
                    intent.putExtra("totaldays", 1);
                    intent.putExtra("departureDate", visit_Date);
                    intent.putExtra("destination", destination);
                    intent.putExtra("visitDate", visit_Date);
                    intent.putExtra("carExit", CarExit);
                    try {
                        if (!busDetails.toString().equals("{}")) {
                            intent.putExtra("busOrder", busDetails.toString());
                        }
                    } catch (NullPointerException e) {
                        intent.putExtra("busOrder", "null");
                    }
                    try {
                        if (!snowObject.toString().equals("{}")) {
                            intent.putExtra("snowMagicaOrder", snowObject.toString());
                        }
                    } catch (NullPointerException e) {
                        intent.putExtra("snowMagicaOrder", "null");
                    }
                    startActivity(intent);
//                    getApiCallPlaceLoader(jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getApiCallPlaceLoader(JSONObject jsonObject) {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", userModel.getUserSessionId());
            jsonRequest.put("visitDate", visit_Date);
            jsonRequest.put("departureDate", dept_date);
            jsonRequest.put("totaldays", tot_date);
            jsonRequest.put("destination", destination);
            jsonRequest.put("productQuantityMap", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("THEMEPARKTICKETLIST", ApiUrl.URL_PLACEORDER);
            Log.i("THEMETICKETREQ", jsonRequest.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PLACEORDER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("ORDER_PLACE", response.toString());

                        boolean success = response.getBoolean("success");
                        String errorMsgs = response.getString("errorMsgs");
                        String Msgs = response.getString("message");
                        loadDlg.dismiss();
                        if (success) {
                            String message = response.getString("message");
                            String uid = response.getString("uid");
                            String orderId = response.getString("orderId");
                            String transactionId = response.getString("transactionId");
                            double ammount = response.getDouble("amount");

                            Intent intent = new Intent(AddonsActivity.this, ImagicaOrderActivity.class);
                            intent.putExtra("uid", uid);
                            intent.putExtra("orderId", orderId);
                            intent.putExtra("transactionId", transactionId);
                            intent.putExtra("ammount", ammount);


                            startActivity(intent);

                        } else {

                            if (!errorMsgs.equals("null")) {
                                CustomToast.showMessage(AddonsActivity.this, errorMsgs.replace("[", "").replace("]", ""));
                            } else {
                                CustomToast.showMessage(AddonsActivity.this, Msgs.replace("[", "").replace("]", ""));
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(AddonsActivity.this, NetworkErrorHandler.getMessage(error, AddonsActivity.this));

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("hash", "1234");
                    return hashMap;
                }
            };
            int socketTimeout = 60000;
//
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);

        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
