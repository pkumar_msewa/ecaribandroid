package in.msewa.ecarib.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.CircleModel;
import in.msewa.model.OperatorsModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.SecurityUtil;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 3/11/2016.
 */
public class OtpDeviceVerificationActivity extends AppCompatActivity {

  private EditText etVerifyOTP;
  private Button btnVerify, btnVerifyResend;
  private TextInputLayout ilVerifyOtp;


  private LoadingDialog loadDlg;
  private String userMobileNo;
  private String otpCode = null;
  private String password = "";
  private int count = 0;
  private String regId = "";

  private JSONObject jsonRequest;
  //Volley Tag
  private String tag_json_obj = "json_user";
  private UserModel session = UserModel.getInstance();


  SharedPreferences phonePreferences;
  public static final String PHONE = "phone";
  private String paymentlimit;
  private boolean valid = true;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_verify_device);
    loadDlg = new LoadingDialog(OtpDeviceVerificationActivity.this);
    phonePreferences = getSharedPreferences(PHONE, Context.MODE_PRIVATE);

    userMobileNo = getIntent().getStringExtra("userMobileNo");
    otpCode = getIntent().getStringExtra("OtpCode");
    password = getIntent().getStringExtra("devPassword");
    regId = getIntent().getStringExtra("devRegId");
    paymentlimit = getIntent().getStringExtra("type");

    btnVerifyResend = (Button) findViewById(R.id.btnVerifyResend);
    btnVerify = (Button) findViewById(R.id.btnVerify);
    etVerifyOTP = (EditText) findViewById(R.id.etVerifyOTP);
    ilVerifyOtp = (TextInputLayout) findViewById(R.id.ilVerifyOtp);


    etVerifyOTP.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
        if (etVerifyOTP.getText().length() == 6) {
//                    etVerifyOTP.setText(otpCode);
          if (!TextUtils.isEmpty(paymentlimit)) {
            try {
              JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("jsonRequest"));
              jsonObject.remove("mobileToken");
              jsonObject.put("mobileToken", etVerifyOTP.getText().toString());
              saveTraniscationLimit(jsonObject);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          } else {
            attemptLoginNew();
          }
        }
      }
    });
    if (otpCode != null && !otpCode.isEmpty()) {
      etVerifyOTP.setText(otpCode);
    }

    btnVerifyResend.setTextColor(Color.parseColor("#ffffff"));
    btnVerify.setTextColor(Color.parseColor("#ffffff"));

    btnVerifyResend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        count++;
        if (count != 3) {
          if (!TextUtils.isEmpty(paymentlimit)) {
            try {
              valid = false;
              saveTraniscationLimit(new JSONObject(getIntent().getStringExtra("jsonRequest")));
            } catch (JSONException e) {
              e.printStackTrace();
            }
          } else {
            loadDlg.show();
            resendOTP();
          }
        } else {
          btnVerifyResend.setClickable(false);
          btnVerifyResend.setBackgroundResource(R.drawable.bg_button_gray_pressed);
        }
      }
    });


    btnVerify.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
//                loadDlg.show();
        submitForm();
      }
    });

  }


  private void submitForm() {
    if (!validateOTP()) {
      return;
    }
    if (!TextUtils.isEmpty(paymentlimit)) {
      try {
        JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("jsonRequest"));
        jsonObject.remove("mobileToken");
        jsonObject.put("mobileToken", etVerifyOTP.getText().toString());
        saveTraniscationLimit(jsonObject);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    } else {
      attemptLoginNew();
    }

  }


  private boolean validateOTP() {
    if (etVerifyOTP.getText().toString().trim().isEmpty()) {
      ilVerifyOtp.setError("Please Enter OTP");
      requestFocus(etVerifyOTP);
      return false;
    } else if (etVerifyOTP.getText().toString().trim().length() < 3) {
      ilVerifyOtp.setError("Enter correct OTP");
      requestFocus(etVerifyOTP);
    } else {
      ilVerifyOtp.setErrorEnabled(false);
    }

    return true;
  }

  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }

  private void resendOTP() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", userMobileNo);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("OTPURL", ApiUrl.URL_RESEND_OTP_DEVICE);
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RESEND_OTP_DEVICE, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            Log.i("RESENDOTP", jsonObj.toString());
            String code = jsonObj.getString("code");
            String detail = jsonObj.getString("message");
            if (code != null & code.equals("S00")) {
              if (!etVerifyOTP.getText().toString().isEmpty()) {
                etVerifyOTP.setText("");
              }
              loadDlg.dismiss();
              Toast.makeText(OtpDeviceVerificationActivity.this, detail, Toast.LENGTH_SHORT).show();
            } else {
              loadDlg.dismiss();


            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(OtpDeviceVerificationActivity.this, getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(OtpDeviceVerificationActivity.this, getResources().getString(R.string.server_exception));

          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void attemptLoginNew() {
    loadDlg.show();
    jsonRequest = new JSONObject();

    try {
      jsonRequest.put("username", userMobileNo);
      jsonRequest.put("password", password);
      jsonRequest.put("validate", true);
      jsonRequest.put("mobileToken", etVerifyOTP.getText().toString());
      if (!regId.isEmpty()) {
        jsonRequest.put("registrationId", regId);
      }
      if (SecurityUtil.getIMEI(OtpDeviceVerificationActivity.this) != null) {
        jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(OtpDeviceVerificationActivity.this) + "-" + SecurityUtil.getIMEI(OtpDeviceVerificationActivity.this));
      } else {
        jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(OtpDeviceVerificationActivity.this));
      }

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("URL LOGIN", ApiUrl.URL_LOGIN);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGIN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("LOgin Resonse", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);

              //New Implementation
              JSONObject jsonDetail = jsonObject.getJSONObject("details");
              String userSessionId = jsonDetail.getString("sessionId");
              String nikiOffer = "nooffer";
              if (jsonDetail.has("nikiOffer")) {
                nikiOffer = jsonDetail.getString("nikiOffer");
              }
              JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");

              double userBalanceD = accDetail.getDouble("balance");
              String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

              long userAcNoL = accDetail.getLong("accountNumber");
              String userAcNo = String.valueOf(userAcNoL);

              int userPointsI = accDetail.getInt("points");
              String userPoints = String.valueOf(userPointsI);

              JSONObject accType = accDetail.getJSONObject("accountType");
              String accName = accType.getString("name");
              String accCode = accType.getString("code");

              double accBalanceLimitD = accType.getDouble("balanceLimit");
              double accMonthlyLimitD = accType.getDouble("monthlyLimit");
              double accDailyLimitD = accType.getDouble("dailyLimit");

              String accBalanceLimit = String.valueOf(accBalanceLimitD);
              String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
              String accDailyLimit = String.valueOf(accDailyLimitD);

              JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");

              String userFName = jsonUserDetail.getString("firstName");
              String userLName = jsonUserDetail.getString("lastName");
              String userMobile = jsonUserDetail.getString("contactNo");
              String userEmail = jsonUserDetail.getString("email");
              String userEmailStatus = jsonUserDetail.getString("emailStatus");

              String userAddress = jsonUserDetail.getString("address");
              String userImage = jsonUserDetail.getString("image");
              String encodedImage = jsonUserDetail.getString("encodedImage");
              String images = "";
              if (!encodedImage.equals("")) {
                images = encodedImage;
              } else {
                images = userImage;
              }


              boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");
              String userDob = jsonUserDetail.getString("dateOfBirth");

              boolean hasRefer;
              if (response.has("hasRefer")) {
                hasRefer = response.getBoolean("hasRefer");
              } else {
                hasRefer = true;
              }
              String userGender = "";
              if (!jsonUserDetail.isNull("gender")) {
                userGender = jsonUserDetail.getString("gender");
              }
              boolean iplEnable = false, ebsEnable = false, vnetEnable = false, razorPayEnable = false;
              String mdexToken = "", mdexKey = "", iplPrediction = "", iplSchedule = "", iplMyPrediction = "";
              if (response.has("iplEnable")) {
                iplEnable = response.getBoolean("iplEnable");
                mdexToken = response.getString("mdexToken");
                mdexKey = response.getString("mdexKey");
                iplPrediction = response.getString("iplPrediction");
                iplSchedule = response.getString("iplSchedule");
                iplMyPrediction = response.getString("iplMyPrediction");

              }
              if (response.has("ebsEnable") || response.has("vnetEnable") || response.has("razorPayEnable")) {
                ebsEnable = response.getBoolean("ebsEnable");
                vnetEnable = response.getBoolean("vnetEnable");
                razorPayEnable = response.getBoolean("razorPayEnable");
              }
              UserModel.deleteAll(UserModel.class);
              try {
                //Encrypting sessionId
                UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, images, userDob, userGender, isMPin, userPoints, hasRefer, nikiOffer, iplEnable, iplMyPrediction, iplPrediction, iplSchedule, mdexKey, mdexToken, ebsEnable, vnetEnable, razorPayEnable);
                userModel.save();
              } catch (Exception e) {
                e.printStackTrace();
              }


              session.setUserSessionId(userSessionId);
              session.setUserFirstName(userFName);
              session.setUserLastName(userLName);
              session.setUserMobileNo(userMobile);
              session.setUserEmail(userEmail);
              session.setEmailIsActive(userEmailStatus);
              session.setUserAcCode(accCode);
              session.setUserAcNo(userAcNo);
              session.setUserBalance(userBalance);
              session.setUserAcName(accName);
              session.setUserMonthlyLimit(accMonthlyLimit);
              session.setUserDailyLimit(accDailyLimit);
              session.setUserBalanceLimit(accBalanceLimit);
              session.setUserImage(images);
              session.setUserAddress(userAddress);
              session.setUserGender(userGender);
              session.setUserDob(userDob);
              session.setMPin(isMPin);
              session.setHasRefer(hasRefer);
              session.setIsValid(1);
              session.setNikiOffer(nikiOffer);
              session.setUserPoints(userPoints);
              session.setIplEnable(iplEnable);
              session.setIplMyPrediction(iplMyPrediction);
              session.setIplPrediction(iplPrediction);
              session.setMdexKey(mdexKey);
              session.setVnetEnable(vnetEnable);
              session.setEbsEnable(ebsEnable);
              session.setRazorPayEnable(razorPayEnable);
              session.setIplSchedule(iplSchedule);
              session.setMdexToken(mdexToken);
              OperatorsModel.deleteAll(OperatorsModel.class);
              CircleModel.deleteAll(CircleModel.class);
              loadDlg.dismiss();

              SharedPreferences.Editor editor = phonePreferences.edit();
              editor.clear();
              editor.putString("phone", userMobile);
              editor.apply();

              loadDlg.dismiss();
              startActivity(new Intent(OtpDeviceVerificationActivity.this, HomeMainActivity.class));
              finish();
            } else {
              String message = response.getString("message");
              loadDlg.dismiss();
              CustomToast.showMessage(OtpDeviceVerificationActivity.this, message);

            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(OtpDeviceVerificationActivity.this, getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(OtpDeviceVerificationActivity.this, NetworkErrorHandler.getMessage(error, OtpDeviceVerificationActivity.this));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };

      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void saveTraniscationLimit(JSONObject jsonObject) {
    final LoadingDialog loadingDialog = new LoadingDialog(OtpDeviceVerificationActivity.this);
    loadingDialog.show();

    AndroidNetworking.post(ApiUrl.URL_UPDATE_TRANSATION_LIMIT)
      .setPriority(Priority.HIGH)
      .setTag("test")
      .addJSONObjectBody(jsonObject)
      .build().getAsJSONObject(new JSONObjectRequestListener() {
      @Override
      public void onResponse(JSONObject response) throws JSONException {
        loadingDialog.dismiss();
        if (response.getString("code").equalsIgnoreCase("S00")) {
//            btnSubmit.setVisibility(View.GONE);
          if (valid) {
            CustomAlertDialog customAlertDialog = new CustomAlertDialog(OtpDeviceVerificationActivity.this, R.string.dialog_title, response.getString("message"));
            customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
//                startActivity(new Intent(OtpDeviceVerificationActivity.this, HomeMainActivity.class));
              }
            });
            customAlertDialog.show();
          } else {
            valid = true;
            CustomToast.showMessage(OtpDeviceVerificationActivity.this, response.getString("message"));
          }
//            getDisEnableView(etAmountOneDay, tvOneDayAmount, etAmountOneDay.getText().toString());
//            getDisEnableView(etAmountOneMonth, tvMonthAmount, etAmountOneMonth.getText().toString());
//            getDisEnableView(etTranscationOneMonth, tvMonthTranscation, etTranscationOneMonth.getText().toString());
//            getDisEnableView(etTranscationOneDay, tvOneDayTranscation, etTranscationOneDay.getText().toString());
        } else if (response.getString("code").equalsIgnoreCase("F03")) {
          loadingDialog.dismiss();
          showInvalidSessionDialog();
        } else {
          CustomToast.showMessage(OtpDeviceVerificationActivity.this, response.getString("message"));
        }
      }

      @Override
      public void onError(ANError anError) {
        loadingDialog.dismiss();
        CustomToast.showMessage(OtpDeviceVerificationActivity.this, getResources().getString(R.string.server_exception));

      }
    });


  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(OtpDeviceVerificationActivity.this, R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    finish();
    LocalBroadcastManager.getInstance(OtpDeviceVerificationActivity.this).sendBroadcast(intent);
  }


}
