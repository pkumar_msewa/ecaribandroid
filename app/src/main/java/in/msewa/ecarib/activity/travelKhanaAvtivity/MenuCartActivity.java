
package in.msewa.ecarib.activity.travelKhanaAvtivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.MenuCartAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.PQCart;
import in.msewa.model.TravelKhanaMenuModel;
import in.msewa.model.TravelKhanaStationListModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveMenuListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 4/6/2016.
 */
public class MenuCartActivity extends AppCompatActivity implements AddRemoveMenuListner {
  private PQCart cart = PQCart.getInstance();
  private ListView lvInCartItem;
  private Button btnBuyNext;
  private TextView tvTotalRupees, tvInfo;
  private double totalCost = 0.0;
  private ArrayList<TravelKhanaMenuModel> cartMenuArray;
  private Toolbar toolbar;
  private ImageButton ivBackBtn;

  //Volley
  private String tag_json_obj = "json_travel";
  private LinearLayout llNoBus;
  private ArrayList<TravelKhanaStationListModel> stationList;
  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;
  private String station, date, trainNumber;
  private ArrayList<TravelKhanaMenuModel> menuItemArray;
  private LoadingDialog loadingDialog;
  private String outletId;
  private JSONArray items;
  private String oDate, oETA;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_menu_cart);
    tvTotalRupees = (TextView) findViewById(R.id.tvBuyTotalRupess);
    tvInfo = (TextView) findViewById(R.id.tvInfo);
    lvInCartItem = (ListView) findViewById(R.id.hlvInCart);
    btnBuyNext = (Button) findViewById(R.id.btnBuyNext);
    cartMenuArray = getIntent().getParcelableArrayListExtra("SELECTEDMENUARRAY");
    tvInfo.setText(getIntent().getStringExtra("INFO"));
    outletId = getIntent().getStringExtra("OUTLETID");
    trainNumber = getIntent().getStringExtra("TRAINNO");
    oDate = getIntent().getStringExtra("DATE");
    oETA = getIntent().getStringExtra("ETA");
    station = getIntent().getStringExtra("STATION");
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    loadingDialog = new LoadingDialog(MenuCartActivity.this);
    items = getMenuArray();
    getMenuPriceCalculation(items);
    lvInCartItem.setAdapter(new MenuCartAdapter(MenuCartActivity.this, cartMenuArray, this, outletId));

    LocalBroadcastManager.getInstance(this).registerReceiver(cMessageReceiver, new IntentFilter("food-book-done"));

    btnBuyNext.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        Intent orderFoodIntent = new Intent(MenuCartActivity.this, OrderFoodActivity.class);
        orderFoodIntent.putExtra("ITEMARRAY", String.valueOf(items));
        orderFoodIntent.putExtra("OUTLETID", outletId);
        orderFoodIntent.putExtra("AMOUNT", tvTotalRupees.getText().toString());
        orderFoodIntent.putExtra("TRAINNO", trainNumber);
        orderFoodIntent.putExtra("DATE", oDate);
        orderFoodIntent.putExtra("ARRTIME", oETA);
        orderFoodIntent.putExtra("STATION", station);
        startActivity(orderFoodIntent);
      }
    });
  }

  public JSONArray getMenuArray() {
    JSONArray menuArray = new JSONArray();
    for (int i = 0; i < cartMenuArray.size(); i++) {
      JSONObject object = new JSONObject();
      try {
        object.put("quantity", (long) cartMenuArray.get(i).getQuantity());
        object.put("itemId", String.valueOf(cartMenuArray.get(i).getItemId()));
        menuArray.put(object);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
    return menuArray;
  }

  public void getMenuPriceCalculation(JSONArray items) {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("order_outlet_id", outletId);
      jsonRequest.put("orderMenu", items);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("PRICECALREQ", jsonRequest.toString());
      Log.i("PRICECALURL", ApiUrl.URL_TRAVELKHANA_MENU_PRICE_CAL);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_MENU_PRICE_CAL, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("PRICECALRES", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {
              JSONObject menuPriceCalDTO = response.getJSONObject("menuPriceCalDTO");
              JSONObject userOrderInfo = menuPriceCalDTO.getJSONObject("userOrderInfo");
              double totalCustomerPayable = userOrderInfo.getDouble("totalCustomerPayable");
              tvTotalRupees.setText(String.valueOf(totalCustomerPayable));
              loadingDialog.dismiss();
            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog();
            } else {
              loadingDialog.dismiss();
              CustomToast.showMessage(MenuCartActivity.this, message);

            }
          } catch (JSONException e) {
            e.printStackTrace();
            CustomToast.showMessage(getApplicationContext(), "Oops, something went wrong. Please after sometime");
            finish();
            loadingDialog.dismiss();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };

      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(MenuCartActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(MenuCartActivity.this).sendBroadcast(intent);
  }

  @Override
  public void taskCompleted(ArrayList<TravelKhanaMenuModel> menuArray) {

  }

  @Override
  public void AddRemoveComplete(double totapPrice, JSONArray itemsarray) {
    tvTotalRupees.setText(String.valueOf(totapPrice));
    items = itemsarray;
  }

  private BroadcastReceiver cMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      finish();
    }
  };

  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(cMessageReceiver);
    super.onDestroy();
  }

}
