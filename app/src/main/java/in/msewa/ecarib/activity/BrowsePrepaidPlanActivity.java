package in.msewa.ecarib.activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.fragment.fragmentbrowseplanPrepaid.FullTalkTimeFragment;
import in.msewa.fragment.fragmentbrowseplanPrepaid.SpecialPlanFragment;
import in.msewa.fragment.fragmentbrowseplanPrepaid.ThreeGFragment;
import in.msewa.fragment.fragmentbrowseplanPrepaid.TopUpFragment;
import in.msewa.fragment.fragmentbrowseplanPrepaid.TwoGFragment;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.MobilePlansModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.MobilePlansCheck;

/**
 * Created by Ksf on 3/16/2016.
 */
public class BrowsePrepaidPlanActivity extends AppCompatActivity {

    /*
     * Sliding tabs Setup
	 */

    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private Toolbar tbMobileTopUp;
    private FragmentManager fragmentManager;
    CharSequence TitlesEnglish[] = {"Full talktime", "Special", "2G", "3G", "Top up"};
    int NumbOfTabs = 5;

    private String operatorCode;
    private String circleCode;

    private ImageButton ivBackBtn;
    private Toolbar toolbar;

    private JSONObject jsonRequest;
    private LoadingDialog loadingDialog;

    //Volley Tag
    private String tag_json_obj = "json_browse_plan";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(BrowsePrepaidPlanActivity.this);
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        circleCode = getIntent().getStringExtra("circleCode");
        operatorCode = getIntent().getStringExtra("operatorCode");
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);

        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        browsePlan();


    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                FullTalkTimeFragment tab1 = new FullTalkTimeFragment();
                return tab1;
            } else if (position == 1) {
                SpecialPlanFragment tab2 = new SpecialPlanFragment();
                return tab2;
            } else if (position == 2) {
                TwoGFragment tab3 = new TwoGFragment();
                return tab3;
            } else if (position == 3) {
                ThreeGFragment tab3 = new ThreeGFragment();
                return tab3;
            } else {
                TopUpFragment tab3 = new TopUpFragment();
                return tab3;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }


        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }


    private void browsePlan() {
        jsonRequest = new JSONObject();
        loadingDialog.show();
        try {
            jsonRequest.put("circleCode", circleCode);
            jsonRequest.put("operatorCode", operatorCode);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("BrowseRequest", jsonRequest.toString());
            Log.i("BrowseURL", ApiUrl.URL_BROWSE_PLAN);
            AndroidNetworking.post(ApiUrl.URL_BROWSE_PLAN)
                    .addJSONObjectBody(jsonRequest) // posting json
                    .setTag("test")
                    .setPriority(Priority.HIGH)
                    .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
                    .build().setAnalyticsListener(new AnalyticsListener() {
                @Override
                public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                    Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
                }
            }).getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    Log.i("API RESPOMSE", jsonObj.toString());
                    ArrayList<MobilePlansModel> talkTimePlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> topUpPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> threeGPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> twoGPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> specialPlans = new ArrayList<>();


                    try {
                        String responseString = jsonObj.getString("response");
                        JSONObject response = new JSONObject(responseString);

                        JSONArray planArray = response.getJSONArray("plans");
                        for (int i = 0; i < planArray.length(); i++) {
                            JSONObject c = planArray.getJSONObject(i);
                            String plan_name = c.getString("planName").trim();
                            String plan_desc = c.getString("description");
                            String plan_amount = c.getString("amount");
                            String plan_validity = c.getString("validity");
                            String plan_operator_code = c.getString("smsDaakCode");

                            if (plan_name.equals("Full Talktime")) {
                                talkTimePlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("Top up")) {
                                topUpPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("3G Data") || plan_name.equals("3G Plans")) {
                                threeGPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("2G Data") || plan_name.equals("Data Plans")) {
                                twoGPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else {
                                specialPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            }

                        }


                        MobilePlansCheck plans = MobilePlansCheck.getInstance();
                        plans.setTalkTimePlans(talkTimePlans);
                        plans.setTopUpPlans(topUpPlans);
                        plans.setThreeGPlans(threeGPlans);
                        plans.setTwoGPlans(twoGPlans);
                        plans.setSpecialPlans(specialPlans);

                        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
                        mSlidingTabLayout.setupWithViewPager(mainPager);
                        loadingDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    loadingDialog.dismiss();
                    finish();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
                }
            });
        }
    }


    public static final int getColors(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }


}
