package in.msewa.ecarib.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.ebs.android.sdk.PaymentActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


public class LoadMoneySucessActivity extends AppCompatActivity {
  String payment_id;
  String PaymentId;
  String AccountId;
  String MerchantRefNo;
  String Amount;
  String DateCreated;
  String Description;
  String Mode;
  String IsFlagged;
  String BillingName;
  String BillingAddress;
  String BillingCity;
  String BillingState;
  String BillingPostalCode;
  String BillingCountry;
  String BillingPhone;
  String BillingEmail;
  String DeliveryName;
  String DeliveryAddress;
  String DeliveryCity;
  String DeliveryState;
  String DeliveryPostalCode;
  String DeliveryCountry;
  String DeliveryPhone;
  String PaymentStatus;
  String PaymentMode;
  String SecureHash;

  //Views and Dialog
  private LinearLayout llPaymentFailure;
  private Button btnPaymentSuccess, btnPaymentCancel, btnPaymentRetry;
  private TextView tvPaymentStatus;
  private LoadingDialog loadingDialog;
  private String type;
  SharedPreferences loadMoneyPref;

  private UserModel session = UserModel.getInstance();
  private String tag_json_obj = "load_money";
  private String jsonBooking;
  private boolean roundTrip;
  private boolean contingFlight;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_load_money_success);
    loadingDialog = new LoadingDialog(LoadMoneySucessActivity.this);

    llPaymentFailure = (LinearLayout) findViewById(R.id.llPaymentFailure);
    btnPaymentSuccess = (Button) findViewById(R.id.btnPaymentSuccess);
    btnPaymentCancel = (Button) findViewById(R.id.btnPaymentCancel);
    btnPaymentRetry = (Button) findViewById(R.id.btnPaymentRetry);
    tvPaymentStatus = (TextView) findViewById(R.id.tvPaymentStatus);

    loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
    type = loadMoneyPref.getString("type", "");
    jsonBooking = loadMoneyPref.getString("jsonBooking", "");
    roundTrip = loadMoneyPref.getBoolean("roundTrip", false);
    contingFlight = loadMoneyPref.getBoolean("conting", false);
    Intent intent = getIntent();
    payment_id = intent.getStringExtra("payment_id");
    getJsonReport();

    btnPaymentSuccess.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        sendRefresh();
        loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferences = loadMoneyPref.edit();
        sharedPreferences.clear();
        sharedPreferences.apply();
        sendFinish();
      }
    });

    btnPaymentRetry.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
        LoadMoneySucessActivity.this.finish();
        startActivity(i);
      }
    });
    btnPaymentCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
//                LoadMoneySucessActivity.this.finish();
        loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferences = loadMoneyPref.edit();
        sharedPreferences.clear();
        sharedPreferences.apply();
        sendRefresh();
      }
    });
  }

  private void getJsonReport() {
    String response = payment_id;
    JSONObject jObject;
    try {
      jObject = new JSONObject(response.toString());
      Log.i("JSONVALUE", jObject.toString());
      PaymentId = jObject.getString("PaymentId");
      AccountId = jObject.getString("AccountId");
      MerchantRefNo = jObject.getString("MerchantRefNo");
      Amount = jObject.getString("Amount");
      DateCreated = jObject.getString("DateCreated");
      Description = jObject.getString("Description");
      Mode = jObject.getString("Mode");
      IsFlagged = jObject.getString("IsFlagged");
      BillingName = jObject.getString("BillingName");
      BillingAddress = jObject.getString("BillingAddress");
      BillingCity = jObject.getString("BillingCity");
      BillingState = jObject.getString("BillingState");
      BillingPostalCode = jObject.getString("BillingPostalCode");
      BillingCountry = jObject.getString("BillingCountry");
      BillingPhone = jObject.getString("BillingPhone");
      BillingEmail = jObject.getString("BillingEmail");
      DeliveryName = jObject.getString("DeliveryName");
      DeliveryAddress = jObject.getString("DeliveryAddress");
      DeliveryCity = jObject.getString("DeliveryCity");
      DeliveryState = jObject.getString("DeliveryState");
      DeliveryPostalCode = jObject.getString("DeliveryPostalCode");
      DeliveryCountry = jObject.getString("DeliveryCountry");
      DeliveryPhone = jObject.getString("DeliveryPhone");
      PaymentStatus = jObject.getString("PaymentStatus");
      PaymentMode = jObject.getString("PaymentMode");
      SecureHash = jObject.getString("SecureHash");


      if (PaymentStatus.equalsIgnoreCase("failed")) {
        tvPaymentStatus.setText("");
        btnPaymentSuccess.setVisibility(View.GONE);
        llPaymentFailure.setVisibility(View.GONE);
//                addingResultToView(false);
        callVerifyPayment("0");


      } else {
        tvPaymentStatus.setText("");
        btnPaymentSuccess.setVisibility(View.GONE);
        llPaymentFailure.setVisibility(View.GONE);
        //VerifyTransactionIsSuccess
        callVerifyPayment("1");
      }

    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  private void addingResultToView(boolean isSuccess) {
    TableLayout table_payment = (TableLayout) findViewById(R.id.tlResponse);
    ArrayList<String> arrlist = new ArrayList<>();
    arrlist.add("Payment Id");
    arrlist.add("MerchantRef No");
    arrlist.add("Amount");
    arrlist.add("Date Created");
    arrlist.add("Is Flagged");
    arrlist.add("Name");
    arrlist.add("Billing Country");
    arrlist.add("Phone");
    arrlist.add("Email");
    arrlist.add("Payment Status");
    arrlist.add("Payment Mode");

    ArrayList<String> arrlist1 = new ArrayList<>();
    arrlist1.add(PaymentId);
    arrlist1.add(MerchantRefNo);
    arrlist1.add(Amount);
    arrlist1.add(DateCreated);
    arrlist1.add(IsFlagged);
    arrlist1.add(BillingName);
    arrlist1.add(BillingCountry);
    arrlist1.add(BillingPhone);
    arrlist1.add(BillingEmail);
    arrlist1.add(PaymentStatus);
    arrlist1.add(PaymentMode);

    for (int i = 0; i < arrlist.size(); i++) {
      TableRow row = new TableRow(this);

      TextView textH = new TextView(this);
      TextView textC = new TextView(this);
      TextView textV = new TextView(this);

      textH.setText(arrlist.get(i));
      textC.setText(":  ");
      textV.setText(arrlist1.get(i));
      textV.setTypeface(null, Typeface.BOLD);

      row.addView(textH);
      row.addView(textC);
      row.addView(textV);

      table_payment.addView(row);
    }

    if (isSuccess) {

      tvPaymentStatus.setText("Load Money Successful.");
      btnPaymentSuccess.setVisibility(View.VISIBLE);
      llPaymentFailure.setVisibility(View.GONE);
    } else {
      tvPaymentStatus.setText("Load Money Failed.");
      btnPaymentSuccess.setVisibility(View.GONE);
      llPaymentFailure.setVisibility(View.VISIBLE);
    }
  }


  public void callVerifyPayment(final String respCode) {
    Log.i("Response Code", respCode);

    loadingDialog.show();
    String URL;
    if (type.equalsIgnoreCase("flight")) {
      URL = ApiUrl.URL_VERIFY_TRANSACTION_FLIGHT_EBS_KIT;
    } else {
      URL = ApiUrl.URL_VERIFY_TRANSACTION_EBS_KIT;
    }
    StringRequest postReq = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
      @Override
      public void onResponse(String stringResponse) {
        Log.i("Bill PayResponse", stringResponse.toString());
        try {
          Log.i("url Verify", ApiUrl.URL_VERIFY_TRANSACTION_EBS_KIT);
          Log.i("JsonResponse Verify", stringResponse.toString());

          JSONObject response = new JSONObject(stringResponse);

          boolean isSuccess = response.getBoolean("success");
          if (isSuccess) {
            if (type.equals("normal")) {
              loadingDialog.dismiss();
              addingResultToView(isSuccess);

            } else if (type.equalsIgnoreCase("flight")) {
              JSONObject jsonRequest = new JSONObject(jsonBooking);
              Log.i("value", jsonRequest.toString());
              loadingDialog.show();
              String Urls = "";
//                            if (roundTrip) {
//                                if (contingFlight) {
//                                    Urls = ApiUrl.URL_FLIGHT_CHECKOUT_ROUNDCONTING;
////                                    jsonRequest.put("flightType","RoundwayConnecting");
//                                } else {
//                                    Urls = ApiUrl.URL_FLIGHT_CHECKOUT_ROUND;
//                                    jsonRequest.put("flightType","DirectRounway");
//                                }
//
//                            } else {
//                                if (contingFlight) {
//                                    Urls = ApiUrl.URL_FLIGHT_CHECKOUT_CONTING;
//                                    jsonRequest.put("flightType","DirectConnecting");
//                                } else {
//                                    Urls = ApiUrl.URL_FLIGHT_CHECKOUT;
//                                    jsonRequest.put("flightType","Direct");
//                                }
//
//                            }
              jsonRequest.put("transactionId", MerchantRefNo);
              jsonRequest.put("paymentMethod", "EBS");
              longLog(jsonRequest.toString());
              JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_BOOKING, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                  loadingDialog.dismiss();
                  try {
                    Log.i("CheckOut Resonse", response.toString());
                    String code = response.getString("code");
                    loadingDialog.dismiss();
                    if (code != null && code.equals("S00")) {
//                                            sendRefresh();
                      showSuccessDialog();

                    } else if (code != null && code.equals("F03")) {
                      showInvalidSessionDialog();
                    } else {
                      String message = response.getString("message");
                      showCustomDialog(message);

                    }
                  } catch (JSONException e) {
                    loadingDialog.dismiss();
                    showCustomDialog(getResources().getString(R.string.server_exception2));
                    e.printStackTrace();
                  }
                }
              }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  loadingDialog.dismiss();
                  showCustomDialog(NetworkErrorHandler.getMessage(error, LoadMoneySucessActivity.this));
                  error.printStackTrace();
                }
              }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                  HashMap<String, String> map = new HashMap<>();
                  return map;
                }

              };
//
              int socketTimeout = 60000000;
              RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
              postReq.setRetryPolicy(policy);
              PayQwikApplication.getInstance().addToRequestQueue(postReq, "loadmoney");
            } else {
              loadingDialog.dismiss();
              sendFinish();
              finish();
            }
          } else {
            if (type.equals("normal")) {
              addingResultToView(isSuccess);
              loadingDialog.dismiss();
            } else if (type.equalsIgnoreCase("flight")) {
              showCustomDialog("Flight Payment Failed");
            } else {
              loadingDialog.dismiss();
              sendFinish();
              finish();
            }
          }
        } catch (JSONException e) {
          loadingDialog.dismiss();
          e.printStackTrace();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        loadingDialog.dismiss();
        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

      }
    }) {
      @Override
      protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("responseCode", respCode);
        params.put("responseMessage", "Ok");
        params.put("dateCreated", DateCreated);
        params.put("paymentId", PaymentId);
        params.put("merchantRefNo", MerchantRefNo);
        params.put("mode", Mode);
        params.put("billingName", BillingName);
        params.put("billingState", BillingState);
        params.put("billingPostalCode", BillingPostalCode);
        params.put("billingCountry", BillingCountry);
        params.put("billingPhone", BillingPhone);
        params.put("billingEmail", BillingEmail);
        params.put("deliveryName", DeliveryName);
        params.put("deliveryAddress", DeliveryAddress);

        params.put("deliveryCity", DeliveryCity);
        params.put("deliveryState", DeliveryState);
        params.put("deliveryPostalCode", DeliveryPostalCode);
        params.put("deliveryCountry", DeliveryCountry);
        params.put("deliveryPhone", DeliveryPhone);
        params.put("isFlagged", IsFlagged);
        params.put("secureHash", SecureHash);
        params.put("accountId", AccountId);
        return params;

      }
    };

    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().

      addToRequestQueue(postReq, tag_json_obj);
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    startActivity(new Intent(LoadMoneySucessActivity.this, HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }


  private void sendFinish() {
    Log.i("Success", "Send Finish");
    Intent intent = new Intent("loadMoney-done");
    intent.putExtra("result", "1");
    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    LoadMoneySucessActivity.this.finish();
  }


  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(LoadMoneySucessActivity.this, R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(LoadMoneySucessActivity.this).sendBroadcast(intent);
  }

  public void showSuccessDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }
    CustomSuccessDialog builder = new CustomSuccessDialog(LoadMoneySucessActivity.this, "Booked Successful", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        sendRefresh();
        Intent i = new Intent(LoadMoneySucessActivity.this, HomeMainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(i);
      }
    });
    builder.show();
  }

  public String getSuccessMessage() {
    String source =
      "<b><font color=#000000> </font></b>" + "<font color=#000000>" + "Flight Ticket Booked Successfully" + "</font><br>" +
        "<b><font color=#000000> Please check your email for details </font></b>" + "<font>";
    return source;
  }

  public void showCustomDialog(String msg) {
    CustomAlertDialog builder = new CustomAlertDialog(LoadMoneySucessActivity.this, R.string.dialog_message_b, msg);
    builder.setNegativeButton("ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        Intent i = new Intent(LoadMoneySucessActivity.this, HomeMainActivity.class);
        sendFinish();
        startActivity(i);
        finish();
      }
    });
    builder.show();
  }

  public static void longLog(String str) {
    if (str.length() > 4000) {
      Log.d("bokkingLIST", str.substring(0, 4000));
      longLog(str.substring(4000));
    } else
      Log.d("bokkingLIST", str);
  }

}
