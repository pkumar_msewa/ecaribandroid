package in.msewa.ecarib.activity.travelKhanaAvtivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.msewa.adapter.FoodMenuListAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.TravelKhanaMenuModel;
import in.msewa.model.TravelKhanaStationListModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveMenuListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class FoodListActivity extends AppCompatActivity implements AddRemoveMenuListner {

  private ListView lvListBus;
  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private LoadingDialog loadingDialog;
  private Button btnNext, btnChangeStation;
  private FoodMenuListAdapter foodMenuAdapter;
  private TextView tvCurrentDateAndTime, tvCurrentStation;
  private String sCode, sName, sDateTime, dCode, dName, dDateTime, tnameno, oDate, oETA;
  private LinearLayout llList;
  private ArrayList<TravelKhanaMenuModel> selectedMenyArray;

  //Volley
  private String tag_json_obj = "json_travel";
  private LinearLayout llNoBus;
  private ArrayList<TravelKhanaStationListModel> stationList;
  private UserModel session = UserModel.getInstance();
  private JSONObject jsonRequest;
  private String station, date, trainNumber;
  private ArrayList<TravelKhanaMenuModel> menuItemArray;
  private String outletId;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_food_list_travelkhana);
    loadingDialog = new LoadingDialog(FoodListActivity.this);
    llNoBus = (LinearLayout) findViewById(R.id.llNoBus);
    llList = (LinearLayout) findViewById(R.id.llList);
    btnChangeStation = (Button) findViewById(R.id.btnChangeStation);
    btnNext = (Button) findViewById(R.id.btnNext);
    station = getIntent().getStringExtra("station");
    date = getIntent().getStringExtra("date");
    trainNumber = getIntent().getStringExtra("trainNumber");
    menuItemArray = new ArrayList<>();
    lvListBus = (ListView) findViewById(R.id.lvListBus);
    //press back button in toolbar
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    btnNext = (Button) findViewById(R.id.btnNext);
    tvCurrentDateAndTime = (TextView) findViewById(R.id.tvCurrentDateAndTime);
    tvCurrentStation = (TextView) findViewById(R.id.tvCurrentStation);
    getTrainMenuList();
    selectedMenyArray = new ArrayList<>();
    btnNext.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (selectedMenyArray.size() != 0) {
          Intent menuCartIntent = new Intent(FoodListActivity.this, MenuCartActivity.class);
          menuCartIntent.putParcelableArrayListExtra("SELECTEDMENUARRAY", selectedMenyArray);
          String info = tvCurrentDateAndTime.getText().toString() + " - " + tvCurrentStation.getText().toString();
          menuCartIntent.putExtra("INFO", info);
          menuCartIntent.putExtra("OUTLETID", outletId);
          menuCartIntent.putExtra("TRAINNO", trainNumber);
          menuCartIntent.putExtra("DATE", oDate);
          menuCartIntent.putExtra("ETA", oETA);
          menuCartIntent.putExtra("STATION", station);
          startActivity(menuCartIntent);
        } else {
          CustomToast.showMessage(FoodListActivity.this, "Sorry, No item found in cart");
        }
      }
    });
    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("select-station-done"));
    LocalBroadcastManager.getInstance(this).registerReceiver(fMessageReceiver, new IntentFilter("change-trip"));
    LocalBroadcastManager.getInstance(this).registerReceiver(cMessageReceiver, new IntentFilter("food-book-done"));

    btnChangeStation.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent busStationListIntent = new Intent(getApplicationContext(), StationListActivity.class);
        busStationListIntent.putParcelableArrayListExtra("STATIONSLIST", stationList);
        busStationListIntent.putExtra("SCODE", sCode);
        busStationListIntent.putExtra("SNAME", sName);
        busStationListIntent.putExtra("SDATETIME", sDateTime);
        busStationListIntent.putExtra("DCODE", dCode);
        busStationListIntent.putExtra("DNAME", dName);
        busStationListIntent.putExtra("DDATETIME", dDateTime);
        busStationListIntent.putExtra("TNAMENO", tnameno);
        startActivity(busStationListIntent);
      }
    });
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String time = intent.getStringExtra("time");
      String stationCode = intent.getStringExtra("stationCode");
      String stationName = intent.getStringExtra("stationName");
      String date = intent.getStringExtra("date");
      oDate = date;
      oETA = time;
      station = stationCode;
      Log.i("PARAMS", time + " " + stationCode + " " + stationName + " " + date);
      tvCurrentDateAndTime.setText("DELIVER AT " + time + ", " + date);
      getMenuList(date, stationCode, time, stationName);
    }
  };

  private BroadcastReceiver fMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      finish();
    }
  };

  private BroadcastReceiver cMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      finish();
    }
  };


  public void getTrainMenuList() {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("date", date);
      jsonRequest.put("station", station);
      jsonRequest.put("trainNumber", trainNumber);
//            jsonRequest.put("returnDate", "");


    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("TRAINMENUREQ", jsonRequest.toString());
      Log.i("TRAINMENUURL", ApiUrl.URL_TRAVELKHANA_TRAIN_MENU_LIST);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_TRAIN_MENU_LIST, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("TRAINMENURES", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {

              JSONObject trainRoutesListResponse = response.getJSONObject("trainRoutesListResponse");
              JSONArray stationsDTOArray = trainRoutesListResponse.getJSONArray("stationsDTO");
              if (stationsDTOArray.length() != 0) {
                stationList = new ArrayList<>();
                for (int j = 0; j < stationsDTOArray.length(); j++) {
                  JSONObject s = stationsDTOArray.getJSONObject(j);
                  if (s.getBoolean("foodAvailablity")) {
                    String stationCode = s.getString("stationCode");
                    String arrivalTime = s.getString("arrivalTime");
                    String dateAtStation = s.getString("dateAtStation");
                    String departureTime = s.getString("departureTime");
                    String stationName = s.getString("stationName");
                    TravelKhanaStationListModel travelkhanastationmodel = new TravelKhanaStationListModel(stationCode, arrivalTime, dateAtStation, departureTime, stationName);
                    stationList.add(travelkhanastationmodel);
                  }
                }
                oDate = stationsDTOArray.getJSONObject(0).getString("dateAtStation");
                oETA = stationsDTOArray.getJSONObject(0).getString("arrivalTime");
                outletId = trainRoutesListResponse.getString("outletId");
                sCode = stationsDTOArray.getJSONObject(0).getString("stationCode");
                sName = stationsDTOArray.getJSONObject(0).getString("stationName");
                sDateTime = stationsDTOArray.getJSONObject(0).getString("arrivalTime") + "     " + stationsDTOArray.getJSONObject(0).getString("dateAtStation");
                dCode = stationsDTOArray.getJSONObject(stationsDTOArray.length() - 1).getString("stationCode");
                dName = stationsDTOArray.getJSONObject(stationsDTOArray.length() - 1).getString("stationName");
                dDateTime = stationsDTOArray.getJSONObject(stationsDTOArray.length() - 1).getString("arrivalTime") + "     " + stationsDTOArray.getJSONObject(stationsDTOArray.length() - 1).getString("dateAtStation");
                tnameno = trainRoutesListResponse.getString("trainNumber") + " / " + trainRoutesListResponse.getString("trainName");
              } else {
                loadingDialog.dismiss();
                stationList = new ArrayList<>();
              }
              JSONArray categoriesMenuArray = trainRoutesListResponse.getJSONArray("categoriesMenu");
              menuItemArray.clear();
              for (int i = 0; i < categoriesMenuArray.length(); i++) {
                JSONObject cat = categoriesMenuArray.getJSONObject(i);
                JSONArray menuArray = cat.getJSONArray("listOfMenuDTOobject");
                for (int j = 0; j < menuArray.length(); j++) {
                  JSONObject c = menuArray.getJSONObject(j);
                  String itemName = c.getString("itemName");
                  String image = c.getString("image");
                  double customerPayable = c.getDouble("customerPayable");
                  long itemId = c.getLong("itemId");
                  String menuTag = c.getString("menuTag");
                  String description = c.getString("description");
                  String openingTime = c.getString("openingTime");
                  TravelKhanaMenuModel menuModel = new TravelKhanaMenuModel(itemName, image, customerPayable, itemId, menuTag, description, 1, openingTime, "Add");
                  menuItemArray.add(menuModel);
                }


              }
              if (!menuItemArray.isEmpty() && menuItemArray.size() != 0) {
//                                LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
//                                View dateView = layoutInflater.inflate(R.layout.header_bus_list, null);
//                                TextView tvHeaderBusListDate = (TextView) dateView.findViewById(R.id.tvHeaderBusListDate);
//                                TextView tvSourceCity = (TextView) dateView.findViewById(R.id.tvSourceCity);
//                                TextView tvDestCity = (TextView) dateView.findViewById(R.id.tvDestCity);
//                                tvSourceCity.setText(sourceName);
//                                tvDestCity.setText(destinationName);
//                                String[] splitDate = dateOfDep.split("-");
//
//                                tvHeaderBusListDate.setText(splitDate[0] + " " + getMonth(Integer.valueOf(splitDate[1])) + " " + splitDate[2]);
//                                lvListBus.addHeaderView(dateView);
                tvCurrentDateAndTime.setText("Delivery will at " + oETA + ", " + trainRoutesListResponse.getString("date"));
                tvCurrentStation.setText(stationList.get(0).getStationName());
                foodMenuAdapter = new FoodMenuListAdapter(FoodListActivity.this, menuItemArray, FoodListActivity.this);
                lvListBus.setAdapter(foodMenuAdapter);
                selectedMenyArray = new ArrayList<>();
                loadingDialog.dismiss();
              } else {
                loadingDialog.dismiss();
                llNoBus.setVisibility(View.VISIBLE);
                llList.setVisibility(View.GONE);
              }

            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog();
            } else if (code != null && code.equals("N00")) {
              loadingDialog.dismiss();
              llNoBus.setVisibility(View.VISIBLE);
              llList.setVisibility(View.GONE);
            } else {
              loadingDialog.dismiss();
              CustomToast.showMessage(FoodListActivity.this, message);
              finish();

            }
          } catch (JSONException e) {
            e.printStackTrace();
            CustomToast.showMessage(getApplicationContext(), "Oops, something went wrong. Please after sometime");
            finish();
            loadingDialog.dismiss();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          error.printStackTrace();
          finish();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };

      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void getMenuList(final String date, String station, String arrivalTime, final String stationName) {
    loadingDialog.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("date", date);
      jsonRequest.put("station", station);
      jsonRequest.put("arrivalTime", arrivalTime);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("MENUREQ", jsonRequest.toString());
      Log.i("MENUURL", ApiUrl.URL_TRAVELKHANA_MENU_LIST);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_MENU_LIST, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("MENURES", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {
              menuItemArray.clear();
              JSONObject listOfOuletobject = response.getJSONObject("listOfOuletobject");
              JSONArray categoriesMenuArray = listOfOuletobject.getJSONArray("categoriesMenu");
              for (int i = 0; i < categoriesMenuArray.length(); i++) {
                JSONObject cat = categoriesMenuArray.getJSONObject(i);
                JSONArray menuArray = cat.getJSONArray("listOfMenuDTOobject");
                for (int j = 0; j < menuArray.length(); j++) {
                  JSONObject c = menuArray.getJSONObject(j);
                  String itemName = c.getString("itemName");
                  String image = c.getString("image");
                  double customerPayable = c.getDouble("customerPayable");
                  long itemId = c.getLong("itemId");
                  String menuTag = c.getString("menuTag");
                  String description = c.getString("description");
                  String openingTime = c.getString("openingTime");
                  TravelKhanaMenuModel menuModel = new TravelKhanaMenuModel(itemName, image, customerPayable, itemId, menuTag, description, 1, openingTime, "Add");
                  menuItemArray.add(menuModel);
                }


              }
              if (!menuItemArray.isEmpty() && menuItemArray.size() != 0) {
//                tvCurrentDateAndTime.setText("DELIVER AT " + menuItemArray.get(0).getTime() + ", " + date);
                tvCurrentStation.setText(stationName);
                foodMenuAdapter = new FoodMenuListAdapter(FoodListActivity.this, menuItemArray, FoodListActivity.this);
                lvListBus.setAdapter(foodMenuAdapter);
                selectedMenyArray = new ArrayList<>();
                loadingDialog.dismiss();
              } else {
                loadingDialog.dismiss();
                llNoBus.setVisibility(View.VISIBLE);
                lvListBus.setVisibility(View.GONE);
              }

            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog();
            } else {
              loadingDialog.dismiss();
              llNoBus.setVisibility(View.VISIBLE);
              lvListBus.setVisibility(View.GONE);
              CustomToast.showMessage(FoodListActivity.this, message);

            }
          } catch (JSONException e) {
            e.printStackTrace();
            CustomToast.showMessage(getApplicationContext(), "Oops, something went wrong. Please after sometime");
            finish();
            loadingDialog.dismiss();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          error.printStackTrace();
          finish();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };

      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public String getMonth(int month) {
    return new DateFormatSymbols().getMonths()[month - 1];
  }


  public String getDay(int day) {
    return new DateFormatSymbols().getWeekdays()[day + 1];
  }

  @Override
  protected void onResume() {
    super.onResume();

  }

  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    LocalBroadcastManager.getInstance(this).unregisterReceiver(fMessageReceiver);
    LocalBroadcastManager.getInstance(this).unregisterReceiver(cMessageReceiver);
    super.onDestroy();
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(FoodListActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });

    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(FoodListActivity.this).sendBroadcast(intent);
  }


  private Date parseDate(String date) {
    Date arrTime = null;
    SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    try {
      arrTime = arrinput.parse(date);
//                depTime = depinput.parse(busArray.get(position).getBusDepTime());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return arrTime;
  }

  @Override
  public void taskCompleted(ArrayList<TravelKhanaMenuModel> menuArray) {
    selectedMenyArray = menuArray;
  }

  @Override
  public void AddRemoveComplete(double totapPrice, JSONArray items) {

  }


}
