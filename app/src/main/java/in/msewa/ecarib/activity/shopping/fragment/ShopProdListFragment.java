package in.msewa.ecarib.activity.shopping.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.orm.query.Select;
import com.readystatesoftware.viewbadger.BadgeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.BaseSliderView;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.TextProgressBar;
import in.msewa.custom.ViewPagerEx;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.CategoryListModel;
import in.msewa.model.PQCart;
import in.msewa.model.ShoppingModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveCartListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.CatAdapter;
import in.msewa.ecarib.activity.shopping.ShopAdapter;
import in.msewa.ecarib.activity.shopping.filter.FiltterActivity;

/**
 * Created by Ksf on 4/29/2016.
 */
public class ShopProdListFragment extends Fragment implements AddRemoveCartListner, View.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

  private static String selectValue;
  AlertDialog alertDialog;
  private RecyclerView rvShopping;
  private TextView tvcart_point;
  private List<ShoppingModel> shoppingArray;
  private RequestQueue rq;
  private String cartValue = "0";
  private PQCart pqCart = PQCart.getInstance();
  private ShopAdapter shoppingAdapter;
  private UserModel session = UserModel.getInstance();
  private Button btnGoCart;
  private LoadingDialog loadDlg;
  private Toolbar toolbar;
  private TextView itemOne;
  private TextView itemTwo;
  private TextView itemThree;
  private TextView itemFive;
  private TextView itemFour;
  private LinearLayout llSort, llFilter;
  private Button submit;
  private View view1, view2;
  private JsonObjectRequest postReq;
  private String tag_json_obj = "json_events";
  private JSONObject jsonRequest;
  private String action;
  private ImageView ivFab;
  private BadgeView badge;
  private View rootView;
  private String CATID;
  private TextView tvCategory;
  private List<CategoryListModel> categoryListModel;
  private CatAdapter catAdapter;
  private GetCat getCatTask = null;
  private Boolean checker;
  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("updates");
      if (action.equals("1")) {
        pqCart.clearCart();
//                ShoppingModel.delete(ShoppingModel.class);
//                shoppingArray.clear();
////                tvcart_point.setText("0");
//                shoppingAdapter.notifyDataSetChanged();
        badge.hide();

      } else if (action.equals("2")) {
        cartValue = String.valueOf(pqCart.getProductsInCartArray().size());
//                tvcart_point.setText(cartValue);
        if (Integer.parseInt(cartValue) > 0) {
          badge.setText(cartValue);
          badge.show();
        }
//                buildCounterDrawable(pqCart.getProductsInCartArray().size(), R.drawable.shopping_cart);
      }
    }
  };
  private BroadcastReceiver fMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      action = intent.getStringExtra("TYPE");
      if (action.equals("SUBCAT")) {
        String catCode = intent.getStringExtra("CATID");
        String subCatCode = intent.getStringExtra("SUBCATID");
        getShoppingItems("SUBCAT", catCode, subCatCode, "");
      } else if (action.equals("all")) {
        getShoppingItems("all", "", "", "");
      } else {
        String brandCode = intent.getStringExtra("BRANDID");
        getShoppingItems("BRAND", "", "", brandCode);
      }
    }
  };
  private TextProgressBar progressBar;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    categoryListModel = Select.from(CategoryListModel.class).list();

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_prod_list, null);
    progressBar = (TextProgressBar) rootView.findViewById(R.id.progressBar);
//        view1 = getLayoutInflater().inflate(R.layout.activity_shopping, null);
    view2 = getActivity().getLayoutInflater().inflate(R.layout.shoppingpoplate, null);
    loadDlg = new LoadingDialog(getActivity());
    badge = new BadgeView(getActivity(), ivFab);
    llFilter = (LinearLayout) rootView.findViewById(R.id.llFilter);
    llSort = (LinearLayout) rootView.findViewById(R.id.llSort);
    tvCategory = (TextView) rootView.findViewById(R.id.tvCategory);
//        etCategory.setFocusable(false);
    checker = true;
    llFilter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent filterIntent = new Intent(getActivity(), FiltterActivity.class);
        startActivity(filterIntent);
      }
    });

    llSort.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showSortingDialog();
      }
    });
    rvShopping = (RecyclerView) rootView.findViewById(R.id.rvShoppingProd);
    rvShopping.addItemDecoration(new SpacesItemDecoration(8));
    final GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
    rvShopping.setLayoutManager(manager);
    rvShopping.setHasFixedSize(true);
    shoppingArray = Select.from(ShoppingModel.class).list();
    shoppingArray = new ArrayList<>();

    getCatTask = new GetCat();
    getCatTask.execute();

    tvCategory.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (categoryListModel == null || categoryListModel.size() == 0) {
          CustomToast.showMessage(getActivity(), "Sorry error occured please try again");
        } else {
          showCategoryDialog();
        }

      }
    });


    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(fMessageReceiver, new IntentFilter("filter-activated"));

    try {
      final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
      rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
    } catch (KeyManagementException | NoSuchAlgorithmException e) {
      e.printStackTrace();
    }

//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameInShop, new ShopCatListFragment()).commit();
//        ivFab = (ImageView)  rootView.findViewById(R.id.ivFab);

    Bundle bundle = this.getArguments();
    if (bundle != null) {
      CATID = bundle.getString("CATID");
    }
    if (shoppingArray != null && shoppingArray.size() != 0) {
      Collections.sort(shoppingArray, new compPopulation());
      shoppingAdapter = new ShopAdapter(getActivity(), shoppingArray, (AddRemoveCartListner) getActivity(), getHostFragmentManager());
      rvShopping.setAdapter(shoppingAdapter);
    } else {
      getShoppingItems("all", CATID, "", "");

    }
    return rootView;
  }

  public void getCatList() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobile", session.getUserMobileNo());
      jsonRequest.put("countryName", "india");


    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("CATLIST", ApiUrl.URL_SHOPPING_CATEGORY_LIST);
      Log.i("CATREQ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPING_CATEGORY_LIST, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          loadDlg.dismiss();
          try {
            Log.i("CATRES", jsonObj.toString());
            String code = jsonObj.getString("code");

            if (code != null && code.equals("S00")) {
              try {
                if (!jsonObj.isNull("response")) {
                  categoryListModel.clear();
                  CategoryListModel.deleteAll(CategoryListModel.class);
                  JSONArray productJArray = jsonObj.getJSONArray("response");
                  if (productJArray.length() != 0) {

                    for (int i = 0; i < productJArray.length(); i++) {
                      JSONObject c = productJArray.getJSONObject(i);
                      JSONObject catId = c.getJSONObject("country");
                      long cId = c.getLong("id");
                      String cImage = c.getString("categoryImage");
                      String cName = c.getString("categoryName");
                      CategoryListModel ccategoryListModel = new CategoryListModel(String.valueOf(cId), cName, cImage);
                      categoryListModel.add((ccategoryListModel));

                    }
                    if (categoryListModel.size() == 0) {
                      CustomToast.showMessage(getActivity(), "No category Found");
                    }
                  } else {
                    CustomToast.showMessage(getActivity(), "No category Found");
                  }
                }
              } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
              }
              loadDlg.dismiss();

            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void showCategoryDialog() {
    final ArrayList<String> bankVal = new ArrayList<>();
    for (CategoryListModel countryModel : categoryListModel) {
      bankVal.add(countryModel.getcategoryName());
    }

    AlertDialog.Builder bankDialog =
      new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
    bankDialog.setTitle("Select Category");

    bankDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });

    bankDialog.setItems(bankVal.toArray(new String[bankVal.size()]),
      new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int i) {
          tvCategory.setText(bankVal.get(i));
          CATID = categoryListModel.get(i).getcategoryCode();
          getShoppingItems("all", CATID, "", "");
        }
      });
    bankDialog.show();

  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

  public FragmentManager getHostFragmentManager() {
    FragmentManager fm = getFragmentManager();
    if (fm == null && isAdded() && getActivity() != null) {
      fm = (getActivity()).getSupportFragmentManager();
    }
    return fm;
  }

  public void getShoppingItems(String type, String catId, String subCatId, String brandID) {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      if (type.equals("SUBCAT")) {
        jsonRequest.put("subCategoryId", subCatId);
      } else if (type.equals("BRAND")) {
        jsonRequest.put("brandId", brandID);
      }
      jsonRequest.put("mobile", session.getUserMobileNo());
      jsonRequest.put("country", "india");
      jsonRequest.put("categoryId", catId);


    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("SHOPPINGURL", ApiUrl.URL_GET_ALL_SHOPPING_PRODUCTS);
      Log.i("SHOPPINGREQ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_ALL_SHOPPING_PRODUCTS, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          loadDlg.dismiss();
          try {
            Log.i("SHOPPINGRES", jsonObj.toString());
            String code = jsonObj.getString("code");

            if (code != null && code.equals("S00")) {
              try {
                if (!jsonObj.isNull("response")) {
                  shoppingArray.clear();
//                                    ShoppingModel.deleteAll(ShoppingModel.class);

                  JSONArray productJArray = jsonObj.getJSONArray("response");
                  if (productJArray.length() != 0 && !productJArray.equals("[]")) {

                    for (int i = 0; i < productJArray.length(); i++) {
                      JSONObject c = productJArray.getJSONObject(i);
                      long pId = c.getLong("id");
                      String pImage1, pImage2, pImage3, pImage4;
                      if (c.getString("productImage") != null) {
                        pImage1 = c.getString("productImage");
                      } else {
                        pImage1 = "@";
                      }
                      if (c.getString("productImage2") != null && !c.getString("productImage2").equals("null")) {
                        pImage2 = c.getString("productImage2");
                      } else {
                        pImage2 = "@";
                      }
                      if (c.getString("productImage3") != null && !c.getString("productImage3").equals("null")) {
                        pImage3 = c.getString("productImage3");
                      } else {
                        pImage3 = "@";
                      }
                      if (c.getString("productImage4") != null && !c.getString("productImage4").equals("null")) {
                        pImage4 = c.getString("productImage4");
                      } else {
                        pImage4 = "@";
                      }
                      String pImage = pImage1 + "#" + pImage2 + "#" + pImage3 + "#" + pImage4;
                      Log.i("IMAGE1", pImage);
                      String pName = c.getString("productName");
                      String pDesc = c.getString("description");
                      long pPrice = c.getLong("unitPrice");
//                                String pStatus;
//                                if (!c.getString("status").isEmpty()) {
//                                    pStatus = c.getString("status");
//                                } else {
//                                    pStatus = "no status";
//                                }
                      long pWeight = c.getLong("weight");
                      String pCat = c.getString("category");
                      String pSubCat = c.getString("subCategory");
                      String pBrand = c.getString("brand");
                      ShoppingModel sModel = new ShoppingModel(pId, pName, pDesc, pPrice, pImage, "success", pWeight, pCat, pSubCat, pBrand, 0);
//                                            sModel.save();
                      shoppingArray.add((sModel));
                      Log.i("LISTSIADP", String.valueOf(Select.from(ShoppingModel.class).list().size()));
                    }
                    progressBar.setVisibility( View.GONE);
                    if (shoppingArray.size() != 0) {
                      Collections.sort(shoppingArray, new compPopulation());
                      try {
                        progressBar.setVisibility( View.GONE);
                        shoppingAdapter = new ShopAdapter(getActivity(), shoppingArray, (AddRemoveCartListner) getActivity(), getHostFragmentManager());
                        rvShopping.setAdapter(shoppingAdapter);
                      } catch (NullPointerException e) {
                        e.printStackTrace();
                      }
                    }
                    getLowToHigh(shoppingArray);
                  } else {
//                                        if (shoppingArray.size() != 0) {
//                                            Collections.sort(shoppingArray, new compPopulation());
//                                            try {
//                                                shoppingAdapter = new ShopAdapter(getActivity(), shoppingArray, (AddRemoveCartListner) getActivity(), getHostFragmentManager());
//                                                rvShopping.setAdapter(shoppingAdapter);
//                                            } catch (NullPointerException e) {
//                                                e.printStackTrace();
//                                            }
                    CustomToast.showMessage(getActivity(), "Product is not found");
//                                        }
                  }
                }
              } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
              }
//                            getHighToLow(shoppingArray);
              loadDlg.dismiss();

            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  @Override
  public void taskCompleted(String response) {
    if (response.equals("Add")) {
      cartValue = String.valueOf(Integer.parseInt(cartValue) + 1);
      tvcart_point.setText(cartValue);
      if (Integer.parseInt(cartValue) > 0) {
        badge.setText(cartValue);
        badge.show();
      }
//            shoppingAdapter.notifyDataSetChanged();
    } else {
      cartValue = String.valueOf(Integer.parseInt(cartValue) - 1);
//            shoppingAdapter.notifyDataSetChanged();
//            tvcart_point.setText(cartValue);
      if (Integer.parseInt(cartValue) > 0) {
        badge.setText(cartValue);
        badge.show();
      }
    }

  }

//    public void getHighToLow(List<ShoppingModel> shoppingArray) {
//        if (shoppingArray != null && shoppingArray.size() != 0) {
//            Collections.sort(shoppingArray, Collections.reverseOrder());
//            shoppingAdapter = new ShopAdapter(getActivity(), shoppingArray, (AddRemoveCartListner) getActivity());
//            rvShopping.setAdapter(shoppingAdapter);
//        }
//    }

  public void getLowToHigh(List<ShoppingModel> shoppingArray) {
    if (shoppingArray != null && shoppingArray.size() != 0) {
      Collections.sort(shoppingArray, new Comparator<ShoppingModel>() {
        @Override
        public int compare(ShoppingModel lhs, ShoppingModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
          return lhs.getpPrice() > rhs.getpPrice() ? 1 : (lhs.getpPrice() < rhs.getpPrice()) ? -1 : 0;
        }
      });
      try {
        shoppingAdapter.notifyDataSetChanged();
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }

  public void getHighToLow(List<ShoppingModel> shoppingArray) {
    if (shoppingArray != null && shoppingArray.size() != 0) {
      Collections.sort(shoppingArray, new Comparator<ShoppingModel>() {
        @Override
        public int compare(ShoppingModel lhs, ShoppingModel rhs) {
          // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
          return lhs.getpPrice() > rhs.getpPrice() ? -1 : (lhs.getpPrice() < rhs.getpPrice()) ? 1 : 0;
        }
      });
      try {
        shoppingAdapter.notifyDataSetChanged();
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
  }

  public void showSortingDialog() {
    LayoutInflater myLayout = LayoutInflater.from(getActivity());

    View dialogView = myLayout.inflate(R.layout.shoppingpoplate, null);

    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

    alertDialogBuilder.setView(dialogView);

    // create alert dialog
    alertDialog = alertDialogBuilder.create();
    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    itemOne = (TextView) dialogView.findViewById(R.id.itemOne);
    itemTwo = (TextView) dialogView.findViewById(R.id.itemTwo);
    itemThree = (TextView) dialogView.findViewById(R.id.itemThree);
    itemFive = (TextView) dialogView.findViewById(R.id.itemFive);
    itemFour = (TextView) dialogView.findViewById(R.id.itemFour);
    submit = (Button) dialogView.findViewById(R.id.submit);
    ImageView ivclose = (ImageView) dialogView.findViewById(R.id.ivclose);
    ivclose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        alertDialog.dismiss();
      }
    });
    itemOne.setOnClickListener(this);
    itemTwo.setOnClickListener(this);
    itemThree.setOnClickListener(this);
    itemFive.setOnClickListener(this);
    itemFour.setOnClickListener(this);
    submit.setOnClickListener(this);

    // show it
    alertDialog.show();
  }

  @Override
  public void onResume() {
    super.onResume();
    tvCategory.setText("");
    Log.i("CHECKFRST", checker.toString());
//        if(checker){
//            getShoppingItemsFirst();
//            checker=false;
//            Log.i("CHECKFRST", checker.toString());
//        }

//        cartValue = String.valueOf(pqCart.getProductsInCart().size());
//        Log.i("CARTONRESUME", cartValue);
////        tvcart_point.setText(cartValue);
//        if(Integer.parseInt(cartValue)>0){
//            badge.setText(cartValue);
//            badge.show();
//        }
    if (selectValue != null && selectValue.length() != 0) {
      if (selectValue.equalsIgnoreCase("Price - high to low")) {
        getHighToLow(shoppingArray);
        selectValue = "";
      } else if (selectValue.equalsIgnoreCase("Price - low to high")) {
        getLowToHigh(shoppingArray);
        selectValue = "";
      }
    }
  }

  @Override
  public void onClick(View v) {
    Intent i = getActivity().getIntent();
    switch (v.getId()) {
      case R.id.itemOne:
        itemOne.setTextColor(getResources().getColor(R.color.white_text));
        itemOne.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        itemTwo.setTextColor(getResources().getColor(R.color.black));
        itemTwo.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFour.setTextColor(getResources().getColor(R.color.black));
        itemFour.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemThree.setTextColor(getResources().getColor(R.color.black));
        itemThree.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFive.setTextColor(getResources().getColor(R.color.black));
        itemFive.setBackgroundColor(getResources().getColor(R.color.white_text));
        selectValue = itemOne.getText().toString();
        break;
      case R.id.itemTwo:
        itemOne.setTextColor(getResources().getColor(R.color.black));
        itemOne.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemTwo.setTextColor(getResources().getColor(R.color.white_text));
        itemTwo.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        itemFour.setTextColor(getResources().getColor(R.color.black));
        itemFour.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemThree.setTextColor(getResources().getColor(R.color.black));
        itemThree.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFive.setTextColor(getResources().getColor(R.color.black));
        itemFive.setBackgroundColor(getResources().getColor(R.color.white_text));
        selectValue = itemTwo.getText().toString();
        break;
      case R.id.itemThree:
        itemOne.setTextColor(getResources().getColor(R.color.black));
        itemOne.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemTwo.setTextColor(getResources().getColor(R.color.black));
        itemTwo.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFour.setTextColor(getResources().getColor(R.color.black));
        itemFour.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemThree.setTextColor(getResources().getColor(R.color.white_text));
        itemThree.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        itemFive.setTextColor(getResources().getColor(R.color.black));
        itemFive.setBackgroundColor(getResources().getColor(R.color.white_text));
        selectValue = itemThree.getText().toString();
        break;
      case R.id.itemFour:
        itemOne.setTextColor(getResources().getColor(R.color.black));
        itemOne.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemTwo.setTextColor(getResources().getColor(R.color.black));
        itemTwo.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFour.setTextColor(getResources().getColor(R.color.white_text));
        itemFour.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        itemThree.setTextColor(getResources().getColor(R.color.black));
        itemThree.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFive.setTextColor(getResources().getColor(R.color.black));
        itemFive.setBackgroundColor(getResources().getColor(R.color.white_text));
        break;
      case R.id.itemFive:
        itemOne.setTextColor(getResources().getColor(R.color.black));
        itemOne.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemTwo.setTextColor(getResources().getColor(R.color.black));
        itemTwo.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFour.setTextColor(getResources().getColor(R.color.black));
        itemFour.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemThree.setTextColor(getResources().getColor(R.color.black));
        itemThree.setBackgroundColor(getResources().getColor(R.color.white_text));
        itemFive.setTextColor(getResources().getColor(R.color.white_text));
        itemFive.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        selectValue = itemFive.getText().toString();
        break;
      case R.id.submit:
        alertDialog.dismiss();
        if (selectValue != null && selectValue.length() != 0) {
          if (selectValue.equalsIgnoreCase("Price - high to low")) {
            getHighToLow(shoppingArray);
            selectValue = "";
          } else if (selectValue.equalsIgnoreCase("Price - low to high")) {
            getLowToHigh(shoppingArray);
            selectValue = "";
          } else {
            Toast.makeText(getActivity(), "Coming soon...", Toast.LENGTH_SHORT).show();
            break;
          }
        }

    }
  }

  @Override
  public void onSliderClick(BaseSliderView slider) {

  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {

  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "6");
    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
  }

  @Override
  public void onDestroy() {
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(fMessageReceiver);
    super.onDestroy();
  }

  private class GetCat extends AsyncTask<Void, Void, Void> {
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      loadDlg.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
      getCatList();
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      tvCategory.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (categoryListModel == null || categoryListModel.size() == 0) {
            CustomToast.showMessage(getActivity(), "Error fetching category list, please try again later");
          } else {
            showCategoryDialog();
          }
        }
      });
      loadDlg.dismiss();
    }
  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }

  public class compPopulation implements Comparator<ShoppingModel> {

    public int compare(ShoppingModel a, ShoppingModel b) {
      Log.i("MAx", "value");
      if (a.getpPrice() > b.getpPrice())
        return -1; // highest value first
      if (a.getpPrice() == b.getpPrice())
        return 0;
      return 1;
    }
  }

}
