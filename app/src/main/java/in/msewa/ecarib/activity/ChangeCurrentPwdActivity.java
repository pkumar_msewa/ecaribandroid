package in.msewa.ecarib.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.TLSSocketFactory;

/**
 * Created by Ksf on 5/14/2016.
 */

public class ChangeCurrentPwdActivity extends AppCompatActivity {
    private RequestQueue rq;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();

    private EditText etChangeNewPwd, etChangeCurrentPwd;
    private TextInputLayout ilChangeCurrentPwd, ilChangeNewPwd;
    private Button btnChangePwdDismiss, btnChangePwd;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    private ImageButton iBtnShowCurrentPwd,iBtnShowCurrentNewPwd;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_current_pwd);

        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(ChangeCurrentPwdActivity.this);
            rq = Volley.newRequestQueue(ChangeCurrentPwdActivity.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton)findViewById(R.id.ivBackBtn);

        iBtnShowCurrentPwd = (ImageButton) findViewById(R.id.iBtnShowCurrentPwd);
        iBtnShowCurrentNewPwd = (ImageButton) findViewById(R.id.iBtnShowCurrentNewPwd);

        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        loadDlg = new LoadingDialog(ChangeCurrentPwdActivity.this);


        etChangeNewPwd = (EditText) findViewById(R.id.etChangeNewPwd);
        etChangeCurrentPwd = (EditText) findViewById(R.id.etChangeCurrentPwd);

        ilChangeCurrentPwd = (TextInputLayout) findViewById(R.id.ilChangeCurrentPwd);
        ilChangeNewPwd = (TextInputLayout) findViewById(R.id.ilChangeNewPwd);


        btnChangePwd = (Button) findViewById(R.id.btnChangePwd);
        btnChangePwdDismiss = (Button) findViewById(R.id.btnChangePwdDismiss);

        btnChangePwdDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitQuery();

            }
        });

        iBtnShowCurrentPwd.setOnTouchListener(new View.OnTouchListener()
        {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    etChangeCurrentPwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etChangeCurrentPwd.setSelection(etChangeCurrentPwd.length());
                }

                else if (event.getAction() == MotionEvent.ACTION_UP){
                    etChangeCurrentPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etChangeCurrentPwd.setSelection(etChangeCurrentPwd.length());
                }

                return false;
            }
        });

        iBtnShowCurrentNewPwd.setOnTouchListener(new View.OnTouchListener()
        {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    etChangeNewPwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etChangeNewPwd.setSelection(etChangeNewPwd.length());
                }

                else if (event.getAction() == MotionEvent.ACTION_UP){
                    etChangeNewPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etChangeNewPwd.setSelection(etChangeNewPwd.length());
                }

                return false;
            }
        });


    }

    private void submitQuery() {
        if (!validateCurrentMPin()) {
            return;
        }
        if (!validateNewMPin()) {
            return;
        }
        changePassword();
    }

    private boolean validateCurrentMPin() {
        if (etChangeCurrentPwd.getText().toString().trim().isEmpty() || etChangeCurrentPwd.getText().toString().trim().length() < 6) {
            ilChangeCurrentPwd.setError(getResources().getString(R.string.password_validity));
            requestFocus(etChangeCurrentPwd);
            return false;
        } else {
            ilChangeCurrentPwd.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateNewMPin() {
        if (etChangeNewPwd.getText().toString().trim().isEmpty() || etChangeNewPwd.getText().toString().trim().length() < 6) {
            ilChangeNewPwd.setError(getResources().getString(R.string.password_validity));
            requestFocus(etChangeNewPwd);
            return false;
        }  else {
            ilChangeNewPwd.setErrorEnabled(false);
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void changePassword() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("username", session.getUserMobileNo());
            jsonRequest.put("password", etChangeCurrentPwd.getText().toString());
            jsonRequest.put("newPassword", etChangeNewPwd.getText().toString());
            jsonRequest.put("confirmPassword", etChangeNewPwd.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CHANGE_CURRENT_PWD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Chan PWD Response", response.toString());
                        String code = response.getString("code");
                        String jsonString = response.getString("response");
                        JSONObject jsonObject = new JSONObject(jsonString);
                        String message = jsonObject.getString("message");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            etChangeNewPwd.getText().clear();
                            etChangeCurrentPwd.getText().clear();
                            //Completed
                            CustomToast.showMessage(getApplicationContext(),message);
                            finish();

                        }
                        else if(code != null && code.equals("F03")){
                            CustomToast.showMessage(getApplicationContext(),message);
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        }
                        else {
                            CustomToast.showMessage(getApplicationContext(),message);
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(),getResources().getString(R.string.server_exception2));

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(),getResources().getString(R.string.server_exception));
                    error.printStackTrace();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
          PayQwikApplication.getInstance().addToRequestQueue(postReq,"test");
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(ChangeCurrentPwdActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

}

