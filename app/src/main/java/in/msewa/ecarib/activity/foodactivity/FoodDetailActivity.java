package in.msewa.ecarib.activity.foodactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import in.msewa.adapter.FoodImageListAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.model.FoodCart;
import in.msewa.model.FoodModel;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 12/9/2016.
 */
public class FoodDetailActivity extends AppCompatActivity {

    private FoodModel foodModel;
    private RecyclerView rvFoodDetail;
    private FoodImageListAdapter foodImageListAdapter;
    private AQuery aq;
    private ImageView ivFoodDetail;

    private Button btnFoodDetailBuyNow;
    private Button btnFoodDetailAddToCart;

    private FoodCart foodCart = FoodCart.getInstance();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        foodModel = getIntent().getParcelableExtra("FoodModel");
        aq = new AQuery(FoodDetailActivity.this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("food-image-changed"));

        TextView wvFoodDetails = (TextView) findViewById(R.id.wvFoodDetails);
        TextView tvFoodDetailApplicablePriceList = (TextView) findViewById(R.id.tvFoodDetailApplicablePriceList);
        TextView tvFoodDetailRetailPriceList = (TextView) findViewById(R.id.tvFoodDetailRetailPriceList);
        TextView tvFoodDetailTitle = (TextView) findViewById(R.id.tvFoodDetailTitle);
        TextView tvFoodDetailDiscountList = (TextView) findViewById(R.id.tvFoodDetailDiscountList);

        btnFoodDetailBuyNow = (Button) findViewById(R.id.btnFoodDetailBuyNow);
        btnFoodDetailAddToCart = (Button) findViewById(R.id.btnFoodDetailAddToCart);

        ivFoodDetail = (ImageView) findViewById(R.id.ivFoodDetail);
        rvFoodDetail = (RecyclerView) findViewById(R.id.rvFoodDetail);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            wvFoodDetails.setText(Html.fromHtml(foodModel.getpDetails(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            wvFoodDetails.setText(Html.fromHtml(foodModel.getpDetails()));

        }

        tvFoodDetailApplicablePriceList.setText(getResources().getString(R.string.rupease) + " " + foodModel.getpApplicablePrice());
        tvFoodDetailTitle.setText(foodModel.getpName());

        if (foodModel.isDiscount()) {
            tvFoodDetailRetailPriceList.setText(getResources().getString(R.string.rupease) + " " + foodModel.getpRetailPrice());
            tvFoodDetailRetailPriceList.setPaintFlags(tvFoodDetailRetailPriceList.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvFoodDetailRetailPriceList.setTextColor(ContextCompat.getColor(FoodDetailActivity.this, R.color.mark_red));
            tvFoodDetailRetailPriceList.setVisibility(View.VISIBLE);
            tvFoodDetailDiscountList.setText(foodModel.getpDiscountPrice() + "% Off");
            tvFoodDetailDiscountList.setVisibility(View.VISIBLE);
        } else {
            tvFoodDetailRetailPriceList.setVisibility(View.GONE);
            tvFoodDetailDiscountList.setVisibility(View.GONE);
        }

        tvFoodDetailApplicablePriceList.setText(getResources().getString(R.string.rupease) + " " + foodModel.getpApplicablePrice());


        //Horizontal Image Views
        if (foodModel.getpImageUrls().size() != 0) {
            aq.id(ivFoodDetail).image(foodModel.getpImageUrls().get(0), true, true);

            int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
            rvFoodDetail.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

            GridLayoutManager manager = new GridLayoutManager(FoodDetailActivity.this, 1);
            manager.setOrientation(GridLayoutManager.HORIZONTAL);
            rvFoodDetail.setLayoutManager(manager);
            rvFoodDetail.setHasFixedSize(true);

            foodImageListAdapter = new FoodImageListAdapter(FoodDetailActivity.this, foodModel.getpImageUrls());
            rvFoodDetail.setAdapter(foodImageListAdapter);
        }

        btnFoodDetailBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomToast.showMessage(getApplicationContext(), "Buy");

            }
        });

        if (foodCart.isProductInCart(foodModel)) {
            btnFoodDetailAddToCart.setText("Remove");
            btnFoodDetailAddToCart.setBackgroundResource(R.drawable.bg_button_cart);
        } else {
            btnFoodDetailAddToCart.setText("Add to cart");
            btnFoodDetailAddToCart.setBackgroundResource(R.drawable.bg_button);
        }

        btnFoodDetailAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!foodCart.isProductInCart(foodModel)) {
                    ((Button) view).setText("Remove");
//                    btnFoodDetailAddToCart.setText("Remove");
                    ((Button) view).setBackgroundResource(R.drawable.bg_button_cart);
                } else {
                    ((Button) view).setText("Add to cart");
                    ((Button) view).setBackgroundResource(R.drawable.bg_button);
                }
                foodCart.addProductsInCart(foodModel);
                invalidateOptionsMenu();
                Intent intent = new Intent("food-cart-changed");
                LocalBroadcastManager.getInstance(FoodDetailActivity.this).sendBroadcast(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shopping_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.menuCart);
        menuItem.setIcon(buildCounterDrawable(foodCart.getProductsInCartArray().size(), R.drawable.shopping_cart));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuCart) {
            startActivity(new Intent(FoodDetailActivity.this, FoodCartListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private Drawable buildCounterDrawable(int count, int backgroundImageId) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.layout_cart_icon, null);
        view.setBackgroundResource(backgroundImageId);

        if (count == 0) {
            View counterTextPanel = view.findViewById(R.id.cartCount);
            counterTextPanel.setVisibility(View.GONE);
        } else {
            TextView textView = (TextView) view.findViewById(R.id.cartCount);
            textView.setText("" + count);
        }

        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }


    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra("position", 0);
            aq.id(ivFoodDetail).image(foodModel.getpImageUrls().get(position), true, true);
        }
    };

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


}
