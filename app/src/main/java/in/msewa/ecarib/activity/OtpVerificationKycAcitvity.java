package in.msewa.ecarib.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 12/6/2016.
 */
public class OtpVerificationKycAcitvity extends AppCompatActivity {

  private EditText etVerifyOTP;
  private Button btnVerify;
  private Button btnVerifyResend;
  private TextInputLayout ilVerifyOtp;

  private LoadingDialog loadDlg;
  private UserModel session = UserModel.getInstance();

  private JSONObject jsonRequest;
  //Volley Tag
  private String tag_json_obj = "json_user";

  private View focusView = null;
  private boolean cancel;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_verify_mobile);
    btnVerifyResend = (Button) findViewById(R.id.btnVerifyResend);
    btnVerify = (Button) findViewById(R.id.btnVerify);
    etVerifyOTP = (EditText) findViewById(R.id.etVerifyOTP);
    ilVerifyOtp = (TextInputLayout) findViewById(R.id.ilVerifyOtp);
    btnVerifyResend.setVisibility(View.GONE);
    btnVerify.setTextColor(Color.parseColor("#ffffff"));
    loadDlg = new LoadingDialog(OtpVerificationKycAcitvity.this);
    btnVerify.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        attemptPayment();
      }
    });

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

  }

  private void attemptPayment() {
    etVerifyOTP.setError(null);
    cancel = false;
    checkCustomerOTP(etVerifyOTP.getText().toString());
    if (cancel) {
      focusView.requestFocus();
    } else {
      verifyOTP();
    }
  }

  private void checkCustomerOTP(String otp) {
    CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(otp);
    if (!gasCheckLog.isValid) {
      ilVerifyOtp.setError(getString(gasCheckLog.msg));
      focusView = etVerifyOTP;
      cancel = true;
    }
  }


  private void verifyOTP() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("otp", etVerifyOTP.getText().toString());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VERIFY_KYC_DATA, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            String code = response.getString("code");
            if (code != null & code.equals("S00")) {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomSuccessDialog customSuccessDialog = new CustomSuccessDialog(OtpVerificationKycAcitvity.this, "", message);
                customSuccessDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    sendRefresh();
                    finishAffinity();
                    startActivity(new Intent(OtpVerificationKycAcitvity.this, HomeMainActivity.class));
                  }
                });
                customSuccessDialog.show();
              }
            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {
              loadDlg.dismiss();
              if (response.has("message") && response.getString("message") != null) {
                String message = response.getString("message");
                CustomToast.showMessage(OtpVerificationKycAcitvity.this, message);
              }
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(OtpVerificationKycAcitvity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(OtpVerificationKycAcitvity.this).sendBroadcast(intent);
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "3");
    LocalBroadcastManager.getInstance(OtpVerificationKycAcitvity.this).sendBroadcast(intent);
  }


}

