package in.msewa.ecarib.activity.shopping.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import in.msewa.custom.CustomToast;
import in.msewa.metadata.AvenuesParams;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.PQTelebuyCCAvenueWebViewActivity;
import in.msewa.ecarib.activity.shopping.PQTelebuyPaymentHolder;


public class PQChoosePaymentModeFragment extends Fragment {

    private View rootView;

    private ProgressDialog pDialog;
    private TextView tvPaymentDetails;
    private TextView tvDescriptions, tvDetails;
    private TextView tvLoadMoney;
    private Button btnProceedLoadMoney;

    private String loadMoneyAmount;

    private View focusView = null;
    private boolean cancel;

    private PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();

    public PQChoosePaymentModeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_pqtelebuy_payment_ccavenue, container, false);

        tvPaymentDetails = (TextView) rootView.findViewById(R.id.tvPaymentDetails);
        btnProceedLoadMoney = (Button) rootView.findViewById(R.id.btnProceedLoadMoney);

        btnProceedLoadMoney.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                proceedToPayment();
            }
        });

        PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();
        payTelebuy.createOrderId();
        tvPaymentDetails.setText(Html.fromHtml("<b>TOTAL AMOUNT TO BE PAID</b><br>" + payTelebuy.getAmount() + "<br>"
                + "<b>ORDER ID</b><br>" + payTelebuy.getOrderId() + "<br>"
                + "<b>FULL NAME</b><br>" + payTelebuy.getName() + "<br>"
                + "<b>ADDRESS 1</b><br>" + payTelebuy.getAddress() + "<br>"
                + "<b>CITY/TOWN</b><br>" + payTelebuy.getCity() + "<br>"
                + "<b>STATE</b><br>" + payTelebuy.getState() + "<br>"
                + "<b>PIN CODE</b><br>" + payTelebuy.getZip() + "<br>"
                + "<b>MOBILE NO.</b><br>" + payTelebuy.getTelephone()));


        return rootView;
    }

    public void proceedToPayment() {
        cancel = false;
        if (cancel) {
            focusView.requestFocus();
        } else {
            dialogBeforeContinue();
        }
    }


    private void dialogBeforeContinue() {
        String vOrderId = payTelebuy.getOrderId();
        String vRsaKeyUrl = PQTelebuyPaymentHolder.RSA_URL;
        if (/*selectedCardType!=null && selectedPaymentOption!=null && */!vOrderId.equals("") && !vRsaKeyUrl.equals("")) {
            Intent intent = new Intent(getActivity(), PQTelebuyCCAvenueWebViewActivity.class);
            intent.putExtra(AvenuesParams.ORDER_ID, payTelebuy.getOrderId());
            intent.putExtra(AvenuesParams.ACCESS_CODE, PQTelebuyPaymentHolder.ACCESS_CODE);
            intent.putExtra(AvenuesParams.MERCHANT_ID, PQTelebuyPaymentHolder.MERCHANT_ID);
            intent.putExtra(AvenuesParams.CURRENCY, PQTelebuyPaymentHolder.CURRENCY);
            intent.putExtra(AvenuesParams.AMOUNT, payTelebuy.getAmount());

            intent.putExtra(AvenuesParams.BILLING_NAME, payTelebuy.getName());
            intent.putExtra(AvenuesParams.BILLING_ADDRESS, payTelebuy.getAddress());
            intent.putExtra(AvenuesParams.BILLING_COUNTRY, "India");
            intent.putExtra(AvenuesParams.BILLING_STATE, payTelebuy.getState());
            intent.putExtra(AvenuesParams.BILLING_CITY, payTelebuy.getCity());
            intent.putExtra(AvenuesParams.BILLING_ZIP, payTelebuy.getZip());
            intent.putExtra(AvenuesParams.BILLING_TEL, payTelebuy.getTelephone());
            intent.putExtra(AvenuesParams.BILLING_EMAIL, payTelebuy.getEmailId());
            intent.putExtra(AvenuesParams.DELIVERY_NAME, payTelebuy.getName());
            intent.putExtra(AvenuesParams.DELIVERY_ADDRESS, payTelebuy.getAddress());
            intent.putExtra(AvenuesParams.DELIVERY_COUNTRY, "India");
            intent.putExtra(AvenuesParams.DELIVERY_STATE, payTelebuy.getState());
            intent.putExtra(AvenuesParams.DELIVERY_CITY, payTelebuy.getCity());
            intent.putExtra(AvenuesParams.DELIVERY_ZIP, payTelebuy.getZip());
            intent.putExtra(AvenuesParams.DELIVERY_TEL, payTelebuy.getTelephone());
            intent.putExtra(AvenuesParams.CARD_NUMBER, "");
            intent.putExtra(AvenuesParams.CVV, "");
            intent.putExtra(AvenuesParams.EXPIRY_YEAR, "");
            intent.putExtra(AvenuesParams.EXPIRY_MONTH, "");
            intent.putExtra(AvenuesParams.ISSUING_BANK, "");
            intent.putExtra(AvenuesParams.REDIRECT_URL, PQTelebuyPaymentHolder.REDIRECT_URL);
            intent.putExtra(AvenuesParams.CANCEL_URL, PQTelebuyPaymentHolder.CANCEL_URL);
            intent.putExtra(AvenuesParams.RSA_KEY_URL, PQTelebuyPaymentHolder.RSA_URL);
            startActivity(intent);
            getActivity().finish();
        } else {
            CustomToast.showMessage(getActivity(), "Amount/Currency/Access code/Merchant Id & RSA key Url are mandatory."); //More validations can be added as per requirement.
        }

    }

}
