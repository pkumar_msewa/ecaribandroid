package in.msewa.ecarib.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.ResultIPC;
import in.msewa.fragment.TicketFragment;
import in.msewa.fragment.packageFragment;
import in.msewa.model.ImagicaPackageModel;
import in.msewa.model.ImagicaTicketModel;
import in.msewa.model.TicketDetail;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.imagicaActivity.AddonsActivity;

/**
 * Created by Ksf on 3/27/2016.
 */
public class PackageTicketActivity extends AppCompatActivity {

    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    Double amount1, amount2;
    private CharSequence TitlesEnglish[];
    int NumbOfTabs = 2;
    private ArrayList<ImagicaPackageModel> imagicaPackageModels;
    private ArrayList<ImagicaTicketModel> imagicaTicketModels;
    private ArrayList<String> TitleList;
    private TextView tvcarttotalamount;
    private ArrayList<TicketDetail> imagicaTicketDetails;
    private int adsys;
    private UserModel user = UserModel.getInstance();
    private Button btnaddons;
    private JSONObject jsonRequest;
    private LoadingDialog loadDlg;
    private String dept_date;
    private int tot_date;
    private String visit_Date;
    private String destination;
    private String tag_json_obj = "lshdf";
    private int sys;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.fragment_imagica);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        loadDlg = new LoadingDialog(PackageTicketActivity.this);
        imagicaPackageModels = getIntent().getParcelableArrayListExtra("models");
        sys = getIntent().getIntExtra("ticket", 0);
        adsys = getIntent().getIntExtra("addons", 0);

        dept_date = getIntent().getStringExtra("departureDate");
        tot_date = getIntent().getIntExtra("totaldays", 1);
        visit_Date = getIntent().getStringExtra("visitDate");
        destination = getIntent().getStringExtra("destination");
        String amount = getIntent().getStringExtra("totalAmount");

        ResultIPC resultIPC = ResultIPC.get();
        imagicaTicketModels = (ArrayList<ImagicaTicketModel>) resultIPC.getLargeData(sys);

        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        TitlesEnglish = new CharSequence[]{"Tickets", "Packages"};
        tvcarttotalamount = (TextView) findViewById(R.id.tvcarttotalamount);

        btnaddons = (Button) findViewById(R.id.btnaddons);
//        if (imagicaPackageModels.size() == 0) {
//            TitlesEnglish = new CharSequence[]{"Tickets"};
//            mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), TitlesEnglish, NumbOfTabs));
//        } else {
        mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), TitlesEnglish, NumbOfTabs));
//        }

        mSlidingTabLayout.setupWithViewPager(mainPager);
//
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("Packages"));

        btnaddons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultIPC resultIPC = ResultIPC.get();
                resultIPC.getaddOnsModels(adsys);
                if (!tvcarttotalamount.getText().equals("0.0")) {

                    Intent intent = new Intent(PackageTicketActivity.this, AddonsActivity.class);
                    intent.putExtra("addon", adsys);
                    intent.putExtra("ticketProduct", product);
                    intent.putExtra("totaldays", 1);
                    intent.putExtra("departureDate", visit_Date);
                    intent.putExtra("destination", destination);
                    intent.putExtra("visitDate", visit_Date);
                    intent.putExtra("totalAmount", tvcarttotalamount.getText().toString());
                    finish();
                    startActivity(intent);
                } else {
                    CustomToast.showMessage(PackageTicketActivity.this, "Please enter all the details.");
                }
            }
        });
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {
        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }


        @Override
        public Fragment getItem(int position) {

            if (position == 1) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("package", imagicaPackageModels);
                bundle.putString("type", destination);
                packageFragment fragment = new packageFragment();
                fragment.setArguments(bundle);
                return fragment;
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt("ticket", sys);
                bundle.putInt("addons", adsys);
                bundle.putString("type", destination);
                TicketFragment ticketFragment = new TicketFragment();
                ticketFragment.setArguments(bundle);
                return ticketFragment;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TitlesEnglish[position];
        }

        @Override
        public int getCount() {
            return TitlesEnglish.length;
        }
    }


    private static double totalTicketAmount = 0.0, totalpackageAmount = 0.0;
    private String product, productPackage;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("updates");
            if (action.equals("1")) {
                String type = intent.getStringExtra("type");
                if (type.equals("package")) {
                    totalpackageAmount = Double.parseDouble(intent.getStringExtra("amount"));
                    productPackage = intent.getStringExtra("packageProduct");
                } else {
                    totalTicketAmount = Double.parseDouble(intent.getStringExtra("ticketAmount"));
                    product = intent.getStringExtra("ticketProduct");
                }
                tvcarttotalamount.setText(String.valueOf(totalpackageAmount + totalTicketAmount));
            }
        }
    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString("value", tvcarttotalamount.getText().toString());
    }
}
