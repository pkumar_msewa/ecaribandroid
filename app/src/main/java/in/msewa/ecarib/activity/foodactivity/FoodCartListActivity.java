package in.msewa.ecarib.activity.foodactivity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import in.msewa.adapter.FoodCartListAdapter;
import in.msewa.model.FoodCart;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 12/21/2016.
 */
public class FoodCartListActivity extends AppCompatActivity {

    private RecyclerView rvFoodCart;
    private FoodCart foodCart = FoodCart.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cart_food);
        rvFoodCart = (RecyclerView) findViewById(R.id.rvFoodCart);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
        rvFoodCart .addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        GridLayoutManager manager = new GridLayoutManager(FoodCartListActivity.this, 1);
        rvFoodCart.setLayoutManager(manager);
        rvFoodCart.setHasFixedSize(true);

        FoodCartListAdapter rcAdapter = new FoodCartListAdapter(FoodCartListActivity.this,foodCart.getProductsInCartArray());
        rvFoodCart.setAdapter(rcAdapter);

    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }


}
