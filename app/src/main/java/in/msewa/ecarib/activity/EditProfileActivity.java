package in.msewa.ecarib.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.permission.PermissionHandler;
import in.msewa.permission.Permissions;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.ImageLoadingUtils;
import in.msewa.util.MultipartRequest;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.Utility;


public class EditProfileActivity extends AppCompatActivity {

  private EditText etEditFirstName, etEditAddress, etEditEmail, etEditMobile, etEditLastName, etEditDob;
  private RadioButton rbEditMale, rbEditFeMale;
  private Button btnEditSubmit;
  private ImageView ivEditProfilePic;
  private String firstName, lastName, address, email, mobileNo, gender, dob;
  private LoadingDialog loadDlg;
  private TextInputLayout ilEditFName, ilEditLName, ilEditMobile, ilEditEmail, ilEditAddress, ilEditDob;
  private FloatingActionButton btnEditProfilePic;
  private UserModel session = UserModel.getInstance();

  private AlertDialog.Builder dialogPicture;
  public static final String IMAGE_DIRECTORY_NAME = "PayQwik";
  private final int CameraResult = 1;
  private final int GalleryResult = 2;
  private Uri fileUri;
  private String userImage;
  private boolean isImage = false;
  private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
  private ImageLoadingUtils utils;

  private Map<String, String> uploadHeaderParams;

  private String updatedImageUrl = null;

  private JSONObject jsonRequest;

  private Toolbar toolbar;
  private ImageButton ivBackBtn;

  private Bitmap btpUserImage;
  private String imageName = "";

  private boolean isChanged = false;

  //Volley Tag
  private String tag_json_obj = "json_user";
  private String userChoosenTask;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    utils = new ImageLoadingUtils(getApplicationContext());
    setContentView(R.layout.activity_edit_profile);
    loadDlg = new LoadingDialog(EditProfileActivity.this);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

    etEditFirstName = (EditText) findViewById(R.id.etEditFirstName);
    etEditAddress = (EditText) findViewById(R.id.etEditAddress);
    etEditEmail = (EditText) findViewById(R.id.etEditEmail);
    etEditMobile = (EditText) findViewById(R.id.etEditMobile);
    etEditLastName = (EditText) findViewById(R.id.etEditLastName);
    btnEditProfilePic = (FloatingActionButton) findViewById(R.id.btnEditProfilePic);
    etEditDob = (EditText) findViewById(R.id.etEditDob);

    rbEditMale = (RadioButton) findViewById(R.id.rbEditMale);
    rbEditFeMale = (RadioButton) findViewById(R.id.rbEditFeMale);


    if (session.emailIsActive.equals("Active")) {
      etEditEmail.setFocusable(false);
      etEditEmail.setEnabled(false);
    } else {
      etEditEmail.setFocusable(true);
    }


    etEditMobile.setFocusable(false);
    etEditMobile.setEnabled(false);

    etEditDob.setFocusable(false);
    etEditDob.setEnabled(false);
//        etEditDob.setFocusable(false);

    btnEditSubmit = (Button) findViewById(R.id.btnEditSubmit);
    ilEditFName = (TextInputLayout) findViewById(R.id.ilEditFName);
    ilEditLName = (TextInputLayout) findViewById(R.id.ilEditLName);
    ilEditMobile = (TextInputLayout) findViewById(R.id.ilEditMobile);
    ilEditEmail = (TextInputLayout) findViewById(R.id.ilEditEmail);
    ilEditAddress = (TextInputLayout) findViewById(R.id.ilEditAddress);
    ivEditProfilePic = (ImageView) findViewById(R.id.ivEditProfilePic);
    ilEditDob = (TextInputLayout) findViewById(R.id.ilEditDob);


    etEditFirstName.addTextChangedListener(new MyTextWatcher(etEditFirstName));
    etEditAddress.addTextChangedListener(new MyTextWatcher(etEditAddress));
    rbEditMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        isChanged = true;
      }
    });

    rbEditFeMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        isChanged = true;
      }
    });

    fillViews();


    btnEditSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        submitForm();
      }
    });


    btnEditProfilePic.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {

        selectImage();
//                CharSequence[] types = {"Gallery", "Camera"};
//                dialogPicture = new AlertDialog.Builder(EditProfileActivity.this, R.style.AppCompatAlertDialogStyle);
//                dialogPicture.setTitle("Select to change picture");
//                dialogPicture.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
//                dialogPicture.setItems(types, new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int position) {
//                        switch (position) {
//                            case 0:
//                                takePicfromgallery();
//                                dialog.dismiss();
//                                return;
//                            case 1:
//                                takepicfromcamera();
//                                dialog.dismiss();
//                                return;
//                        }
//                    }
//
//                    private void takePicfromgallery() {
//                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        intent.setType("image/*");
//                        startActivityForResult(intent, GalleryResult);
//                    }
//
//                    private void takepicfromcamera() {
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        fileUri = getOutputMediaFileUri();
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                        startActivityForResult(intent, CameraResult);
//                    }
//
//                    public Uri getOutputMediaFileUri() {
//                        return Uri.fromFile(getOutputMediaFile());
//                    }
//
//                    private File getOutputMediaFile() {
//                        File mediaStorageDir = new File(
//                                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
//
//                        if (!mediaStorageDir.exists()) {
//                            mediaStorageDir.mkdirs();
//
//                        }
//
//                        File mediaFile;
//                        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + ".jpg");
//
//                        return mediaFile;
//                    }
//
//                });
//
//                dialogPicture.show();

      }
    });

  }

  private void fillViews() {
    etEditLastName.setText(session.getUserLastName());
    etEditFirstName.setText(session.getUserFirstName());
    etEditMobile.setText(session.getUserMobileNo());
    etEditEmail.setText(session.getUserEmail());
    etEditAddress.setText(session.getUserAddress());
    etEditDob.setText(session.getUserDob());

    if (session.getUserGender() != null) {
      if (session.getUserGender().equals("M")) {
        rbEditMale.setChecked(true);
      } else if (session.getUserGender().equals("F")) {
        rbEditFeMale.setChecked(true);
      } else {
        rbEditFeMale.setChecked(false);
        rbEditMale.setChecked(false);
      }
    }
    AQuery aq = new AQuery(EditProfileActivity.this);
    aq.id(ivEditProfilePic).background(R.drawable.ic_user_avatar).image(ApiUrl.URL_IMAGE_MAIN + session.getUserImage(), true, true);

//        Picasso.with(getApplicationContext()).load(ApiUrl.URL_IMAGE_MAIN + session.getUserImage()).placeholder(R.drawable.ic_user_load).into(ivEditProfilePic);

//        if(session.getUserImage()!=null&&!session.getUserImage().isEmpty()){
//        }
//        else{
//            Picasso.with(getApplicationContext()).load(R.drawable.ic_user_load).into(ivEditProfilePic);
//        }

  }


  private void submitForm() {
    if (!validateFName()) {
      return;
    }

//        if (!validateAddress()) {
//            return;
//        }
    if (!validateEmail()) {
      return;
    }
//        if(!validateGender()){
//            return;
//        }

    firstName = etEditFirstName.getText().toString();
    lastName = etEditLastName.getText().toString();
    address = etEditAddress.getText().toString();
    dob = etEditDob.getText().toString();
    email = etEditEmail.getText().toString();

    if (rbEditMale.isChecked()) {
      gender = "M";
    } else if (rbEditFeMale.isChecked()) {
      gender = "F";
    } else {
      gender = "NA";
    }


    loadDlg.show();
    editProfileNew();


  }

  private boolean validateFName() {
    if (etEditFirstName.getText().toString().trim().isEmpty()) {
      ilEditFName.setError("Enter first name");
      requestFocus(ilEditFName);
      return false;
    } else {
      ilEditFName.setErrorEnabled(false);
    }
    return true;
  }

  private boolean validateLName() {
    if (etEditLastName.getText().toString().trim().isEmpty()) {
      ilEditLName.setError("Enter last name");
      requestFocus(etEditLastName);
      return false;
    } else {
      ilEditLName.setErrorEnabled(false);
    }
    return true;
  }

  private boolean validateAddress() {
    if (etEditAddress.getText().toString().trim().isEmpty()) {
      ilEditAddress.setError("Enter address");
      requestFocus(etEditAddress);
      return false;
    } else {
      ilEditAddress.setErrorEnabled(false);
    }
    return true;
  }

//    private boolean validateEmail() {
//        String email = etRegisterEmail.getText().toString().trim();
//        if (!email.isEmpty()) {
//            if (!isValidEmail(email)) {
//                ilRegisterEmail.setError("Enter valid email");
//                requestFocus(ilRegisterEmail);
//                return false;
//            }
//        } else {
//            ilRegisterEmail.setErrorEnabled(false);
//        }
//
//        return true;
//    }

  private boolean validateEmail() {
    if (etEditEmail.getText().toString().trim().isEmpty() || !isValidEmail(etEditEmail.getText().toString().trim())) {
      ilEditEmail.setError("Enter valid email");
      requestFocus(ilEditEmail);
      return false;
    } else {
      ilEditEmail.setErrorEnabled(false);
    }

    return true;
  }

  private boolean validateGender() {
    if (rbEditMale.isChecked() || rbEditFeMale.isChecked()) {
      return true;
    } else {
      CustomToast.showMessage(getApplicationContext(), "Please select your gender");
      return false;
    }

  }

  private static boolean isValidEmail(String email) {
    return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }


  private void requestFocus(View view) {
    if (view.requestFocus()) {
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
  }


  private void editProfileNew() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("firstName", firstName);
      jsonRequest.put("lastName", "");
      jsonRequest.put("address", address);
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("email", email);
      jsonRequest.put("gender", gender);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_EDIT_USER, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            Log.i("Edit Response", jsonObj.toString());
            String code = jsonObj.getString("code");
            String message = jsonObj.getString("message");
            loadDlg.dismiss();
            if (code != null & code.equals("S00")) {
              loadDlg.dismiss();
              sendRefresh(message);

            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {
              loadDlg.dismiss();
              Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  private void sendRefresh(String message) {
    if (!message.equals("Image Uploaded Successfully")) {
      Intent intent = new Intent("setting-change");
      intent.putExtra("updates", "2");
      LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
      CustomToast.showMessage(getApplicationContext(), message);

      Handler mHandler = new Handler();
      mHandler.postDelayed(updateTask, 2000);
    } else {
      CustomToast.showMessage(getApplicationContext(), message);
    }


  }

  private Runnable updateTask = new Runnable() {
    @Override
    public void run() {
      loadDlg.dismiss();
      finish();
    }
  };

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(EditProfileActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }


  @Override
  public void onBackPressed() {
    if (isChanged) {
      showExitDialog();
    } else {
      super.onBackPressed();
    }
  }

  private void showExitDialog() {
    android.support.v7.app.AlertDialog.Builder exitDialog =
      new android.support.v7.app.AlertDialog.Builder(EditProfileActivity.this, R.style.AppCompatAlertDialogStyle);
    exitDialog.setTitle("Do you really want to Exit without saving the changes?");
    exitDialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    exitDialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        finish();
      }
    });
    exitDialog.show();
  }

  private class MyTextWatcher implements TextWatcher {

    private View view;

    private MyTextWatcher(View view) {
      this.view = view;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {
      isChanged = true;
    }
  }

  private void selectImage() {
    final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

    AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
    builder.setTitle("Upload Profile Image!");
    builder.setItems(items, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int item) {
        boolean result = Utility.checkPermission(EditProfileActivity.this);

        if (items[item].equals("Take Photo")) {
          userChoosenTask = "Take Photo";
          if (result)
            cameraIntent();

        } else if (items[item].equals("Choose from Gallery")) {
          userChoosenTask = "Choose from Gallery";
          if (result)
            galleryIntent();

        } else if (items[item].equals("Cancel")) {
          dialog.dismiss();
        }
      }
    });
    builder.show();
  }

  private void galleryIntent() {
    if (Build.VERSION.SDK_INT >= 23) {
      Permissions.check(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0, null,
        new PermissionHandler() {
          @Override
          public void onGranted() {
            //do your task
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_PICK);//
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
          }
        });
//
    } else {
      Intent intent = new Intent();
      intent.setAction(Intent.ACTION_PICK);//
      intent.setType("image/*");
      startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


  }

  private void cameraIntent() {
    if (Build.VERSION.SDK_INT >= 23) {
      Permissions.check(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0, null,
        new PermissionHandler() {
          @Override
          public void onGranted() {
            //do your task
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
          }
        });
//
    } else {
      Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      startActivityForResult(intent, REQUEST_CAMERA);
    }

  }

  @RequiresApi(api = Build.VERSION_CODES.KITKAT)
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == SELECT_FILE)
        onSelectFromGalleryResult(data);
      else if (requestCode == REQUEST_CAMERA)
        onCaptureImageResult(data);
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.KITKAT)
  private void onCaptureImageResult(Intent data) {
    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    assert thumbnail != null;
    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
    Uri tempUri = getImageUri(EditProfileActivity.this, thumbnail);
    String filePath = getRealPathFromURI(tempUri);
    uploadImage(compressImage(filePath));

    ivEditProfilePic.setImageBitmap(thumbnail);

  }

  @TargetApi(Build.VERSION_CODES.KITKAT)
  @SuppressWarnings("deprecation")
  private void onSelectFromGalleryResult(Intent data) {

    Bitmap bm = null;
    if (data != null) {
      try {
        Uri selectedImageUri = data.getData();
        ivEditProfilePic.setImageBitmap(getCompressedBitmap(getPath(EditProfileActivity.this, selectedImageUri)));
        uploadImage(compressImage(getPath(EditProfileActivity.this, selectedImageUri)));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

  }

  public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
    int width = image.getWidth();
    int height = image.getHeight();

    float bitmapRatio = (float) width / (float) height;
    if (bitmapRatio > 1) {
      width = maxSize;
      height = (int) (width / bitmapRatio);
    } else {
      height = maxSize;
      width = (int) (height * bitmapRatio);
    }
    return Bitmap.createScaledBitmap(image, width, height, true);
  }

  @RequiresApi(api = Build.VERSION_CODES.KITKAT)
  public static String getPath(final Context context, final Uri uri) {

    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

    // DocumentProvider
    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
      // ExternalStorageProvider
      if (isExternalStorageDocument(uri)) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        final String type = split[0];

        if ("primary".equalsIgnoreCase(type)) {
          return Environment.getExternalStorageDirectory() + "/" + split[1];
        }

        // TODO handle non-primary volumes
      }
      // DownloadsProvider
      else if (isDownloadsDocument(uri)) {

        final String id = DocumentsContract.getDocumentId(uri);
        final Uri contentUri = ContentUris.withAppendedId(
          Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

        return getDataColumn(context, contentUri, null, null);
      }
      // MediaProvider
      else if (isMediaDocument(uri)) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        final String type = split[0];

        Uri contentUri = null;
        if ("image".equals(type)) {
          contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        } else if ("video".equals(type)) {
          contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else if ("audio".equals(type)) {
          contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        }

        final String selection = "_id=?";
        final String[] selectionArgs = new String[]{
          split[1]
        };

        return getDataColumn(context, contentUri, selection, selectionArgs);
      }
    }
    // MediaStore (and general)
    else if ("content".equalsIgnoreCase(uri.getScheme())) {
      return getDataColumn(context, uri, null, null);
    }
    // File
    else if ("file".equalsIgnoreCase(uri.getScheme())) {
      return uri.getPath();
    }

    return null;
  }

  public static String getDataColumn(Context context, Uri uri, String selection,
                                     String[] selectionArgs) {

    Cursor cursor = null;
    final String column = "_data";
    final String[] projection = {
      column
    };

    try {
      cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
        null);
      if (cursor != null && cursor.moveToFirst()) {
        final int column_index = cursor.getColumnIndexOrThrow(column);
        return cursor.getString(column_index);
      }
    } finally {
      if (cursor != null)
        cursor.close();
    }
    return null;
  }

  public static boolean isExternalStorageDocument(Uri uri) {
    return "com.android.externalstorage.documents".equals(uri.getAuthority());
  }


  public static boolean isDownloadsDocument(Uri uri) {
    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
  }

  public static boolean isMediaDocument(Uri uri) {
    return "com.android.providers.media.documents".equals(uri.getAuthority());
  }

  public Uri getImageUri(Context inContext, Bitmap inImage) {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
    String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
    return Uri.parse(path);
  }

  @RequiresApi(api = Build.VERSION_CODES.KITKAT)
  public String getRealPathFromURI(Uri uri) {
    try (Cursor cursor = getContentResolver().query(uri, null, null, null, null)) {
      if (cursor.moveToFirst()) {
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
      } else return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }


  public void uploadImage(String imagePath) {
    loadDlg.show();
    File file = new File(imagePath);
    uploadHeaderParams = new HashMap<>();
    uploadHeaderParams.put("sessionId", session.getUserSessionId());

    MultipartRequest mr = new MultipartRequest(ApiUrl.URL_UPLOAD_IMAGE, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError volleyError) {
        loadDlg.dismiss();
//                Picasso.with(getApplicationContext()).load(ApiUrl.URL_IMAGE_MAIN + session.getUserImage()).placeholder(R.mipmap.ic_user_load).into(ivUser);
        CustomToast.showMessage(EditProfileActivity.this, NetworkErrorHandler.getMessage(volleyError, EditProfileActivity.this));

      }
    },
      new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
          JSONObject jsonObj;
          try {
            jsonObj = new JSONObject(response);
            System.out.println("reee" + response);
            String code = jsonObj.getString("code");
            if (code != null & code.equals("S00")) {
              loadDlg.dismiss();
              sendRefresh("Image Uploaded Successfully");
//                                CustomToast.showMessage(EditProfileActivity.this, "Image Uploaded Successfully");
            } else {
              loadDlg.dismiss();
              CustomToast.showMessage(EditProfileActivity.this, jsonObj.getString("message"));
//                                Toast.makeText(getApplicationContext(), "Error occurred please try again", Toast.LENGTH_SHORT).show();
            }

          } catch (JSONException e) {
//                            Picasso.with(getApplicationContext()).load(ApiUrl.URL_IMAGE_MAIN + session.getUserImage()).placeholder(R.mipmap.ic_user_load).into(ivUser);
            CustomToast.showMessage(EditProfileActivity.this, "Exception Caused");
            e.printStackTrace();
          }


        }
      }, file, uploadHeaderParams, "profilePicture"
    );
    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    mr.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(mr);

  }

  public String compressImage(String filePath) {


    Bitmap scaledBitmap = null;
    BitmapFactory.Options options = new BitmapFactory.Options();

    options.inJustDecodeBounds = true;
    Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

    int actualHeight = options.outHeight;
    int actualWidth = options.outWidth;


    float maxHeight = 816.0f;
    float maxWidth = 612.0f;
    float imgRatio = actualWidth / actualHeight;
    float maxRatio = maxWidth / maxHeight;


    if (actualHeight > maxHeight || actualWidth > maxWidth) {
      if (imgRatio < maxRatio) {
        imgRatio = maxHeight / actualHeight;
        actualWidth = (int) (imgRatio * actualWidth);
        actualHeight = (int) maxHeight;
      } else if (imgRatio > maxRatio) {
        imgRatio = maxWidth / actualWidth;
        actualHeight = (int) (imgRatio * actualHeight);
        actualWidth = (int) maxWidth;
      } else {
        actualHeight = (int) maxHeight;
        actualWidth = (int) maxWidth;

      }
    }


    options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
    options.inJustDecodeBounds = false;
    options.inPurgeable = true;
    options.inInputShareable = true;
    options.inTempStorage = new byte[16 * 1024];

    try {
      bmp = BitmapFactory.decodeFile(filePath, options);
    } catch (OutOfMemoryError exception) {
      exception.printStackTrace();

    }
    try {
      scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
    } catch (OutOfMemoryError exception) {
      exception.printStackTrace();
    }

    float ratioX = actualWidth / (float) options.outWidth;
    float ratioY = actualHeight / (float) options.outHeight;
    float middleX = actualWidth / 2.0f;
    float middleY = actualHeight / 2.0f;

    Matrix scaleMatrix = new Matrix();
    scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

    Canvas canvas = new Canvas(scaledBitmap);
    canvas.setMatrix(scaleMatrix);
    canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

    ExifInterface exif;
    try {
      exif = new ExifInterface(filePath);

      int orientation = exif.getAttributeInt(
        ExifInterface.TAG_ORIENTATION, 0);
      Log.d("EXIF", "Exif: " + orientation);
      Matrix matrix = new Matrix();
      if (orientation == 6) {
        matrix.postRotate(90);
        Log.d("EXIF", "Exif: " + orientation);
      } else if (orientation == 3) {
        matrix.postRotate(180);
        Log.d("EXIF", "Exif: " + orientation);
      } else if (orientation == 8) {
        matrix.postRotate(270);
        Log.d("EXIF", "Exif: " + orientation);
      }
      scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
        scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
        true);
    } catch (IOException e) {
      e.printStackTrace();
    }

    FileOutputStream out = null;
    String filename = getFilename();
    try {
      out = new FileOutputStream(filename);

      scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    return filename;

  }

  public String getFilename() {
    File file = new File(Environment.getExternalStorageDirectory().getPath(), "PayQwik/Images");
    if (!file.exists()) {
      file.mkdirs();
    }
    return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");

  }

  public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {
      final int heightRatio = Math.round((float) height / (float) reqHeight);
      final int widthRatio = Math.round((float) width / (float) reqWidth);
      inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
    }
    final float totalPixels = width * height;
    final float totalReqPixelsCap = reqWidth * reqHeight * 2;
    while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
      inSampleSize++;
    }

    return inSampleSize;
  }

  //UPDATED!
  public String getPath(Uri uri) {
    String[] projection = {MediaStore.Images.Media.DATA};
    Cursor cursor = managedQuery(uri, projection, null, null, null);
    if (cursor != null) {
      //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
      //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
      int column_index = cursor
        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
      cursor.moveToFirst();
      return cursor.getString(column_index);
    } else return null;
  }

  public Bitmap getCompressedBitmap(String imagePath) {
    float maxHeight = 1920.0f;
    float maxWidth = 1080.0f;
    Bitmap scaledBitmap = null;
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

    int actualHeight = options.outHeight;
    int actualWidth = options.outWidth;
    float imgRatio = (float) actualWidth / (float) actualHeight;
    float maxRatio = maxWidth / maxHeight;

    if (actualHeight > maxHeight || actualWidth > maxWidth) {
      if (imgRatio < maxRatio) {
        imgRatio = maxHeight / actualHeight;
        actualWidth = (int) (imgRatio * actualWidth);
        actualHeight = (int) maxHeight;
      } else if (imgRatio > maxRatio) {
        imgRatio = maxWidth / actualWidth;
        actualHeight = (int) (imgRatio * actualHeight);
        actualWidth = (int) maxWidth;
      } else {
        actualHeight = (int) maxHeight;
        actualWidth = (int) maxWidth;

      }
    }

    options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
    options.inJustDecodeBounds = false;
    options.inDither = false;
    options.inPurgeable = true;
    options.inInputShareable = true;
    options.inTempStorage = new byte[16 * 1024];

    try {
      bmp = BitmapFactory.decodeFile(imagePath, options);
    } catch (OutOfMemoryError exception) {
      exception.printStackTrace();

    }
    try {
      scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
    } catch (OutOfMemoryError exception) {
      exception.printStackTrace();
    }

    float ratioX = actualWidth / (float) options.outWidth;
    float ratioY = actualHeight / (float) options.outHeight;
    float middleX = actualWidth / 2.0f;
    float middleY = actualHeight / 2.0f;

    Matrix scaleMatrix = new Matrix();
    scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

    Canvas canvas = new Canvas(scaledBitmap);
    canvas.setMatrix(scaleMatrix);
    canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

    ExifInterface exif = null;
    try {
      exif = new ExifInterface(imagePath);
      int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
      Matrix matrix = new Matrix();
      if (orientation == 6) {
        matrix.postRotate(90);
      } else if (orientation == 3) {
        matrix.postRotate(180);
      } else if (orientation == 8) {
        matrix.postRotate(270);
      }
      scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
    } catch (IOException e) {
      e.printStackTrace();
    }
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 85, out);

    byte[] byteArray = out.toByteArray();

    Bitmap updatedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

    return updatedBitmap;
  }


}
