package in.msewa.ecarib.activity.shopping.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.SliderLayout;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.PQCart;
import in.msewa.model.ShoppingModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveCartListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.LoginRegActivity;
import in.msewa.ecarib.activity.shopping.ShopAdapter;



/**
 * Created by Ksf on 4/6/2016.
 */
public class ShopProdDetailFragment extends Fragment implements  AddRemoveCartListner {
    LoadingDialog loadingDialog;
    private ShoppingModel shoppingModel;
    private Button addToCart, btn_shopping_details_cancel;
    private TextView shoppingTitle, shoppingprice, tvDescr;
    private ImageView ivproductImageMain, ivproductImage1, ivproductImage2, ivproductImage3, ivproductImage4;
    private YouTubePlayerView youTubeView;
    private YouTubePlayer ytp;
    private UserModel session = UserModel.getInstance();
    private PQCart pqCart = PQCart.getInstance();
    private String cartValue = "0";

    private String vCode;

    private RequestQueue rq;
    private SliderLayout mDemoSlider;
    private TextView tvshopping_product_selling_price;
    private ImageView ivToolBarback;
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_events";
    private Button btn_shopping_details_addTocart;
    private ShopAdapter shoppingAdapter;
    private Button btnGoCart;
    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_prod_detail, null);
        loadingDialog = new LoadingDialog(getActivity());
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            shoppingModel = bundle.getParcelable("PRODMODEL");
        }
//        shoppingModel = getIntent().getParcelableExtra("ShoppingData");
        ivproductImageMain = (ImageView) rootView.findViewById(R.id.ivproductImageMain);
        ivproductImage1 = (ImageView) rootView.findViewById(R.id.ivproductImage1);
        ivproductImage2 = (ImageView) rootView.findViewById(R.id.ivproductImage2);
        ivproductImage3 = (ImageView) rootView.findViewById(R.id.ivproductImage3);
        ivproductImage4 = (ImageView) rootView.findViewById(R.id.ivproductImage4);
        addToCart = (Button) rootView.findViewById(R.id.btn_shopping_details_addTocart);
        shoppingTitle = (TextView) rootView.findViewById(R.id.tvshopping_details_title);
        shoppingprice = (TextView) rootView.findViewById(R.id.tvshopping_product_price);
        tvshopping_product_selling_price = (TextView) rootView.findViewById(R.id.tvshopping_product_selling_price);
        tvDescr = (TextView) rootView.findViewById(R.id.tvDescr);
        btn_shopping_details_cancel = (Button) rootView.findViewById(R.id.btn_shopping_details_cancel);
        btnGoCart = (Button) rootView.findViewById(R.id.btnGoCart);
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("cart-clear"));


        btn_shopping_details_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        final AQuery aQuery = new AQuery(getActivity());
        String[] resultArray = shoppingModel.getpImage().trim().split("#");
        final String pImage1 = resultArray[0];
        aQuery.id(ivproductImage1).image(pImage1).background(R.drawable.squareborder_shop).getContext();
        final String pImage2 = resultArray[1];
        final String pImage3 = resultArray[2];
        final String pImage4 = resultArray[3];

        if(!pImage2.equals("@")){
            ivproductImage2.setVisibility(View.VISIBLE);
            aQuery.id(ivproductImage2).image(pImage2).background(R.drawable.squareborder_shop).getContext();
        }if(!pImage3.equals("@")){
            ivproductImage3.setVisibility(View.VISIBLE);
            aQuery.id(ivproductImage3).image(pImage3).background(R.drawable.squareborder_shop).getContext();
        }if(!pImage4.equals("@")){
            ivproductImage4.setVisibility(View.VISIBLE);
            aQuery.id(ivproductImage4).image(pImage4).background(R.drawable.squareborder_shop).getContext();
        }
        ivproductImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aQuery.id(ivproductImageMain).image(pImage1).background(R.drawable.squareborder_shop).getContext();
            }
        });
        ivproductImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aQuery.id(ivproductImageMain).image(pImage2).background(R.drawable.squareborder_shop).getContext();
            }
        });
        ivproductImage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aQuery.id(ivproductImageMain).image(pImage3).background(R.drawable.squareborder_shop).getContext();
            }
        });
        ivproductImage4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aQuery.id(ivproductImageMain).image(pImage4).background(R.drawable.squareborder_shop).getContext();
            }
        });
        aQuery.id(ivproductImageMain).image(pImage1).background(R.drawable.squareborder_shop).getContext();

        tvDescr.setText(shoppingModel.getpDesc());
        shoppingTitle.setText(shoppingModel.getpName());
        shoppingprice.setText(""+getString(R.string.rupease)  +" "+ shoppingModel.getpPrice());
        tvshopping_product_selling_price.setText("Brand - " + shoppingModel.getpBrand()+"");

        if (pqCart.isProductInCart(shoppingModel)) {
            addToCart.setText("Remove");
            addToCart.setBackgroundResource(R.drawable.bg_red_btn);

        } else {
            addToCart.setText("Add");
            addToCart.setBackgroundResource(R.drawable.bg_button_pressed);
        }

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.getIsValid() == 1) {
                    if (pqCart.isProductInCart(shoppingModel)) {
                        removeProduct(shoppingModel);
                        addToCart.setBackgroundResource(R.drawable.bg_button_pressed);
                    } else {
                        addProduct(shoppingModel);
                        addToCart.setBackgroundResource(R.drawable.bg_red_btn);
                    }
                } else {
                    Intent loginIntent = new Intent(getActivity(), LoginRegActivity.class);
                    startActivity(loginIntent);
                }
            }
        });

//        btnGoCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i("CARTVALUE", cartValue);
//                if (session.getIsValid() == 1) {
//                    if (cartValue.equals("0")) {
//                        CustomToast.showMessage(getActivity(), "You have no products in cart.");
//                    } else {
//                        Intent goToCartIntent = new Intent(getActivity(), TeleBuyPaymentActivity.class);
//                        startActivity(goToCartIntent);
//                    }
//                } else {
//                    Intent loginIntent = new Intent(getActivity(), LoginRegActivity.class);
//                    startActivity(loginIntent);
//                }
//            }
//        });

        return rootView;
    }


    private String getMessage() {
        String message = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "<p> <strike>" + getString(R.string.rupease) + shoppingModel.getpPrice() + "</strike> </p>\n" +
                "</body>\n" +
                "</html>";
        return message;
    }

//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getStringExtra("updates");
//            if (action.equals("1")) {
//                pqCart.clearCart();
////                ShoppingModel.delete(ShoppingModel.class);
////                shoppingArray.clear();
//                tvcart_point.setText("0");
////                shoppingAdapter.notifyDataSetChanged();
//
//            }else if(action.equals("2")){
//                cartValue= String.valueOf(pqCart.getProductsInCartArray().size());
//                tvcart_point.setText(cartValue);
//            }
//        }
//    };

    @Override
    public void onResume() {
        super.onResume();
        cartValue = String.valueOf(pqCart.getProductsInCart().size());
        Log.i("CARTONRESUME", cartValue);
//        tvcart_point.setText(cartValue);
        if (pqCart.isProductInCart(shoppingModel)) {
            addToCart.setText("Remove");
            addToCart.setBackgroundResource(R.drawable.bg_red_btn);

        } else {
            addToCart.setText("Add");
            addToCart.setBackgroundResource(R.drawable.bg_button_pressed);
        }
    }


    public void removeProduct(final ShoppingModel shoppingModel) {
        loadingDialog.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("productId", shoppingModel.getPid());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("CARTREMOVEONEURL", ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART);
            Log.i("CARTREMOVEONEREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("CARTREMOVEONERES", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        loadingDialog.dismiss();
                        if (code != null && code.equals("S00")) {
                            pqCart.addRemoveProductsInCart(shoppingModel, shoppingModel.getpQty());
                            Intent intent = new Intent("cart-clear");
                            intent.putExtra("updates", "2");
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                            addToCart.setText("Add");

                        } else {
                            loadingDialog.dismiss();
                            CustomToast.showMessage(getActivity(), "Item cannot be removed");
                        }

                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void addProduct(final ShoppingModel shoppingModel) {
        loadingDialog.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("email", session.getUserEmail());
            jsonRequest.put("firstName", session.getUserFirstName());
            jsonRequest.put("lastName", session.getUserFirstName());
            jsonRequest.put("country", "india");
            jsonRequest.put("productId", shoppingModel.getPid());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("CARTADDONEURL", ApiUrl.URL_SHOPPONG_ADD_ITEM_CART);
            Log.i("CARTADDONEREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_ADD_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("CARTADDONERES", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            pqCart.addRemoveProductsInCart(shoppingModel, shoppingModel.getpQty());
                            Intent intent = new Intent("cart-clear");
                            intent.putExtra("updates", "2");
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                            addToCart.setText("Remove");
                            loadingDialog.dismiss();
                        } else {
                            loadingDialog.dismiss();
                            CustomToast.showMessage(getActivity(), "Item cannot be added");
                        }

                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    public void taskCompleted(String response) {
        if (response.equals("Add")) {
            cartValue = String.valueOf(Integer.parseInt(cartValue) + 1);
//            tvcart_point.setText(cartValue);
            shoppingAdapter.notifyDataSetChanged();
        } else {
            cartValue = String.valueOf(Integer.parseInt(cartValue) - 1);
            shoppingAdapter.notifyDataSetChanged();
//            tvcart_point.setText(cartValue);
        }
    }
}
