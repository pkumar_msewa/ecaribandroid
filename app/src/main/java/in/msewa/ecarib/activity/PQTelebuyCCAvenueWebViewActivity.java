package in.msewa.ecarib.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import in.msewa.metadata.AvenuesParams;
import in.msewa.model.PQCart;
import in.msewa.model.PQTelebuyPaymentHolder;
import in.msewa.model.UserModel;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.PQStatusActivity;
import in.msewa.util.RSAUtility;
import in.msewa.util.ServiceUtility;
import in.msewa.ecarib.activity.shopping.ServiceHandler;

public class PQTelebuyCCAvenueWebViewActivity extends AppCompatActivity {

    private UserModel session = UserModel.getInstance();
    private PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();


    private ProgressDialog dialog;
    Intent mainIntent;
    String html, encVal;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_webview);
        mainIntent = getIntent();



        // Calling async task to get display content
        new RenderView().execute();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class RenderView extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            dialog = new ProgressDialog(PQTelebuyCCAvenueWebViewActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(AvenuesParams.ACCESS_CODE, PQTelebuyPaymentHolder.ACCESS_CODE));
            params.add(new BasicNameValuePair(AvenuesParams.AMOUNT, payTelebuy.getAmount()));
            params.add(new BasicNameValuePair(AvenuesParams.ORDER_ID, payTelebuy.getOrderId()));


            String vResponse = sh.makeServiceCallFormEncoded(PQTelebuyPaymentHolder.RSA_URL, ServiceHandler.POST, params);

            if (vResponse != null) {
            } else {
            }
            if (!ServiceUtility.chkNull(vResponse).equals("") && ServiceUtility.chkNull(vResponse).toString().indexOf("ERROR") == -1) {
                StringBuffer vEncVal = new StringBuffer("");
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CARD_NUMBER, ""));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.EXPIRY_YEAR, ""));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.EXPIRY_MONTH, ""));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CVV, ""));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, payTelebuy.getAmount()));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, PQTelebuyPaymentHolder.CURRENCY));
                encVal = RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), vResponse);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();

            @SuppressWarnings("unused")
            class MyJavaScriptInterface {
                @JavascriptInterface
                public void processHTML(String html) {
                    // process the html as needed by the app
                    // TODO Process the HTML
                    String status = null;
                    if (html.indexOf("Failure") != -1) {
                        status = "Transaction Declined!";
                    } else if (html.indexOf("Success") != -1) {
                        status = "Transaction Successful!";
                    } else if (html.indexOf("Aborted") != -1) {
                        status = "Transaction Cancelled!";
                    } else {
                        status = "Status Not Known!";
                    }


                    final Intent intent = new Intent(getApplicationContext(), PQStatusActivity.class);
                    intent.putExtra("transStatus", status);
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                PQCart cart = PQCart.getInstance();
                                cart.setCartUpdated(true);
                                startActivity(intent);
                                finish();
                            }
                        }
                    };
                    timer.start();
                }
            }

            final WebView webview = (WebView) findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(webview, url);
                    final WebView v = view;
                    if (url.indexOf("/ccavResponseHandler.aspx") != -1) {
                        webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                    }
                    Handler lHandler = new Handler();
                    lHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            v.scrollTo(0, v.getContentHeight());
                        }
                    }, 200);
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    // TODO Auto-generated method stub
                    super.onPageStarted(view, url, favicon);
                }
            });

			/* An instance of this class will be registered as a JavaScript interface */
            StringBuffer params = new StringBuffer();
            params.append(ServiceUtility.addToPostParams(AvenuesParams.ACCESS_CODE, PQTelebuyPaymentHolder.ACCESS_CODE));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_ID, PQTelebuyPaymentHolder.MERCHANT_ID));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.ORDER_ID, payTelebuy.getOrderId()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.REDIRECT_URL, PQTelebuyPaymentHolder.REDIRECT_URL));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.CANCEL_URL, PQTelebuyPaymentHolder.CANCEL_URL));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.LANGUAGE, "EN"));



            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_NAME, session.getUserFirstName() + " " + session.getUserLastName()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ADDRESS, payTelebuy.getAddress()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_CITY, payTelebuy.getCity()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_STATE, payTelebuy.getState()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ZIP, payTelebuy.getZip()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_COUNTRY, "India"));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_TEL, session.getUserMobileNo()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_EMAIL, (session.getUserEmail() == null)?"care@msewa.in":session.getUserEmail()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_NAME, session.getUserFirstName() + " " + session.getUserLastName()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ADDRESS, payTelebuy.getAddress()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_CITY, payTelebuy.getCity()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_STATE, payTelebuy.getState()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ZIP, payTelebuy.getZip()));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_COUNTRY, "India"));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_TEL, session.getUserMobileNo()));

            params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM1, "additional Info."));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM2, "additional Info."));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM3, "additional Info."));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM4, "additional Info."));

            try {
                if (encVal != null) {
                    params.append(ServiceUtility.addToPostParams(AvenuesParams.ENC_VAL, URLEncoder.encode(encVal, "UTF-8")));
                }
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            String vPostParams = params.substring(0, params.length() - 1);
            try {
                webview.postUrl(AvenuesParams.TRANS_URL, EncodingUtils.getBytes(vPostParams, "UTF-8"));
            } catch (Exception e) {
                showToast("Exception occured while opening webview.");
            }
        }
    }

    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        // TODO Dialog to ask if the user wants to cancel the payment
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setMessage("Are you sure you want to cancel the transaction?");
        alertBuilder.setCancelable(true);
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        alertBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // TODO Analytics
//		EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // TODO Analytics
//		EasyTracker.getInstance(this).activityStop(this);
    }
}
