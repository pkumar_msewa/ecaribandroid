package in.msewa.ecarib.activity.shopping;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.PQCart;
import in.msewa.model.ShoppingModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddRemoveCartListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.fragment.ShopProdDetailFragment;

/**
 * Created by Kashif-PC on 2/21/2017.
 */
public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.RecyclerViewHolders> {

  private List<ShoppingModel> shoppingArray;
  //    private Context context;
  private AppCompatActivity act;
  private LayoutInflater layoutInflater;
  private RecyclerView.ViewHolder viewHolder;
  private PQCart pqCart = PQCart.getInstance();
  private RequestQueue rq;
  private UserModel session = UserModel.getInstance();
  private AddRemoveCartListner adpListner;
  private Context context;
  private JsonObjectRequest postReq;
  private String tag_json_obj = "json_events";
  private LoadingDialog loadingDialog;
  private FragmentManager supportFragmentManager;


  public ShopAdapter(Context context, List<ShoppingModel> shoppingArray, AddRemoveCartListner adapterListner, FragmentManager supportFragmentManager) {
    this.shoppingArray = shoppingArray;
    this.adpListner = adapterListner;
    this.context = context;
    this.supportFragmentManager = supportFragmentManager;


    try {
      rq = Volley.newRequestQueue(context);
      layoutInflater = LayoutInflater.from(context);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  @Override
  public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

    View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_product_item, null);
    return new RecyclerViewHolders(layoutView);
  }

  @Override
  public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {
    holder.tvOldRate.setVisibility(View.GONE);
    holder.tvDiscAmount.setVisibility(View.GONE);
    holder.tvAmount.setText("Rs. " + shoppingArray.get(position).getpPrice() + "/-");
    holder.tvProductName.setText(shoppingArray.get(position).getpName());
    holder.tvDiscPerc.setText(shoppingArray.get(position).getpBrand());
    AQuery aQuery = new AQuery(context);
    String[] resultArray = shoppingArray.get(position).getpImage().trim().split("#");
    String pImage = resultArray[1];

    try {
      URL sourceUrl = new URL(pImage.replaceAll(" ", "%20"));
//      aQuery.id(holder.ivProdImage).image(sourceUrl.toString(),true,true).background(R.drawable.ic_loading_image).getContext();
              Picasso.with(context).load(sourceUrl.toString()).placeholder(R.drawable.ic_loading_image).resize(300,300).into(holder.ivProdImage);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }


    if (pqCart.isProductInCart(shoppingArray.get(position))) {
      holder.ivAddtoCart.setVisibility(View.GONE);
      holder.ivCartAdded.setVisibility(View.VISIBLE);

    } else {
      holder.ivAddtoCart.setVisibility(View.VISIBLE);
      holder.ivCartAdded.setVisibility(View.GONE);
    }
    loadingDialog = new LoadingDialog(context);
    holder.ivAddtoCart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        addProduct(shoppingArray.get(position), holder);
      }
    });
    holder.ivCartAdded.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        removeProduct(shoppingArray.get(position), holder);
      }
    });
    holder.llItem.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          ShopProdDetailFragment shopProdDetailFragment = new ShopProdDetailFragment();
          Bundle bundle = new Bundle();
          bundle.putParcelable("PRODMODEL", shoppingArray.get(position));
          shopProdDetailFragment.setArguments(bundle);
          supportFragmentManager.beginTransaction().replace(R.id.frameInShop, shopProdDetailFragment).addToBackStack("").commit();
        } catch (IndexOutOfBoundsException e) {
          e.printStackTrace();
        }
      }
    });

  }

  public Uri getUriFromUrl(String thisUrl) throws MalformedURLException {
    URL url = new URL(thisUrl);
    Uri.Builder builder = new Uri.Builder()
      .scheme(url.getProtocol())
      .authority(url.getAuthority())
      .appendPath(url.getPath());
    return builder.build();
  }

  @Override
  public int getItemCount() {
    return this.shoppingArray.size();
  }

  public void removeProduct(final ShoppingModel shoppingModel, final RecyclerViewHolders holder) {

    loadingDialog.show();
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobile", session.getUserMobileNo());
      jsonRequest.put("productId", shoppingModel.getPid());

    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("CARTREMOVEONEURL", ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART);
      Log.i("CARTREMOVEONEREQ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            Log.i("CARTREMOVEONERES", jsonObj.toString());
            String code = jsonObj.getString("code");

            loadingDialog.dismiss();
            if (code != null && code.equals("S00")) {
              pqCart.addRemoveProductsInCart(shoppingModel, shoppingModel.getpQty());
              Intent intent = new Intent("cart-clear");
              intent.putExtra("updates", "2");
              LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
              String cartValue = String.valueOf(pqCart.getProductsInCart().size());
              Log.i("CARTADAPSIZE", cartValue);
              holder.ivCartAdded.setVisibility(View.GONE);
              holder.ivAddtoCart.setVisibility(View.VISIBLE);
              loadingDialog.dismiss();


            } else {
              loadingDialog.dismiss();
              CustomToast.showMessage(context, "Item cannot be removed");
            }

          } catch (JSONException e) {
            loadingDialog.dismiss();
            e.printStackTrace();
            Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 120000;

      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

//    public void removeProduct(final ShoppingModel shopingModel) {
//        loadingDialog.show();
//        JSONObject jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobilenumber", session.getUserMobileNo());
//            jsonRequest.put("productDetailid", shopingModel.getPid());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("CARTREMOVEONEURL", ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART);
//            Log.i("CARTREMOVEONEREQ", jsonRequest.toString());
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject jsonObj) {
//                    try {
//                        Log.i("CARTREMOVEONERES", jsonObj.toString());
//                        String code = jsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            long qty = jsonObj.getLong("cartSize");
//                            pqCart.addRemoveProductsInCart(shopingModel, qty);
//                            adpListner.taskCompleted("Remove");
////                            viewHolder.btnShoppingRemove.setText("Add");
//                            loadingDialog.dismiss();
//                            notifyDataSetChanged();
//                        }
//
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        e.printStackTrace();
//                        Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
//    public void addProduct(final ShoppingModel shopingModel) {
//        loadingDialog.show();
//        JSONObject jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobilenumber", session.getUserMobileNo());
//            jsonRequest.put("productDetailid", shopingModel.getPid());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("CARTADDONEURL", ApiUrl.URL_SHOPPONG_ADD_ITEM_CART);
//            Log.i("CARTADDONEREQ", jsonRequest.toString());
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_ADD_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject jsonObj) {
//                    try {
//                        Log.i("CARTADDONERES", jsonObj.toString());
//                        String code = jsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            long qty = jsonObj.getLong("cartSize");
//                            pqCart.addRemoveProductsInCart(shopingModel, qty);
////                            viewHolder.btnShoppingRemove.setText("Remove");
//                            adpListner.taskCompleted("Add");
//                            loadingDialog.dismiss();
//                            notifyDataSetChanged();
//                        }
//
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        e.printStackTrace();
//                        Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }

  public void addProduct(final ShoppingModel shoppingModel, final RecyclerViewHolders holder) {
    loadingDialog.show();
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobile", session.getUserMobileNo());
      jsonRequest.put("email", session.getUserEmail());
      jsonRequest.put("firstName", session.getUserFirstName());
      jsonRequest.put("lastName", session.getUserFirstName());
      jsonRequest.put("country", "india");
      jsonRequest.put("productId", shoppingModel.getPid());

    } catch (JSONException e) {
      e.printStackTrace();
    }

    if (jsonRequest != null) {
      Log.i("CARTADDONEURL", ApiUrl.URL_SHOPPONG_ADD_ITEM_CART);
      Log.i("CARTADDONEREQ", jsonRequest.toString());
      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_ADD_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            Log.i("CARTADDONERES", jsonObj.toString());
            String code = jsonObj.getString("code");

            if (code != null && code.equals("S00")) {
              pqCart.addRemoveProductsInCart(shoppingModel, shoppingModel.getpQty());
              Intent intent = new Intent("cart-clear");
              intent.putExtra("updates", "2");
              LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
              String cartValue = String.valueOf(pqCart.getProductsInCart().size());
              Log.i("CARTADAPSIZE", cartValue);
              holder.ivAddtoCart.setVisibility(View.GONE);
              holder.ivCartAdded.setVisibility(View.VISIBLE);
              loadingDialog.dismiss();
            } else {
              loadingDialog.dismiss();
              CustomToast.showMessage(context, "Item cannot be added");
            }

          } catch (JSONException e) {
            loadingDialog.dismiss();
            e.printStackTrace();
            Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 120000;

      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public class RecyclerViewHolders extends RecyclerView.ViewHolder {
    public LinearLayout llItem;
    public ImageView ivProdImage;
    public ImageView ivAddtoWishlist;
    public ImageView ivAddtoCart;
    public ImageView ivCartAdded;
    public TextView tvProductName;
    public TextView tvOldRate;
    public TextView tvDiscAmount;
    public TextView tvAmount;
    public TextView tvDiscPerc;


    public RecyclerViewHolders(View itemView) {
      super(itemView);
      llItem = (LinearLayout) itemView.findViewById(R.id.llItem);
      ivProdImage = (ImageView) itemView.findViewById(R.id.ivProdImage);
      ivAddtoWishlist = (ImageView) itemView.findViewById(R.id.ivAddtoWishlist);
      ivAddtoCart = (ImageView) itemView.findViewById(R.id.ivAddtoCart);
      ivCartAdded = (ImageView) itemView.findViewById(R.id.ivCartAdded);
      tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
      tvOldRate = (TextView) itemView.findViewById(R.id.tvOldRate);
      tvDiscAmount = (TextView) itemView.findViewById(R.id.tvDiscAmount);
      tvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
      tvDiscPerc = (TextView) itemView.findViewById(R.id.tvDiscPerc);

    }

  }


}
