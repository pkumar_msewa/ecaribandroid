package in.msewa.ecarib.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.ecarib.activity.useractivity.LoginActivity;
import in.msewa.metadata.ApiUrl;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import me.philio.pinentry.PinEntryView;


/**
 * Created by Azhar on 29/10/2018.
 */
public class OtpVerificationActivity extends AppCompatActivity {

  private LoadingDialog loadDlg;
  private String userMobileNo;
  private String otpCode;
  private PinEntryView etVerifyPin;
  private TextView timmer, tvSender, tvResend;
  private ImageView ivBtn,ivLogo;
  private int count = 0;
  private JSONObject jsonRequest;
  //Volley Tag
  private String tag_json_obj = "json_user";

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_verify_mob);
    loadDlg = new LoadingDialog(this);
    userMobileNo = getIntent().getStringExtra("userMobileNo");
    LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));


    tvResend = (TextView) findViewById(R.id.tvResend);
    tvSender = (TextView) findViewById(R.id.tvSender);
    etVerifyPin = findViewById(R.id.etVerifyPin);
    timmer = findViewById(R.id.timmer);
    ivBtn=(ImageView)findViewById(R.id.ivBtn);
    ivLogo=(ImageView)findViewById(R.id.ivLogo);

    etVerifyPin = (PinEntryView) findViewById(R.id.etVerifyPin);

    Glide.with(this)
            .load(R.drawable.ic_top_banner)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(ivLogo);
    startTimer();
    tvSender.setText("OTP Sent to "+userMobileNo);
    etVerifyPin.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() == 6) {
          verifyOTP();
        }

      }
    });

    tvResend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        count++;
        if (count != 3) {
          loadDlg.show();
          resendOTP();
        } else {
          tvResend.setClickable(false);
          tvResend.setBackgroundResource(R.drawable.bg_button_gray_pressed);
        }
      }
    });
  }

  private void startTimer(){
    CountDownTimer countDownTimer = new CountDownTimer(120000, 1000) {
      @Override
      public void onTick(long millisUntilFinished) {
        long second = (millisUntilFinished / 1000) % 60;
        long minute = (millisUntilFinished / (1000 * 60)) % 60;
        long hour = (millisUntilFinished / (1000 * 60 * 60)) % 24;

        String time = String.format("%02d:%02d", minute, second);
        timmer.setText(time);
      }

      @Override
      public void onFinish() {
        timmer.setVisibility(View.GONE);
        tvResend.setVisibility(View.VISIBLE);
      }
    };
    countDownTimer.start();
  }

  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    super.onDestroy();
  }

  BroadcastReceiver broadcastReceiver =  new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      Bundle b = intent.getExtras();
      if(b!=null){
        String message = b.getString("message");
        Log.i("BroadCast",message);
        message = message.replaceAll("\\.","");
        Log.i("BroadCast",message);
        timmer.setVisibility(View.GONE);
        tvResend.setVisibility(View.VISIBLE);
        otpCode = message;
        etVerifyPin.setText(message);
        verifyOTP();

      }

    }
  };

  private void resendOTP() {
    startTimer();
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", userMobileNo);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("ResendOTP Request", jsonRequest.toString());
      Log.i("ResendOTP Request", ApiUrl.URL_RESEND_OTP);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RESEND_OTP, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          timmer.setVisibility(View.GONE);
          tvResend.setVisibility(View.VISIBLE);
          Log.i("ResendOTP Request", jsonObj.toString());
          loadDlg.dismiss();
          try {
            String code = jsonObj.getString("code");
            String detail = jsonObj.getString("details");
            if (code != null & code.equals("S00")) {
              CustomToast.showMessage(OtpVerificationActivity.this, detail);
              if (!etVerifyPin.getText().toString().isEmpty()) {
                etVerifyPin.setText("");
              }
            } else {
              Toast.makeText(OtpVerificationActivity.this, detail, Toast.LENGTH_SHORT).show();

            }
          } catch (JSONException e) {
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          timmer.setVisibility(View.GONE);
          tvResend.setVisibility(View.VISIBLE);
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
          error.printStackTrace();
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  private void verifyOTP() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("mobileNumber", userMobileNo);
      jsonRequest.put("key", etVerifyPin.getText().toString());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("verifyOTP Request", jsonRequest.toString());
      Log.i("verifyOTP URL", ApiUrl.URL_VERIFY_PHONE);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VERIFY_PHONE, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          Log.i("verifyOTP Response", response.toString());
          loadDlg.dismiss();
          try {
            String level = response.getString("status");
            String code = response.getString("code");
            String msg = response.getString("message");

            if (code != null && code.equals("S00")) {
              showLoginDialog();
            } else {
              CustomToast.showMessage(OtpVerificationActivity.this, msg);
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  public void showLoginDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(OtpVerificationActivity.this, R.string.dialog_title2, Html.fromHtml(generateMessage()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        LocalBroadcastManager.getInstance(OtpVerificationActivity.this).unregisterReceiver(broadcastReceiver);
        Intent otpIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(otpIntent);
        finish();

      }
    });

    builder.show();
  }

  public String generateMessage() {
    String source = "<b><font color=#000000> Registration Successful.</font></b>" +
      "<br><br><b><font color=#ff0000> Please Login to use Ecarib. </font></b><br></br>";
    return source;
  }


}
