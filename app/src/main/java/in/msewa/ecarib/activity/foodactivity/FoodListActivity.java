package in.msewa.ecarib.activity.foodactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.FoodListAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadMoreGridView;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.FoodCart;
import in.msewa.model.FoodModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by Kashif-PC on 12/9/2016.
 */
public class FoodListActivity extends AppCompatActivity {
  private LoadMoreGridView gvFood;
  private FoodListAdapter foodListAdapter;
  private ProgressBar pbFoodList;
  private String tag_json_obj = "json_zepo";
  private ArrayList<FoodModel> foodArray;

  private FoodCart foodCart = FoodCart.getInstance();

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_food_list);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

    LocalBroadcastManager.getInstance(this).registerReceiver(mCartReceiver, new IntentFilter("food-cart-changed"));

    foodArray = new ArrayList<>();
    gvFood = (LoadMoreGridView) findViewById(R.id.gvFood);
    pbFoodList = (ProgressBar) findViewById(R.id.pbFoodList);
    gvFood.setVisibility(View.GONE);
    pbFoodList.setVisibility(View.VISIBLE);
    loadProducts();

    gvFood.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent foodDetailIntent = new Intent(FoodListActivity.this, FoodDetailActivity.class);
        foodDetailIntent.putExtra("FoodModel", foodArray.get(i));
        startActivity(foodDetailIntent);
      }
    });

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.shopping_menu, menu);
    MenuItem menuItem = menu.findItem(R.id.menuCart);
    menuItem.setIcon(buildCounterDrawable(foodCart.getProductsInCartArray().size(), R.drawable.shopping_cart));
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menuCart) {
      startActivity(new Intent(FoodListActivity.this, FoodCartListActivity.class));

    }
    return super.onOptionsItemSelected(item);
  }

  private BroadcastReceiver mCartReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      invalidateOptionsMenu();

    }
  };

  private Drawable buildCounterDrawable(int count, int backgroundImageId) {
    LayoutInflater inflater = LayoutInflater.from(this);
    View view = inflater.inflate(R.layout.layout_cart_icon, null);
    view.setBackgroundResource(backgroundImageId);

    if (count == 0) {
      View counterTextPanel = view.findViewById(R.id.cartCount);
      counterTextPanel.setVisibility(View.GONE);
    } else {
      TextView textView = (TextView) view.findViewById(R.id.cartCount);
      textView.setText("" + count);
    }

    view.measure(
      View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
      View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
    view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

    view.setDrawingCacheEnabled(true);
    view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
    view.setDrawingCacheEnabled(false);

    return new BitmapDrawable(getResources(), bitmap);
  }

  private void loadProducts() {
    StringRequest postReq = new StringRequest(Request.Method.GET, ApiUrl.URL_ZEPO_FETCH_PRODUCTS + "products?page=1&pageSize=10", new Response.Listener<String>() {
      @Override
      public void onResponse(String res) {
        try {

          JSONObject response = new JSONObject(res);
          JSONArray foodJsonArray = response.getJSONArray("products");
          for (int i = 0; i < foodJsonArray.length(); i++) {
            JSONObject c = foodJsonArray.getJSONObject(i);
            long pId = c.getLong("productId");
            long storeId = c.getLong("storeId");
            String pName = c.getString("name");
            String pCode = c.getString("code");
            String pRetailPrice = c.getString("retailPrice");
            String pApplicablePrice = c.getString("applicablePrice");
            String pShippingPrice = c.getString("shippingCharges");
            String pThumb = c.getString("productThumbImageUrl");

            boolean isDiscount = c.getBoolean("isDiscountApplied");
            int pDiscountPrice = c.getInt("percentageDiscount");

            boolean isCod = c.getBoolean("isCODAvailable");
            String pDetails = c.getString("description");

            ArrayList<String> pImageUrls = new ArrayList<>();

            JSONArray foodPicJsonArray = c.getJSONArray("productImages");
            for (int j = 0; j < foodPicJsonArray.length(); j++) {
              JSONObject d = foodPicJsonArray.getJSONObject(j);
              JSONObject innerImage = d.getJSONObject("productImageUrl");
              String imageLocal = innerImage.getString("big");
              pImageUrls.add(imageLocal);
            }

            FoodModel myCouponsModel = new FoodModel(pId, storeId, pName, pCode, pRetailPrice, pApplicablePrice, pShippingPrice, pThumb, isDiscount, pDiscountPrice, isCod, pDetails, pImageUrls);
            foodArray.add(myCouponsModel);
          }

          foodListAdapter = new FoodListAdapter(FoodListActivity.this, foodArray);
          gvFood.setAdapter(foodListAdapter);
          gvFood.setVisibility(View.VISIBLE);
          pbFoodList.setVisibility(View.GONE);
        } catch (JSONException e) {
          CustomToast.showMessage(FoodListActivity.this, getResources().getString(R.string.server_exception2));
          e.printStackTrace();
        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        CustomToast.showMessage(FoodListActivity.this, getResources().getString(R.string.server_exception));
      }
    }) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("Authorization", "ZEPO D4134D6E225CF52AE2896D8F2DC99:MjM1MDgzMGFlYzk3YjFlY2M0MmQ3NmE1Mjc1ZWE2NDQ0ZTlhZDJhZA==");
        return map;
      }

    };
    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    postReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

  }

  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mCartReceiver);
    super.onDestroy();
  }
}
