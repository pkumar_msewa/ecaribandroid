package in.msewa.ecarib.activity.imagicaActivity;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.ResultIPC;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.AddOnsModel;
import in.msewa.model.ImagicaPackageModel;
import in.msewa.model.ImagicaTicketModel;
import in.msewa.model.TicketDetail;
import in.msewa.model.UserModel;
import in.msewa.util.CheckLog;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.PayingDetailsValidation;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.PackageTicketActivity;

public class ImagicaList extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private Button btnThemePark, btnWaterPark;
    private MaterialEditText etThemeParkDate, etWaterParkDate;
    private View focusView = null;
    private JsonObjectRequest postReq;
    private LoadingDialog loadingDialog;

    private Spinner spAdult, spChild, spSnrCtzn, spStudent;
    private EditText etVisitDate, etDeprTDate;
    private UserModel getSession = UserModel.getInstance();
    private ArrayList<String> alNumbers;
    private ArrayList<String> alPackages;


    private boolean cancel;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagica_list);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("imagica-booking-done"));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        loadDlg = new LoadingDialog(ImagicaList.this);
        spAdult = (Spinner) findViewById(R.id.spAdult);
        spChild = (Spinner) findViewById(R.id.spChild);
        spSnrCtzn = (Spinner) findViewById(R.id.spSnrCtzn);
        spStudent = (Spinner) findViewById(R.id.spStudent);
        etWaterParkDate = (MaterialEditText) findViewById(R.id.etWaterParkDate);
        etVisitDate = (MaterialEditText) findViewById(R.id.etFDate);
        etDeprTDate = (MaterialEditText) findViewById(R.id.etTDate);
        btnWaterPark = (Button) findViewById(R.id.btnWaterPark);
        btnThemePark = (Button) findViewById(R.id.btnThemePark);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);

        alNumbers = new ArrayList<>();
        for (int i = 0; i < 10; i++) {

            alNumbers.add(String.valueOf(i));

        }
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(ImagicaList.this, android.R.layout.select_dialog_item, alNumbers);
        spAdult.setAdapter(stringArrayAdapter);
        spChild.setAdapter(stringArrayAdapter);
        spSnrCtzn.setAdapter(stringArrayAdapter);
        spStudent.setAdapter(stringArrayAdapter);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        etVisitDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (etVisitDate.getText().toString() != null && !etVisitDate.getText().toString().isEmpty()) {
                    String[] dateSplit = etVisitDate.getText().toString().split("-");

                    int mYear = Integer.valueOf(dateSplit[2]);
                    int mMonth = Integer.valueOf(dateSplit[1]) - 1;
                    int mDay = Integer.valueOf(dateSplit[0]);

                    DatePickerDialog dialog = new DatePickerDialog(ImagicaList.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etVisitDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));

                        }
                    }, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setCustomTitle(null);
                    dialog.setTitle("Select Arrival Date");
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(ImagicaList.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etVisitDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setTitle("Select Arrival Date");
                    dialog.setCustomTitle(null);
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                }

            }
        });
        etDeprTDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (etDeprTDate.getText().toString() != null && !etDeprTDate.getText().toString().isEmpty()) {
                    String[] dateSplit = etDeprTDate.getText().toString().split("-");

                    int mYear = Integer.valueOf(dateSplit[2]);
                    int mMonth = Integer.valueOf(dateSplit[1]) - 1;
                    int mDay = Integer.valueOf(dateSplit[0]);

                    DatePickerDialog dialog = new DatePickerDialog(ImagicaList.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etDeprTDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));

                        }
                    }, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setCustomTitle(null);
                    dialog.setTitle("Select Arrival Date");
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(ImagicaList.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etDeprTDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setTitle("Select Arrival Date");
                    dialog.setCustomTitle(null);
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                }

            }
        });

        etWaterParkDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (etWaterParkDate.getText().toString() != null && !etWaterParkDate.getText().toString().isEmpty()) {
                    String[] dateSplit = etWaterParkDate.getText().toString().split("-");

                    int mYear = Integer.valueOf(dateSplit[2]);
                    int mMonth = Integer.valueOf(dateSplit[1]) - 1;
                    int mDay = Integer.valueOf(dateSplit[0]);

                    DatePickerDialog dialog = new DatePickerDialog(ImagicaList.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etWaterParkDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));

                        }
                    }, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setCustomTitle(null);
                    dialog.setTitle("Select Arrival Date");
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(ImagicaList.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etWaterParkDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setTitle("Select Arrival Date");
                    dialog.setCustomTitle(null);
                    dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                    dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();
                }

            }
        });
        btnThemePark.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
        btnWaterPark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etWaterParkDate.getText().toString() != null && !etWaterParkDate.getText().toString().isEmpty()) {
                    attemptSearchTicket("WATER_PARK");
                }else {
                    etWaterParkDate.setError("First select your vist date");
                }

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnThemePark:
                attemptSearch("theme");
                break;
            case R.id.btnWaterPark:
                attemptSearch("water");
                break;
        }
    }

    private boolean VisitDate() {
        String VisitDate = etVisitDate.getText().toString().trim();
        if (etVisitDate != null && !etVisitDate.getText().toString().isEmpty()) {
            return true;
        } else {
            etVisitDate.setError("First select your vist date");
        }

        return false;
    }

    private boolean DepartureDate() {
        String DepartureDate = etDeprTDate.getText().toString().trim();
        if (etDeprTDate != null && !etDeprTDate.getText().toString().isEmpty()) {
            return true;
        } else {
            etDeprTDate.setError("First select your Departure date");
        }

        return false;
    }

    private boolean ValAdult() {
        String ValAdult = spAdult.getSelectedItem().toString();
        if (spAdult != null && !spAdult.getSelectedItem().toString().isEmpty()) {
            return true;
        } else {
            Toast.makeText(getApplicationContext(), "choose at least 1", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    private void attemptSearch(String type) {
        if (type.equals("theme")) {
            etVisitDate.setError(null);
            cancel = false;
            checVisitDate(etVisitDate.getText().toString());
            if (cancel) {
                focusView.requestFocus();
            } else {
                searchTickets(etVisitDate.getText().toString(), "theme");
            }
        } else {
            etWaterParkDate.setError(null);
            cancel = false;
            checkWaterDate(etWaterParkDate.getText().toString());
            if (cancel) {
                focusView.requestFocus();
            } else {
                searchTickets(etWaterParkDate.getText().toString(), "water");
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {

        if (!VisitDate()) {
            return;
        }
        attemptSearchTicket("THE");
    }

    public void attemptSearchTicket(final String water_park) {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();

        try {
            if (water_park.equalsIgnoreCase("WATER_PARK")) {
                jsonRequest.put("adult", 1);
                jsonRequest.put("child", 0);
                jsonRequest.put("senior", 0);
                jsonRequest.put("college", 0);
                jsonRequest.put("totalDays", 1);
                jsonRequest.put("dateVisit", etWaterParkDate.getText().toString());
                jsonRequest.put("dateDeparture", etWaterParkDate.getText().toString());
                jsonRequest.put("destination", water_park);
                jsonRequest.put("sessionId", getSession.getUserSessionId());
            } else {
                jsonRequest.put("sessionId", getSession.getUserSessionId());
                jsonRequest.put("adult", 1);
                jsonRequest.put("child", 0);
                jsonRequest.put("senior", 0);
                jsonRequest.put("college", 0);
                jsonRequest.put("totalDays", 1);
                jsonRequest.put("dateVisit", etVisitDate.getText().toString());
                jsonRequest.put("dateDeparture", etVisitDate.getText().toString());
                jsonRequest.put("destination", "THEME_PARK");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("THEMEPARKTICKETLIST", ApiUrl.URL_IMAGICA_THEME_PARK_LIST);
            Log.i("THEMETICKETREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_IMAGICA_THEME_PARK_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject hb) {
                    try {
                        Log.i("THEMEPARKRESPONSE", hb.toString());
                        boolean success = hb.getBoolean("success");
                        String msg = hb.getString("message");
                        loadDlg.dismiss();
                        if (success) {
                            JSONArray jsonArray = hb.getJSONArray("packages");
                            alPackages = new ArrayList<>();
                            ArrayList<ImagicaPackageModel> imagicaPackageModels = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                String pimageURL = jsonArray.getJSONObject(i).getString("imageUrl");
                                String pdescription = jsonArray.getJSONObject(i).getString("description");
                                String ptitle = jsonArray.getJSONObject(i).getString("title");
                                String pquantity = jsonArray.getJSONObject(i).getString("quantity");
                                String pcostPer = jsonArray.getJSONObject(i).getString("costPer");
                                String pcostTotal = jsonArray.getJSONObject(i).getString("costTotal");
                                String pcurrency = jsonArray.getJSONObject(i).getString("currency");
                                String pproductID = jsonArray.getJSONObject(i).getString("productId");
                                Boolean carExists = false;
                                Boolean busExists = false;
                                String passengerType = "0", tdquantity = "0";
                                if (!jsonArray.getJSONObject(i).isNull("travelDetail")) {
                                    carExists = jsonArray.getJSONObject(i).getJSONObject("travelDetail").getBoolean("carExists");
                                    busExists = jsonArray.getJSONObject(i).getJSONObject("travelDetail").getBoolean("busExists");
                                    passengerType = jsonArray.getJSONObject(i).getJSONObject("ticketDetail").getString("passengerType");
                                    tdquantity = jsonArray.getJSONObject(i).getJSONObject("ticketDetail").getString("quantity");
                                }
                                String bdquantity = "0";
                                String bdaddOneName = "0";
                                String bdaddOnId = "0";
                                if (!jsonArray.getJSONObject(i).isNull("brunchDetail")) {
                                    bdquantity = jsonArray.getJSONObject(i).getJSONObject("brunchDetail").getString("quantity");
                                    bdaddOneName = jsonArray.getJSONObject(i).getJSONObject("brunchDetail").getString("addOneName");
                                    bdaddOnId = jsonArray.getJSONObject(i).getJSONObject("brunchDetail").getString("addOnId");
                                }
                                String cdquantity, cdaddOnId, cdaddOnName;
                                if (!jsonArray.getJSONObject(i).isNull("carAddonDetail")) {
                                    cdquantity = String.valueOf(jsonArray.getJSONObject(i).getJSONObject("carAddonDetail").getInt("quantity"));
                                    cdaddOnId = jsonArray.getJSONObject(i).getJSONObject("carAddonDetail").getString("addOnId");
                                    cdaddOnName = jsonArray.getJSONObject(i).getJSONObject("carAddonDetail").getString("addOnName");
                                } else {
                                    cdquantity = "0";
                                    cdaddOnId = "0";
                                    cdaddOnName = "0";
                                }

                                ArrayList<ImagicaPackageModel.TaxDetailList> taxdetailses = new ArrayList<>();
                                JSONArray taxDetailList = jsonArray.getJSONObject(i).getJSONArray("taxDetailList");

                                for (int j = 0; j < taxDetailList.length(); j++) {
                                    String Name = taxDetailList.getJSONObject(j).getString("name");
                                    String PricePerQty = taxDetailList.getJSONObject(j).getString("pricePerQty");


                                    ImagicaPackageModel.TaxDetailList taxDetailList1 = new ImagicaPackageModel.TaxDetailList();
                                    taxDetailList1.setName(Name);
                                    taxDetailList1.setPricePerQty(Double.parseDouble(PricePerQty));
                                    taxdetailses.add(taxDetailList1);

                                }
                                ImagicaPackageModel packageModel = new ImagicaPackageModel(pimageURL, pdescription.trim(), ptitle.trim(), pcurrency.trim(), passengerType.trim(), bdaddOneName.trim(), cdaddOnName.trim(), Integer.parseInt(pquantity.trim()), Integer.parseInt(tdquantity.trim()), Integer.parseInt(bdquantity.trim()), Integer.parseInt(cdquantity.trim()), Double.parseDouble(pcostPer.trim()), Double.parseDouble(pcostTotal.trim()), Long.parseLong(pproductID.trim()), Long.parseLong(cdaddOnId.trim()), Long.parseLong(bdaddOnId.trim()), carExists, busExists, taxdetailses);
                                imagicaPackageModels.add(packageModel);
                                Log.i("dps", imagicaPackageModels.toString());

                            }
                            JSONArray ticketjsonArray = hb.getJSONArray("tickets");
                            alPackages = new ArrayList<>();
                            ArrayList<ImagicaTicketModel> imagicaTicketModels = new ArrayList<>();

                            for (int i = 0; i < ticketjsonArray.length(); i++) {
                                String ticketImage = ticketjsonArray.getJSONObject(i).getString("image");
                                String ticketTitle = ticketjsonArray.getJSONObject(i).getString("title");
                                String ticketDescription = ticketjsonArray.getJSONObject(i).getString("description");
                                String ticketProductType = ticketjsonArray.getJSONObject(i).getString("productType");
                                String ticketTerms = ticketjsonArray.getJSONObject(i).getString("terms");
                                String ticketCurrency = ticketjsonArray.getJSONObject(i).getString("currency");

                                ArrayList<TicketDetail> ticketDetailArrayList = new ArrayList<>();
                                if (ticketjsonArray.getJSONObject(i).has("ticketTypeList")) {
                                    JSONArray ticketTypeListArray = ticketjsonArray.getJSONObject(i).getJSONArray("ticketTypeList");

                                    for (int j = 0; j < ticketTypeListArray.length(); j++) {
                                        String ticketTypeListtype = ticketTypeListArray.getJSONObject(j).getString("type");
                                        String ticketTypeListquantity = ticketTypeListArray.getJSONObject(j).getString("quantity");
                                        String ticketTypeListcostPerQty = ticketTypeListArray.getJSONObject(j).getString("costPerQty");
                                        String ticketTypeListtotalCost = ticketTypeListArray.getJSONObject(j).getString("totalCost");
                                        String ticketTypeListproductId = ticketTypeListArray.getJSONObject(j).getString("productId");
                                        String ticketTypeListplu = ticketTypeListArray.getJSONObject(j).getString("plu");
                                        TicketDetail ticketdetails = new TicketDetail();
                                        ticketdetails.setTdtype(ticketTypeListtype);
                                        ticketdetails.setTittle(ticketTitle);
                                        ticketdetails.setTdquantity(Long.parseLong(ticketTypeListquantity));
                                        ticketdetails.setTdcostPerQty(Double.parseDouble(ticketTypeListcostPerQty));
                                        ticketdetails.setTdcostTotalQty(Double.parseDouble(ticketTypeListtotalCost));
                                        ticketdetails.setTdproductID(Long.parseLong(ticketTypeListproductId));
                                        ticketdetails.setTdplu(Long.parseLong(ticketTypeListplu));
                                        ticketDetailArrayList.add(ticketdetails);

//                                        ArrayList<TicketDetailList> taxdetailsLists = new ArrayList<>();
                                        JSONArray TickettaxDetailsListArray = ticketTypeListArray.getJSONObject(j).getJSONArray("taxDetailsList");
                                        for (int k = 0; k < TickettaxDetailsListArray.length(); k++) {
                                            String tickettaxDetailsListname = TickettaxDetailsListArray.getJSONObject(k).getString("name");
                                            String tickettaxDetailsListpricePerQty = TickettaxDetailsListArray.getJSONObject(k).getString("pricePerQty");

//                                            TicketDetail.TicketDetailList TICKETDetailList = new TicketDetail.TicketDetailList();
//                                            TICKETDetailList.setName(tickettaxDetailsListname);
//                                            TICKETDetailList.setPricePerQty(Double.parseDouble(tickettaxDetailsListpricePerQty));
//
//                                            taxdetailsLists.add(TICKETDetailList);

                                        }
                                        Log.i("Adult", ticketTypeListtype);
                                    }
                                }
                                for (TicketDetail ticketDetail : ticketDetailArrayList) {
                                    Log.i("Ticket", ticketDetail.getTdtype());
                                }
                                ImagicaTicketModel ticketModel = new ImagicaTicketModel(ticketDescription, ticketTitle, ticketProductType, ticketCurrency, ticketImage, ticketTerms, Integer.parseInt(ticketTerms), ticketDetailArrayList);
                                imagicaTicketModels.add(ticketModel);
                            }

                            String VisitDate = hb.getString("visitDate");
                            String DepartureDate = hb.getString("departureDate");
                            String BusinessUnit = hb.getString("businessUnit");

                            ArrayList<AddOnsModel> AddOnsModels = new ArrayList<>();
                            if (hb.has("addOns")) {
                                JSONObject addOns = hb.getJSONObject("addOns");
                                ArrayList<AddOnsModel.CarAdons> carAddonsmodel = new ArrayList<>();
                                if (!addOns.isNull("carAddonDetailList")) {
                                    JSONArray carAdonArray = addOns.getJSONArray("carAddonDetailList");
                                    alPackages = new ArrayList<>();
                                    for (int i = 0; i < carAdonArray.length(); i++) {
                                        String cadltypeId = carAdonArray.getJSONObject(i).getString("typeId");
                                        String cadltype = carAdonArray.getJSONObject(i).getString("type");
                                        String cadlcurrency = carAdonArray.getJSONObject(i).getString("currency");
                                        String cadlquantity = carAdonArray.getJSONObject(i).getString("quantity");
                                        String cadltotalAddOnCost = carAdonArray.getJSONObject(i).getString("totalAddOnCost");
                                        String cadladdOnCost = carAdonArray.getJSONObject(i).getString("addOnCost");
                                        String cadltitle = carAdonArray.getJSONObject(i).getString("title");
                                        String cadlaid = carAdonArray.getJSONObject(i).getString("aid");
                                        String cadlimageUrl = carAdonArray.getJSONObject(i).getString("imageUrl");

                                        AddOnsModel.CarAdons carAdons = new AddOnsModel.CarAdons();
                                        carAdons.setTypeId(cadltypeId);

                                        carAdons.setType(cadltype);
                                        carAdons.setCurrency(cadlcurrency);
                                        carAdons.setQuantity(cadlquantity);
                                        carAdons.setTotalAddOnCost(cadltotalAddOnCost);
                                        carAdons.setAddOnCost(cadladdOnCost);
                                        carAdons.setcTitle(cadltitle);
                                        carAdons.setAid(cadlaid);
                                        carAdons.setImageUr(cadlimageUrl);
                                        carAdons.setCarAdTitle("Car Addons");
                                        carAddonsmodel.add(carAdons);

                                        Log.i("d1", carAdons.getAid());

                                        JSONArray taxDetailList = carAdonArray.getJSONObject(i).getJSONArray("taxDetails");
                                        ArrayList<AddOnsModel.CarAdons.taxDetails> taxdetails = new ArrayList<>();

                                        for (int j = 0; j < taxDetailList.length(); j++) {
                                            String cadlName = taxDetailList.getJSONObject(j).getString("name");
                                            String cadlpPerqty = taxDetailList.getJSONObject(j).getString("pricePerQty");
                                            Log.i("i2", cadlName.toString());
                                            AddOnsModel.CarAdons.taxDetails taxDetailList1 = new AddOnsModel.CarAdons.taxDetails();
                                            taxDetailList1.setName(cadlName);
                                            taxDetailList1.setPricePerQty(cadlpPerqty);
                                            taxdetails.add(taxDetailList1);
                                            Log.i("d2", taxDetailList1.getName());

                                        }
                                        //AddOnsModel CarAddonDetailList = new AddOnsModel(cadltypeId, cadltype, cadlcurrency, cadlquantity, cadltotalAddOnCost, cadladdOnCost, cadltitle, cadlaid, cadlimageUrl,taxdetails);

                                    }
                                }
                                ArrayList<AddOnsModel.BusAdons> busAddonsmodel = new ArrayList<>();
                                if (!addOns.isNull("busAddonDetailList")) {
                                    JSONArray busAdonArray = addOns.getJSONArray("busAddonDetailList");
                                    alPackages = new ArrayList<>();
                                    for (int i = 0; i < busAdonArray.length(); i++) {
                                        String badltypeId = busAdonArray.getJSONObject(i).getString("typeId");
                                        String badltype = busAdonArray.getJSONObject(i).getString("type");
                                        String badlcurrency = busAdonArray.getJSONObject(i).getString("currency");
                                        String badlquantity = busAdonArray.getJSONObject(i).getString("quantity");
                                        String badltotalAddOnCost = busAdonArray.getJSONObject(i).getString("totalAddOnCost");
                                        String badladdOnCost = busAdonArray.getJSONObject(i).getString("addOnCost");
                                        String badltitle = busAdonArray.getJSONObject(i).getString("title");
                                        String badlaid = busAdonArray.getJSONObject(i).getString("aid");
                                        String badlimageUrl = busAdonArray.getJSONObject(i).getString("imageUrl");

                                        AddOnsModel.BusAdons busAdons = new AddOnsModel.BusAdons();
                                        busAdons.setTypeId(badltypeId);
                                        busAdons.setType(badltype);
                                        busAdons.setCurrency(badlcurrency);
                                        busAdons.setQuantity(badlquantity);
                                        busAdons.setTotalAddOnCost(badltotalAddOnCost);
                                        busAdons.setAddOnCost(badladdOnCost);
                                        busAdons.setTitle(badltitle);
                                        busAdons.setAid(badlaid);
                                        busAdons.setImageUr(badlimageUrl);


                                        Log.i("d3", busAdons.getTitle());

                                        ArrayList<AddOnsModel.BusAdons.taxDetails> taxdetails = new ArrayList<>();
                                        JSONArray taxDetailList = busAdonArray.getJSONObject(i).getJSONArray("taxDetails");
                                        for (int j = 0; j < taxDetailList.length(); j++) {
                                            String badlName = taxDetailList.getJSONObject(j).getString("name");
                                            String badlpPerqty = taxDetailList.getJSONObject(j).getString("pricePerQty");
                                            AddOnsModel.BusAdons.taxDetails taxDetailList1 = new AddOnsModel.BusAdons.taxDetails();
                                            taxDetailList1.setName(badlName);
                                            taxDetailList1.setPricePerQty(badlpPerqty);
                                            taxdetails.add(taxDetailList1);
                                            Log.i("d4", taxDetailList1.getName());


                                        }
                                        String badlplaceId = busAdonArray.getJSONObject(i).getString("placeId");
                                        String badlplaceName = busAdonArray.getJSONObject(i).getString("placeName");
                                        busAdons.setPlaceId(badlplaceId);
                                        busAdons.setPlaceName(badlplaceName);
                                        Log.i("d5", busAdons.getPlaceId());


                                        ArrayList<AddOnsModel.BusAdons.locationList> locationList = new ArrayList<>();
                                        JSONArray locationListarray = busAdonArray.getJSONObject(i).getJSONArray("locationList");
                                        for (int j = 0; j < locationListarray.length(); j++) {
                                            String badllocationId = locationListarray.getJSONObject(j).getString("locationId");
                                            String badlName = locationListarray.getJSONObject(j).getString("locationName");
                                            Log.i("i5", badlName.toString());
                                            AddOnsModel.BusAdons.locationList locationList1 = new AddOnsModel.BusAdons.locationList();
                                            locationList1.setLocationName(badlName);
                                            locationList1.setLocationId(badllocationId);
                                            locationList.add(locationList1);
                                            Log.i("d6", locationList1.getLocationName());

                                        }
                                        if (locationList.size() != 0) {
                                            busAdons.setLocationList(locationList);
                                        }
                                        busAddonsmodel.add(busAdons);


                                    }
                                }
                                AddOnsModel.photoAddonDetail photoAddOnObject1 = new AddOnsModel.photoAddonDetail();
                                if (addOns.has("photoAddonDetail") && !addOns.isNull("photoAddonDetail")) {
                                    JSONObject photoAddOnObject = addOns.getJSONObject("photoAddonDetail");

                                    String phtypeId = photoAddOnObject.getString("typeId");
                                    String phtype = photoAddOnObject.getString("type");
                                    String phcurrency = photoAddOnObject.getString("currency");
                                    String phquantity = photoAddOnObject.getString("quantity");
                                    String phtotalAddOnCost = photoAddOnObject.getString("totalAddOnCost");
                                    String phaddOnCost = photoAddOnObject.getString("addOnCost");
                                    String phtitle = photoAddOnObject.getString("title");
                                    String phaid = photoAddOnObject.getString("aid");
                                    String phimageUrl = photoAddOnObject.getString("imageUrl");

                                    photoAddOnObject1.setTypeId(phtypeId);
                                    photoAddOnObject1.setType(phtype);
                                    photoAddOnObject1.setCurrency(phcurrency);
                                    photoAddOnObject1.setQuantity(phquantity);
                                    photoAddOnObject1.setTotalAddOnCost(phtotalAddOnCost);
                                    photoAddOnObject1.setAddOnCost(phaddOnCost);
                                    photoAddOnObject1.setTitle(phtitle);
                                    photoAddOnObject1.setAid(phaid);
                                    photoAddOnObject1.setImageUrl(phimageUrl);

                                    Log.i("d7", photoAddOnObject1.getType());

                                    ArrayList<AddOnsModel.photoAddonDetail.taxDetails> ticketType = new ArrayList<>();
                                    JSONArray tsxDetailArray = photoAddOnObject.getJSONArray("taxDetails");
                                    for (int i = 0; i < tsxDetailArray.length(); i++) {

                                        String tdname = tsxDetailArray.getJSONObject(i).getString("name");
                                        String tdpricePerQty = tsxDetailArray.getJSONObject(i).getString("pricePerQty");

                                        AddOnsModel.photoAddonDetail.taxDetails taxDetails1 = new AddOnsModel.photoAddonDetail.taxDetails();

                                        taxDetails1.setName(tdname);
                                        taxDetails1.setPricePerQty(tdpricePerQty);
                                        Log.i("d8", taxDetails1.getPricePerQty());
                                        ticketType.add(taxDetails1);
                                    }
                                    photoAddOnObject1.setTaxdetails(ticketType);
                                }
                                AddOnsModel.snowMagicaAddOn snowMagicaAddOn1 = null;
                                if (addOns.has("snowMagicaAddOn") && !addOns.isNull("snowMagicaAddOn")) {
                                    JSONObject snowMagicaAddOnObject = addOns.getJSONObject("snowMagicaAddOn");
                                    snowMagicaAddOn1 = new AddOnsModel.snowMagicaAddOn();
                                    if (!snowMagicaAddOnObject.getString("prodType").equals(JSONObject.NULL)) {
                                        String smaprodType = snowMagicaAddOnObject.getString("prodType");
                                        String smaticketDescription = snowMagicaAddOnObject.getString("ticketDescription");
                                        String smatitle = snowMagicaAddOnObject.getString("title");
                                        String smaimageUrl = snowMagicaAddOnObject.getString("imageUrl");
                                        String smacurrency = snowMagicaAddOnObject.getString("currency");
                                        snowMagicaAddOn1.setProdType(smaprodType);
                                        snowMagicaAddOn1.setTicketDescription(smaticketDescription);
                                        snowMagicaAddOn1.setTitle(smatitle);
                                        snowMagicaAddOn1.setImageUrl(smaimageUrl);
                                        snowMagicaAddOn1.setCurrency(smacurrency);
                                    } else {
                                        snowMagicaAddOn1.setProdType("null");
                                        snowMagicaAddOn1.setTicketDescription("null");
                                        snowMagicaAddOn1.setTitle("null");
                                        snowMagicaAddOn1.setImageUrl("null");
                                        snowMagicaAddOn1.setCurrency("null");
                                    }
                                    ArrayList<AddOnsModel.snowMagicaAddOn.sessionList> sessionList = new ArrayList<>();
                                    if (!snowMagicaAddOnObject.getJSONArray("sessionList").equals(JSONObject.NULL)) {
                                        JSONArray sessionListArray = snowMagicaAddOnObject.getJSONArray("sessionList");
                                        alPackages = new ArrayList<>();
                                        for (int i = 0; i < sessionListArray.length(); i++) {
                                            String sessessionId = sessionListArray.getJSONObject(i).getString("sessionId");
                                            String sessessionName = sessionListArray.getJSONObject(i).getString("sessionName");
                                            AddOnsModel.snowMagicaAddOn.sessionList sessionList1 = new AddOnsModel.snowMagicaAddOn.sessionList();
                                            sessionList1.setSessionId(sessessionId);
                                            sessionList1.setSessionName(sessessionName);

                                            Log.i("d9", sessionList1.getSessionId());

                                            JSONArray timeSlotsArray = sessionListArray.getJSONObject(i).getJSONArray("timeSlots");
                                            ArrayList<AddOnsModel.snowMagicaAddOn.sessionList.timeSlots> timeSlots = new ArrayList<>();
                                            for (int j = 0; j < timeSlotsArray.length(); j++) {
                                                String tmbadllocationId = timeSlotsArray.getJSONObject(j).getString("slotId");
                                                String tmslotName = timeSlotsArray.getJSONObject(j).getString("slotName");
                                                AddOnsModel.snowMagicaAddOn.sessionList.timeSlots timeSlots1 = new AddOnsModel.snowMagicaAddOn.sessionList.timeSlots();
                                                timeSlots1.setSlotId(tmbadllocationId);
                                                timeSlots1.setSlotName(tmslotName);
                                                Log.i("d10", timeSlots1.getSlotName());
                                                timeSlots.add(timeSlots1);

                                            }
                                            sessionList1.setTimelots(timeSlots);
                                            sessionList.add(sessionList1);

                                            //String sestimeSlots = sessionListArray.getJSONObject(i).getString("timeSlots");
                                        }
                                    }


                                    JSONObject ticketTypeobject = snowMagicaAddOnObject.getJSONObject("ticketType");

                                    String tttype = ticketTypeobject.getString("type");
                                    String ttquantity = ticketTypeobject.getString("quantity");
                                    String ttcostPerQty = ticketTypeobject.getString("costPerQty");
                                    String tttotalCost = ticketTypeobject.getString("totalCost");
                                    String ttproductId = ticketTypeobject.getString("productId");
                                    String ttplu = ticketTypeobject.getString("plu");
                                    AddOnsModel.snowMagicaAddOn.ticketType ticketType1 = new AddOnsModel.snowMagicaAddOn.ticketType();

                                    ticketType1.setType(tttype);
                                    ticketType1.setQuantity(ttquantity);
                                    ticketType1.setCostPerQty(ttcostPerQty);
                                    ticketType1.setTotalCost(tttotalCost);
                                    ticketType1.setProductId(ttproductId);
                                    ticketType1.setPlu(ttplu);


                                    JSONArray taxDetailsListArray = ticketTypeobject.getJSONArray("taxDetailsList");
                                    alPackages = new ArrayList<>();
//                                    ArrayList<AddOnsModel.snowMagicaAddOn.ticketType.taxDetailsList> taxDetailsList = new ArrayList<>();
//                                    for (int i = 0; i < taxDetailsListArray.length(); i++) {
//                                        String ttaxname = taxDetailsListArray.getJSONObject(i).getString("name");
//                                        String ttaxpricePerQty = taxDetailsListArray.getJSONObject(i).getString("pricePerQty");
//                                        AddOnsModel.snowMagicaAddOn.ticketType.taxDetailsList taxDetailsList1 = new AddOnsModel.snowMagicaAddOn.ticketType.taxDetailsList();
//                                        taxDetailsList1.setName(ttaxname);
//                                        taxDetailsList1.setPricePerQty(ttaxpricePerQty);
//                                        taxDetailsList.add(taxDetailsList1);
//                                        Log.i("d12", taxDetailsList1.getName());
//
//
//                                    }
//
//                                    snowMagicaAddOn1.setTicketType(ticketType1);
//                                    snowMagicaAddOn1.setSessionlist(sessionList);
                                }
                                ArrayList<AddOnsModel.BrunchAdons> brunchAddonsmodel = new ArrayList<>();
                                if (addOns.has("brunchAddon") && !addOns.isNull("brunchAddon")) {
                                    JSONArray brunchAddonArray = addOns.getJSONArray("brunchAddon");
                                    alPackages = new ArrayList<>();
                                    for (int i = 0; i < brunchAddonArray.length(); i++) {
                                        String brcurrencyCode = brunchAddonArray.getJSONObject(i).getString("currencyCode");
                                        String brproductId = brunchAddonArray.getJSONObject(i).getString("productId");
                                        String brquantity = brunchAddonArray.getJSONObject(i).getString("quantity");
                                        String brtotalCost = brunchAddonArray.getJSONObject(i).getString("totalCost");
                                        String brcostPer = brunchAddonArray.getJSONObject(i).getString("costPer");
                                        String brimageUrl = brunchAddonArray.getJSONObject(i).getString("imageUrl");
                                        String brdescription = brunchAddonArray.getJSONObject(i).getString("description");
                                        String brtitle = brunchAddonArray.getJSONObject(i).getString("title");
                                        String braid = brunchAddonArray.getJSONObject(i).getString("aid");
                                        String brbruchTitle = brunchAddonArray.getJSONObject(i).getString("bruchTitle");

                                        AddOnsModel.BrunchAdons brunchAdons = new AddOnsModel.BrunchAdons();
                                        brunchAdons.setCurrencyCode(brcurrencyCode);
                                        brunchAdons.setProductId(brproductId);
                                        brunchAdons.setQuantity(brquantity);
                                        brunchAdons.setTotalCost(brtotalCost);
                                        brunchAdons.setCostPer(brcostPer);
                                        brunchAdons.setImageUrl(brimageUrl);
                                        brunchAdons.setDescription(brdescription);
                                        brunchAdons.setTitle(brtitle);
                                        brunchAdons.setAid(braid);
                                        brunchAdons.setBruchTitle(brbruchTitle);

                                        JSONArray taxDetailListArray = brunchAddonArray.getJSONObject(i).getJSONArray("taxDetailList");
                                        ArrayList<AddOnsModel.BrunchAdons.taxDetailList> taxDetailLists = new ArrayList<>();
                                        for (int j = 0; j < taxDetailListArray.length(); j++) {
                                            String txname = taxDetailListArray.getJSONObject(j).getString("name");
                                            String txpricePerQty = taxDetailListArray.getJSONObject(j).getString("pricePerQty");
                                            AddOnsModel.BrunchAdons.taxDetailList brtaxdetail = new AddOnsModel.BrunchAdons.taxDetailList();
                                            brtaxdetail.setName(txname);
                                            brtaxdetail.setPricePerQty(txpricePerQty);
                                            taxDetailLists.add(brtaxdetail);
                                        }
                                        brunchAddonsmodel.add(brunchAdons);
                                    }
                                }


                                AddOnsModel addOnsModel = new AddOnsModel(carAddonsmodel, busAddonsmodel, photoAddOnObject1, snowMagicaAddOn1, brunchAddonsmodel);
                                AddOnsModels.add(addOnsModel);
                            }

                            try {

                                Intent intent = new Intent(ImagicaList.this, PackageTicketActivity.class);
                                intent.putParcelableArrayListExtra("models", imagicaPackageModels);
                                ResultIPC resultIPC = ResultIPC.get();
                                int sys = resultIPC.setLargeData(imagicaTicketModels);
                                int adsys = resultIPC.setaddOnsModels(AddOnsModels);
                                intent.putExtra("addons", adsys);
                                intent.putExtra("ticket", sys);
                                intent.putExtra("totaldays", 1);
                                intent.putExtra("departureDate", etVisitDate.getText().toString());
                                intent.putExtra("visitDate", etVisitDate.getText().toString());
                                if (water_park.equalsIgnoreCase("WATER_PARK")) {
                                    intent.putExtra("destination", "WATER_PARK");
                                    intent.putExtra("departureDate", etWaterParkDate.getText().toString());
                                    intent.putExtra("visitDate", etWaterParkDate.getText().toString());
                                } else {
                                    intent.putExtra("destination", "THEME_PARK");
                                }
                                startActivity(intent);
                            } catch (StackOverflowError e) {
                                e.printStackTrace();
                            }
                        } else if (msg.equalsIgnoreCase("Please login and try again.")) {
                            showInvalidSessionDialog();
                        } else {
                            String errorMsgs = hb.getString("errorMsgs");
                            String msgs = hb.getString("message");
                            if (!errorMsgs.equalsIgnoreCase("null") && !errorMsgs.isEmpty()) {
                                CustomToast.showMessage(ImagicaList.this, errorMsgs.replace("[", "").replace("]", ""));
                            } else {
                                CustomToast.showMessage(ImagicaList.this, msgs.replace("[", "").replace("]", ""));
                            }

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(ImagicaList.this, NetworkErrorHandler.getMessage(error, ImagicaList.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    //


    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(ImagicaList.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(ImagicaList.this).sendBroadcast(intent);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("imagica.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


    private void checVisitDate(String busDepDate) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busDepDate);
        if (!gasCheckLog.isValid) {
            etVisitDate.setError("Date Required");
            focusView = etVisitDate;
            cancel = true;
        }
    }

    private void checkDperDate(String busDepDate) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busDepDate);
        if (!gasCheckLog.isValid) {
            etVisitDate.setError("Date Required");
            focusView = etVisitDate;
            cancel = true;
        }
    }


    private void checkWaterDate(String busDepDate) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busDepDate);
        if (!gasCheckLog.isValid) {
            etWaterParkDate.setError("Date Required");
            focusView = etWaterParkDate;
            cancel = true;
        }
    }

    private void searchTickets(final String aDate, final String type) {
        if (type.equals("theme")) {
            Intent imagicaTicketIntent = new Intent(getApplicationContext(), ImagicaThemeParkList.class);
            imagicaTicketIntent.putExtra("aDate", aDate);
            startActivity(imagicaTicketIntent);

        } else {
            Intent imagicaTicketIntent = new Intent(getApplicationContext(), ImagicaWaterParkList.class);
            imagicaTicketIntent.putExtra("aDate", aDate);
            startActivity(imagicaTicketIntent);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }
}
