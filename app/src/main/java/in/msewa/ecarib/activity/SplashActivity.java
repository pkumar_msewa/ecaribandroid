package in.msewa.ecarib.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidquery.AQuery;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.ecarib.activity.useractivity.LoginActivity;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.BillPaymentOperatorsModel;
import in.msewa.model.BusSaveModel;
import in.msewa.model.FlightSaveModel;
import in.msewa.model.UserModel;
import in.msewa.permission.PermissionHandler;
import in.msewa.permission.Permissions;
import in.msewa.util.ConnectionDetector;
import in.msewa.util.EncryptDecryptCheckUtility;
import in.msewa.util.SecurityUtil;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


public class SplashActivity extends AppCompatActivity {
  private static final int REQUEST = 112;
  private UserModel session = UserModel.getInstance();
  private List<UserModel> userArray;

  //For  now set it to true
  private boolean mPinActive;
  private ConnectionDetector connectionDetector;
  Boolean isInternetPresent = false;
  Boolean isResumed = false;


  private JSONObject jsonRequest;
  private ProgressBar pbSplashStatus;
  private TextView tvSplashStatus;
  private String versionName = "";

  private int versionCode;
  private String apiVersion = "";
  private boolean isVersion = true;

  private LoadingDialog loadingDialog;

  //Music Dailog
  SharedPreferences musicPreferences;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    PayQwikApplication.this_MainActivity = this;
    setContentView(R.layout.activity_splash);
    musicPreferences = getSharedPreferences("ShowMusicDailog", Context.MODE_PRIVATE);
    ImageView ivBanner = findViewById(R.id.ivBanner);

    AQuery aQuery = new AQuery(SplashActivity.this);
    aQuery.id(ivBanner).background(R.mipmap.splash_image).getContext();
    loadingDialog = new LoadingDialog(SplashActivity.this);
    tvSplashStatus = findViewById(R.id.tvSplashStatus);
    pbSplashStatus = findViewById(R.id.pbSplashStatus);
    connectionDetector = new ConnectionDetector(SplashActivity.this);
    isInternetPresent = connectionDetector.isConnectingToInternet();
    ImageView ivBtn = (ImageView) findViewById(R.id.ivBtn);
    ImageView ivLogo = (ImageView) findViewById(R.id.ivLogo);

    SharedPreferences sharedPreferences = getSharedPreferences("Tutioral", Context.MODE_PRIVATE);
    if (sharedPreferences.getBoolean("Tutioral", false)) {

    } else {
      sharedPreferences.edit().putBoolean("Tutioral", false).apply();
    }
    Glide.with(this)
            .load(R.drawable.ic_top_banner)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(ivLogo);
    Glide.with(this)
            .load(R.drawable.ic_btm)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(ivBtn);
    firstProcess();
    //Music Dailog

//    FirebaseDynamicLinks.getInstance()
//      .getDynamicLink(getIntent())
//      .addOnSuccessListener(SplashActivity.this, new OnSuccessListener<PendingDynamicLinkData>() {
//        @Override
//        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
//          Uri deepLink = null;
//          if (pendingDynamicLinkData != null) {
//            deepLink = pendingDynamicLinkData.getLink();
//            if (NikiConfig.isNikiInitialized()) {
////              NikiConfig.getInstance().sendDeepLinkData(SplashActivity.this, deepLink.toString());
//            }
//          }
//
//        }
//      })
//      .addOnFailureListener(this, new OnFailureListener() {
//        @Override
//        public void onFailure(@NonNull Exception e) {
//        }
//      });
    SharedPreferences.Editor editor = musicPreferences.edit();
    editor.clear();
    editor.putBoolean("ShowMusicDailog", true);
    editor.apply();
    // ATTENTION: This was auto-generated to handle app links.
    Intent appLinkIntent = getIntent();
    String appLinkAction = appLinkIntent.getAction();
    Uri appLinkData = appLinkIntent.getData();
  }


  private void checkEncrypt() {
    try {
      String enData = EncryptDecryptCheckUtility.encrypt_data("Secret Message");
      String deData = EncryptDecryptCheckUtility.decrypt_data(enData);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  private void firstProcess() {
    connectionDetector = new ConnectionDetector(SplashActivity.this);
    isInternetPresent = connectionDetector.isConnectingToInternet();
    if (isInternetPresent) {
      pbSplashStatus.setVisibility(View.VISIBLE);
      tvSplashStatus.setVisibility(View.VISIBLE);
//            checkLavaVersion();
//      workLoad(1000);
//    }
      if (Build.VERSION.SDK_INT >= 23) {
        Permissions.check(SplashActivity.this, Manifest.permission.READ_PHONE_STATE, 0,
                new PermissionHandler() {
                  @Override
                  public void onGranted() {

//              checkLavaVersion();
                    workLoad(1000);
                  }
                });

      } else {
//        checkLavaVersion();
        workLoad(1000);
      }

    } else {
      pbSplashStatus.setVisibility(View.GONE);
      tvSplashStatus.setVisibility(View.GONE);
      showCustomDialog();
    }
  }


  private void checkAppVersion() {
    try {
      pbSplashStatus.setVisibility(View.VISIBLE);
      tvSplashStatus.setVisibility(View.VISIBLE);
      tvSplashStatus.setText("Checking app version, please wait.");

      PackageInfo pInfo = null;
      try {
        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
      } catch (PackageManager.NameNotFoundException e) {
        e.printStackTrace();
      }

      versionName = pInfo.versionName;
      jsonRequest = new JSONObject();
      jsonRequest.put("versionName", versionName);
      jsonRequest.put("key", "DF25E45253D995EA2DE65244159BA8EA");

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    AndroidNetworking.post(ApiUrl.URL_VER_CHECK)
            .addJSONObjectBody(jsonRequest)
            .setPriority(Priority.HIGH)
            .setTag("test")
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
              @Override
              public void onResponse(JSONObject response) throws JSONException {
                try {
                  String code = response.getString("code");
                  String message = response.getString("message");
                  if (code != null & code.equals("V00")) {
                    isVersion = true;
                    if (!checkSession()) {
                      pbSplashStatus.setVisibility(View.GONE);
                      tvSplashStatus.setVisibility(View.GONE);
                      BusSaveModel.deleteAll(BusSaveModel.class);
                      FlightSaveModel.deleteAll(FlightSaveModel.class);
                      Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                      startActivity(mainIntent);
                      finish();
                    } else {
                      getUserDetail();
                    }
                  } else {
                    isVersion = false;
                    showOlderVersionDialog(message);
                    tvSplashStatus.setText(message);
                  }
                } catch (JSONException e) {
                  checkAppVersion();
                }
              }

              @Override
              public void onError(ANError anError) {
                checkAppVersion();
              }
            });

  }


  private boolean checkSession() {
    pbSplashStatus.setVisibility(View.VISIBLE);
    tvSplashStatus.setVisibility(View.VISIBLE);
    tvSplashStatus.setText("Checking session, please wait.");
    try {
      userArray = Select.from(UserModel.class).list();

      if (userArray.size() != 0) {
        try {
          //Decrypting sessionId
          session.setUserSessionId(userArray.get(0).getUserSessionId());
        } catch (Exception e) {
          e.printStackTrace();
        }
        session.setUserEmail(userArray.get(0).getUserEmail());
        session.setUserAcNo(userArray.get(0).getUserAcNo());
        session.setUserAcCode(userArray.get(0).getUserAcCode());
        session.setUserMobileNo(userArray.get(0).getUserMobileNo());
        session.setUserFirstName(userArray.get(0).getUserFirstName());
        session.setUserLastName(userArray.get(0).getUserLastName());
        session.setUserBalance(userArray.get(0).getUserBalance());
        session.setUserAddress(userArray.get(0).getUserAddress());
        session.setEmailIsActive(userArray.get(0).getEmailIsActive());
        session.setUserImage(userArray.get(0).getUserImage());
        session.setMPin(userArray.get(0).isMPin());
        session.setUserPoints(userArray.get(0).getUserPoints());
        session.setUserAcName(userArray.get(0).getUserAcName());
        mPinActive = session.isMPin;
        session.setIsValid(1);
        pbSplashStatus.setVisibility(View.GONE);
        tvSplashStatus.setVisibility(View.GONE);

        return true;
      } else {
        session.setIsValid(0);
        pbSplashStatus.setVisibility(View.GONE);
        tvSplashStatus.setVisibility(View.GONE);

        return false;
      }
    } catch (SQLiteException e) {
      return false;
    }

  }


  public void showCustomDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(PayQwikApplication.this_MainActivity, R.string.dialog_title2, Html.fromHtml(generateNoInternetMessage()));

    builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        isResumed = true;
        dialog.dismiss();
        startActivity(new Intent(Settings.ACTION_SETTINGS));

      }
    });

    builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        firstProcess();
      }
    });
    builder.show();
  }

  public void showOlderVersionDialog(String message) {
    try {
      CustomAlertDialog builder = new CustomAlertDialog(PayQwikApplication.this_MainActivity, R.string.dialog_title2, Html.fromHtml(message));
      builder.setCancelable(false);
      builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          try {
            Uri marketUri = Uri.parse("market://details?id=" + SplashActivity.this.getPackageName());
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
            dialog.dismiss();
            startActivity(marketIntent);
          } catch (ActivityNotFoundException exception) {
            exception.printStackTrace();
          }
        }
      });
      builder.show();
    } catch (WindowManager.BadTokenException e) {

    }
  }

  public String generateNoInternetMessage() {
    String source = "<b><font color=#000000> No internet connection.</font></b>" +
            "<br><br><b><font color=#ff0000> Please check your internet connection and try again.</font></b><br></br>";
    return source;
  }

  public String generateOldVersionMessage() {
    String source = "<b><font color=#000000> Current installed V-PayQwik App version is outdated." + "</font></b>" +
            "<br><br><b><font color=#ff0000> Please update V-PayQwik, to continue.</font></b><br></br>";
    return source;
  }


  @Override
  protected void onResume() {
    super.onResume();
    if (isResumed) {
      checkLavaVersion();
    }

    if (!isVersion) {
      showOlderVersionDialog(tvSplashStatus.getText().toString());
    }

  }

  private void workLoad(final int time) {
    Thread thread = new Thread() {
      @Override
      public void run() {
        try {
          Thread.sleep(time);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                          if (!checkSession()) {
                            try {
                              BusSaveModel.deleteAll(BusSaveModel.class);
                              FlightSaveModel.deleteAll(FlightSaveModel.class);
                              BillPaymentOperatorsModel.deleteAll(BillPaymentOperatorsModel.class);
                            } catch (SQLiteException e) {

                            }
                            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(mainIntent);
                            finish();
                          } else {
                            getUserDetail();
                          }
                        }

                      }
        );
      }

    };
    thread.start();

  }


  public void getUserDetail() {
    UserModel userModel = Select.from(UserModel.class).first();
    pbSplashStatus.setVisibility(View.VISIBLE);
    tvSplashStatus.setVisibility(View.VISIBLE);
    tvSplashStatus.setText("Getting user details, please wait.");
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", userModel.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("GET USER PARAMS", jsonRequest.toString());
      Log.i("UserUrl", ApiUrl.URL_GET_USER_DETAILS);
      AndroidNetworking.post(ApiUrl.URL_GET_USER_DETAILS)
              .addJSONObjectBody(jsonRequest) // posting json
              .setTag("test")
              .setPriority(Priority.IMMEDIATE)
              .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
              .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          // do anything with response
          try {
            Log.i("GET USER RESPONSE", response.toString());
            String code = response.getString("code");
            if (code != null & code.equals("S00")) {
              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);

              //New Implementation
              JSONObject jsonDetail = jsonObject.getJSONObject("details");
              String nikiOffer = jsonDetail.getString("nikiOffer");
              JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");
              double userBalanceD = accDetail.getDouble("balance");
              String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

              long userAcNoL = accDetail.getLong("accountNumber");
              String userAcNo = String.valueOf(userAcNoL);
              int userPointsI = accDetail.getInt("points");
              String userPoints = String.valueOf(userPointsI);


              JSONObject accType = accDetail.getJSONObject("accountType");

              String accName = accType.getString("name");
              String accCode = accType.getString("code");


              double accBalanceLimitD = accType.getDouble("balanceLimit");
              double accMonthlyLimitD = accType.getDouble("monthlyLimit");
              double accDailyLimitD = accType.getDouble("dailyLimit");

              String accBalanceLimit = String.valueOf(accBalanceLimitD);
              String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
              String accDailyLimit = String.valueOf(accDailyLimitD);

              JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
              String userFName = jsonUserDetail.getString("firstName");
              String userLName = jsonUserDetail.getString("lastName");
              String userMobile = jsonUserDetail.getString("contactNo");
              String userEmail = jsonUserDetail.getString("email");
              String userEmailStatus = jsonUserDetail.getString("emailStatus");

              String userAddress = jsonUserDetail.getString("address");
              String userImage = jsonUserDetail.getString("image");
              boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");

              String userDob = jsonUserDetail.getString("dateOfBirth");
              String userGender = jsonUserDetail.getString("gender");
              String encodedImage = jsonUserDetail.getString("encodedImage");
              String images = "";
              if (!encodedImage.equals("")) {
                images = encodedImage;
              } else {
                images = userImage;
              }
              boolean hasRefer = response.getBoolean("hasRefer");
              boolean iplEnable = false, ebsEnable = false, vnetEnable = false, razorPayEnable = false;
              String mdexToken = "", mdexKey = "", iplPrediction = "", iplSchedule = "", iplMyPrediction = "";
              if (response.has("iplEnable")) {
                iplEnable = response.getBoolean("iplEnable");
                mdexToken = response.getString("mdexToken");
                mdexKey = response.getString("mdexKey");
                iplPrediction = response.getString("iplPrediction");
                iplSchedule = response.getString("iplSchedule");
                iplMyPrediction = response.getString("iplMyPrediction");

              }
              if (response.has("ebsEnable") || response.has("vnetEnable") || response.has("razorPayEnable")) {
                ebsEnable = response.getBoolean("ebsEnable");
                vnetEnable = response.getBoolean("vnetEnable");
                razorPayEnable = response.getBoolean("razorPayEnable");
              }
              try {
                UserModel.deleteAll(UserModel.class);
              } catch (SQLiteException e) {

              }
              try {
                //Encrypting sessionId
                UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, images, userDob, userGender, isMPin, userPoints, hasRefer, nikiOffer, iplEnable, iplMyPrediction, iplPrediction, iplSchedule, mdexKey, mdexToken, ebsEnable, vnetEnable, razorPayEnable);
                userModel.save();
              } catch (Exception e) {
                e.printStackTrace();
              }

              session.setUserSessionId(session.getUserSessionId());
              session.setUserFirstName(userFName);
              session.setUserLastName(userLName);
              session.setUserMobileNo(userMobile);
              session.setUserEmail(userEmail);
              session.setEmailIsActive(userEmailStatus);
              session.setUserAcCode(accCode);
              session.setUserAcNo(userAcNo);
              session.setUserBalance(userBalance);
              session.setUserAcName(accName);
              session.setUserMonthlyLimit(accMonthlyLimit);
              session.setUserDailyLimit(accDailyLimit);
              session.setUserBalanceLimit(accBalanceLimit);
              session.setUserImage(images);
              session.setUserAddress(userAddress);
              session.setUserGender(userGender);
              session.setUserDob(userDob);
              session.setMPin(isMPin);
              session.setHasRefer(hasRefer);
              session.setIsValid(1);
              session.setEbsEnable(ebsEnable);
              session.setVnetEnable(vnetEnable);
              session.setRazorPayEnable(razorPayEnable);
              session.setNikiOffer(nikiOffer);
              session.setUserPoints(userPoints);
              session.setIplEnable(iplEnable);
              session.setIplMyPrediction(iplMyPrediction);
              session.setIplPrediction(iplPrediction);
              session.setIplSchedule(iplSchedule);
              session.setMdexKey(mdexKey);
              session.setMdexToken(mdexToken);

              if (mPinActive) {
                pbSplashStatus.setVisibility(View.GONE);
                tvSplashStatus.setVisibility(View.GONE);

                Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
                startActivity(mainIntent);
                finish();
              } else {
                pbSplashStatus.setVisibility(View.GONE);
                tvSplashStatus.setVisibility(View.GONE);

                Intent mainIntent = new Intent(SplashActivity.this, HomeMainActivity.class);
                startActivity(mainIntent);
                finish();
              }

            } else if (code.equals("F03")) {
              promoteLogout();
            } else {
              if (mPinActive) {
                pbSplashStatus.setVisibility(View.GONE);
                tvSplashStatus.setVisibility(View.GONE);

                Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
                startActivity(mainIntent);
                finish();
              } else {
                pbSplashStatus.setVisibility(View.GONE);
                tvSplashStatus.setVisibility(View.GONE);

                Intent mainIntent = new Intent(SplashActivity.this, HomeMainActivity.class);
                startActivity(mainIntent);
                finish();
              }
            }
          } catch (JSONException e) {
            e.printStackTrace();
            if (mPinActive) {
              pbSplashStatus.setVisibility(View.GONE);
              tvSplashStatus.setVisibility(View.GONE);

              Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
              startActivity(mainIntent);
              finish();
            } else {
              pbSplashStatus.setVisibility(View.GONE);
              tvSplashStatus.setVisibility(View.GONE);

              Intent mainIntent = new Intent(SplashActivity.this, HomeMainActivity.class);
              startActivity(mainIntent);
              finish();
            }

          }
        }

        //                }
//        }
        @Override
        public void onError(ANError error) {
          Log.i("value", error.getErrorDetail());
          // handle error
          if (mPinActive) {
            pbSplashStatus.setVisibility(View.GONE);
            tvSplashStatus.setVisibility(View.GONE);

            Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
            startActivity(mainIntent);
            finish();
          } else {
            pbSplashStatus.setVisibility(View.GONE);
            tvSplashStatus.setVisibility(View.GONE);

            Intent mainIntent = new Intent(SplashActivity.this, HomeMainActivity.class);
            startActivity(mainIntent);
            finish();
          }


        }
      });
//            {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<String, String>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };

//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            rq.add(postReq);
    }
  }


  private String getAppVersion() {
    try {
      PackageInfo packageInfo = getPackageManager()
              .getPackageInfo(getPackageName(), 0);
      return packageInfo.versionName;
    } catch (PackageManager.NameNotFoundException e) {
      // should never happen
      throw new RuntimeException("Could not get package name: " + e);
    }
  }

  private void promoteLogout() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGOUT, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObj) {
          try {
            String code = jsonObj.getString("code");
            if (code != null && code.equals("S00")) {
              UserModel.deleteAll(UserModel.class);
              loadingDialog.dismiss();
              startActivity(new Intent(SplashActivity.this, LoginActivity.class));
              finish();
            } else {
              loadingDialog.dismiss();
              UserModel.deleteAll(UserModel.class);
              loadingDialog.dismiss();
              startActivity(new Intent(SplashActivity.this, LoginActivity.class));
              finish();
            }
          } catch (JSONException e) {
            loadingDialog.dismiss();
            CustomToast.showMessage(SplashActivity.this, getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadingDialog.dismiss();
          CustomToast.showMessage(SplashActivity.this, getResources().getString(R.string.server_exception));
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, "json");
    }
  }


  public static final int compareVersions(String ver1, String ver2) {

    String[] vals1 = ver1.split("\\.");
    String[] vals2 = ver2.split("\\.");
    int i = 0;
    while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
      i++;
    }

    if (i < vals1.length && i < vals2.length) {
      int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
      return diff < 0 ? -1 : diff == 0 ? 0 : 1;
    }

    return vals1.length < vals2.length ? -1 : vals1.length == vals2.length ? 0 : 1;
  }

  private void checkLavaVersion() {
    jsonRequest = new JSONObject();
    try {
      pbSplashStatus.setVisibility(View.VISIBLE);
      tvSplashStatus.setVisibility(View.VISIBLE);
      tvSplashStatus.setText("Checking app version, please wait.");
      jsonRequest = new JSONObject();
      jsonRequest.put("device", android.os.Build.DEVICE);
      jsonRequest.put("imei", SecurityUtil.getIMEI(SplashActivity.this));
      jsonRequest.put("model", android.os.Build.MODEL);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, "https://www.vpayqwik.com/ws/api/partner/registerDevice", jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            boolean status = response.getBoolean("success");
            if (status) {
              checkAppVersion();
            } else {
              checkAppVersion();
            }
          } catch (JSONException e) {
            e.printStackTrace();
          }

        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          if (!checkSession()) {
            pbSplashStatus.setVisibility(View.GONE);
            tvSplashStatus.setVisibility(View.GONE);
            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(mainIntent);
            finish();
          } else {
            getUserDetail();
          }
          error.printStackTrace();
//                    CustomToast.showMessage(SplashActivity.this, Ne);
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> stringStringHashMap = new HashMap<>();
          stringStringHashMap.put("apiKey", "654D0FE324466FC80F934BDD848AA087");
          return stringStringHashMap;
        }
      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, "json");
    }
  }

}

