package in.msewa.ecarib.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import in.msewa.fragment.fragmenthowtoupgrade.VijayaBankUserUpgradeFragment;
import in.msewa.ecarib.LinkAdaharFragment;
import in.msewa.ecarib.R;

/**
 * Created by Ksf on 10/15/2016.
 */
public class HowToUpgradeActivity extends AppCompatActivity {

  CharSequence TitlesEnglish[] = null;
  private boolean updates;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_upgrade_wallet);
    //press back button in toolbar
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    updates = getIntent().getBooleanExtra("kyc", false);
    if (updates) {
      TitlesEnglish = new CharSequence[]{getResources().getString(R.string.upgrade_vijaya_bank), "Aadhaar Card"};
      ivBackBtn.setVisibility(View.GONE);
    } else {
      TitlesEnglish = new CharSequence[]{"Aadhaar Card", getResources().getString(R.string.upgrade_vijaya_bank)};
    }
    FragmentManager fragmentManager = getSupportFragmentManager();
    ViewPager mainPager = (ViewPager) findViewById(R.id.mainPagerUpgrade);
    TabLayout mSlidingTabLayout = (TabLayout) findViewById(R.id.tabLayoutUpgrade);
    mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, 2));
    mSlidingTabLayout.setupWithViewPager(mainPager);

  }


  public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
      super(fm);
      this.Titles = mTitles;
      this.NumbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {
      if (updates) {
        if (position == 0) {
          return new VijayaBankUserUpgradeFragment();
        } else {
          return new LinkAdaharFragment();
        }
      } else {

        if (position == 0) {
          return new LinkAdaharFragment();
        } else {
          return new VijayaBankUserUpgradeFragment();
        }
      }

    }

    @Override
    public CharSequence getPageTitle(int position) {
      return Titles[position];
    }

    @Override
    public int getCount() {
      return TitlesEnglish.length;
    }
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    View view = getCurrentFocus();
    if (view != null) {
      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
  }
}
