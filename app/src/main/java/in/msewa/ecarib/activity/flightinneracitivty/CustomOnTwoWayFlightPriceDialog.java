package in.msewa.ecarib.activity.flightinneracitivty;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import in.msewa.ecarib.R;


/**
 * Created by Kashif-PC on 1/21/2017.
 */
public class CustomOnTwoWayFlightPriceDialog extends AlertDialog.Builder {

    public CustomOnTwoWayFlightPriceDialog(Context context, String fare, double basefirstfare, double totalfirstTax, double totalfirstPrice, double basesecondfare, double totalsecondTax, double totalsecondPrice, String totalValues) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_twoway_price, null, false);
        TextView conenfee = (TextView) viewDialog.findViewById(R.id.conenfee);
        TextView cadTotalmsg = (TextView) viewDialog.findViewById(R.id.cadTotalmsg);
        cadTotalmsg.setText(totalValues);
        conenfee.setText("Convenience Fee  :   200");
        TextView cadBaseFirstFare = (TextView) viewDialog.findViewById(R.id.cadBaseFirstFare);
        TextView cadBaseFirstTax = (TextView) viewDialog.findViewById(R.id.cadBaseFirstTax);
        TextView cadBasesecondFare = (TextView) viewDialog.findViewById(R.id.cadBasesecondFare);
        TextView cadBasesecondTax = (TextView) viewDialog.findViewById(R.id.cadBasesecondTax);
        TextView cadTotalFare = (TextView) viewDialog.findViewById(R.id.cadTotalFare);
        String priceNew = String.valueOf(Double.parseDouble(fare) + Double.parseDouble("200"));
        cadTotalFare.setText("Total Amount  : " + priceNew);
        cadBaseFirstFare.setText(String.valueOf("OneWay BaseFare  : " + basefirstfare));
        cadBaseFirstTax.setText(String.valueOf("OneWay Tax  : " + totalfirstTax));
        cadBasesecondFare.setText(String.valueOf("RoundWay BaseFare  : " + basesecondfare));
        cadBasesecondTax.setText(String.valueOf("RoundWay Tax  : " + totalsecondTax));


        this.setCancelable(false);

        this.setView(viewDialog);


    }
}

