package in.msewa.ecarib.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidquery.AQuery;
import com.niki.config.NikiConfig;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.ecarib.activity.useractivity.LoginActivity;
import in.msewa.fragment.fragmentipl.MyPredictionfragment;
import in.msewa.fragment.fragmentnaviagtionitems.BillPaymentFragment;
import in.msewa.fragment.fragmentnaviagtionitems.CustomerSupportTabLayoutFragment;
import in.msewa.fragment.fragmentnaviagtionitems.DonationByQrFragment;
import in.msewa.fragment.fragmentnaviagtionitems.ExpenseTrackFragment;
import in.msewa.fragment.fragmentnaviagtionitems.HomeFragment;
import in.msewa.fragment.fragmentnaviagtionitems.InviteFreindFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MerchantPayByQrFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MerchantPayFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MobileTouUpFragment;
import in.msewa.fragment.fragmentnaviagtionitems.QwikPaymentFragment;
import in.msewa.fragment.fragmentnaviagtionitems.ReceiptFragment;
import in.msewa.fragment.fragmentnaviagtionitems.RedeemCouponFragment;
import in.msewa.fragment.fragmentnaviagtionitems.RequestMoneyFragment;
import in.msewa.fragment.fragmentnaviagtionitems.SharePointsFragment;
import in.msewa.fragment.fragmentnaviagtionitems.TravelFragment;
import in.msewa.fragment.fragmentqrpays.QRGeneratorFragment;
import in.msewa.fragment.paymentlimit.PaymentLimitFragment;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BusSaveModel;
import in.msewa.model.FlightSaveModel;
import in.msewa.model.PQCart;
import in.msewa.model.ShoppingModel;
import in.msewa.model.UserModel;
import in.msewa.nearbymerchant.MobileMerchantFragment;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.ShoppingActivity;

//import com.razorpay.Checkout;

public class HomeMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
  private UserModel session = UserModel.getInstance();
  private DrawerLayout drawer;
  private LoadingDialog loadDlg;

  //Shopping
  private List<ShoppingModel> shoppingArray;
  private PQCart pqCart = PQCart.getInstance();

  //Language Shared Preference.
  public static final String LANG = "lang";
  SharedPreferences langPreferences;
  private String language;

  private JSONObject jsonRequest;

  private String mPinCreated = "";
  private boolean isChanged = false;

  //Volley Tag
  private String tag_json_obj = "json_user";
  private JsonObjectRequest postReq;

  private TextView tvNavUserName, tvNavUserAccNo, tvNavUserAccName;
  private Button btnUpgradeWallet;
  private CircleImageView ivNavProfilPic;
  private AlertDialog alertDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    langPreferences = getSharedPreferences(LANG, Context.MODE_PRIVATE);
    language = langPreferences.getString("language", "");

    checkLangPref();

    setContentView(R.layout.activity_home_main);
    shoppingArray = new ArrayList<>();
    loadDlg = new LoadingDialog(HomeMainActivity.this);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.setNavigationIcon(R.drawable.ic_menu_button);
    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    View header = navigationView.getHeaderView(0);
    MenuItem item = navigationView.getMenu().findItem(R.id.nav_ipl);
    List<UserModel> userModels = Select.from(UserModel.class).list();
    if (userModels.get(0).isIplEnable()) {
      item.setVisible(true);
    } else {
      item.setVisible(false);
    }
    View view = getCurrentFocus();
    if (view != null) {
      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    tvNavUserName = (TextView) header.findViewById(R.id.tvUsername);
    tvNavUserAccNo = (TextView) header.findViewById(R.id.tv_accountNumber);
    tvNavUserAccName = (TextView) header.findViewById(R.id.tvNavUserAccName);
    btnUpgradeWallet = (Button) header.findViewById(R.id.btnUpgradeWallet);
    ivNavProfilPic = (CircleImageView) header.findViewById(R.id.ivProfile);

    setNavigationHeader();


    drawer = (DrawerLayout)

      findViewById(R.id.drawer_layout);
    toolbar.setNavigationOnClickListener(new View.OnClickListener()

    {
      @Override
      public void onClick(View v) {
        drawer.openDrawer(Gravity.LEFT);
      }
    });


    //New Thread
    Handler mHandler = new Handler();
    mHandler.postDelayed(navigationTask, 200);
    toolbar.setTitle("");

    LocalBroadcastManager.getInstance(HomeMainActivity.this).registerReceiver(mMessageReceiver, new IntentFilter("setting-change"));
//        getUserDetail("");

    mPinCreated = getIntent().getStringExtra("mPinBundle");
    if (mPinCreated != null && mPinCreated.equals("mPinCreated"))

    {
      List<UserModel> currentUserList = Select.from(UserModel.class).list();
      UserModel currentUser = currentUserList.get(0);
      currentUser.setMPin(true);
      session.setMPin(true);
      currentUser.save();

//            getUserDetail("");
    }
//    getCartItems();

  }

  private void setNavigationHeader() {
    AQuery aq = new AQuery(HomeMainActivity.this);
    if (session.getUserImage() != null && !session.getUserImage().equals("")) {
      if (session.getUserImage().contains("resources")) {
        aq.id(ivNavProfilPic).background(R.drawable.ic_user_avatar).image(ApiUrl.URL_IMAGE_MAIN + session.getUserImage());
      } else {

        aq.id(ivNavProfilPic).background(R.drawable.ic_user_avatar).image(decodeFromBase64ToBitmap(session.getUserImage())).getContext();
      }

    } else {
      aq.id(ivNavProfilPic).background(R.drawable.ic_user_avatar);
    }

    tvNavUserName.setText(session.getUserFirstName());
    tvNavUserAccNo.setText("Acc No: " + session.getUserAcNo());
    btnUpgradeWallet.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(getApplicationContext(), HowToUpgradeActivity.class));
      }
    });

    if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
      tvNavUserAccName.setText("Wallet:  " + session.getUserAcName());
      btnUpgradeWallet.setVisibility(View.GONE);

    } else {
      tvNavUserAccName.setText("Wallet:  " + session.getUserAcName());
      btnUpgradeWallet.setVisibility(View.VISIBLE);

    }
  }

  public void showLangDialog() {
    final String[] items = {getResources().getString(R.string.lang_english), getResources().getString(R.string.lang_hindi), getResources().getString(R.string.lang_kannada), getResources().getString(R.string.lang_telgu), getResources().getString(R.string.lang_malyalam), getResources().getString(R.string.lang_tamil), getResources().getString(R.string.lang_marathi), getResources().getString(R.string.lang_bengali), getResources().getString(R.string.lang_gujrati)};
    android.support.v7.app.AlertDialog.Builder langDialog =
      new android.support.v7.app.AlertDialog.Builder(HomeMainActivity.this, R.style.AppCompatAlertDialogStyle);
    langDialog.setTitle("Select your language");
    langDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        SharedPreferences.Editor editor = langPreferences.edit();
        editor.clear();
        editor.putString("language", "English");
        editor.apply();
        dialog.dismiss();
      }
    });
    langDialog.setItems(items, new DialogInterface.OnClickListener() {

      public void onClick(DialogInterface dialog, int position) {
        if (position == 0) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "English");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);

        } else if (position == 1) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Hindi");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);

        } else if (position == 2) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Kannada");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);
        } else if (position == 3) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Telgu");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);
        } else if (position == 4) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Malyalam");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);
        } else if (position == 5) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Tamil");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);
        } else if (position == 6) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Marathi");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);
        } else if (position == 7) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Bengali");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);
        } else if (position == 8) {
          SharedPreferences.Editor editor = langPreferences.edit();
          editor.clear();
          editor.putString("language", "Gujrati");
          editor.apply();
          Intent intent = getIntent();
          finish();
          startActivity(intent);
        }
      }

    });

    langDialog.show();
  }

  public void setViews() {
    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    displayView(1);
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("updates");
      if (action.equals("1")) {
        //Balance Update
        getUserDetail("");
//        getUserBalance();
        isChanged = true;
      } else if (action.equals("2")) {
        //USer Details Update
        //TODO UserSub Details
//                getUserSubDetails();
        getUserDetail("");
        isChanged = true;
      } else if (action.equals("8")) {
        //USer Details Update
        //TODO UserSub Details
//                getUserSubDetails();
//        getUserDetail("");
        getUserBalance();
        isChanged = true;
      } else if (action.equals("3")) {
        //Reward Points Update and KYC check
        getUserPointsAndKYC();
        isChanged = true;
      } else if (action.equals("4")) {
        promoteLogout();
      } else if (action.equals("5")) {
        promoteLogout();
      } else if (action.equals("6")) {
//        loadDlg.show();
//        getCartItems();
      }

    }
  };

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    MenuItem menuItem = menu.findItem(R.id.nav_ipl);
    if (session.isIplEnable()) {
      menuItem.setVisible(true);
    } else {
      menuItem.setVisible(false);
    }
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menuSetting) {
      Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
      startActivity(intent);
      return true;
    } else if (id == R.id.nav_ipl) {
      getSupportFragmentManager().beginTransaction().replace(R.id.container_body, new MyPredictionfragment()).addToBackStack(null).commit();
      return true;
    }
//        if (id == R.id.menuNotifications) {
//            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
//            startActivity(intent);
//            return true;
//        }
//        else if (id == R.id.menuSignOut) {
//            loadDlg.show();
//            promoteLogout();
//            return true;
//        }
    else if (id == R.id.menuRate) {
      Uri marketUri = Uri.parse("market://details?id=" + getApplication().getPackageName());
      Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
      startActivity(marketIntent);
      return true;
    } else if (id == R.id.menuLanguage) {
      showLangDialog();
      return true;
    } else if (id == R.id.menuNotifications) {

      getUserDetail("");
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
      getSupportFragmentManager().popBackStack();
    } else if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      android.support.v7.app.AlertDialog.Builder exitDialog =
        new android.support.v7.app.AlertDialog.Builder(HomeMainActivity.this, R.style.AppCompatAlertDialogStyle);
      exitDialog.setTitle("Do you really want to Exit?");
      exitDialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.dismiss();
        }
      });
      exitDialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.dismiss();
          finishAffinity();
          finish();
        }
      });
      exitDialog.show();
    }
  }


  @SuppressWarnings("StatementWithEmptyBody")
  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.nav_home) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(1);
        }
      }, 300);
    } else if (id == R.id.nav_mobile_topup) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(3);
        }
      }, 300);
    } else if (id == R.id.nav_bill_pay) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(4);
        }
      }, 300);
    } else if (id == R.id.nav_req_money) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(5);
        }
      }, 300);
    } else if (id == R.id.nav_travel) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(6);
        }
      }, 300);
    } else if (id == R.id.nav_donation) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(17);
        }
      }, 300);
    } else if (id == R.id.nav_payment_limit) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(20);
        }
      }, 300);
    }
//        else if (id == R.id.nav_refer_no) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (!session.isHasRefer()) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeMainActivity.this, R.style.AppCompatAlertDialogStyleMusic);
//                        LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                        View viewDialog = inflater1.inflate(R.layout.dialog_earn_to_pay, null, false);
//                        builder.setView(viewDialog);
//                        alertDialog = builder.create();
//                        ImageButton ok = (ImageButton) viewDialog.findViewById(R.id.btnOK);
//                        Button btnTermCondition = (Button) viewDialog.findViewById(R.id.btnTermCondition);
//                        final CheckBox cbterms = (CheckBox) viewDialog.findViewById(R.id.cbterms);
//                        ok.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                if (cbterms.isChecked()) {
//                                    startActivity((new Intent(HomeMainActivity.this, Home.class)));
//                                    finish();
//                                } else {
//                                    CustomToast.showMessage(HomeMainActivity.this, "Please Agree  the Terms And Condition");
//                                }
//                            }
//                        });
//
//                        btnTermCondition.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent menuIntent = new Intent(HomeMainActivity.this, MainMenuDetailActivity.class);
//                                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "TermsAndCondition");
//                                startActivity(menuIntent);
//                            }
//                        });
//
//                        alertDialog.show();
//                    }
//                }
//            }, 300);
//        }
    else if (id == R.id.nav_quick_payment) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(8);
        }
      }, 300);
    } else if (id == R.id.nav_my_rq) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(9);
        }
      }, 300);
    } else if (id == R.id.nav_redeem_coupen) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(10);
        }
      }, 300);
    } else if (id == R.id.nav_invite) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(11);
        }
      }, 300);
    } else if (id == R.id.nav_statement) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(12);
        }
      }, 300);
    } else if (id == R.id.nav_Customer_Support) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(13);
        }
      }, 300);
    } else if (id == R.id.nav_share_points) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(14);
        }
      }, 300);
    } else if (id == R.id.nav_exp_tracker) {
      drawer.postDelayed(new Runnable() {
        @Override
        public void run() {
          displayView(16);
        }
      }, 300);
    } else if (id == R.id.nav_ipl) {
      getSupportFragmentManager().beginTransaction().replace(R.id.container_body, new MyPredictionfragment()).addToBackStack(null).commit();
    } else if (id == R.id.nav_near) {
      getSupportFragmentManager().beginTransaction().replace(R.id.container_body, new MobileMerchantFragment()).addToBackStack(null).commit();
    }
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  private void displayView(int position) {

    Fragment fragment = null;
    Bundle navBundle = new Bundle();
    navBundle.putString(AppMetadata.FRAGMENT_TYPE, AppMetadata.FRAGMENT_TYPE);

    if (position == 1) {
      fragment = new HomeFragment();
    } else if (position == 2) {

      Intent shoppingIntent = new Intent(getApplicationContext(), ShoppingActivity.class);
      startActivity(shoppingIntent);
    } else if (position == 3) {
      fragment = new MobileTouUpFragment();
    } else if (position == 4) {
      fragment = new BillPaymentFragment();
    } else if (position == 5) {
      fragment = new RequestMoneyFragment();
    } else if (position == 6) {
//            fragment = new ShoppingFragment();
      fragment = new TravelFragment();
//            Bundle bType = new Bundle();
//            bType.putString("URL", "http://travel.vpayqwik.com/");
//            fragment.setArguments(bType);

    } else if (position == 7) {
      fragment = new MerchantPayFragment();
    } else if (position == 8) {
      fragment = new QwikPaymentFragment();
    } else if (position == 9) {
      fragment = new QRGeneratorFragment();
    } else if (position == 10) {
      fragment = new RedeemCouponFragment();
    } else if (position == 11) {
      fragment = new InviteFreindFragment();
    } else if (position == 12) {
      fragment = new ReceiptFragment();

    } else if (position == 13) {
      fragment = new CustomerSupportTabLayoutFragment();
    } else if (position == 23) {
      fragment = new MerchantPayByQrFragment();
    }
        /* ele if (position == 13) {
            final String[] items = {"+918025011300"};
            android.support.v7.app.AlertDialog.Builder callDialog =
                    new android.support.v7.app.AlertDialog.Builder(HomeMainActivity.this, R.style.AppCompatAlertDialogStyle);
            callDialog.setTitle("Select a no. to call");
            callDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            callDialog.setItems(items, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int position) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + items[position].toString().trim()));
                    if (ActivityCompat.checkSelfPermission(HomeMainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    startActivity(callIntent);
                }

            });

            callDialog.show();

        } else if (position == 14) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "care@vpayqwik.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));

        }*/
    else if (position == 14) {
      fragment = new SharePointsFragment();
    } else if (position == 16) {
      fragment = new ExpenseTrackFragment();
    } else if (position == 17) {
      fragment = new DonationByQrFragment();
    } else if (position == 20) {
      fragment = new  PaymentLimitFragment();
    }
    if (fragment != null) {
      try {
        View view = getCurrentFocus();
        if (view != null) {
          InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
          imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
      } catch (IllegalStateException e) {
        isChanged = true;
      }
    }
  }


  public void getUserSubDetails() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("GET USER PARAMS", jsonRequest.toString());
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_SUB_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("GET USER RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              String userEmail = response.getString("email");
              String userEmailStatus = response.getString("emailStatus");
              String userName = response.getString("name");
              String userAddress = response.getString("address");
              String userGender = response.getString("gender");


              List<UserModel> currentUserList = Select.from(UserModel.class).list();
              UserModel currentUser = currentUserList.get(0);
              currentUser.setUserFirstName(userName);
              currentUser.setUserAddress(userAddress);
              currentUser.setUserGender(userGender);
              currentUser.setUserEmail(userEmail);
              currentUser.setEmailIsActive(userEmailStatus);

              session.setUserFirstName(userName);
              session.setUserAddress(userAddress);
              session.setUserGender(userGender);
              session.setUserEmail(userEmail);
              session.setEmailIsActive(userEmailStatus);

              currentUser.save();

            }
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();

        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<String, String>();
          map.put("hash", "1234");
          return map;
        }

      };

      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  public void getUserPointsAndKYC() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("GET USER PARAMS", jsonRequest.toString());
      AndroidNetworking.post(ApiUrl.URL_GET_USER_POINTS)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              int userPoints = response.getInt("rewardPoints");
              String userPointsString = String.valueOf(userPoints);
              String userAccName = response.getString("accountType");

              List<UserModel> currentUserList = Select.from(UserModel.class).list();
              UserModel currentUser = currentUserList.get(0);
              currentUser.setUserPoints(userPointsString);
              currentUser.setUserAcName(userAccName);
              session.setUserPoints(userPointsString);
              session.setUserAcName(userAccName);
              currentUser.save();

            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError error) {
          loadDlg.dismiss();
          error.printStackTrace();
        }
      });
    }
  }

  public void getUserBalance() {
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      AndroidNetworking.post(ApiUrl.URL_GET_USER_BALANCE)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            Log.i("GET USER RESPONSE", response.toString());
            String code = response.getString("code");

            if (code != null && code.equals("S00")) {
              double userBalanceD = response.getDouble("balance");
              String userBalance = String.valueOf(userBalanceD);

              List<UserModel> currentUserList = Select.from(UserModel.class).list();
              UserModel currentUser = currentUserList.get(0);
              currentUser.setUserBalance(userBalance);
              session.setUserBalance(userBalance);
              currentUser.save();

            }
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
        }
      });
    }
  }


  public void getUserDetail(final String type) {
    loadDlg = new LoadingDialog(HomeMainActivity.this);
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      AndroidNetworking.post(ApiUrl.URL_GET_USER_DETAILS)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {

            String code = response.getString("code");
            String message = response.getString("message");
            try {

              loadDlg.dismiss();
            } catch (NullPointerException e) {
            }
            if (code != null && code.equals("S00")) {

              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);

              //New Implementation
              JSONObject jsonDetail = jsonObject.getJSONObject("details");
              String nikiOffer = jsonDetail.getString("nikiOffer");
              JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");
              double userBalanceD = accDetail.getDouble("balance");
              String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

              long userAcNoL = accDetail.getLong("accountNumber");
              String userAcNo = String.valueOf(userAcNoL);

              int userPointsI = accDetail.getInt("points");
              String userPoints = String.valueOf(userPointsI);


              JSONObject accType = accDetail.getJSONObject("accountType");

              String accName = accType.getString("name");
              String accCode = accType.getString("code");
              double accBalanceLimitD = accType.getDouble("balanceLimit");
              double accMonthlyLimitD = accType.getDouble("monthlyLimit");
              double accDailyLimitD = accType.getDouble("dailyLimit");

              String accBalanceLimit = String.valueOf(accBalanceLimitD);
              String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
              String accDailyLimit = String.valueOf(accDailyLimitD);

              JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
              String userFName = jsonUserDetail.getString("firstName");
              String userLName = jsonUserDetail.getString("lastName");
              String userMobile = jsonUserDetail.getString("contactNo");
              String userEmail = jsonUserDetail.getString("email");
              String userEmailStatus = jsonUserDetail.getString("emailStatus");

              String userAddress = jsonUserDetail.getString("address");
              String userImage = jsonUserDetail.getString("image");
              String encodedImage = jsonUserDetail.getString("encodedImage");
              boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");

              String userDob = jsonUserDetail.getString("dateOfBirth");
              String userGender = jsonUserDetail.getString("gender");
              boolean hasRefer = response.getBoolean("hasRefer");
              String images = "";
              if (!encodedImage.equals("")) {
                images = encodedImage;
              } else {
                images = userImage;
              }
              boolean iplEnable = false, ebsEnable = false, vnetEnable = false, razorPayEnable = false;
              String mdexToken = "", mdexKey = "", iplPrediction = "", iplSchedule = "", iplMyPrediction = "";
              if (response.has("iplEnable")) {
                iplEnable = response.getBoolean("iplEnable");
                mdexToken = response.getString("mdexToken");
                mdexKey = response.getString("mdexKey");
                iplPrediction = response.getString("iplPrediction");
                iplSchedule = response.getString("iplSchedule");
                iplMyPrediction = response.getString("iplMyPrediction");

              }
              if (response.has("ebsEnable") || response.has("vnetEnable") || response.has("razorPayEnable")) {
                ebsEnable = response.getBoolean("ebsEnable");
                vnetEnable = response.getBoolean("vnetEnable");
                razorPayEnable = response.getBoolean("razorPayEnable");
              }
              UserModel.deleteAll(UserModel.class);
              try {
                //Encrypting sessionId
                UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, images, userDob, userGender, isMPin, userPoints, hasRefer, nikiOffer, iplEnable, iplMyPrediction, iplPrediction, iplSchedule, mdexKey, mdexToken, ebsEnable, vnetEnable, razorPayEnable);
                userModel.save();
              } catch (Exception e) {
                e.printStackTrace();
              }

              session.setUserSessionId(session.getUserSessionId());
              session.setUserFirstName(userFName);
              session.setUserLastName(userLName);
              session.setUserMobileNo(userMobile);
              session.setUserEmail(userEmail);
              session.setEmailIsActive(userEmailStatus);
              session.setUserAcCode(accCode);
              session.setUserAcNo(userAcNo);
              session.setUserBalance(userBalance);
              session.setUserAcName(accName);
              session.setUserMonthlyLimit(accMonthlyLimit);
              session.setUserDailyLimit(accDailyLimit);
              session.setUserBalanceLimit(accBalanceLimit);
              session.setUserImage(images);
              session.setUserAddress(userAddress);
              session.setUserGender(userGender);
              session.setUserDob(userDob);
              session.setMPin(isMPin);
              session.setHasRefer(hasRefer);
              session.setIsValid(1);
              session.setVnetEnable(vnetEnable);
              session.setEbsEnable(ebsEnable);
              session.setRazorPayEnable(razorPayEnable);
              session.setNikiOffer(nikiOffer);
              session.setUserPoints(userPoints);
              session.setIplEnable(iplEnable);
              session.setIplMyPrediction(iplMyPrediction);
              session.setIplPrediction(iplPrediction);
              session.setMdexKey(mdexKey);
              session.setIplSchedule(iplSchedule);
              session.setMdexToken(mdexToken);

              try {
                finishAffinity();
              } catch (NullPointerException e) {

              }
              overridePendingTransition(0, 0);
              startActivity(getIntent());
              overridePendingTransition(0, 0);
              isChanged = true;
            } else {
              assert code != null;
              if (code.equalsIgnoreCase("F03")) {
                isChanged = true;
                showInvalidSessionDialog(message);
              }
            }
          } catch (JSONException e) {
            e.printStackTrace();
            try {

              loadDlg.dismiss();
            } catch (NullPointerException e1) {
            }
          }
        }

        @Override
        public void onError(ANError anError) {
          try {

            loadDlg.dismiss();
          } catch (NullPointerException e) {
          }


        }
      });
    }
  }

  public void showInvalidSessionDialog(String message) {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(HomeMainActivity.this, R.string.dialog_title2, message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        promoteLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }


//  private void getCartItemsOld() {
//    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_GET_CART + session.getUserMobileNo(), (String) null,
//      new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject response) {
//          try {
//            if (response.has("retrieveorder")) {
//              JSONArray productJArray = response.getJSONArray("retrieveorder");
//              for (int i = 0; i < productJArray.length(); i++) {
//                JSONObject c = productJArray.getJSONObject(i);
//                String pId = c.getString("prodid");
//
//                Select specificShopQueryGt = Select.from(ShoppingModel.class).where(Condition.prop("pid").eq(pId));
//                ShoppingModel spM = (ShoppingModel) specificShopQueryGt.first();
//
//                if (spM != null) {
//                  shoppingArray.add(spM);
//                }
//              }
//              if (shoppingArray.size() != 0) {
//                pqCart.createNewCart(shoppingArray);
//              }
//            }
//          } catch (JSONException e) {
//            e.printStackTrace();
//          }
//
//        }
//      }, new Response.ErrorListener() {
//
//      @Override
//      public void onErrorResponse(VolleyError error) {
//      }
//    }) {
//
//    };
//    PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
//
//  }

  //
//  public void getCartItems() {
////        loadDlg.show();
//    JSONObject jsonRequest = new JSONObject();
//    try {
//      jsonRequest.put("mobile", session.getUserMobileNo());
//
//    } catch (JSONException e) {
//      e.printStackTrace();
//    }
//
//    if (jsonRequest != null) {
//      Log.i("SHOWCARTURL", ApiUrl.URL_SHOPPONG_SHOW_CART);
//      Log.i("SHOWCARTREQ", jsonRequest.toString());
//      postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_SHOW_CART, jsonRequest, new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject jsonObj) {
//          try {
//            Log.i("SHOWCARTRES", jsonObj.toString());
//            String code = jsonObj.getString("code");
//            loadDlg.dismiss();
//            if (code != null && code.equals("S00")) {
//              shoppingArray.clear();
//              pqCart.clearCart();
//              JSONArray productJArray = jsonObj.getJSONArray("response");
//
//              for (int i = 0; i < productJArray.length(); i++) {
//                JSONObject c = productJArray.getJSONObject(i);
//                long qty = c.getLong("quantity");
//                JSONObject product = c.getJSONObject("product");
//                long pId = product.getLong("id");
////                                long price = c.getLong("price");
////                                long quantity = c.getLong("quantity");
////                                long cartId = c.getLong("cartId");
////                                String status = c.getString("status");
////                                String pName = c.getString("productName");
////                                String fileName = c.getString("fileName");
////                                long totalprice = c.getLong("totalprice");
////                                long userId = c.getLong("userId");
////                                PQCartModel sModel = new PQCartModel(pId, pName, "", price, "http://66.207.206.54:8034/PrayQwikrWeb1/resources/images/uploaded-images/" + fileName, status, 0, "", "", "");
////                                sModel.save();
////                                long pId = c.getLong("productId");
//                Select specificShopQueryGt = Select.from(ShoppingModel.class).where(Condition.prop("pid").eq(String.valueOf(pId)));
//                ShoppingModel spM = (ShoppingModel) specificShopQueryGt.first();
//                if (spM != null) {
//                  Log.i("value", String.valueOf(spM.getPid()));
//                  spM.setpQty(qty);
//                  spM.save();
//                  shoppingArray.add(spM);
//                }
//              }
//              if (shoppingArray.size() != 0) {
//                pqCart.setProductsInCart(pqCart.createNewCart(shoppingArray));
//                Log.i("valuess", String.valueOf(pqCart.getProductsInCartArray().size()));
//              }
////                    }
////                    if (shoppingArray.size() != 0) {
////                        pqCart.createNewCart(shoppingArray);
////                    }
//              loadDlg.dismiss();
//            }
//
//          } catch (JSONException e) {
//            loadDlg.dismiss();
//            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//          }
//        }
//      }, new Response.ErrorListener()
//
//      {
//        @Override
//        public void onErrorResponse(VolleyError error) {
//          loadDlg.dismiss();
//          error.printStackTrace();
//          CustomToast.showMessage(HomeMainActivity.this, NetworkErrorHandler.getMessage(error, HomeMainActivity.this));
//        }
//      })
//
//      {
//        @Override
//        public Map<String, String> getHeaders() throws AuthFailureError {
//          HashMap<String, String> map = new HashMap<>();
//          map.put("hash", "1234");
//          return map;
//        }
//
//      }
//
//      ;
//      int socketTimeout = 120000;
//
//      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//      postReq.setRetryPolicy(policy);
//      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//    }
//  }


  private void promoteLogout() {

    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
    } catch (JSONException e) {
//            e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      AndroidNetworking.post(ApiUrl.URL_LOGOUT)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          loadDlg.dismiss();
          try {
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              if (NikiConfig.isNikiInitialized()) {
                NikiConfig.getInstance().logout(HomeMainActivity.this);
              }
//              Checkout.clearUserData(HomeMainActivity.this);
              SharedPreferences kycValues = getSharedPreferences("kyc", Context.MODE_PRIVATE);
              kycValues.edit().clear().remove("KYC").apply();
              UserModel.deleteAll(UserModel.class);
              BusSaveModel.deleteAll(BusSaveModel.class);
              FlightSaveModel.deleteAll(FlightSaveModel.class);
              loadDlg.dismiss();
              Intent i = new Intent(getApplicationContext(), LoginActivity.class);
              i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
              finish();
              finishAffinity();
              startActivity(i);
            } else {
//              Checkout.clearUserData(HomeMainActivity.this);
              if (NikiConfig.isNikiInitialized()) {
                NikiConfig.getInstance().logout(HomeMainActivity.this);
              }
              SharedPreferences kycValues = getSharedPreferences("kyc", Context.MODE_PRIVATE);
              kycValues.edit().clear().remove("KYC").apply();
              loadDlg.dismiss();
              UserModel.deleteAll(UserModel.class);
              BusSaveModel.deleteAll(BusSaveModel.class);
              FlightSaveModel.deleteAll(FlightSaveModel.class);
              loadDlg.dismiss();

              Intent i = new Intent(HomeMainActivity.this, LoginActivity.class);
              i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
              finish();
              finishAffinity();
              startActivity(i);
            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(HomeMainActivity.this, getResources().getString(R.string.server_exception));
            e.printStackTrace();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          CustomToast.showMessage(HomeMainActivity.this, getResources().getString(R.string.server_exception));
        }
      });
    }
  }


  private void checkLangPref() {
    switch (language) {
      case "English":
        setLocale("en");
        break;
      case "Hindi":
        setLocale("hn");
        break;
      case "Kannada":
        setLocale("kd");
        break;
      case "Marathi":
        setLocale("mrt");
        break;
      case "Telgu":
        setLocale("tel");
        break;
      case "Malyalam":
        setLocale("mly");
        break;
      case "Tamil":
        setLocale("tml");
        break;
      case "Bengali":
        setLocale("bgl");
        break;
      case "Gujrati":
        setLocale("grt");
        break;
      default:
        setLocale("en");
        showLangDialog();
    }

  }

  public void setLocale(String lang) {
    Locale locale = new Locale(lang);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
  }

  @Override
  protected void onDestroy() {
    if (loadDlg != null) {
      loadDlg.dismiss();
      loadDlg = null;
    }
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    super.onDestroy();
  }

  @Override
  protected void onResume() {
    super.onResume();
    View view = getCurrentFocus();
    if (view != null) {
      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    if (isChanged) {
      setNavigationHeader();
      setViews();
    }
  }

  private Runnable navigationTask = new Runnable() {
    @Override
    public void run() {
      setViews();
    }
  };

  private Bitmap decodeFromBase64ToBitmap(String encodedImage)

  {
    try {
      byte[] decodedString = Base64.decode(encodedImage, Base64.NO_WRAP);
      ByteArrayInputStream bais = new ByteArrayInputStream(decodedString);
      Log.d("TAG", "byte array input stream size: " + bais.available());

      Bitmap decodedBitmap = BitmapFactory.decodeStream(bais);
      return decodedBitmap;
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
      return null;
    }

//        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

  }

  @Override
  protected void onPause() {
    super.onPause();
    if (isFinishing()) {
      if (alertDialog != null) {
        alertDialog.dismiss();
        alertDialog = null;
      }
    }
  }
}
