package in.msewa.ecarib.activity.housejoy;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import io.appsfly.core.models.AppsFlyClientConfig;
import io.appsfly.core.providers.AppsFlyProvider;
import io.appsfly.core.services.Callback;
import io.appsfly.microapp.MicroAppLauncher;
//import io.appsfly.core.providers.AppsFlyProvider;

public class HJPaymentActivity extends AppCompatActivity {

  private UserModel session = UserModel.getInstance();

  private LoadingDialog loadDlg;

  //HJ onActivityResult
  private String amount;
  private String transactionRefNo;


  private String serviceName;
  private String hJtransactionRefNo;
  private String JobId;
  private String date;
  private String time;
  private String customerName;
  private String mobile;
  private String address;
  private boolean payment_offline;

  private String merchantUserName = "houseJoy@msewa.com";

  //Volley Tag
  private String tag_json_obj = "json_invite_freinds";
  private JSONObject jsonRequest;
  private JSONObject userContextData;
  private String parameters;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_hjpayment);

    loadDlg = new LoadingDialog(HJPaymentActivity.this);

    amount = getIntent().getStringExtra("amount");
    transactionRefNo = getIntent().getStringExtra("transactionRefNo");
    parameters = getIntent().getStringExtra("parameters");
    Log.i("paramters", parameters);

    ArrayList<AppsFlyClientConfig> appsFlyClientConfigs = new ArrayList<AppsFlyClientConfig>();
    AppsFlyClientConfig appsflyConfig = new AppsFlyClientConfig(HJPaymentActivity.this, AppMetadata.hj_microapp_id, AppMetadata.hj_execution_url);
    appsFlyClientConfigs.add(appsflyConfig);
    AppsFlyProvider.getInstance().initialize(appsFlyClientConfigs, this, new Callback<Void>() {
      @Override
      public void send(Void aVoid) {
        try {
          MicroAppLauncher.pushApp(AppMetadata.hj_microapp_id, AppMetadata.hj_payment_intent_string, new JSONObject(parameters), HJPaymentActivity.this);
        } catch (JSONException e) {
          e.printStackTrace();
        }
      }
    });

  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    JSONObject resultData;
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && requestCode == 9200) {
      String dataStr = data.getStringExtra("postData");
      Log.i("HJRC1", "" + resultCode);
      Log.i("HJDATA1", dataStr);
      try {
        resultData = new JSONObject(dataStr);
        hJtransactionRefNo = resultData.getString("transactionRefNo");
        JobId = resultData.getString("JobId");
        date = resultData.getString("date");
        time = resultData.getString("time");
        customerName = resultData.getString("customerName");
        mobile = resultData.getString("mobile");
        address = resultData.getString("mobile");
        payment_offline = resultData.getBoolean("payment_offline");
        serviceName = resultData.getString("serviceName");
        if (!JobId.equals("") && JobId != null || !hJtransactionRefNo.equals("") && hJtransactionRefNo != null) {
          successHjPayment();
          Log.i("HJRC1", "success called 1");
        } else {
          cancelHjPayment();
          Log.i("HJRC1", "cancel called 1");
        }
      } catch (JSONException e) {
        e.printStackTrace();
      }
      //Get values from resultData with keys specified by the MicroApp Service Provider .
    } else if (resultCode == RESULT_CANCELED && requestCode == 9200) {
      Log.i("HJRCCANCEL1", "" + resultCode);
      if (data != null) {
        String dataStr = data.getStringExtra("result");
        Log.i("HJDATACANCEL1", dataStr);
      }
//            cancelHjPayment();
    }
  }

  private void successHjPayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("transactionRefNo", transactionRefNo);
      jsonRequest.put("jobId", JobId);
      jsonRequest.put("date", date);
      jsonRequest.put("time", time);
      jsonRequest.put("customerName", customerName);
      jsonRequest.put("mobile", mobile);
      jsonRequest.put("mobileNo", session.getUserMobileNo());
      jsonRequest.put("address", address);
      jsonRequest.put("paymentOffline", payment_offline);
      jsonRequest.put("merchantUserName", merchantUserName);
      jsonRequest.put("serviceName", serviceName);
      jsonRequest.put("amount", amount);
      jsonRequest.put("hjtxnRefNo", hJtransactionRefNo);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("HJS1 Request", jsonRequest.toString());
      Log.i("HJS1 URL", ApiUrl.URL_HJ_SUCCESS_TXN);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_HJ_SUCCESS_TXN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          loadDlg.dismiss();
          try {
            Log.i("HJS1 Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");

            if (code != null && code.equals("S00")) {
              sendRefresh();
              CustomSuccessDialog builder = new CustomSuccessDialog(HJPaymentActivity.this, "Booking Successful", "You will get your booking details in SMS & Email shortly.");
              builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  dialog.dismiss();
                  finish();
                }
              });
              builder.show();

            } else if (code.equals("F03")) {
              showInvalidSessionDialog();
            } else {
              CustomToast.showMessage(HJPaymentActivity.this, message);
              finish();
            }
          } catch (JSONException e) {
            CustomToast.showMessage(HJPaymentActivity.this, getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(HJPaymentActivity.this, NetworkErrorHandler.getMessage(error, HJPaymentActivity.this));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void cancelHjPayment() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("transactionRefNo", transactionRefNo);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }
    if (jsonRequest != null) {
      Log.i("HJS1 Request", jsonRequest.toString());
      Log.i("HJS1 URL", ApiUrl.URL_HJ_SUCCESS_TXN);
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_HJ_SUCCESS_TXN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          loadDlg.dismiss();
          try {
            Log.i("HJS1 Response", response.toString());
            String code = response.getString("code");
            String message = response.getString("message");

            if (code != null && code.equals("S00")) {

              CustomSuccessDialog builder = new CustomSuccessDialog(HJPaymentActivity.this, "Payment failed.", "Your booking is not completed.");
              builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  dialog.dismiss();
                  finish();
                }
              });
              builder.show();

            } else if (code.equals("F03")) {
              showInvalidSessionDialog();
            } else {
              CustomToast.showMessage(HJPaymentActivity.this, message);
              finish();
            }
          } catch (JSONException e) {
            CustomToast.showMessage(HJPaymentActivity.this, getResources().getString(R.string.server_exception2));
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          CustomToast.showMessage(HJPaymentActivity.this, NetworkErrorHandler.getMessage(error, HJPaymentActivity.this));
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "1234");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }

  private void sendRefresh() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "2");
    LocalBroadcastManager.getInstance(HJPaymentActivity.this).sendBroadcast(intent);
  }

  public void showInvalidSessionDialog() {
    CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.show();
  }

  private void sendLogout() {
    finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
  }

}
