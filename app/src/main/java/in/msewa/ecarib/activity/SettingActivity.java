package in.msewa.ecarib.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 3/25/2016.
 */
public class SettingActivity extends AppCompatActivity {
  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private static String appVersion;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_setting);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);

    try {
      PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
      appVersion = pinfo.versionName;
    } catch (Exception e) {
      e.printStackTrace();
    }

    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
//        getFragmentManager().beginTransaction().replace(R.id.flSettingContent, new MyPreferenceFragment()).commit();

  }

  public static class MyPreferenceFragment extends PreferenceFragment {
    private Preference prefCreateMPin;
    private Preference prefForgotMPin;
    private Preference prefChangeMPin;
    private Preference prefChangePassword;
    private Preference prefVerifyEmail;
    private Preference prefAppVersion;
    private Preference prefAppSignOut;
    private UserModel session = UserModel.getInstance();

    private Preference prefEditProfile;
    private RequestQueue rq;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.activity_settings);
      try {
        final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(getActivity());
        rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
      } catch (KeyManagementException e) {
        e.printStackTrace();
      } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
      loadDlg = new LoadingDialog(getActivity());

      prefEditProfile = getPreferenceManager().findPreference("prefEditProfile");
      prefCreateMPin = getPreferenceManager().findPreference("prefCreateMPin");
      prefForgotMPin = getPreferenceManager().findPreference("prefForgotMPin");
      prefChangeMPin = getPreferenceManager().findPreference("prefChangeMPin");
      prefChangePassword = getPreferenceManager().findPreference("prefChangePassword");
      prefVerifyEmail = getPreferenceManager().findPreference("prefVerifyEmail");
      prefAppVersion = getPreferenceManager().findPreference("prefAppVersion");
      prefAppSignOut = getPreferenceManager().findPreference("prefAppSignOut");
      prefAppVersion.setSummary(appVersion);

      if (session.isMPin) {
        prefCreateMPin.setEnabled(false);
        prefChangeMPin.setEnabled(true);
        prefForgotMPin.setEnabled(true);
      } else {
        prefCreateMPin.setEnabled(true);
        prefChangeMPin.setEnabled(false);
        prefForgotMPin.setEnabled(false);
      }

      if (session.emailIsActive.equalsIgnoreCase("Active")) {
        prefVerifyEmail.setEnabled(false);
      } else {
        prefVerifyEmail.setEnabled(true);
      }


      prefEditProfile.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          startActivity(new Intent(getActivity(), EditProfileActivity.class));
          return false;
        }
      });

      prefAppSignOut.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          sendLogoutApp();
          return false;
        }
      });

      prefChangePassword.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          startActivity(new Intent(getActivity(), ChangeCurrentPwdActivity.class));
          return false;
        }
      });

      prefCreateMPin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          startActivity(new Intent(getActivity(), CreatMPinActivity.class).putExtra("IntentType", "Setting"));
          return false;
        }
      });

      prefForgotMPin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          startActivity(new Intent(getActivity(), ForgetMPinActivity.class));
          return false;
        }
      });

      prefChangeMPin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          startActivity(new Intent(getActivity(), ChangeMPinActivity.class));
          return false;
        }
      });

      prefVerifyEmail.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          showEmailDialog(generateVerifyMessage());
          return false;
        }
      });
    }


    public void showCustomDialog(String message, int type) {
      CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
      if (type == 1) {
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
            loadDlg.show();
            deleteMPin();
          }
        });
      }

      builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.dismiss();
        }
      });
      builder.show();
    }

    public void showEmailDialog(String message) {
      CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
      builder.setPositiveButton("Resend", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.dismiss();
          resendCode();
        }
      });
      builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.dismiss();
        }
      });
      builder.show();
    }


    public String generateDeleteMessage() {
      String source = "<b><font color=#000000> This will delete your MPin and delete your active session </font></b>" +
        "<br><b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
      return source;
    }

    public String generateVerifyMessage() {
      String source = "<b><font color=#000000> Please check your inbox and verify email.</font></b>" +
        "<br><br><b><font color=#ff0000> You can check registered email address in edit profile section. </font></b><br></br>";
      return source;
    }

    private void deleteMPin() {
      jsonRequest = new JSONObject();
      try {
        jsonRequest.put("sessionId", session.getUserSessionId());

      } catch (JSONException e) {
        e.printStackTrace();
        jsonRequest = null;
      }

      if (jsonRequest != null) {
        Log.i("JsonRequest", jsonRequest.toString());
        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_DELETE_M_PIN, jsonRequest, new Response.Listener<JSONObject>() {
          @Override
          public void onResponse(JSONObject response) {
            try {
              Log.i("Delete PIN RESPONSE", response.toString());
              String code = response.getString("code");
              if (code != null && code.equals("S00")) {
                loadDlg.dismiss();
                getActivity().finish();
                //logout here
                sendLogout();


              } else if (code != null && code.equals("F03")) {
                loadDlg.dismiss();
                showInvalidSessionDialog();
              } else {
                loadDlg.dismiss();
                if (response.has("message") && response.getString("message") != null) {
                  String message = response.getString("message");
                  CustomToast.showMessage(getActivity(), message);
                } else {
                  CustomToast.showMessage(getActivity(), "Error message is null");
                }

              }

            } catch (JSONException e) {
              loadDlg.dismiss();
              e.printStackTrace();
              Toast.makeText(getActivity(), "Error connecting to server", Toast.LENGTH_SHORT).show();

            }
          }
        }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            loadDlg.dismiss();
            error.printStackTrace();

          }
        }) {
          @Override
          public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("hash", "1234");
            return map;
          }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(postReq, "test");
      }
    }


    private void resendCode() {
      loadDlg.show();
      StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_RESEND_EMAIL, new Response.Listener<String>() {
        @Override
        public void onResponse(String responseString) {
          try {
            Log.i("url", ApiUrl.URL_RESEND_EMAIL);

            Log.i("JsonResponse", responseString.toString());
            JSONObject response = new JSONObject(responseString);

            String code = response.getString("code");
            String message = response.getString("message");
            if (code != null && code.equals("S00")) {
              loadDlg.dismiss();
              CustomToast.showMessage(getActivity(), message);

            } else {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            }

          } catch (JSONException e) {
            loadDlg.dismiss();
            e.printStackTrace();
            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          loadDlg.dismiss();
          error.printStackTrace();
          CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

        }
      }) {
        @Override
        protected Map<String, String> getParams() {
          Map<String, String> params = new HashMap<>();
          params.put("sessionId", session.getUserSessionId());
          Log.i("sessionId", session.getUserSessionId());
          return params;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, "test");

    }


    private void sendLogout() {
      Intent intent = new Intent("setting-change");
      intent.putExtra("updates", "4");
      LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogoutApp() {
      Intent intent = new Intent("setting-change");
      intent.putExtra("updates", "5");
      LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
      getActivity().finish();
    }


    public void showInvalidSessionDialog() {
      CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
      builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          sendLogout();
        }
      });
      builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.dismiss();

        }
      });
      builder.show();
    }


  }


  public static final int getColors(Context context, int id) {
    final int version = Build.VERSION.SDK_INT;
    if (version >= 23) {
      return ContextCompat.getColor(context, id);
    } else {
      return context.getResources().getColor(id);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    getFragmentManager().beginTransaction().replace(R.id.flSettingContent, new MyPreferenceFragment()).commit();
  }
}
