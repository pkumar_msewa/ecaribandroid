package in.msewa.ecarib.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.niki.config.NikiConfig;
import com.niki.config.SessionConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomBlockDialog;
import in.msewa.custom.CustomSuccessDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.VerifyLoginOTPDialog;
import in.msewa.fragment.LoadMoneyFragment;
import in.msewa.fragment.fragmentipl.IplFragment;
import in.msewa.fragment.fragmentnaviagtionitems.AdventureCatFragment;
import in.msewa.fragment.fragmentnaviagtionitems.BankTransferFragment;
import in.msewa.fragment.fragmentnaviagtionitems.BillPaymentFragment;
import in.msewa.fragment.fragmentnaviagtionitems.DonationByQrFragment;
import in.msewa.fragment.fragmentnaviagtionitems.FundTransferFragment;
import in.msewa.fragment.fragmentnaviagtionitems.InviteFreindFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MVisaFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MerchantPayByQrFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MerchantPayFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MobileTouUpFragment;
import in.msewa.fragment.fragmentnaviagtionitems.MyCouponsFragment;
import in.msewa.fragment.fragmentnaviagtionitems.OfferLisFragment;
import in.msewa.fragment.fragmentnaviagtionitems.QwikPaymentFragment;
import in.msewa.fragment.fragmentnaviagtionitems.ReceiptFragment;
import in.msewa.fragment.fragmentnaviagtionitems.RequestMoneyFragment;
import in.msewa.fragment.fragmentnaviagtionitems.SharePointsFragment;
import in.msewa.fragment.fragmentnaviagtionitems.SplitMoneyFragment;
import in.msewa.fragment.fragmentnaviagtionitems.TravelFragment;
import in.msewa.fragment.fragmentnaviagtionitems.TravelHealthFragment;
import in.msewa.fragment.fragmentnaviagtionitems.TravelKhanaFragment;
import in.msewa.fragment.fragmentnaviagtionitems.YuppTvFragment;
import in.msewa.fragment.fragmenttravel.BusTravelFragment;
import in.msewa.fragment.fragmenttravel.FlightTravelFragment;
import in.msewa.fragment.paymentlimit.PaymentSetLimitFragment;
import in.msewa.fragment.treatcard.TreatcardFragment;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.SplitMoneyGroupModel;
import in.msewa.model.UserModel;
import in.msewa.util.AddGroupsListner;
import in.msewa.util.AddRemoveCartListner;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.util.VerifyLoginOTPListner;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.ecarib.activity.shopping.ShoppingActivity;

//import com.fss.controller.MainActivity;

public class MainMenuDetailActivity extends AppCompatActivity implements AddGroupsListner {
  private static AddRemoveCartListner mListener;
  private String type;
  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private boolean toFinish = false;

  //split payment
  private LoadingDialog loadDlg;
  private String globalURL = "";
  private String globalObjectString = "";
  private JSONObject jsonObj = new JSONObject();
  private UserModel session = UserModel.getInstance();
  private String tag_json_obj = "json_topup_pay";
  private String serviceProviderName = "";
  private String toMobileNumber = "";
  private String amount = "";

  private String tokenKeyNikki, secretKeyNikki;
  private JSONObject jsonRequest, extraJson;
  //FIREBASE
  public static final String PROPERTY_REG_ID = "registration_id";
  private String regId = "";
  SharedPreferences loadMoneyPref;
  private String paymentLimitType;
  private boolean editValid = false;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_telebuy_pay);

    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("loadMoney-done"));

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    try {
      loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
    } catch (NullPointerException e) {

    }
    loadDlg = new LoadingDialog(MainMenuDetailActivity.this);
    type = getIntent().getStringExtra(AppMetadata.FRAGMENT_TYPE);
    paymentLimitType = getIntent().getStringExtra("paymentlimittype");

    if (type.equals("MobileTopUp")) {
      MobileTouUpFragment fragment = new MobileTouUpFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();

    } else if (type.equals("BillPayment")) {
      BillPaymentFragment fragment = new BillPaymentFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("FundTransfer")) {
      FundTransferFragment fragment = new FundTransferFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("Travel")) {
      TravelFragment fragment = new TravelFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("MerchantPay")) {
      MerchantPayFragment fragment = new MerchantPayFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("QRPay")) {
      MerchantPayByQrFragment fragment = new MerchantPayByQrFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("QwikPay")) {
      QwikPaymentFragment fragment = new QwikPaymentFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("CouponsCode")) {
      OfferLisFragment fragment = new OfferLisFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("LoadMoney")) {
      LoadMoneyFragment fragment = new LoadMoneyFragment();
      String autofill = getIntent().getStringExtra("AutoFill");
      Bundle params = new Bundle();
      params.putString("AutoFill", autofill);
      SharedPreferences.Editor editor = loadMoneyPref.edit();
      editor.clear();
      editor.putString("type", "normal");
      editor.apply();

      if (autofill.equals("yes")) {
        editor.clear();
        editor.putString("type", "split");
        editor.apply();
        String splitAmount = getIntent().getStringExtra("splitAmount");
        params.putString("splitAmount", splitAmount);
        globalURL = getIntent().getStringExtra("URL");
        globalObjectString = getIntent().getStringExtra("REQUESTOBJECT");
        try {
          if (getIntent().getStringExtra("TYPE").equalsIgnoreCase("TOPUP")) {
            jsonObj = new JSONObject(globalObjectString);
            serviceProviderName = jsonObj.getString("serviceProvider");
            toMobileNumber = jsonObj.getString("mobileNo");
            amount = jsonObj.getString("amount");
          } else if (getIntent().getStringExtra("TYPE").equalsIgnoreCase("BILLPAY")) {
            jsonObj = new JSONObject(globalObjectString);
          } else if (getIntent().getStringExtra("TYPE").equalsIgnoreCase("QWICKPAY")) {
            jsonObj = new JSONObject(globalObjectString);
          }
          params.putString("URL", globalURL);
          params.putString("payment", jsonObj.toString());
        } catch (JSONException e) {
          e.printStackTrace();
        }
      }
      fragment.setArguments(params);
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("MyCoupons")) {
      MyCouponsFragment fragment = new MyCouponsFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("BankTransfer")) {
      BankTransferFragment fragment = new BankTransferFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("RequestMoney")) {
      RequestMoneyFragment fragment = new RequestMoneyFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("TermsAndCondition")) {
      TravelHealthFragment fragment = new TravelHealthFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      Bundle bType = new Bundle();
      bType.putString("URL", "https://www.vpayqwik.com/Terms&Conditions");
      fragment.setArguments(bType);
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("treatcard")) {
      TravelHealthFragment fragment = new TravelHealthFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      Bundle bType = new Bundle();
      bType.putString("URL", ApiUrl.URL_TREAT_CARD_WEBSITE);
      fragment.setArguments(bType);
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();

    } else if (type.equals("ipl")) {
      TravelHealthFragment fragment = new TravelHealthFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      Bundle bType = new Bundle();
      bType.putString("URL", ApiUrl.URL_TERMS_CONDITIONS);
      fragment.setArguments(bType);
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("Balance")) {
      ReceiptFragment fragment = new ReceiptFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("Points")) {
      SharePointsFragment fragment = new SharePointsFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("Bus Tickets")) {
      BusTravelFragment fragment = new BusTravelFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("FlightTickets")) {
      FlightTravelFragment fragment = new FlightTravelFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("Shopping")) {
      Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, ShoppingActivity.class);
      startActivity(shoppingIntent);
      finish();
    } else if (type.equals("healthCare")) {
      TravelHealthFragment fragment = new TravelHealthFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      Bundle bType = new Bundle();
      bType.putString("URL", "http://125.16.82.68:3780/?vpqw=1&utm_source=Vpayquik_easy_beta&utm_campaign=Vpayquik_easy_beta&utm_medium=buttom");
      fragment.setArguments(bType);
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("SplitMoney")) {
      SplitMoneyFragment fragment = new SplitMoneyFragment();
      Bundle b = new Bundle();
      b.putString("Type", "");
      fragment.setArguments(b);
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("InviteFriend")) {
      InviteFreindFragment fragment = new InviteFreindFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("MVisa")) {
      MVisaFragment fragment = new MVisaFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("Donation")) {
      DonationByQrFragment fragment = new DonationByQrFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("Adventure")) {
      AdventureCatFragment fragment = new AdventureCatFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("ChatBot")) {
      startNikkiChat();
    } else if (type.equals("Yupptv")) {
      YuppTvFragment fragment = new YuppTvFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equalsIgnoreCase("TreatCard")) {
      TreatcardFragment fragment = new TreatcardFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("travelkhana")) {
      TravelKhanaFragment fragment = new TravelKhanaFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("IPLPredication")) {
      IplFragment fragment = new IplFragment();
      Bundle bundle = new Bundle();
      bundle.putParcelable("IplDetails", getIntent().getParcelableExtra("IplDetails"));
      fragment.setArguments(bundle);
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equals("ipl")) {
      TravelHealthFragment fragment = new TravelHealthFragment();
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      Bundle bType = new Bundle();
      bType.putString("URL", ApiUrl.URL_CAMPAGIN);
      fragment.setArguments(bType);
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    } else if (type.equalsIgnoreCase("PaymentLimitFragment")) {
      PaymentSetLimitFragment fragment = new PaymentSetLimitFragment();
      editValid = true;
      Bundle bundle = new Bundle();
      bundle.putString("paymentlimittype", paymentLimitType);
      fragment.setArguments(bundle);
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.frameInCart, fragment);
      fragmentTransaction.commit();
    }
  }

  private void startNikkiChat() {
    loadDlg.show();
    jsonRequest = new JSONObject();
    try {
      jsonRequest.put("sessionId", session.getUserSessionId());

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_NIKKI_GET_TOKEN, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            String messageAPI = response.getString("message");
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              loadDlg.dismiss();
              String detailsStr = response.getString("details");
              JSONObject details = new JSONObject(detailsStr);
              tokenKeyNikki = details.getString("tokenKey");
              secretKeyNikki = details.getString("secretKey");
              try {
                extraJson = new JSONObject();
                extraJson.put("tokenKey", tokenKeyNikki);
                extraJson.put("secretKey", secretKeyNikki);
              } catch (JSONException e) {
                e.printStackTrace();
              }

              if (NikiConfig.isNikiInitialized()) {
                NikiConfig.getInstance().startNikiChat(MainMenuDetailActivity.this, extraJson);
                finish();
              } else {
                try {
                  JSONObject nikkiJson = new JSONObject();
                  nikkiJson.put("name", session.getUserFirstName());
                  nikkiJson.put("email", session.getUserEmail());
                  nikkiJson.put("phoneNumber", session.getUserMobileNo());
                  regId = getRegistrationId();
                  if (regId != null && !regId.isEmpty()) {
                    nikkiJson.put("regId", regId);
                  }

                  SessionConfig config = new SessionConfig.Builder()
                    .setAccessKey(AppMetadata.appKey)//Mandatory
                    .setSecret(AppMetadata.appSecret)//Mandatory
                    .setParentTheme(false)//Optional (will work only if Base App is using AppCompact Theme, else keep it false only.)
                    .setBackIconResourceId(R.drawable.ic_back)//Optional
                    .setTitleColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null))//Optional
                    .setMerchantTitle("VPayQWik")//Optional
                    .setLogoResourceId(R.drawable.toolbart_niki)//Optional (To set logo in the header)
                    .setLogoGravity(Gravity.CENTER) //Optional (To be used in case of horizontal logo to center align it)
                    .setUserData(nikkiJson)//Optional, See Configuring SDK section on how to create this
                    .build();
                  NikiConfig.initNiki(MainMenuDetailActivity.this, config);
                  finish();
                  NikiConfig.getInstance().startNikiChat(MainMenuDetailActivity.this, extraJson);

                } catch (JSONException e) {
                  e.printStackTrace();
                }
              }

            } else if (code != null && code.equals("F03")) {
              loadDlg.dismiss();
              showInvalidSessionDialog();
            } else {

              CustomToast.showMessage(MainMenuDetailActivity.this, messageAPI);
              loadDlg.dismiss();

            }
          } catch (JSONException e) {
            loadDlg.dismiss();
            CustomToast.showMessage(MainMenuDetailActivity.this, getResources().getString(R.string.server_exception2));
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          CustomToast.showMessage(MainMenuDetailActivity.this, NetworkErrorHandler.getMessage(error, MainMenuDetailActivity.this));
          loadDlg.dismiss();


        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          HashMap<String, String> map = new HashMap<>();
          map.put("hash", "12345");
          return map;
        }

      };
      int socketTimeout = 60000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      postReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }
  }


  private String getRegistrationId() {
    final SharedPreferences prefs = getGCMPreferences();
    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
    if (registrationId.isEmpty()) {
      return "";
    }
    return registrationId;
  }

  private SharedPreferences getGCMPreferences() {
    return getSharedPreferences("FIREBASE",
      Context.MODE_PRIVATE);
  }


  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
  }

  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getStringExtra("result");
      if (action.equals("1")) {
        toFinish = true;
      }

    }
  };


  @Override
  public void addGroupCompleted(String groupName, String groupAmount, String groupType, List<SplitMoneyGroupModel> splitMoneyGroupModels) {
    SplitMoneyFragment fragment = new SplitMoneyFragment();
    Bundle b = new Bundle();
    b.putString("Type", "Add");
    b.putString("GroupName", groupName);
    b.putString("GroupAmount", groupAmount);
    b.putString("GroupType", groupType);
    b.putParcelableArrayList("PeopleArray", (ArrayList<? extends Parcelable>) splitMoneyGroupModels);
    fragment.setArguments(b);
    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
    fragmentTransaction.replace(R.id.frameInCart, fragment);
    fragmentTransaction.commit();
  }

  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    super.onDestroy();
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (toFinish) {
      if (!globalURL.isEmpty() && !globalObjectString.isEmpty()) {
        try {
          promoteRecharge();
        } catch (JSONException e) {
          e.printStackTrace();
        }
      } else {
        finish();
      }

    } else {
    }
  }

  //split money
  public void promoteRecharge() throws JSONException {
    loadDlg = new LoadingDialog(MainMenuDetailActivity.this);
    loadDlg.show();
//    jsonRequest = new JSONObject();
//    try {
//      if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
//        jsonRequest.put("topupType", jsonObj.getString("topupType"));
//        jsonRequest.put("serviceProvider", jsonObj.getString("serviceProvider"));
//        jsonRequest.put("mobileNo", jsonObj.getString("mobileNo"));
//        jsonRequest.put("amount", jsonObj.getString("amount"));
//        if (!jsonObj.getString("topupType").equals("Postpaid")) {
//          jsonRequest.put("area", jsonObj.getString("area"));
//        }
//        jsonRequest.put("sessionId", jsonObj.getString("sessionId"));
//      } else if (getIntent().getStringExtra("TYPE").equals("BILLPAY")) {
//        jsonRequest.put("serviceProvider", jsonObj.getString("serviceProvider"));
//        jsonRequest.put("amount", jsonObj.getString("amount"));
//        jsonRequest.put("sessionId", jsonObj.getString("sessionId"));
//        if (getIntent().getStringExtra("SUBTYPE").equals("DTH")) {
//          jsonRequest.put("dthNo", jsonObj.getString("dthNo"));
//        } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
//          jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
//          jsonRequest.put("cycleNumber", jsonObj.getString("cycleNumber"));
//          jsonRequest.put("cityName", jsonObj.getString("cityName"));
//        } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
//          jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
//        } else if (getIntent().getStringExtra("SUBTYPE").equals("INSURANCE")) {
//          jsonRequest.put("policyNumber", jsonObj.getString("policyNumber"));
//          jsonRequest.put("policyDate", jsonObj.getString("policyDate"));
//        } else if (getIntent().getStringExtra("SUBTYPE").equals("LANDLINE")) {
//          jsonRequest.put("stdCode", jsonObj.getString("stdCode"));
//          jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
//          jsonRequest.put("landlineNumber", jsonObj.getString("landlineNumber"));
//        } else if (getIntent().getStringExtra("SUBTYPE").equalsIgnoreCase("GAS")) {
//          jsonRequest = jsonObj;
//        }
//
//      } else if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
//        jsonRequest = jsonObj;
//      }
//
//    } catch (JSONException e) {
//      e.printStackTrace();
//      jsonRequest = null;
//    }

//    if (jsonObj != null) {
    AndroidNetworking.post(globalURL)
      .addJSONObjectBody(new JSONObject(globalObjectString)) // posting json
      .setTag("test")
      .setPriority(Priority.HIGH)
      .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
      .build().setAnalyticsListener(new AnalyticsListener() {
      @Override
      public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
        Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
      }
    }).getAsJSONObject(new JSONObjectRequestListener() {
      @Override
      public void onResponse(JSONObject response) throws JSONException {
        try {
          String code = response.getString("code");

          if (code != null && code.equals("S00")) {
            loadDlg.dismiss();
            String jsonString = response.getString("response");
            try {
              if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
                loadDlg.dismiss();
                showSuccessDialog(new JSONObject(jsonString));
              } else {
                loadDlg.dismiss();
                showSuccessDialog(new JSONObject(jsonString));
              }
            } catch (NullPointerException e) {
              e.printStackTrace();
            }
          } else if (code != null && code.equals("F03")) {
            loadDlg.dismiss();
            showInvalidSessionDialog();
          } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
            loadDlg.dismiss();
            showBlockDialog();
          } else {
            loadDlg.dismiss();
            String values;
            if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
              values = "Recharge Failed";
            } else {
              values = "Payment Failed";
            }
            CustomAlertDialog builder = new CustomAlertDialog(MainMenuDetailActivity.this, R.string.dialog_title, values);
            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                sendRefresh();
              }
            });
            builder.show();
          }

        } catch (JSONException e) {
          loadDlg.dismiss();
          CustomAlertDialog builder = new CustomAlertDialog(MainMenuDetailActivity.this, R.string.dialog_title, getResources().getString(R.string.server_exception2));
          builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              dialogInterface.dismiss();
              sendRefresh();
            }
          });
          builder.show();
          e.printStackTrace();
        }
      }

      @Override
      public void onError(ANError anError) {
        loadDlg.dismiss();
        CustomAlertDialog builder = new CustomAlertDialog(MainMenuDetailActivity.this, R.string.dialog_title, getResources().getString(R.string.server_exception2));
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            sendRefresh();
          }
        });
        builder.show();
      }
    });
//    } else {
//      loadDlg.dismiss();
//      CustomAlertDialog builder = new CustomAlertDialog(MainMenuDetailActivity.this, R.string.dialog_title, getResources().getString(R.string.server_exception2));
//      builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//        @Override
//        public void onClick(DialogInterface dialogInterface, int i) {
//          dialogInterface.dismiss();
//
//          sendRefresh();
//        }
//      });
//      builder.show();
//    }
  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(MainMenuDetailActivity.this, R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendRefresh() {
    finish();
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(MainMenuDetailActivity.this).sendBroadcast(intent);
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(MainMenuDetailActivity.this).sendBroadcast(intent);
  }

  public void showSuccessDialog(final JSONObject jsonString) {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(getSuccessMessage());
    }

    try {
      if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
        CustomSuccessDialog builder = new CustomSuccessDialog(MainMenuDetailActivity.this, "Recharge Successful", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {

            String sucessMessage = null;
            try {
              sucessMessage = jsonString.getString("balance");
              session.setUserBalance(sucessMessage);
              session.save();
            } catch (JSONException e) {
              e.printStackTrace();
            }
            sendRefresh();
            dialog.dismiss();
            finish();
          }
        });
        builder.show();
      } else {
        CustomSuccessDialog builder = new CustomSuccessDialog(MainMenuDetailActivity.this, "Payment Successful", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            sendRefresh();
            dialog.dismiss();
            finish();
          }
        });
        builder.show();
      }

    } catch (NullPointerException e) {
      e.printStackTrace();
    }


  }

  public String getSuccessMessage() {
    String source = "";
    if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
      source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
        "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
    } else if (getIntent().getStringExtra("TYPE").equals("BILLPAY")) {
      try {
        if (getIntent().getStringExtra("SUBTYPE").equals("DTH")) {
          source =
            "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
              "<b><font color=#000000> DTH No: </font></b>" + "<font>" + jsonObj.getString("dthNo") + "</font><br>" +
              "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br><br>";
        } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
          String cycleNo = jsonObj.getString("cycleNumber");
          if (cycleNo != null && !cycleNo.isEmpty()) {
            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
              "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
              "<b><font color=#000000> Cycle No: </font></b>" + "<font>" + cycleNo + "</font><br>" +
              "<b><font color=#000000> Amount: </font></b>" + "<font>" + jsonObj.getString("amount") + "</font><br>";
          } else {
            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
              "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
              "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>";
          }
        } else if (getIntent().getStringExtra("SUBTYPE").equals("GAS")) {
          String payAmount = getIntent().getStringExtra("payAmount");
          String additionalCharges = getIntent().getStringExtra("AddCharge");
          source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
            "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
            "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>" +
            "<b><font color=#000000> Service Charge: </font></b>" + "<font>" + additionalCharges + "</font><br>" +
            "<b><font color=#000000> Total Charge: </font></b>" + "<font>" + payAmount + "</font><br><br>" +
            "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        } else if (getIntent().getStringExtra("SUBTYPE").equals("INSURANCE")) {
          source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
            "<b><font color=#000000> Policy No: </font></b>" + "<font>" + jsonObj.getString("policyNumber") + "</font><br>" +
            "<b><font color=#000000> Date: </font></b>" + "<font>" + jsonObj.getString("policyDate") + "</font><br>" +
            "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br><br>";
        } else if (getIntent().getStringExtra("SUBTYPE").equals("LANDLINE")) {
          String acNo = jsonObj.getString("accountNumber");
          if (acNo != null && !acNo.isEmpty()) {
            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
              "<b><font color=#000000> STD Code: </font></b>" + "<font>" + jsonObj.getString("stdCode") + "</font><br>" +
              "<b><font color=#000000> Phone No: </font></b>" + "<font>" + jsonObj.getString("landlineNumber") + "</font><br>" +
              "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>" +
              "<b><font color=#000000> Ac No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br><br>";
          } else {
            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
              "<b><font color=#000000> STD Code: </font></b>" + "<font>" + jsonObj.getString("stdCode") + "</font><br>" +
              "<b><font color=#000000> Phone No: </font></b>" + "<font>" + jsonObj.getString("landlineNumber") + "</font><br>" +
              "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>";
          }
        }

      } catch (JSONException e) {
        e.printStackTrace();
      }
    } else if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
      source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>Payment to Merchant Successful </font><br>";
    }

    return source;
  }

  public void showBlockDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getBlockSession());
    }
    CustomBlockDialog builder = new CustomBlockDialog(MainMenuDetailActivity.this, "Please contact customer care.", result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
        dialog.dismiss();
        finish();
      }
    });
    builder.show();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_OK && requestCode == 1) {
      final LoadingDialog loadDlg = new LoadingDialog(MainMenuDetailActivity.this);
      loadDlg.show();
      jsonRequest = new JSONObject();
      try {
        jsonRequest.put("sessionId", session.getUserSessionId());

      } catch (JSONException e) {
        e.printStackTrace();
        jsonRequest = null;
        loadDlg.dismiss();
      }

      AndroidNetworking.post(ApiUrl.URL_GET_USER_DETAILS)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .setPriority(Priority.HIGH)
        .setUserAgent("Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        .build().setAnalyticsListener(new AnalyticsListener() {
        @Override
        public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
          Log.i("timeTaken", String.valueOf(timeTakenInMillis) + isFromCache);
        }
      }).getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
          try {

            String code = response.getString("code");
            final String message = response.getString("message");
            loadDlg.dismiss();
            if (code != null && code.equals("S00")) {

              String jsonString = response.getString("response");
              JSONObject jsonObject = new JSONObject(jsonString);

              //New Implementation
              JSONObject jsonDetail = jsonObject.getJSONObject("details");
              String nikiOffer = jsonDetail.getString("nikiOffer");
              JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");
              double userBalanceD = accDetail.getDouble("balance");
              String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

              long userAcNoL = accDetail.getLong("accountNumber");
              String userAcNo = String.valueOf(userAcNoL);

              int userPointsI = accDetail.getInt("points");
              String userPoints = String.valueOf(userPointsI);


              JSONObject accType = accDetail.getJSONObject("accountType");

              String accName = accType.getString("name");
              String accCode = accType.getString("code");
              double accBalanceLimitD = accType.getDouble("balanceLimit");
              double accMonthlyLimitD = accType.getDouble("monthlyLimit");
              double accDailyLimitD = accType.getDouble("dailyLimit");

              String accBalanceLimit = String.valueOf(accBalanceLimitD);
              String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
              String accDailyLimit = String.valueOf(accDailyLimitD);

              JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
              String userFName = jsonUserDetail.getString("firstName");
              String userLName = jsonUserDetail.getString("lastName");
              String userMobile = jsonUserDetail.getString("contactNo");
              String userEmail = jsonUserDetail.getString("email");
              String userEmailStatus = jsonUserDetail.getString("emailStatus");

              String userAddress = jsonUserDetail.getString("address");
              String userImage = jsonUserDetail.getString("image");
              String encodedImage = jsonUserDetail.getString("encodedImage");
              boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");

              String userDob = jsonUserDetail.getString("dateOfBirth");
              String userGender = jsonUserDetail.getString("gender");
              boolean hasRefer = response.getBoolean("hasRefer");
              String images = "";
              if (!encodedImage.equals("")) {
                images = encodedImage;
              } else {
                images = userImage;
              }
              boolean iplEnable = false, ebsEnable = false, vnetEnable = false, razorPayEnable = false;
              String mdexToken = "", mdexKey = "", iplPrediction = "", iplSchedule = "", iplMyPrediction = "";
              if (response.has("iplEnable")) {
                iplEnable = response.getBoolean("iplEnable");
                mdexToken = response.getString("mdexToken");
                mdexKey = response.getString("mdexKey");
                iplPrediction = response.getString("iplPrediction");
                iplSchedule = response.getString("iplSchedule");
                iplMyPrediction = response.getString("iplMyPrediction");

              }
              if (response.has("ebsEnable") || response.has("vnetEnable") || response.has("razorPayEnable")) {
                ebsEnable = response.getBoolean("ebsEnable");
                vnetEnable = response.getBoolean("vnetEnable");
                razorPayEnable = response.getBoolean("razorPayEnable");
              }
              UserModel.deleteAll(UserModel.class);
              try {
                //Encrypting sessionId
                UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, images, userDob, userGender, isMPin, userPoints, hasRefer, nikiOffer, iplEnable, iplMyPrediction, iplPrediction, iplSchedule, mdexKey, mdexToken, ebsEnable, vnetEnable, razorPayEnable);
                userModel.save();
              } catch (Exception e) {
                e.printStackTrace();
              }


              session.setUserSessionId(session.getUserSessionId());
              session.setUserFirstName(userFName);
              session.setUserLastName(userLName);
              session.setUserMobileNo(userMobile);
              session.setUserEmail(userEmail);
              session.setEmailIsActive(userEmailStatus);
              session.setUserAcCode(accCode);
              session.setUserAcNo(userAcNo);
              session.setUserBalance(userBalance);
              session.setUserAcName(accName);
              session.setUserMonthlyLimit(accMonthlyLimit);
              session.setUserDailyLimit(accDailyLimit);
              session.setUserBalanceLimit(accBalanceLimit);
              session.setUserImage(images);
              session.setUserAddress(userAddress);
              session.setUserGender(userGender);
              session.setUserDob(userDob);
              session.setMPin(isMPin);
              session.setRazorPayEnable(razorPayEnable);
              session.setEbsEnable(ebsEnable);
              session.setVnetEnable(vnetEnable);
              session.setHasRefer(hasRefer);
              session.setIsValid(1);
              session.setNikiOffer(nikiOffer);
              session.setUserPoints(userPoints);
              session.setIplEnable(iplEnable);
              session.setIplMyPrediction(iplMyPrediction);
              session.setIplPrediction(iplPrediction);
              session.setMdexKey(mdexKey);
              session.setIplSchedule(iplSchedule);
              session.setMdexToken(mdexToken);
//                finish();
              loadDlg.dismiss();
              finish();
//                startActivity(new Intent(MainMenuDetailActivity.this, MainActivity.class));

            } else {
              assert code != null;
              if (code.equalsIgnoreCase("F03")) {
                loadDlg.dismiss();
                showInvalidSessionDialog();
              }
            }
          } catch (JSONException e) {
            e.printStackTrace();
            finish();
//              startActivity(new Intent(MainMenuDetailActivity.this, MainActivity.class));
            loadDlg.dismiss();
          }
        }

        @Override
        public void onError(ANError anError) {
          loadDlg.dismiss();
          anError.printStackTrace();

        }
      });

    } else if (resultCode == Activity.RESULT_CANCELED && requestCode == 1) {
      CustomAlertDialog customAlertDialog = new CustomAlertDialog(MainMenuDetailActivity.this, R.string.dialog_title, "Payment Failed");
      customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          dialogInterface.dismiss();
          startActivity(new Intent(MainMenuDetailActivity.this, HomeMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }
      });
      customAlertDialog.show();
    }
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.shopping_menu, menu);
    MenuItem menuItem = menu.findItem(R.id.menuCart);
    if (editValid) {
      menuItem.setVisible(true);
      SpannableString s = new SpannableString("Edit");
      s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, s.length(), 0);
      menuItem.setTitle(s);
    } else {
      menuItem.setVisible(false);
    }
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.menuCart) {
      final VerifyLoginOTPDialog verifyLoginOTPDialog = new VerifyLoginOTPDialog(MainMenuDetailActivity.this, new VerifyLoginOTPListner() {
        @Override
        public void onVerifySuccess() {
          mListener.taskCompleted("true");
        }

        @Override
        public void onVerifyError() {

        }
      });
//      verifyLoginOTPDialog.show();

    }
    return true;
  }

  public static void bindListener(AddRemoveCartListner listener) {
    mListener = listener;
  }
}
