package in.msewa.ecarib.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.AsteriskPasswordTransformationMethod;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.custom.TextDrawable;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.TLSSocketFactory;

/**
 * Created by Ksf on 5/6/2016.
 */
public class VerifyMPinActivity extends AppCompatActivity implements View.OnClickListener {
    private FloatingActionButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btndel;
    private EditText etMPin;
    private Button btnForgetPin;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();

    private LoadingDialog loadDlg;
    private RequestQueue rq;
    private TextWatcher textWatcher;
    private boolean isRunning = false;

    private String detailMessage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mpin);

        loadDlg = new LoadingDialog(VerifyMPinActivity.this);
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(VerifyMPinActivity.this);
            rq = Volley.newRequestQueue(VerifyMPinActivity.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        btn1 = (FloatingActionButton) findViewById(R.id.btn1);
        btn2 = (FloatingActionButton) findViewById(R.id.btn2);
        btn3 = (FloatingActionButton) findViewById(R.id.btn3);
        btn4 = (FloatingActionButton) findViewById(R.id.btn4);
        btn5 = (FloatingActionButton) findViewById(R.id.btn5);
        btn6 = (FloatingActionButton) findViewById(R.id.btn6);
        btn7 = (FloatingActionButton) findViewById(R.id.btn7);
        btn8 = (FloatingActionButton) findViewById(R.id.btn8);
        btn9 = (FloatingActionButton) findViewById(R.id.btn9);
        btn0 = (FloatingActionButton) findViewById(R.id.btn0);
        btndel = (FloatingActionButton) findViewById(R.id.btnDel);
        etMPin = (EditText) findViewById(R.id.etMPin);
        btnForgetPin = (Button) findViewById(R.id.btnForgetPin);

        etMPin.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        btn1.setImageDrawable(new TextDrawable(btn1.getContext(), "1", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn2.setImageDrawable(new TextDrawable(btn1.getContext(), "2", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn3.setImageDrawable(new TextDrawable(btn1.getContext(), "3", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn4.setImageDrawable(new TextDrawable(btn1.getContext(), "4", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn5.setImageDrawable(new TextDrawable(btn1.getContext(), "5", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn6.setImageDrawable(new TextDrawable(btn1.getContext(), "6", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn7.setImageDrawable(new TextDrawable(btn1.getContext(), "7", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn8.setImageDrawable(new TextDrawable(btn1.getContext(), "8", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn9.setImageDrawable(new TextDrawable(btn1.getContext(), "9", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));
        btn0.setImageDrawable(new TextDrawable(btn1.getContext(), "0", ColorStateList.valueOf(Color.WHITE), 72.f, TextDrawable.VerticalAlignment.BASELINE));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btndel.setImageDrawable(getResources().getDrawable(R.drawable.ic_refresh, null));
        } else {
            btndel.setImageDrawable(getResources().getDrawable(R.drawable.ic_refresh));
        }

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn0.setOnClickListener(this);
        btndel.setOnClickListener(this);

        btndel.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                etMPin.setText("");
                return false;
            }
        });

        btnForgetPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] items = {"+918025535857", "+918025505857"};
                android.support.v7.app.AlertDialog.Builder callDialog =
                        new android.support.v7.app.AlertDialog.Builder(VerifyMPinActivity.this, R.style.AppCompatAlertDialogStyle);
                callDialog.setTitle("Select a no. to call");
                callDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                callDialog.setItems(items, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int position) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + items[position].toString().trim()));

                        if (ActivityCompat.checkSelfPermission(VerifyMPinActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(callIntent);
                    }

                });

                callDialog.show();
            }
        });


        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 6) {
                    if (!isRunning)
                        verifyMPin();
                }

            }
        };

        etMPin.addTextChangedListener(textWatcher);


    }

    private void verifyMPin() {
        isRunning = true;
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("newMpin", etMPin.getText().toString());
            jsonRequest.put("username", session.getUserMobileNo());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VERIFY_M_PIN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        String jsonString = response.getString("response");
                        JSONObject jsonObject = new JSONObject(jsonString);

                        if(jsonObject.has("details")){
                            detailMessage = jsonObject.getString("details");
                        }

                        if (code != null && code.equals("S00")) {

                            //Verified
                            loadDlg.dismiss();
                            Intent mainIntent = new Intent(VerifyMPinActivity.this, HomeMainActivity.class);
                            startActivity(mainIntent);
                            finish();
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        }
                        else if (code != null && code.equals("F00")){
                            detailMessage = response.getString("message");
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), detailMessage, Toast.LENGTH_SHORT).show();

                        }

                        else {
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), detailMessage, Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(),getResources().getString(R.string.server_exception2));

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getApplicationContext(),getResources().getString(R.string.server_exception));



                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
          PayQwikApplication.getInstance().addToRequestQueue(postReq,"test");
        }
    }


    @Override
    public void onClick(View view) {
        Editable str = etMPin.getText();
        switch (view.getId()) {
            case R.id.btn1:
                str = str.append("1");
                etMPin.setText(str);
                break;
            case R.id.btn2:
                str = str.append("2");
                etMPin.setText(str);
                break;
            case R.id.btn3:
                str = str.append("3");
                etMPin.setText(str);
                break;
            case R.id.btn4:
                str = str.append("4");
                etMPin.setText(str);
                break;
            case R.id.btn5:
                str = str.append("5");
                etMPin.setText(str);
                break;
            case R.id.btn6:
                str = str.append("6");
                etMPin.setText(str);
                break;
            case R.id.btn7:
                str = str.append("7");
                etMPin.setText(str);
                break;
            case R.id.btn8:
                str = str.append("8");
                etMPin.setText(str);
                break;
            case R.id.btn9:
                str = str.append("9");
                etMPin.setText(str);
                break;
            case R.id.btn0:
                str = str.append("0");
                etMPin.setText(str);
                break;
            case R.id.btnDel:
                int length = etMPin.getText().length();
                if (length > 0) {
                    etMPin.getText().delete(length - 1, length);
                }
                isRunning = false;
                break;
        }

    }

    private void promoteLogout() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGOUT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        String code = jsonObj.getString("code");
                        if (code != null && code.equals("S00")) {
                            UserModel.deleteAll(UserModel.class);
                            loadDlg.dismiss();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        } else {
                            loadDlg.dismiss();
                            UserModel.deleteAll(UserModel.class);
                            loadDlg.dismiss();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
          PayQwikApplication.getInstance().addToRequestQueue(postReq,"test");
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(VerifyMPinActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                promoteLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
