package in.msewa.ecarib.activity.shopping.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import in.msewa.model.PQCart;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 4/6/2016.
 */
public class ShopFragOld extends Fragment{
    private PQCart cart = PQCart.getInstance();
    private ListView lvInCartItem;
    private Button btnBuyNext;
    private TextView tvTotalRupees;
    private View rootView;
    private double totalCost = 0.0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_prod_list, container, false);
//        tvTotalRupees = (TextView) rootView.findViewById(R.id.tvBuyTotalRupess);
//        btnBuyNext = (Button) rootView.findViewById(R.id.btnBuyNext);
//        lvInCartItem = (ListView) rootView.findViewById(R.id.hlvInCart);
//        lvInCartItem.setAdapter(new InCartAdapter(getActivity(),this));
//        updateView();

//        btnBuyNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                if (cart.getProductsInCart().size() == 0) {
//                    CustomToast.showMessage(getActivity(), "You have no products in cart.");
//                }
//                else{
//                    TeleBuyDeleveryFragment deliveryFragment = new TeleBuyDeleveryFragment();
//                    Bundle args = new Bundle();
//                    args.putDouble("total_amount", Double.parseDouble(tvTotalRupees.getText().toString().replace(",","")));
//                    deliveryFragment.setArguments(args);
//                    FragmentManager fragmentManager = getFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.addToBackStack(getTag());
//                    fragmentTransaction.replace(R.id.frameInCart, deliveryFragment);
//                    fragmentTransaction.commit();
//                }
//
//            }
//        });
        return rootView;
    }

//    private void updateView() {
//        tvTotalRupees.setText(cart.getTotalCost());
//    }

    private void clearCartBadge() {
        Intent intent = new Intent("cart-clear");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


}
