package in.msewa.ecarib.activity.imagicaActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.msewa.adapter.GiftCardItemAdapter;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.ThemeParkTicketModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;

/**
 * Created by kashifimam on 21/02/17.
 */

public class
ImagicaWaterParkList extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;
    private String sessid, session_name, token, aDate;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;
    private AQuery aq;
    private TextView tvRegularTitle, tvRegularDescrption, tvQtyRegularAdult, tvAmtRegularAdult,
            tvExpressTitle, tvExpressDescrption, tvQtyExpressAdult, tvAmtExpressAdult, tvAdult,tvEAdult;
    private ImageView ivRegularTicket, ivExpressTicket;
    private String regularTicketDescr, regularTitle, regularImageUrl, expressTicketDescr, expressTitle, expressImageUrl;
    private ThemeParkTicketModel regularAdult, expressAdult;
    private float regAdultAmount, expAdultAmount;
    private Button btnRegularAdultBook, btnExpressAdultBook;
    private CardView llRegularTickets, llExpressTickets;


    private GiftCardItemAdapter itemAdp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_park_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("imagica-booking-done"));
        loadDlg = new LoadingDialog(ImagicaWaterParkList.this);
        setSupportActionBar(toolbar);
        sessid = getIntent().getStringExtra("sessid");
        session_name = getIntent().getStringExtra("session_name");
        token = getIntent().getStringExtra("token");
        aDate = getIntent().getStringExtra("aDate");
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        llRegularTickets = (CardView) findViewById(R.id.llRegularTickets);
        llExpressTickets = (CardView) findViewById(R.id.llExpressTickets);
        tvAmtRegularAdult = (TextView) findViewById(R.id.tvAmtRegularAdult);
        tvRegularTitle = (TextView) findViewById(R.id.tvRegularTitle);
        tvRegularDescrption = (TextView) findViewById(R.id.tvRegularDescrption);
        tvQtyRegularAdult = (TextView) findViewById(R.id.tvQtyRegularAdult);
        tvExpressTitle = (TextView) findViewById(R.id.tvExpressTitle);
        tvExpressDescrption = (TextView) findViewById(R.id.tvExpressDescrption);
        tvQtyExpressAdult = (TextView) findViewById(R.id.tvQtyExpressAdult);
        tvAmtExpressAdult = (TextView) findViewById(R.id.tvAmtExpressAdult);
        tvAdult = (TextView) findViewById(R.id.tvAdult);
        tvEAdult = (TextView) findViewById(R.id.tvEAdult);

        ivRegularTicket = (ImageView) findViewById(R.id.ivRegularTicket);
        ivExpressTicket = (ImageView) findViewById(R.id.ivExpressTicket);

        btnRegularAdultBook = (Button) findViewById(R.id.btnRegularAdultBook);
        btnExpressAdultBook = (Button) findViewById(R.id.btnExpressAdultBook);

        tvQtyRegularAdult.setOnClickListener(this);
        tvQtyExpressAdult.setOnClickListener(this);


        btnRegularAdultBook.setOnClickListener(this);
        btnExpressAdultBook.setOnClickListener(this);


        getCardCategory();
    }

   private void setViews() {
       if (regularAdult != null) {
           tvRegularTitle.setText(regularTitle);
           AQuery aq = new AQuery(ImagicaWaterParkList.this);
//           if (regularImageUrl != null && !regularImageUrl.isEmpty()) {
//               aq.id(ivRegularTicket).image(regularImageUrl, true, true);
//           } else {
//               aq.id(ivRegularTicket).background(R.drawable.image_imagica_theme);
//           }
           if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
               tvRegularDescrption.setText(Html.fromHtml(regularTicketDescr, Html.FROM_HTML_MODE_LEGACY));
           } else {
               tvRegularDescrption.setText(Html.fromHtml(regularTicketDescr));
           }
//           Log.i("AMOUNTVAL", "Cost : " + regularAdult.getCost_per() + " KKC : " + regularAdult.getKkc_tax() + " Service tax : " + regularAdult.getService_tax() + " swach tax : " + regularAdult.getSwachh_tax());

           regAdultAmount = Float.parseFloat(regularAdult.getService_tax()) + Float.parseFloat(regularAdult.getCost_per()) + Float.parseFloat(regularAdult.getSwachh_tax()) + Float.parseFloat(regularAdult.getKkc_tax());
           tvAmtRegularAdult.setText(String.format("%.2f", regAdultAmount));
           tvAdult.setText(regularAdult.getType());
       } else {
           llRegularTickets.setVisibility(View.GONE);
       }

       if (expressAdult != null) {
           tvExpressTitle.setText(expressTitle);
//           if (expressImageUrl != null && !expressImageUrl.isEmpty()) {
//               aq.id(ivExpressTicket).image(expressImageUrl, true, true);
//           } else {
//               aq.id(ivExpressTicket).background(R.drawable.image_imagica_theme);
//           }
           if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
               tvExpressDescrption.setText(Html.fromHtml(expressTicketDescr, Html.FROM_HTML_MODE_LEGACY));
           } else {
               tvExpressDescrption.setText(Html.fromHtml(expressTicketDescr));
           }
           expAdultAmount = Float.parseFloat(expressAdult.getService_tax()) + Float.parseFloat(expressAdult.getCost_per()) + Float.parseFloat(expressAdult.getSwachh_tax()) + Float.parseFloat(expressAdult.getKkc_tax());
           tvAmtExpressAdult.setText(String.format("%.2f", expAdultAmount));
           tvEAdult.setText(expressAdult.getType());
       } else {
           llExpressTickets.setVisibility(View.GONE);
       }
   }

    private void showQty(final TextView qtyView, final float amount, final TextView amtView) {
        final CharSequence[] qty = {"1", "2", "3", "4"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ImagicaWaterParkList.this);
        builder.setTitle("Select Quantity");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setItems(qty, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                qtyView.setText(qty[i]);
             float totamt = Float.parseFloat(String.format("%.2f", amount)) * Float.parseFloat(String.valueOf(qty[i]));
                amtView.setText(String.valueOf(totamt));
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
    public void getCardCategory() {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("adult", "1");
            jsonRequest.put("child", "1");
            jsonRequest.put("sc", "1");
            jsonRequest.put("college", "1");
            jsonRequest.put("total_day", "1");
            jsonRequest.put("date_visit", aDate);
            jsonRequest.put("date_departure", aDate);
            jsonRequest.put("destination", "Water Park");
            jsonRequest.put("session_name", session_name);
            jsonRequest.put("sessid", sessid);
            jsonRequest.put("token", token);
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("THEMEPARKTICKETLIST", ApiUrl.URL_IMAGICA_THEME_PARK_LIST);
            Log.i("THEMETICKETREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_IMAGICA_THEME_PARK_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("THEMEPARKRESPONSE", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            JSONObject detailsObj = jsonObj.getJSONObject("details");
                            String messageStr = detailsObj.getString("message");
                            JSONObject messageObj  = new JSONObject(messageStr);
                            JSONObject ticket = messageObj.getJSONObject("ticket");

                            JSONObject regularTicket = ticket.getJSONObject("Regular Ticket");
                            regularTicketDescr = regularTicket.getString("ticket_des");
                            regularTitle = regularTicket.getString("title");
                            regularImageUrl = regularTicket.getString("image_url");

                            if(regularTicket.has("Adult")) {
                                JSONObject adult = regularTicket.getJSONObject("Adult");
                                String type = adult.getString("type");
                                String cost = adult.getString("cost_per");
                                String productId = adult.getString("product_id");
                                JSONObject taxDetails = adult.getJSONObject("tax_details");
                                JSONObject tagServiceTax = taxDetails.getJSONObject("service_tax");
                                String serviceTax = tagServiceTax.getString("price_per_quantity");
                                JSONObject tagSwachhTax = taxDetails.getJSONObject("swachh_tax");
                                String swachhTax = tagSwachhTax.getString("price_per_quantity");
                                JSONObject tagKkcTax = taxDetails.getJSONObject("kkc_tax");
                                String kkcTax = tagKkcTax.getString("price_per_quantity");
                                regularAdult = new ThemeParkTicketModel(type, cost, productId, serviceTax, swachhTax, kkcTax);
                            }

                            JSONObject expressTicket = ticket.getJSONObject("Aquamagica Express");
                            expressTicketDescr = expressTicket.getString("ticket_des");
                            expressTitle = expressTicket.getString("title");
                            expressImageUrl = expressTicket.getString("image_url");

                            if(expressTicket.has("Adult")) {
                                JSONObject eAdult = regularTicket.getJSONObject("Adult");
                                String type1 = eAdult.getString("type");
                                String cost1 = eAdult.getString("cost_per");
                                String productId1 = eAdult.getString("product_id");
                                JSONObject taxDetails1 = eAdult.getJSONObject("tax_details");
                                JSONObject tagServiceTax1 = taxDetails1.getJSONObject("service_tax");
                                String serviceTax1 = tagServiceTax1.getString("price_per_quantity");
                                JSONObject tagSwachhTax1 = taxDetails1.getJSONObject("swachh_tax");
                                String swachhTax1 = tagSwachhTax1.getString("price_per_quantity");
                                JSONObject tagKkcTax1 = taxDetails1.getJSONObject("kkc_tax");
                                String kkcTax1 = tagKkcTax1.getString("price_per_quantity");
                                expressAdult = new ThemeParkTicketModel(type1, cost1, productId1, serviceTax1, swachhTax1, kkcTax1);
                            }

                            loadDlg.dismiss();
                            setViews();


                        }else{
                            loadDlg.dismiss();
                            String message = jsonObj.getString("message");
                            CustomToast.showMessage(ImagicaWaterParkList.this,message);
                            finish();

                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(ImagicaWaterParkList.this, NetworkErrorHandler.getMessage(error, ImagicaWaterParkList.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void createOrder(String pid, final String qty, final String amount) {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("product_id", pid);
            jsonRequest.put("quantity", qty);
            jsonRequest.put("total_day", "1");
            jsonRequest.put("field_visit_date", aDate);
            jsonRequest.put("field_departure_date", aDate);
            jsonRequest.put("field_evt_res_business_unit", "Water Park");
            jsonRequest.put("amount", amount);
            jsonRequest.put("session_name", session_name);
            jsonRequest.put("sessid", sessid);
            jsonRequest.put("token", token);
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("ORDERCREATEURL", ApiUrl.URL_IMAGICA_CREATE_ORDER);
            Log.i("ORDERCREATEREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_IMAGICA_CREATE_ORDER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("ORDERCREATERESP", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            JSONObject detailsObj = jsonObj.getJSONObject("details");
                            String messageStr = detailsObj.getString("message");
                            JSONObject messageObj  = new JSONObject(messageStr);
                            String uid = messageObj.getString("uid");
                            String order_id = messageObj.getString("order_id");
                            Intent imagicaOrderIntent = new Intent(getApplicationContext(), ImagicaOrderActivity.class);
                            imagicaOrderIntent.putExtra("AMOUNT", amount);
                            imagicaOrderIntent.putExtra("QTY", qty);
                            imagicaOrderIntent.putExtra("UID", uid);
                            imagicaOrderIntent.putExtra("ORDER_ID", order_id);
                            imagicaOrderIntent.putExtra("session_name", session_name);
                            imagicaOrderIntent.putExtra("sessid", sessid);
                            imagicaOrderIntent.putExtra("token", token);
                            startActivity(imagicaOrderIntent);
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(ImagicaWaterParkList.this, NetworkErrorHandler.getMessage(error, ImagicaWaterParkList.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvQtyRegularAdult:
                showQty(tvQtyRegularAdult, regAdultAmount, tvAmtRegularAdult);
                break;
            case R.id.tvQtyExpressAdult:
                showQty(tvQtyExpressAdult, expAdultAmount, tvAmtExpressAdult);
                break;
            case R.id.btnRegularAdultBook:
                createOrder(regularAdult.getProduct_id(), tvQtyRegularAdult.getText().toString(), tvAmtRegularAdult.getText().toString());
                break;
            case R.id.btnExpressAdultBook:
                createOrder(expressAdult.getProduct_id(), tvQtyExpressAdult.getText().toString(), tvAmtExpressAdult.getText().toString());
                break;
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

}
