package in.msewa.ecarib.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;
//import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.ShoppingModel;
import in.msewa.ecarib.R;
import in.msewa.util.TLSSocketFactory;

/**
 * Created by Ksf on 4/6/2016.
 */
public class ProductDetailActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private ShoppingModel shopingModel;

    private Button addToCart;
    private TextView shoppingTitle, shoppingprice;
    private ImageView shoppingImage;
    private YouTubePlayerView youTubeView;
    private YouTubePlayer ytp;

    private String vCode;

    private RequestQueue rq;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        shopingModel = getIntent().getParcelableExtra("Shopping Data");

        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(ProductDetailActivity.this);
            rq = Volley.newRequestQueue(getApplicationContext(), new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        declare();

        getVideo();


    }

    private void declare() {
        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        addToCart = (Button) findViewById(R.id.btn_shopping_details_addTocart);
        shoppingTitle = (TextView) findViewById(R.id.tvshopping_details_title);
        shoppingprice = (TextView) findViewById(R.id.tvshopping_product_price);
        shoppingImage = (ImageView) findViewById(R.id.ivshopping_details_image);

        shoppingTitle.setText(shopingModel.getpName());
        shoppingprice.setText("INR " + shopingModel.getpPrice());
        if (shopingModel.getpImage().length() != 0) {
            Picasso.with(getApplicationContext()).load(shopingModel.getpImage())
                    .placeholder(R.drawable.telebuy_logo).into(shoppingImage);
        } else {
            shoppingImage.setVisibility(View.GONE);
        }


    }

    private void getVideo() {
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET, ApiUrl.URL_GET_SHOPPING_VIDEO + shopingModel.getPid(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String res) {
                        try {
                            Log.i("Video Response", res.toString());
                            JSONObject response=new JSONObject(res);
                            JSONArray videoJArray = response.getJSONArray("youtubeurl");
                            for (int i = 0; i < videoJArray.length(); i++) {
                                JSONObject c = videoJArray.getJSONObject(i);
                                if (c.has("url")) {
                                    String url = c.getString("url");
                                    if (url != null && !url.isEmpty() && !url.equals("NA")) {
                                        String[] videoCo = url.substring(url.lastIndexOf("/")).split("/");
                                        vCode = videoCo[1];
                                        Log.i("Video Code", vCode);

                                        try {
                                            Log.i("Video Code", "Sucess");
                                            youTubeView.initialize(AppMetadata.YOUTUBE_DEVELOPER_KEY, ProductDetailActivity.this);
                                        } catch (Exception e) {
                                            Log.i("Video", "Failed");
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

        };
        rq.add(jsonObjReq);

    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo(vCode);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Initialization Fail", Toast.LENGTH_LONG).show();
    }
}
