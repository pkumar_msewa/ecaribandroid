package in.msewa.ecarib.activity.shopping;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.util.EncodingUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.AvenuesParams;
import in.msewa.model.PQCart;
import in.msewa.model.UserModel;
import in.msewa.util.RSAUtility;
import in.msewa.util.ServiceUtility;
import in.msewa.util.TLSSocketFactory;
import in.msewa.ecarib.R;

public class TelebuyCCAvenueWebViewActivity extends AppCompatActivity {

    Intent mainIntent;
    String html, encVal;
    private UserModel session = UserModel.getInstance();
    private PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();
    private LoadingDialog dialog;
    private RequestQueue rq;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_webview);
//        mainIntent = getIntent();
//        dialog = new LoadingDialog(this);
//        dialog.show();
        Toast.makeText(getApplicationContext(),"WebView", Toast.LENGTH_SHORT).show();
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory(this);
            rq = Volley.newRequestQueue(getApplicationContext(), new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        testRender();
//        renderView();
    }

    public void testRender() {
        Log.i("IN Test Render","Render Inside");

        StringRequest postReq = new StringRequest(Request.Method.POST, PQTelebuyPaymentHolder.RSA_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("RegisterAPI",response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),	"Error connecting to server", Toast.LENGTH_LONG).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                Log.i("Acess CODe",PQTelebuyPaymentHolder.ACCESS_CODE);
                Log.i("Amount",payTelebuy.getAmount());
                Log.i("ORDER_ID",payTelebuy.getOrderId());
                params.put(AvenuesParams.ACCESS_CODE, PQTelebuyPaymentHolder.ACCESS_CODE);
                params.put(AvenuesParams.AMOUNT, payTelebuy.getAmount());
                params.put(AvenuesParams.ORDER_ID, payTelebuy.getOrderId());
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        rq.add(postReq);
    }


    public void renderView() {
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, PQTelebuyPaymentHolder.RSA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Response from Server
                        Log.i("RENDER RESPONSE", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj != null) {
                                if (response != null && response.length() <= 4) {
                                    if (!ServiceUtility.chkNull(response).equals("") && !ServiceUtility.chkNull(response).toString().contains("ERROR")) {
                                        StringBuilder vEncVal = new StringBuilder("");
                                        vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CARD_NUMBER, ""));
                                        vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.EXPIRY_YEAR, ""));
                                        vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.EXPIRY_MONTH, ""));
                                        vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CVV, ""));
                                        vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, payTelebuy.getAmount()));
                                        vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, PQTelebuyPaymentHolder.CURRENCY));
                                        encVal = RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), response);
                                    }
                                }
                                postTask();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error: ", error.toString());
                    }

                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                Log.i("Acess CODe",PQTelebuyPaymentHolder.ACCESS_CODE);
                Log.i("Amount",payTelebuy.getAmount());
                Log.i("ORDER_ID",payTelebuy.getOrderId());


                params.put(AvenuesParams.ACCESS_CODE, PQTelebuyPaymentHolder.ACCESS_CODE);
                params.put(AvenuesParams.AMOUNT, payTelebuy.getAmount());
                params.put(AvenuesParams.ORDER_ID, payTelebuy.getOrderId());
                return params;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    }

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    public void postTask() {
        dialog.dismiss();
        class MyJavaScriptInterface {
            @JavascriptInterface
            public void processHTML(String html) {
                // process the html as needed by the app
                // TODO Process the HTML
                String status;
                if (html.contains("Failure")) {
                    status = "Transaction Declined!";
                } else if (html.contains("Success")) {
                    status = "Transaction Successful!";
                } else if (html.contains("Aborted")) {
                    status = "Transaction Cancelled!";
                } else {
                    status = "Status Not Known!";
                }

                final Intent intent = new Intent(getApplicationContext(), PQStatusActivity.class);
                intent.putExtra("transStatus", status);
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            PQCart cart = PQCart.getInstance();
                            cart.setCartUpdated(true);
                            startActivity(intent);
                            finish();
                        }
                    }
                };
                timer.start();
            }
        }

        final WebView webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webview, url);
                final WebView v = view;
                dialog.dismiss();
                if (url.contains("/ccavResponseHandler.aspx")) {
                    webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                }
                Handler lHandler = new Handler();
                lHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.scrollTo(0, v.getContentHeight());
                    }
                }, 200);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                dialog.show();
            }
        });

			/* An instance of this class will be registered as a JavaScript interface */
        StringBuilder params = new StringBuilder();
        params.append(ServiceUtility.addToPostParams(AvenuesParams.ACCESS_CODE, PQTelebuyPaymentHolder.ACCESS_CODE));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_ID, PQTelebuyPaymentHolder.MERCHANT_ID));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.ORDER_ID, payTelebuy.getOrderId()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.REDIRECT_URL, PQTelebuyPaymentHolder.REDIRECT_URL));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.CANCEL_URL, PQTelebuyPaymentHolder.CANCEL_URL));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.LANGUAGE, "EN"));


        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_NAME, session.getUserFirstName() + " " + session.getUserLastName()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ADDRESS, payTelebuy.getAddress()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_CITY, payTelebuy.getCity()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_STATE, payTelebuy.getState()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ZIP, payTelebuy.getZip()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_COUNTRY, "India"));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_TEL, session.getUserMobileNo()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_EMAIL, (session.getUserEmail() == null) ? "care@msewa.in" : session.getUserEmail()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_NAME, session.getUserFirstName() + " " + session.getUserLastName()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ADDRESS, payTelebuy.getAddress()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_CITY, payTelebuy.getCity()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_STATE, payTelebuy.getState()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ZIP, payTelebuy.getZip()));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_COUNTRY, "India"));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_TEL, session.getUserMobileNo()));

        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM1, "additional Info."));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM2, "additional Info."));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM3, "additional Info."));
        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM4, "additional Info."));

        try {
            if (encVal != null) {
                params.append(ServiceUtility.addToPostParams(AvenuesParams.ENC_VAL, URLEncoder.encode(encVal, "UTF-8")));
            }
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        String vPostParams = params.substring(0, params.length() - 1);
        try {
            webview.postUrl(AvenuesParams.TRANS_URL, EncodingUtils.getBytes(vPostParams, "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
