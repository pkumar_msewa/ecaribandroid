package in.msewa.ecarib.activity.businneractivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import in.msewa.adapter.BusCityListAdapter;
import in.msewa.custom.LoadingDialog;
import in.msewa.model.BusCityModel;
import in.msewa.ecarib.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class BusCitySearchActivity extends AppCompatActivity {
    private ListView lvSearchBusCity;
    private MaterialEditText etSearchBusCity;

    private FetchCityTask fetchCityTask;

    private LoadingDialog loadingDialog;
    private ArrayList<BusCityModel> busCityModelList;

    private BusCityListAdapter busCityListAdapter;
    private String searchType = "";

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private TextView tvSearchResult;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_city_search);
        searchType = getIntent().getStringExtra("searchType");
        //press back button in toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        loadingDialog = new LoadingDialog(BusCitySearchActivity.this);

        etSearchBusCity = (MaterialEditText) findViewById(R.id.etSearchBusCity);
        lvSearchBusCity = (ListView) findViewById(R.id.lvSearchBusCity);
        tvSearchResult = (TextView) findViewById(R.id.tvSearchResult);

        busCityModelList = new ArrayList<>();
        String mainJson = loadJSONFromAsset();
        try {
            JSONArray response = new JSONArray(mainJson);
            for (int i = 0; i < response.length(); i++) {
                JSONObject c = response.getJSONObject(i);
                long cityId = c.getLong("Id");
                String cityName = c.getString("Name");
                BusCityModel cModel = new BusCityModel(cityId, cityName);
                busCityModelList.add(cModel);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        tvSearchResult.setText("All Cities");
        busCityListAdapter = new BusCityListAdapter(getApplicationContext(),busCityModelList );
        lvSearchBusCity.setAdapter(busCityListAdapter);
        busCityListAdapter.notifyDataSetChanged();
//        busCityModelList = AppMetadata.getBusCities();
//        busCityListAdapter = new BusCityListAdapter(getApplicationContext(),busCityModelList );
//        lvSearchBusCity.setAdapter(busCityListAdapter);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("booking-done"));

        lvSearchBusCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent("search-complete");
                intent.putExtra("selectedCityName", busCityModelList.get(i).getCityname());
                intent.putExtra("selectedCityCode", busCityModelList.get(i).getCityId());
                intent.putExtra("searchType", searchType);
                LocalBroadcastManager.getInstance(BusCitySearchActivity.this).sendBroadcast(intent);
                loadingDialog.dismiss();
                finish();
            }
        });

        etSearchBusCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if(cs.length()>2){
                    searchBus(String.valueOf(cs));
                }else{
//                    tvSearchResult.setText("Top Cities");
//                    busCityModelList = AppMetadata.getBusCities();
//                    busCityListAdapter.notifyDataSetChanged();

                    String mainJson = loadJSONFromAsset();
                    try {
                        JSONArray response = new JSONArray(mainJson);
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject c = response.getJSONObject(i);
                            long cityId = c.getLong("Id");
                            String cityName = c.getString("Name");
                            BusCityModel cModel = new BusCityModel(cityId, cityName);
                            busCityModelList.add(cModel);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    tvSearchResult.setText("All Cities");
                    busCityListAdapter = new BusCityListAdapter(getApplicationContext(),busCityModelList );
                    lvSearchBusCity.setAdapter(busCityListAdapter);
                    busCityListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("ticket");
            if (action.equals("1")) {
                finish();
            }

        }
    };

    private void searchBus(String cityValue) {
        fetchCityTask = new FetchCityTask();
        fetchCityTask.execute(cityValue);
    }


    private class FetchCityTask extends AsyncTask<String, Void, ArrayList<BusCityModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            busCityModelList.clear();
            loadingDialog.show();
        }

        @Override
        protected ArrayList<BusCityModel> doInBackground(String... selectedCityString) {
            String mainJson = loadJSONFromAsset();
            try {
                JSONArray response = new JSONArray(mainJson);
                for (int i = 0; i < response.length(); i++) {
                    JSONObject c = response.getJSONObject(i);
                    long cityId = c.getLong("Id");
                    String cityName = c.getString("Name");
                    Log.i("Selected",selectedCityString[0]);

                    if ((cityName.toUpperCase()).contains(selectedCityString[0].toUpperCase())) {
                        BusCityModel cModel = new BusCityModel(cityId, cityName);
                        busCityModelList.add(cModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return busCityModelList;
        }

        @Override
        protected void onPostExecute(ArrayList<BusCityModel> result) {
            if (result != null && result.size() != 0) {
                tvSearchResult.setText("Obtained Result");
                busCityListAdapter = new BusCityListAdapter(getApplicationContext(), result);
                lvSearchBusCity.setAdapter(busCityListAdapter);
                loadingDialog.dismiss();
                lvSearchBusCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent("search-complete");
                        intent.putExtra("selectedCityName", busCityModelList.get(i).getCityname());
                        intent.putExtra("selectedCityCode", busCityModelList.get(i).getCityId());
                        intent.putExtra("searchType", searchType);
                        LocalBroadcastManager.getInstance(BusCitySearchActivity.this).sendBroadcast(intent);
                        loadingDialog.dismiss();
                        finish();
                    }
                });
            } else {
                loadingDialog.dismiss();
                tvSearchResult.setText("Obtained Result");
                busCityModelList.clear();
                busCityListAdapter.notifyDataSetChanged();
            }

        }
    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("buscity.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


}
