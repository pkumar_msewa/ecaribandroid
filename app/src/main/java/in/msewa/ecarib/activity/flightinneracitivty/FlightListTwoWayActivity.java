package in.msewa.ecarib.activity.flightinneracitivty;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.msewa.adapter.BusListAdapter;
import in.msewa.adapter.FlightRoundTripListAdapter;
import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomFiltterDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BankListModel;
import in.msewa.model.BusCityModel;
import in.msewa.model.FlightListModel;
import in.msewa.model.FlightModel;
import in.msewa.model.TravelCityModel;
import in.msewa.model.UserModel;
import in.msewa.util.CitySelectedListener;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


public class FlightListTwoWayActivity extends AppCompatActivity {

  private static String selectValue;
  //filtter
  AlertDialog alertDialog;
  private RecyclerView rvFlightUp, rvFlightDown;
  private LoadingDialog loadingDialog;
  private String destinationCode, sourceCode;
  private String dateOfDep, flightClass, dateOfReturn;
  private int flightType;
  //Volley
  private String tag_json_obj = "json_travel";
  private LinearLayout llNoBus;
  private ArrayList<FlightListModel> flightListItem;
  private ArrayList<FlightListModel> flightListOneyWay;
  private ArrayList<FlightListModel> flightListTwoWay;
  private BusListAdapter busTyeAdapter;
  private LinearLayout llSort, LLSortFilter, llArrival, llDeparture, llPrice, llFilter;
  private ImageView ivArrSort, ivDepSort, ivPriceSort;
  private boolean arrcheck = true;
  private boolean depcheck = true;
  private boolean pricecheck = true;

  //header
  private TextView tvHeadOnWardDate, tvHeadFlightReturnDate;
  private JSONObject jsonRequest;

  private TextView tvFlightRoundFinalPrice;
  private Button btnFlightRoundBook;

  private double upPrice = 0, downPrice = 0;
  private FlightListModel flightModelUp, flightModelDown;

  private AppCompatCheckBox allAir_spice_jet_Lines, allAir_vistara_Lines, allAir_air_india_Lines, allAir_indigo_Lines, allAir_jet_air_Lines_Lines, all_go_AirLines;
  private String jSonUp, jSonDown;
  private int postUp = 0, postDown = 0;

  private int noOfAdult, noOfChild, noOfInfant;

  private UserModel session = UserModel.getInstance();


  private String jsonToSendUp = "";
  private String jsonFareToSendUp = "";
  private String jsonToRoundSendUp = "";

  private String jsonToSendDown = "";
  private String jsonFareToSendDown = "";
  private int bondsLength;
  private String nonStopValue = "null";
  private String refund = "null";
  private String before_eleven_am_, before_eleven_and_five_pm_, retrun_before_eleven_am_, above_nine_pm_, before_five_pm_nine_;
  private String retrun_before_eleven_and_five_pm_, retrun_before_five_pm_nine_, retrun_above_nine_pm_;
  private JSONObject contingFlight;
  private boolean allAirLinesValue;
  private FlightRoundTripListAdapter FlightRoundTripListAdaptertwo, FlightRoundTripListAdapterone;
  private LinearLayout filter;

  private String airAsia = "";
  private String airIndia = "";
  private String spicejet = "";
  private String jetairways = "";
  private String indigo = "";
  private String goAir = "";
  private RadioButton rbNonRefundableround, rbRefundableonew;
  private boolean roundWayValue;
  private LinearLayout listfilterstwo;
  private LinearLayout listfilters;
  private int citylist;
  private LinearLayout llNoFiltter;
  private Button Departure, Duration;
  private int currentPostion = 0, count = 0;
  private boolean intentional = false;
  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      int tripType = intent.getIntExtra("tripType", 0);
      intentional = intent.getBooleanExtra("intentional", false);
      if (intentional) {
        upPrice = intent.getDoubleExtra("price-update", 0);
        flightModelUp = intent.getParcelableExtra("flightArray");
        postUp = intent.getIntExtra("position", 0);
      } else if (tripType == 1) {
        upPrice = intent.getDoubleExtra("price-update", 0);
        flightModelUp = intent.getParcelableExtra("flightArray");
        postUp = intent.getIntExtra("position", 0);
      } else {
        downPrice = intent.getDoubleExtra("price-update", 0);
        flightModelDown = intent.getParcelableExtra("flightArray");
        postDown = intent.getIntExtra("position", 0);
      }
      updatePrice();

    }
  };

  private static Date addMinutesToDate(int minutes, Date beforeTime) {
    final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

    long curTimeInMs = beforeTime.getTime();
    Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
    return afterAddingMins;
  }

//    public static void longLog(String str) {
//        if (str.length() > 4000) {
//            Log.d("travel", str.substring(0, 4000));
//            longLog(str.substring(4000));
//        } else
//            Log.d("travel", str);
//    }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    loadingDialog = new LoadingDialog(FlightListTwoWayActivity.this);
    setContentView(R.layout.activity_roundtrip_flight_list);
    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("flight-price"));
    llNoBus = (LinearLayout) findViewById(R.id.llNoBus);
    TextView tvNoBus = (TextView) findViewById(R.id.tvNoBus);
    Departure = (Button) findViewById(R.id.Departure);
    Duration = (Button) findViewById(R.id.Duration);

    ImageView ivNoBus = (ImageView) findViewById(R.id.ivNoBus);
    final Button duration = (Button) findViewById(R.id.Duration);
    final Button departure = (Button) findViewById(R.id.Departure);
    llNoFiltter = (LinearLayout) findViewById(R.id.llNoFiltter);
    final Button price = (Button) findViewById(R.id.Price);
    final Button retrunDeparture = (Button) findViewById(R.id.retrunDeparture);
    final Button retrunDuration = (Button) findViewById(R.id.retrunDuration);
    final Button retrunPrice = (Button) findViewById(R.id.retrunPrice);
    tvHeadOnWardDate = (TextView) findViewById(R.id.tvHeadOnWardDate);
    tvHeadFlightReturnDate = (TextView) findViewById(R.id.tvHeadFlightReturnDate);
    btnFlightRoundBook = (Button) findViewById(R.id.btnFlightRoundBook);
    tvFlightRoundFinalPrice = (TextView) findViewById(R.id.tvFlightRoundFinalPrice);
    filter = (LinearLayout) findViewById(R.id.filter);
    //Setting for flight in bus xml file
    flightListOneyWay = new ArrayList<>();
    flightListTwoWay = new ArrayList<>();
    listfilterstwo = (LinearLayout) findViewById(R.id.listfilterstwo);
    listfilters = (LinearLayout) findViewById(R.id.listfilters);
    tvNoBus.setText(getResources().getString(R.string.no_flight));
    llNoBus.setVisibility(View.GONE);
    ivNoBus.setImageResource(R.drawable.ic_no_flight);
    citylist = getIntent().getIntExtra("citylist", 0);
    noOfAdult = getIntent().getIntExtra("Adult", 0);
    noOfChild = getIntent().getIntExtra("Child", 0);
    noOfInfant = getIntent().getIntExtra("Infant", 0);
    flightClass = getIntent().getStringExtra("Class");
    destinationCode = getIntent().getStringExtra("destination");
    sourceCode = getIntent().getStringExtra("source");
    dateOfDep = getIntent().getStringExtra("date");
    flightType = getIntent().getIntExtra("flightType", 0);
    dateOfReturn = getIntent().getStringExtra("dateOfReturn");
    rvFlightDown = (RecyclerView) findViewById(R.id.rvFlightDown);
    rvFlightUp = (RecyclerView) findViewById(R.id.rvFlightUp);

    int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing_round);
    rvFlightUp.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    rvFlightDown.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

    GridLayoutManager manager = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
    GridLayoutManager manager2 = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
    rvFlightUp.setLayoutManager(manager);
    rvFlightDown.setLayoutManager(manager2);


    //press back button in toolbar
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
    setSupportActionBar(toolbar);
    ivBackBtn.setVisibility(View.VISIBLE);
    ivBackBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

    Button btnFlightRoundBook = (Button) findViewById(R.id.btnFlightRoundBook);
    btnFlightRoundBook.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (flightModelUp != null && flightModelDown != null) {

          @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");

          Date d1 = null;
          Date d2 = null;
          if (dateOfReturn.equalsIgnoreCase(dateOfDep)) {
            compareDate(flightModelUp.getFlightListArray().get(0).getArrivalTime(), flightModelDown.getFlightListArray().get(0).getDepTime());
          } else {
            FlightPriceTwoWayActivity.jsonUpString = jSonUp;
            FlightPriceTwoWayActivity.jsonDownString = jSonDown;
            Intent flightPriceIntent = new Intent(FlightListTwoWayActivity.this, FlightPriceTwoWayActivity.class);

            flightPriceIntent.putExtra("flightUp", flightModelUp);
            flightPriceIntent.putExtra("flightDown", flightModelDown);
            flightPriceIntent.putExtra("postUp", postUp);
            flightPriceIntent.putExtra("postDown", postDown);
            flightPriceIntent.putExtra("dateOfDep", dateOfDep);
            flightPriceIntent.putExtra("dateOfReturn", dateOfReturn);
            flightPriceIntent.putExtra("citylist", citylist);
            flightPriceIntent.putExtra("sourceCode", sourceCode);
            flightPriceIntent.putExtra("destinationCode", destinationCode);

            flightPriceIntent.putExtra("adultNo", noOfAdult);
            flightPriceIntent.putExtra("childNo", noOfChild);
            flightPriceIntent.putExtra("infantNo", noOfInfant);

            flightPriceIntent.putExtra("infantNo", noOfInfant);
            flightPriceIntent.putExtra("flightClass", flightClass);


            flightPriceIntent.putExtra("jsonFareToSendUp", jsonFareToSendUp);

            flightPriceIntent.putExtra("jsonFareToSendDown", jsonFareToSendDown);

            flightPriceIntent.putExtra("jsonToSendUp", jsonToSendUp);
            flightPriceIntent.putExtra("jsonToSendDown", jsonToSendDown);


            startActivity(flightPriceIntent);
          }
        } else if (intentional) {
          if (flightModelUp != null) {
            Intent flightDetailIntent = new Intent(FlightListTwoWayActivity.this, FlightPriceOneWayActivity.class);
            flightDetailIntent.putExtra("FlightArray", flightModelUp);
            flightDetailIntent.putExtra("sourceCode", sourceCode);
            flightDetailIntent.putExtra("intentional", intentional);
            flightDetailIntent.putExtra("destinationCode", destinationCode);
            flightDetailIntent.putExtra("dateOfJourney", dateOfDep);
            flightDetailIntent.putExtra("dateOfReturn", dateOfReturn);
            flightDetailIntent.putExtra("adultNo", noOfAdult);
            flightDetailIntent.putExtra("childNo", noOfChild);
            flightDetailIntent.putExtra("citylist", citylist);
            flightDetailIntent.putExtra("infantNo", noOfInfant);
            flightDetailIntent.putExtra("flightClass", flightClass);
            flightDetailIntent.putExtra("jsonToSend", jsonToSendUp);
            flightDetailIntent.putExtra("jsonFareToSend", jsonFareToSendUp);
            startActivity(flightDetailIntent);
          } else {
            Toast.makeText(getApplicationContext(), "Flight not found", Toast.LENGTH_SHORT).show();
          }
        } else {
          Toast.makeText(getApplicationContext(), "Flight not found", Toast.LENGTH_SHORT).show();
        }
      }
    });

    getFlightList();
    departure.setOnClickListener(new View.OnClickListener()

    {
      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
      @Override
      public void onClick(View v) {
        price.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);

        if (v.isSelected()) {
          getDepartureFilterHightToLow(FlightRoundTripListAdapterone, rvFlightUp);
          v.setSelected(false);
//                    departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
        } else {
          getDepartureFilter(FlightRoundTripListAdapterone, rvFlightUp);
//                    departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
          v.setSelected(true);
        }
      }
    });
    duration.setOnClickListener(new View.OnClickListener()

    {
      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
      @Override
      public void onClick(View v) {
        departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        price.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        if (v.isSelected()) {
          getDUrationHighTOLow(FlightRoundTripListAdapterone, rvFlightUp);
//                    duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
          v.setSelected(false);
        } else {
          getDUrationLowTOHigh(FlightRoundTripListAdapterone, rvFlightUp);
//                    duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
          v.setSelected(true);
        }
      }
    });
    price.setOnClickListener(new View.OnClickListener()

    {
      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
      @Override
      public void onClick(View v) {
        departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        if (v.isSelected()) {
          getpriceHighToLow(FlightRoundTripListAdapterone, rvFlightUp);
//                    price.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
          v.setSelected(false);
        } else {
          getpriceLowToHight(FlightRoundTripListAdapterone, rvFlightUp);
//                    price.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
          v.setSelected(true);
        }
      }
    });
    retrunDeparture.setOnClickListener(new View.OnClickListener()

    {
      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
      @Override
      public void onClick(View v) {
        retrunPrice.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        retrunDuration.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);

        if (v.isSelected()) {
          getDepartureFilterHightToLow(FlightRoundTripListAdaptertwo, rvFlightDown);
          v.setSelected(false);
//                    returdeparture.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
        } else {
          getDepartureFilter(FlightRoundTripListAdaptertwo, rvFlightDown);
//                    departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
          v.setSelected(true);
        }
      }
    });
    retrunDuration.setOnClickListener(new View.OnClickListener()

    {
      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
      @Override
      public void onClick(View v) {
        retrunDeparture.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        retrunPrice.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        if (v.isSelected()) {
          getDUrationHighTOLow(FlightRoundTripListAdaptertwo, rvFlightDown);
//                    duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
          v.setSelected(false);
        } else {
          getDUrationLowTOHigh(FlightRoundTripListAdaptertwo, rvFlightDown);
//                    duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
          v.setSelected(true);
        }
      }
    });
    retrunPrice.setOnClickListener(new View.OnClickListener()

    {
      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
      @Override
      public void onClick(View v) {
        retrunDeparture.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        retrunDuration.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        if (v.isSelected()) {
          getpriceHighToLow(FlightRoundTripListAdaptertwo, rvFlightDown);
//                    price.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
          v.setSelected(false);
        } else {
          getpriceLowToHight(FlightRoundTripListAdaptertwo, rvFlightDown);
//                    price.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
          v.setSelected(true);
        }
      }
    });

  }

  private void getFlightList() {
    loadingDialog.show();

    jsonRequest = new JSONObject();
    try {

      jsonRequest.put("sessionId", session.getUserSessionId());
      jsonRequest.put("origin", sourceCode);
      jsonRequest.put("destination", destinationCode);
      jsonRequest.put("tripType", "RoundTrip");
      jsonRequest.put("cabin", flightClass);
      jsonRequest.put("adults", noOfAdult);
      jsonRequest.put("childs", noOfChild);
      jsonRequest.put("infants", noOfInfant);
      jsonRequest.put("traceId", "AYTM00011111111110002");
      jsonRequest.put("endDate", dateOfReturn);
      jsonRequest.put("beginDate", dateOfDep);

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }


    if (jsonRequest != null) {
      Log.i("jsonRequest", jsonRequest.toString());
      Log.i("URL", ApiUrl.URL_FLIGHT_LIST_DETAILS);
      JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_LIST_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
          try {
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {
              loadingDialog.dismiss();
              JSONObject jsonDetails = response.getJSONObject("details");
              JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");

              Log.i("Journey Array size", jsonJourney.length() + "");
              //OnWard Ticket
              JSONObject jsonFirstJrnyObj = jsonJourney.getJSONObject(0);
              JSONArray jsonSegmentFirstArray = jsonFirstJrnyObj.getJSONArray("segments");
              Log.i("segment", String.valueOf(jsonSegmentFirstArray.length()));
              if (jsonSegmentFirstArray.length() != 0) {
                flightListItem = new ArrayList<>();

                for (int k = 0; k < jsonSegmentFirstArray.length(); k++) {
                  String jsonSegmentFirstString = jsonSegmentFirstArray.getJSONObject(k).toString();
                  ArrayList<FlightModel> flightArray = new ArrayList<>();
                  ArrayList<FlightModel> flightRoundArray = new ArrayList<>();
                  JSONObject segmentObj = jsonSegmentFirstArray.getJSONObject(k);
                  JSONArray bondArray = segmentObj.getJSONArray("bonds");
                  JSONArray legArray1 = bondArray.getJSONObject(0).getJSONArray("legs");
                  bondsLength = bondArray.length();
                  contingFlight = new JSONObject();
                  if (bondArray.length() > 1) {
                    tvHeadFlightReturnDate.setVisibility(View.GONE);
                    filter.setVisibility(View.GONE);
                    llNoBus.setVisibility(View.GONE);
                    listfilters.setVisibility(View.VISIBLE);
                    intentional = true;
                    Duration.setText("Duration");
                    Departure.setText("Departure");
                    JSONArray bounds = new JSONArray();
                    for (int i = 0; i < bondArray.length(); i++) {
                      JSONObject bonObj = bondArray.getJSONObject(i);
                      JSONArray legArray = bonObj.getJSONArray("legs");

                      JSONObject bondary = bondArray.getJSONObject(i);
                      bondary.remove("specialServices");
                      bondary.put("baggageFare", bondary.getString("baggageFare"));
                      bondary.put("ssrFare", bondary.getString("ssrFare"));
                      JSONArray jsonArray = new JSONArray();
                      for (int l = 0; l < legArray.length(); l++) {

                        JSONObject d = legArray.getJSONObject(l);
                        d.remove("cabinClasses");
                        d.remove("ssrDetails");
                        d.remove("amount");
                        d.remove("isConnecting");
                        d.remove("numberOfStops");
                        d.remove("remarks");
                        jsonArray.put(d);
                      }

                      bondary.put("legs", jsonArray);
                      bounds.put(bondary);

                      String s = segmentObj.getJSONArray("fares").toString();
                      s = s.replace("\"fares\":", "\"bookFares\":");
                      JSONArray jsonArray1 = new JSONArray(s);
                      contingFlight.put("fares", jsonArray1);
                      segmentObj.remove("bondType");
                      segmentObj.remove("remark");
                      contingFlight.put("baggageFare", segmentObj.getString("baggageFare"));
                      contingFlight.put("cache", segmentObj.getString("cache"));
                      contingFlight.put("holdBooking", segmentObj.getString("holdBooking"));
                      contingFlight.put("international", segmentObj.getString("international"));
                      contingFlight.put("roundTrip", segmentObj.getString("roundTrip"));
                      contingFlight.put("special", segmentObj.getString("special"));
                      contingFlight.put("specialId", segmentObj.getString("specialId"));
                      contingFlight.put("engineID", segmentObj.getString("engineID"));
                      contingFlight.put("fareRule", segmentObj.getString("fareRule"));
                      contingFlight.put("itineraryKey", segmentObj.getString("itineraryKey"));
                      contingFlight.put("journeyIndex", segmentObj.getString("journeyIndex"));
                      contingFlight.put("nearByAirport", segmentObj.getString("nearByAirport"));
                      contingFlight.put("searchId", segmentObj.getString("searchId"));
                      contingFlight.put("origin", sourceCode);
                      contingFlight.put("destination", destinationCode);
                      contingFlight.put("tripType", "RoundTrip");
                      contingFlight.put("cabin", flightClass);
                      contingFlight.put("adults", noOfAdult);
                      contingFlight.put("childs", noOfChild);
                      contingFlight.put("infants", noOfInfant);
                      contingFlight.put("traceId", "AYTM00011111111110002");
                      contingFlight.put("beginDate", dateOfDep);
                      contingFlight.put("endDate", dateOfReturn);
//                                            longLog(contingFlight.toString());

                      for (int j = 0; j < legArray.length(); j++) {
                        JSONObject d = legArray.getJSONObject(j);
                        jsonToRoundSendUp = legArray.getJSONObject(j).toString();

                        String depAirportCode = d.getString("origin");
                        String depTime = d.getString("departureTime");
                        String arrivalAirportCode = d.getString("destination");
                        String arrivalTime = d.getString("arrivalTime");

                        String flightDuration = d.getString("duration");
                        String flightName = d.getString("airlineName");
                        String flightNo = d.getString("flightNumber");
                        String flightImage = d.getString("airlineName");
                        String flightCode = d.getString("aircraftCode");

                        String flightArrivalTimeZone = d.getString("arrivalDate");

                        String flightDepartureTimeZone = d.getString("departureDate");
                        String baggageUnit = d.getString("baggageUnit");
                        String baggageWeight=d.getString("baggageWeight");

                        String flightArrivalAirportName = d.getString("destination");
                        String flightDepartureAirportName = d.getString("origin");
                        String flightTpe;
                        flightTpe = d.getString("flightName");
                        String flightRule = "";
                        JSONArray fareArray = segmentObj.getJSONArray("fares");
                        JSONObject fareObj = fareArray.getJSONObject(0);
                        JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                        JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                        jsonFareToSendUp = paxFaresArray.getJSONObject(0).toString();

                        if (i == 0) {
                          String duration= bondArray.getJSONObject(i).getString("journeyTime");
                          FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                            flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName, 1, flightTpe, jsonToRoundSendUp, jsonFareToSendUp,baggageUnit,baggageWeight,duration);
                          flightArray.add(flightModel);
                        } else if (i == 1) {
                          String duration= bondArray.getJSONObject(i).getString("journeyTime");
                          FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                            flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName, 1, flightTpe, jsonToRoundSendUp, jsonFareToSendUp,baggageUnit,baggageWeight,duration);
                          flightRoundArray.add(flightModel);
                        }
                      }
                    }
                    contingFlight.put("bonds", bounds);
                    JSONArray fareArray = segmentObj.getJSONArray("fares");
                    JSONObject fareObj = fareArray.getJSONObject(0);
                    JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                    JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);
                    jsonFareToSendUp = paxFaresArray.getJSONObject(0).toString();

                    double flightActualBaseFare = paxFaresObj.getDouble("basicFare");
                    double flightTaxFare = 0.0;
                    double totalFare = 0.0;
                    if (paxFaresObj.has("totalTax")) {
                      flightTaxFare = paxFaresObj.getDouble("totalTax");
                    }
                    if (fareObj.has("totalFareWithOutMarkUp")) {
                      totalFare = fareObj.getDouble("totalFareWithOutMarkUp");
                    }
                    boolean refund = paxFaresObj.getBoolean("refundable");
//                                        longLog(flightRoundArray.get(0).toString());

                    String duration = compareDate1(legArray1.getJSONObject(0).getString("departureTime"), legArray1.getJSONObject(legArray1.length() - 1).getString("arrivalTime"));
                    FlightListModel flightModel = new FlightListModel(flightActualBaseFare, flightTaxFare, 0.0, 0.0, 0.0, totalFare, totalFare, "", flightArray, jsonSegmentFirstString, contingFlight.toString(), flightRoundArray, String.valueOf(refund), legArray1.getJSONObject(0).getString("departureTime"), duration);
                    flightListItem.add(flightModel);
                    flightListOneyWay.add(flightModel);
                  } else {

                    JSONObject bonObj = bondArray.getJSONObject(0);
                    JSONArray legArray = bonObj.getJSONArray("legs");
                    JSONObject segmentConting = jsonSegmentFirstArray.getJSONObject(k);
                    JSONObject contingFlight = new JSONObject();
                    listfilters.setVisibility(View.VISIBLE);
                    if (legArray.length() >= 1) {
                      JSONArray bounds = new JSONArray();
                      JSONObject bondary = bondArray.getJSONObject(0);
                      bondary.remove("specialServices");
                      bondary.put("addOnDetail", "");
                      JSONArray jsonArray = new JSONArray();
                      for (int j = 0; j < legArray.length(); j++) {
                        JSONObject d = legArray.getJSONObject(j);
//                                                d.remove("cabinClasses");
//                                                d.remove("ssrDetails");
//                                                d.remove("amount");
////                                                d.remove("group");
//                                                d.remove("isConnecting");
//                                                d.remove("numberOfStops");
////                                                d.remove("providerCode");
//                                                d.remove("remarks");
                        d.remove("cabinClasses");
                        d.remove("ssrDetails");
                        d.remove("amount");
                        d.remove("isConnecting");
                        d.remove("numberOfStops");
                        d.remove("remarks");
                        jsonArray.put(d);
                      }
                      bondary.put("legs", jsonArray);
                      bounds.put(bondary);
                      contingFlight.put("bonds", bounds);
                      String s = segmentObj.getJSONArray("fares").toString();
                      s = s.replace("\"fares\":", "\"bookFares\":");
                      JSONArray jsonArray1 = new JSONArray(s);
                      contingFlight.put("fares", jsonArray1);
                      segmentConting.remove("bondType");
                      segmentConting.remove("remark");
                      contingFlight.put("baggageFare", segmentConting.getString("baggageFare"));
                      contingFlight.put("cache", segmentConting.getString("cache"));
                      contingFlight.put("holdBooking", segmentConting.getString("holdBooking"));
                      contingFlight.put("international", segmentConting.getString("international"));
                      contingFlight.put("roundTrip", segmentConting.getString("roundTrip"));
                      contingFlight.put("special", segmentConting.getString("special"));
                      contingFlight.put("specialId", segmentConting.getString("specialId"));
                      contingFlight.put("engineID", segmentConting.getString("engineID"));
                      contingFlight.put("fareRule", segmentConting.getString("fareRule"));
                      contingFlight.put("itineraryKey", segmentConting.getString("itineraryKey"));
                      contingFlight.put("journeyIndex", segmentConting.getString("journeyIndex"));
                      contingFlight.put("nearByAirport", segmentConting.getString("nearByAirport"));
                      contingFlight.put("searchId", segmentConting.getString("searchId"));
                      contingFlight.put("origin", sourceCode);
                      contingFlight.put("destination", destinationCode);
                      contingFlight.put("tripType", "OneWay");
                      contingFlight.put("cabin", flightClass);
                      contingFlight.put("adults", noOfAdult);
                      contingFlight.put("childs", noOfChild);
                      contingFlight.put("infants", noOfInfant);
                      contingFlight.put("traceId", "AYTM00011111111110002");
                      contingFlight.put("beginDate", dateOfDep);
                      contingFlight.put("endDate", dateOfDep);

                    }

                    for (int j = 0; j < legArray.length(); j++) {
                      JSONObject d = legArray.getJSONObject(j);
                      jsonToSendUp = legArray.getJSONObject(j).toString();

                      String depAirportCode = d.getString("origin");
                      String depTime = d.getString("departureTime");
                      String arrivalAirportCode = d.getString("destination");
                      String arrivalTime = d.getString("arrivalTime");

                      String flightDuration = d.getString("duration");

                      String flightName = d.getString("airlineName");
                      String flightNo = d.getString("flightNumber");
                      String flightImage = d.getString("airlineName");
                      String flightCode = d.getString("aircraftCode");
                      String baggageUnit = d.getString("baggageUnit");
                      String baggageWeight=d.getString("baggageWeight");
                      String flightArrivalTimeZone = d.getString("arrivalDate");

                      String flightDepartureTimeZone = d.getString("departureDate");

                      String flightArrivalAirportName = d.getString("destination");
                      String flightDepartureAirportName = d.getString("origin");
                      String flightTpe;
                      flightTpe = d.getString("flightName");
                      String flightRule = "";

                      JSONArray fareArray = segmentObj.getJSONArray("fares");
                      JSONObject fareObj = fareArray.getJSONObject(0);
                      JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                      JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                      jsonFareToSendUp = paxFaresArray.getJSONObject(0).toString();
                      String duration= bonObj.getString("journeyTime");
                      FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                        flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName, 1, flightTpe, jsonToSendUp, jsonFareToSendUp,baggageUnit,baggageWeight,duration);
                      flightArray.add(flightModel);


                    }
                    JSONArray fareArray = segmentObj.getJSONArray("fares");
                    JSONObject fareObj = fareArray.getJSONObject(0);
                    JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                    JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                    jsonFareToSendUp = paxFaresArray.getJSONObject(0).toString();

                    double flightActualBaseFare = paxFaresObj.getDouble("basicFare");
                    double flightTaxFare = 0.0;
                    double totalFare = 0.0;
                    if (paxFaresObj.has("totalTax")) {
                      flightTaxFare = paxFaresObj.getDouble("totalTax");
                    }
                    if (fareObj.has("totalFareWithOutMarkUp")) {
                      totalFare = fareObj.getDouble("totalFareWithOutMarkUp");
                    }
                    boolean refund = paxFaresObj.getBoolean("refundable");
                    String duration = compareDate1(legArray.getJSONObject(0).getString("departureTime"), legArray.getJSONObject(legArray.length() - 1).getString("arrivalTime"));
                    FlightListModel flightModel = new FlightListModel(flightActualBaseFare, flightTaxFare, 0.0, 0.0, 0.0, totalFare, totalFare, "", flightArray, jsonSegmentFirstString, contingFlight.toString(), flightRoundArray, String.valueOf(refund), legArray.getJSONObject(0).getString("departureTime"), duration);
                    flightListItem.add(flightModel);
                    flightListOneyWay.add(flightModel);
                    flightModelUp = flightListItem.get(0);
                    upPrice = flightModelUp.getFlightNetFare();
                  }
                }

//                                ivHeaderListDate.setVisibility(View.VISIBLE);
                tvHeadOnWardDate.setText(sourceCode + " " + getResources().getString(R.string.arrow) + " " + destinationCode + "\n" + dateOfDep);

                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                rvFlightUp.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

                GridLayoutManager manager = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
                rvFlightUp.setLayoutManager(manager);
                FlightRoundTripListAdapterone = new FlightRoundTripListAdapter
                  (FlightListTwoWayActivity.this, flightListItem, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
                getpriceLowToHight(FlightRoundTripListAdapterone, rvFlightUp);
//                                rvFlightUp.setAdapter(FlightRoundTripListAdapterone);
                loadingDialog.dismiss();
                llNoBus.setVisibility(View.GONE);
              }

              //Return Ticket
              JSONObject jsonSecondJrnyObj = jsonJourney.getJSONObject(1);
              JSONArray jsonSecondSegmentArray = jsonSecondJrnyObj.getJSONArray("segments");

              if (jsonSegmentFirstArray.length() != 0 && jsonSecondSegmentArray.length() != 0) {
                flightListItem = new ArrayList<>();
                for (int k = 0; k < jsonSecondSegmentArray.length(); k++) {
                  String jsonSegmentSecondString = jsonSecondSegmentArray.getJSONObject(k).toString();
                  ArrayList<FlightModel> flightArray = new ArrayList<>();
                  JSONObject segmentObj = jsonSecondSegmentArray.getJSONObject(k);
                  JSONArray bondArray = segmentObj.getJSONArray("bonds");
                  bondsLength = bondArray.length();
                  listfilterstwo.setVisibility(View.VISIBLE);
                  JSONObject bonObj = bondArray.getJSONObject(0);
                  JSONArray legArray = bonObj.getJSONArray("legs");
                  JSONObject segmentConting = jsonSecondSegmentArray.getJSONObject(k);
                  JSONObject contingFlight = new JSONObject();
                  if (legArray.length() >= 1) {
                    JSONArray bounds = new JSONArray();
                    JSONObject bondary = bondArray.getJSONObject(0);
                    bondary.remove("specialServices");
                    bondary.put("addOnDetail", "");
                    JSONArray jsonArray = new JSONArray();
                    for (int j = 0; j < legArray.length(); j++) {

                      JSONObject d = legArray.getJSONObject(j);
                      d.remove("cabinClasses");
                      d.remove("ssrDetails");
                      d.remove("amount");
                      d.remove("isConnecting");
                      d.remove("numberOfStops");
                      d.remove("remarks");
                      jsonArray.put(d);
                    }
                    bondary.put("legs", jsonArray);
                    bounds.put(bondary);
                    contingFlight.put("bonds", bounds);
                    String s = segmentObj.getJSONArray("fares").toString();
                    s = s.replace("\"fares\":", "\"bookFares\":");
                    JSONArray jsonArray1 = new JSONArray(s);
                    contingFlight.put("fares", jsonArray1);
                    segmentConting.remove("bondType");
                    segmentConting.remove("remark");
                    contingFlight.put("baggageFare", segmentConting.getString("baggageFare"));
                    contingFlight.put("cache", segmentConting.getString("cache"));
                    contingFlight.put("holdBooking", segmentConting.getString("holdBooking"));
                    contingFlight.put("international", segmentConting.getString("international"));
                    contingFlight.put("roundTrip", segmentConting.getString("roundTrip"));
                    contingFlight.put("special", segmentConting.getString("special"));
                    contingFlight.put("specialId", segmentConting.getString("specialId"));
                    contingFlight.put("engineID", segmentConting.getString("engineID"));
                    contingFlight.put("fareRule", segmentConting.getString("fareRule"));
                    contingFlight.put("itineraryKey", segmentConting.getString("itineraryKey"));
                    contingFlight.put("journeyIndex", segmentConting.getString("journeyIndex"));
                    contingFlight.put("nearByAirport", segmentConting.getString("nearByAirport"));
                    contingFlight.put("searchId", segmentConting.getString("searchId"));
                    contingFlight.put("origin", destinationCode);
                    contingFlight.put("destination", sourceCode);
                    contingFlight.put("tripType", "OneWay");
                    contingFlight.put("cabin", flightClass);
                    contingFlight.put("adults", noOfAdult);
                    contingFlight.put("childs", noOfChild);
                    contingFlight.put("infants", noOfInfant);
                    contingFlight.put("traceId", "AYTM00011111111110002");
                    contingFlight.put("beginDate", dateOfReturn);
                    contingFlight.put("endDate", dateOfReturn);

                  }

                  for (int j = 0; j < legArray.length(); j++) {
                    JSONObject d = legArray.getJSONObject(j);
                    jsonToSendDown = legArray.getJSONObject(j).toString();

                    String depAirportCode = d.getString("origin");
                    String depTime = d.getString("departureTime");
                    String arrivalAirportCode = d.getString("destination");
                    String arrivalTime = d.getString("arrivalTime");

                    String flightDuration = d.getString("duration");
                    String flightName = d.getString("airlineName");
                    String flightNo = d.getString("flightNumber");
                    String flightImage = d.getString("airlineName");
                    String flightCode = d.getString("aircraftCode");

                    String flightArrivalTimeZone = d.getString("arrivalDate");

                    String flightDepartureTimeZone = d.getString("departureDate");
                    String baggageUnit = d.getString("baggageUnit");
                    String baggageWeight=d.getString("baggageWeight");

                    String flightArrivalAirportName = d.getString("destination");
                    String flightDepartureAirportName = d.getString("origin");
                    String flightTpe;
                    flightTpe = d.getString("flightName");
                    String flightRule = "";
                    JSONArray fareArray = segmentObj.getJSONArray("fares");
                    JSONObject fareObj = fareArray.getJSONObject(0);
                    JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                    JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);
                    jsonFareToSendDown = paxFaresArray.getJSONObject(0).toString();
                    String duration=bonObj.getString("journeyTime");
                    FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                      flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName, 2, flightTpe, jsonToSendDown, jsonFareToSendDown,baggageUnit,baggageWeight,duration);
                    flightArray.add(flightModel);


                  }
                  JSONArray fareArray = segmentObj.getJSONArray("fares");
                  JSONObject fareObj = fareArray.getJSONObject(0);
                  JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                  JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                  jsonFareToSendDown = paxFaresArray.getJSONObject(0).toString();

                  double flightActualBaseFare = paxFaresObj.getDouble("basicFare");
                  double flightTaxFare = 0.0;
                  double totalFare = 0.0;
                  if (paxFaresObj.has("totalTax")) {
                    flightTaxFare = paxFaresObj.getDouble("totalTax");
                  }
                  if (fareObj.has("totalFareWithOutMarkUp")) {
                    totalFare = fareObj.getDouble("totalFareWithOutMarkUp");
                  }
                  boolean refund = paxFaresObj.getBoolean("refundable");
//                  journeyTime
                  String duration =bonObj.getString("journeyTime");
//                    compareDate1(legArray.getJSONObject(0).getString("departureTime"), legArray.getJSONObject(legArray.length() - 1).getString("arrivalTime"));
                  FlightListModel flightModel = new FlightListModel(flightActualBaseFare, flightTaxFare, 0.0, 0.0, 0.0,
                    totalFare, totalFare, "", flightArray, jsonSegmentSecondString, contingFlight.toString(), new ArrayList<FlightModel>(), String.valueOf(refund), legArray.getJSONObject(0).getString("departureTime"), duration);
                  flightListItem.add(flightModel);
                  flightListTwoWay.add(flightModel);
                  flightModelDown = flightListItem.get(0);
                  downPrice = flightModelDown.getFlightNetFare();
                  updatePrice();


                }

//                                ivHeaderListDate.setVisibility(View.VISIBLE);
                tvHeadFlightReturnDate.setText(destinationCode + " " + getResources().getString(R.string.arrow) + " " + sourceCode + "\n" + dateOfReturn);

                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                rvFlightDown.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

                GridLayoutManager manager = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
                rvFlightDown.setLayoutManager(manager);

                FlightRoundTripListAdaptertwo = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListItem, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
                getpriceLowToHight(FlightRoundTripListAdaptertwo, rvFlightDown);

                loadingDialog.dismiss();
                llNoBus.setVisibility(View.GONE);
              } else if (jsonSegmentFirstArray.length() == 0 && jsonSecondSegmentArray.length() != 0) {
                loadingDialog.dismiss();
                llNoBus.setVisibility(View.VISIBLE);
                rvFlightDown.setVisibility(View.GONE);
              } else if (jsonSegmentFirstArray.length() == 0 && jsonSecondSegmentArray.length() == 0) {
                loadingDialog.dismiss();
                llNoBus.setVisibility(View.VISIBLE);
                rvFlightDown.setVisibility(View.GONE);
              }
            } else if (code != null && code.equals("F03")) {
              loadingDialog.dismiss();
              showInvalidSessionDialog();
            } else {
              loadingDialog.dismiss();
            }

          } catch (
            JSONException e)

          {
            e.printStackTrace();
            loadingDialog.dismiss();
            CustomToast.showMessage(getApplicationContext(),  getResources().getString(R.string.server_exception));
          }
        }
      }, new Response.ErrorListener()

      {

        @Override
        public void onErrorResponse(VolleyError error) {
          CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
          loadingDialog.dismiss();
        }
      })

      {


      };
      int socketTimeout = 600000;
      RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
      jsonObjReq.setRetryPolicy(policy);
      PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

  }

  private void updatePrice() {
    double finalPrice = upPrice + downPrice;
    tvFlightRoundFinalPrice.setText(getResources().getString(R.string.rupease) + " " + finalPrice);
  }

  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    super.onDestroy();
  }

  public void showInvalidSessionDialog() {
    Spanned result;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
    } else {
      result = Html.fromHtml(AppMetadata.getInvalidSession());
    }
    CustomAlertDialog builder = new CustomAlertDialog(FlightListTwoWayActivity.this, R.string.dialog_title2, result);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        sendLogout();
      }
    });
    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

      }
    });
    builder.show();
  }

  private void sendLogout() {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "4");
    LocalBroadcastManager.getInstance(FlightListTwoWayActivity.this).sendBroadcast(intent);
  }

  private String compareDate(String depTime, String arrTime) {
    //HH converts hour in 24 hours format (0-23), day calculation
    Log.i("avrialTime", depTime + "destion::" + arrTime);
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");

    Date d1 = null;
    Date d2 = null;

    try {
      d1 = format.parse(depTime);
      d2 = format.parse(arrTime);

      if (d2.after(d1)) {
        Date currentDepature = addMinutesToDate(60, d1);
        if (d2.after(currentDepature)) {
          FlightPriceTwoWayActivity.jsonUpString = jSonUp;
          FlightPriceTwoWayActivity.jsonDownString = jSonDown;
          Intent flightPriceIntent = new Intent(FlightListTwoWayActivity.this, FlightPriceTwoWayActivity.class);
          flightPriceIntent.putExtra("flightUp", flightModelUp);
          flightPriceIntent.putExtra("flightDown", flightModelDown);
          flightPriceIntent.putExtra("postUp", postUp);
          flightPriceIntent.putExtra("postDown", postDown);
          flightPriceIntent.putExtra("dateOfDep", dateOfDep);
          flightPriceIntent.putExtra("dateOfReturn", dateOfReturn);

          flightPriceIntent.putExtra("sourceCode", sourceCode);
          flightPriceIntent.putExtra("destinationCode", destinationCode);

          flightPriceIntent.putExtra("adultNo", noOfAdult);
          flightPriceIntent.putExtra("childNo", noOfChild);
          flightPriceIntent.putExtra("infantNo", noOfInfant);

          flightPriceIntent.putExtra("infantNo", noOfInfant);
          flightPriceIntent.putExtra("flightClass", flightClass);


          flightPriceIntent.putExtra("jsonFareToSendUp", jsonFareToSendUp);

          flightPriceIntent.putExtra("jsonFareToSendDown", jsonFareToSendDown);

          flightPriceIntent.putExtra("jsonToSendUp", jsonToSendUp);
          flightPriceIntent.putExtra("jsonToSendDown", jsonToSendDown);


          startActivity(flightPriceIntent);
        } else {
          AlertDialog.Builder builder = new AlertDialog.Builder(FlightListTwoWayActivity.this);
          builder.setMessage("The selected flights are overlapped.Please have a minimum difference of 60 minutes between onward and return journey to continue booking.");
          builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              dialogInterface.dismiss();
            }
          });
          builder.setCancelable(false);
          builder.create();
          builder.show();
        }

      } else {
        AlertDialog.Builder builder = new AlertDialog.Builder(FlightListTwoWayActivity.this);
        builder.setMessage("The selected flights are overlapped.Please have a minimum difference of 60 minutes between onward and return journey to continue booking.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.flight_filter, menu);

    return true;

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (flightListItem != null && flightListItem.size() != 0) {
      if (id == R.id.menuFilter) {

        LayoutInflater myLayout = LayoutInflater.from(FlightListTwoWayActivity.this);
        final View dialogView = myLayout.inflate(R.layout.dailog_flight_filter, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FlightListTwoWayActivity.this);
        alertDialogBuilder.setView(dialogView);
        RadioGroup rbNonStop = (RadioGroup) dialogView.findViewById(R.id.rbNonStop);
        RadioGroup rg_refurend = (RadioGroup) dialogView.findViewById(R.id.rg_refurend);
        Button btnFilter = (Button) dialogView.findViewById(R.id.btnFilter);
        rbRefundableonew = (RadioButton) dialogView.findViewById(R.id.rbRefundableonew);
        rbNonRefundableround = (RadioButton) dialogView.findViewById(R.id.rbNonRefundableround);
        Button btnResetFilter = (Button) dialogView.findViewById(R.id.btnResetFilter);
        ImageView ivFilterClose = (ImageView) dialogView.findViewById(R.id.ivFilterClose);
        AppCompatCheckBox allAirLines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAirLines);
        final Spinner spDepature = (Spinner) dialogView.findViewById(R.id.spDepature);
        RadioButton rbRefundable = (RadioButton) dialogView.findViewById(R.id.rbRefundable);
        Spinner spRetrunDepature = (Spinner) dialogView.findViewById(R.id.spRetrunDepature);
        spRetrunDepature.setVisibility(View.GONE);
        TextView retrun = (TextView) dialogView.findViewById(R.id.retrun);
        retrun.setVisibility(View.GONE);
        ArrayList<String> strings = new ArrayList<>();
        strings.add("All Time");
        strings.add("Before 11:00 am");
        strings.add("11:00 - 5:00 pm");
        strings.add("5:00 - 9:00 pm");
        strings.add("above 9:00 pm");
        LinearLayout oneway = (LinearLayout) dialogView.findViewById(R.id.oneway);
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(FlightListTwoWayActivity.this, android.R.layout.simple_expandable_list_item_1, strings);
        spDepature.setAdapter(stringArrayAdapter);
        spRetrunDepature.setAdapter(stringArrayAdapter);
        all_go_AirLines = (AppCompatCheckBox) dialogView.findViewById(R.id.all_go_AirLines);
        allAir_indigo_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_indigo_Lines);
        allAir_jet_air_Lines_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_jet_air_Lines_Lines);
        allAir_spice_jet_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_spice_jet_Lines);
        allAir_air_india_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_air_india_Lines);
        allAir_vistara_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_vistara_Lines);
        ivFilterClose.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            alertDialog.dismiss();
          }
        });
        if (intentional) {
          oneway.setVisibility(View.GONE);
        }
        rbNonRefundableround.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              roundWayValue = false;
            }
          }
        });
        rbRefundableonew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              roundWayValue = true;
            }
          }
        });
        if (rbRefundableonew.isChecked()) {
          roundWayValue = true;
        }
        airAsia = "";
        airIndia = "";
        spicejet = "";
        jetairways = "";
        indigo = "";
        goAir = "";
        count = 0;
        currentPostion = 0;
        all_go_AirLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              goAir = "GoAir";
              count++;
            } else {
              count--;
            }

          }
        });
        allAir_indigo_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              indigo = "Indigo";
              count++;
            } else {
              count--;
            }
          }
        });
        allAir_jet_air_Lines_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              jetairways = "JetAirWays";
              count++;
            } else {
              count--;
            }
          }
        });
        allAir_spice_jet_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              spicejet = "Spicejet";
              count++;
            } else {
              count--;
            }

          }
        });
        allAir_air_india_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              airIndia = "AirIndia";
              count++;
            } else {
              count--;
            }

          }
        });
        allAir_vistara_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              airAsia = "AirAsia";
              count++;
            } else {
              count--;
            }

          }
        });
        btnResetFilter.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            alertDialog.dismiss();
            airAsia = "";
            airIndia = "";
            spicejet = "";
            jetairways = "";
            indigo = "";
            goAir = "";
            count = 0;
            currentPostion = 0;
            FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListOneyWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
            getpriceLowToHight(FlightRoundTripListAdapterone, rvFlightUp);
            FlightRoundTripListAdaptertwo = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListTwoWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
            getpriceLowToHight(FlightRoundTripListAdaptertwo, rvFlightDown);

          }
        });
        allAirLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
              all_go_AirLines.setChecked(true);
              allAir_indigo_Lines.setChecked(true);
              allAir_jet_air_Lines_Lines.setChecked(true);
              allAir_spice_jet_Lines.setChecked(true);
              allAir_air_india_Lines.setChecked(true);
              allAir_vistara_Lines.setChecked(true);
              allAirLinesValue = true;
            } else {
              all_go_AirLines.setChecked(false);
              allAir_indigo_Lines.setChecked(false);
              allAir_jet_air_Lines_Lines.setChecked(false);
              allAir_spice_jet_Lines.setChecked(false);
              allAir_air_india_Lines.setChecked(false);
              allAir_vistara_Lines.setChecked(false);
              allAirLinesValue = false;
            }
          }
        });


        refund = "null";
        nonStopValue = "null";
        rbNonStop.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                               @Override
                                               public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                 switch (checkedId) {
                                                   case R.id.nonStop:
                                                     // do operations specific to this selection
                                                     nonStopValue = "1";
                                                     break;
                                                   case R.id.one_Stop:
                                                     // do operations specific to this selection
                                                     nonStopValue = "2";
                                                     break;
                                                   case R.id.moreStops:
                                                     // do operations specific to this selection
                                                     nonStopValue = "3";
                                                     break;
                                                 }
                                               }
                                             }
        );
        rg_refurend.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                 @Override
                                                 public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                   switch (checkedId) {
                                                     case R.id.rbRefundable:
                                                       // do operations specific to this selection
                                                       refund = "true";
                                                       break;
                                                     case R.id.rbNonRefundable:
                                                       // do operations specific to this selection
                                                       refund = "false";
                                                       break;

                                                   }
                                                 }
                                               }
        );
        btnFilter.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            filtterList(refund, nonStopValue, spDepature.getSelectedItem().toString());
            alertDialog.dismiss();
          }
        });


        // create alert dialog
        alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();
        alertDialog.setCancelable(true);
      }
    } else {
      CustomToast.showMessage(FlightListTwoWayActivity.this, getResources().getString(R.string.no_flight));
    }
    return super.onOptionsItemSelected(item);
  }

  private void filtterList(String refund, String nonStopValue, String depature) {
    Log.i("values", String.valueOf(roundWayValue));
    List<FlightListModel> flightListModels = new ArrayList<>();

    if (refund.equalsIgnoreCase("null") || !nonStopValue.equalsIgnoreCase("null") || !refund.equalsIgnoreCase("null") || nonStopValue.equalsIgnoreCase("null")) {

      List<FlightListModel> flightListItem;
      List<FlightListModel> flightListModels1 = null;
      if (roundWayValue) {
        flightListItem = FlightRoundTripListAdapterone.getFlightArray();
        flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);
      } else if (!roundWayValue) {
        flightListItem = FlightRoundTripListAdaptertwo.getFlightArray();
        flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);
      }

      if (flightListModels1 != null && flightListModels1.size() != 0 && depature.equalsIgnoreCase("All Time")) {
        flightListModels = flightListModels1;
      } else if (refund.equalsIgnoreCase("null") && nonStopValue.equalsIgnoreCase("null") && !depature.equalsIgnoreCase("All Time")) {
        if (roundWayValue) {
          flightListModels1 = FlightRoundTripListAdapterone.getFlightArray();
        } else if (!roundWayValue) {
          flightListModels1 = FlightRoundTripListAdaptertwo.getFlightArray();
        }
      }

      if (refund.equalsIgnoreCase("null") && nonStopValue.equalsIgnoreCase("null") && !allAirLinesValue && depature.equalsIgnoreCase("All Time")) {
        if (roundWayValue) {
          flightListModels = FlightRoundTripListAdapterone.getFlightArray();
        } else if (!roundWayValue) {
          flightListModels = FlightRoundTripListAdaptertwo.getFlightArray();
        }
      }
      if (refund.equalsIgnoreCase("null") && nonStopValue.equalsIgnoreCase("null") && allAirLinesValue && depature.equalsIgnoreCase("All Time")) {
        if (roundWayValue) {
          flightListModels = FlightRoundTripListAdapterone.getFlightArray();
        } else if (!roundWayValue) {
          flightListModels = FlightRoundTripListAdaptertwo.getFlightArray();
        }
      }
      if (flightListModels1.size() != 0 && flightListModels1 != null && depature.contains("Before")) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("11:00 AM");
          String hoursList = displayFormat.format(date);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).before(date(hoursList))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("11:00 - 5:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("11:00 AM");
          Date secondDate = parseFormat.parse("5:00 PM");
          String hoursList = displayFormat.format(date);
          String hoursList1 = displayFormat.format(secondDate);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
              flightListModels.add(listModel);
            }
          }
        } catch (ParseException e) {
          e.printStackTrace();
        }

      } else if (depature.contains("5:00 - 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("5:00 PM");
          Date secondDate = parseFormat.parse("9:00 PM");
          String hoursList = displayFormat.format(date);
          String hoursList1 = displayFormat.format(secondDate);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
              flightListModels.add(listModel);
            }
          }
        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("above 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("9:00 PM");
          String hoursList = displayFormat.format(date);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).after(date(hoursList))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      }

    } else if (!refund.equalsIgnoreCase("null") && !nonStopValue.equalsIgnoreCase("null") && depature != null && !depature.isEmpty()) {
      List<FlightListModel> flightListItem;
      ArrayList<FlightListModel> flightListModels1 = null;
      if (roundWayValue) {
        flightListItem = FlightRoundTripListAdapterone.getFlightArray();
        flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);
      } else if (!roundWayValue) {
        flightListItem = FlightRoundTripListAdaptertwo.getFlightArray();
        flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);
      }
      if (flightListModels1.size() != 0 && flightListModels1 != null && depature.contains("Before")) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("11:00 AM");
          String hoursList = displayFormat.format(date);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).before(date(hoursList))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("11:00 - 5:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("11:00 AM");
          Date secondDate = parseFormat.parse("5:00 PM");
          String hoursList = displayFormat.format(date);
          String hoursList1 = displayFormat.format(secondDate);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }

      } else if (depature.contains("5:00 - 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("5:00 PM");
          Date secondDate = parseFormat.parse("9:00 PM");
          String hoursList = displayFormat.format(date);
          String hoursList1 = displayFormat.format(secondDate);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("above 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date = parseFormat.parse("9:00 PM");
          String hoursList = displayFormat.format(date);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).after(date(hoursList))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("All Time") && flightListModels1.size() != 0 && flightListModels1 != null) {
        flightListModels = flightListModels1;
      }


    }
    if (!refund.equalsIgnoreCase("null") && !nonStopValue.equalsIgnoreCase("null") && depature != null && !depature.isEmpty()) {
      List<FlightListModel> flightListItem;
      ArrayList<FlightListModel> flightListModels1 = null;
      if (roundWayValue) {
        flightListItem = FlightRoundTripListAdapterone.getFlightArray();
        flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);

      } else if (!roundWayValue) {
        flightListItem = FlightRoundTripListAdaptertwo.getFlightArray();
        flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);
      }


      if (flightListModels1 != null && depature.equalsIgnoreCase("All Time")) {
        flightListModels = flightListModels1;
      } else if (flightListModels1 != null && !depature.equalsIgnoreCase("All Time")) {
        flightListModels = flightListModels1;
      }
      if (flightListModels1.size() != 0 && flightListModels1 != null && depature.contains("Before")) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date1 = parseFormat.parse("11:00 AM");
          String hoursList = displayFormat.format(date1);
          for (FlightListModel listModel : flightListModels1) {
            Log.i("depature9", listModel.getDepature() + hoursList);
            if (date(listModel.getDepature()).before(date(hoursList))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("11:00-5:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date1 = parseFormat.parse("11:00 AM");
          Date secondDate = parseFormat.parse("5:00 PM");
          String hoursList = displayFormat.format(date1);
          String hoursList1 = displayFormat.format(secondDate);
          for (FlightListModel listModel : flightListModels1) {
            Log.i("depature9", listModel.getDepature() + hoursList + hoursList1);
            if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }

      } else if (depature.contains("5:00 - 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date1 = parseFormat.parse("5:00 PM");
          Date secondDate = parseFormat.parse("9:00 PM");
          String hoursList = displayFormat.format(date1);
          String hoursList1 = displayFormat.format(secondDate);
          for (FlightListModel listModel : flightListModels1) {
            if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("above 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
          Date date1 = parseFormat.parse("9:00 PM");
          String hoursList = displayFormat.format(date1);
          for (FlightListModel listModel : flightListModels1) {
            Log.i("depature9", listModel.getDepature() + hoursList);
            if (date(listModel.getDepature()).after(date(hoursList))) {
              flightListModels.add(listModel);
            }
          }

        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else if (depature.contains("All Time") && flightListModels1.size() != 0 && flightListModels1 != null) {
        flightListModels = flightListModels1;
      }

    }
    if (allAirLinesValue && flightListModels.size() != 0 && flightListModels != null) {
      if (getNonStop(flightListModels, "Spicejet").size() != 0 && getNonStop(flightListModels, "JetAirways").size() != 0 && getNonStop(flightListModels, "Indigo").size() != 0 && getNonStop(flightListModels, "Airindia").size() != 0 && getNonStop(flightListModels, "AirAsia").size() != 0 && getNonStop(flightListModels, "GoAir").size() != 0) {
        flightListModels.addAll(getNonStop(flightListModels, "Spicejet"));
        flightListModels.addAll(getNonStop(flightListModels, "JetAirWays"));
        flightListModels.addAll(getNonStop(flightListModels, "Indigo"));
        flightListModels.addAll(getNonStop(flightListModels, "AirIndia"));
        flightListModels.addAll(getNonStop(flightListModels, "AirAsia"));
        flightListModels.addAll(getNonStop(flightListModels, "GoAir"));
      } else {
        flightListModels = new ArrayList<>();
      }

    }
    if (!allAirLinesValue && flightListModels.size() != 0 && flightListModels != null) {
      if (spicejet.equalsIgnoreCase("") && jetairways.equalsIgnoreCase("") && indigo.equalsIgnoreCase("") && airIndia.equalsIgnoreCase("") && airAsia.equalsIgnoreCase("") && goAir.equalsIgnoreCase("")) {
        if (flightListModels != null && flightListModels.size() != 0) {
          if (roundWayValue) {
//                        airAsia = "";
//                        airIndia = "";
//                        spicejet = "";
//                        jetairways = "";
//                        indigo = "";
//                        goAir = "";
//                        count = 0;
//                        currentPostion = 0;
            FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
            rvFlightUp.setAdapter(FlightRoundTripListAdapterone);
          } else if (!roundWayValue) {
//                        airAsia = "";
//                        airIndia = "";
//                        spicejet = "";
//                        jetairways = "";
//                        indigo = "";
//                        goAir = "";
//                        count = 0;
//                        currentPostion = 0;
            FlightRoundTripListAdaptertwo = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
            rvFlightDown.setAdapter(FlightRoundTripListAdaptertwo);
          }
        } else {
          if (refund.equalsIgnoreCase("null") && nonStopValue.equalsIgnoreCase("null") && depature.equalsIgnoreCase("All Time")) {
            if (roundWayValue) {
//                            airAsia = "";
//                            airIndia = "";
//                            spicejet = "";
//                            jetairways = "";
//                            indigo = "";
//                            goAir = "";
//                            count = 0;
//                            currentPostion = 0;
              FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListOneyWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
              rvFlightUp.setAdapter(FlightRoundTripListAdapterone);
            } else if (!roundWayValue) {
//                            airAsia = "";
//                            airIndia = "";
//                            spicejet = "";
//                            jetairways = "";
//                            indigo = "";
//                            goAir = "";
//                            count = 0;
//                            currentPostion = 0;
              FlightRoundTripListAdaptertwo = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListTwoWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
              rvFlightDown.setAdapter(FlightRoundTripListAdaptertwo);
            }
          } else {
            llNoFiltter.setVisibility(View.VISIBLE);
            CustomFiltterDialog customFiltterDialog = new CustomFiltterDialog(FlightListTwoWayActivity.this, "", "", new CitySelectedListener() {
              @Override
              public void citySelected(String type, long cityCode, String cityName) {
                if (roundWayValue) {
                  airAsia = "";
                  airIndia = "";
                  spicejet = "";
                  jetairways = "";
                  indigo = "";
                  goAir = "";
                  count = 0;
                  currentPostion = 0;
                  llNoFiltter.setVisibility(View.GONE);
                  FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListOneyWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
                  rvFlightUp.setAdapter(FlightRoundTripListAdapterone);
                } else if (!roundWayValue) {
                  airAsia = "";
                  airIndia = "";
                  spicejet = "";
                  jetairways = "";
                  indigo = "";
                  goAir = "";
                  count = 0;
                  currentPostion = 0;
                  llNoFiltter.setVisibility(View.GONE);
                  FlightRoundTripListAdaptertwo = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListTwoWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
                  rvFlightDown.setAdapter(FlightRoundTripListAdaptertwo);
                }
              }

              @Override
              public void cityFilter(BusCityModel type) {

              }

              @Override
              public void bankFilter(BankListModel type) {

              }

              @Override
              public void travelFilter(TravelCityModel type) {

              }
            });

            customFiltterDialog.show();

          }
        }
      } else {
        ArrayList<FlightListModel> models = new ArrayList<>();
        models.addAll(getNonStop(flightListModels, spicejet));
        models.addAll(getNonStop(flightListModels, jetairways));
        models.addAll(getNonStop(flightListModels, indigo));
        models.addAll(getNonStop(flightListModels, airAsia));
        models.addAll(getNonStop(flightListModels, airIndia));
        models.addAll(getNonStop(flightListModels, goAir));
        Log.i("count", currentPostion + "::" + count);
        if (count == currentPostion) {
          flightListModels = models;
        } else {
          flightListModels = new ArrayList<>();
        }

      }
    }


    if (flightListModels != null && flightListModels.size() != 0) {
      if (roundWayValue) {
        airAsia = "";
        airIndia = "";
        spicejet = "";
        jetairways = "";
        indigo = "";
        goAir = "";
        count = 0;
        currentPostion = 0;
        FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
        rvFlightUp.setAdapter(FlightRoundTripListAdapterone);
      } else if (!roundWayValue) {
        airAsia = "";
        airIndia = "";
        spicejet = "";
        jetairways = "";
        indigo = "";
        goAir = "";
        count = 0;
        currentPostion = 0;
        FlightRoundTripListAdaptertwo = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
        rvFlightDown.setAdapter(FlightRoundTripListAdaptertwo);
      }
    } else {
      llNoFiltter.setVisibility(View.VISIBLE);

      CustomFiltterDialog customFiltterDialog = new CustomFiltterDialog(FlightListTwoWayActivity.this, "", "", new CitySelectedListener() {
        @Override
        public void citySelected(String type, long cityCode, String cityName) {
          if (roundWayValue) {
            airAsia = "";
            airIndia = "";
            spicejet = "";
            jetairways = "";
            indigo = "";
            goAir = "";
            count = 0;
            currentPostion = 0;
            llNoFiltter.setVisibility(View.GONE);
            FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListOneyWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
            rvFlightUp.setAdapter(FlightRoundTripListAdapterone);
          } else {
            llNoFiltter.setVisibility(View.GONE);
            airAsia = "";
            airIndia = "";
            spicejet = "";
            jetairways = "";
            indigo = "";
            goAir = "";
            count = 0;
            currentPostion = 0;
            FlightRoundTripListAdaptertwo = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListTwoWay, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
            rvFlightDown.setAdapter(FlightRoundTripListAdapterone);
          }
        }

        @Override
        public void cityFilter(BusCityModel type) {

        }

        @Override
        public void bankFilter(BankListModel type) {

        }

        @Override
        public void travelFilter(TravelCityModel type) {

        }
      });

      customFiltterDialog.show();
    }

  }

  ArrayList<FlightListModel> getRefurend(List<FlightListModel> flightListItem, String refund, String nonStop) {

    ArrayList<FlightListModel> flightListModels = new ArrayList<>();
    for (FlightListModel listModel : flightListItem) {
      Log.i("list_refund", String.valueOf(listModel.getFlightListArray().size()));
      if (!nonStop.equalsIgnoreCase("null") && !refund.equalsIgnoreCase("null")) {
        if (listModel.getFlightListArray().size() == Integer.parseInt(nonStop)) {
          if (listModel.getRefund().equalsIgnoreCase(refund)) {
            flightListModels.add(listModel);
          }
        }
      }
      if (refund.equalsIgnoreCase("null") && !nonStop.equalsIgnoreCase("null")) {
        if (listModel.getFlightListArray().size() == Integer.parseInt(nonStop)) {
          flightListModels.add(listModel);
        }
      }
      if (nonStop.equalsIgnoreCase("null") && !refund.equalsIgnoreCase("null")) {
        if (listModel.getRefund().equalsIgnoreCase(refund)) {
          flightListModels.add(listModel);
        }
      }
    }
    Log.i("list_refund", refund + nonStop + "list" + flightListModels.size());
    return flightListModels;
  }

  ArrayList<FlightListModel> getNonStop(List<FlightListModel> flightListItem, String flag) {
    Log.i("flag", flag);
    ArrayList<FlightListModel> flightListModels = new ArrayList<>();
    for (FlightListModel listModel : flightListItem) {
      if (listModel.getFlightJsonString().contains("\"flightName\":" + "\"" + flag + "\"")) {
        flightListModels.add(listModel);
      }

    }
    if (flightListModels != null && flightListModels.size() != 0) {
      currentPostion++;
    }
    return flightListModels;
  }

  private void getDepartureFilterHightToLow(FlightRoundTripListAdapter FlightRoundTripListAdapterone, RecyclerView recyclerView) {
    List<FlightListModel> flightListModels = FlightRoundTripListAdapterone.getFlightArray();
    List<FlightListModel> listModels = new ArrayList<>();

    Collections.sort(flightListModels, new Comparator<FlightListModel>() {
      @Override
      public int compare(FlightListModel lhs, FlightListModel rhs) {
        return parseDate(lhs.getDepature()) == null ? Integer.MAX_VALUE : parseDate(rhs.getDepature()) == null ? Integer.MIN_VALUE : parseDate(lhs.getDepature()).compareTo(parseDate(rhs.getDepature()));

      }
    });

    if (flightListModels != null && flightListModels.size() != 0) {

      FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListItem, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);

      recyclerView.setAdapter(FlightRoundTripListAdapterone);
    }


  }

  private Date date(String date) {
    Date arrTime = null;
    SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    try {
      arrTime = arrinput.parse(date);
//                depTime = depinput.parse(busArray.get(position).getBusDepTime());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return arrTime;
  }

  private Long parseDate(String date) {
    Date arrTime = null;
    SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    try {
      arrTime = arrinput.parse(date);
//                depTime = depinput.parse(busArray.get(position).getBusDepTime());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return arrTime.getTime();
  }

  private Long parseDuration(String date) {
    Date arrTime = null;
    SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
    try {
      String s = date.replaceAll(" h", ":").trim();
      String value = s.replaceAll("m", "").trim();
      arrTime = arrinput.parse(s.trim());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return arrTime.getTime();
  }

  private void getDUrationLowTOHigh(FlightRoundTripListAdapter FlightRoundTripListAdapterone, RecyclerView recyclerView) {
    List<FlightListModel> flightListModels = FlightRoundTripListAdapterone.getFlightArray();
    List<FlightListModel> listModels = new ArrayList<>();

    Collections.sort(flightListModels, new Comparator<FlightListModel>() {
      @Override
      public int compare(FlightListModel lhs, FlightListModel rhs) {
        return parseDuration(lhs.getDuration()) == null ? Integer.MAX_VALUE : parseDuration(rhs.getDuration()) == null ? Integer.MIN_VALUE : parseDuration(rhs.getDuration()).compareTo(parseDuration(lhs.getDuration()));
      }
    });

    if (flightListModels != null && flightListModels.size() != 0) {

      FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);

      recyclerView.setAdapter(FlightRoundTripListAdapterone);
    }
  }

  private void getDUrationHighTOLow(FlightRoundTripListAdapter FlightRoundTripListAdapterone, RecyclerView recyclerView) {
    List<FlightListModel> flightListModels = FlightRoundTripListAdapterone.getFlightArray();
    List<FlightListModel> listModels = new ArrayList<>();

    Collections.sort(flightListModels, new Comparator<FlightListModel>() {
      @Override
      public int compare(FlightListModel lhs, FlightListModel rhs) {
        return parseDuration(rhs.getDuration()) == null ? Integer.MAX_VALUE : parseDuration(lhs.getDuration()) == null ? Integer.MIN_VALUE : parseDuration(lhs.getDuration()).compareTo(parseDuration(rhs.getDuration()));
      }
    });
    if (flightListModels != null && flightListModels.size() != 0) {
      FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);

      recyclerView.setAdapter(FlightRoundTripListAdapterone);
    }
  }

  private void getpriceHighToLow(FlightRoundTripListAdapter FlightRoundTripListAdapterone, RecyclerView recyclerView) {
    List<FlightListModel> flightListModels = FlightRoundTripListAdapterone.getFlightArray();

    Collections.sort(flightListModels, new Comparator<FlightListModel>() {
      @Override
      public int compare(FlightListModel lhs, FlightListModel rhs) {
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
        return (lhs.getFlightNetFare()) > rhs.getFlightNetFare() ? -1 : lhs.getFlightNetFare() < rhs.getFlightNetFare() ? 1 : 0;
      }
    });

    if (flightListModels != null && flightListModels.size() != 0) {
      FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);
      recyclerView.setAdapter(FlightRoundTripListAdapterone);
    }
  }

  private void getpriceLowToHight(FlightRoundTripListAdapter FlightRoundTripListAdapterone, RecyclerView recyclerView) {
    List<FlightListModel> flightListModels = FlightRoundTripListAdapterone.getFlightArray();


    Collections.sort(flightListModels, new Comparator<FlightListModel>() {
      @Override
      public int compare(FlightListModel lhs, FlightListModel rhs) {
        // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
        return lhs.getFlightNetFare() > rhs.getFlightNetFare() ? 1 : lhs.getFlightNetFare() < rhs.getFlightNetFare() ? -1 : 0;
      }
    });

    if (flightListModels != null && flightListModels.size() != 0) {

      FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);


      recyclerView.setAdapter(FlightRoundTripListAdapterone);
    }
  }

  private void getDepartureFilter(FlightRoundTripListAdapter FlightRoundTripListAdapterone, RecyclerView recyclerView) {
    List<FlightListModel> flightListModels = FlightRoundTripListAdapterone.getFlightArray();
    Collections.sort(flightListModels, new Comparator<FlightListModel>() {
      @Override
      public int compare(FlightListModel lhs, FlightListModel rhs) {
        return parseDate(lhs.getDepature()) == null ? Integer.MAX_VALUE : parseDate(rhs.getDepature()) == null ? Integer.MIN_VALUE : parseDate(rhs.getDepature()).compareTo(parseDate(lhs.getDepature()));

      }
    });

    if (flightListModels != null && flightListModels.size() != 0) {

      FlightRoundTripListAdapterone = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListModels, sourceCode, destinationCode, dateOfDep, noOfAdult, noOfChild, noOfInfant, bondsLength);

      recyclerView.setAdapter(FlightRoundTripListAdapterone);
    }
  }

  public String compareDate1(String depTime, String arrTime) {
    //HH converts hour in 24 hours format (0-23), day calculation
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");

    Date d1 = null;
    Date d2 = null;


    try {
      d1 = format.parse(depTime);
      d2 = format.parse(arrTime);

      long difference = d2.getTime() - d1.getTime();

      if (difference < 0) {
        Date dateMax = format.parse("24:00");
        Date dateMin = format.parse("00:00");
        difference = (dateMax.getTime() - d1.getTime()) + (d2.getTime() - dateMin.getTime());
      }
      int days = (int) (difference / (1000 * 60 * 60 * 24));
      int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
      int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

      Calendar cal = Calendar.getInstance();
      cal.setTime(d1);
      cal.add(Calendar.HOUR, (int) hours);
      cal.add(Calendar.MINUTE, (int) min);
      String newTime = format.format(cal.getTime());
      if (days >= 1) {
        String date2 = String.valueOf(days + "d" + hours + "h" + min + "m");
        return date2.replace("-", "");
      } else {
        String date2 = String.valueOf(hours + " h" + min + " m");
        return date2.replace("-", "");
      }
//            }
      //in milliseconds


    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;

  }

  public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
      this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
      outRect.left = space;
      outRect.right = space;
      outRect.bottom = space;
      outRect.top = space;
    }
  }


}
