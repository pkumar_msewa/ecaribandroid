package in.msewa.ecarib.activity.shopping;//package in.payqwik.test.activity.shopping;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Paint;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//import com.squareup.picasso.Picasso;
//import com.uber.sdk.android.core.auth.LoginActivity;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import in.msewa.custom.CustomToast;
//import in.msewa.custom.LoadingDialog;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.model.PQCart;
//import in.msewa.model.ShoppingModel;
//import in.msewa.model.UserModel;
//import in.msewa.test.PayQwikApplication;
//import in.msewa.test.R;
//import in.msewa.util.AddRemoveCartListner;
//import in.msewa.util.NetworkErrorHandler;
//
//
//public class ShoppingAdapter extends BaseAdapter {
//
//    private List<ShoppingModel> shoppingArray;
//    //    private Context context;
//    private AppCompatActivity act;
//    private LayoutInflater layoutInflater;
//    ViewHolder viewHolder;
//    private PQCart pqCart = PQCart.getInstance();
//    private RequestQueue rq;
//    private UserModel session = UserModel.getInstance();
//    private AddRemoveCartListner adpListner;
//    private Context context;
//    private JsonObjectRequest postReq;
//    private String tag_json_obj = "json_events";
//
//    private LoadingDialog loadingDialog;
//
//    public ShoppingAdapter(Context context, List<ShoppingModel> shoppingArray, AppCompatActivity act, AddRemoveCartListner adapterListner) {
//        this.shoppingArray = shoppingArray;
//        this.act = act;
//        this.adpListner = adapterListner;
//        this.context = context;
//
//        loadingDialog = new LoadingDialog(act);
//
//        try {
//            rq = Volley.newRequestQueue(act);
//            layoutInflater = LayoutInflater.from(act);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    @Override
//    public int getCount() {
//        return shoppingArray.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return shoppingArray.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return i;
//    }
//
//    @Override
//    public View getView(int i, View convertView, ViewGroup viewGroup) {
//        final int currentPostion = i;
//        if (convertView == null) {
//            convertView = layoutInflater.inflate(R.layout.adapter_shopping, viewGroup, false);
//            viewHolder = new ViewHolder();
//            viewHolder.ivShoppingItems = (ImageView) convertView.findViewById(R.id.ivShoppingItems);
//            viewHolder.tvShoppingPrice = (TextView) convertView.findViewById(R.id.tvShoppingPrice);
//            viewHolder.tvShoppingName = (TextView) convertView.findViewById(R.id.tvShoppingName);
//            viewHolder.tvShoppingBrand = (TextView) convertView.findViewById(R.id.tvShoppingBrand);
//            viewHolder.btnShoppingDetails = (Button) convertView.findViewById(R.id.btnShoppingDetails);
//            viewHolder.btnShoppingRemove = (Button) convertView.findViewById(R.id.btnShoppingRemove);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }
//
//        if (pqCart.isProductInCart(shoppingArray.get(currentPostion))) {
//            viewHolder.btnShoppingRemove.setText("Remove");
//            viewHolder.btnShoppingRemove.setBackgroundResource(R.drawable.bg_gray_btn);
//
//        } else {
//            viewHolder.btnShoppingRemove.setText("Add");
//            viewHolder.btnShoppingRemove.setBackgroundResource(R.drawable.bg_red_btn);
//        }
//
//
//        viewHolder.btnShoppingRemove.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (session.getIsValid() == 1) {
//                    if (pqCart.isProductInCart(shoppingArray.get(currentPostion))) {
//                        removeProduct(shoppingArray.get(currentPostion));
//                        viewHolder.btnShoppingRemove.setText("Demo");
//                    } else {
//                        addProduct(shoppingArray.get(currentPostion));
//                        viewHolder.btnShoppingRemove.setText("Demo1");
//                    }
//                } else {
//                    Intent loginIntent = new Intent(act, LoginActivity.class);
//                    act.startActivity(loginIntent);
//                }
//            }
//        });
//
//        viewHolder.tvShoppingPrice.setText("Rs. " + shoppingArray.get(currentPostion).getpPrice());
//        viewHolder.tvShoppingName.setText(shoppingArray.get(currentPostion).getpName());
//        viewHolder.tvShoppingBrand.setText(shoppingArray.get(currentPostion).getpBrand());
//        Picasso.with(act).load(shoppingArray.get(currentPostion).getpImage()).placeholder(R.drawable.loading_image).into(viewHolder.ivShoppingItems);
//
//        viewHolder.btnShoppingDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent detailIntent = new Intent(act, ProductDetailActivity.class);
//                detailIntent.putExtra("Shopping Data", shoppingArray.get(currentPostion));
//                detailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                act.startActivity(detailIntent);
//            }
//        });
//        return convertView;
//
//    }
//
//
////    public void removeProduct(final ShoppingModel shopingModel){
////        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_REMOVE_CART+session.getUserMobileNo()+"&PRODID="+shopingModel.getPid()+"&SIZEID=-1", (String)null,
////                new Response.Listener<JSONObject>() {
////                    @Override
////                    public void onResponse(JSONObject response) {
////                        Log.i("aDD rESPONSE",response.toString());
////                        try {
////                            String responseCode = response.getString("response_code");
////                            if (responseCode.equals("0")) {
////                                pqCart.addProductsInCart(shopingModel);
////                                notifyDataSetChanged();
////                                adpListner.taskCompleted("Remove");
////                                loadingDialog.dismiss();
////
////                            }
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                            loadingDialog.dismiss();
////                        }
////                    }
////                }, new Response.ErrorListener() {
////
////            @Override
////            public void onErrorResponse(VolleyError error) {
////                loadingDialog.dismiss();
////            }
////
////        }) {
////            @Override
////            public Map<String, String> getHeaders() throws AuthFailureError {
////                HashMap<String, String> map = new HashMap<>();
////                map.put("key", "12345");
////                return map;
////            }
////
////        };
////        rq.add(jsonObjReq);
////    }
//
//    public void removeProduct(final ShoppingModel shopingModel) {
//        loadingDialog.show();
//        JSONObject jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobilenumber", session.getUserMobileNo());
//            jsonRequest.put("productDetailid", shopingModel.getPid());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("CARTREMOVEONEURL", ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART);
//            Log.i("CARTREMOVEONEREQ", jsonRequest.toString());
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_REMOVE_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject jsonObj) {
//                    try {
//                        Log.i("CARTREMOVEONERES", jsonObj.toString());
//                        String code = jsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            pqCart.addProductsInCart(shopingModel);
//                            adpListner.taskCompleted("Remove");
//                            loadingDialog.dismiss();
//                            notifyDataSetChanged();
//                        }
//
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        e.printStackTrace();
//                        Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 120000;
//
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
//    public void addProduct(final ShoppingModel shopingModel) {
//        loadingDialog.show();
//        JSONObject jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobilenumber", session.getUserMobileNo());
//            jsonRequest.put("productDetailid", shopingModel.getPid());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("CARTADDONEURL", ApiUrl.URL_SHOPPONG_ADD_ITEM_CART);
//            Log.i("CARTADDONEREQ", jsonRequest.toString());
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_ADD_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject jsonObj) {
//                    try {
//                        Log.i("CARTADDONERES", jsonObj.toString());
//                        String code = jsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            pqCart.addProductsInCart(shopingModel);
//                            viewHolder.btnShoppingRemove.setText("Remove");
//                            adpListner.taskCompleted("Add");
//                            loadingDialog.dismiss();
//                            notifyDataSetChanged();
//                        }
//
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        e.printStackTrace();
//                        Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 120000;
//
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
////    public void addProduct(final ShoppingModel shopingModel){
////        String pName = null;
////        try {
////            pName = URLEncoder.encode(shopingModel.getpName(), "UTF-8");
////        } catch (UnsupportedEncodingException e) {
////            e.printStackTrace();
////        }
////        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_ADD_CART+session.getUserMobileNo()+"&PRODID="+shopingModel.getPid()+"&PRODDESC="+ pName+"&SIZEID=1&SIZEDESC=NA&QNTY=-1&EACH=1000", (String)null,
////                new Response.Listener<JSONObject>() {
////                    @Override
////                    public void onResponse(JSONObject response) {
////                        Log.i("aDD rESPONSE",response.toString());
////                        try {
////                            String responseCode = response.getString("response_code");
////                            if (responseCode.equals("0")) {
////                                pqCart.addProductsInCart(shopingModel);
////                                notifyDataSetChanged();
////                                adpListner.taskCompleted("Add");
////                                loadingDialog.dismiss();
////
////                            }
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                            loadingDialog.dismiss();
////                        }
////                    }
////                }, new Response.ErrorListener() {
////
////            @Override
////            public void onErrorResponse(VolleyError error) {
////                loadingDialog.dismiss();
////            }
////
////        }) {
////            @Override
////            public Map<String, String> getHeaders() throws AuthFailureError {
////                HashMap<String, String> map = new HashMap<>();
////                map.put("key", "12345");
////                return map;
////            }
////
////        };
////        rq.add(jsonObjReq);
////    }
//
//
//    static class ViewHolder {
//        ImageView ivShoppingItems;
//        TextView tvShoppingPrice, tvShoppingName, tvShoppingBrand;
//        Button btnShoppingDetails, btnShoppingRemove;
//
//    }
//}
