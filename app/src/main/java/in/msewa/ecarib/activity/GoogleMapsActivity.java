//package in.msewa.vpayqwik.activity;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.TextView;
//
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.GoogleMapOptions;
//import com.google.android.gms.maps.model.BitmapDescriptor;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.maps.android.clustering.Cluster;
//import com.google.maps.android.clustering.ClusterItem;
//import com.google.maps.android.clustering.ClusterManager;
//import com.google.maps.android.clustering.view.DefaultClusterRenderer;
//
//import in.msewa.model.Person;
//import in.msewa.vpayqwik.R;
//
//
//public class GoogleMapsActivity extends BaseGoogleMapsActivity implements ClusterManager.OnClusterClickListener<Person>,
//  ClusterManager.OnClusterItemClickListener<Person>,
//  ClusterManager.OnClusterItemInfoWindowClickListener<Person> {
//
//  private ClusterManager<Person> mClusterManager;
//  private GoogleMap googleMaps;
//  private Person clickedVenueMarker;
//
//  @Override
//  protected void onCreate(Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//  }
//
//  @Override
//  public void onMapReady(GoogleMap googleMap) {
//    setupMap(googleMap);
////
//    googleMaps = googleMap;
//    mClusterManager = new ClusterManager<>(this, googleMap);
//    googleMap.setOnCameraIdleListener(mClusterManager);
//    googleMap.setOnMarkerClickListener(mClusterManager);
//    googleMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
//    googleMap.setOnInfoWindowClickListener(mClusterManager);
//
//    addPersonItems();
//
//    RenderClusterInfoWindow renderClusterInfoWindow = new RenderClusterInfoWindow(this, googleMap, mClusterManager);
////    mClusterManager.setRenderer(renderClusterInfoWindow);
//    mClusterManager.setAnimation(true);
//    mClusterManager.cluster();
//    mClusterManager.setOnClusterItemClickListener(this);
//    mClusterManager.setOnClusterItemInfoWindowClickListener(this);
//    mClusterManager.setOnClusterClickListener(this);
//    mClusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//      @Override
//      public View getInfoWindow(Marker marker) {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        final View view = inflater.inflate(R.layout.custom_info_window, null);
//
//        TextView venueNameTextView = (TextView) view.findViewById(R.id.venue_name);
//
//        TextView venueAddressTextView = (TextView) view.findViewById(R.id.venue_address);
//        venueNameTextView.setText(clickedVenueMarker.getName());
//        venueAddressTextView.setText(clickedVenueMarker.getTitle());
//
//        return view;
//      }
//
//      @Override
//      public View getInfoContents(Marker marker) {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        final View view = inflater.inflate(R.layout.custom_info_window, null);
//
//        TextView venueNameTextView = (TextView) view.findViewById(R.id.venue_name);
//
//        TextView venueAddressTextView = (TextView) view.findViewById(R.id.venue_address);
//        venueNameTextView.setText(clickedVenueMarker.getName());
//        venueAddressTextView.setText(clickedVenueMarker.getTitle());
//
//        return view;
//
//      }
//    });
//    mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//      @Override
//      public View getInfoWindow(Marker marker) {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        final View view = inflater.inflate(R.layout.custom_info_window, null);
//
//        TextView venueNameTextView = (TextView) view.findViewById(R.id.venue_name);
//
//        TextView venueAddressTextView = (TextView) view.findViewById(R.id.venue_address);
//        venueNameTextView.setText(clickedVenueMarker.getName());
//        venueAddressTextView.setText(clickedVenueMarker.getTitle());
//
//        return view;
//      }
//
//      @Override
//      public View getInfoContents(Marker marker) {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        final View view = inflater.inflate(R.layout.custom_info_window, null);
//
//        TextView venueNameTextView = (TextView) view.findViewById(R.id.venue_name);
//
//        TextView venueAddressTextView = (TextView) view.findViewById(R.id.venue_address);
//        venueNameTextView.setText(clickedVenueMarker.getName());
//        venueAddressTextView.setText(clickedVenueMarker.getTitle());
//
//        return view;
//
//      }
//    });
//
//  }
//
//  private void addPersonItems() {
//    for (int i = 0; i < 6; i++) {
//      mClusterManager.addItem(new Person(-26.187616, 28.079329, "PJ", "https://twitter.com/pjapplez"));
//      mClusterManager.addItem(new Person(41.643414, 41.639900, "PJ2", "https://twitter.com/pjapplez"));
//      mClusterManager.addItem(new Person(18.101904, 78.852074, "PJ5", "https://twitter.com/pjapplez"));
//      mClusterManager.addItem(new Person(15.756595, 76.192696, "PJ6", "https://twitter.com/pjapplez"));
//      mClusterManager.addItem(new Person(25.989836, 79.450035, "PJ7", "https://twitter.com/pjapplez"));
//      mClusterManager.addItem(new Person(28.590361, 78.571762, "PJ8", "https://twitter.com/pjapplez"));
//    }
//
//  }
//
//  @Override
//  public boolean onClusterClick(Cluster<Person> cluster) {
//    LatLngBounds.Builder builder = LatLngBounds.builder();
//
//    for (ClusterItem item : cluster.getItems()) {
//      LatLng venuePosition = item.getPosition();
//      builder.include(venuePosition);
//    }
//
//    final LatLngBounds bounds = builder.build();
//
//    try {
//      googleMaps.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));
//    } catch (Exception error) {
//    }
//
//    return true;
//  }
//
//
//  @Override
//  public boolean onClusterItemClick(Person person) {
//    clickedVenueMarker = person;
//    Intent venueActivity = new Intent(this, LoginRegActivity.class);
//    startActivity(venueActivity);
//
//    return false;
//  }
//
//  @Override
//  public void onClusterItemInfoWindowClick(Person person) {
//    Intent venueActivity = new Intent(this, LoginRegActivity.class);
//    startActivity(venueActivity);
//
//  }
//
//  @Override
//  public void onPointerCaptureChanged(boolean hasCapture) {
//
//  }
//
//  private class RenderClusterInfoWindow extends DefaultClusterRenderer<Person> {
//
//    private final Context mContext;
//
//    public RenderClusterInfoWindow(Context context, GoogleMap map, ClusterManager<Person> clusterManager) {
//      super(context, map, clusterManager);
//      mContext = context;
//    }
//
//    @Override
//    protected void onClusterItemRendered(Person clusterItem, Marker marker) {
//      super.onClusterItemRendered(clusterItem, marker);
//      marker.showInfoWindow();
//    }
//
//    @Override
//    protected void onBeforeClusterItemRendered(Person item, MarkerOptions markerOptions) {
////      Bitmap venueCircle = VenueCircleFactory.createFromVenue(item);
////      markerOptions.icon(BitmapDescriptorFactory.fromBitmap(venueCircle);
//      markerOptions.snippet(item.getName());
//      markerOptions.title(item.getTitle());
////      markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_make_a_donation));
//
//    }
//
//    @Override
//    protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
////      Bitmap venueCircle = VenueCircleFactory.createFromCluster(cluster);
////      markerOptions.icon(BitmapDescriptorFactory.fromBitmap(venueCircle);
//
//
//    }
//
//    @Override
//    protected boolean shouldRenderAsCluster(Cluster<Person> cluster) {
//      return cluster.getSize() > 1;
//    }
//
//  }
//}
