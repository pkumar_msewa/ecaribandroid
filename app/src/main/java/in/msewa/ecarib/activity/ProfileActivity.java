package in.msewa.ecarib.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.model.UserModel;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;
import in.msewa.util.TLSSocketFactory;


/**
 * Created by Ksf on 3/16/2016.
 */
public class ProfileActivity extends AppCompatActivity {

    private TextView tvName, tvMobile, tvEmail;
    private Button btnLogout;
    private Toolbar tbProfile;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        loadDlg = new LoadingDialog(ProfileActivity.this);

        tbProfile = (Toolbar) findViewById(R.id.tbProfile);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        tvName = (TextView) findViewById(R.id.tvName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvEmail = (TextView) findViewById(R.id.tvEmail);


        tbProfile.setBackgroundColor(getColors(getApplicationContext(),R.color.colorPrimary));
        tbProfile.setNavigationIcon(R.drawable.ic_back);
        tbProfile.setTitle("Profile");
        tbProfile.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if(session.getIsValid()==1){
            tbProfile.setTitle("User Profile");
            tvEmail.setText("Email: "+session.getUserEmail());
            tvName.setText("Hello! "+session.getUserFirstName()+ " " + session.getUserLastName());
            tvMobile.setText("RupEase Id:"+session.getUserMobileNo());

            Log.i("Session Id",session.getUserSessionId());
        }

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDlg.show();
                attemptLogout();
            }
        });



    }

    public void attemptLogout() {
        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_LOGOUT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String code = jsonObj.getString("code");

                    if(code!=null && code.equals("S00")){
                        UserModel.deleteAll(UserModel.class);
                        loadDlg.dismiss();
                        Intent resultIntent = new Intent();
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }
                    else{
                        loadDlg.dismiss();
                        Toast.makeText(getApplicationContext(), "Error occurred, try again", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    loadDlg.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadDlg.dismiss();
                Toast.makeText(getApplicationContext(),	"Error connecting to server", Toast.LENGTH_LONG).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("sessionId", session.getUserSessionId());
                return map;
            }



        };

      PayQwikApplication.getInstance().addToRequestQueue(postReq,"text");

    }

    public static final int getColors(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

}
