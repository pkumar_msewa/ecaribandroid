package in.msewa.ecarib.activity.travelKhanaAvtivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.msewa.custom.CustomAlertDialog;
import in.msewa.custom.CustomToast;
import in.msewa.custom.LoadingDialog;
import in.msewa.metadata.ApiUrl;
import in.msewa.metadata.AppMetadata;
import in.msewa.model.BusSeatModel;
import in.msewa.model.TravelMyOrderModel;
import in.msewa.model.UserModel;
import in.msewa.util.NetworkErrorHandler;
import in.msewa.ecarib.PayQwikApplication;
import in.msewa.ecarib.R;


/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class MyOrdersActivity extends AppCompatActivity {

    private LinearLayout llMenuDetails;

    private long desId, sourceId;

    private HashMap<String, BusSeatModel> seatSelectedMap;
    private TextView tvOrderId, tvStatus, tvTrainNameNo, tvDateTime, tvCustomerName, tvSeatCoach, tvPnr, tvPayableAmount;
    private String orderID;
    private LoadingDialog loadingDialog;
    private UserModel session = UserModel.getInstance();
    private ArrayList<TravelMyOrderModel> menuOrderlist;
    private String tag_json_obj = "json_travel_my_orders";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_travelkhana_my_orders);

        tvOrderId = (TextView) findViewById(R.id.tvOrderId);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvTrainNameNo = (TextView) findViewById(R.id.tvTrainNameNo);
        tvDateTime = (TextView) findViewById(R.id.tvDateTime);
        tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
        tvSeatCoach = (TextView) findViewById(R.id.tvSeatCoach);
        tvPnr = (TextView) findViewById(R.id.tvPnr);
        tvPayableAmount = (TextView) findViewById(R.id.tvPayableAmount);
        loadingDialog = new LoadingDialog(MyOrdersActivity.this);

        //press back button in toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llMenuDetails = (LinearLayout) findViewById(R.id.llMenuDetails);
        orderID = getIntent().getStringExtra("OrderID");
        getBookedTicketsList();
    }

    public void getBookedTicketsList() {
        loadingDialog.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("orderId", orderID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("TRACURL", ApiUrl.URL_TRAVELKHANA_TRACKER);
            Log.i("TRACKREQ",jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_TRAVELKHANA_TRACKER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("BUSBOOKEDRES", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        String message = jsonObj.getString("message");

                        if (code != null && code.equals("S00")) {
                            JSONArray userOrderArray = jsonObj.getJSONArray("tKUserOrderDTO");

                                JSONObject c = userOrderArray.getJSONObject(0);
                                long userOrderId = c.getLong("userOrderId");
                                String name = c.getString("name");
                                String coach = c.getString("coach");
                                String seat = c.getString("seat");
                                long pnr = c.getLong("pnr");
                                long totalAmount = c.getLong("totalAmount");
                                long trainNo = c.getLong("trainNo");
                                String date = c.getString("date");
                                String trainName = c.getString("trainName");
                                String eta = c.getString("eta");
                                String status = c.getString("status");

                                JSONArray menuDetails = c.getJSONArray("tkTrackMenuDTO");
                                if (menuDetails.length() != 0) {
                                    menuOrderlist = new ArrayList<>();
                                    TravelMyOrderModel initialModel = new TravelMyOrderModel("ITEM","QTY");
                                    menuOrderlist.add(initialModel);
                                    for (int j = 0; j < menuDetails.length(); j++) {
                                        JSONObject p = menuDetails.getJSONObject(j);
                                        String itemName = p.getString("itemName");
                                        long quantity = p.getLong("quantity");
                                        TravelMyOrderModel travelMyOrderModel = new TravelMyOrderModel(itemName,String.valueOf(quantity));
                                        menuOrderlist.add(travelMyOrderModel);
                                    }
                                }
                            tvOrderId.setText("Order # "+String.valueOf(userOrderId));
                            tvStatus.setText(status);
                            if(status.equalsIgnoreCase("booked")){
                                tvStatus.setTextColor(getResources().getColor(R.color.success));
                            }else if(status.equalsIgnoreCase("cancelled")) {
                                tvStatus.setTextColor(getResources().getColor(R.color.faliure));
                            }else {
                                tvStatus.setTextColor(getResources().getColor(R.color.others));
                            }
                            tvTrainNameNo.setText(String.valueOf(trainNo)+" / "+trainName);
                            tvDateTime.setText(date+" ETA : "+eta);
                            tvCustomerName.setText(name);
                            tvSeatCoach.setText(coach+" / "+seat);
                            tvPnr.setText("PNR : "+pnr );
                            tvPayableAmount.setText("Total Amount Paid : "+getResources().getString(R.string.rupease)+" "+String.valueOf(totalAmount));
                            generateViewsForSeat();
                            loadingDialog.dismiss();

                        } else if (code != null && code.equals("F03")) {
                            showInvalidSessionDialog();
                            loadingDialog.dismiss();
                        } else {
                            loadingDialog.dismiss();
                            CustomToast.showMessage(MyOrdersActivity.this, message);
                            finish();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadingDialog.dismiss();
                        Toast.makeText(MyOrdersActivity.this, "Oops, something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(MyOrdersActivity.this, NetworkErrorHandler.getMessage(error, MyOrdersActivity.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void generateViewsForSeat() {

        for (int i=0; i<menuOrderlist.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View passengerDetailView = layoutInflater.inflate(R.layout.view_travelkhana_order_detail, null);
            llMenuDetails.addView(passengerDetailView);
            TextView tvItemName = (TextView) passengerDetailView.findViewById(R.id.tvItemName);
            tvItemName.setText(menuOrderlist.get(i).getItemName());
            TextView tvItemQuantity = (TextView) passengerDetailView.findViewById(R.id.tvItemQuantity);
            tvItemQuantity.setText(menuOrderlist.get(i).getItemQuantity());
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(MyOrdersActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(MyOrdersActivity.this).sendBroadcast(intent);
    }
}
