//package in.msewa.upimerchant;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.os.Build;
//import android.os.Handler;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.Toast;
//
//import com.fss.controller.MainActivity;
//import com.fss.utility.UtilityClass;
//
//import org.apache.commons.lang.RandomStringUtils;
//
//import java.math.BigInteger;
//import java.security.SecureRandom;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.HashMap;
//import java.util.Vector;
//
//import in.msewa.model.ResposeXmlModel_Mer;
//import in.msewa.model.UserModel;
//import in.msewa.util.AESAlgorithm_Mer;
//import in.msewa.util.UtilityClass_Mer;
//
//public class MerchantDetails {
//
//  public static String PayerCode;
//  String customer_name, customer_email, amount_entered, note_entered;
//  Boolean setValidation = true;
//  int permissionCheck;
//
//
//  // Optional Params
//  public static String Mer_CurrentLocation = "SIPCOT-Thalambur Road, Semmanjeri";
//  public static String Mer_GeoCode = "12.8355885,80.270718";
//  public static String Mer_Device_Internet_IP = "10.10.10.10";
//
//
//  // Mandatory Params
//  public static String merchant_vpa;
//  public static String Mer_TransPassword = "";
//  public static String Mer_DeviceUniqueSerialId = Build.SERIAL;
//  public static String Mer_MerchantId;
//  public static String Mer_TerminalId;
//  public static String Mer_SubMerchantID;
//  public static String Mer_PaymentType;
//  public static String Mer_Txn_Ref_Id = "";
//  public static String Mer_Txn_Currency = "INR";
//  public static String Mer_MSDK_TokenCode;
//  public static String Mer_ENCRYPTED_SERVER_TOKEN_CODE;
//  String Mer_Credentials;
//
//  public SecureRandom random = new SecureRandom();
//  private UserModel session = UserModel.getInstance();
//
//  public String nextSessionId() {
//    return new BigInteger(130, random).toString(32);
//  }
//
//  HashMap<String, String> storeHeaderDatas;
//
//  HashMap<String, String> storeTokenDatas;
//
//
//  String TokenFromFss, Mer_SDK_Token;
//
//  String DeviceId;
//  String AppPackageId;
//  String Type;
//
//  ProgressDialog pd;
//
//  Boolean FinalApproval_Enter_toMSDK = false;
//
//  String MobileNumber = "";
//
//  // Decrypted DEK
//
//  String DEK_DECRYPTED;
//
//  EditText merchantId, terminalId, transCodeId;
//  String MerId, TerId, Passcode;
//  LinearLayout submit_lay, button_lay;
//  Button submit;
//
//  Context mContext;
//
//
//  public MerchantDetails(Context context) {
//    this.mContext = context;
//  }
//
//
//  public void ReStartProcess() {
//
//    new Handler().postDelayed(new Runnable() {
//
//      @Override
//      public void run() {
//
//        call_DEK_WebServices(mContext);
//
//      }
//    }, 2 * 1000); // wait for 5 seconds
//  }
////
////  public void setonclicklistener() {
////
////    submit.setOnClickListener(new View.OnClickListener() {
////      @Override
////      public void onClick(View view) {
////
////        result.setText("");
////
////        MerId = merchantId.getText().toString().trim();
////        TerId = terminalId.getText().toString().trim();
////        Passcode = transCodeId.getText().toString().trim();
////
////        if (MerId == null || MerId.length() == 0) {
////          setValidation = false;
////          merchantId.setError("Please enter valid merchant Id.");
////        }
////        if (TerId == null || TerId.length() == 0) {
////          setValidation = false;
////          terminalId.setError("Please enter valid terminal Id.");
////        }
////        if (Passcode == null || Passcode.length() == 0) {
////          setValidation = false;
////          transCodeId.setError("Please enter valid TransPasscode.");
////        }
////
////        Mer_MerchantId = MerId;
////        Mer_TerminalId = TerId;
////        Mer_TransPassword = Passcode;
////
////        if (setValidation) {
////
////          call_DEK_WebServices();
////        }
////      }
////    });
////
////
////    Pay.setOnClickListener(new View.OnClickListener() {
////      @Override
////      public void onClick(View view) {
////
////        Mer_PaymentType = "Pay";
////
////        result.setText("");
////
////        customer_name = customerName.getText().toString().trim();
////        customer_email = customerEmail.getText().toString().trim();
////        amount_entered = amount.getText().toString().trim();
////        merchant_vpa = merchantvpa.getText().toString().trim();
////        note_entered = note.getText().toString().trim();
////        MerId = merchantId.getText().toString().trim();
////        TerId = terminalId.getText().toString().trim();
////        Passcode = transCodeId.getText().toString().trim();
////
////
////        setValidation = true;
////
////        if (customer_name == null || customer_name.length() == 0) {
////          setValidation = false;
////          customerName.setError("Please enter valid name.");
////        }
////        if (customer_email == null || customer_email.length() == 0) {
////          setValidation = false;
////          customerEmail.setError("Please enter valid email.");
////        }
////        if (amount_entered == null || amount_entered.length() == 0) {
////          setValidation = false;
////          amount.setError("Please enter valid amount.");
////        } else if (Double.parseDouble(amount_entered) == 0) {
////          setValidation = false;
////          amount.setError("Please enter valid amount.");
////        }
////        if (merchant_vpa == null || merchant_vpa.length() == 0) {
////          setValidation = false;
////          merchantvpa.setError("Please enter valid VPA.");
////        }
////        if (MerId == null || MerId.length() == 0) {
////          setValidation = false;
////          merchantId.setError("Please enter valid merchant Id.");
////        }
////        if (TerId == null || TerId.length() == 0) {
////          setValidation = false;
////          terminalId.setError("Please enter valid terminal Id.");
////        }
////        if (Passcode == null || Passcode.length() == 0) {
////          setValidation = false;
////          transCodeId.setError("Please enter valid TransPasscode.");
////        }
////
////        Mer_MerchantId = MerId;
////        Mer_TerminalId = TerId;
////        Mer_TransPassword = Passcode;
////
////        if (setValidation) {
////
////          if (FinalApproval_Enter_toMSDK == true) {
////
////            Intent in = new Intent(MerchantDetails.this, HomeMainActivity.class);
////            in.putExtra("Mer_CustomerName", customer_name);
////            in.putExtra("Mer_Customer_EmailId", customer_email);
////            in.putExtra("Mer_PayAmount", amount_entered);
////            in.putExtra("Mer_VPA", merchant_vpa);
////            in.putExtra("Mer_Notes", note_entered);
////            in.putExtra("Mer_PayeeCode", Mer_PayeeCode);
////
////            in.putExtra("Mer_TransPassword", Mer_TransPassword);
////            in.putExtra("Mer_DeviceUniqueSerialId", Mer_DeviceUniqueSerialId);
////            in.putExtra("Mer_MerchantId", Mer_MerchantId);
////            in.putExtra("Mer_TerminalId", Mer_TerminalId);
////            in.putExtra("Mer_SubMerchantID", Mer_SubMerchantID);
////            in.putExtra("Mer_Credentials", Mer_Credentials);
////
////            in.putExtra("Mer_Token", Mer_ENCRYPTED_SERVER_TOKEN_CODE);
////
////            in.putExtra("Mer_PaymentType", Mer_PaymentType);
////// Pay - Existing Pay Functionality
////// Additional MSDK Functionality added as per NPCI Circular 15,15A,15B,15C etc..
//////CollectApproval//ScanPay//DeRegisterUPI
//////            if (scannedData != null && scannedData.length() >= 0) {
//////              in.putExtra("Mer_PaymentScanData", scannedData);
//////            } else {
////            in.putExtra("Mer_PaymentScanData", "");
//////            }
////
////            in.putExtra("Mer_CurrentLocation", Mer_CurrentLocation);
////            in.putExtra("Mer_GeoCode", Mer_GeoCode);
////            in.putExtra("Mer_Device_Internet_IP", Mer_Device_Internet_IP);
////            in.putExtra("Mer_Txn_Ref_Id", Mer_Txn_Ref_Id); // Mandatory
////            in.putExtra("Mer_Txn_Currency", Mer_Txn_Currency); // Mandatory
////            startActivityForResult(in, 0);
////
////
////          } else {
////            Toast.makeText(mContext, "Merchant Validation Failed.Please check and re-start again for validation.", Toast.LENGTH_LONG).show();
////
////            ReStartProcess();
////          }
////        }
////      }
////    });
////  }
//
//  public void showProgressDialog(String msg) {
//
//    if (pd == null) {
//      pd.setMessage(msg);
//      pd.show();
//    } else {
//      pd.setMessage(msg);
//      pd.show();
//    }
//  }
//
//  public void hideProgressDialog() {
//
//    if (pd == null) {
//
//    } else {
//      pd.hide();
//    }
//  }
//
//  public void call_DEK_WebServices(Context mContext) {
//
//    try {
//
//      String inputs[] = {"no details"};
//
//      if (UtilityClass_Mer.isNetworkConnected(mContext)) {
//
//
//        storeHeaderDatas = new HashMap<>();
//
//        storeHeaderDatas = UtilityClass_Mer.storeDefaultValues(mContext);
//
//        storeHeaderDatas.put(UtilityClass_Mer.MOBILENO, MobileNumber);
//
//        new Ws_GetDEK(mContext).execute(inputs);
//
//      } else {
//
//        Toast.makeText(mContext, "Internet Not Available", Toast.LENGTH_SHORT)
//          .show();
//      }
//
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//
//  public void call_Token_from_MSDK(String merchantcredentials, Context mContext) {
//
//    if (merchantcredentials != null && merchantcredentials.length() > 0) {
//
//      DeviceId = Mer_DeviceUniqueSerialId;
//      AppPackageId = UtilityClass_Mer.getPackageName(mContext);
//      Type = "First";
//
//      TokenFromFss = UtilityClass.getMainSDKToken(mContext, DeviceId, AppPackageId, Type);
//
//      if (TokenFromFss != null && TokenFromFss.length() > 0) {
//
//        callSend_SDK_Token_WebServices(TokenFromFss, mContext);
//
//      } else {
//        Toast.makeText(mContext, "Problem Generating TokenCode fom MSDK.Please re-initiate again from MSDK Token.", Toast.LENGTH_SHORT).show();
//      }
//
//    } else {
//      Toast.makeText(mContext, "Problem getting Merchant Credentials.Please re-initiate from DEK services.", Toast.LENGTH_SHORT).show();
//    }
//  }
//
//  public String callEncrypWork(String encDEK, String KEK_HC, String tranId, String passcode) {
//
//    try {
//      Mer_Txn_Ref_Id = tranId;
//      Mer_TransPassword = passcode;
//
//      DEK_DECRYPTED = AESAlgorithm_Mer.decryptTextusingAES(encDEK, KEK_HC);
//      UtilityClass.LogPrinter('I', "upi", "DEK_DECRYPTED::::" + DEK_DECRYPTED);
//
//      String Data = Mer_Txn_Ref_Id + "#" + Mer_TransPassword;
//      String deckek = AESAlgorithm_Mer.encryptTextusingAES(Data, DEK_DECRYPTED);
//
//      Mer_Credentials = deckek;
//
////      call_Token_from_MSDK(Mer_Credentials, context);
//
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//    return Mer_Credentials;
//  }
//
//
//  public void callSend_SDK_Token_WebServices(String TokenFromFss, Context mContext) {
//
//    try {
//
//      String inputs[] = {"no details"};
//
//      if (UtilityClass_Mer.isNetworkConnected(mContext)) {
//
//
//        storeHeaderDatas = new HashMap<>();
//
//        storeHeaderDatas = UtilityClass_Mer.storeDefaultValues(mContext);
//        storeHeaderDatas.put("MerchantCredentials", Mer_Credentials);
//
//        storeTokenDatas = new HashMap<>();
//
//        storeTokenDatas.put("TokenCode", TokenFromFss);
//        storeTokenDatas.put("PackageName", UtilityClass_Mer.getPackageName(mContext));
//
//        new WsSDKTokenGeneration(mContext).execute(inputs);
//
//      } else {
//
//        Toast.makeText(mContext, "Internet Not Available", Toast.LENGTH_SHORT)
//          .show();
//      }
//
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//
//
////  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
////
////    super.onActivityResult(requestCode, resultCode, data);
////
////    Log.v("upi", "resultCode::::::::" + resultCode);
////    Log.v("upi", "requestCode::::::::" + requestCode);
////    Log.v("upi", "data::::::::" + data);
////
////    if (requestCode == 0) {
////
////      submit_lay.setVisibility(View.VISIBLE);
////      button_lay.setVisibility(View.GONE);
////
////      if (data != null) {
////        String response = data.getStringExtra("response");
////
////        result.setText(response);
////      }
////    } else {
////      result.setText("requestCode Problem");
////    }
////  }
//
//  public String buildNewP37() {
//
//    GregorianCalendar cal = new GregorianCalendar();
//    cal.setGregorianChange(new Date(Long.MAX_VALUE));
//
//    Date todayJD = cal.getTime();
//    Date date = new Date();
//
//    SimpleDateFormat sdfJuliandayOfYear = new SimpleDateFormat("DDD");
//    String julianDate = sdfJuliandayOfYear.format(todayJD);
//    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//    String currDate = dateFormat.format(date.getTime());
//
//    String p37 = currDate.substring(3, 4) + julianDate
//      + currDate.substring(8, 10)
//      + RandomStringUtils.randomNumeric(6);
//
//    return p37;
//  }
//
//  //-----------------------------------------------------------------------------------------------------------
//
//  class Ws_GetDEK extends AsyncTask<String[], Void, String> {
//
//    Context activity;
//
//    public Ws_GetDEK(Context mobileNoValidation) {
//
//      this.activity = mobileNoValidation;
//
//    }
//
//    @Override
//    public void onPreExecute() {
//      super.onPreExecute();
//
//      showProgressDialog("Loading DEK Services.");
//    }
//
//    @Override
//    protected String doInBackground(String[]... params) {
//
//      return ConnectWebService_Mer.Fn_GenerateMerchantDEK(storeHeaderDatas, activity);
//
//    }
//
//    protected void onPostExecute(String result) {
//
//      try {
//
//        hideProgressDialog();
//
//        processtheResult(result);
//
//      } catch (Exception e) {
//
//        e.printStackTrace();
//
//      }
//    }
//
//  }
//
//  //-----------------------------------------------------------------------------------------------------------
//  public void processtheResult(String result) {
//
//    if (result != null) {
//
//      Vector<ResposeXmlModel_Mer> vectorObjectRespModel = new Vector<ResposeXmlModel_Mer>();
//
//      vectorObjectRespModel = ConnectWebService_Mer.vectorObjectResponseModel;
//
//      if (vectorObjectRespModel != null) {
//
//        if (vectorObjectRespModel.size() > 0) {
//
//          for (int i = 0; i < vectorObjectRespModel.size(); i++) {
//
//            String responseCode = vectorObjectRespModel.get(i).getResCode();
//
//            String responseDesc = vectorObjectRespModel.get(i)
//              .getResDesc();
//
//            String DEKKEY = vectorObjectRespModel.get(i).getDEKKey();
//
//            if (responseCode != null) {
//
//              if (responseCode.equalsIgnoreCase("000")) {
//
////                callEncrypWork(DEKKEY, mContext);
//
//              } else {
//
//                Toast.makeText(mContext, "Error:" + responseDesc, Toast.LENGTH_SHORT).show();
//              }
//
//            } else {
//
//              Toast.makeText(mContext, "Due to connectivity/UPI Network problem.", Toast.LENGTH_SHORT).show();
//            }
//
//          }
//
//        }
//      } else {
//        Toast.makeText(mContext, "Due to connectivity/UPI Network problem.", Toast.LENGTH_SHORT).show();
//      }
//
//    } else {
//      Toast.makeText(mContext, "Due to connectivity/UPI Network problem.", Toast.LENGTH_SHORT).show();
//
//    }
//
//  }
//
//  class WsSDKTokenGeneration extends AsyncTask<String[], Void, String> {
//
//    Context activity;
//
//    public WsSDKTokenGeneration(Context mobileNoValidation) {
//
//      this.activity = mobileNoValidation;
//    }
//
//    @Override
//    public void onPreExecute() {
//
//      super.onPreExecute();
//
////      showProgressDialog("Sending Token to server");
//    }
//
//    @Override
//    protected String doInBackground(String[]... params) {
//
//      return ConnectWebService_Mer.Fn_MerchantTokenGen(storeHeaderDatas, storeTokenDatas, mContext);
//
//    }
//
//    protected void onPostExecute(String result) {
//
//      try {
//
////        hideProgressDialog();
//
//        if (result != null) {
//
//          Vector<ResposeXmlModel_Mer> vectorObjectRespModel = new Vector<ResposeXmlModel_Mer>();
//
//          vectorObjectRespModel = ConnectWebService_Mer.vectorObjectResponseModel;
//
//          if (vectorObjectRespModel != null) {
//
//            if (vectorObjectRespModel.size() > 0) {
//
//              for (int i = 0; i < vectorObjectRespModel.size(); i++) {
//
//                String responseCode = vectorObjectRespModel.get(i)
//                  .getResCode();
//
//                String responseDesc = vectorObjectRespModel.get(i)
//                  .getResDesc();
//
//                String Mer_Token = vectorObjectRespModel.get(i).getToken();
//
//                if (responseCode != null) {
//
//                  if (responseCode.equalsIgnoreCase("000")) {
//
//                    Change_ServerToken_To_Encrypted(Mer_Token);
//
////                    submit_lay.setVisibility(View.GONE);
////                    button_lay.setVisibility(View.VISIBLE);
//
//                  } else {
//
//                    Toast.makeText(mContext, "Error:" + responseDesc, Toast.LENGTH_SHORT).show();
//                  }
//
//                } else {
//                  Toast.makeText(mContext, "Due to connectivity/UPI Network problem.", Toast.LENGTH_SHORT).show();
//
//                }
//
//              }
//
//            }
//          } else {
//            Toast.makeText(mContext, "Due to connectivity/UPI Network problem.", Toast.LENGTH_SHORT).show();
//          }
//
//        } else {
//          Toast.makeText(mContext, "Due to connectivity/UPI Network problem.", Toast.LENGTH_SHORT).show();
//
//        }
//
//      } catch (Exception e) {
//
//        e.printStackTrace();
//
//      }
//    }
//
//  }
//
//  public boolean Change_ServerToken_To_Encrypted(String mer_Token) {
//
//    try {
//
//      String Data = mer_Token;
//
//      Mer_ENCRYPTED_SERVER_TOKEN_CODE = AESAlgorithm_Mer.encryptTextusingAES(Data, DEK_DECRYPTED);
//
//      FinalApproval_Enter_toMSDK = true;
//
//      Intent in = new Intent(mContext, MainActivity.class);
//
////    in.putExtra("Mer_CustomerName",session.getUserFirstName());
////    in.putExtra("Mer_Customer_EmailId",session.getUserEmail());
////    in.putExtra("Mer_PayAmount",amount);
////    in.putExtra("Mer_VPA",id);
////    in.putExtra("Mer_Notes","Demo UPI test");
////    in.putExtra("Mer_PayerCode", UPIConstants.PAYEE_CODE);
////    in.putExtra("Mer_TransPassword",trxPass);
////    in.putExtra("Mer_DeviceUniqueSerialId",SecurityUtil.getAndroidId(getActivity()));
////    in.putExtra("Mer_MerchantId",UPIConstants.MERCHANT_ID);
////    in.putExtra("Mer_SubMerchantID",UPIConstants.SUB_MERCHANT_ID);
////    in.putExtra("Mer_TerminalId",UPIConstants.TERMINAL_ID);
////    in.putExtra("Mer_Credentials", trxPass);
////    in.putExtra("Mer_Token", encrMerToken);
////    in.putExtra("Mer_PaymentType","pay"); // type = pay
////    in.putExtra("Mer_CurrentLocation","bangalore");
////    in.putExtra("Mer_Device_Internet_IP","100.100.100.100");
////    in.putExtra("Mer_Txn_Ref_Id", trxRefId);
////    in.putExtra("Mer_Txn_Currency", "INR");
//
////    startActivityForResult(in, 1);
//
//      in.putExtra("Mer_CustomerName", session.getUserFirstName());
//      in.putExtra("Mer_Customer_EmailId", session.getUserEmail());
//      in.putExtra("Mer_PayAmount", "10");
//      in.putExtra("Mer_VPA", merchant_vpa);
//      in.putExtra("Mer_Notes", "Demo UPI test");
//      in.putExtra("Mer_PayeeCode", MerchantDetails.PayerCode);
//      in.putExtra("Mer_TransPassword", MerchantDetails.Mer_TransPassword);
//      in.putExtra("Mer_DeviceUniqueSerialId", Mer_DeviceUniqueSerialId);
//      in.putExtra("Mer_MerchantId", MerchantDetails.Mer_MerchantId);
//      in.putExtra("Mer_TerminalId", MerchantDetails.Mer_TerminalId);
//      in.putExtra("Mer_SubMerchantID", MerchantDetails.Mer_SubMerchantID);
//
////            if (scannedData != null && scannedData.length() >= 0) {
////              in.putExtra("Mer_PaymentScanData", scannedData);
////            } else {
//      in.putExtra("Mer_PaymentScanData", "");
////            }
//      in.putExtra("Mer_Credentials", Mer_Credentials);
//      in.putExtra("Mer_Token", Mer_ENCRYPTED_SERVER_TOKEN_CODE);
//      in.putExtra("Mer_PaymentType", "Pay");
//      in.putExtra("Mer_CurrentLocation", "SIPCOT-Thalambur Road, Semmanjeri");
//      in.putExtra("Mer_GeoCode", "12.8355885,80.270718");
//      in.putExtra("Mer_Device_Internet_IP", MerchantDetails.Mer_Device_Internet_IP);
//      in.putExtra("Mer_Txn_Ref_Id", Mer_Txn_Ref_Id);
//      in.putExtra("Mer_Txn_Currency", "INR");
//      ((Activity) mContext).startActivityForResult(in, 1);
//
//    } catch (Exception e) {
//
//      FinalApproval_Enter_toMSDK = false;
//
//      e.printStackTrace();
//    }
//    return FinalApproval_Enter_toMSDK;
//  }
//
//  public String getToken() {
//    return Mer_ENCRYPTED_SERVER_TOKEN_CODE;
//  }
//}
