//package in.msewa.upimerchant;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.fss.utility.UtilityClass;
//import com.fss.webservices.SslRequest;
//
//import org.ksoap2.SoapEnvelope;
//import org.ksoap2.serialization.PropertyInfo;
//import org.ksoap2.serialization.SoapObject;
//import org.ksoap2.serialization.SoapSerializationEnvelope;
//import org.ksoap2.transport.HttpsServiceConnectionSE;
//import org.ksoap2.transport.HttpsTransportSE;
//
//import java.io.IOException;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.UnrecoverableKeyException;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Set;
//import java.util.Vector;
//
//import javax.net.ssl.SSLContext;
//
//import in.msewa.model.ResposeXmlModel_Mer;
//import in.msewa.parsing.ParsingClass_Mer;
//import in.msewa.util.UtilityClass_Mer;
//
///**
// * Created by harikrishnanv on 02-03-2017.
// */
//
//public class ConnectWebService_Mer {
//
//    public static Vector<ResposeXmlModel_Mer> vectorObjectResponseModel;
//
//
//    public static String Fn_GenerateMerchantDEK(HashMap<String, String> storeHeaderDatas, Context mContext) {
//
//        Object response = null;
//
//        String METHOD_NAME = "GenerateMerchantDEK";
//
//        String NAMESPACE = "http://com/fss/upi";
//
//        String SOAP_ACTION = "";
//
//        HashMap<String, String> otherDetails = new HashMap<String, String>();
//        otherDetails.putAll(storeHeaderDatas);
//
//        try {
//
//            SoapObject RequestParent = new SoapObject(NAMESPACE, METHOD_NAME);
//
//            SoapObject Request1 = new SoapObject(NAMESPACE, "req");
//
//            PropertyInfo pi = new PropertyInfo();
//
//            SoapObject HeaderRequest = new SoapObject(NAMESPACE, "UPI");
//
//            Set mapSet = (Set) storeHeaderDatas.entrySet();
//
//            Iterator mapIterator = mapSet.iterator();
//
//            while (mapIterator.hasNext()) {
//
//                Map.Entry mapEntry = (Map.Entry) mapIterator.next();
//
//                // getKey Method of HashMap access a key of map
//                String keyValue = (String) mapEntry.getKey();
//
//                // getValue method returns corresponding key's value
//                String value = (String) mapEntry.getValue();
//
//                pi = new PropertyInfo();
//
//                pi.setNamespace("java:com.fss.upi.req");
//
//                pi.setName(keyValue);
//
//                pi.setValue(value);
//
//                HeaderRequest.addProperty(pi);
//            }
//
//            Request1.addSoapObject(HeaderRequest);
//
//            RequestParent.addSoapObject(Request1);
//
//            Log.v("upi", "-Fn_GenerateMerchantDEK--RequestParent:" + RequestParent);
//
//            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//                    SoapEnvelope.VER10);
//
//            soapEnvelope.dotNet = false;
//
//            soapEnvelope.setOutputSoapObject(RequestParent);
//
//            response = getHttpClientResponse(SOAP_ACTION, soapEnvelope,mContext);
//
//            vectorObjectResponseModel = new Vector<ResposeXmlModel_Mer>();
//
//            String MSGID = storeHeaderDatas.get(UtilityClass.MSGID).trim() ;
//
//            UtilityClass.LogPrinter('I', "UPI","METHOD_NAME:"+METHOD_NAME+" MSGID:"+ MSGID);
//
//            UtilityClass.LogPrinter('I', "UPI"," response:"+ response);
//
//            ParsingClass_Mer objectParsing = new ParsingClass_Mer();
//
//            vectorObjectResponseModel = objectParsing
//                    .readXMLRes_DEK(response,MSGID);
//
//            return response.toString();
//
//        } catch (Exception ex) {
//
//            ex.printStackTrace();
//
//            return null;
//        }
//
//    }
//
//    public static String Fn_MerchantTokenGen(HashMap<String, String> storeHeaderDatas,
//
//                                             HashMap<String, String> storeTokenDatas, Context mContext) {
//
//        Object response = null;
//
//        String METHOD_NAME = "MerchantTokenGen";
//
//        String NAMESPACE = "http://com/fss/upi";
//
//        String SOAP_ACTION = "";
//
//        try {
//
//            SoapObject RequestParent = new SoapObject(NAMESPACE, METHOD_NAME);
//
//            SoapObject Request1 = new SoapObject(NAMESPACE, "req");
//
//            PropertyInfo pi = new PropertyInfo();
//
//            Set mapSet = (Set) storeTokenDatas.entrySet();
//
//            Iterator mapIterator = mapSet.iterator();
//
//            while (mapIterator.hasNext()) {
//
//                Map.Entry mapEntry = (Map.Entry) mapIterator.next();
//
//                // getKey Method of HashMap access a key of map
//                String keyValue = (String) mapEntry.getKey();
//
//                // getValue method returns corresponding key's value
//                String value = (String) mapEntry.getValue();
//
//                //log("upi", "Key:" + keyValue + "--value:" + value);
//
//                pi = new PropertyInfo();
//
//                pi.setNamespace("java:com.fss.upi.req");
//
//                pi.setName(keyValue);
//
//                pi.setValue(value);
//
//                Request1.addProperty(pi);
//            }
//
//            SoapObject HeaderRequest = new SoapObject(NAMESPACE, "UPI");
//
//            mapSet = (Set) storeHeaderDatas.entrySet();
//
//            mapIterator = mapSet.iterator();
//
//            while (mapIterator.hasNext()) {
//
//                Map.Entry mapEntry = (Map.Entry) mapIterator.next();
//
//                // getKey Method of HashMap access a key of map
//                String keyValue = (String) mapEntry.getKey();
//
//                // getValue method returns corresponding key's value
//                String value = (String) mapEntry.getValue();
//
//                pi = new PropertyInfo();
//
//                pi.setNamespace("java:com.fss.upi.req");
//
//                pi.setName(keyValue);
//
//                pi.setValue(value);
//
//                HeaderRequest.addProperty(pi);
//            }
//
//            Request1.addSoapObject(HeaderRequest);
//
//            RequestParent.addSoapObject(Request1);
//
//            Log.v("upi", "-Fn_MerchantTokenGen--RequestParent:" + RequestParent);
//
//            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//                    SoapEnvelope.VER10);
//
//            soapEnvelope.dotNet = false;
//
//            soapEnvelope.setOutputSoapObject(RequestParent);
//
//            response = getHttpClientResponse(SOAP_ACTION, soapEnvelope,mContext);
//
//            vectorObjectResponseModel = new Vector<ResposeXmlModel_Mer>();
//
//            String MSGID = storeHeaderDatas.get(UtilityClass.MSGID).trim() ;
//
//            UtilityClass.LogPrinter('I', "UPI","METHOD_NAME:"+METHOD_NAME+" MSGID:"+ MSGID);
//
//            UtilityClass.LogPrinter('I', "UPI"," response:"+ response);
//
//            ParsingClass_Mer objectParsing = new ParsingClass_Mer();
//
//            vectorObjectResponseModel = objectParsing.readXMLRes_TokenGeneration(response,MSGID);
//
//            return response.toString();
//
//        } catch (Exception ex) {
//
//            ex.printStackTrace();
//
//            return null;
//        }
//
//    }
//
//    private static Object getHttpClientResponse(String SOAP_ACTION, SoapSerializationEnvelope ReqSoapEnvelope, Context context) {
//        try {
//
//            HttpsTransportSE androidHttpsTransport = new HttpsTransportSE(UtilityClass_Mer.PI_ENABLE_URL, 443, UtilityClass_Mer.MERCHANT_ENABLE, UtilityClass.WS_Timeout_Limit);
//            ((HttpsServiceConnectionSE) androidHttpsTransport.getServiceConnection()).setSSLSocketFactory(trustAllHosts(context).getSocketFactory());
//            androidHttpsTransport.call(SOAP_ACTION, ReqSoapEnvelope);
//
//            UtilityClass_Mer.LogPrinter('I', "response::", ""+ReqSoapEnvelope.getResponse());
//            return ReqSoapEnvelope.getResponse();
//        } catch (Exception e) {
//
//            return null;
//        }
//    }
//
//    protected static SSLContext trustAllHosts(Context mContext) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, IOException {
//
//        return SslRequest.allowAllSSL(mContext);
//    }
//}
