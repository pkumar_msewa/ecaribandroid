-optimizationpasses 5
-verbose
-useuniqueclassmembernames
-keepattributes SourceFile,LineNumberTable
-allowaccessmodification




#Keep classes that are referenced on the AndroidManifest
-keep public class * extends android.support.v7.app.AppCompatActivity
-keep public class * extends android.app.Application
-keep public class * extends com.orm.SugarRecord
-keep class in.msewa.vpayqwik.** { *; }
-keep class org.xmlpull.v1.** { *;}
 -dontwarn org.xmlpull.v1.**
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-adaptresourcefilenames
-keep class in.msewa.model.** { *; }
-keep class in.msewa.adapter.** { *; }
-keep class in.msewa.Animations.** { *; }
-keep class in.msewa.fragment.** { *; }
-keep class in.msewa.shoppingold.** { *; }
-keep class in.msewa.util.** { *; }
-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends Request<String>

-keep interface android.support.v4.app.** { *; }
-keep class android.support.v4.app.** { *; }
-keep class org.apache.http.** { *; }
-keep class okhttp3.** { *; }

-dontwarn org.apache.commons.**
-dontwarn org.apache.http.**
-dontwarn com.google.**
-dontwarn com.github.**
-dontwarn com.android.volley.**
-dontwarn com.androidquery.**
-dontwarn okio.**

-dontnote android.net.http.*
-dontnote org.apache.commons.codec.**
-dontnote org.apache.http.**
-dontnote okhttp3.**

-keeppackagenames org.jsoup.nodes
-keep public enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#-dontwarn okhttp3.**
#-dontwarn com.squareup.**


## Keep Picasso
#-keep class com.squareup.picasso.** { *; }
#-keepclasseswithmembers class * {
#    @com.squareup.picasso.** *;
#}
#-keepclassmembers class * {
#    @com.squareup.picasso.** *;
#}


 -ignorewarnings
## Keep OKHttp
# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-keep class com.google.android.gms.iid.zzd { *; }
-keep class android.support.v4.content.ContextCompat { *; }
## Keep AQuery
-keep class com.androidquery.** { *; }
-keepclasseswithmembers class * {
    @com.androidquery.** *;
}
-keepclassmembers class * {
    @com.androidquery.** *;
}
-keepclassmembers class com.dom925.xxxx {
   public *;
}
#To remove debug logs:
-assumenosideeffects class android.util.Log {
public static boolean isLoggable(java.lang.String, int);
    public static *** d(...);
    public static *** i(...);
    public static *** e(...);
    public static *** w(...);
    public static *** wtf(...);
    public static *** v(...);
}
-ignorewarnings
-dontnote android.net.http.*
-dontnote org.apache.commons.codec.**
-dontnote org.apache.http.**

#Maintain java native methods
-keepclasseswithmembernames class * {
    native <methods>;
}

#To maintain custom components names that are used on layouts XML.
#Uncomment if having any problem with the approach below
#-keep public class custom.components.package.and.name.**

#To maintain custom components names that are used on layouts XML:
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}


#To keep parcelable classes (to serialize - deserialize objects to sent through Intents)
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

#Keep the R
-keepclassmembers class **.R$* {
    public static <fields>;
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames class * implements android.os.Parcelable
-keepclassmembers class * implements android.os.Parcelable {
  public static final *** CREATOR;
}

-keep @interface android.support.annotation.Keep
-keep @android.support.annotation.Keep class *
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <methods>;
}

-keep @interface com.google.android.gms.common.annotation.KeepName
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
  @com.google.android.gms.common.annotation.KeepName *;
}

-keep @interface com.google.android.gms.common.util.DynamiteApi
-keep public @com.google.android.gms.common.util.DynamiteApi class * {
  public <fields>;
  public <methods>;
}

-dontwarn android.security.NetworkSecurityPolicy



-keep public class com.niki.config.**

 -keep public class com.niki.config.**{
   public protected *;
 }

 -keepclasseswithmembernames class * {
     native <methods>;
 }
  -keep class **.R$* {
  <fields>;
 }
 -dontwarn com.squareup.picasso.**
 -dontwarn retrofit2.**
 -dontwarn okio.**
 -dontwarn com.zendesk.util.**

 -dontwarn com.fasterxml.jackson.databind.**
 -dontwarn com.pubnub.**
 -keep class retrofit2.** { *; }
 -keepattributes Signature
 -keepattributes Exceptions

 -keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
 public static java.lang.String TABLENAME;
 }
 -keep class **$Properties

 # If you do not use SQLCipher:
 -dontwarn org.greenrobot.greendao.database.**
 # If you do not use Rx:
 -dontwarn rx.**

 -keep class org.greenrobot.greendao.**

 -keep class android.support.v7.internal.** { *; }
 -keep interface android.support.v7.internal.** { *; }
 -keep class android.support.v7.** { *; }
 -keep interface android.support.v7.** { *; }


 # For using GSON @Expose annotation
 -keepattributes *Annotation*

 # Gson specific classes
 -keep class sun.misc.Unsafe { *; }
 #-keep class com.google.gson.stream.** { *; }

 # Application classes that will be serialized/deserialized over Gson
 -keep class com.niki.models.** { *; }

 -keep class com.google.gson.**
 -keep class * implements com.google.gson.TypeAdapterFactory
 -keep class * implements com.google.gson.JsonSerializer
 -keep class * implements com.google.gson.JsonDeserializer
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keepattributes JavascriptInterface
-keepattributes Annotation

